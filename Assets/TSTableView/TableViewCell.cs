﻿using UnityEngine;
using System.Collections;
using Ignite.AOM;
using Zenject;

namespace Tacticsoft
{
    /// <summary>
    /// The base class for cells in a TableView. ITableViewDataSource returns pointers
    /// to these objects
    /// </summary>
    public class TableViewCell : MonoBehaviour
    {
        /// <summary>
        /// TableView will cache unused cells and reuse them according to their
        /// reuse identifier. Override this to add custom cache grouping logic.
        /// </summary>
        /// 

        public virtual string reuseIdentifier
        {
            get
            {
                return this.GetType().Name;
            }
        }

        public virtual void ClearCellData()
        {
            // Clear cell data before go back to reuseable list
        }

        public class Factory : Factory<GameObject, TableViewCell>
        {
        }
    }

    public class TableViewCellFactory : IFactory<GameObject, TableViewCell>
    {
        private readonly DiContainer _container;

        public TableViewCellFactory(DiContainer container)
        {
            _container = container;
        }

        public TableViewCell Create(GameObject prefab)
        {
            GameObject tableViewCellObj = _container.InstantiatePrefab(prefab);
            TableViewCell tableViewCell = tableViewCellObj.GetComponent<TableViewCell>();

            return tableViewCell;
        }
    }
}
