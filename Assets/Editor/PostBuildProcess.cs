﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using System.IO;
using System.Linq;

public class PostBuildProcess
{
	internal static void CopyAndReplaceDirectory (string srcPath, string dstPath)
	{
		if (Directory.Exists (dstPath))
			Directory.Delete (dstPath);
		if (File.Exists (dstPath))
			File.Delete (dstPath);
		
		Directory.CreateDirectory (dstPath);
		
		foreach (var file in Directory.GetFiles(srcPath))
			File.Copy (file, Path.Combine (dstPath, Path.GetFileName (file)));
		
		foreach (var dir in Directory.GetDirectories(srcPath))
			CopyAndReplaceDirectory (dir, Path.Combine (dstPath, Path.GetFileName (dir)));
	}
	
	[PostProcessBuild]
	public static void OnPostProcessBuild (BuildTarget buildTarget, string path)
	{
//		if (buildTarget == BuildTarget.iOS) {
#if UNITY_IOS
			ProcessForiOS (path);
#endif
//		}
	}

#if UNITY_IOS
	private static void ProcessForiOS (string path)
	{
		string pjPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

		PBXProject pj = new PBXProject ();
		pj.ReadFromString (File.ReadAllText (pjPath));
		
		string target = pj.TargetGuidByName ("Unity-iPhone");

        // Add custom system frameworks. Duplicate frameworks are ignored.
        // needed by our native plugin in Assets/Plugins/iOS
        pj.AddFrameworkToProject(target, "AddressBook.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "AssetsLibrary.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "CoreData.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "CoreLocation.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "CoreMotion.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "CoreTelephony.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "CoreText.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "Foundation.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "QuartzCore.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "SafariServices.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "Security.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "StoreKit.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "SystemConfiguration.framework", false /*not weak*/);
        pj.AddFrameworkToProject(target, "StoreKit.framework", false /*not weak*/);

		pj.AddFrameworkToProject(target, "EventKit.framework", false /*not weak*/);
		pj.AddFrameworkToProject(target, "EventKitUI.framework", false /*not weak*/);
		pj.AddFrameworkToProject(target, "MessageUI.framework", false /*not weak*/);

        AddUsrLib(pj, target, "libc++.tbd");
        AddUsrLib(pj, target, "libz.tbd");

        // Set a custom link flag
        pj.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC");
		
		pj.SetBuildProperty (target, "CLANG_ENABLE_MODULES", "YES");
		
		File.WriteAllText (pjPath, pj.WriteToString ());
    }

    private static void AddUsrLib(PBXProject pj, string targetGuid, string framework)
    {
        string fileGuid = pj.AddFile("usr/lib/" + framework, "Frameworks/" + framework, PBXSourceTree.Sdk);
        pj.AddFileToBuild(targetGuid, fileGuid);
    }
#endif
}