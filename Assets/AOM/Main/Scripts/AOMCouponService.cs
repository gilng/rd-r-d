﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.MiniJSON;
using System;

namespace Ignite.AOM
{
	public class AOMCouponService {

		public class RedeemCouponResult{
			public bool success;
			public string error;
			public int relics;
			public int gems;
			public bool unlockAdFree;

//			public string msg;
		}

		public delegate void RedeemCouponCallBack(RedeemCouponResult result);
		private RedeemCouponCallBack redeemCouponCallBackFunction;

		private static AOMCouponService instance;

		public static AOMCouponService Instance {
			get {
				if(instance == null) {
					instance = new AOMCouponService();
				}
				return instance;
			}
		}

		private AOMCouponService ()
		{
		}

		public IEnumerator RedemCoupon(string couponCode, RedeemCouponCallBack callBack)
		{
			redeemCouponCallBackFunction = callBack;

			WWWForm form = new WWWForm();
			form.AddField( "coupon_code", couponCode );
			form.AddField( "redeemed_by", PlayerPrefs.GetString ("PLAYER_NAME", "NO NAME") );

//			string url = "http://localhost:3000/api/redeemCoupon";
			string url = "https://aom-coupon.herokuapp.com/api/redeemCoupon";

			WWW www = new WWW(url, form.data);

			yield return www;

			RedeemCouponResult result = new RedeemCouponResult();

			if (www.error == null)
			{
				var dict = Json.Deserialize (www.text) as Dictionary<string, object>;
				
				if(dict != null)
				{
					result.success = true;
					result.relics = int.Parse(dict["relics"].ToString());
					result.gems = int.Parse(dict["gems"].ToString());
					result.unlockAdFree = Convert.ToBoolean(dict["unlockAdFree"]);

//					result.msg = dict["msg"].ToString();
				}

			} else {

				if (www.text == null || www.text == "") {
					result = null;
				} else {
					var dict = Json.Deserialize (www.text) as Dictionary<string, object>;
					
					if(dict != null)
					{
						result.success = false;
						Dictionary<string, object> errorObject = (Dictionary<string, object>)dict["error"];
						result.error = errorObject["message"].ToString();
					}
				}
			}
			
			redeemCouponCallBackFunction(result);
		}
	}
}
