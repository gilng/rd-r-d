﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Ignite.AOM
{
	public class TapDamageText : MonoBehaviour {
		
		public Text textObject;
		private float startY;

		float yTimer, yTime = 0.15f;
		float aTimer, aTime = 0.15f;

		// Use this for initialization
		void Start () {

			yTimer = yTime;
			aTimer = aTime;
		}

		void Update() {

//			if (yTimer > 0)
//			{
//				if (yTimer == yTime) {
//					textObject.gameObject.transform.SetSiblingIndex(0);
//				}
//
//				yTimer -= Time.deltaTime;
//			}
//			else
//			{
//				if (aTimer > 0) {
//					aTimer -= Time.deltaTime;
//				} else {
//					Color color = textObject.color;
//					color.a -= Time.deltaTime * 1.8f;
//					textObject.color = color;
//
//					Color outlineColor = textObject.GetComponent<Outline>().effectColor;
//					outlineColor.a -= Time.deltaTime * 1.8f;
//					textObject.GetComponent<Outline>().effectColor = outlineColor;
//				}
//
//				textObject.gameObject.transform.SetSiblingIndex(0);
//
//				Vector3 updatePoint = textObject.transform.position;
//				updatePoint.y += 0.05f;
//				textObject.transform.position = updatePoint;
//				
//				if(textObject.transform.localPosition.y >= startY + 300) {
//					gameObject.SetActive(false);
//					yTimer = yTime;
//					aTimer = aTime;
//				}
//			}
		}

		public void Spawn() {
			Color color = textObject.color;
			color.a = 1;
			textObject.color = color;

			Color outlineColor = new Color(1, 1, 1, 155/255f);
			textObject.GetComponent<Outline>().effectColor = outlineColor;

			textObject.transform.localScale = new Vector3(1, 1, 1);
			textObject.transform.localPosition = new Vector2(UnityEngine.Random.Range(-16, 15), 300);

			startY = textObject.transform.localPosition.y;
		}

		public void SpawnForMoneyText(Vector2 pos) {
			Color color = textObject.color;
			color.a = 1;
			textObject.color = color;

			Color outlineColor = new Color(0, 0, 0, 155/255f);
			textObject.GetComponent<Outline>().effectColor = outlineColor;

			textObject.transform.localScale = new Vector3(1, 1, 1);
			textObject.transform.position = pos;

			startY = textObject.transform.localPosition.y;
		}
	}
}
