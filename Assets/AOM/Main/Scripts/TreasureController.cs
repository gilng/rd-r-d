﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Zenject;

namespace Ignite.AOM
{
    public class TreasureController : MonoBehaviour
    {
        private UIViewLoader _uiViewLoader;
        private EnemyCore _enemyCore;
        private GalleryCore _galleryCore;
        private IUserService _userService;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader, EnemyCore enemyCore, GalleryCore galleryCore,
            IUserService userService)
        {
            _uiViewLoader = uiViewLoader;
            _enemyCore = enemyCore;
            _galleryCore = galleryCore;
            _userService = userService;
        }

        public GameObject fairy, box, box_display, showADMenu;
        public bool treasureTrigger = false;

        public Sprite spriteBoxClose;
        public Sprite spriteBoxOpen;

        public bool fairyTrigger = false;
        bool isboxDrop = false;

        private bool boxShouldDisappear = false;

        float boxSpeed;

        Vector3 boxDropPos;
        float boxGoalPosY;

        void Start()
        {
            fairy.SetActive(false);
            box.SetActive(false);

            EventManager.OnHiddenMenuShow += ChangeFairyPosition;
            EventManager.OnIgnoredReward += ClearCollectReward;
        }

        void OnDestroy()
        {
            EventManager.OnCollectReward -= OnCollectReward;
            EventManager.OnHiddenMenuShow -= ChangeFairyPosition;
            EventManager.OnIgnoredReward -= ClearCollectReward;
        }

        void ChangeFairyPosition(bool isShow)
        {
            if (isShow)
            {
                this.gameObject.transform.localPosition = new Vector3(0, -Background.instance.mountainAMove, 0);
                boxGoalPosY = Background.instance.mountainAMove;
            }
            else
            {
                this.gameObject.transform.localPosition = new Vector3(0, 0, 0);
                boxGoalPosY = 0;
            }
        }

        public void FairyStart()
        {
            box.transform.position = new Vector3(0, 12, 0);

            isboxDrop = false;

            fairy.SetActive(true);
            fairy.GetComponent<Animator>().SetInteger("AnimeSelect", 1);
        }

        void Update()
        {
            if (boxShouldDisappear && box)
            {
                Color boxColor = box.GetComponent<SpriteRenderer>().color;
                boxColor.a -= Time.deltaTime * 0.8f;
                box.GetComponent<SpriteRenderer>().color = boxColor;

                if (boxColor.a <= 0)
                {
                    boxShouldDisappear = false;
                    box.GetComponent<SpriteRenderer>().sprite = spriteBoxClose;
                    box.SetActive(false);
                    box.transform.position = new Vector3(0, 12, 0);
                }
            }

            if (isboxDrop)
            {
                BoxDropping();
            }
        }

        // Fairy use only
        public void DropTreasureBox()
        {
            if (fairyTrigger == false)
            {
                fairyTrigger = true;

                SoundManager.Instance.PlaySoundWithType(SoundType.TreasureCollect_1);

                GamePlayStatManager.Instance.FairyPresent();

                box_display.SetActive(false);

                fairy.GetComponent<Animator>().CrossFade("FairyTrig", 0.3f);

                box.SetActive(true);
                isboxDrop = true;
                boxSpeed = 0;

                Color boxColor = box.GetComponent<SpriteRenderer>().color;
                boxColor.a = 1;
                box.GetComponent<SpriteRenderer>().color = boxColor;
                box.transform.position = new Vector3(fairy.transform.position.x, fairy.transform.position.y - 0.5f,
                    fairy.transform.position.z);
            }
        }

        // Fairy use only
        public void FairyLeave()
        {
            fairy.GetComponent<Animator>().SetInteger("AnimeSelect", 0);

            fairy.SetActive(false);
            box_display.SetActive(true);

            fairyTrigger = false;
//			isboxDrop = false;

            EventManager.FairyLeave(false);
        }

        // Box use only
        void BoxDropping()
        {
            if (box.transform.localPosition.y > 0.6f + boxGoalPosY)
            {
                boxSpeed += Time.deltaTime * -1 * 0.125f;
                box.transform.Translate(0, boxSpeed, 0);
            }
            else
            {
                isboxDrop = false;

                EventManager.FairyLeave(false);

                BoxDrop();
            }
        }

        // Box use only (Run once)
        void BoxDrop()
        {
            box.GetComponent<SpriteRenderer>().sprite = spriteBoxOpen;

            boxShouldDisappear = true;

            boxDropPos = box.transform.position;

//            int randomMax = ArtifactAssets.MoeSpirit026.GetBalance() > 0 ? (_userService.CurrentUserSummary != null ? 6 : 5) : 5;

            int randomMax = ArtifactAssets.MoeSpirit026.GetBalance() > 0 ? (_userService.CurrentUserSummary != null ? 5 : 4) : 4;

#if UNITY_IOS || UNITY_ANDROID
            if (!ADManager.Instance.CheckRewardedAdReady())
            {
                randomMax = 1;
            }
#endif

            randomMax = (_enemyCore.CurrentEnemyType == EnemyType.BigBoss ||
                         _enemyCore.CurrentEnemyType == EnemyType.MiniBoss)
                ? 1
                : randomMax;

            switch (UnityEngine.Random.Range(0, randomMax))
            {
                case 0: // Normal : Monster Gold * 10
                    GameManager.Instance.goldPool.GetComponent<GoldPool>()
                        .GoldDropFromFairy(
                            Formulas.EnemyGoldDropForLevel(_enemyCore.Stage, EnemyType.Normal) * 10 *
                            (1 + ArtifactAssets.MoeSpirit005.EffectValue), boxDropPos.x, boxDropPos.y);
                    break;

//                case 1: // Normal : Diamond +1
//                    GameManager.Instance.diamondPool.GetComponent<DiamondPool>()
//                        .DiamondDropFromFairy((int) (1 * (1 + ArtifactAssets.MoeSpirit017.EffectValue)), boxDropPos.x,
//                            -3.0f);
//                    break;

                case 1: // AD Video : Monster Gold * 50
                    ShowVideoADMenu(VideoAdRewardType.UltimateGold);
                    EventManager.OnCollectReward += OnCollectReward;
                    break;

                case 2: // AD Video : Diamond +5
                    ShowVideoADMenu(VideoAdRewardType.UltimateDiamond);
                    EventManager.OnCollectReward += OnCollectReward;
                    break;

                case 3: // AD Video : Rub quota +1
                    ShowVideoADMenu(VideoAdRewardType.UltimateRubQuota);
                    EventManager.OnCollectReward += OnCollectReward;
                    break;

                case 4: // AD Video : Ruby +30
                    ShowVideoADMenu(VideoAdRewardType.UltimateRuby);
                    EventManager.OnCollectReward += OnCollectReward;
                    break;
            }
        }

        void OnCollectReward(VideoAdRewardType type, bool doubleReward)
        {
            double treasureReward = 0;

            if (type == VideoAdRewardType.UltimateGold)
            {
                treasureReward = Formulas.EnemyGoldDropForLevel(_enemyCore.Stage, EnemyType.Normal) * 50 *
                                 (1 + ArtifactAssets.MoeSpirit005.EffectValue) * (doubleReward ? 2 : 1);
                GameManager.Instance.goldPool.GetComponent<GoldPool>()
                    .GoldDropFromFairy(treasureReward, boxDropPos.x, boxDropPos.y);
            }
            else if (type == VideoAdRewardType.UltimateDiamond)
            {
                treasureReward = 5 * (1 + ArtifactAssets.MoeSpirit017.EffectValue);
                GameManager.Instance.diamondPool.GetComponent<DiamondPool>()
                    .DiamondDropFromFairy((int) treasureReward, boxDropPos.x, -3.0f);
            }
            else if (type == VideoAdRewardType.UltimateRubQuota)
            {
                treasureReward = (2 + (int)(ArtifactAssets.MoeSpirit008.EffectValue * 100)) * (doubleReward ? 2 : 1);

                _galleryCore.RechargingTouchCredit((int) treasureReward, true);
                GameManager.Instance.UpdateTouchCredit();

                bool isMaxCP = (PlayerPrefs.GetInt("PP_GALLERY_GIRL_TOUCH_CREDIT", 5) >=
                                PlayerPrefs.GetInt("PP_GALLERY_GIRL_TOUCH_CREDIT_MAX", 5));

                GameManager.Instance.rechargeAnime.transform.FindChild("Point/Text")
                    .gameObject.GetComponent<Text>()
                    .text = isMaxCP ? "Max" : "+" + (int) treasureReward;
                GameManager.Instance.rechargeAnime.SetActive(true);
            }
            else if (type == VideoAdRewardType.UltimateRuby)
            {
                treasureReward = (ArtifactAssets.MoeSpirit026.EffectValue * 100) * (doubleReward ? 2 : 1);

                UserManager.Instance.GiveRuby((int) treasureReward);
            }

            ClearCollectReward();
        }

        public void ClearCollectReward()
        {
            EventManager.OnCollectReward -= OnCollectReward;
        }

        public void ShowVideoADMenu(VideoAdRewardType videoAdRewardType)
        {
            GameObject fairyAdView = _uiViewLoader.LoadOverlayView("FairyADMenuView", null, false, false);
            fairyAdView.GetComponent<FairyAdViewController>().SetFairyRewardType(videoAdRewardType);
        }
    }
}