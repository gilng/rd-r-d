﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using UnityEngine.UI;
using Zenject;

namespace Ignite.AOM
{
    //Inherit from TableViewCell instead of MonoBehavior to use the GameObject
    //containing this component as a cell in a TableView
    public class ArtifactTableCell : TableViewCell
    {
        private UIViewLoader _uiViewLoader;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader)
        {
            _uiViewLoader = uiViewLoader;
        }

        public GameObject buyButton, showAllButton;
        public GameObject levelUpButton, levelUpButton_block;
        public Text levelUpPrice;
        public Text afName;
        public Text level;
        public Text effect;
        public Text damageEffect;

        public Image icon;

        public GameObject rareLevelStar1;
        public GameObject rareLevelStar2;
        public GameObject rareLevelStar3;

        private int row;
        private ArtifactVG item;

        public ArtifactVG Item
        {
            get { return item; }
            set { item = value; }
        }

        public void SetRowNumber(int rowNumber)
        {
            row = rowNumber;
        }

        public void NotifyBecameVisible()
        {
        }

        public void ShowGachaOptionOverlay()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            GameObject gachaMenuView = _uiViewLoader.LoadOverlayView("GachaMenuView", null, false, false);
        }

        public void LevelUpItem()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
//            if (item.CanAffordOne())
//            {
                item.BuyOne();
//            }
        }

        public void ShowAllArtifact()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            GameObject moetifactListView = _uiViewLoader.LoadOverlayView("MoetifactTableMenuView", null, false, true);
        }

        public void ShowArtifactDetail()
        {
            if (item != null)
            {
                GameObject mfDetailView = _uiViewLoader.LoadOverlayView("MoetifactAndMoeSpiritDetailView", null, false, Time.timeScale == 0);
                mfDetailView.GetComponent<MoetifactAndMoeSpiritDetailViewController>().SetDisplayItem(item);
            }
        }
    }
}