using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;
using UnityEngine.UI;
using System;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    //Inherit from TableViewCell instead of MonoBehavior to use the GameObject
    //containing this component as a cell in a TableView
    public class HeroTableCell : TableViewCell
    {
        private UIViewLoader _uiViewLoader;

        private HeroCore _heroCore;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader, HeroCore heroCore)
        {
            _uiViewLoader = uiViewLoader;

            _heroCore = heroCore;
        }

        public HeroTableViewController parentTableController;
        public GameObject icon;

        public string heroName;
        public Text nameText;

        public int level = -1;
        public Text levelText;

        public double dps = -1;
        public Text dpsText;

        public double priceForOne = -1;
        public Text priceForOneText;
        public double priceForTen = -1;
        public Text priceForTenText;
        public double priceForHun = -1;
        public Text priceForHunText;

        public GameObject levelUpBG;
        public Button levelUpButton;
        public GameObject levelUpButton_Block;
        public GameObject newLabel;

        public double nextDPS = -1;
        public Text nextLevelDPS;

        public Button levelUpTenButton;
        public Button levelUpHunButton;
        public Button unlockSkillButton;

        public double priceForSkill = -1;
        public Text priceForUnlockSkill;

        public Text unlockSkillButtonLabel;

        public string keyForskillName;
        public Text unlockSkillName;

        public List<Image> skillImage;
        public List<Text> skillLevelText;

        //Revive
        public GameObject graveIcon;

        public Text revivesLabel;
        public DateTime reviveTime;
        public Text reviveTimeLabel;
        public GameObject deadOverlay;
        public GameObject reviveBtn;
        public Text reviveBtnLabel;
        public double priceForRevive = -1;
        public Text revivePrice;

        public GameObject levelUpEffect;

        public TutorialReceiver tutorialReceiver;

        private int row;
        private int unlockSkillIdx;
        private double levelUpButtonDownTime = 0;
        private double showTenAndHundredButtonTime = 0;
        private double lastBuyOneTime = 0;
        private float updateTimer = 0;

        private bool opendHundredPanel = false;

        public Sprite[] currHeroIcon;
        public string[] currHeroIconName;

        void Awake()
        {
            EventManager.OnLanguageChange += UpdateCellLanguage;
        }

        void Start()
        {
            EnableBuyTenAndHundredButton(false);
        }

        void OnDestroy()
        {
            EventManager.OnLanguageChange -= UpdateCellLanguage;
        }

        void Update()
        {
            if (levelUpButtonDownTime != 0)
            {
                double holding = UnitConverter.ConvertToTimestamp(System.DateTime.Now) - levelUpButtonDownTime;
                if (holding >= 0.5)
                {
                    if (HeroAssets.Heroes[row].CanAffordOne())
                    {
                        EnableBuyTenAndHundredButton(true);
                    }

                    showTenAndHundredButtonTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                    levelUpButtonDownTime = 0;
                }
            }

            if (showTenAndHundredButtonTime != 0)
            {
                double showing = UnitConverter.ConvertToTimestamp(System.DateTime.Now) - showTenAndHundredButtonTime;
                if (showing >= 2.6)
                {
                    EnableBuyTenAndHundredButton(false);

                    showTenAndHundredButtonTime = 0;
                }
                else
                {
                    EnableBuyTenAndHundredButton(true);
                }
            }

            if (!_heroCore.HeroAlive[row])
            {
                updateTimer += Time.deltaTime;
                if (updateTimer >= 1)
                {
                    TimeSpan waitingTime = TimeSpan.FromSeconds((reviveTime - DateTime.Now).TotalSeconds);
                    reviveTimeLabel.text = string.Format("{0:D2}:{1:D2}:{2:D2}", waitingTime.Hours, waitingTime.Minutes,
                        waitingTime.Seconds);

                    double currPriceForRevive = Math.Ceiling(waitingTime.TotalHours) * 8;
                    if (priceForRevive != currPriceForRevive)
                    {
                        priceForRevive = currPriceForRevive;
                        revivePrice.text = priceForRevive.ToString();
                        UpdateReviveButtonState();
                    }
                    updateTimer = 0;
                }
            }
        }

        public bool isHeroDead = false;

        public void OnHeroDead(DateTime reviveTime)
        {
            if (!isHeroDead)
            {
                //Debug.Log ("HeroDead "+name);
                this.reviveTime = reviveTime;
                graveIcon.SetActive(true);
                deadOverlay.SetActive(true);
                reviveBtn.SetActive(true);
                reviveTimeLabel.gameObject.SetActive(true);
                revivesLabel.gameObject.SetActive(true);
                revivesLabel.text = LanguageManager.Instance.GetTextValue("revives_in");
                revivePrice.gameObject.SetActive(true);

                if (unlockSkillButton.gameObject.activeSelf)
                {
                    unlockSkillButton.gameObject.SetActive(false);
                }

                for (int i = 0; i < skillImage.Count; i++)
                {
                    if (skillImage[i].gameObject.activeSelf)
                        skillImage[i].gameObject.SetActive(false);
                }

                isHeroDead = true;
                isHeroRevive = false;
            }
        }

        public void UpdateReviveButtonState()
        {
            if (isHeroDead)
            {
                if (UserManager.Instance.CurrentUserSummary != null && UserManager.Instance.CurrentUserSummary.Gem >= (int) priceForRevive)
                {
                    if (reviveBtn.GetComponent<Image>().sprite !=
                        reviveBtn.GetComponent<Button>().spriteState.highlightedSprite)
                        reviveBtn.GetComponent<Image>().sprite =
                            reviveBtn.GetComponent<Button>().spriteState.highlightedSprite;
                }
                else
                {
                    if (reviveBtn.GetComponent<Image>().sprite !=
                        reviveBtn.GetComponent<Button>().spriteState.pressedSprite)
                        reviveBtn.GetComponent<Image>().sprite =
                            reviveBtn.GetComponent<Button>().spriteState.pressedSprite;
                }
            }
        }

        public bool isHeroRevive = false;

        public void OnHeroRevive()
        {
            if (!isHeroRevive)
            {
                graveIcon.SetActive(false);
                deadOverlay.SetActive(false);
                reviveBtn.SetActive(false);
                reviveTimeLabel.gameObject.SetActive(false);
                revivesLabel.gameObject.SetActive(false);
                revivePrice.gameObject.SetActive(false);

                if (levelUpButton.gameObject.activeSelf)
                {
                    if (levelUpButton.interactable != HeroAssets.Heroes[row].CanAffordOne())
                        levelUpButton.interactable = HeroAssets.Heroes[row].CanAffordOne();

                    if (HeroAssets.Heroes[row].CanAffordOne())
                    {
                        if (levelUpButton.GetComponent<Image>().sprite != levelUpButton.GetComponent<Button>()
                                .spriteState.highlightedSprite)
                            levelUpButton.GetComponent<Image>().sprite = levelUpButton.GetComponent<Button>()
                                .spriteState.highlightedSprite;
                    }
                    else
                    {
                        if (levelUpButton.GetComponent<Image>().sprite !=
                            levelUpButton.GetComponent<Button>().spriteState.pressedSprite)
                            levelUpButton.GetComponent<Image>().sprite = levelUpButton.GetComponent<Button>()
                                .spriteState.pressedSprite;
                    }
                }

                isHeroDead = false;
                isHeroRevive = true;
            }
        }

        public void ReviveHero()
        {
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                if (UserManager.Instance.CurrentUserSummary.Gem >= (int) priceForRevive)
//                {
                    _heroCore.ReviveHero(row, int.Parse(revivePrice.text));
//                }
//                else
//                {
//                    OpenShopMenu();
//                }
//            }
        }

        public void OpenShopMenu()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            _uiViewLoader.LoadOverlayView("ShopMenu", null, false, false);
        }

        public void EnableBuyTenAndHundredButton(bool isEnable)
        {
            bool canAffordOne = HeroAssets.Heroes[row].CanAffordOne();
            levelUpBG.SetActive(isEnable);

            if (levelUpBG.activeSelf)
            {
                bool canAffordTen = HeroAssets.Heroes[row].CanAffordTen();
                levelUpTenButton.gameObject.SetActive(true);
                levelUpTenButton.GetComponent<Image>().sprite = canAffordTen
                    ? levelUpTenButton.GetComponent<Button>().spriteState.highlightedSprite
                    : levelUpTenButton.GetComponent<Button>().spriteState.pressedSprite;
                levelUpTenButton.interactable = canAffordOne ? true : false;

                bool canAffordHundred = HeroAssets.Heroes[row].CanAffordHundred();
                levelUpHunButton.gameObject.SetActive(true);
                levelUpHunButton.GetComponent<Image>().sprite = canAffordHundred
                    ? levelUpHunButton.GetComponent<Button>().spriteState.highlightedSprite
                    : levelUpHunButton.GetComponent<Button>().spriteState.pressedSprite;
                levelUpHunButton.interactable = canAffordOne ? true : false;


                // Check arrive MaxLevel
                if (level <= HeroAssets.Heroes[row].MaxLevel - 100)
                {
                    levelUpBG.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(489, 129);
                }
                else if (level <= HeroAssets.Heroes[row].MaxLevel - 10)
                {
                    levelUpHunButton.gameObject.SetActive(false);
                    levelUpBG.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(254, 129);
                }
                else
                {
                    levelUpTenButton.gameObject.SetActive(false);
                    levelUpHunButton.gameObject.SetActive(false);
                    levelUpBG.SetActive(false);
                }
            }
            else
            {
                levelUpTenButton.gameObject.SetActive(false);
                levelUpHunButton.gameObject.SetActive(false);
            }
        }

        public void SetRowNumber(int rowNumber)
        {
            //			m_rowNumberText.text = "Row " + rowNumber.ToString();
            row = rowNumber;
        }

        public void SetUnlockSkillIdx(int idx)
        {
            unlockSkillIdx = idx;
        }

        //		private int m_numTimesBecameVisible;
        public void NotifyBecameVisible()
        {
            //			m_numTimesBecameVisible++;
            //			m_visibleCountText.text = "# rows this cell showed : " + m_numTimesBecameVisible.ToString();
        }

        public void levelUpButtonDown()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            levelUpButtonDownTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
        }

        public void levelUpButtonUp()
        {
            if (HeroAssets.Heroes[row].CanAffordOne())
            {
                if (showTenAndHundredButtonTime != 0)
                    showTenAndHundredButtonTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);

                if (UnitConverter.ConvertToTimestamp(System.DateTime.Now) - lastBuyOneTime <= 0.5)
                {
                    if (HeroAssets.Heroes[row].CanAffordOne())
                    {
                        EnableBuyTenAndHundredButton(true);
                    }

                    showTenAndHundredButtonTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                }

                if (UnitConverter.ConvertToTimestamp(System.DateTime.Now) - levelUpButtonDownTime <= 0.3)
                {
                    lastBuyOneTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                    levelUpButtonDownTime = 0;

                    HeroAssets.Heroes[row].BuyOne();

                    if (!levelUpEffect.activeSelf)
                        levelUpEffect.SetActive(true);

                    levelUpEffect.GetComponent<ParticleSystem>().Play();

                    UpdateHeroSkill();
                }

                if (tutorialReceiver != null)
                {
                    if (tutorialReceiver.isTriggered)
                    {
                        TutorialManager.Instance.UpdateTutorialProgress(tutorialReceiver.tutorialId);

                        if (GamePlayStatManager.Instance.Stat[StatType.InviteFriends] == 0 &&
                            PlayerPrefs.GetString("TryToInviteFriends", "No") != "Yes")
                            GameManager.Instance.friendsButton.transform.FindChild("TutorialNotify_Symbol")
                                .gameObject.SetActive(true);
                    }
                }
            }

            levelUpHunButton.GetComponent<Image>().sprite = (HeroAssets.Heroes[row].CanAffordHundred())
                ? levelUpHunButton.GetComponent<Button>().spriteState.highlightedSprite
                : levelUpHunButton.GetComponent<Button>().spriteState.pressedSprite;
            levelUpTenButton.GetComponent<Image>().sprite = (HeroAssets.Heroes[row].CanAffordTen())
                ? levelUpTenButton.GetComponent<Button>().spriteState.highlightedSprite
                : levelUpTenButton.GetComponent<Button>().spriteState.pressedSprite;
            levelUpButton.GetComponent<Image>().sprite = (HeroAssets.Heroes[row].CanAffordOne())
                ? levelUpButton.GetComponent<Button>().spriteState.highlightedSprite
                : levelUpButton.GetComponent<Button>().spriteState.pressedSprite;

            levelUpHunButton.interactable = (HeroAssets.Heroes[row].CanAffordOne()) ? true : false;
            levelUpTenButton.interactable = (HeroAssets.Heroes[row].CanAffordOne()) ? true : false;
            levelUpButton.interactable = (HeroAssets.Heroes[row].CanAffordOne()) ? true : false;

            if (level >= HeroAssets.Heroes[row].MaxLevel)
            {
                if (levelUpButton.gameObject.activeInHierarchy)
                {
                    levelUpButton.gameObject.SetActive(false);
                    levelUpButton_Block.gameObject.SetActive(false);

                    EnableBuyTenAndHundredButton(false);
                }
            }
            else
            {
                if (!levelUpButton.gameObject.activeInHierarchy)
                {
                    levelUpButton.gameObject.SetActive(true);
                    levelUpButton_Block.gameObject.SetActive(true);
                }
            }
        }

        public void levelUpButtonExit()
        {
            levelUpButtonDownTime = 0;
        }

        public void LevelUpTenButtonPressed()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            if (showTenAndHundredButtonTime != 0)
                showTenAndHundredButtonTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);

            //Debug.Log("Level up 10 hero: "+HeroAssets.Heroes[row].ItemId);
            HeroAssets.Heroes[row].BuyTen();

            levelUpHunButton.GetComponent<Image>().sprite = (HeroAssets.Heroes[row].CanAffordHundred())
                ? levelUpHunButton.GetComponent<Button>().spriteState.highlightedSprite
                : levelUpHunButton.GetComponent<Button>().spriteState.pressedSprite;
            levelUpTenButton.GetComponent<Image>().sprite = (HeroAssets.Heroes[row].CanAffordTen())
                ? levelUpTenButton.GetComponent<Button>().spriteState.highlightedSprite
                : levelUpTenButton.GetComponent<Button>().spriteState.pressedSprite;
            levelUpButton.GetComponent<Image>().sprite = (HeroAssets.Heroes[row].CanAffordOne())
                ? levelUpButton.GetComponent<Button>().spriteState.highlightedSprite
                : levelUpButton.GetComponent<Button>().spriteState.pressedSprite;

            levelUpHunButton.interactable = (HeroAssets.Heroes[row].CanAffordOne()) ? true : false;
            levelUpTenButton.interactable = (HeroAssets.Heroes[row].CanAffordOne()) ? true : false;
            levelUpButton.interactable = (HeroAssets.Heroes[row].CanAffordOne()) ? true : false;

            if (!levelUpEffect.activeSelf)
                levelUpEffect.SetActive(true);

            levelUpEffect.GetComponent<ParticleSystem>().Play();

            UpdateHeroSkill();

            CheckLevelUpBtnStatus();
        }

        public void LevelUpHunButtonPressed()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            if (showTenAndHundredButtonTime != 0)
                showTenAndHundredButtonTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);

            //			Debug.Log("Level up 100 hero: "+HeroAssets.Heroes[row].ItemId);
            HeroAssets.Heroes[row].BuyHundred();

            levelUpHunButton.GetComponent<Image>().sprite = (HeroAssets.Heroes[row].CanAffordHundred())
                ? levelUpHunButton.GetComponent<Button>().spriteState.highlightedSprite
                : levelUpHunButton.GetComponent<Button>().spriteState.pressedSprite;
            levelUpTenButton.GetComponent<Image>().sprite = (HeroAssets.Heroes[row].CanAffordTen())
                ? levelUpTenButton.GetComponent<Button>().spriteState.highlightedSprite
                : levelUpTenButton.GetComponent<Button>().spriteState.pressedSprite;
            levelUpButton.GetComponent<Image>().sprite = (HeroAssets.Heroes[row].CanAffordOne())
                ? levelUpButton.GetComponent<Button>().spriteState.highlightedSprite
                : levelUpButton.GetComponent<Button>().spriteState.pressedSprite;

            levelUpHunButton.interactable = (HeroAssets.Heroes[row].CanAffordOne()) ? true : false;
            levelUpTenButton.interactable = (HeroAssets.Heroes[row].CanAffordOne()) ? true : false;
            levelUpButton.interactable = (HeroAssets.Heroes[row].CanAffordOne()) ? true : false;

            if (!levelUpEffect.activeSelf)
                levelUpEffect.SetActive(true);

            levelUpEffect.GetComponent<ParticleSystem>().Play();

            UpdateHeroSkill();

            CheckLevelUpBtnStatus();
        }

        void CheckLevelUpBtnStatus()
        {
            if (level <= HeroAssets.Heroes[row].MaxLevel - 100)
            {
                levelUpBG.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(489, 129);
            }
            else if (level <= HeroAssets.Heroes[row].MaxLevel - 10)
            {
                levelUpHunButton.gameObject.SetActive(false);
                levelUpBG.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(254, 129);
            }
            else
            {
                levelUpTenButton.gameObject.SetActive(false);
                levelUpHunButton.gameObject.SetActive(false);
                levelUpBG.SetActive(false);
            }
        }

        public void UnlockSkillButtonPressed()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[row].ItemId][unlockSkillIdx].CanAffordOne())
            {
                HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[row].ItemId][unlockSkillIdx].BuyOne();

                //				string skill_path = "Sprites/icon-hero-skill/icon-hero-skill-40X40/"+ HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes [row].ItemId][unlockSkillIdx].EffectType.ToString();
                //
                //				skillImage[unlockSkillIdx].sprite = Resources.Load<Sprite>( skill_path );
                //				skillImage[unlockSkillIdx].gameObject.SetActive(true);
                //				skillLevelText[unlockSkillIdx].gameObject.SetActive(false);
                //
                //				// Show next lv icon
                //				if (unlockSkillIdx + 1 < 8) {
                //					skillImage[ unlockSkillIdx + 1 ].gameObject.SetActive(true);
                //
                //					skillLevelText[ unlockSkillIdx + 1 ].gameObject.SetActive(true);
                //					skillLevelText[ unlockSkillIdx + 1 ].text = "LV\n"+ (( HeroAssets.Heroes[row].GetBalance () / 1000) + AOMConstant.UNLOCK_HERO_SKILL_LEVELS [ unlockSkillIdx + 1 ]);
                //				}

                UpdateHeroSkill();

                if (!levelUpEffect.activeSelf)
                    levelUpEffect.SetActive(true);

                levelUpEffect.GetComponent<ParticleSystem>().Play();
            }
        }

        private System.Text.StringBuilder sb = new System.Text.StringBuilder();

        public void UpdateHeroSkill()
        {
            if (level >= AOMConstant.UNLOCK_HERO_SKILL_LEVELS[0])
            {
                bool newSkillUnlock = false;

                //set unlock skill btn to active
                for (int i = 0; i < HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[row].ItemId].Count; i++)
                {
                    HeroPassiveSkillVG skill = HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[row].ItemId][i];

                    int targetBalance = (level >= skill.CharacterMinLevel) ? 1 : 0;
                    if (skill.GetBalance() < targetBalance)
                    {
                        newSkillUnlock = true;

                        if (unlockSkillButton.interactable != skill.CanAffordOne())
                            unlockSkillButton.interactable = skill.CanAffordOne();

                        if (skill.CanAffordOne())
                        {
                            if (unlockSkillButton.GetComponent<Image>().sprite != unlockSkillButton
                                    .GetComponent<Button>()
                                    .spriteState.highlightedSprite)
                                unlockSkillButton.GetComponent<Image>().sprite = unlockSkillButton
                                    .GetComponent<Button>()
                                    .spriteState.highlightedSprite;
                        }
                        else
                        {
                            if (unlockSkillButton.GetComponent<Image>().sprite != unlockSkillButton
                                    .GetComponent<Button>()
                                    .spriteState.pressedSprite)
                                unlockSkillButton.GetComponent<Image>().sprite = unlockSkillButton
                                    .GetComponent<Button>()
                                    .spriteState.pressedSprite;
                        }
                        SetUnlockSkillIdx(i);

                        if (priceForSkill != skill.PriceForOne)
                        {
                            priceForSkill = skill.PriceForOne;
                            priceForUnlockSkill.text = UnitConverter.ConverterDoubleToString(priceForSkill);
                        }

                        sb.Append(skill.ItemId.Remove(skill.ItemId.Length - 1, 1));
                        sb.Append(i);
                        sb.Append("_name");

                        if (keyForskillName != sb.ToString())
                        {
                            keyForskillName = sb.ToString();

                            unlockSkillName.text = LanguageManager.Instance.GetTextValue(keyForskillName);
                        }

                        sb.Clear();

                        break;
                    }
                    else if (skill.GetBalance() == targetBalance)
                    {
                        newSkillUnlock = false;
                    }

                    if (level >= AOMConstant.UNLOCK_HERO_SKILL_LEVELS[i])
                    {
                        if (skillImage[i].sprite.name != HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[row].ItemId][i]
                                .EffectType.ToString())
                        {
                            skillImage[i].sprite =
                                currHeroIcon[
                                    Array.IndexOf(currHeroIconName,
                                        HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[row].ItemId][i]
                                            .EffectType.ToString())];
                        }

                        if (!skillImage[i].gameObject.activeSelf)
                            skillImage[i].gameObject.SetActive(true);

                        if (skillLevelText[i].gameObject.activeSelf)
                            skillLevelText[i].gameObject.SetActive(false);

                        // Show next lv icon
                        if (i + 1 < 7)
                        {
                            HeroPassiveSkillVG nextSkill =
                                HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[row].ItemId][i + 1];

                            if (nextSkill.GetBalance() < 1)
                            {
                                if (!skillImage[i + 1].gameObject.activeSelf)
                                    skillImage[i + 1].gameObject.SetActive(true);

                                if (!skillLevelText[i + 1].gameObject.activeSelf)
                                    skillLevelText[i + 1].gameObject.SetActive(true);

                                if (skillLevelText[i + 1].text != "LV\n" + AOMConstant.UNLOCK_HERO_SKILL_LEVELS[i + 1])
                                    skillLevelText[i + 1].text = "LV\n" + AOMConstant.UNLOCK_HERO_SKILL_LEVELS[i + 1];
                            }
                        }
                    }
                }

                if (unlockSkillButton.gameObject.activeSelf != newSkillUnlock)
                {
                    unlockSkillButton.gameObject.SetActive(newSkillUnlock);

                    //if (!newSkillUnlock)
                    //{
                    //    if (!levelUpButton.gameObject.activeSelf)
                    //    {
                    //        levelUpButton.gameObject.SetActive(true);
                    //
                    //        if (levelUpButton.interactable != HeroAssets.Heroes[row].CanAffordOne())
                    //            levelUpButton.interactable = HeroAssets.Heroes[row].CanAffordOne();
                    //
                    //        if (HeroAssets.Heroes[row].CanAffordOne())
                    //        {
                    //            if (levelUpButton.GetComponent<Image>().sprite != levelUpButton.GetComponent<Button>().spriteState.highlightedSprite)
                    //                levelUpButton.GetComponent<Image>().sprite = levelUpButton.GetComponent<Button>().spriteState.highlightedSprite;
                    //        }
                    //        else
                    //        {
                    //            if (levelUpButton.GetComponent<Image>().sprite != levelUpButton.GetComponent<Button>().spriteState.pressedSprite)
                    //                levelUpButton.GetComponent<Image>().sprite = levelUpButton.GetComponent<Button>().spriteState.pressedSprite;
                    //        }
                    //    }
                    //}
                }
            }
            else if (level > 0)
            {
                if (!skillImage[0].gameObject.activeSelf)
                    skillImage[0].gameObject.SetActive(true);

                if (!skillLevelText[0].gameObject.activeSelf)
                {
                    skillLevelText[0].gameObject.SetActive(true);
                    skillLevelText[0].text = "LV\n10";
                }
            }
            else if (level == 0)
            {
                if (unlockSkillButton.gameObject.activeSelf)
                {
                    unlockSkillButton.gameObject.SetActive(false);
                }

                for (int i = 0; i < HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[row].ItemId].Count; i++)
                {
                    if (skillImage[i].gameObject.activeSelf)
                        skillImage[i].gameObject.SetActive(false);
                }

                if (!levelUpButton.gameObject.activeSelf)
                {
                    levelUpButton.gameObject.SetActive(true);

                    if (levelUpButton.interactable != HeroAssets.Heroes[row].CanAffordOne())
                        levelUpButton.interactable = HeroAssets.Heroes[row].CanAffordOne();

                    if (HeroAssets.Heroes[row].CanAffordOne())
                    {
                        if (levelUpButton.GetComponent<Image>().sprite != levelUpButton.GetComponent<Button>()
                                .spriteState.highlightedSprite)
                            levelUpButton.GetComponent<Image>().sprite = levelUpButton.GetComponent<Button>()
                                .spriteState.highlightedSprite;
                    }
                    else
                    {
                        if (levelUpButton.GetComponent<Image>().sprite !=
                            levelUpButton.GetComponent<Button>().spriteState.pressedSprite)
                            levelUpButton.GetComponent<Image>().sprite = levelUpButton.GetComponent<Button>()
                                .spriteState.pressedSprite;
                    }
                }
            }
        }

        public void UpdateSkillButtonStatus()
        {
            if (unlockSkillButton.gameObject.activeSelf)
            {
                HeroPassiveSkillVG skill = HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[row].ItemId][unlockSkillIdx];

                if (unlockSkillButton.interactable != skill.CanAffordOne())
                    unlockSkillButton.interactable = skill.CanAffordOne();

                if (skill.CanAffordOne())
                {
                    if (unlockSkillButton.GetComponent<Image>().sprite != unlockSkillButton.GetComponent<Button>()
                            .spriteState.highlightedSprite)
                        unlockSkillButton.GetComponent<Image>().sprite = unlockSkillButton.GetComponent<Button>()
                            .spriteState.highlightedSprite;
                }
                else
                {
                    if (unlockSkillButton.GetComponent<Image>().sprite != unlockSkillButton.GetComponent<Button>()
                            .spriteState.pressedSprite)
                        unlockSkillButton.GetComponent<Image>().sprite = unlockSkillButton.GetComponent<Button>()
                            .spriteState.pressedSprite;
                }
            }
        }

        public void UpdateCellLanguage()
        {
            sb.Append(HeroAssets.Heroes[row].ItemId);
            sb.Append("_name");

            heroName = LanguageManager.Instance.GetTextValue(sb.ToString());
            nameText.text = heroName;
            sb.Clear();

            unlockSkillName.text = LanguageManager.Instance.GetTextValue(keyForskillName);

            if (isHeroDead)
                revivesLabel.text = LanguageManager.Instance.GetTextValue("revives_in");
        }

        public void HeroTableCellOnClick()
        {
            HeroManager.instance.DisplayHeroSkillMenu(row);
        }

        public override void ClearCellData()
        {
            base.ClearCellData();

            parentTableController = null;

            currHeroIcon = null;
            currHeroIconName = null;

            icon.GetComponent<Image>().sprite = null;

            heroName = "";
            nameText.text = "";

            level = -1;
            levelText.text = "";

            dps = -1;
            dpsText.text = "";

            nextDPS = -1;
            nextLevelDPS.text = "";

            priceForOne = -1;
            priceForOneText.text = "";
            priceForTen = -1;
            priceForTenText.text = "";
            priceForHun = -1;
            priceForHunText.text = "";

            newLabel.SetActive(false);

            priceForSkill = -1;
            priceForUnlockSkill.text = "";

            levelUpButtonDownTime = 0;
            showTenAndHundredButtonTime = 0;
            lastBuyOneTime = 0;
            updateTimer = 0;

            opendHundredPanel = false;
        }
    }
}