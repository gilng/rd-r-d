using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    //An example implementation of a class that communicates with a TableView
    public class AvatarTableViewController : MonoBehaviour, ITableViewDataSource
    {
        private TableViewCell.Factory _tableViewCellFactory;
        private AvatarCore _avatarCore;

        [Inject]
        public void ConstructorSafeAttribute(TableViewCell.Factory tableViewCellFactory, AvatarCore avatarCore)
        {
            _tableViewCellFactory = tableViewCellFactory;
            _avatarCore = avatarCore;
        }

        public AvatarTableCell m_cellPrefab;
        public TableView m_tableView;
        public int m_numRows;
        private int m_numInstancesCreated = 0;
        //public GameObject prestigeMenu;
        private System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //public AvatarSkillMenuManager menu;
        public Sprite[] skillIcons;

        //Register as the TableView's delegate (required) and data source (optional)
        //to receive the calls
        void Awake()
        {
            m_tableView.dataSource = this;
        }

        #region ITableViewDataSource

        //Will be called by the TableView to know how many rows are in this table
        public int GetNumberOfRowsForTableView(TableView tableView)
        {
            return m_numRows;
        }

        //Will be called by the TableView to know what is the height of each row
        public float GetHeightForRowInTableView(TableView tableView, int row)
        {
            return (m_cellPrefab.transform as RectTransform).rect.height;
        }

        //Will be called by the TableView when a cell needs to be created for display
        public TableViewCell GetCellForRowInTableView(TableView tableView, int row)
        {
            AvatarTableCell cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as AvatarTableCell;
            if (cell == null)
            {
                //cell = (AvatarTableCell)GameObject.Instantiate(m_cellPrefab);
                cell = _tableViewCellFactory.Create(m_cellPrefab.gameObject) as AvatarTableCell;
                sb.Append("VisibleCounterCellInstance_");
                sb.Append((++m_numInstancesCreated).ToString());
                cell.name = sb.ToString();//"VisibleCounterCellInstance_" + (++m_numInstancesCreated).ToString();
                sb.Clear();
            }

            // Tutorial Event 01 Setup
            if (row == 0 && cell.tutorialReceiver == null)
            {
                cell.tutorialReceiver = cell.gameObject.AddComponent<TutorialReceiver>();
                cell.tutorialReceiver.TutorialEventSetup(2);
            }

            cell.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);

            cell = (AvatarTableCell)UpdateCellContent(cell, row);
            cell.SetRowNumber(row);

            return cell;
        }

        #endregion

        #region Table View event handlers

        //Will be called by the TableView when a cell's visibility changed
        public void TableViewCellVisibilityChanged(int row, bool isVisible)
        {
            //Debug.Log(string.Format("Row {0} visibility changed to {1}", row, isVisible));
            if (isVisible)
            {
                AvatarTableCell cell = (AvatarTableCell)m_tableView.GetCellAtRow(row);
                cell.NotifyBecameVisible();
            }
        }

        public TableViewCell UpdateCellContent(AvatarTableCell cell, int row)
        {
            if (cell.item == null)
            {
                if (row == 0)
                {
                    cell.avatarName = PlayerPrefs.GetString("PLAYER_NAME", "Hero");

                    cell.nameText.text = cell.avatarName;

                    cell.item = AOMAssets.AvatarLevelVG;

                    cell.icon.GetComponent<Image>().sprite = skillIcons[row];
                }
                else if (row > 0 && row <= 6)
                {
                    if (row == 1)
                        cell.item = AOMAssets.HeavenlyStrikeASVG;
                    else if (row == 2)
                        cell.item = AOMAssets.ShadowCloneASVG;
                    else if (row == 3)
                        cell.item = AOMAssets.CriticalStrikeASVG;
                    else if (row == 4)
                        cell.item = AOMAssets.WarCryASVG;
                    else if (row == 5)
                        cell.item = AOMAssets.BerserkerRageASVG;
                    else if (row == 6)
                        cell.item = AOMAssets.HandOfMidasASVG;

                    //cell.menu = menu;

                    cell.icon.GetComponent<Image>().sprite = skillIcons[row];

                    sb.Append("active_skill_");
                    sb.Append(row);

                    cell.avatarName = LanguageManager.Instance.GetTextValue(sb.ToString());
                    cell.nameText.text = cell.avatarName;

                    sb.Clear();

                    sb.Append(cell.item.ItemId);
                    sb.Append("_desc");

                    cell.description = LanguageManager.Instance.GetTextValue(sb.ToString());

                    sb.Clear();

                    if (((CharacterSkillVG)cell.item).CanBuy())
                    {
                        cell.nextLevelDescription = LanguageManager.Instance.GetTextValue("Level_Up");
                    }
                    else
                    {
                        cell.nextLevelDescription = LanguageManager.Instance.GetTextValue("to_unlock_skill");
                    }
                }
				else if (row == 7)
                {
                    //if (cell.PrestigeMenu == null)
                    //{
                        //Prestige
                        cell.item = null;

                        //cell.PrestigeMenu = prestigeMenu;

                        cell.icon.GetComponent<Image>().sprite = skillIcons[row];

                        cell.nextLevelCoinIcon.SetActive(false);
                        cell.nextLevelUpText.SetActive(false);

                        sb.Append("active_skill_");
                        sb.Append(row);

                        cell.avatarName = LanguageManager.Instance.GetTextValue(sb.ToString());
                        cell.nameText.text = cell.avatarName;
                        sb.Clear();

                        cell.levelText.text = "";
                        cell.nextLevelPriceText.text = "";
                        cell.tenLevelPriceText.text = "";
                        cell.hunLevelPriceText.text = "";

                        cell.description = LanguageManager.Instance.GetTextValue("active_skill_7_desc");
                        cell.descriptionText.text = cell.description;
						cell.descriptionText.alignment = TextAnchor.UpperLeft;

                        if (AOMAssets.AvatarLevelVG.GetBalance() < _avatarCore.PrestigeMinLevel)
                        {
							cell.nextLevelDescription = "LV600 " + LanguageManager.Instance.GetTextValue("prestige_btn");

							cell.levelUpButton.interactable = false;
                            cell.levelUpButton.GetComponent<Image>().sprite = cell.levelUpButton.GetComponent<Button>().spriteState.pressedSprite;
                        }
                        else
                        {
                            cell.nextLevelDescription = LanguageManager.Instance.GetTextValue("prestige_btn");

							cell.levelUpButton.interactable = true;
                            cell.levelUpButton.GetComponent<Image>().sprite = cell.levelUpButton.GetComponent<Button>().spriteState.highlightedSprite;
                        }

						cell.nextLevelDescriptionText.text = cell.nextLevelDescription;
                    //}
                }
            }

            if (row == 0)
            {
                // Avatar Name
                if (cell.avatarName != PlayerPrefs.GetString("PLAYER_NAME", "Tap Hero"))
                {
                    cell.avatarName = PlayerPrefs.GetString("PLAYER_NAME", "Tap Hero");
                    cell.nameText.text = cell.avatarName;
                }

                // Avatar Level
                CharacterLevelVG avatarLevel = AOMAssets.AvatarLevelVG;
                int level = avatarLevel.GetBalance();

                if (cell.level != level)
                {
                    cell.level = level;

                    sb.Append("LV. ");

                    if (cell.level >= avatarLevel.MaxLevel)
                    {
                        sb.Append("MAX");
                    }
                    else
                    {
                        sb.Append(cell.level.ToString());
                    }

                    cell.levelText.text = sb.ToString();//"LV. " + avatarLevel.GetBalance();
                    sb.Clear();

                    cell.priceForOne = avatarLevel.PriceForOne;
                    cell.nextLevelPriceText.text = UnitConverter.ConverterDoubleToString(cell.priceForOne);
                    cell.priceForTen = avatarLevel.PriceForTen;
                    cell.tenLevelPriceText.text = UnitConverter.ConverterDoubleToString(cell.priceForTen);
                    cell.priceForHun = avatarLevel.PriceForHundred;
                    cell.hunLevelPriceText.text = UnitConverter.ConverterDoubleToString(cell.priceForHun);

                    sb.Append("DMG: ");
                    sb.Append(UnitConverter.ConverterDoubleToString(_avatarCore.TapDamage));

                    cell.descriptionText.text = sb.ToString();//"DMG: " + UnitConverter.ConverterDoubleToString(DamageFormulas.AvatarBaseDamageForLevel(avatarLevel.GetBalance()));
					cell.descriptionText.alignment = TextAnchor.MiddleLeft;

                    sb.Clear();

                    sb.Append("+");
                    sb.Append(UnitConverter.ConverterDoubleToString(_avatarCore.TapNextLVDamage - _avatarCore.TapDamage));

                    cell.nextLevelDescriptionText.text = sb.ToString();//"DMG: " + UnitConverter.ConverterDoubleToString(DamageFormulas.AvatarBaseDamageForLevel(avatarLevel.GetBalance()));

                    sb.Clear();
                }

                if (avatarLevel.GetBalance() < avatarLevel.MaxLevel)
                {
                    if (!cell.levelUpButton.gameObject.activeSelf)
                    {
                        cell.levelUpButton.gameObject.SetActive(true);
                    }

                    if (avatarLevel.CanAffordOne())
                    {
                        if (!cell.levelUpButton.interactable)
                            cell.levelUpButton.interactable = true;

                        if (cell.levelUpButton.GetComponent<Image>().sprite != cell.levelUpButton.GetComponent<Button>().spriteState.highlightedSprite)
                            cell.levelUpButton.GetComponent<Image>().sprite = cell.levelUpButton.GetComponent<Button>().spriteState.highlightedSprite;

                    }
                    else
                    {
                        if (cell.levelUpButton.interactable)
                            cell.levelUpButton.interactable = false;

                        if (cell.levelUpButton.GetComponent<Image>().sprite != cell.levelUpButton.GetComponent<Button>().spriteState.pressedSprite)
                            cell.levelUpButton.GetComponent<Image>().sprite = cell.levelUpButton.GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
                else
                {
                    if (cell.levelUpButton.gameObject.activeSelf)
                    {
                        cell.levelUpButton.gameObject.SetActive(false);
                        cell.EnableBuyTenAndHundredButton(false);
                    }
                }
            }
            else if (row >= 1 && row <= 6)
            {
                CharacterSkillVG skillVG = (CharacterSkillVG)cell.item;

                int level = cell.item.GetBalance();
                if (cell.level != level)
                {
                    cell.level = level;

                    sb.Append("LV. ");
                    sb.Append(cell.level.ToString());
                    cell.levelText.text = sb.ToString();//"LV. " + item.GetBalance();
                    sb.Clear();

                    cell.priceForOne = skillVG.PriceForOne;
                    cell.nextLevelPriceText.text = UnitConverter.ConverterDoubleToString(skillVG.PriceForOne);

                    if (cell.level != 0)
                    {
                        if (row == 3 || row == 4 || row == 5 || row == 6)
                            cell.effectValueString = (skillVG.EffectValue * 100).ToString();
                        else
                            cell.effectValueString = UnitConverter.ConverterDoubleToString(skillVG.EffectValue);
                    }
                    else
                    {
                        if (row == 1)
                            cell.effectValueString = "140";
                        else if (row == 2)
                            cell.effectValueString = "7";
                        else if (row == 3)
                            cell.effectValueString = "17";
                        else if (row == 4)
                            cell.effectValueString = "150";
                        else if (row == 5)
                            cell.effectValueString = "70";
                        else if (row == 6)
                            cell.effectValueString = "15";
                        else
                            cell.effectValueString = "";
                    }

                    cell.descriptionText.text = string.Format(cell.description, cell.effectValueString);//"DMG: " + UnitConverter.ConverterDoubleToString(skillVG.EffectValue);
					cell.descriptionText.alignment = TextAnchor.MiddleLeft;
                }

                if (skillVG.CanBuy())
                {
                    if (!cell.nextLevelUpText.activeSelf)
                    {
                        cell.nextLevelUpText.SetActive(true);
                        cell.nextLevelDescription = LanguageManager.Instance.GetTextValue("Level_Up");
                    }

                    cell.nextLevelDescriptionText.text = cell.nextLevelDescription;

                    if (skillVG.GetBalance() < skillVG.MaxLevel)
                    {
                        if (!cell.levelUpButton.gameObject.activeSelf)
                        {
                            cell.levelUpButton.gameObject.SetActive(true);
                        }

                        if (skillVG.CanAffordOne())
                        {
                            if (!cell.levelUpButton.interactable)
                                cell.levelUpButton.interactable = true;

                            if (cell.levelUpButton.GetComponent<Image>().sprite != cell.levelUpButton.GetComponent<Button>().spriteState.highlightedSprite)
                                cell.levelUpButton.GetComponent<Image>().sprite = cell.levelUpButton.GetComponent<Button>().spriteState.highlightedSprite;

                            if (skillVG.GetBalance() < 1)
                            {
                                if (!cell.newLabel.activeSelf)
                                    cell.newLabel.SetActive(true);
                            }
                            else
                            {
                                if (cell.newLabel.activeSelf)
                                    cell.newLabel.SetActive(false);
                            }
                        }
                        else
                        {
                            if (cell.levelUpButton.interactable)
                                cell.levelUpButton.interactable = false;

                            if (cell.levelUpButton.GetComponent<Image>().sprite != cell.levelUpButton.GetComponent<Button>().spriteState.pressedSprite)
                                cell.levelUpButton.GetComponent<Image>().sprite = cell.levelUpButton.GetComponent<Button>().spriteState.pressedSprite;

                            if (cell.newLabel.activeSelf)
                                cell.newLabel.SetActive(false);
                        }
                    }
                    else
                    {
                        if (cell.levelUpButton.gameObject.activeSelf)
                        {
                            cell.levelUpButton.gameObject.SetActive(false);
                        }
                    }
                }
                else
                {
                    if (cell.newLabel.activeSelf)
                        cell.newLabel.SetActive(false);

                    if (cell.nextLevelUpText.activeSelf)
                    {
                        cell.nextLevelUpText.SetActive(false);
                        cell.nextLevelDescription = LanguageManager.Instance.GetTextValue("to_unlock_skill");
                    }

                    if (cell.unlockLevel != skillVG.CharacterMinLevel)
                    {
                        cell.unlockLevel = skillVG.CharacterMinLevel;
                    }

                    sb.Append("LV");
                    sb.Append(cell.unlockLevel.ToString());
                    sb.Append(cell.nextLevelDescription);

                    if (cell.nextLevelDescriptionText.text != sb.ToString())
                        cell.nextLevelDescriptionText.text = sb.ToString();//"LV" + skillVG.CharacterMinLevel + " to unlock";
                    sb.Clear();

                    if (cell.levelUpButton.interactable)
                        cell.levelUpButton.interactable = false;

                    if (cell.levelUpButton.GetComponent<Image>().sprite != cell.levelUpButton.GetComponent<Button>().spriteState.pressedSprite)
                        cell.levelUpButton.GetComponent<Image>().sprite = cell.levelUpButton.GetComponent<Button>().spriteState.pressedSprite;


                }
            }
            else if (row == 7)
            {
                // Prestige
                if (AOMAssets.AvatarLevelVG.GetBalance() < _avatarCore.PrestigeMinLevel)
                {
                    if (cell.levelUpButton.interactable)
                    {
                        cell.levelUpButton.interactable = false;

                        cell.nextLevelDescription = LanguageManager.Instance.GetTextValue("prestige_btn");

                        if (cell.levelUpButton.GetComponent<Image>().sprite != cell.levelUpButton.GetComponent<Button>().spriteState.pressedSprite)
                            cell.levelUpButton.GetComponent<Image>().sprite = cell.levelUpButton.GetComponent<Button>().spriteState.pressedSprite;

                        sb.Append("LV600 ");
                        sb.Append(cell.nextLevelDescription);

                        cell.nextLevelDescriptionText.text = sb.ToString();
                    }
                }
                else
                {
                    if (!cell.levelUpButton.interactable)
                    {
                        cell.levelUpButton.interactable = true;

                        cell.nextLevelDescription = LanguageManager.Instance.GetTextValue("prestige_btn");

                        cell.nextLevelDescriptionText.text = cell.nextLevelDescription;

                        if (cell.levelUpButton.GetComponent<Image>().sprite != cell.levelUpButton.GetComponent<Button>().spriteState.highlightedSprite)
                            cell.levelUpButton.GetComponent<Image>().sprite = cell.levelUpButton.GetComponent<Button>().spriteState.highlightedSprite;

                    }
                }

                sb.Clear();
            }

            return cell;
        }

        #endregion
        public void UpdateCellForRowInTableView(TableView tableView, int row, TableViewCell cell, bool goldBalanceIncreased)
        {
            UpdateCellContent((AvatarTableCell)cell, row);
        }
    }
}