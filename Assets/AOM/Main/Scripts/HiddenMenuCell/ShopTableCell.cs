﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Soomla.Store;
using SmartLocalization;

namespace Ignite.AOM
{
    public class ShopTableCell : SelectionObjectController
    {
        public ShopTableViewController containerTableViewController;

        VirtualCurrencyPack diamondPack;
        VirtualGood itemPack;

        public Text itemName;
        public Text itemDesc;
        public Text itemSold;
        public Image icon;

        public Text buttonLabel;
        public Text priceLabel;

        public Button buyBtn;

        private StringBuilder sb = new StringBuilder();

        public void Init(ShopTableViewController containerTableViewController, VirtualCurrencyPack diamondPack)
        {
            this.containerTableViewController = containerTableViewController;
            this.diamondPack = diamondPack;

            sb.Clear();
            sb.Append(this.diamondPack.ItemId);
            sb.Append("_name");
            itemName.text = LanguageManager.Instance.GetTextValue(sb.ToString());

            sb.Clear();
            sb.Append(this.diamondPack.ItemId);
            sb.Append("_desc");
            itemDesc.text = LanguageManager.Instance.GetTextValue(sb.ToString());

            buttonLabel.text = LanguageManager.Instance.GetTextValue("buy");
            priceLabel.text = AOMAssets.CurrencyPackMarketPriceAndCurrency(this.diamondPack.ItemId);

            string[] idList = { "diamonds_1200", "diamonds_3100", "diamonds_6500", "diamonds_14000" };

            for (int i = 0; i < idList.Length; i++)
            {
                if (this.diamondPack.ItemId == idList[i])
                {
                    icon.sprite = containerTableViewController.thumbnailIcon[Array.IndexOf(containerTableViewController.thumbnailIconName, this.diamondPack.ItemId + "_pack")];
                    break;
                }
                else
                {
                    icon.sprite = containerTableViewController.thumbnailIcon[Array.IndexOf(containerTableViewController.thumbnailIconName, this.diamondPack.ItemId)];
                }
            }
        }

        public void Init(ShopTableViewController containerTableViewController, VirtualGood itemPack)
        {
            this.containerTableViewController = containerTableViewController;
            this.itemPack = itemPack;

            sb.Clear();
            sb.Append(this.itemPack.ItemId);
            sb.Append("_name");
            itemName.text = LanguageManager.Instance.GetTextValue(sb.ToString());

            sb.Clear();
            sb.Append(this.itemPack.ItemId);
            sb.Append("_desc");
            itemDesc.text = LanguageManager.Instance.GetTextValue(sb.ToString());

            buttonLabel.text = LanguageManager.Instance.GetTextValue("buy");

#if UNITY_IOS
            priceLabel.text = AOMAssets.ProductMarketPriceAndCurrency(this.itemPack.ItemId.Replace("key", "keyofworld"));
#elif UNITY_ANDROID
            priceLabel.text = AOMAssets.ProductMarketPriceAndCurrency(this.itemPack.ItemId);
#endif

            icon.sprite = containerTableViewController.thumbnailIcon[Array.IndexOf(containerTableViewController.thumbnailIconName, this.itemPack.ItemId)];
        }

        public void Buy()
        {
            if (diamondPack != null)
            {
                try
                {
                    SoundManager.Instance.PlaySoundWithType(SoundType.Purchase_CashierBell);

                    AOMStoreInventory.BuyItem(this.diamondPack.ItemId);
                }
                catch (Exception e)
                {
                }
            }
            else if (itemPack != null)
            {
                try
                {
                    SoundManager.Instance.PlaySoundWithType(SoundType.Purchase_CashierBell);

                    AOMStoreInventory.BuyItem(this.itemPack.ItemId);
                }
                catch (Exception e)
                {
                }
            }
        }
    }
}
