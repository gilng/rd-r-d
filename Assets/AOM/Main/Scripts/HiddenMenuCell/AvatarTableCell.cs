using UnityEngine;
using System.Collections;
using Tacticsoft;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    //Inherit from TableViewCell instead of MonoBehavior to use the GameObject
    //containing this component as a cell in a TableView
    public class AvatarTableCell : TableViewCell
    {
        private UIViewLoader _uiViewLoader;
        private AvatarCore _avatarCore;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader, AvatarCore avatarCore)
        {
            _uiViewLoader = uiViewLoader;
            _avatarCore = avatarCore;
        }

        //public AvatarSkillMenuManager menu;
        public GameObject icon;

        public string avatarName;
        public Text nameText;

        public int level = -1;
        public Text levelText;

        public string effectValueString;
        public string description;
        public Text descriptionText;

        public GameObject newLabel;

        public Button levelUpButton;
        public Button levelUpTenButton;
        public Button levelUpHunButton;

        public GameObject levelUpBG;

        public double priceForOne = -1;
        public Text nextLevelPriceText;
        public double priceForTen = -1;
        public Text tenLevelPriceText;
        public double priceForHun = -1;
        public Text hunLevelPriceText;

        public int unlockLevel = -1;
        public string nextLevelDescription;
        public Text nextLevelDescriptionText;

        public GameObject nextLevelUpText;
        public GameObject nextLevelCoinIcon;

        private double levelUpButtonDownTime = 0;
        private double showTenAndHundredButtonTime = 0;
        private double lastBuyOneTime = 0;

        //private GameObject prestigeMenu;
        public LevelUpVG item;

        public GameObject levelUpEffect;

        public TutorialReceiver tutorialReceiver;

        private int row;

        private bool opendHundredPanel = false;

        public void SetRowNumber(int rowNumber)
        {
            //			Debug.Log("Row: " + rowNumber);
            //			m_rowNumberText.text = "Row " + rowNumber.ToString();
            row = rowNumber;
        }

        //public GameObject PrestigeMenu
        //{
        //    get { return this.prestigeMenu; }
        //    set { this.prestigeMenu = value; }
        //}

        void Awake()
        {
            EventManager.OnLanguageChange += UpdateCellLanguage;
        }

        void Start()
        {
            EnableBuyTenAndHundredButton(false);
        }

        void OnDestroy()
        {
            EventManager.OnLanguageChange -= UpdateCellLanguage;
        }

        void Update()
        {
            if (item == AOMAssets.AvatarLevelVG)
            {
                if (levelUpButtonDownTime != 0)
                {
                    double holding = UnitConverter.ConvertToTimestamp(System.DateTime.Now) - levelUpButtonDownTime;

                    if (holding >= 0.5)
                    {
                        if (AOMAssets.AvatarLevelVG.CanAffordOne())
                        {
                            EnableBuyTenAndHundredButton(true);
                        }

                        showTenAndHundredButtonTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                        levelUpButtonDownTime = 0;
                    }
                }

                if (showTenAndHundredButtonTime != 0)
                {
                    double showing = UnitConverter.ConvertToTimestamp(System.DateTime.Now) - showTenAndHundredButtonTime;
                    if (showing >= 2.6)
                    {
                        EnableBuyTenAndHundredButton(false);

                        showTenAndHundredButtonTime = 0;
                    }
                    else
                    {
                        EnableBuyTenAndHundredButton(true);
                    }
                }
            }
        }

        public void EnableBuyTenAndHundredButton(bool isEnable)
        {
            bool canAffordOne = AOMAssets.AvatarLevelVG.CanAffordOne();
            levelUpBG.SetActive(isEnable);

            if (levelUpBG.activeSelf)
            {
                bool canAffordTen = AOMAssets.AvatarLevelVG.CanAffordTen();
                levelUpTenButton.gameObject.SetActive(true);
                levelUpTenButton.GetComponent<Image>().sprite = canAffordTen ? levelUpTenButton.GetComponent<Button>().spriteState.highlightedSprite : levelUpTenButton.GetComponent<Button>().spriteState.pressedSprite;
                levelUpTenButton.interactable = canAffordOne ? true : false;

                bool canAffordHundred = AOMAssets.AvatarLevelVG.CanAffordHundred();
                levelUpHunButton.gameObject.SetActive(true);
                levelUpHunButton.GetComponent<Image>().sprite = canAffordHundred ? levelUpHunButton.GetComponent<Button>().spriteState.highlightedSprite : levelUpHunButton.GetComponent<Button>().spriteState.pressedSprite;
                levelUpHunButton.interactable = canAffordOne ? true : false;

                // Check arrive MaxLevel
                if (level <= AOMAssets.AvatarLevelVG.MaxLevel - 100)
                {
                    levelUpBG.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(489, 129);
                }
                else if (level <= AOMAssets.AvatarLevelVG.MaxLevel - 10)
                {
                    levelUpHunButton.gameObject.SetActive(false);
                    levelUpBG.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(254, 129);
                }
                else
                {
                    levelUpTenButton.gameObject.SetActive(false);
                    levelUpHunButton.gameObject.SetActive(false);
                    levelUpBG.SetActive(false);
                }
            }
            else
            {
                levelUpTenButton.gameObject.SetActive(false);
                levelUpHunButton.gameObject.SetActive(false);
            }
        }

        //		private int m_numTimesBecameVisible;
        public void NotifyBecameVisible()
        {
            //			m_numTimesBecameVisible++;
            //			m_visibleCountText.text = "# rows this cell showed : " + m_numTimesBecameVisible.ToString();
        }

        public void levelUpButtonDown()
        {
            //			if(row == 0)
            //			{
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            levelUpButtonDownTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
            //				if(showTenAndHundredButtonTime != 0)
            //					showTenAndHundredButtonTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
            //
            //				if(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - lastBuyOneTime <= 0.5)
            //				{
            //					if(AOMAssets.AvatarLevelVG.CanAffordOne())
            //						ShowBuyTenButton();
            //					if(AOMAssets.AvatarLevelVG.CanAffordTen())
            //						ShowBuyHundredButton();
            //					
            //					showTenAndHundredButtonTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
            //				}
            //			}
        }

        public void levelUpButtonUp()
        {
            if (levelUpButton.interactable)
            {
                if (item == null)
                {
                    GameObject prestigeMenuView = _uiViewLoader.LoadOverlayView("PrestigeMenuView", null, false, false);

                    return;
                }

                if (item.CanAffordOne())
                {
                    if (showTenAndHundredButtonTime != 0)
                        showTenAndHundredButtonTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);

                    if (UnitConverter.ConvertToTimestamp(System.DateTime.Now) - lastBuyOneTime <= 0.5)
                    {
                        //if (AOMAssets.AvatarLevelVG.CanAffordTen())
                        //{
                        //    ShowBuyHundredButton(AOMAssets.AvatarLevelVG.CanAffordHundred());
                        //}
                        if (item == AOMAssets.AvatarLevelVG)
                        {
                            if (AOMAssets.AvatarLevelVG.CanAffordOne())
                            {
                                //ShowBuyTenButton(AOMAssets.AvatarLevelVG.CanAffordTen());
                                EnableBuyTenAndHundredButton(true);
                            }
                        }

                        showTenAndHundredButtonTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                    }

                    if (UnitConverter.ConvertToTimestamp(System.DateTime.Now) - levelUpButtonDownTime <= 0.3)
                    {
                        lastBuyOneTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                        levelUpButtonDownTime = 0;

                        if (item.CanAffordOne())
                        {
                            item.BuyOne();

                            levelUpEffect.SetActive(true);
                            levelUpEffect.GetComponent<ParticleSystem>().Play();

                            SoundManager.Instance.PlaySoundWithType(SoundType.HeroLvUp_1);

                            Avatar.instance.levelUpEffect.SetActive(true);
                            Avatar.instance.levelUpEffect.GetComponent<ParticleSystem>().Play();
                        }
                    }

                    if (tutorialReceiver != null)
                    {
                        if (tutorialReceiver.isTriggered)
                            TutorialManager.Instance.UpdateTutorialProgress(tutorialReceiver.tutorialId);
                    }
                }
            }

            //			levelUpButton.GetComponent<Image> ().sprite = item.CanAffordOne() ? levelUpButton.GetComponent<Button>().spriteState.highlightedSprite : levelUpButton.GetComponent<Button>().spriteState.pressedSprite;
        }

        public void levelUpButtonExit()
        {
            levelUpButtonDownTime = 0;
        }

        public void levelUpTenButtonPressed()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            if (showTenAndHundredButtonTime != 0)
                showTenAndHundredButtonTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);

            item.BuyTen();

            levelUpTenButton.GetComponent<Image>().sprite = (AOMAssets.AvatarLevelVG.CanAffordTen()) ? levelUpTenButton.GetComponent<Button>().spriteState.highlightedSprite : levelUpTenButton.GetComponent<Button>().spriteState.pressedSprite;
            levelUpButton.GetComponent<Image>().sprite = item.CanAffordOne() ? levelUpButton.GetComponent<Button>().spriteState.highlightedSprite : levelUpButton.GetComponent<Button>().spriteState.pressedSprite;

            levelUpTenButton.interactable = (AOMAssets.AvatarLevelVG.CanAffordOne()) ? true : false;

            levelUpEffect.SetActive(true);
            levelUpEffect.GetComponent<ParticleSystem>().Play();

            SoundManager.Instance.PlaySoundWithType(SoundType.HeroLvUp_1);

            Avatar.instance.levelUpEffect.SetActive(true);
            Avatar.instance.levelUpEffect.GetComponent<ParticleSystem>().Play();
        }

        public void levelUpHunButtonPressed()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            if (showTenAndHundredButtonTime != 0)
                showTenAndHundredButtonTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);

            item.BuyHundred();

            levelUpHunButton.GetComponent<Image>().sprite = (AOMAssets.AvatarLevelVG.CanAffordHundred()) ? levelUpHunButton.GetComponent<Button>().spriteState.highlightedSprite : levelUpHunButton.GetComponent<Button>().spriteState.pressedSprite;
            levelUpTenButton.GetComponent<Image>().sprite = (AOMAssets.AvatarLevelVG.CanAffordTen()) ? levelUpTenButton.GetComponent<Button>().spriteState.highlightedSprite : levelUpTenButton.GetComponent<Button>().spriteState.pressedSprite;
            levelUpButton.GetComponent<Image>().sprite = item.CanAffordOne() ? levelUpButton.GetComponent<Button>().spriteState.highlightedSprite : levelUpButton.GetComponent<Button>().spriteState.pressedSprite;

            levelUpHunButton.interactable = (AOMAssets.AvatarLevelVG.CanAffordOne()) ? true : false;
            levelUpTenButton.interactable = (AOMAssets.AvatarLevelVG.CanAffordOne()) ? true : false;

            levelUpEffect.SetActive(true);
            levelUpEffect.GetComponent<ParticleSystem>().Play();

            SoundManager.Instance.PlaySoundWithType(SoundType.HeroLvUp_1);

            Avatar.instance.levelUpEffect.SetActive(true);
            Avatar.instance.levelUpEffect.GetComponent<ParticleSystem>().Play();
        }

        private System.Text.StringBuilder sb = new System.Text.StringBuilder();
        public void UpdateCellLanguage()
        {
            if (row > 0)
            {
                sb.Append("active_skill_");
                sb.Append(row);

                avatarName = LanguageManager.Instance.GetTextValue(sb.ToString());
                nameText.text = avatarName;

                sb.Clear();

                if (row >= 1 && row <= 6)
                {
                    sb.Append(item.ItemId);
                    sb.Append("_desc");

                    description = LanguageManager.Instance.GetTextValue(sb.ToString());
                    descriptionText.text = string.Format(description, effectValueString);
                    descriptionText.alignment = TextAnchor.MiddleLeft;

                    sb.Clear();

                    if (((CharacterSkillVG)item).CanBuy())
                    {
                        nextLevelDescription = LanguageManager.Instance.GetTextValue("Level_Up");
                        nextLevelDescriptionText.text = nextLevelDescription;
                    }
                    else
                    {
                        nextLevelDescription = LanguageManager.Instance.GetTextValue("to_unlock_skill");

                        sb.Append("LV");
                        sb.Append(unlockLevel.ToString());
                        sb.Append(nextLevelDescription);

                        nextLevelDescriptionText.text = sb.ToString();//"LV" + skillVG.CharacterMinLevel + " to unlock";
                        sb.Clear();
                    }
                }
                else if (row == 7)
                {
                    description = LanguageManager.Instance.GetTextValue("active_skill_7_desc");
                    descriptionText.text = description;
                    descriptionText.alignment = TextAnchor.UpperLeft;

                    if (AOMAssets.AvatarLevelVG.GetBalance() < _avatarCore.PrestigeMinLevel)
                    {
                        nextLevelDescription = LanguageManager.Instance.GetTextValue("prestige_btn");

                        sb.Append("LV600 ");
                        sb.Append(nextLevelDescription);

                        nextLevelDescriptionText.text = sb.ToString();
                        sb.Clear();
                    }
                    else
                    {
                        nextLevelDescription = LanguageManager.Instance.GetTextValue("prestige_btn");
                        nextLevelDescriptionText.text = nextLevelDescription;
                    }
                }
            }
        }

        public void AvatarTableCellOnClick()
        {
            if (item != null && item != AOMAssets.AvatarLevelVG)
            {
                GameObject avatarSkillDetailView = _uiViewLoader.LoadOverlayView("AvatarSkillMenuView", null, false, false);
                avatarSkillDetailView.GetComponent<AvatarSkillDetailViewController>().SetSkill((AvatarActiveSkillVG)item);
            }
        }
    }
}