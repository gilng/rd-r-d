﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Ignite.AOM
{
    public class AchievementTableCell : TableViewCell
    {
        private int row;
        public Image icon;
        public List<Image> stars;
        public Text achievementName, achievementSubtitle;
        public Text stat;
        public Button rewardBtn;
        private int rewards;
        public Text rewardsTxt, rewardsBtnLabel;
        public GameObject rewardsBtnIcon_1, rewardsBtnIcon_2;
        public Sprite star;

        public bool pressToReward = true;

        public MenuManager friendsInviteMenu;


        public void SetRowNumber(int rowNumber)
        {
            row = rowNumber;
        }

        public void NotifyBecameVisible()
        {
        }

        public void Reward()
        {
            if (pressToReward)
            {
                AchievementVG achievement = AchievementAssets.Achievements[row];

                rewards = achievement.Rewards;

                if (achievement.CanAffordOne())
                {
                    if (row == 0)
                    {
                        UserManager.Instance.GiveMoecrystals((int)rewards, () =>
                        {
                            achievement.BuyOne();

                            EventManager.RelicValueAdd((int) rewards);
                        });
                    }
                    else
                    {
                        UserManager.Instance.GiveGems((int)rewards, () =>
                        {
                            achievement.BuyOne();
                        });
                    }

                    //StartDiamondDropAnimation ();
                }

                SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
                //SoundManager.Instance.PlaySoundWithType(SoundType.DiamondCollected);
            }
            else
            {
                if (friendsInviteMenu != null)
                {
                    friendsInviteMenu.ShowMenu();
                }
                else
                {
                    GameManager.Instance.ShowFacebookInvitationMenu(false);
                }
            }
        }

        //IEnumerator StartDiamondAnimation(int i, int amount)
        //{
        //    yield return new WaitForSeconds(UnityEngine.Random.Range(0, 0.3f));
        //    float y = rewardBtn.transform.position.y + Mathf.Sin(Mathf.PI * (1.0f / 10) * i) * 1;
        //    float x = rewardBtn.transform.position.x + Mathf.Cos(Mathf.PI * (1.0f / 10) * i) * 1;
        //    //GameObject obj = Instantiate (AchievementMenuManager.instance.diamondForAnimation);
        //    GameObject obj = GameManager.instance.diamondPool.GetComponent<DiamondPool>().GetDiamond();
        //    obj.transform.position = new Vector3(x, y, 0);
        //    obj.SetActive(true);

        //    obj.GetComponent<SpriteRenderer>().sortingLayerName = "BreakScene";

        //    if (obj.GetComponent<DiamondDropAnimation>() == null)
        //    {
        //        obj.AddComponent<DiamondDropAnimation>().StartDrop(x, y, x * 1.5f, y * 1.5f,
        //                                             AchievementMenuManager.instance.diamondIcon.transform.position.x,
        //                                             AchievementMenuManager.instance.diamondIcon.transform.position.y,
        //                                          amount);
        //    }
        //}

        //public void StartDiamondDropAnimation()
        //{
        //    if (rewards < 1)
        //        return;
        //    int i = 1;
        //    int numberOfDiamond = 15;
        //    //only update balance 5 times
        //    int amountPerDiamond = rewards / 5;

        //    while (i <= numberOfDiamond)
        //    {
        //        if (i == 3 || i == 6 || i == 9 || i == 12 || i == 15)
        //            StartCoroutine(StartDiamondAnimation(i, amountPerDiamond));
        //        else
        //            StartCoroutine(StartDiamondAnimation(i, 0));
        //        i++;
        //    }
        //}

    }
}