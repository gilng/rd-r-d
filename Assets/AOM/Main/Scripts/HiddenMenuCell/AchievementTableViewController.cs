﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;
using SmartLocalization;
using System.Collections.Generic;
using System;
using Zenject;

namespace Ignite.AOM
{
    public class AchievementTableViewController : MonoBehaviour, ITableViewDataSource
    {
        private TableViewCell.Factory _tableViewCellFactory;

        [Inject]
        public void ConstructorSafeAttribute(TableViewCell.Factory tableViewCellFactory)
        {
            _tableViewCellFactory = tableViewCellFactory;
        }

        public AchievementTableCell m_cellPrefab;
        public TableView m_tableView;
        public int m_numRows;
        private int m_numInstancesCreated = 0;
        private System.Text.StringBuilder sb = new System.Text.StringBuilder();

        public MenuManager friendsInviteMenu;

        Sprite[] thumbnailIcon;
        string[] thumbnailIconName;

        //Register as the TableView's delegate (required) and data source (optional) to receive the calls
        void Awake()
        {
            m_tableView.dataSource = this;
            m_numRows = AchievementAssets.Achievements.Count;
            EventManager.OnAchievementVGBalanceChange += ReloadTable;
            EventManager.OnFriendInvited += ReloadTable;
        }

        void ReloadTable()
        {
            m_tableView.FastReloadData(false);
        }

        void OnDestroy()
        {
            EventManager.OnAchievementVGBalanceChange -= ReloadTable;
            EventManager.OnFriendInvited -= ReloadTable;
        }

        #region ITableViewDataSource

        //Will be called by the TableView to know how many rows are in this table
        public int GetNumberOfRowsForTableView(TableView tableView)
        {
            return m_numRows;
        }

        //Will be called by the TableView to know what is the height of each row
        public float GetHeightForRowInTableView(TableView tableView, int row)
        {
            return (m_cellPrefab.transform as RectTransform).rect.height;
        }

        //Will be called by the TableView when a cell needs to be created for display
        public TableViewCell GetCellForRowInTableView(TableView tableView, int row)
        {
            AchievementTableCell cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as AchievementTableCell;
            if (cell == null)
            {
                //cell = (AchievementTableCell)GameObject.Instantiate (m_cellPrefab);
                cell = _tableViewCellFactory.Create(m_cellPrefab.gameObject) as AchievementTableCell;
                sb.Append("VisibleCounterCellInstance_");
                sb.Append((++m_numInstancesCreated).ToString());
                cell.name = sb.ToString();//"VisibleCounterCellInstance_" + (++m_numInstancesCreated).ToString();
                sb.Clear();
            }
            UpdateCellContent(row, cell, false, true);
            cell.SetRowNumber(row);
            return cell;
        }

        #endregion

        #region Table View event handlers

        //Will be called by the TableView when a cell's visibility changed
        public void TableViewCellVisibilityChanged(int row, bool isVisible)
        {
            //Debug.Log(string.Format("Row {0} visibility changed to {1}", row, isVisible));
            if (isVisible)
            {
                AchievementTableCell cell = (AchievementTableCell)m_tableView.GetCellAtRow(row);
                cell.NotifyBecameVisible();
            }
        }

        public void UpdateCellContent(int row, AchievementTableCell cell, bool balanceIncreased, bool forceUpdate = false)
        {
            AchievementVG achievement = AchievementAssets.Achievements[row];
            for (int i = 0; i < achievement.GetBalance(); i++)
            {
                cell.stars[i].sprite = cell.star;
            }

            if (thumbnailIcon == null && thumbnailIconName == null)
            {
                thumbnailIcon = Resources.LoadAll<Sprite>("Sprites/table_ui_achievement/table_ui_achievement");
                thumbnailIconName = new string[thumbnailIcon.Length];

                for (int i = 0; i < thumbnailIconName.Length; i++)
                {
                    thumbnailIconName[i] = thumbnailIcon[i].name;
                }
            }

            cell.icon.sprite = thumbnailIcon[Array.IndexOf(thumbnailIconName, achievement.ItemId)];

            sb.Clear();
            sb.Append(achievement.ItemId);
            sb.Append("_name");
            cell.achievementName.text = string.Format(LanguageManager.Instance.GetTextValue(sb.ToString()), UnitConverter.ConverterDoubleToString(achievement.PriceForOne));

            if (row == 0)
            {

                if (cell.achievementSubtitle.gameObject.activeSelf == false)
                {
                    cell.achievementSubtitle.gameObject.SetActive(true);
                }

                cell.achievementSubtitle.text = string.Format(LanguageManager.Instance.GetTextValue("achievement_subtitle_friendsInvitedNum"), GamePlayStatManager.Instance.Stat[achievement.StatType]);

                cell.stat.text = string.Format("{0}/{1}",
                                                UnitConverter.ConverterDoubleToString(Mathf.Min((float)GamePlayStatManager.Instance.Stat[achievement.StatType], (float)achievement.PriceForOne)),
                                                UnitConverter.ConverterDoubleToString(achievement.PriceForOne));

                cell.rewardsTxt.gameObject.SetActive(achievement.CanAffordOne());
                cell.rewardsTxt.text = UnitConverter.ConverterDoubleToString(achievement.Rewards);

            }
            else
            {

                if (cell.achievementSubtitle.gameObject.activeSelf == true)
                {
                    cell.achievementSubtitle.gameObject.SetActive(false);
                    cell.achievementSubtitle.text = "";
                }

                cell.stat.text = string.Format("{0}/{1}",
                                                UnitConverter.ConverterDoubleToString(GamePlayStatManager.Instance.Stat[achievement.StatType]),
                                                UnitConverter.ConverterDoubleToString(achievement.PriceForOne));

                cell.rewardsTxt.text = UnitConverter.ConverterDoubleToString(achievement.Rewards);
            }

            cell.rewardsBtnLabel.text = LanguageManager.Instance.GetTextValue("achievement_btnLabel");



            if (achievement.Completed())
            {
                if (cell.rewardBtn.gameObject.activeSelf)
                    cell.rewardBtn.gameObject.SetActive(false);
            }
            else if (!achievement.CanAffordOne())
            {

                cell.rewardsBtnIcon_2.SetActive(false);
                cell.rewardsBtnIcon_1.SetActive(row != 0);

                if (row != 0)
                {
                    cell.rewardBtn.GetComponent<Image>().sprite = cell.rewardBtn.spriteState.pressedSprite;
                    cell.rewardBtn.interactable = false;
                }
                else
                {
                    cell.rewardBtn.GetComponent<Image>().sprite = cell.rewardBtn.spriteState.disabledSprite;

                    cell.rewardsBtnLabel.text = LanguageManager.Instance.GetTextValue("achievement_btnLabel_invite");
                    if (cell.friendsInviteMenu == null)
                        cell.friendsInviteMenu = friendsInviteMenu;
                    cell.pressToReward = false;
                }
            }
            else
            {
                cell.rewardsBtnIcon_1.SetActive(row != 0);
                cell.rewardsBtnIcon_2.SetActive(row == 0);

                cell.rewardBtn.GetComponent<Image>().sprite = cell.rewardBtn.spriteState.highlightedSprite;
                cell.rewardBtn.interactable = true;

                cell.rewardsBtnLabel.text = LanguageManager.Instance.GetTextValue("achievement_btnLabel");
                cell.pressToReward = true;
            }
        }

        #endregion
        public void UpdateCellForRowInTableView(TableView tableView, int row, TableViewCell cell, bool balanceIncreased)
        {
            UpdateCellContent(row, (AchievementTableCell)cell, balanceIncreased);
        }

    }
}
