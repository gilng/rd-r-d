﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Soomla.Store;
using System.Collections;
using Zenject;

namespace Ignite.AOM
{
    public class ShopTableViewController : UIOverlayViewController
    {
        private SelectionObjectController.Factory _selectionObjectControllerFactory;

        [Inject]
        public void ConstructorSafeAttribute(SelectionObjectController.Factory selectionObjectControllerFactory)
        {
            _selectionObjectControllerFactory = selectionObjectControllerFactory;
        }

        public Text title;
        public VirtualCurrencyPack[] diamondPack;
        public List<VirtualGood> itemPack;

        public Sprite[] thumbnailIcon;
        public string[] thumbnailIconName;

        public GridLayoutGroup shopLayoutGrid;
        public RectTransform shopLayoutRect;
        public RectTransform shopScrollViewRect;

        List<ShopTableCell> allShopObj = new List<ShopTableCell>();

        // Use this for initialization
        public override void InitView(UIViewController previousUIView = null, bool pause = false)
        {
            base.InitView(previousUIView, pause);

            AddListener();

            diamondPack = AOMAssets.DiamondPack;
            itemPack = AOMAssets.NonConsumableItems;

            thumbnailIcon = Resources.LoadAll<Sprite>("Sprites/table_ui_diamond/table_ui_diamond");
            thumbnailIconName = new string[thumbnailIcon.Length];

            for (int i = 0; i < thumbnailIconName.Length; i++)
            {
                thumbnailIconName[i] = thumbnailIcon[i].name;
            }

            StartCoroutine(LoadShopContent());
        }

        void AddListener()
        {
            EventManager.OnReloadTebleRow += ReloadTableRow;
            EventManager.OnGalleryCloseOverlay += CloseView;
        }

        void RemoveListener()
        {
            EventManager.OnReloadTebleRow -= ReloadTableRow;
            EventManager.OnGalleryCloseOverlay -= CloseView;
        }

        IEnumerator LoadShopContent()
        {
            for (int i = 0; i < itemPack.Count; i++)
            {
                ShopTableCell shopTableCellController = _selectionObjectControllerFactory.Create("ShopTableCell") as ShopTableCell;

                shopTableCellController.gameObject.name = itemPack[i].ItemId;

                shopTableCellController.gameObject.transform.SetParent(shopLayoutGrid.transform, false);

                shopTableCellController.gameObject.transform.localPosition = Vector3.zero;

                shopTableCellController.Init(this, itemPack[i]);

                if (itemPack[i].ItemId.Contains("key_"))
                {
                    shopTableCellController.buyBtn.gameObject.SetActive(itemPack[i].GetBalance() == 0);
                }

                //if (itemPack[i].ItemId == AOMAssets.HalloweenKeyGIVG.ItemId)
                //    shopTableCellController.buyBtn.gameObject.SetActive(AOMAssets.HalloweenKeyGIVG.GetBalance() == 0);
                //else if (itemPack[i].ItemId == AOMAssets.SummerKeyGIVG.ItemId)
                //    shopTableCellController.buyBtn.gameObject.SetActive(AOMAssets.SummerKeyGIVG.GetBalance() == 0);
                //else if (itemPack[i].ItemId == AOMAssets.ForestKeyGIVG.ItemId)
                //    shopTableCellController.buyBtn.gameObject.SetActive(AOMAssets.ForestKeyGIVG.GetBalance() == 0);
                //else if (itemPack[i].ItemId == AOMAssets.SchoolKeyGIVG.ItemId)
                //    shopTableCellController.buyBtn.gameObject.SetActive(AOMAssets.SchoolKeyGIVG.GetBalance() == 0);
                //else if (itemPack[i].ItemId == AOMAssets.AnotherWorldKeyGIVG.ItemId)
                //    shopTableCellController.buyBtn.gameObject.SetActive(AOMAssets.AnotherWorldKeyGIVG.GetBalance() == 0);

                allShopObj.Add(shopTableCellController);
            }

            for (int i = 0; i < diamondPack.Length; i++)
            {
                ShopTableCell shopTableCellController = _selectionObjectControllerFactory.Create("ShopTableCell") as ShopTableCell;

                shopTableCellController.gameObject.name = diamondPack[i].ItemId;

                shopTableCellController.gameObject.transform.SetParent(shopLayoutGrid.transform, false);

                shopTableCellController.gameObject.transform.localPosition = Vector3.zero;

                shopTableCellController.Init(this, diamondPack[i]);

                allShopObj.Add(shopTableCellController);
            }

            ResizeShopLayoutRect();

            yield return null;
        }

        void ResizeShopLayoutRect()
        {
            float shopLayoutRectSizeX = shopLayoutGrid.cellSize.x;
            float shopLayoutRectSizeY = (shopLayoutGrid.transform.childCount * shopLayoutGrid.cellSize.y) + ((shopLayoutGrid.transform.childCount - 1) * shopLayoutGrid.spacing.y);

            if (shopScrollViewRect.sizeDelta.y > shopLayoutRectSizeY)
                shopLayoutRectSizeY = shopScrollViewRect.sizeDelta.y;

            shopScrollViewRect.sizeDelta = new Vector2(shopLayoutRectSizeX, shopLayoutRectSizeY);

            shopLayoutRect.sizeDelta = new Vector2(shopLayoutRectSizeX, shopLayoutRectSizeY);
            shopLayoutRect.transform.localPosition = new Vector3(shopLayoutRect.transform.localPosition.x, 0, shopLayoutRect.transform.localPosition.z);
        }

        void ReloadTableRow()
        {
            foreach (ShopTableCell s in allShopObj)
            {
                s.transform.SetParent(null);
                Destroy(s.gameObject);
            }

            allShopObj.Clear();

            StartCoroutine(LoadShopContent());
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            RemoveListener();

            base.CloseView();
        }
    }
}
