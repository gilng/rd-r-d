﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using UnityEngine.UI;
using Soomla.Store;
using System;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    //Inherit from TableViewCell instead of MonoBehavior to use the GameObject
    //containing this component as a cell in a TableView
    public class DiamondTableCell : TableViewCell
    {
        private UIViewLoader _uiViewLoader;
        private GameItemCore _gameItemCore;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader, GameItemCore gameItemCore)
        {
            _uiViewLoader = uiViewLoader;
            _gameItemCore = gameItemCore;
        }

        public VirtualGood virtualGood;
        public VirtualCurrencyPack currencyPack;
        public GameItemVG item;
        public Text itemName;
        public Text itemDesc;
        public Text itemSold;
        public Text buttonLabel;
        public Text priceLabel;
        public Text freeLabel;
        private int row;
        public Button buyBtn;
        public Image icon, priceIcon;

        public void SetRowNumber(int rowNumber)
        {
            row = rowNumber;
        }

        public void NotifyBecameVisible()
        {
        }

        public void Buy()
        {
            if (item != null)
            {
                if (item.CanAffordOne())
                {
                    // Sound
                    if (AOMAssets.DoomGIVG == item)
                        SoundManager.Instance.PlaySoundWithType(SoundType.trigger);
//                    else
//                        SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                    item.BuyOne(() =>
                    {
                        _gameItemCore.UseItem(item.ItemId);

                        if (AOMAssets.MakeItRainGIVG == item || AOMAssets.DoomGIVG == item)
                        {
                            buyBtn.enabled = false;
                            buyBtn.GetComponent<Image>().sprite = buyBtn.GetComponent<Button>().spriteState.pressedSprite;
                            StartCoroutine(BuyButtonUnlock());
                        }

                        if (item.ItemId.Contains("key_"))
                        {
                            // Moetan Package Key
                            GameItemVG packageKeyVG = AOMAssets.GetGameItemVGFromItemId(item.ItemId);

                            if (packageKeyVG.GetBalance() == 0)
                            {
                                packageKeyVG.Give(1);
                                GameManager.Instance.enemyPool.KeyUnlock(packageKeyVG.ItemId);

                                GameObject gachaResultView = _uiViewLoader.LoadOverlayView("GachaResultMenuView", null, false, false);
                                gachaResultView.GetComponent<GachaResultViewController>().SetItemId(packageKeyVG, GachaType.Purchase);
                            }

                            buyBtn.gameObject.SetActive(false);
                        }
                    });
                }
                else
                {
                    OpenShopMenu();
                }
            }
            else if (currencyPack != null)
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.Purchase_CashierBell);

                try
                {
                    AOMStoreInventory.BuyItem(currencyPack.ItemId);
                }
                catch (Exception e)
                {
                }
            }
            else if (virtualGood != null)
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                try
                {
                    AOMStoreInventory.BuyItem(virtualGood.ItemId);
                }
                catch (Exception e)
                {
                }
            }

            // Logic
            if (buttonLabel.text == LanguageManager.Instance.GetTextValue("likeFB"))
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                OpenFacebookPage();

                UserManager.Instance.GiveGems(25, () =>
                {
                    PlayerPrefs.SetInt("FACEBOOK_LIKED", 1);
                    PlayerPrefs.SetInt("DONT_SHOW_FACEBOOK_LIKED", 1);
                });
            }
            else if (buttonLabel.text == LanguageManager.Instance.GetTextValue("followTwitter"))
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                OpenTwitterPage();

                UserManager.Instance.GiveGems(25, () =>
                {
                    PlayerPrefs.SetInt("TWITTER_FOLLOWED", 1);
                    PlayerPrefs.SetInt("DONT_SHOW_TWITTER_FOLLOWED", 1);
                });
            }
            else if (buttonLabel.text == LanguageManager.Instance.GetTextValue("giveRate"))
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

#if UNITY_EDITOR
                Application.OpenURL("https://play.google.com/store/apps/details?id=game.ignite.aom");
#elif UNITY_ANDROID
				//https://play.google.com/store/apps/details?id=game.ignite.aom
				Application.OpenURL("market://details?id=game.ignite.aom");
#elif UNITY_IPHONE
				Application.OpenURL("itms-apps://itunes.apple.com/app/id1059991997");
#endif

                UserManager.Instance.GiveGems(25, () =>
                {
                    PlayerPrefs.SetInt("APP_RATED", 1);
                    PlayerPrefs.SetInt("DONT_SHOW_RATE_OUR_GAME", 1);
                });
            }
        }

        void OpenFacebookPage()
        {
#if UNITY_EDITOR
            Application.OpenURL("http://facebook.com/aomzh");
#elif UNITY_IPHONE
            float startTime;
            startTime = Time.timeSinceLevelLoad;

            //open the facebook app
            Application.OpenURL("fb://profile/224859004521486");

            if (Time.timeSinceLevelLoad - startTime <= 1f)
            {
                //fail. Open safari.
                Application.OpenURL("http://facebook.com/aomzh");
            }
#elif UNITY_ANDROID
            if(checkPackageAppIsPresent("com.facebook.katana"))
            {
                Application.OpenURL("fb://page/224859004521486");
            }
            else
            {
                Application.OpenURL("http://facebook.com/aomzh");
            }
#endif
        }

        void OpenTwitterPage()
        {
#if UNITY_EDITOR
            Application.OpenURL("https://twitter.com/Attack_on_Moe");
#elif UNITY_IPHONE
            float startTime;
            startTime = Time.timeSinceLevelLoad;

            //open the twitter app
            Application.OpenURL("twitter:///user?screen_name=Attack_on_Moe");

            if (Time.timeSinceLevelLoad - startTime <= 1f)
            {
                //fail. Open safari.
                Application.OpenURL("https://twitter.com/Attack_on_Moe");
            }
#elif UNITY_ANDROID
            if(checkPackageAppIsPresent("com.twitter.android"))
            {
                Application.OpenURL("twitter://user?user_id=701743743548977152");
            }
            else
            {
                Application.OpenURL("https://twitter.com/Attack_on_Moe");
            }
#endif
        }

#if UNITY_ANDROID
        private bool checkPackageAppIsPresent(string package)
        {
            AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");

            //take the list of all packages on the device
            AndroidJavaObject appList = packageManager.Call<AndroidJavaObject>("getInstalledPackages", 0);
            int num = appList.Call<int>("size");
            for (int i = 0; i < num; i++)
            {
                AndroidJavaObject appInfo = appList.Call<AndroidJavaObject>("get", i);
                string packageNew = appInfo.Get<string>("packageName");
                if (packageNew.CompareTo(package) == 0)
                {
                    return true;
                }
            }
            return false;
        }
#endif

        IEnumerator BuyButtonUnlock()
        {
            if (AOMAssets.MakeItRainGIVG == item)
            {
                yield return new WaitForSeconds(10.0f);
            }
            else if (AOMAssets.DoomGIVG == item)
            {
                yield return new WaitForSeconds(2.0f);
            }
            else
            {
                yield return new WaitForSeconds(1.0f);
            }

            buyBtn.enabled = true;

            if (item.CanAffordOne())
            {
                buyBtn.GetComponent<Image>().sprite = buyBtn.GetComponent<Button>().spriteState.highlightedSprite;
            }
        }

        public void OpenShopMenu()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            _uiViewLoader.LoadOverlayView("ShopMenu", null, false, false);
        }
    }
}