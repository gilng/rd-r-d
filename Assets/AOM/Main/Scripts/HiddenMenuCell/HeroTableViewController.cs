using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;
using SmartLocalization;
using System;
using Zenject;

namespace Ignite.AOM
{
    //An example implementation of a class that communicates with a TableView
    public class HeroTableViewController : MonoBehaviour, ITableViewDataSource
    {
        private TableViewCell.Factory _tableViewCellFactory;
        private HeroCore _heroCore;

        [Inject]
        public void ConstructorSafeAttribute(TableViewCell.Factory tableViewCellFactory, HeroCore heroCore)
        {
            _tableViewCellFactory = tableViewCellFactory;
            _heroCore = heroCore;
        }

        public HeroTableCell m_cellPrefab;
        public TableView m_tableView;
        private int m_numRows;
        private int m_numInstancesCreated = 0;
        private System.Text.StringBuilder sb = new System.Text.StringBuilder();
        private System.Text.StringBuilder sb2 = new System.Text.StringBuilder();

        Sprite[] heroIcon;
        string[] heroIconName;

        void Awake()
        {
            m_tableView.dataSource = this;

            EventManager.OnGoldBalanceUpdated += RenderHeroList;
            EventManager.OnDiamondBalanceUpdated += UpdateHeroListReviveState;

            EventManager.OnPrestigeComplete += RefreshHeroTable;

            EventManager.OnHeroDead += HeroDead;
            EventManager.OnHeroRevive += HeroRevive;

            if (heroIcon == null && heroIconName == null)
            {
                heroIcon = Resources.LoadAll<Sprite>("Sprites/table_ui_hero/table_ui_hero");
                heroIconName = new string[heroIcon.Length];

                for (int i = 0; i < heroIconName.Length; i++)
                {
                    heroIconName[i] = heroIcon[i].name;
                }
            }
        }

        void OnDestroy()
        {
            EventManager.OnGoldBalanceUpdated -= RenderHeroList;
            EventManager.OnDiamondBalanceUpdated -= UpdateHeroListReviveState;

            EventManager.OnPrestigeComplete -= RefreshHeroTable;

            EventManager.OnHeroDead -= HeroDead;
            EventManager.OnHeroRevive -= HeroRevive;
        }

        //Register as the TableView's delegate (required) and data source (optional)
        //to receive the calls
        void Start()
        {
            //m_numRows = HeroAssets.Heroes.Count;
            //for (int i = 0; i < HeroAssets.Heroes.Count; i++)
            //{
            //    if (!HeroAssets.Heroes[i].CanAffordOne() && HeroAssets.Heroes[i].GetBalance() < 1)
            //    {
            //        m_numRows = i + 1;
            //        break;
            //    }
            //}

            RefreshHeroTable();
        }

        void HeroDead(int heroIdx, DateTime reviveTime)
        {
            ((HeroTableCell) (m_tableView.m_visibleCells[heroIdx])).OnHeroDead(reviveTime);
        }

        void HeroRevive(int heroIdx)
        {
            ((HeroTableCell) (m_tableView.m_visibleCells[heroIdx])).OnHeroRevive();
            UpdateCellContent(heroIdx, (HeroTableCell) m_tableView.m_visibleCells[heroIdx], false, true);
        }

        void RenderHeroList()
        {
            if (HeroAssets.Heroes[m_numRows - 1].CanAffordOne())
            {
                IncreaseNumberOfRow();
            }
        }

        void UpdateHeroListReviveState()
        {
            for (int i = 0; i < m_tableView.m_visibleCells.Count; i++)
            {
                ((HeroTableCell) (m_tableView.m_visibleCells[i])).UpdateReviveButtonState();
            }
        }

        public void IncreaseNumberOfRow()
        {
            if (m_numRows < HeroAssets.Heroes.Count)
            {
                m_numRows += 1;
                m_tableView.ReloadData();
            }
        }

        public void RefreshHeroTable()
        {
            m_numRows = HeroAssets.Heroes.Count;
            for (int i = 0; i < HeroAssets.Heroes.Count; i++)
            {
                if (!HeroAssets.Heroes[i].CanAffordOne() && HeroAssets.Heroes[i].GetBalance() < 1)
                {
                    m_numRows = i + 1;
                    break;
                }
            }

            m_tableView.ReloadData();
        }

        #region ITableViewDataSource

        //Will be called by the TableView to know how many rows are in this table
        public int GetNumberOfRowsForTableView(TableView tableView)
        {
            int cellH = 180;
            (transform.parent as RectTransform).sizeDelta = new Vector2((transform.parent as RectTransform).sizeDelta.x,
                m_numRows * cellH);
            (transform as RectTransform).sizeDelta =
                new Vector2((transform as RectTransform).sizeDelta.x, m_numRows * cellH);
            Vector2 pos = transform.localPosition;
            pos.y = (m_numRows * cellH) * -1;
            transform.localPosition = pos;
            return m_numRows;
        }

        //Will be called by the TableView to know what is the height of each row
        public float GetHeightForRowInTableView(TableView tableView, int row)
        {
            return (m_cellPrefab.transform as RectTransform).rect.height;
        }

        //Will be called by the TableView when a cell needs to be created for display
        public TableViewCell GetCellForRowInTableView(TableView tableView, int row)
        {
            HeroTableCell cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as HeroTableCell;
            if (cell == null)
            {
                //cell = (HeroTableCell)GameObject.Instantiate(m_cellPrefab);
                cell = _tableViewCellFactory.Create(m_cellPrefab.gameObject) as HeroTableCell;
                sb.Append("VisibleCounterCellInstance_");
                sb.Append((++m_numInstancesCreated).ToString());
                cell.name = sb.ToString(); //"VisibleCounterCellInstance_" + (++m_numInstancesCreated).ToString();
                sb.Clear();
            }

            // Tutorial Event 02 Setup
            if (row == 0 && cell.tutorialReceiver == null)
            {
                cell.tutorialReceiver = cell.gameObject.AddComponent<TutorialReceiver>();
                cell.tutorialReceiver.TutorialEventSetup(4);
            }

            if (cell.gameObject.GetComponent<TutorialReceiver>() == null)
            {
                cell.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);
            }

            cell.SetRowNumber(row);
            UpdateCellContent(row, cell, false, true);

            return cell;
        }

        public void UpdateCellContent(int row, HeroTableCell cell, bool goldBalanceIncreased, bool forceUpdate = false)
        {
            if (forceUpdate || (goldBalanceIncreased && (!cell.levelUpButton.interactable ||
                                                         (cell.unlockSkillButton.gameObject.activeInHierarchy &&
                                                          !cell.unlockSkillButton.interactable) ||
                                                         HeroAssets.Heroes[row].CanAffordTen()) ||
                                HeroAssets.Heroes[row].CanAffordHundred())
                || (!goldBalanceIncreased && (cell.levelUpButton.interactable ||
                                              (cell.unlockSkillButton.gameObject.activeInHierarchy &&
                                               cell.unlockSkillButton.interactable))))
            {
                if (cell.parentTableController == null)
                {
                    cell.parentTableController = this;
                    cell.currHeroIcon = heroIcon;
                    cell.currHeroIconName = heroIconName;

                    cell.icon.GetComponent<Image>().sprite =
                        heroIcon[Array.IndexOf(heroIconName, HeroAssets.Heroes[row].ItemId)];

                    sb.Append(HeroAssets.Heroes[row].ItemId);
                    sb.Append("_name");

                    cell.heroName = LanguageManager.Instance.GetTextValue(sb.ToString());
                    cell.nameText.text = cell.heroName;

                    sb.Clear();
                }

                int level = HeroAssets.Heroes[row].GetBalance();

                if (cell.level != level)
                {
                    cell.level = level;

                    sb.Append("LV. ");

                    if (cell.level >= HeroAssets.Heroes[row].MaxLevel)
                    {
                        sb.Append("MAX");
                    }
                    else
                    {
                        sb.Append(cell.level.ToString());
                    }

                    cell.levelText.text = sb.ToString();
                    sb.Clear();
                }

                if (cell.dps != _heroCore.HeroDPS[row])
                {
                    cell.dps = _heroCore.HeroDPS[row];

                    sb.Append("DPS: ");
                    sb.Append(UnitConverter.ConverterDoubleToString(cell.dps));
                    cell.dpsText.text = sb.ToString();
                    sb.Clear();
                }

                if (_heroCore.HeroAlive[row])
                {
                    if (cell.levelUpButton.interactable != HeroAssets.Heroes[row].CanAffordOne())
                        cell.levelUpButton.interactable = HeroAssets.Heroes[row].CanAffordOne();

                    double nextDPS = _heroCore.HeroDPSOnNextLevel[row] - _heroCore.HeroDPS[row];
                    if (cell.nextDPS != nextDPS)
                    {
                        cell.nextDPS = nextDPS;

                        sb.Append("+");
                        sb.Append(UnitConverter.ConverterDoubleToString(cell.nextDPS));

                        cell.nextLevelDPS.text = sb.ToString();
                        sb.Clear();
                    }

                    if (cell.priceForOne != HeroAssets.Heroes[row].PriceForOne)
                    {
                        cell.priceForOne = HeroAssets.Heroes[row].PriceForOne;
                        cell.priceForOneText.text = UnitConverter.ConverterDoubleToString(cell.priceForOne);
                    }

                    if (cell.priceForTen != HeroAssets.Heroes[row].PriceForTen)
                    {
                        cell.priceForTen = HeroAssets.Heroes[row].PriceForTen;
                        cell.priceForTenText.text = UnitConverter.ConverterDoubleToString(cell.priceForTen);
                    }

                    if (cell.priceForHun != HeroAssets.Heroes[row].PriceForHundred)
                    {
                        cell.priceForHun = HeroAssets.Heroes[row].PriceForHundred;
                        cell.priceForHunText.text = UnitConverter.ConverterDoubleToString(cell.priceForHun);
                    }

                    if (HeroAssets.Heroes[row].CanAffordOne())
                    {
                        if (HeroAssets.Heroes[row].GetBalance() < 1)
                        {
                            if (!cell.newLabel.activeSelf)
                                cell.newLabel.SetActive(true);
                        }
                        else
                        {
                            if (cell.newLabel.activeSelf)
                                cell.newLabel.SetActive(false);
                        }

//                        if (cell.levelUpButton.gameObject.activeSelf)
//                        {
//                            if (!cell.levelUpButton.interactable)
//                                cell.levelUpButton.interactable = true;
//
//                            if (cell.levelUpButton.GetComponent<Image>().sprite != cell.levelUpButton.GetComponent<Button>().spriteState.highlightedSprite)
//                                cell.levelUpButton.GetComponent<Image>().sprite = cell.levelUpButton.GetComponent<Button>().spriteState.highlightedSprite;
//                        }

                        if (cell.levelUpButton.gameObject.activeSelf)
                        {
                            if (!cell.levelUpButton.interactable)
                                cell.levelUpButton.interactable = true;

                            if (cell.levelUpButton.GetComponent<Image>().sprite != cell.levelUpButton
                                    .GetComponent<Button>()
                                    .spriteState.highlightedSprite)
                                cell.levelUpButton.GetComponent<Image>().sprite = cell.levelUpButton
                                    .GetComponent<Button>()
                                    .spriteState.highlightedSprite;
                        }
                    }
                    else
                    {
                        if (cell.newLabel.activeSelf)
                            cell.newLabel.SetActive(false);

                        if (cell.levelUpButton.gameObject.activeSelf)
                        {
                            if (cell.levelUpButton.interactable)
                                cell.levelUpButton.interactable = false;

                            if (cell.levelUpButton.GetComponent<Image>().sprite != cell.levelUpButton
                                    .GetComponent<Button>()
                                    .spriteState.pressedSprite)
                                cell.levelUpButton.GetComponent<Image>().sprite = cell.levelUpButton
                                    .GetComponent<Button>()
                                    .spriteState.pressedSprite;
                        }
                    }

                    if (!goldBalanceIncreased)
                    {
                        cell.UpdateHeroSkill();
                    }
                    else
                    {
                        cell.UpdateSkillButtonStatus();
                    }

                    if (cell.levelUpButton.gameObject.activeSelf == cell.unlockSkillButton.gameObject.activeSelf)
                    {
                        cell.levelUpButton.gameObject.SetActive(!cell.unlockSkillButton.gameObject.activeSelf);
                    }

                    if (cell.level >= HeroAssets.Heroes[row].MaxLevel)
                    {
                        if (cell.levelUpButton.gameObject.activeSelf)
                        {
                            cell.levelUpButton.gameObject.SetActive(false);
                            cell.levelUpButton_Block.gameObject.SetActive(false);

                            cell.EnableBuyTenAndHundredButton(false);
                        }
                    }
                    else
                    {
                        if (!cell.levelUpButton.gameObject.activeSelf)
                        {
                            cell.levelUpButton.gameObject.SetActive(true);
                            cell.levelUpButton_Block.gameObject.SetActive(true);
                        }
                    }

                    cell.OnHeroRevive();
                }
                else
                {
                    cell.OnHeroDead(_heroCore.HeroReviveTime[row]);
                }
            }
        }

        public void UpdateCellForRowInTableView(TableView tableView, int row, TableViewCell cell,
            bool goldBalanceIncreased)
        {
            UpdateCellContent(row, (HeroTableCell) cell, goldBalanceIncreased);
        }

        #endregion

        #region Table View event handlers

        //Will be called by the TableView when a cell's visibility changed
        public void TableViewCellVisibilityChanged(int row, bool isVisible)
        {
            //Debug.Log(string.Format("Row {0} visibility changed to {1}", row, isVisible));
            if (isVisible)
            {
                HeroTableCell cell = (HeroTableCell) m_tableView.GetCellAtRow(row);
                cell.NotifyBecameVisible();
            }
        }

        #endregion
    }
}