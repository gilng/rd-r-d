using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;
using SmartLocalization;
using Soomla.Store;
using System.Text;
using System;
using Zenject;

namespace Ignite.AOM
{
    //An example implementation of a class that communicates with a TableView
    public class DiamondTableViewController : MonoBehaviour, ITableViewDataSource
    {
        private TableViewCell.Factory _tableViewCellFactory;
        private AvatarSkillCore _avatarSkillCore;
        private EnemyCore _enemyCore;
        private GameItemCore _gameItemCore;

        [Inject]
        public void ConstructorSafeAttribute(TableViewCell.Factory tableViewCellFactory, AvatarSkillCore avatarSkillCore, EnemyCore enemyCore, GameItemCore gameItemCore)
        {
            _tableViewCellFactory = tableViewCellFactory;

            _avatarSkillCore = avatarSkillCore;
            _enemyCore = enemyCore;
            _gameItemCore = gameItemCore;
        }

        public DiamondTableCell m_cellPrefab;
        public TableView m_tableView;
        //public VirtualCurrencyPack[] diamondPack;
        private int m_numRows;
        private int m_numInstancesCreated = 0;
        private StringBuilder sb = new StringBuilder();
        private Text timeLabelForGuardianShield, timeLabelForPowerOfHolding;
        Sprite[] thumbnailIcon;
        string[] thumbnailIconName;

        private List<string> addedItem = new List<string>();

        public RectTransform diamondScrollViewContent, fieldGuideLayoutRect;


        //Register as the TableView's delegate (required) and data source (optional) to receive the calls

        void Awake()
        {
            LoadTableRow();

            m_tableView.dataSource = this;
            EventManager.OnDiamondBalanceUpdated += Reload;
            EventManager.OnGameItemVGBalanceChange += Reload;
            EventManager.OnDiamondPriceUpdated += Reload;
            EventManager.OnReloadTebleRow += Reload;

            if (thumbnailIcon == null && thumbnailIconName == null)
            {
                thumbnailIcon = Resources.LoadAll<Sprite>("Sprites/table_ui_diamond/table_ui_diamond");
                thumbnailIconName = new string[thumbnailIcon.Length];

                for (int i = 0; i < thumbnailIconName.Length; i++)
                {
                    thumbnailIconName[i] = thumbnailIcon[i].name;
                }
            }
        }

        public void LoadTableRow()
        {
            m_numRows = AOMAssets.GameItemVGs.Count;

#if UNITY_ANDROID || UNITY_EDITOR
            if (PlayerPrefs.GetInt("APP_RATED", 0) != 1) // = 1 means already "GiveRate"
                m_numRows += 1;
#endif

            if (PlayerPrefs.GetInt("FACEBOOK_LIKED", 0) != 1) // = 1 means already "LikeFB"
                m_numRows += 1;

            if (PlayerPrefs.GetInt("TWITTER_FOLLOWED", 0) != 1) // = 1 means already "FollowTwitter"
                m_numRows += 1;

            fieldGuideLayoutRect.sizeDelta = new Vector2(0, 180 * (m_numRows + 1));
            diamondScrollViewContent.sizeDelta = new Vector2(0, 180 * (m_numRows + 1));
            this.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 180 * (m_numRows + 1));
            this.gameObject.transform.localPosition = new Vector3(0, -(180 * (m_numRows + 1)), 0);
        }

        void OnDestroy()
        {
            EventManager.OnDiamondBalanceUpdated -= Reload;
            EventManager.OnGameItemVGBalanceChange -= Reload;
            EventManager.OnDiamondPriceUpdated -= Reload;
            EventManager.OnReloadTebleRow -= Reload;
        }

        //void Start()
        //{
        //    diamondPack = AOMAssets.DiamondPack;
        //}

        void Update()
        {
            ((SkillRefreshGIVG)(AOMAssets.SkillRefreshGIVG)).CheckPriceUpdated(_avatarSkillCore.TotalCooldownTime());
            ((GuardianShieldGIVG)(AOMAssets.GuardianShieldGIVG)).CheckPriceUpdated(_enemyCore.Stage);
            if (_gameItemCore.GuardianShieldTime != null && timeLabelForGuardianShield != null && (_gameItemCore.GuardianShieldTime - DateTime.Now).TotalSeconds > 0)
            {
                timeLabelForGuardianShield.text = UnitConverter.SecondsToTime((_gameItemCore.GuardianShieldTime - DateTime.Now).TotalSeconds);
            }

            if (Avatar.instance.UsingPowerOfHolding)
                timeLabelForPowerOfHolding.text = Avatar.instance.PowerOfHoldingRemain();
        }


        bool isInit = true;

        void Reload()
        {
            LoadTableRow();

            m_tableView.ReloadData();

            bool removeReusableCell = false;

            do
            {
                DiamondTableCell cell = m_tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as DiamondTableCell;

                if (cell != null)
                    Destroy(cell.gameObject);
                else
                    removeReusableCell = true;
            } while (removeReusableCell == false);

        }

        #region ITableViewDataSource

        //Will be called by the TableView to know how many rows are in this table
        public int GetNumberOfRowsForTableView(TableView tableView)
        {
            //#if UNITY_ANDROID || UNITY_EDITOR
            //               m_numRows = 8;
            //#elif UNITY_IPHONE
            //              m_numRows = 7;
            //#endif
            return m_numRows;
        }

        //Will be called by the TableView to know what is the height of each row
        public float GetHeightForRowInTableView(TableView tableView, int row)
        {
            return (m_cellPrefab.transform as RectTransform).rect.height;
        }

        //Will be called by the TableView when a cell needs to be created for display
        public TableViewCell GetCellForRowInTableView(TableView tableView, int row)
        {
            DiamondTableCell cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as DiamondTableCell;

            if (cell == null)
            {
                //cell = (DiamondTableCell)GameObject.Instantiate(m_cellPrefab);
                cell = _tableViewCellFactory.Create(m_cellPrefab.gameObject) as DiamondTableCell;
                cell.name = "VisibleCounterCellInstance_" + (++m_numInstancesCreated).ToString();
            }

            cell = (DiamondTableCell)UpdateCellContent(cell, row);
            cell.SetRowNumber(row);

            return cell;
        }

        #endregion

        public TableViewCell UpdateCellContent(DiamondTableCell cell, int row)
        {
            string itemId = "";
            string itemName = "";
            string itemDesc = "";
            string itemPrice = "";
            string itemFree = "";
            string buttonLabel = LanguageManager.Instance.GetTextValue("buy");

            if (row < AOMAssets.GameItemVGs.Count)
            {
                cell.currencyPack = null;
                cell.virtualGood = null;
                cell.item = AOMAssets.GameItemVGs[row];
                itemId = cell.item.ItemId;
                itemPrice = cell.item.Price.ToString();
                itemFree = "";
                if (cell.item.GetBalance() > 0)
                {
                    buttonLabel = LanguageManager.Instance.GetTextValue("use");
                    sb.Clear();
                    sb.Append("x");
                    sb.Append(cell.item.GetBalance());
                    itemPrice = sb.ToString();

                    // Bought key
                    if (itemId.Contains("key_"))
                    {
                        cell.buyBtn.gameObject.SetActive(false);
                    }

                    //if (AOMAssets.AnotherWorldKeyGIVG.ItemId == itemId)
                    //{
                    //    if (AOMAssets.AnotherWorldKeyGIVG.GetBalance() > 0)
                    //        cell.buyBtn.gameObject.SetActive(false);
                    //}

                    //if (AOMAssets.SchoolKeyGIVG.ItemId == itemId)
                    //{
                    //    if (AOMAssets.SchoolKeyGIVG.GetBalance() > 0)
                    //        cell.buyBtn.gameObject.SetActive(false);
                    //}

                    //if (AOMAssets.ForestKeyGIVG.ItemId == itemId)
                    //{
                    //    if (AOMAssets.ForestKeyGIVG.GetBalance() > 0)
                    //        cell.buyBtn.gameObject.SetActive(false);
                    //}

                    //if (AOMAssets.SummerKeyGIVG.ItemId == itemId)
                    //{
                    //    if (AOMAssets.SummerKeyGIVG.GetBalance() > 0)
                    //        cell.buyBtn.gameObject.SetActive(false);
                    //}

                    //if (AOMAssets.HalloweenKeyGIVG.ItemId == itemId)
                    //{
                    //    if (AOMAssets.HalloweenKeyGIVG.GetBalance() > 0)
                    //        cell.buyBtn.gameObject.SetActive(false);
                    //}
                    // Bought key
                }
                else
                {
                    bool isUsing = false;
                    if (AOMAssets.GuardianShieldGIVG.ItemId == itemId)
                    {
                        timeLabelForGuardianShield = cell.priceLabel;
                        if (_gameItemCore.GuardianShieldTime != null && (_gameItemCore.GuardianShieldTime - DateTime.Now).TotalSeconds > 0)
                        {
                            isUsing = true;
                            itemPrice = UnitConverter.SecondsToTime((_gameItemCore.GuardianShieldTime - DateTime.Now).TotalSeconds);
                        }
                    }

                    if (AOMAssets.PowerOfHoldingGIVG.ItemId == itemId)
                    {
                        timeLabelForPowerOfHolding = cell.priceLabel;
                        isUsing = Avatar.instance.UsingPowerOfHolding;
                        if (isUsing)
                        {
                            if (cell.priceIcon.gameObject.activeSelf)
                                cell.priceIcon.gameObject.SetActive(false);

                            itemPrice = Avatar.instance.PowerOfHoldingRemain();
                        }
                        else
                        {
                            if (cell.priceIcon.gameObject.activeSelf == false)
                                cell.priceIcon.gameObject.SetActive(true);
                        }
                    }

                    // Bought key
                    if (itemId.Contains("key_"))
                    {
                        cell.buyBtn.gameObject.SetActive(true);
                    }

                    //if (AOMAssets.AnotherWorldKeyGIVG.ItemId == itemId)
                    //{
                    //    if (AOMAssets.AnotherWorldKeyGIVG.GetBalance() > 0)
                    //        cell.buyBtn.gameObject.SetActive(false);
                    //}

                    //if (AOMAssets.SchoolKeyGIVG.ItemId == itemId)
                    //{
                    //    if (AOMAssets.SchoolKeyGIVG.GetBalance() > 0)
                    //        cell.buyBtn.gameObject.SetActive(false);
                    //}

                    //if (AOMAssets.ForestKeyGIVG.ItemId == itemId)
                    //{
                    //    if (AOMAssets.ForestKeyGIVG.GetBalance() > 0)
                    //        cell.buyBtn.gameObject.SetActive(false);
                    //}

                    //if (AOMAssets.SummerKeyGIVG.ItemId == itemId)
                    //{
                    //    if (AOMAssets.SummerKeyGIVG.GetBalance() > 0)
                    //        cell.buyBtn.gameObject.SetActive(false);
                    //}

                    //if (AOMAssets.HalloweenKeyGIVG.ItemId == itemId)
                    //{
                    //    if (AOMAssets.HalloweenKeyGIVG.GetBalance() > 0)
                    //        cell.buyBtn.gameObject.SetActive(false);
                    //}
                    // Bought key

                    if (cell.item.CanAffordOne())
                    {
                        if (!isUsing)
                        {
                            cell.buyBtn.enabled = true;
                            cell.buyBtn.GetComponent<Image>().sprite = cell.buyBtn.GetComponent<Button>().spriteState.highlightedSprite;
                        }
                        else
                        {
                            cell.buyBtn.enabled = false;
                            cell.buyBtn.GetComponent<Image>().sprite = cell.buyBtn.GetComponent<Button>().spriteState.pressedSprite;
                        }
                    }
                    else
                    {
                        cell.buyBtn.enabled = true;
                        cell.buyBtn.GetComponent<Image>().sprite = cell.buyBtn.GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else
            {
                List<string> freeItemList = new List<string>();

#if UNITY_ANDROID || UNITY_EDITOR
                if (PlayerPrefs.GetInt("APP_RATED", 0) == 0) // = 1 means already "GiveRate"
                    freeItemList.Add("diamonds_25_2");
#endif

                if (PlayerPrefs.GetInt("FACEBOOK_LIKED", 0) == 0) // = 1 means already "LikeFB"
                    freeItemList.Add("diamonds_25");

                if (PlayerPrefs.GetInt("TWITTER_FOLLOWED", 0) == 0) // = 1 means already "FollowTwitter"
                    freeItemList.Add("diamonds_25_3");

                for (int i = 0; i < addedItem.Count; i++)
                {
                    if (freeItemList.Contains(addedItem[i]))
                        freeItemList.Remove(addedItem[i]);
                }

                if (freeItemList.Count > 0)
                {
                    cell.item = null;
                    cell.currencyPack = null;
                    cell.virtualGood = null;

                    cell.buyBtn.enabled = true;
                    cell.buyBtn.GetComponent<Image>().sprite = cell.buyBtn.GetComponent<Button>().spriteState.highlightedSprite;

                    if (cell.priceIcon.gameObject.activeSelf)
                        cell.priceIcon.gameObject.SetActive(false);

                    itemPrice = "";
                    itemFree = LanguageManager.Instance.GetTextValue("BuyFree");
                    itemId = freeItemList[0];

                    switch (itemId)
                    {
                        case "diamonds_25": // Like FB
                            buttonLabel = LanguageManager.Instance.GetTextValue("likeFB");
                            break;
                        case "diamonds_25_2": // Give Rate
                            buttonLabel = LanguageManager.Instance.GetTextValue("giveRate");
                            break;
                        case "diamonds_25_3": //Follow Twitter
                            buttonLabel = LanguageManager.Instance.GetTextValue("followTwitter");
                            break;
                    }

                    addedItem.Add(itemId);

                    if (row == m_numRows - 1)
                    {
                        addedItem.Clear();
                    }
                }
            }

            if (itemId != "")
            {
                sb.Clear();
                sb.Append(itemId);
                sb.Append("_name");
                itemName = LanguageManager.Instance.GetTextValue(sb.ToString());
                cell.itemName.text = itemName;

                cell.icon.sprite = thumbnailIcon[Array.IndexOf(thumbnailIconName, itemId)];

                if (row == AOMAssets.NonConsumableItems.Count)
                {
                    sb.Clear();
                    sb.Append(itemId);
                    sb.Append("_desc");
                    itemDesc = string.Format(LanguageManager.Instance.GetTextValue(sb.ToString()), UnitConverter.ConverterDoubleToString(Formulas.EnemyGoldDropForLevel(_enemyCore.Stage, EnemyType.Normal) * 5000));
                }
                else
                {
                    if (itemId == "diamonds_25_2")
                    {
                        sb.Clear();
                        sb.Append(itemId);
                        sb.Append("_desc_1");
                        itemDesc = LanguageManager.Instance.GetTextValue(sb.ToString());

#if UNITY_ANDROID || UNITY_EDITOR
                        sb.Clear();
                        sb.Append(itemId);
                        sb.Append("_desc_2");
                        itemDesc += LanguageManager.Instance.GetTextValue(sb.ToString());
#endif
                    }
                    else
                    {
                        sb.Clear();
                        sb.Append(itemId);
                        sb.Append("_desc");
                        itemDesc = LanguageManager.Instance.GetTextValue(sb.ToString());
                    }
                }

                cell.itemDesc.text = itemDesc;
                cell.priceLabel.text = itemPrice;
                cell.freeLabel.text = itemFree;
                cell.buttonLabel.text = buttonLabel;
            }

            return cell;
        }

        #region Table View event handlers

        //Will be called by the TableView when a cell's visibility changed
        public void TableViewCellVisibilityChanged(int row, bool isVisible)
        {
            //Debug.Log(string.Format("Row {0} visibility changed to {1}", row, isVisible));
            if (isVisible)
            {
                DiamondTableCell cell = (DiamondTableCell)m_tableView.GetCellAtRow(row);
                cell.NotifyBecameVisible();
            }
        }

        #endregion

        public void UpdateCellForRowInTableView(TableView tableView, int row, TableViewCell cell, bool goldBalanceIncreased)
        {
            UpdateCellContent((DiamondTableCell)cell, row);
        }
    }
}
