﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Tacticsoft;
using SmartLocalization;
using System.Collections.Generic;
using System;
using Zenject;

namespace Ignite.AOM
{
    //An example implementation of a class that communicates with a TableView
    public class ArtifactTableViewController : MonoBehaviour, ITableViewDataSource
    {
        private TableViewCell.Factory _tableViewCellFactory;
        private ArtifactCore _artifactCore;

        [Inject]
        public void ConstructorSafeAttribute(TableViewCell.Factory tableViewCellFactory, ArtifactCore artifactCore)
        {
            _tableViewCellFactory = tableViewCellFactory;
            _artifactCore = artifactCore;
        }

        public ArtifactTableCell m_cellPrefab;
        public TableView m_tableView;
        private IDictionary<string, ArtifactVG> afs = new Dictionary<string, ArtifactVG>();
        private IList<string> purchasedAfIDs;
        public int m_numRows;
        public int previous_m_numRows = -1;
        private int m_numInstancesCreated = 0;

        private System.Text.StringBuilder sb = new System.Text.StringBuilder();

        private Dictionary<string, Sprite> thumbnailIcon;



        //Register as the TableView's delegate (required) and data source (optional)
        //to receive the calls
        void Awake()
        {
            m_tableView.dataSource = this;
            EventManager.OnRelicPriceUpdated += RenderArtifactTable;
            EventManager.OnRelicBalanceUpdated += RenderArtifactTable;

            EventManager.OnSalvageHandler += RenderArtifactTable;
        }

        void OnDestroy()
        {
            EventManager.OnRelicPriceUpdated -= RenderArtifactTable;
            EventManager.OnRelicBalanceUpdated -= RenderArtifactTable;

            EventManager.OnSalvageHandler -= RenderArtifactTable;
        }

        void Start()
        {
            ReloadArtifactList();
        }

        void RenderArtifactTable()
        {
            RenderArtifactTable(null, 0);
        }

        void RenderArtifactTable(ArtifactVG vg, int balance)
        {
            previous_m_numRows = m_numRows;
            ReloadArtifactList();
            if (m_numRows == previous_m_numRows)
            {
                m_tableView.FastReloadData(false);
            }
            else
            {
                m_tableView.ReloadData();
            }
        }

        void ReloadArtifactList()
        {
            purchasedAfIDs = _artifactCore.PurchasedAFIds();
            IList<ArtifactVG> afList = _artifactCore.FindOwnedArtifact();
            m_numRows = 1;
            afs.Clear();
            if (afList != null && afList.Count > 0)
            {
                for (int i = 0; i < afList.Count; i++)
                {
                    ArtifactVG af = afList[i];
                    afs[af.ItemId] = af;
                }
                m_numRows += afList.Count;
            }
        }

        #region ITableViewDataSource

        //Will be called by the TableView to know how many rows are in this table
        public int GetNumberOfRowsForTableView(TableView tableView)
        {
            int cellH = 180;
            (transform.parent as RectTransform).sizeDelta = new Vector2((transform.parent as RectTransform).sizeDelta.x, m_numRows * cellH);
            (transform as RectTransform).sizeDelta = new Vector2((transform as RectTransform).sizeDelta.x, m_numRows * cellH);
            Vector2 pos = transform.localPosition;
            pos.y = (m_numRows * cellH) * -1;
            transform.localPosition = pos;
            return m_numRows;
        }

        //Will be called by the TableView to know what is the height of each row
        public float GetHeightForRowInTableView(TableView tableView, int row)
        {
            return (m_cellPrefab.transform as RectTransform).rect.height;
        }

        //Will be called by the TableView when a cell needs to be created for display
        public TableViewCell GetCellForRowInTableView(TableView tableView, int row)
        {
            ArtifactTableCell cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as ArtifactTableCell;
            if (cell == null)
            {
                //cell = (ArtifactTableCell)GameObject.Instantiate(m_cellPrefab);
                cell = _tableViewCellFactory.Create(m_cellPrefab.gameObject) as ArtifactTableCell;
                sb.Append("VisibleCounterCellInstance_");
                sb.Append((++m_numInstancesCreated).ToString());
                cell.name = sb.ToString();//"VisibleCounterCellInstance_" + (++m_numInstancesCreated).ToString();
                sb.Clear();
            }
            UpdateCellContent(row, cell, false, true);
            cell.SetRowNumber(row);
            return cell;
        }

        #endregion

        #region Table View event handlers
        //Will be called by the TableView when a cell's visibility changed
        public void TableViewCellVisibilityChanged(int row, bool isVisible)
        {
            //Debug.Log(string.Format("Row {0} visibility changed to {1}", row, isVisible));
            if (isVisible)
            {
                ArtifactTableCell cell = (ArtifactTableCell)m_tableView.GetCellAtRow(row);
                cell.NotifyBecameVisible();
            }
        }

        public void UpdateCellContent(int row, ArtifactTableCell cell, bool balanceIncreased, bool forceUpdate = false)
        {
            if (thumbnailIcon == null)
            {
                thumbnailIcon = new Dictionary<string, Sprite>();

                Sprite[] _artifact = Resources.LoadAll<Sprite>("Sprites/overlay_icon_artifact/overlay_icon_artifact");

                for (int i = 0; i < _artifact.Length; i++)
                {
                    thumbnailIcon.Add(_artifact[i].name, _artifact[i]);
                }
            }

            if (row == 0)
            {
                //Gacha cell
                if (cell.afName.gameObject.activeSelf)
                    cell.afName.gameObject.SetActive(false);

                if (cell.effect.gameObject.activeSelf)
                    cell.effect.gameObject.SetActive(false);

                if (cell.damageEffect.gameObject.activeSelf)
                    cell.damageEffect.gameObject.SetActive(false);

                if (!cell.buyButton.activeSelf)
                    cell.buyButton.SetActive(true);

                if (!cell.showAllButton.activeSelf)
                    cell.showAllButton.SetActive(true);

                if (cell.levelUpButton.activeSelf)
                    cell.levelUpButton.SetActive(false);

                if (cell.icon.gameObject.activeSelf)
                {
                    cell.icon.gameObject.SetActive(false);

                    cell.rareLevelStar1.gameObject.SetActive(false);
                    cell.rareLevelStar2.gameObject.SetActive(false);
                    cell.rareLevelStar3.gameObject.SetActive(false);
                }

                if (cell.level.gameObject.activeInHierarchy)
                    cell.level.gameObject.SetActive(false);
            }
            else
            {
                // Level up cell
                if (!cell.afName.gameObject.activeSelf)
                    cell.afName.gameObject.SetActive(true);

                if (!cell.effect.gameObject.activeSelf)
                    cell.effect.gameObject.SetActive(true);

                if (!cell.damageEffect.gameObject.activeSelf)
                    cell.damageEffect.gameObject.SetActive(true);

                if (cell.buyButton.activeSelf)
                    cell.buyButton.SetActive(false);

                if (cell.showAllButton.activeSelf)
                    cell.showAllButton.SetActive(false);

                if (cell.Item != afs[purchasedAfIDs[row - 1]])
                    cell.Item = afs[purchasedAfIDs[row - 1]];

                if (cell.afName.text != LanguageManager.Instance.GetTextValue(cell.Item.ItemId + "_name"))
                    cell.afName.text = LanguageManager.Instance.GetTextValue(cell.Item.ItemId + "_name");

                if (cell.Item.CanGacha())
                {
                    if (cell.Item.GetBalance() == cell.Item.MaxLevel)
                    {
                        cell.levelUpButton.SetActive(false);
                        cell.levelUpButton_block.SetActive(false);
                    }
                    else
                    {
                        if (!cell.levelUpButton.activeSelf)
                        {
                            cell.levelUpButton.SetActive(true);
                            cell.levelUpButton_block.SetActive(true);
                        }

                        if (cell.Item.CanAffordOne())
                        {
                            cell.levelUpButton.GetComponent<Button>().interactable = true;
                            cell.levelUpButton.GetComponent<Image>().sprite =
                                cell.levelUpButton.GetComponent<Button>().spriteState.highlightedSprite;
                        }
                        else
                        {
                            cell.levelUpButton.GetComponent<Button>().interactable = false;
                            cell.levelUpButton.GetComponent<Image>().sprite =
                                cell.levelUpButton.GetComponent<Button>().spriteState.pressedSprite;
                        }
                    }
                }
                else
                {
                    cell.levelUpButton.SetActive(false);
                    cell.levelUpButton_block.SetActive(false);
                }

                cell.level.text = "LV.";
                cell.level.text += (cell.Item.GetBalance() == cell.Item.MaxLevel) ? "Max" : cell.Item.GetBalance().ToString();
                if (!cell.level.gameObject.activeInHierarchy)
                    cell.level.gameObject.SetActive(true);

                cell.levelUpPrice.text = UnitConverter.ConverterDoubleToString(cell.Item.PriceForOne);

                sb.Append(cell.Item.ItemId);
                sb.Append("_skill_desc");
                cell.effect.text = string.Format(LanguageManager.Instance.GetTextValue(sb.ToString()), (cell.Item.EffectValue * 100).ToString());
                sb.Clear();

                cell.damageEffect.text = string.Format(LanguageManager.Instance.GetTextValue("Artifact_all_dmg"), (cell.Item.AllDamageValue * 100).ToString());

                if (!cell.icon.gameObject.activeSelf)
                    cell.icon.gameObject.SetActive(true);

                cell.icon.sprite = thumbnailIcon[cell.Item.ItemId];

                cell.rareLevelStar1.SetActive(cell.Item.RareLevel > 0);
                cell.rareLevelStar2.SetActive(cell.Item.RareLevel > 1);
                cell.rareLevelStar3.SetActive(cell.Item.RareLevel > 2);

                if (cell.Item.RareLevel == 1)
                {
                    cell.rareLevelStar1.transform.localPosition = new Vector3(0, 20, 0);
                }
                else if (cell.Item.RareLevel == 2)
                {
                    cell.rareLevelStar1.transform.localPosition = new Vector3(-30, 20, 0);
                    cell.rareLevelStar2.transform.localPosition = new Vector3(30, 20, 0);
                }
                else if (cell.Item.RareLevel == 3)
                {
                    cell.rareLevelStar1.transform.localPosition = new Vector3(-45, 20, 0);
                    cell.rareLevelStar2.transform.localPosition = new Vector3(0, 20, 0);
                    cell.rareLevelStar3.transform.localPosition = new Vector3(45, 20, 0);
                }
            }
        }

        #endregion

        public void UpdateCellForRowInTableView(TableView tableView, int row, TableViewCell cell, bool balanceIncreased)
        {
            UpdateCellContent(row, (ArtifactTableCell)cell, balanceIncreased);
        }
    }
}
