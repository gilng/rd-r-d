﻿using UnityEngine;
using System.Collections;
using System;
using Zenject;

namespace Ignite.AOM
{
    public class Hero : MonoBehaviour
    {
        private HeroCore _heroCore;

        [Inject]
        public void ConstructorSafeAttribute(HeroCore heroCore)
        {
            _heroCore = heroCore;
        }

        public Animator heroAnimator;
        private int attackStae = Animator.StringToHash("atk");
        private float cd;
        private bool alive = true;
        private bool idle = false;
        private int level = 1;
        private double baseCost;
        private int heroId;
        public Sprite grave;
        private Sprite aliveSprite;
        public ObjectPool effectPool;
        private float attackDuration;
        public GameObject spawnEffect, levelUpEffect, unlockSkillEffect, deadEffect;

        public void TriggerAttack(float duration)
        {
            if (effectPool)
            {
                attackDuration = duration;
                GameObject attackEffect = effectPool.GetPooledObject();
                attackEffect.SetActive(true);
                StartCoroutine(AttackEffectGone(attackEffect));
            }
        }

        IEnumerator AttackEffectGone(GameObject effect)
        {
            yield return new WaitForSeconds(attackDuration);
            effect.SetActive(false);
        }

        IEnumerator Attack()
        {
            //Random start attack delay
            float attackDelay = UnityEngine.Random.Range(0f, 2f);

            yield return new WaitForSeconds(attackDelay);

            while (alive && !idle)
            {
                if (GameManager.Instance.gameStarted)
                {
                    heroAnimator.Play(attackStae);
                    heroAnimator.speed = (float)_heroCore.BoostSpeed;
                    double dps = _heroCore.HeroDPS[heroId - 1];
                    //					if(heroId == 1)
                    //						Debug.Log(" hero : "+ heroId+" dps: "+dps*(cd/_heroCore.BoostSpeed)+" cd: "+(cd/_heroCore.BoostSpeed) );
                    //					yield return new WaitForSeconds(0.2f);
                    if (GameManager.Instance.enemy != null && GameManager.Instance.enemy.IsDamageable && !GameManager.Instance.inPrestige)
                    {
                        GameManager.Instance.enemy.TakeDamage(dps * (cd / _heroCore.BoostSpeed), false);
                    }
                }
                float time = (float)(cd / _heroCore.BoostSpeed);
                yield return new WaitForSeconds(time);
            }
        }

        public void PlaySpawnEffect()
        {
            if (spawnEffect)
            {
                spawnEffect.SetActive(true);
                SoundManager.Instance.PlaySoundWithType(SoundType.HeroSpawn_1);
            }
        }

        public void PlayLevelUpEffect()
        {
            if (levelUpEffect)
            {
                levelUpEffect.SetActive(true);
                SoundManager.Instance.PlaySoundWithType(SoundType.HeroLvUp_1);
            }
        }

        public void PlayUnlockSkillEffect()
        {
            if (levelUpEffect)
                unlockSkillEffect.SetActive(true);
        }

        public void PlayDeadEffect()
        {
            if (deadEffect)
            {
                deadEffect.SetActive(true);
                SoundManager.Instance.PlaySoundWithType(SoundType.hero_killed);
            }
        }

        public void StartAttack(string heroId, double baseCost)
        {
            this.heroId = int.Parse(heroId.Replace("HE", ""));
            this.baseCost = baseCost;
            //			Debug.Log("StartAttack hero : "+ heroId+" baseCost: "+baseCost );
            //			cd = Random.Range (0.8f, 2.0f);
            cd = 2f;
            StartCoroutine(Attack());
        }

        public void SetLevel(int lv)
        {
            level = lv;
        }

        public void Idle()
        {
            heroAnimator.SetBool("he_atk", false);
            idle = true;
        }

        public void Die()
        {
            if (alive)
            {
                alive = false;
                heroAnimator.enabled = false;
                aliveSprite = gameObject.GetComponent<SpriteRenderer>().sprite;
                gameObject.GetComponent<SpriteRenderer>().sprite = grave;
            }
        }

        public void Revive()
        {
            if (!alive)
            {
                alive = true;
                gameObject.GetComponent<SpriteRenderer>().sprite = aliveSprite;
                heroAnimator.enabled = true;
                PlaySpawnEffect();
                StartCoroutine(Attack());
            }
        }
    }
}