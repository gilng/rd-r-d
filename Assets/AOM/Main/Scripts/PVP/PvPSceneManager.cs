﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class PvPSceneManager : MonoBehaviour
    {
        private UIViewLoader _uiViewLoader;
        private IUserService _userService;
        private IPvPChallengeService _pvpChallengeService;
        private SocialManager _socialManager;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader, IUserService userService,
            IPvPChallengeService pvpChallengeService, SocialManager socialManager)
        {
            _uiViewLoader = uiViewLoader;
            _userService = userService;
            _pvpChallengeService = pvpChallengeService;
            _socialManager = socialManager;
        }

        private static PvPSceneManager _instance;

        public static PvPSceneManager Instance
        {
            get { return _instance; }
        }

        private float pvpBattleCountDown = -1;
        private float pvpBattleCountDownTotal = 6;
        public Slider pvpBattleTimeline;

        bool _battleStarted = false;

        public bool BattleStarted
        {
            get { return _battleStarted; }
        }

        private double tapCount = 0;
        private double friendScore = 0;
        public Text tapCountText;
        public Text friendTapCountText;
        public Image tapCountImage;

        private float currentTapCountTextWidth = 0;

        private int ruby = 0;
        private string rubyString;
        public Text rubyText;
        public Image rubyImage;

        private float currentRubyTextWidth = 0;

        public Enemy bossCharacter;

        public EnemyPool bossCharacterPool;

        public GameObject rubyPool;

        private float rubyDropProbability = 12.0f;

        public GameObject loadingOverlay_Group;
        public GameObject bgm;

        public GameObject eff_Battle;
        public Text playerName;
        public Text friendName;

        // Use this for initialization
        void Awake()
        {
            _instance = this;

            Input.multiTouchEnabled = true;

            Time.timeScale = 1f;

            EventManager.OnChallengeScoreBalanceUpdated += RefreshTapScore;

            SoundManager.Instance.bgm = this.bgm.GetComponent<AudioSource>();
            SoundManager.Instance.FixBGM();
            SoundManager.Instance.PlayBgmWithType(SoundType.bgm_pvp);

            ShowLoadingOverlay(true, true);
        }

        void Start()
        {
            CheckLanguageVersion();

            StartCoroutine(InitPvPBoss());
        }

        private float speedCountTimer = 0;
        private int tapCountPerSecond = 0;
        List<int> tapCountPerSecondList = new List<int>();

        void Update()
        {
            if (_battleStarted)
            {
                if (pvpBattleCountDown != -1)
                {
                    pvpBattleCountDown -= Time.deltaTime;

                    speedCountTimer += Time.deltaTime;

                    pvpBattleTimeline.value = pvpBattleCountDown / pvpBattleCountDownTotal;

                    if (pvpBattleCountDown <= 0)
                    {
                        _battleStarted = false;

                        BattleEnd();
                    }

                    if (speedCountTimer >= 1 || !_battleStarted)
                    {
                        tapCountPerSecondList.Add(tapCountPerSecond);
                        tapCountPerSecond = 0;
                        speedCountTimer = 0;
                    }
                }
            }
        }

        void OnDestroy()
        {
            EventManager.OnChallengeScoreBalanceUpdated -= RefreshTapScore;

            SoundManager.Instance.bgm = null;

            Resources.UnloadUnusedAssets();

            System.GC.Collect(0);
        }

        public void CheckLanguageVersion()
        {
            string lang = PlayerPrefs.GetString(LanguageMenuViewController.Language_PP_KEY, "");
            if (lang != "")
            {
                LanguageManager.Instance.ChangeLanguage(lang);
                EventManager.ChangeLanguage();
            }
            else
            {
                if (Application.systemLanguage == SystemLanguage.ChineseTraditional ||
                    Application.systemLanguage == SystemLanguage.ChineseSimplified)
                    LanguageManager.Instance.ChangeLanguage("zh-cht");
                else if (Application.systemLanguage == SystemLanguage.Japanese)
                    LanguageManager.Instance.ChangeLanguage("ja");
                else if (Application.systemLanguage == SystemLanguage.Korean)
                    LanguageManager.Instance.ChangeLanguage("ko");
                else
                    LanguageManager.Instance.ChangeLanguage("en");

                EventManager.ChangeLanguage();
            }
        }

        IEnumerator InitPvPBoss()
        {
            string pvpBossId = "Boss_099";

            bossCharacterPool.LoadMoetanById(pvpBossId, false, () =>
            {
                bossCharacter = (Enemy) bossCharacterPool.currentEnemy.GetComponent(typeof(Enemy));

                if (bossCharacter != null)
                {
                    bossCharacter.gameObject.SetActive(true);
                    bossCharacter.SpawnAnimation();
                }
            });

            yield return StartCoroutine(InitDeadEffect());
        }

        IEnumerator InitDeadEffect()
        {
            bossCharacterPool.InitDeadEffect();

            yield return StartCoroutine(InitBattle());
        }

        private int selectedfriendCount = 0;

        IEnumerator InitBattle()
        {
            ShowLoadingOverlay(false);

            pvpBattleCountDownTotal = (float) (6 + ArtifactAssets.MoeSpirit097.EffectValue * 100);
            pvpBattleCountDown = pvpBattleCountDownTotal;

            pvpBattleTimeline.gameObject.SetActive(true);
            pvpBattleTimeline.value = 1;

            friendScore = _pvpChallengeService.SelectedChallenge != null
                ? _pvpChallengeService.SelectedChallenge.SenderScore
                : 0;

            playerName.text = _userService.CurrentUserSummary.DisplayName;
            friendName.text = _pvpChallengeService.SelectedFriendDisplayName == "pvp_allFriend"
                ? LanguageManager.Instance.GetTextValue(_pvpChallengeService.SelectedFriendDisplayName)
                : _pvpChallengeService.SelectedFriendDisplayName;

            selectedfriendCount =
                _pvpChallengeService.SelectedFriendUsers != null && _pvpChallengeService.SelectedFriendUsers.Count > 0
                    ? _pvpChallengeService.SelectedFriendUsers.Count
                    : 1;

            RefreshTapScore();
            RefreshRubyBalance();

            yield return new WaitForSeconds(0.5f);

            BattleReady();
        }

        public void BattleReady()
        {
            ShowLoadingOverlay(false);

            eff_Battle.SetActive(true);
            eff_Battle.GetComponent<Animator>().Play("Play_Start");
            eff_Battle.GetComponent<Button>().onClick.AddListener(BattleStart);
        }

        void BattleStart()
        {
            eff_Battle.GetComponent<Button>().onClick.RemoveListener(BattleStart);
            eff_Battle.SetActive(false);

            _battleStarted = true;
        }

        void BattleEnd()
        {
            eff_Battle.SetActive(true);
            eff_Battle.GetComponent<Animator>().Play("Play_End");

            StartCoroutine(BattleEnd_AnimeFinish());
        }

        IEnumerator BattleEnd_AnimeFinish()
        {
            yield return new WaitForSeconds(2f);

            BattleEnd_ShowResult();
        }

        void BattleEnd_ShowResult()
        {
            eff_Battle.SetActive(false);

            string _msg = "";

            if (_pvpChallengeService.SelectedChallenge != null)
            {
                _msg = LanguageManager.Instance.GetTextValue((tapCount > friendScore)
                    ? "pvp_result_msg_receiveFriend_win"
                    : "pvp_result_msg_receiveFriend_lose");
            }
            else
            {
                _msg = string.Format(LanguageManager.Instance.GetTextValue("pvp_result_msg_challengeFriend"),
                    friendName.text);
            }

            GameObject resultPopupView = _uiViewLoader.LoadOverlayView("PvPResultPopupOverlay", null, false, false);
            PvPResultViewController resultPopupViewController = resultPopupView.GetComponent<PvPResultViewController>();
            resultPopupViewController.InputResult(_msg, (int) tapCount, ruby, () =>
            {
                if (SocialManager.CheckForInternetConnection())
                {
                    ShowLoadingOverlay(true, true);

                    int maxTapSpeed = tapCountPerSecondList.Max();

                    UserManager.Instance.GiveRuby(ruby, () =>
                    {
                        ruby = 0;

                        if (_pvpChallengeService.SelectedFriendUser != null)
                        {
                            if (_pvpChallengeService.IsSenderInCurrentChallenge)
                            {
                                _pvpChallengeService.ChallengeFriend((int) tapCount, () =>
                                    {
                                        ReportChallengeStatics((int) tapCount, maxTapSpeed);

                                        LoadBattleScene();
                                    },
                                    error =>
                                    {
                                        Debug.Log(error.ToString());

                                        if (error == "TimeOut")
                                        {
                                            ShowTimeOutPopup();
                                        }
                                        else
                                        {
                                            ShowSyncDataErrorPopup();
                                        }
                                    });
                            }
                            else
                            {
                                _pvpChallengeService.AcceptChallenge((int) tapCount, () =>
                                    {
                                        ReportChallengeStatics((int) tapCount, maxTapSpeed);

                                        LoadBattleScene();
                                    },
                                    error =>
                                    {
                                        Debug.Log(error.ToString());

                                        if (error == "TimeOut")
                                        {
                                            ShowTimeOutPopup();
                                        }
                                        else
                                        {
                                            ShowSyncDataErrorPopup();
                                        }
                                    });
                            }
                        }
                        else if (_pvpChallengeService.SelectedFriendUsers != null &&
                                 _pvpChallengeService.SelectedFriendUsers.Count > 0)
                        {
                            _pvpChallengeService.ChallengeMultipleFriends(_pvpChallengeService.SelectedFriendUsers,
                                (int) tapCount, () =>
                                {
                                    ReportChallengeStatics((int) tapCount, maxTapSpeed);

                                    LoadBattleScene();
                                },
                                error =>
                                {
                                    Debug.Log(error.ToString());

                                    if (error == "TimeOut")
                                    {
                                        ShowTimeOutPopup();
                                    }
                                    else
                                    {
                                        ShowSyncDataErrorPopup();
                                    }
                                });
                        }
                        else
                        {
                            int _userScore = (int) tapCount;
                            int _scoreRange = Mathf.FloorToInt(_userScore * 0.15f);
                            int _npcScore = Mathf.Max(100,
                                _userScore + UnityEngine.Random.Range(_scoreRange * -2, _scoreRange));

                            PlayerPrefs.SetInt("PVP_DUMMY_SCORE_USER", _userScore);
                            PlayerPrefs.SetInt("PVP_DUMMY_SCORE_NPC", _npcScore);
                            PlayerPrefs.SetString("PVP_DUMMY_STATUS",
                                ((_npcScore > _userScore) ? PvPStatus.Result_Lose : PvPStatus.Result_Win).ToString());
                            PlayerPrefs.SetString("PVP_DUMMY_CHALLENGE_TIME", DateTime.UtcNow.ToString());

                            LoadBattleScene();
                        }
                    });
                }
                else
                {
                    ShowNoInternetPopup(BattleEnd_ShowResult);
                }
            });
        }

        void ReportChallengeStatics(int tapCount, int maxTapSpeed)
        {
            GamePlayStatManager.Instance.ReachedMaxPvPScore(tapCount);
            if (GamePlayStatManager.Instance.reachedMaxPvPScore)
            {
                _socialManager.ReportMaxPvPScore(tapCount);
                GamePlayStatManager.Instance.reachedMaxPvPScore = false;
            }

            GamePlayStatManager.Instance.ReachedMaxPvPTapSpeed(maxTapSpeed);
            if (GamePlayStatManager.Instance.reachedMaxPvPTapSpeed)
            {
                _socialManager.ReportMaxPvPTapSpeed(maxTapSpeed);
                GamePlayStatManager.Instance.reachedMaxPvPTapSpeed = false;
            }
        }


        public void RefreshTapScore()
        {
            if (BattleStarted)
            {
                tapCount += 1;

                tapCountPerSecond += 1;

                RubyDrop();
            }

            tapCountText.text = tapCount.ToString("000");

            if (_pvpChallengeService.IsSenderInCurrentChallenge)
            {
                friendTapCountText.text = "vs ???";
            }
            else
            {
                tapCountText.color = tapCount > friendScore ? Color.green : Color.red;
                friendTapCountText.text = "vs " + friendScore.ToString("000");
            }

            float tmpTapCountTextWidth = tapCountText.preferredWidth;
            if (tmpTapCountTextWidth != currentTapCountTextWidth)
            {
                Vector3 tapCountImagePosition = tapCountImage.transform.localPosition;
                tapCountImagePosition.x = tapCountText.transform.localPosition.x - (tmpTapCountTextWidth / 2) -
                                          tapCountImage.rectTransform.sizeDelta.x;
                tapCountImagePosition.y = tapCountText.transform.localPosition.y;
                tapCountImage.transform.localPosition = tapCountImagePosition;
                currentTapCountTextWidth = tmpTapCountTextWidth;
            }
        }

        public void RefreshRubyBalance()
        {
            if (BattleStarted)
            {
                ruby += 1;
            }

            rubyString = ruby.ToString();

            if (rubyText.text != rubyString)
            {
                rubyText.text = rubyString;
                float tmpRubyTextWidth = rubyText.preferredWidth;
                if (tmpRubyTextWidth != currentRubyTextWidth)
                {
                    Vector3 rubyImagePosition = rubyImage.transform.localPosition;
                    rubyImagePosition.x = rubyText.transform.localPosition.x - (tmpRubyTextWidth / 2) -
                                          rubyImage.rectTransform.sizeDelta.x;
                    rubyImagePosition.y = rubyText.transform.localPosition.y;
                    rubyImage.transform.localPosition = rubyImagePosition;
                    currentRubyTextWidth = tmpRubyTextWidth;
                }
            }
        }

        public void LoadBattleScene()
        {
            _pvpChallengeService.ChallengeCompleted = true;
            Application.LoadLevel("MainScene");
        }

        void RubyDrop()
        {
            if (UnityEngine.Random.Range(0.0f, 100.0f) <= rubyDropProbability +
                (float) ArtifactAssets.MoeSpirit003.EffectValue)
                rubyPool.GetComponent<RubyPool>().RubyDrop(1 * selectedfriendCount);
        }

        public void ShowLoadingOverlay(bool active, bool isChangeTips = false)
        {
            if (isChangeTips)
                loadingOverlay_Group.GetComponent<TipsManager>().ChangeTips();

            loadingOverlay_Group.SetActive(active);
        }

        public void ShowTimeOutPopup()
        {
            var msgPopupView = _uiViewLoader.LoadOverlayView("MsgPopupOverlay", null, false,
                Time.timeScale == 0);
            var msgPopupViewController = msgPopupView.GetComponent<MsgPopupViewController>();
            msgPopupViewController.InputMsg(LanguageManager.Instance.GetTextValue("saveLoadWarning_title"),
                LanguageManager.Instance.GetTextValue("errorMsg_TimeOut"), "btn_ok", LoadBattleScene);
        }

        public void ShowSyncDataErrorPopup()
        {
            var msgPopupView = _uiViewLoader.LoadOverlayView("MsgPopupOverlay", null, false,
                Time.timeScale == 0);
            var msgPopupViewController = msgPopupView.GetComponent<MsgPopupViewController>();
            msgPopupViewController.InputMsg(LanguageManager.Instance.GetTextValue("saveLoadWarning_title"),
                LanguageManager.Instance.GetTextValue("pvp_errorMsg_LoadParseFail"), "btn_ok", LoadBattleScene);
        }

        public void ShowNoInternetPopup(Action retryCallback)
        {
            var msgPopupView = _uiViewLoader.LoadOverlayView("MsgPopupOverlay", null, false, Time.timeScale == 0);
            var msgPopupViewController = msgPopupView.GetComponent<MsgPopupViewController>();
            msgPopupViewController.InputMsg(LanguageManager.Instance.GetTextValue("saveLoadWarning_title"),
                LanguageManager.Instance.GetTextValue("pvp_errorMsg_NoInternet"), "dropbox_errorMsg_Btn_Retry",
                retryCallback);
        }
    }
}