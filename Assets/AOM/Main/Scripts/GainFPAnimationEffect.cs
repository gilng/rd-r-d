﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class GainFPAnimationEffect : MonoBehaviour
    {
        public Animator animator;

        private Vector2 startPos;
        private Vector2 controlPoint;
        private Vector2 endPos;

        private bool isPlaying = false;

        private float timer;
        private float curveX, curveY;

        private float endTime = 1.1f;


        public void InitEffect(Vector3 _startPos, Vector3 _endPos, Vector2 _controlPoint)
        {
            startPos = new Vector2(_startPos.x, _startPos.y);
            endPos = new Vector2(_endPos.x, _endPos.y);

            controlPoint = _controlPoint;

            transform.position = new Vector3(startPos.x, startPos.y, 0);
            transform.localScale = Vector3.one;

            timer = 0;

            isPlaying = true;

            animator.Play("Stay");
        }

        void EndEffect()
        {
            isPlaying = false;

            animator.Play("End");

            StartCoroutine(EndEffect_finishAnime());
        }

        IEnumerator EndEffect_finishAnime()
        {
            yield return new WaitForSeconds(endTime);

            Destroy(this.gameObject);
        }

        // Update is called once per frame
        void Update()
        {
            if (isPlaying)
            {
                timer += Time.fixedDeltaTime * 5;

                curveX = (((1 - timer) * (1 - timer)) * startPos.x) + (2 * timer * (1 - timer) * controlPoint.x) +
                         ((timer * timer) * endPos.x);
                curveY = (((1 - timer) * (1 - timer)) * startPos.y) + (2 * timer * (1 - timer) * controlPoint.y) +
                         ((timer * timer) * endPos.y);

                if (timer < endTime)
                {
                    transform.position = new Vector3(curveX, curveY, 0);
                }
                else
                {
                    EndEffect();
                }
            }
        }
    }
}