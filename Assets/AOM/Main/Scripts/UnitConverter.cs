﻿using UnityEngine;
using System;
using System.Collections;

namespace Ignite.AOM
{
	public class UnitConverter : MonoBehaviour
	{

//		private static string[] units = {"", "万", "億", "兆", "京", "垓", "DD", "穣", "溝", "澗", "正", "載", "極", "恒河沙", "阿僧祇", "那由他", "不可思議", "無量大数"};
//
//		public static string ConverterDoubleToString(double value) {
//
////			Debug.Log(value);
//			double result = value;
//			int unit = 0;
//
//			if(value >= 10000)
//			{
////				result /= 10;
//				while (result / 10000 >= 1)
//				{
//					//				Debug.Log(unit + " - " + result);
//					unit++;
//					result /= 10000;
//				}
//			}
//
////			Debug.Log(unit + " - " + result);
//
//			if(unit == 0)
//				return (Math.Truncate(result * 100) / 100).ToString("0.00");
//			else if (unit < units.Length)
//				return (Math.Truncate(result * 100) / 100).ToString("0.00") + units[unit];
//			else
//				return value.ToString("E2");
//		}

		private static string[] units = {"", "K", "M", "B", "T", "aa", "bb", "cc", "dd", "ee", "ff", "gg",
											"hh", "ii", "jj", "kk", "ll", "mm", "nn", "oo", "pp", "qq", "rr", 
											"ss", "tt", "uu", "vv", "ww", "xx", "yy", "zz"};
		private static System.Text.StringBuilder sb = new System.Text.StringBuilder ();

		public static string ConverterDoubleToString (double value)
		{
//			Debug.Log(value);
			double result = value;
			int unit = 0;
			bool navigateVal = value < 0;
			if (navigateVal) {
				result *= -1;
				value *= -1;
			}
			if (value >= 1000) {
				while (result / 1000 >= 1) {
					unit++;
					result /= 1000;
				}
			}

			if(!navigateVal)
			{
				if (unit == 0) {
				    if (result % 1 == 0)
				    {
				        return result.ToString();
				    }
				    else
				    {
//				        if (unit == 0)
//				        {
//				            return  Mathf.RoundToInt((float)result).ToString();
//				        }
//				        else
//				        {
				            return (Math.Truncate (result * 100) / 100).ToString ("0.00");
//				        }
				    }
				} else if (unit < units.Length) {
					sb.Clear ();
					sb.Append (((float)result).ToString ("0.00"));
					sb.Append (units [unit]);
					return sb.ToString ();
				} else
					return value.ToString ("E2");
			}
			else
			{
				sb.Clear ();
				sb.Append("-");
				if (unit == 0) {
					if (result % 1 == 0)
						sb.Append(result.ToString());
					else
						sb.Append((Math.Truncate (result * 100) / 100).ToString ("0.00"));
				} else if (unit < units.Length) 
				{
					sb.Append ((Math.Truncate (result * 100) / 100).ToString ("0.00"));
					sb.Append (units [unit]);
				}
				else
					sb.Append(value.ToString ("E2"));
				return sb.ToString ();
			}
		}

		public static double ConvertToTimestamp (DateTime value)
		{
			//create Timespan by subtracting the value provided from
			//the Unix Epoch
			TimeSpan span = (value - new DateTime (1970, 1, 1, 0, 0, 0, 0).ToLocalTime ());
			
			//return the total seconds (which is a UNIX timestamp)
			return (double)span.TotalSeconds;
		}

		public static DateTime ConvertToDateTime (double timeStamp){
			return new DateTime (1970, 1, 1, 0, 0, 0, 0).AddSeconds(timeStamp).ToLocalTime();
		}

		public static string SecondsToTime(double totalSeconds){
			int hours = (int)((int)totalSeconds/(60*60)) ;
			int mins = ((int)totalSeconds - (60*60* hours))/60;
			int seconds = ((int)totalSeconds - (60*60* hours) - (60*mins));
			return string.Format("{0:D2}:{1:D2}:{2:D2}",hours,mins,seconds);
		}

        public static string SecondsToMinsAndSeconds(double totalSeconds)
        {
            int mins = ((int)totalSeconds - (60 * 60 * ((int)totalSeconds / (60 * 60)))) / 60;
            int seconds = ((int)totalSeconds - (60 * 60 * ((int)totalSeconds / (60 * 60))) - (60 * mins));
            return string.Format("{0:D2}:{1:D2}", mins, seconds);
        }
	}
}
