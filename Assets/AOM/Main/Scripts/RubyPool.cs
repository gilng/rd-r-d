﻿using UnityEngine;
using System.Collections;
using Zenject;

namespace Ignite.AOM
{
    public class RubyPool : MonoBehaviour
    {
        private ObjectPool rubyPool;

        // Use this for initialization
        void Start()
        {
            rubyPool = GetComponent<ObjectPool>();
        }

        public void RemoveAllRuby()
        {
            rubyPool.CollectAllObjects();
        }

        public void RubyDrop(int amount)
        {
            int numberOfRuby = amount;

            for (int i = 0; i < numberOfRuby; i++)
            {
                GameObject rubyGo = rubyPool.GetPooledObject();

                RubyDrop rd = rubyGo.GetComponent<RubyDrop>();

                rd.ForceReturnDroppedGold();

                if (PvPSceneManager.Instance != null)
                    PvPSceneManager.Instance.RefreshRubyBalance();

                rubyGo.SetActive(true);

                rd.StartDrop(0f,
                             1.25f,
                             -2.5f,
                             2.5f,
                             6f,
                             3.5f,
                             PvPSceneManager.Instance.rubyImage.rectTransform.transform.position.x,
                             PvPSceneManager.Instance.rubyImage.rectTransform.transform.position.y,
                             amount / numberOfRuby,
                             true,
                             false);
            }
        }
    }
}
