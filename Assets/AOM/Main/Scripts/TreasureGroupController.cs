﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Zenject;

namespace Ignite.AOM
{
    public class TreasureGroupController : MonoBehaviour
    {
        private EnemyCore _enemyCore;

        [Inject]
        public void ConstructorSafeAttribute(EnemyCore enemyCore)
        {
            _enemyCore = enemyCore;
        }

        public Sprite spriteBoxClose;
        public Sprite spriteBoxOpen;

        List<GameObject> boxList, boxList_Opened, boxDisplayList;
        List<float> boxSpeed;

        bool endAnime = false;

        float boxGoalPosY;

        void Start()
        {

            boxList = new List<GameObject>();
            boxList_Opened = new List<GameObject>();
            boxDisplayList = new List<GameObject>();

            boxSpeed = new List<float>();

            this.gameObject.SetActive(false);

            EventManager.OnHiddenMenuShow += ChangeFairyPosition;
        }

        void OnDestroy()
        {
            EventManager.OnHiddenMenuShow -= ChangeFairyPosition;
        }

        void ChangeFairyPosition(bool isShow)
        {
            if (isShow)
            {
                this.gameObject.transform.localPosition = new Vector3(0, -Background.instance.mountainAMove, 0);

                boxGoalPosY = Background.instance.mountainAMove;
            }
            else
            {
                this.gameObject.transform.localPosition = new Vector3(0, 0, 0);

                boxGoalPosY = 0;
            }
        }

        public void FairyGroupStart()
        {
            this.gameObject.SetActive(true);
            this.GetComponent<Animator>().SetInteger("AnimeSelect", 1);

            endAnime = false;
        }

        public void FairyGroupLeave()
        {
            this.GetComponent<Animator>().SetInteger("AnimeSelect", 0);
            this.GetComponent<Animator>().Play("FairyTreasureRain_Stay");

            for (int i = 0; i < boxDisplayList.Count; i++)
            {
                boxDisplayList[i].SetActive(true);
            }

            endAnime = true;
        }

        public void FairyDropBox(string boxID)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.TreasureCollect_1);

            GameObject fairy = this.gameObject.transform.FindChild("FairyGroup/Side_" + boxID[0] + "/Fairy_" + boxID).gameObject;
            GameObject box = this.gameObject.transform.FindChild("BoxGroup/Box_" + boxID).gameObject;
            GameObject boxDisplay = fairy.transform.FindChild("Box_Display").gameObject;

            boxDisplayList.Add(boxDisplay);

            boxDisplay.SetActive(false);

            if (box.GetComponent<SpriteRenderer>().sprite != spriteBoxClose)
                box.GetComponent<SpriteRenderer>().sprite = spriteBoxClose;

            box.SetActive(true);
            box.transform.localPosition = (boxID[0] == 'L') ? new Vector3(fairy.transform.localPosition.x * -1, fairy.transform.localPosition.y - 0.5f, fairy.transform.localPosition.z) : new Vector3(fairy.transform.localPosition.x, fairy.transform.localPosition.y - 0.5f, fairy.transform.localPosition.z);

            if (boxList.Contains(box) == false)
            {
                boxList.Add(box);
                boxSpeed.Add(0);
            }
        }

        void Update()
        {

            if (boxList.Count > 0)
            {
                for (int i = 0; i < boxList.Count; i++)
                {
                    if (boxList_Opened.Contains(boxList[i]) == false)
                    {
                        if (boxList[i].transform.localPosition.y > 0.6f + boxGoalPosY)
                        {
                            boxSpeed[i] += Time.deltaTime * -1 * 0.125f;
                            boxList[i].transform.Translate(0, boxSpeed[i], 0);
                        }
                        else
                        {
                            if (boxList[i].GetComponent<SpriteRenderer>().sprite != spriteBoxOpen)
                            {
                                boxList[i].GetComponent<SpriteRenderer>().sprite = spriteBoxOpen;
                                //								GameManager.instance.goldPool.GetComponent<GoldPool>().GoldDropFromFairy((Formulas.EnemyGoldDropForLevel(_enemyCore.Stage,EnemyType.Normal) * 5000) / 16, boxList[i].transform.position.x, -3.0f);

                                GameManager.Instance.goldPool.GetComponent<GoldPool>().GoldDropFromFairy((Formulas.EnemyGoldDropForLevel(_enemyCore.Stage, EnemyType.Normal) * 5000) / 16, boxList[i].transform.position.x, boxList[i].transform.position.y);
                            }
                            else
                            {
                                SpriteRenderer boxSprite = boxList[i].GetComponent<SpriteRenderer>();
                                Color boxColor = boxSprite.color;

                                if (boxColor.a > 0)
                                {
                                    boxColor.a -= Time.deltaTime * 0.8f;
                                    boxSprite.color = boxColor;
                                }
                                else
                                {
                                    boxList_Opened.Add(boxList[i]);
                                }
                            }
                        }
                    }
                }
            }

            if (boxList_Opened.Count == boxList.Count && endAnime)
            {
                this.gameObject.SetActive(false);

                for (int i = 0; i < boxList.Count; i++)
                {
                    SpriteRenderer boxSprite = boxList[i].GetComponent<SpriteRenderer>();
                    Color boxColor = boxSprite.color;

                    boxColor.a = 1;
                    boxSprite.color = boxColor;
                    boxSprite.sprite = spriteBoxClose;

                    boxList[i].transform.position = new Vector3(0, 12, 0);
                    boxList[i].SetActive(false);
                }

                boxList.Clear();
                boxList_Opened.Clear();
                boxDisplayList.Clear();
                boxSpeed.Clear();
            }
        }

    }
}