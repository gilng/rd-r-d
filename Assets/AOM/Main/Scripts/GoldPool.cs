﻿using UnityEngine;
using System.Collections;
using Zenject;

namespace Ignite.AOM
{
    public class GoldPool : MonoBehaviour
    {
        private EnemyCore _enemyCore;

        [Inject]
        public void ConstructorSafeAttribute(EnemyCore enemyCore)
        {
            _enemyCore = enemyCore;
        }

        private ObjectPool goldPool;
        private ObjectPool textObjectPool;

        private float dropStartPointX;
        private float dropStartPointY;
        private float dropControlPointXLeft;
        private float dropControlPointXRight;
        private float dropControlPointYUp;
        private float dropControlPointYDown;
        private double dropAmount;
        private int numberOfCoin;

        private int currentNumberOfCoin;
        private float releaseTime = 0.002f;
        private float runningTime = 0f;
        private bool dropping = false;

        private bool directReturning = false;

        void Awake()
        {
            EventManager.OnPrestige += RemoveAllGold;
        }

        void Start()
        {
            goldPool = GetComponent<ObjectPool>();
            //			textObjectPool=(ObjectPool)GameObject.Find("DamageTextLayer").GetComponent<ObjectPool>();
            textObjectPool = GameManager.Instance.coinTextObjectPool;
        }

        public void RemoveAllGold()
        {
            goldPool.CollectAllObjects();
            GoldBalanceEventBuffer.instance.RemoveAllValue();
        }

        void OnDestroy()
        {
            EventManager.OnPrestige -= RemoveAllGold;
        }

        public void GoldDrop(double amount)
        {
            int numberOfCoin;
            if (amount < 3)
            {
                numberOfCoin = 1;
            }
            else if (amount % 3 == 0)
            {
                numberOfCoin = 3;
            }
            else if (amount % 4 == 0)
            {
                numberOfCoin = 4;
            }
            else if (amount % 5 == 0)
            {
                numberOfCoin = 5;
            }
            else
            {
                numberOfCoin = 3;
            }

            if (_enemyCore.CurrentEnemyType == EnemyType.TreasureChest)
                numberOfCoin = 20;

            for (int i = 0; i < numberOfCoin; i++)
            {
                GameObject goldGo = goldPool.GetPooledObject();

                GoldDrop gd = goldGo.GetComponent<GoldDrop>();

                gd.ForceReturnDroppedGold();

                goldGo.SetActive(true);

                gd.TextObjectPool = textObjectPool;
                gd.StartDrop(0f,
                             2f,
                             -1.5f,
                             1.5f,
                             3.8f,
                             3.8f,
                             GameManager.Instance.goldImage.rectTransform.transform.position.x,
                             GameManager.Instance.goldImage.rectTransform.transform.position.y,
                             amount / numberOfCoin);
            }
        }

        public void GoldDropFromFairy(double amount, float boxPositionX, float boxPositionY)
        {
            int numberOfCoin = 5;
            for (int i = 0; i < numberOfCoin; i++)
            {
                GameObject goldGo = goldPool.GetPooledObject();
                goldGo.SetActive(true);
                GoldDrop gd = goldGo.GetComponent<GoldDrop>();
                gd.TextObjectPool = textObjectPool;
                gd.StartDrop(boxPositionX,
                             boxPositionY + 0.25f,
                             boxPositionX - 1.5f,
                             boxPositionX + 1.5f,
                             boxPositionY + 3.8f,
                             boxPositionY + 3.8f,
                             GameManager.Instance.goldImage.rectTransform.transform.position.x,
                             GameManager.Instance.goldImage.rectTransform.transform.position.y,
                             amount / numberOfCoin);
            }
        }

        public void OfflineGoldDrop()
        {
            numberOfCoin = Random.Range(8, 11);

            dropStartPointX = GameManager.Instance.offlineGoldDropBtn.GetComponent<RectTransform>().transform.position.x;
            dropStartPointY = GameManager.Instance.offlineGoldDropBtn.GetComponent<RectTransform>().transform.position.y;

            runningTime = 0f;
            currentNumberOfCoin = 0;

            dropping = true;
            directReturning = true;
        }

        void Update()
        {
            if (dropping)
            {
                runningTime += Time.deltaTime;
                if (runningTime >= releaseTime)
                {
                    GameObject goldGo = goldPool.GetPooledObject();
                    GoldDrop gd = goldGo.GetComponent<GoldDrop>();
                    gd.TextObjectPool = textObjectPool;
                    if (directReturning)
                    {
                        gd.CallToReturn(dropStartPointX,
                                        dropStartPointY,
                                        Random.Range(dropStartPointX * 0.5f, dropStartPointX * 2f),
                                        Random.Range(dropStartPointY * 0.5f, dropStartPointY * 2f),
                                        GameManager.Instance.goldImage.rectTransform.transform.position.x,
                                        GameManager.Instance.goldImage.rectTransform.transform.position.y);
                    }
                    else
                    {
                        gd.StartDrop(dropStartPointX,
                                     dropStartPointY,
                                     dropControlPointXLeft,
                                     dropControlPointXRight,
                                     dropControlPointYUp,
                                     dropControlPointYDown,
                                     GameManager.Instance.goldImage.rectTransform.transform.position.x,
                                     GameManager.Instance.goldImage.rectTransform.transform.position.y,
                                     (directReturning) ? 0 : (dropAmount / numberOfCoin));
                    }
                    goldGo.SetActive(true);
                    runningTime = 0;
                    currentNumberOfCoin++;
                }
                if (currentNumberOfCoin >= numberOfCoin)
                {
                    dropping = false;
                    directReturning = false;
                    runningTime = 0f;
                    currentNumberOfCoin = 0;
                    numberOfCoin = 0;
                }
            }


        }

    }
}