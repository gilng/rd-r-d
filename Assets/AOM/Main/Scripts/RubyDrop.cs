﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Ignite.AOM
{
    public class RubyDrop : CurrencyDrop
    {
        public GameObject child;
        public SpriteRenderer spRenderer;

        private float leavingTimer = 0;
        private Color color = new Color(1, 1, 1, 1);

        protected override void PauseReturn()
        {
            returning = false;

            if (Random.Range(0, 100) < 60)
            {
                child.SetActive(true);
                child.transform.rotation = Quaternion.AngleAxis(Random.Range(0f, 360f), Vector3.zero);
                child.GetComponent<Animator>().Play("Ruby_Shine");
            }

            // Pause a period...
            StartCoroutine(DisappearCD());
        }

        IEnumerator DisappearCD()
        {
            yield return new WaitForSeconds(3f);

            customFeatureRun = true;
        }

        protected override void CustomFeature()
        {
            base.CustomFeature();

            leavingTimer += Time.deltaTime;

            if (leavingTimer > 0.2)
            {
                customFeatureRun = false;
                leavingTimer = 0;
                color.a = 1;
                spRenderer.color = color;
                child.SetActive(false);
                gameObject.SetActive(false);
            }
            else
            {
                Vector3 v3 = this.transform.localPosition;

                v3.y += leavingTimer;

                this.transform.localPosition = v3;
                this.transform.rotation = Quaternion.Euler(0, 0, Random.Range(360, 720));

                color.a = 1 - leavingTimer * 5;
                spRenderer.color = color;
            }
        }

        protected override void AfterReturn()
        {
        }

        protected override void OnCollect()
        {
        }
    }
}