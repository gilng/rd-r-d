using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
	[RequireComponent(typeof(ParticleSystem))]
	public class CFX_AutoDestructShuriken2 : MonoBehaviour
	{
		public bool OnlyDeactivate;
		
		void OnEnable()
		{
			StartCoroutine("CheckIfAlive");
		}

		void OnDisable()
		{			
			if (this.gameObject.activeInHierarchy == false)
			{
				if (this.gameObject.name == "Levelup_effect") {
					this.gameObject.SetActive(false);
				}
			}
		}
		
		IEnumerator CheckIfAlive ()
		{
			while(true)
			{
				yield return new WaitForSeconds(0.05f);
				if(!GetComponent<ParticleSystem>().IsAlive(false))
				{
					this.gameObject.SetActive(false);
				}
			}

			while(true)
			{
				yield return new WaitForSeconds(0.5f);
				if(!GetComponent<ParticleSystem>().IsAlive(true))
				{
					if(OnlyDeactivate)
					{
						#if UNITY_3_5
							this.gameObject.SetActiveRecursively(false);
						#else
							this.gameObject.SetActive(false);
						#endif
					}
					else
						GameObject.Destroy(this.gameObject);
					break;
				}
			}
		}
	}
}
