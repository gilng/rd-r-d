﻿using System;
using UnityEngine;
using System.Collections;
using AssetBundles;
using UnityEngine.UI;
using System.Collections.Generic;
using SmartLocalization;

namespace Ignite.AOM
{
    public class AssetBundleLoader : MonoBehaviour
    {
        private static AssetBundleLoader _instance;
        public static AssetBundleLoader Instance
        {
            get
            {
                return _instance;
            }
        }

        public string[] assetBundleList
        {
            get
            {
                return AOMAssetBundlesConstant.ASSET_BUNDLE_LIST;
            }
        }

        string m_BaseDownloadingURL = "";
        public string BaseDownloadingURL
        {
            get { return m_BaseDownloadingURL; }
            set { m_BaseDownloadingURL = value; }
        }

        WWW m_DownloadingWWW;

        Dictionary<string, AssetBundle> m_LoadedAssetBundles = new Dictionary<string, AssetBundle>();

        static GameObject m_loadedAssetObj;
        public static GameObject LoadedAssetObj
        {
            get { return m_loadedAssetObj; }
            set { m_loadedAssetObj = value; }
        }

        //public GameObject downloadingOverlay;

        bool inited = false;

        void Awake()
        {
            _instance = this;

            // Don't destroy this gameObject as we depend on it to run the loading script.
            DontDestroyOnLoad(this.gameObject);
        }

        void Update()
        {
            if (inited)
            {
                if (m_LoadedAssetBundles.Count + 1 == assetBundleList.Length)
                {
                    EventManager.AssetBundleDownloadCallback(true);
                }
                else
                {
                    if (m_DownloadingWWW != null)
                        EventManager.UpdateAssetDownloadProgress(m_LoadedAssetBundles.Count + 1, m_DownloadingWWW.progress);
                }
            }
        }

        public void InitAssetBundle()
        {
            EventManager.VerfiyAssetBundleVersion();

            StartCoroutine(Initialize());

            inited = true;
        }

        // Initialize the downloading url and AssetBundleManifest object.
        protected IEnumerator Initialize()
        {
#if UNITY_ANDROID || UNITY_IOS
            SetSourceAssetBundleURL("http://aomcdn.ignite-ga.me/asset_bundle/AssetBundles/");
#elif UNITY_EDITOR
            SetSourceAssetBundleURL("https://s3-ap-southeast-1.amazonaws.com/attack-on-moe/asset_bundle/AssetBundles/");
#endif

            // Init Bundle of Content Data
            for (int i = 1; i < assetBundleList.Length; i++)
            {
                yield return StartCoroutine(InstantiateBundlePack(assetBundleList[i]));
            }
        }

        public void SetSourceAssetBundleURL(string absolutePath)
        {
            BaseDownloadingURL = absolutePath + Utility.GetPlatformName() + "/";
        }

        protected IEnumerator InstantiateBundlePack(string assetBundleName)
        {
#if UNITY_EDITOR
            // This is simply to get the elapsed time for this phase of AssetLoading.
            float startTime = Time.realtimeSinceStartup;
#endif

            // Load asset from assetBundle.
            yield return StartCoroutine(DownloadAndCacheBundle(assetBundleName));

#if UNITY_EDITOR
            // Calculate and display the elapsed time.
            float elapsedTime = Time.realtimeSinceStartup - startTime;
            Debug.Log(assetBundleName + " was loaded successfully in " + elapsedTime + " seconds");
#endif
        }

        IEnumerator DownloadAndCacheBundle(string assetBundleName)
        {
            string path = BaseDownloadingURL + assetBundleName;

#if UNITY_IOS && !UNITY_EDITOR
            UnityEngine.iOS.Device.SetNoBackupFlag(path);
#endif
            // Wait for the Caching system to be ready
            while (!Caching.ready)
                yield return null;

            // Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
            using (WWW www = WWW.LoadFromCacheOrDownload(path, 0))
            {
                m_DownloadingWWW = www;
                yield return www;

                m_DownloadingWWW = null;

                if (www.error != null)
                {
                    inited = false;

                    EventManager.AssetBundleDownloadCallback(false);
//#if UNITY_EDITOR
//                  throw new Exception("WWW download had an error:" + www.error);
//#endif
                }
                else
                {
                    AssetBundle bundle = www.assetBundle;
                    m_LoadedAssetBundles.Add(assetBundleName, bundle);

                    //bundle.Unload(false);
                }

                yield return null;
            } // memory is freed from the web stream (www.Dispose() gets called implicitly)
        }

        public void LoadAsset(string assetBundleName, string assetName, Action callback)
        {
            StartCoroutine(InstantiateGameObjectAsync(assetBundleName, assetName, callback));
        }

        protected IEnumerator InstantiateGameObjectAsync(string assetBundleName, string assetName, Action callback)
        {
#if UNITY_EDITOR
            // This is simply to get the elapsed time for this phase of AssetLoading.
            float startTime = Time.realtimeSinceStartup;
#endif

            // Load asset from assetBundle.
            if (assetName != "")
            {
                // Get the asset.
                AssetBundle bundle;
                m_LoadedAssetBundles.TryGetValue(assetBundleName, out bundle);

                if (bundle != null)
                {
                    GameObject prefab = bundle.LoadAsset(assetName) as GameObject;

                    if (prefab != null)
                    {
                        LoadedAssetObj = GameObject.Instantiate(prefab);
                    }
                }
            }

#if UNITY_EDITOR
            // Calculate and display the elapsed time.
            float elapsedTime = Time.realtimeSinceStartup - startTime;
            Debug.Log(assetName + (LoadedAssetObj == null ? " was not" : " was") + " loaded successfully in " + elapsedTime + " seconds");
#endif

            yield return null;

            callback();
        }

        public void RemoveLoadedAsset()
        {
            LoadedAssetObj = null;
        }

        public void LoadSpriteAsset(string assetBundleName, string assetName, SpriteRenderer targetSpriteObj, Action callback)
        {
            StartCoroutine(InstantiateSpriteObjectAsync(assetBundleName, assetName, targetSpriteObj, callback));
        }

        protected IEnumerator InstantiateSpriteObjectAsync(string assetBundleName, string assetName, SpriteRenderer targetSpriteObj, Action callback)
        {
#if UNITY_EDITOR
            // This is simply to get the elapsed time for this phase of AssetLoading.
            float startTime = Time.realtimeSinceStartup;
#endif

            if (assetName != "")
            {
                // Get the asset.
                AssetBundle bundle;
                m_LoadedAssetBundles.TryGetValue(assetBundleName, out bundle);

                if (bundle != null)
                {
                    Sprite sprite = bundle.LoadAsset<Sprite>(assetName);

                    if (sprite != null)
                    {
                        targetSpriteObj.sprite = sprite;
                    }
                }
            }

#if UNITY_EDITOR
            // Calculate and display the elapsed time.
            float elapsedTime = Time.realtimeSinceStartup - startTime;
            Debug.Log(assetName + (targetSpriteObj.sprite == null ? " was not" : " was") + " loaded successfully in " + elapsedTime + " seconds");
#endif

            yield return null;

            callback();
        }

        public void LoadImageAsset(string assetBundleName, string assetName, Image targetImageObj)
        {
            StartCoroutine(InstantiateImageObjectAsync(assetBundleName, assetName, targetImageObj));
        }

        protected IEnumerator InstantiateImageObjectAsync(string assetBundleName, string assetName, Image targetImageObj)
        {
#if UNITY_EDITOR
            // This is simply to get the elapsed time for this phase of AssetLoading.
            float startTime = Time.realtimeSinceStartup;
#endif
            if (assetName != "")
            {
                // Get the asset.
                AssetBundle bundle;
                m_LoadedAssetBundles.TryGetValue(assetBundleName, out bundle);

                if (bundle != null)
                {
                    Sprite sprite = bundle.LoadAsset<Sprite>(assetName);

                    if (sprite != null)
                    {
                        targetImageObj.sprite = sprite;
                    }
                }
            }

#if UNITY_EDITOR
            // Calculate and display the elapsed time.
            float elapsedTime = Time.realtimeSinceStartup - startTime;
            Debug.Log(assetName + (targetImageObj.sprite == null ? " was not" : " was") + " loaded successfully in " + elapsedTime + " seconds");
#endif

            yield return null;
        }
    }
}
