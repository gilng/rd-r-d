using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using System;
using System.Collections;
using System.Collections.Generic;
using Zenject;
using Parse;
using System.Threading.Tasks;
using System.Linq;
using Ignite.AOM.Event;

namespace Ignite.AOM
{
    public class GameManager : MonoBehaviour
    {
        private VersionCheckerManager _versionCheckerManager;
        private SocialManager _socialManager;
        private CloudDataManager _cloudDataManager;

        private IUserService _userService;
        private IPvPChallengeService _pvpChallengeService;

        private UIViewLoader _uiViewLoader;

        private AvatarCore _avatarCore;
        private HeroCore _heroCore;
        private EnemyCore _enemyCore;
        private ArtifactCore _artifactCore;

        private GalleryCore _galleryCore;

        private GoldDropCore _goldDropCore;

        [Inject]
        public void ConstructorSafeAttribute(VersionCheckerManager versionCheckerManager, SocialManager socialManager,
            CloudDataManager cloudDataManager, IUserService userService, IPvPChallengeService pvpChallengeService,
            UIViewLoader uiViewLoader, AvatarCore avatarCore, HeroCore heroCore,
            EnemyCore enemyCore, ArtifactCore artifactCore, GalleryCore galleryCore, GoldDropCore goldDropCore)
        {
            _versionCheckerManager = versionCheckerManager;
            _socialManager = socialManager;
            _cloudDataManager = cloudDataManager;

            _userService = userService;
            _pvpChallengeService = pvpChallengeService;

            _uiViewLoader = uiViewLoader;

            _avatarCore = avatarCore;
            _heroCore = heroCore;
            _enemyCore = enemyCore;
            _artifactCore = artifactCore;

            _galleryCore = galleryCore;

            _goldDropCore = goldDropCore;
        }

        private static GameManager _instance;

        public static GameManager Instance
        {
            get { return _instance; }
        }

        public Enemy enemy;
        public GameObject enemyObject;
        public Text currentStageText;
        public Text currentWaveText;
        public Text currentWorldNameText;
        private double gold;
        private string goldString;
        public Text goldText;
        public Image goldImage;
        private double diamond;
        private string diamondString;
        public Text diamondText;
        public Image diamondImage;
        public Relic relic;
        private bool relicDropped;
        public GameObject breakScene;
        public GameObject blockScene;
        public GameObject breakCharPosition;
        public SpriteRenderer breakSceneChar;
        public GameObject loadingScene;
        public GameObject goldPool;
        public GameObject diamondPool;
        public GameObject offlineGoldDropBtn;
        public GameObject fullTouchCreditBtn;
        public TreasureController fairyTreasureBoxBtn;
        public GameObject fairyTreasureGroup;
        public GameObject diamondIcon;
        public GameObject diamondIconBalance;
        public GameObject relicIcon;
        public GameObject relicIconBalance;
        public GameObject relicForAnimation;
        public int relicEarnedFromPrestige = 2;

        private bool _gameReady;

        public bool GameReady
        {
            get { return _gameReady; }
            set
            {
                _gameReady = value;
                if (_gameReady)
                {
                    GameStateEvents.OnGameReady();
                }
            }
        }

        public bool gameStarted = false;
        public Text bossFightCountDownText;
        public Button bossFightButton;
        private float bossFightCountDown = -1;
        public Slider bossFightTimeline;
        private Vector3 bossTypeStartPos;
        private Vector3 bossTypeEndPos;
        public GameObject enemyHPTextObject;
        public GameObject sliderObject;
        public GameObject enemyName;
        public ObjectPool dmgTextObjectPool;
        public ObjectPool coinTextObjectPool;
        public EnemyPool enemyPool;
        private float currentGoldTextWidth = 0;
        private float currentDiamondTextWidth = 0;
        private float fairyAppearCountDown = 15.0f;
        private bool isFairyAppearing = false;
        private float baseFairyAppearChance = 10.0f;
        public GameObject sprite;
        public GameObject prestigeEffect;
        public GameObject menu;
        public bool inPrestige;
        public DateTime gameStartTime;
        public Button settingButton;
        public Button galleryButton;
        public Button achievementButton;
        public Button leaderBoardButton;
        public Button friendsButton;

        public BossTextEffectManager bossTextEffect;

        public GameObject bgm;

        private System.Text.StringBuilder sb = new System.Text.StringBuilder();

        private double lastTouch;
        private int touchCredit;
        private int maxTouchCredit;
        private int creditRechangeTime = 3600;

        public GameObject rechargeAnime;

        //float autoSaveTime = 60;

        float promotionTime = 900, promotionTimer = 900;
        public bool showingPromotionBoard = false;

        bool autoSaving = false;

        public Text mailText;
        public GameObject mailTextObj;

        // Use this for initialization
        void Awake()
        {
            _instance = this;

            Input.multiTouchEnabled = false;

#if UNITY_ANDROID || UNITY_EDITOR
            Application.targetFrameRate = -1;
#elif UNITY_IOS
            Application.targetFrameRate = 60;
#endif

            EventManager.OnParseUserInvalidToken += OnParseUserInvalidToken;

            EventManager.OnGoldBalanceUpdated += RefreshGoldText;
            EventManager.OnEnemyShouldDropRelic += RelicDrop;
            EventManager.OnFairyLeave += OnFairyLeave;
            EventManager.OnRelicBalanceUpdated += UpdateRelicBalanceLabel;
            EventManager.OnDiamondBalanceUpdated += UpdateDiamondBalanceLabel;
            EventManager.OnNewAchievementRecord += ShowNewAchievementIcon;
            EventManager.OnNewGalleryReach += ReachedNewGallery;
            EventManager.OnLanguageChange += RefreshStageHUD;
            EventManager.OnArtifactVGBalanceChange += OnArtifactVGBalanceChange;
            EventManager.OnParseDataLoaded += ParseDataLoaded;

            //EventManager.OnParseFriendRelationError += OnParseFriendRelationError;

            gameStartTime = DateTime.Now;

            SoundManager.Instance.bgm = this.bgm.GetComponent<AudioSource>();
            SoundManager.Instance.FixBGM();
        }

        void OnDestroy()
        {
            EventManager.OnParseUserInvalidToken -= OnParseUserInvalidToken;

            EventManager.OnGoldBalanceUpdated -= RefreshGoldText;
            EventManager.OnEnemyShouldDropRelic -= RelicDrop;
            EventManager.OnFairyLeave -= OnFairyLeave;
            EventManager.OnRelicBalanceUpdated -= UpdateRelicBalanceLabel;
            EventManager.OnDiamondBalanceUpdated -= UpdateDiamondBalanceLabel;
            EventManager.OnNewAchievementRecord -= ShowNewAchievementIcon;
            EventManager.OnNewGalleryReach -= ReachedNewGallery;
            EventManager.OnLanguageChange -= RefreshStageHUD;
            EventManager.OnArtifactVGBalanceChange -= OnArtifactVGBalanceChange;
            EventManager.OnParseDataLoaded -= ParseDataLoaded;

            //EventManager.OnParseFriendRelationError -= OnParseFriendRelationError;

            SoundManager.Instance.bgm = null;

            Resources.UnloadUnusedAssets();

            System.GC.Collect(0);
        }

        void Start()
        {
            CheckLanguageVersion();

            StartCoroutine(UpdateAllDataBalance());

            NoticeDynamicDataManager.Instance.LoadDynamicContent();

            // Check Mailbox data
            if (SocialManager.CheckForInternetConnection())
            {
                if (_socialManager.parseSignedIn)
                {
                    StartCoroutine(LoadParseData());
                }
                else
                {
                    mailTextObj.SetActive(false);
                    mailText.text = "";
                }
            }
            else
            {
                mailTextObj.SetActive(false);
                mailText.text = "";
            }
        }

        public void CheckLanguageVersion()
        {
            string lang = PlayerPrefs.GetString(LanguageMenuViewController.Language_PP_KEY, "");
            if (lang != "")
            {
                LanguageManager.Instance.ChangeLanguage(lang);
                EventManager.ChangeLanguage();
            }
            else
            {
                if (Application.systemLanguage == SystemLanguage.ChineseTraditional ||
                    Application.systemLanguage == SystemLanguage.ChineseSimplified)
                    LanguageManager.Instance.ChangeLanguage("zh-cht");
                else if (Application.systemLanguage == SystemLanguage.Japanese)
                    LanguageManager.Instance.ChangeLanguage("ja");
                else if (Application.systemLanguage == SystemLanguage.Korean)
                    LanguageManager.Instance.ChangeLanguage("ko");
                else
                    LanguageManager.Instance.ChangeLanguage("en");

                EventManager.ChangeLanguage();
            }
        }

        void TutorialEventSetup()
        {
            if (!TutorialManager.Instance.CheckTutorialCompletedById(5))
            {
                bossFightButton.gameObject.AddComponent<TutorialReceiver>().TutorialEventSetup(5);
            }
            else
            {
                bossFightButton.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);
            }

            if (!TutorialManager.Instance.CheckTutorialCompletedById(7))
            {
                galleryButton.gameObject.AddComponent<TutorialReceiver>().TutorialEventSetup(7);
            }
            else
            {
                galleryButton.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);
            }

            if (TutorialManager.Instance.CheckTutorialCompletedById(1) &&
                TutorialManager.Instance.CheckTutorialCompletedById(2) &&
                TutorialManager.Instance.CheckTutorialCompletedById(3) &&
                !TutorialManager.Instance.CheckTutorialCompletedById(4) &&
                GamePlayStatManager.Instance.Stat[StatType.InviteFriends] == 0 &&
                PlayerPrefs.GetString("TryToInviteFriends", "No") != "Yes")
            {
                friendsButton.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(true);
            }
            else
            {
                friendsButton.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);
            }
        }

        GamePlayStatManager gpsManager;

        IEnumerator UpdateAllDataBalance()
        {
            gpsManager = GamePlayStatManager.Instance;

            _artifactCore.InitCore();
            _heroCore.InitCore();
            _enemyCore.InitCore();
            _avatarCore.InitCore();

            if (_cloudDataManager != null && _cloudDataManager.progressLoaded)
            {
                if (!_cloudDataManager.allCloudDataRestored)
                {
                    SyncAllCloudData();
                    _cloudDataManager.allCloudDataRestored = true;
                }
            }
            //else
            //{
            //    if (_socialManager != null)
            //    {
            //        if (!_socialManager.socialSignedIn)
            //        {
            //            Action signInSuccessCallback = () =>
            //            {
            //                if (GamePlayStatManager.Instance != null)
            //                {
            //                    if (GamePlayStatManager.Instance.Stat[StatType.ReachHighestStage] > 0)
            //                        _socialManager.ReportScore(Convert.ToInt32(GamePlayStatManager.Instance.Stat[StatType.ReachHighestStage]));
            //                }
            //            };

            //            _socialManager.SignIn(true, signInSuccessCallback);
            //        }
            //    }
            //}

            ApplyPurchasedItem();

            _artifactCore.RecalAll();
            _heroCore.RecalAll();
            _enemyCore.RecalAll();
            _avatarCore.RecalAll();
            _galleryCore.RecalAll();

            HeroManager.instance.init();

            yield return StartCoroutine(InitCurrentEnemy());
        }

        public void ApplyPurchasedItem()
        {
            // For Fix 0.9.0 key bug
            if (PlayerPrefs.GetInt(AOMAssets.KEY_OF_WORLD_001.ItemId, 0) == 1 &&
                AOMAssets.AnotherWorldKeyGIVG.GetBalance() == 0)
            {
                AOMAssets.AnotherWorldKeyGIVG.Give(1);
                PlayerPrefs.DeleteKey(AOMAssets.KEY_OF_WORLD_001.ItemId);
            }

            // Apply The Key Of the Dark World
            if (AOMAssets.AnotherWorldKeyGIVG.GetBalance() > 0)
            {
                enemyPool.KeyUnlock(AOMAssets.AnotherWorldKeyGIVG.ItemId);
            }

            // Apply The Key Of school
            if (AOMAssets.SchoolKeyGIVG.GetBalance() > 0)
            {
                enemyPool.KeyUnlock(AOMAssets.SchoolKeyGIVG.ItemId);
            }

            // Apply The Key Of forest
            if (AOMAssets.ForestKeyGIVG.GetBalance() > 0)
            {
                enemyPool.KeyUnlock(AOMAssets.ForestKeyGIVG.ItemId);
            }

            // Apply The Key Of water world
            if (AOMAssets.SummerKeyGIVG.GetBalance() > 0)
            {
                enemyPool.KeyUnlock(AOMAssets.SummerKeyGIVG.ItemId);
            }

            //// Apply The Key Of Halloween
            if (AOMAssets.HalloweenKeyGIVG.GetBalance() > 0)
            {
                enemyPool.KeyUnlock(AOMAssets.HalloweenKeyGIVG.ItemId);
            }

            //// Apply The Key Of Fukubukuro
            if (AOMAssets.FukubukuroKeyGIVG.GetBalance() > 0)
            {
                enemyPool.KeyUnlock(AOMAssets.FukubukuroKeyGIVG.ItemId);
            }

            //// Apply The Key Of PvP Character 1
            if (AOMAssets.PvPKey001GIVG.GetBalance() > 0)
            {
                enemyPool.KeyUnlock(AOMAssets.PvPKey001GIVG.ItemId);
            }

            //// Apply The Key Of PvP Character 2
            if (AOMAssets.PvPKey002GIVG.GetBalance() > 0)
            {
                enemyPool.KeyUnlock(AOMAssets.PvPKey002GIVG.ItemId);
            }
        }

        public void SyncAllCloudData()
        {
            if (_cloudDataManager != null && _cloudDataManager.CurrentSaveData != null)
            {
                AOMStoreInventory.SyncCloudGoldBalance(_cloudDataManager.CurrentSaveData);
//                AOMStoreInventory.SyncCloudDiamondBalance(_cloudDataManager.CurrentSaveData);
//                AOMStoreInventory.SyncCloudRelicBalance(_cloudDataManager.CurrentSaveData);

                SyncPurchasedItem();

                _enemyCore.SyncCloudStageProgress(_cloudDataManager.CurrentSaveData);

                SyncPlayerSetting();

                SyncTutorialProgress();

                SyncCloudAvatarLevelBalance();

                SyncCloudHeroLevelBalance();
                _heroCore.SyncCloudHeroAliveState(_cloudDataManager.CurrentSaveData);

                SyncCloudArtifactLevelBalance();

                SyncCloudMoespiritLevelBalance();

                SyncCloudGirlStatus();

                SyncCloudAchievementBalance();

                SyncCloudGamePlayState();
            }
        }

        void SyncPlayerSetting()
        {
            // Player Name
            if (_cloudDataManager.CurrentSaveData.playerName != "")
            {
                PlayerPrefs.SetString("PLAYER_NAME", _cloudDataManager.CurrentSaveData.playerName);
            }
        }

        void SyncCloudAvatarLevelBalance()
        {
            //Debug.Log("Sync Avatar");
            int cloudSavedAvatarLevel = _cloudDataManager.CurrentSaveData.avatarLevel;
            AOMAssets.AvatarLevelVG.ResetBalance(cloudSavedAvatarLevel, false);

            if (_cloudDataManager.CurrentSaveData.avatarSkillList.Count > 0)
            {
                for (int i = 0; i < AOMAssets.AvatarActiveSkillVGs.Count; i++)
                {
                    //Debug.Log("Sync AvatarSkill - " + AOMAssets.AvatarActiveSkillVGs[i].ItemId);
                    int cloudSavedAvatarSkillLevel = _cloudDataManager.CurrentSaveData.avatarSkillList[i];
                    AOMAssets.AvatarActiveSkillVGs[i].ResetBalance(cloudSavedAvatarSkillLevel, false);
                }
            }
        }

        void SyncCloudHeroLevelBalance()
        {
            if (_cloudDataManager.CurrentSaveData.heroList.Count > 0)
            {
                for (int i = 0; i < HeroAssets.Heroes.Count; i++)
                {
                    //Debug.Log("Sync Hero - " + HeroAssets.Heroes[i].ItemId);
                    int cloudSavedHeroLevel = _cloudDataManager.CurrentSaveData.heroList[i];
                    HeroAssets.Heroes[i].ResetBalance(cloudSavedHeroLevel, false);

                    if (_cloudDataManager.CurrentSaveData.heroPassiveList.Count > 0)
                    {
                        for (int j = 0; j < HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[i].ItemId].Count; j++)
                        {
                            //Debug.Log("Sync HeroSkill - " + HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[i].ItemId][j].ItemId);
                            int skillBalance = (j + 1) <= _cloudDataManager.CurrentSaveData.heroPassiveList[i] ? 1 : 0;
                            HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[i].ItemId][j]
                                .ResetBalance(skillBalance,
                                    false);
                        }
                    }
                }
            }
        }

        void SyncCloudArtifactLevelBalance()
        {
            if (_cloudDataManager.CurrentSaveData.artifactOrderList != null)
            {
                if (_cloudDataManager.CurrentSaveData.artifactOrderList.Count ==
                    _cloudDataManager.CurrentSaveData.artifactList.Count)
                {
                    if (_cloudDataManager.CurrentSaveData.artifactOrderList.Count > 0)
                    {
                        for (int i = 0; i < _cloudDataManager.CurrentSaveData.artifactOrderList.Count; i++)
                        {
                            string artifactId = "AR" +
                                                _cloudDataManager.CurrentSaveData.artifactOrderList[i].ToString("000");

                            int cloudSavedArtifactLevel = _cloudDataManager.CurrentSaveData.artifactList[i];

                            _artifactCore.RestorePurchasedArtifact(artifactId, cloudSavedArtifactLevel);
                        }
                    }
                }
            }
            else
            {
                if (_cloudDataManager.CurrentSaveData.artifactList.Count == ArtifactAssets.Artifacts.Count)
                {
                    if (_cloudDataManager.CurrentSaveData.artifactList.Count > 0)
                    {
                        for (int i = 0; i < ArtifactAssets.Artifacts.Count; i++)
                        {
                            int cloudSavedArtifactLevel = _cloudDataManager.CurrentSaveData.artifactList[i];

                            if (cloudSavedArtifactLevel > 0)
                                _artifactCore.RestorePurchasedArtifact(ArtifactAssets.Artifacts[i].ItemId,
                                    cloudSavedArtifactLevel);
                        }
                    }
                }
            }
        }

        void SyncCloudMoespiritLevelBalance()
        {
            if (_cloudDataManager.CurrentSaveData.moespiritList != null)
            {
                if (_cloudDataManager.CurrentSaveData.moespiritList.Count > 0)
                {
                    for (int i = 0; i < _cloudDataManager.CurrentSaveData.moespiritList.Count; i++)
                    {
                        int balance = _cloudDataManager.CurrentSaveData.moespiritList[i];
                        ArtifactAssets.MoeSpirits[i].ResetBalance(balance);
                    }
                }
            }
        }

        void SyncCloudGirlStatus()
        {
            if (_cloudDataManager.CurrentSaveData.girlStatusList.Count > 0)
            {
                Debug.Log(_cloudDataManager.CurrentSaveData.girlStatusList.Count);
                for (int i = 0; i < GalleryCore.Girls.Count; i++)
                {
                    if (i < _cloudDataManager.CurrentSaveData.girlStatusList.Count)
                    {
//                        Debug.Log("Before Sync Girl - " + GalleryCore.Girls[i].girlId);
//                        Debug.Log("Status - " + GalleryCore.Girls[i].Status);
//                        Debug.Log("Exp - " + GalleryCore.Girls[i].Exp);
//                        Debug.Log("Reached Break Level - " + GalleryCore.Girls[i].ReachedBreakLevel);

                        GalleryCore.Girls[i]
                            .RestoreGirlsStatus(_cloudDataManager.CurrentSaveData.girlStatusList[i],
                                _cloudDataManager.CurrentSaveData.girlExpList[i],
                                _cloudDataManager.CurrentSaveData.girlReachedBreakLevelList[i]);

//                        Debug.Log("After Sync Girl - " + GalleryCore.Girls[i].girlId);
//                        Debug.Log("Status - " + GalleryCore.Girls[i].Status);
//                        Debug.Log("Exp - " + GalleryCore.Girls[i].Exp);
//                        Debug.Log("Reached Break Level - " + GalleryCore.Girls[i].ReachedBreakLevel);
                    }
                }
            }

            if (_cloudDataManager.CurrentSaveData.touchCredit != null)
            {
                _galleryCore.TouchCredit = _cloudDataManager.CurrentSaveData.touchCredit;
            }
        }

        void SyncCloudAchievementBalance()
        {
            if (_cloudDataManager.CurrentSaveData.achievementList.Count > 0)
            {
                for (int i = 0; i < AchievementAssets.Achievements.Count; i++)
                {
                    //Debug.Log("Sync Achievement - " + AchievementAssets.Achievements[i].ItemId);
                    AchievementAssets.Achievements[i]
                        .ResetBalance(_cloudDataManager.CurrentSaveData.achievementList[i]);
                }
            }
        }

        void SyncCloudGamePlayState()
        {
            if (GamePlayStatManager.Instance != null)
            {
                GamePlayStatManager.Instance.RestoreCloudSavedGameplayState(_cloudDataManager.CurrentSaveData);

                if (GamePlayStatManager.Instance.Stat[StatType.ReachHighestStage] > 0)
                    _socialManager.ReportMaxStage(
                        Convert.ToInt32(GamePlayStatManager.Instance.Stat[StatType.ReachHighestStage]));
            }

            if (_cloudDataManager.CurrentSaveData.pp_FBInvitedList != null)
            {
                if (FBManager.Instance != null)
                    PlayerPrefs.SetString(FBManager.Instance.PP_FB_INVITED_LIST,
                        _cloudDataManager.CurrentSaveData.pp_FBInvitedList);
            }

            if (_cloudDataManager.CurrentSaveData.noAdmob != null)
                AdMobManager.Instance.ShouldNoADs = _cloudDataManager.CurrentSaveData.noAdmob;

            if (_cloudDataManager.CurrentSaveData.appRated != null)
                PlayerPrefs.SetInt("APP_RATED", _cloudDataManager.CurrentSaveData.appRated ? 1 : 0);

            if (_cloudDataManager.CurrentSaveData.facebookLiked != null)
                PlayerPrefs.SetInt("FACEBOOK_LIKED", _cloudDataManager.CurrentSaveData.facebookLiked ? 1 : 0);

            if (_cloudDataManager.CurrentSaveData.twitterFollowed != null)
                PlayerPrefs.SetInt("TWITTER_FOLLOWED", _cloudDataManager.CurrentSaveData.twitterFollowed ? 1 : 0);
        }

        void SyncPurchasedItem()
        {
            if (_cloudDataManager.CurrentSaveData.key001Purchased != null)
                AOMAssets.AnotherWorldKeyGIVG.ResetBalance(_cloudDataManager.CurrentSaveData.key001Purchased ? 1 : 0);
            if (_cloudDataManager.CurrentSaveData.key002Purchased != null)
                AOMAssets.SchoolKeyGIVG.ResetBalance(_cloudDataManager.CurrentSaveData.key002Purchased ? 1 : 0);
            if (_cloudDataManager.CurrentSaveData.key003Purchased != null)
                AOMAssets.ForestKeyGIVG.ResetBalance(_cloudDataManager.CurrentSaveData.key003Purchased ? 1 : 0);
            if (_cloudDataManager.CurrentSaveData.key004Purchased != null)
                AOMAssets.SummerKeyGIVG.ResetBalance(_cloudDataManager.CurrentSaveData.key004Purchased ? 1 : 0);
            if (_cloudDataManager.CurrentSaveData.key005Purchased != null)
                AOMAssets.HalloweenKeyGIVG.ResetBalance(_cloudDataManager.CurrentSaveData.key005Purchased ? 1 : 0);
            if (_cloudDataManager.CurrentSaveData.key006Purchased != null)
                AOMAssets.FukubukuroKeyGIVG.ResetBalance(_cloudDataManager.CurrentSaveData.key006Purchased ? 1 : 0);

            if (_cloudDataManager.CurrentSaveData.pvpGiftKey001Purchased != null)
                AOMAssets.PvPKey001GIVG.ResetBalance(_cloudDataManager.CurrentSaveData.pvpGiftKey001Purchased ? 1 : 0);
            if (_cloudDataManager.CurrentSaveData.pvpGiftKey002Purchased != null)
                AOMAssets.PvPKey002GIVG.ResetBalance(_cloudDataManager.CurrentSaveData.pvpGiftKey002Purchased ? 1 : 0);

            if (_cloudDataManager.CurrentSaveData.goldPackPurchased != null)
                PlayerPrefs.SetInt("promotional_goldpack_PURCHASED",
                    _cloudDataManager.CurrentSaveData.goldPackPurchased ? 1 : 0);
            if (_cloudDataManager.CurrentSaveData.silverPackPurchased != null)
                PlayerPrefs.SetInt("promotional_silverpack_PURCHASED",
                    _cloudDataManager.CurrentSaveData.silverPackPurchased ? 1 : 0);
            if (_cloudDataManager.CurrentSaveData.bronzePackPurchased != null)
                PlayerPrefs.SetInt("promotional_bronzepack_PURCHASED",
                    _cloudDataManager.CurrentSaveData.bronzePackPurchased ? 1 : 0);
        }

        void SyncTutorialProgress()
        {
            if (_cloudDataManager.CurrentSaveData.tutorialCompleted != null)
            {
                PlayerPrefs.SetInt("UserTutorialCompleted",
                    _cloudDataManager.CurrentSaveData.tutorialCompleted ? 1 : 0);

                if (_cloudDataManager.CurrentSaveData.tutorialCompleted)
                {
                    for (int i = 1; i <= 9; i++)
                    {
                        TutorialManager.Instance.UpdateTutorialProgress(i);
                    }
                }
            }
        }

        IEnumerator InitCurrentEnemy()
        {
            if (StageManager.Instance != null)
                StageManager.Instance.ChangeStageWorld(false);

            loadEnemy(() =>
            {
                if (enemy != null)
                {
                    enemy.OnDie += OnEnemyDie;
                    enemy.Spawn();

                    if (_enemyCore.CurrentEnemyType == EnemyType.BigBoss ||
                        _enemyCore.CurrentEnemyType == EnemyType.MiniBoss)
                    {
                        HideBossFightHUD(true);

                        enemy.OnDie -= OnEnemyDie;

                        if (_enemyCore.CurrentEnemyType == EnemyType.BigBoss)
                            enemyPool.RemoveBossById(enemyObject.name);
                        else if (_enemyCore.CurrentEnemyType == EnemyType.MiniBoss)
                            enemyPool.RemoveMiniBossById(enemyObject.name);
                        else
                            enemyPool.RemoveEnemy();

                        LoadNextLevel(true);
                    }
                }
            });


            RefreshBossFightButton();

            yield return StartCoroutine(InitDeadEffect());
        }

        IEnumerator InitDeadEffect()
        {
            enemyPool.InitDeadEffect();

            yield return StartCoroutine(InitGameStart());
        }

        IEnumerator InitGameStart()
        {
            RefreshStageHUD();

            if (OfflineGoldManager.instance != null)
            {
                if (OfflineGoldManager.instance.CheckOfflineGold())
                    offlineGoldDropBtn.SetActive(true);
            }

            RefreshGoldText();
            RefreshDiamondText();

            TutorialEventSetup();

            if (_versionCheckerManager.VersionPass)
            {
                // Show fully charge of touch credit button
                if (_galleryCore.OwnedGirls())
                {
                    lastTouch = _galleryCore.LastTouch;
                    touchCredit = _galleryCore.TouchCredit;
                    maxTouchCredit = _galleryCore.MaxTouchCredit;

//                    fullTouchCreditBtn.SetActive(touchCredit >= maxTouchCredit);
                }
            }

            yield return null;

            GameStart();
        }

        IEnumerator PreloadMenu()
        {
            HiddenMenu.Instance.ShowHiddenMenu(0);
            yield return new WaitForSeconds(0.5f);

            HiddenMenu.Instance.ShowHiddenMenu(1);
            yield return new WaitForSeconds(0.5f);

            HiddenMenu.Instance.ShowHiddenMenu(2);
            yield return new WaitForSeconds(0.5f);

            HiddenMenu.Instance.ShowHiddenMenu(3);
            yield return new WaitForSeconds(0.5f);

            HiddenMenu.Instance.CloseHiddenMenu();

            GameReady = true;

            ParseDataLoaded();

            if (_pvpChallengeService.ChallengeCompleted)
                ShowPvPMenu();
        }

        void ParseDataLoaded()
        {
            if (NoticeDynamicDataManager.Instance.LoadedSuccess && _gameReady)
            {
                loadingScene.SetActive(false);
                gameStarted = true;

                if (PlayerPrefs.GetString("PLAYER_NAME", "").Length == 0)
                {
                    ShowStoryIntro();
                }
                else
                {
                    SoundManager.Instance.PlayBgmWithType(SoundType.bgm);

                    ShowNoticeBoard();
                }
            }
        }

        public void ShowNoticeBoard()
        {
            if (TutorialManager.Instance.CheckTutorialCompletedById(7)) // Gallery rub tutorial
            {
                if (!OfflineGoldManager.instance.noticeBoardShowing)
                {
                    if (UnitConverter.ConvertToTimestamp(System.DateTime.Now) -
                        OfflineGoldManager.instance.noticeBoardLastShowTime >= 86400)
                    {
                        // Dynamic Notice board
                        CreateDynamicNoticeBoard();
                    }
                    else
                    {
                        if (_versionCheckerManager.ReparationAvailable)
                        {
                            ShowReparationBoard();
                        }
                    }
                }

                AdMobManager.Instance.InitFirstAd();
            }
        }

        void CreateDynamicNoticeBoard()
        {
            GameObject noticeDynamicMenuView =
                _uiViewLoader.LoadOverlayView("NoticeDynamicMenuView", null, false, false);
            noticeDynamicMenuView.GetComponent<NoticeDynamicMenuViewController>()
                .LoadContent(NoticeDynamicDataManager.Instance.DynamicData);

            if (OfflineGoldManager.instance != null)
            {
                OfflineGoldManager.instance.noticeBoardShowing = true;
            }
        }

        public void ShowReparationBoard()
        {
            if (
                PlayerPrefs.GetInt(ReparationViewController.Reparation_PP_KEY + _versionCheckerManager.CurrentVersion,
                    0) ==
                0)
            {
                GameObject reparationView = _uiViewLoader.LoadOverlayView("ReparationMenuView", null, false, false);
            }
        }

        public void ShowPvPMenu()
        {
            _pvpChallengeService.ChallengeCompleted = false;
            HiddenMenu.Instance.OpenPvPMenu();
        }

        public void CompletePrestigeAnimation()
        {
            menu.SetActive(true);
            sprite.SetActive(true);
            prestigeEffect.SetActive(false);

            if (ArtifactAssets.MoeSpirit023.GetBalance() > 0)
            {
                UserManager.Instance.GiveGems((int) (ArtifactAssets.MoeSpirit023.EffectValue * 100), ShowDiamondIcon,
                    false);
            }

            RefreshGoldText();
            RefreshDiamondText();
            RefreshBossFightButton();

            if (StageManager.Instance != null)
                StageManager.Instance.ChangeStageWorld(false);

            loadEnemy(() =>
            {
                enemy.OnDie += OnEnemyDie;
                enemy.Spawn();

                RefreshStageHUD();
            });

            inPrestige = false;
            StartRelicDropAnimation();
        }

        void StartPrestige()
        {
            offlineGoldDropBtn.SetActive(false);
            PlayerPrefs.SetString(OfflineGoldManager.LAST_ACTIVE_TIME,
                UnitConverter.ConvertToTimestamp(System.DateTime.Now).ToString());

            OfflineGoldManager.instance.SaveUnPickedOfflineGold(true);

            GamePlayStatManager.Instance.LostGoldFromPrestige(AOMStoreInventory.GetGoldBalance());
            relicEarnedFromPrestige = _avatarCore.Prestige();

            enemy.OnDie -= OnEnemyDie;

            if (_enemyCore.CurrentEnemyType == EnemyType.BigBoss)
                enemyPool.RemoveBossById(enemyObject.name);
            else if (_enemyCore.CurrentEnemyType == EnemyType.MiniBoss)
                enemyPool.RemoveMiniBossById(enemyObject.name);
            else
                enemyPool.RemoveEnemy();

            HideBossFightHUD(true);

            PlayerPrefs.SetInt("Arrived_EndOfWorld", 0);
        }

        void StartSuperPrestige()
        {
            relicEarnedFromPrestige = _avatarCore.SuperPrestige();

            enemy.OnDie -= OnEnemyDie;

            if (_enemyCore.CurrentEnemyType == EnemyType.BigBoss)
                enemyPool.RemoveBossById(enemyObject.name);
            else if (_enemyCore.CurrentEnemyType == EnemyType.MiniBoss)
                enemyPool.RemoveMiniBossById(enemyObject.name);
            else
                enemyPool.RemoveEnemy();
        }

        public void PlayPrestigeAnimation(bool clearRecord = true)
        {
            inPrestige = true;
            menu.SetActive(false);
            sprite.SetActive(false);
            prestigeEffect.SetActive(true);

            if (clearRecord)
            {
                Invoke("StartPrestige", 4f);
                Invoke("CompletePrestigeAnimation", 6f);
            }
            else
            {
                Invoke("StartSuperPrestige", 4f);
                Invoke("CompletePrestigeAnimation", 6f);
            }
        }

        void RefreshBossFightButton()
        {
            if ((_enemyCore.CurrentEnemyType == EnemyType.BigBoss ||
                 _enemyCore.CurrentEnemyType == EnemyType.MiniBoss))
            {
                if (bossFightButton.gameObject.transform.FindChild("fight_boss").gameObject.activeSelf)
                    bossFightButton.gameObject.transform.FindChild("fight_boss").gameObject.SetActive(false);

                if (!bossFightButton.gameObject.transform.FindChild("leave_battle").gameObject.activeSelf)
                    bossFightButton.gameObject.transform.FindChild("leave_battle").gameObject.SetActive(true);

                if (!bossFightButton.gameObject.activeSelf)
                    bossFightButton.gameObject.SetActive(true);

                if (currentWaveText.gameObject.activeSelf)
                    currentWaveText.gameObject.SetActive(false);
            }
            else if (_enemyCore.IsInfinityLoop)
            {
                if (!bossFightButton.gameObject.transform.FindChild("fight_boss").gameObject.activeSelf)
                    bossFightButton.gameObject.transform.FindChild("fight_boss").gameObject.SetActive(true);

                if (bossFightButton.gameObject.transform.FindChild("leave_battle").gameObject.activeSelf)
                    bossFightButton.gameObject.transform.FindChild("leave_battle").gameObject.SetActive(false);

                if (!bossFightButton.gameObject.activeSelf)
                    bossFightButton.gameObject.SetActive(true);

                if (currentWaveText.gameObject.activeSelf)
                    currentWaveText.gameObject.SetActive(false);

                // Tutorial Event 05 Trigger
                if (!TutorialManager.Instance.CheckTutorialCompletedById(5))
                {
                    if (bossFightButton.gameObject.GetComponent<TutorialReceiver>() != null)
                    {
                        if (!bossFightButton.gameObject.GetComponent<TutorialReceiver>().isTriggered)
                        {
                            TutorialManager.Instance.TriggerTutorial(5);

                            GameObject bossFailedMenuView = _uiViewLoader.LoadOverlayView("BossFailedMenuView", null,
                                false, false);
                        }
                    }
                }
            }
            else
            {
                if (bossFightButton.gameObject.activeSelf)
                    bossFightButton.gameObject.SetActive(false);

                if (!currentWaveText.gameObject.activeSelf)
                    currentWaveText.gameObject.SetActive(true);
            }
        }

        void OnFairyLeave(bool val)
        {
            isFairyAppearing = val;
        }

        IEnumerator StartRelicAnimation(int i, int amount)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(0, 0.3f));
            float y = Mathf.Sin(Mathf.PI * (1.0f / 10) * i) * 1;
            float x = Mathf.Cos(Mathf.PI * (1.0f / 10) * i) * 1;
            GameObject obj = Instantiate(relicForAnimation.gameObject);
            obj.transform.position = new Vector3(x, y, 0);
            obj.SetActive(true);
            obj.GetComponent<RelicDropAnimation>()
                .StartDrop(x, y, x * 4, y * 4, relicIcon.transform.position.x, relicIcon.transform.position.y, amount);
        }

        IEnumerator HideRelicIcon()
        {
            yield return new WaitForSeconds(3);
            relicIcon.SetActive(false);
        }

        public void StartRelicDropAnimation()
        {
            int i = 0;
            int numberOfRelic = Math.Min(20, relicEarnedFromPrestige);
            int amountPerRelic = 1;
            int amountForLastRelic = 1;
            if (numberOfRelic > 1)
            {
                amountPerRelic = relicEarnedFromPrestige / (numberOfRelic - 1);
                amountForLastRelic = relicEarnedFromPrestige % (numberOfRelic - 1);
            }
            while (i < numberOfRelic)
            {
                if (i == numberOfRelic - 1)
                    StartCoroutine(StartRelicAnimation(i, amountForLastRelic));
                else
                    StartCoroutine(StartRelicAnimation(i, amountPerRelic));
                i++;
            }

            relicIconBalance.GetComponent<Text>().text = UserManager.Instance.CurrentUserSummary != null
                ? UnitConverter.ConverterDoubleToString(UserManager.Instance.CurrentUserSummary.Moecrystal)
                : UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetRelicBalance());
            relicIcon.SetActive(true);
            StartCoroutine(HideRelicIcon());
        }

        void Update()
        {
            if (gameStarted)
            {
                if (!isFairyAppearing)
                {
                    fairyAppearCountDown -= Time.deltaTime;

                    if (fairyAppearCountDown <= 0)
                    {
                        float fairyAppearChance = UnityEngine.Random.Range(0f, 100f);

                        if (fairyTreasureBoxBtn != null && fairyAppearChance < baseFairyAppearChance &&
                            TutorialManager.Instance.CheckTutorialCompletedById(2))
                        {
                            fairyTreasureBoxBtn.FairyStart();
                            isFairyAppearing = true;
                            baseFairyAppearChance = 10.0f;
                        }
                        else
                        {
                            baseFairyAppearChance += 5.0f;
                        }

                        fairyAppearCountDown = 15.0f;
                    }
                }


                // [[ For testing fairy feature! ]]
//                if (!isFairyAppearing)
//                {
//                    fairyAppearCountDown -= Time.deltaTime;
//
//                    if (fairyAppearCountDown <= 0)
//                    {
//                        fairyTreasureBoxBtn.FairyStart();
//                        isFairyAppearing = true;
//
//                        fairyAppearCountDown = 5.0f;
//                    }
//                }


                if (bossFightCountDown != -1 && !breakScene.activeInHierarchy)
                {
                    bossFightCountDown -= Time.deltaTime;
                    bossFightCountDownText.text = bossFightCountDown.ToString("0.0");
                    bossFightTimeline.value = bossFightCountDown / _enemyCore.BossFightCountDown;

                    if (bossFightCountDown <= 0)
                    {
                        HideBossFightHUD(false);

                        enemy.OnDie -= OnEnemyDie;

                        if (_enemyCore.CurrentEnemyType == EnemyType.BigBoss)
                            enemyPool.RemoveBossById(enemyObject.name);
                        else if (_enemyCore.CurrentEnemyType == EnemyType.MiniBoss)
                            enemyPool.RemoveMiniBossById(enemyObject.name);
                        else
                            enemyPool.RemoveEnemy();

                        LoadNextLevel(true);

                        isFairyAppearing = false;
                    }
                }

                // Promotion
                if (!showingPromotionBoard)
                {
                    if (TutorialManager.Instance.CheckTutorialCompletedById(7))
                    {
                        if (PlayerPrefs.GetInt("DONT_SHOW_RATE_OUR_GAME", 0) == 0 ||
                            PlayerPrefs.GetInt("DONT_SHOW_FACEBOOK_LIKED", 0) == 0 ||
                            PlayerPrefs.GetInt("DONT_SHOW_TWITTER_FOLLOWED", 0) == 0)
                        {
                            if (promotionTimer > 0)
                            {
                                promotionTimer -= Time.deltaTime;
                            }
                            else
                            {
                                promotionTimer = promotionTime;

                                GameObject promotionMenuView = _uiViewLoader.LoadOverlayView("PromotionMenuView", null,
                                    false, false);
                            }
                        }
                    }
                }

                //if (Time.timeScale != 0)
                //{
                //    if ((DateTime.UtcNow - _cloudDataManager.lastSaveDateTime).TotalMinutes >= autoSaveTime)
                //    {
                //        AutoSave();
                //    }
                //}

                if (Application.platform == RuntimePlatform.Android ||
                    Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    if (Input.touchCount > 0)
                    {
                        if (Input.GetTouch(0).phase == TouchPhase.Began)
                        {
                            checkTouch(Input.GetTouch(0).position);
                        }
                    }
                }
                else if (Application.platform == RuntimePlatform.WindowsEditor ||
                         Application.platform == RuntimePlatform.OSXEditor)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        checkTouch(Input.mousePosition);
                    }
                }

                if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Home))
                {
                    if (HiddenMenu.Instance != null && HiddenMenu.Instance.hiddenMenu.activeSelf)
                    {
                        HiddenMenu.Instance.CloseHiddenMenu();
                    }
                    else
                    {
#if UNITY_EDITOR
                        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_IOS || UNITY_ANDROID
                        Application.Quit();
#endif
                    }
                }

                if (_versionCheckerManager.VersionPass)
                {
                    // Recharging gallery touch credit
                    if (_galleryCore.OwnedGirls())
                    {
                        if (lastTouch != 0 && touchCredit < maxTouchCredit)
                        {
                            if (UnitConverter.ConvertToTimestamp(System.DateTime.Now) - lastTouch >= creditRechangeTime)
                            {
                                touchCredit +=
                                    (int) (UnitConverter.ConvertToTimestamp(System.DateTime.Now) - lastTouch) /
                                    creditRechangeTime;
                                lastTouch += creditRechangeTime;

                                if (touchCredit >= maxTouchCredit)
                                {
                                    lastTouch = 0;
//                                    if (!fullTouchCreditBtn.activeSelf)
//                                        fullTouchCreditBtn.SetActive(true);
                                }
                                else
                                {
//                                    if (fullTouchCreditBtn.activeSelf)
//                                        fullTouchCreditBtn.SetActive(false);
                                }

                                _galleryCore.TouchCredit = (touchCredit >= maxTouchCredit)
                                    ? maxTouchCredit
                                    : touchCredit;
                                _galleryCore.LastTouch = lastTouch;
                            }
                        }
                        else
                        {
                            if (lastTouch == 0)
                            {
                                lastTouch = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                                _galleryCore.LastTouch = lastTouch;
                            }

                            if (touchCredit >= maxTouchCredit)
                            {
                                lastTouch = 0;

                                _galleryCore.LastTouch = lastTouch;

//                                if (!fullTouchCreditBtn.activeSelf)
//                                    fullTouchCreditBtn.SetActive(true);
                            }
                            else
                            {
//                                if (fullTouchCreditBtn.activeSelf)
//                                    fullTouchCreditBtn.SetActive(false);
                            }
                        }
                    }
                }
            }
        }

        IEnumerator LoadParseData()
        {
            ParseQuery<DropBoxItem> query =
                new ParseQuery<DropBoxItem>().WhereEqualTo("user", ParseUser.CurrentUser).OrderBy("updatedAt");

            Task<IEnumerable<DropBoxItem>> t = query.FindAsync();

            while (!t.IsCompleted)
            {
                yield return null;
            }

            if (!t.IsFaulted && !t.IsCanceled && t.IsCompleted)
            {
                List<DropBoxItem> list = t.Result.ToList();

                if (list != null)
                {
                    int mailNum = 0;

                    for (int i = 0; i < list.Count; i++)
                    {
                        TimeSpan ts = DateTime.Parse(list[i].CreatedAt.ToString()).AddDays(7).Subtract(DateTime.UtcNow);

                        if (ts.TotalSeconds > 0)
                        {
                            mailNum++;
                        }
                        else
                        {
                            Task deleteTask = list[i].DeleteAsync();
                        }
                    }

                    mailTextObj.SetActive(mailNum > 0);
                    mailText.text = mailNum == 0 ? "" : ((mailNum >= 10) ? "9+" : mailNum.ToString());
                }
            }
        }

        void UpdateRelicBalanceLabel()
        {
            relicIconBalance.GetComponent<Text>().text = UserManager.Instance.CurrentUserSummary != null
                ? UnitConverter.ConverterDoubleToString(UserManager.Instance.CurrentUserSummary.Moecrystal)
                : UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetRelicBalance());
        }

        void UpdateDiamondBalanceLabel()
        {
            diamondIconBalance.GetComponent<Text>().text = UserManager.Instance.CurrentUserSummary != null
                ? UserManager.Instance.CurrentUserSummary.Gem.ToString()
                : AOMStoreInventory.GetDiamondBalance().ToString();

            RefreshDiamondText();
        }

        public void UpdateTouchCredit()
        {
            lastTouch = _galleryCore.LastTouch;
            touchCredit = _galleryCore.TouchCredit;
            maxTouchCredit = _galleryCore.MaxTouchCredit;
        }

        public void ShowDiamondIcon()
        {
            diamondIcon.SetActive(true);
            StartCoroutine(HideDiamondIcon());
        }

        IEnumerator HideDiamondIcon()
        {
            yield return new WaitForSeconds(2);
            GameManager.Instance.diamondIcon.SetActive(false);
        }

        void ReachedNewGallery()
        {
            galleryButton.GetComponent<Image>().sprite = galleryButton.GetComponent<Button>().spriteState.pressedSprite;

            // Tutorial Event 07 Trigger
            if (!TutorialManager.Instance.CheckTutorialCompletedById(7))
            {
                TutorialManager.Instance.TriggerTutorial(7);
            }
        }

        public void ShowBossCharacterFieldGuide()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (galleryButton.GetComponent<Image>().sprite ==
                galleryButton.GetComponent<Button>().spriteState.pressedSprite)
                galleryButton.GetComponent<Image>().sprite =
                    galleryButton.GetComponent<Button>().spriteState.highlightedSprite;

            loadingScene.SetActive(true);
            loadingScene.GetComponent<TipsManager>().ChangeTips();

            if (galleryButton.gameObject.GetComponent<TutorialReceiver>() != null)
            {
                if (galleryButton.gameObject.GetComponent<TutorialReceiver>().isTriggered)
                    TutorialManager.Instance.UpdateTutorialProgress(7);
            }

            AutoSave();

            Application.LoadLevelAsync("FieldGuideScene");
        }

        public void BossFightButtonPressed()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            bool isBoss = _enemyCore.CurrentEnemyType == EnemyType.BigBoss ||
                          _enemyCore.CurrentEnemyType == EnemyType.MiniBoss;

            if (isBoss)
            {
                HideBossFightHUD(false);
            }
            else
            {
                bossFightTimeline.value = 1;
            }

            enemy.OnDie -= OnEnemyDie;

            if (_enemyCore.CurrentEnemyType == EnemyType.BigBoss)
                enemyPool.RemoveBossById(enemyObject.name);
            else if (_enemyCore.CurrentEnemyType == EnemyType.MiniBoss)
                enemyPool.RemoveMiniBossById(enemyObject.name);
            else
                enemyPool.RemoveEnemy();

            LoadNextLevel(isBoss);
        }

        private void BossFightStart()
        {
            //Debug.Log("BossFightStart");
            bossFightCountDown = _enemyCore.BossFightCountDown;
            bossFightCountDownText.gameObject.SetActive(true);
            bossFightTimeline.gameObject.SetActive(true);

            if (_enemyCore.CurrentEnemyType == EnemyType.BigBoss)
            {
                bossTextEffect.EffectPlay(3);
            }
            else if (_enemyCore.CurrentEnemyType == EnemyType.MiniBoss)
            {
                bossTextEffect.EffectPlay(1);
            }
        }

        private void HideBossFightHUD(bool success)
        {
            //Debug.Log("BossFightEnd");
            bossFightCountDown = -1;
            bossFightCountDownText.gameObject.SetActive(false);
            bossFightTimeline.gameObject.SetActive(false);

            if (!success)
            {
                if (_enemyCore.CurrentEnemyType == EnemyType.BigBoss)
                {
                    bossTextEffect.EffectPlay(4);
                }
                else if (_enemyCore.CurrentEnemyType == EnemyType.MiniBoss)
                {
                    bossTextEffect.EffectPlay(2);
                }
            }

            if (!relic.canCollect)
            {
                relic.Disappear();
                relicDropped = false;
            }
        }

        private void RelicDrop()
        {
            relic.Appear();
            relicDropped = true;
        }

        bool isMoetanDefeated = false;

        void OnEnemyDie()
        {
            EnemyCore ec = _enemyCore;

            if (relicDropped)
            {
                relic.Unlock();
                relicDropped = false;
            }

            if (ec.CurrentEnemyType == EnemyType.BigBoss || ec.CurrentEnemyType == EnemyType.MiniBoss)
            {
                HideBossFightHUD(true);
            }

            GamePlayStatManager.Instance.EnemyKillWithType(ec.CurrentEnemyType);

            enemy.OnDie -= OnEnemyDie;

            if (_enemyCore.CurrentEnemyType == EnemyType.BigBoss)
            {
                enemyPool.RemoveBossById(enemyObject.name);

                isMoetanDefeated = true;

                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
                    if (ArtifactAssets.MoeSpirit030.GetBalance() > 0)
                    {
                        diamondPool.GetComponent<DiamondPool>()
                            .DiamondDropFromFairy((int) (ArtifactAssets.MoeSpirit030.EffectValue * 100), 0, 0);
                    }
                }

                if (StageManager.Instance != null)
                    StageManager.Instance.ChangeStageWorld();
            }
            else if (_enemyCore.CurrentEnemyType == EnemyType.MiniBoss)
            {
                enemyPool.RemoveMiniBossById(enemyObject.name);
            }
            else
            {
                enemyPool.RemoveEnemy();
            }

            CoinDrop();
            LoadNextLevel(ec.IsInfinityLoop);
        }

        public void GetMoetanDefeatedMoeCrystal()
        {
            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                return;
            }

            if (isMoetanDefeated)
            {
                if (_enemyCore.Stage > 80)
                {
                    int relicVal = 1;

                    if (UnityEngine.Random.Range(0, 100) <= (int) (ArtifactAssets.Artifact033.EffectValue * 100))
                    {
                        relicVal++;
                    }

                    if (UnityEngine.Random.Range(0, 100) <= (int) (ArtifactAssets.MoeSpirit025.EffectValue * 100))
                    {
                        relicVal++;
                    }

                    relicEarnedFromPrestige = relicVal;
                    StartRelicDropAnimation();
                }

                isMoetanDefeated = false;
            }
        }

        public void LoadNextLevel(bool repeatLevel)
        {
            _enemyCore.NextEnemy(repeatLevel);

            loadEnemy(() =>
            {
                enemy.OnDie += OnEnemyDie;

                RefreshStageHUD();
                enemy.Spawn();

                if (_enemyCore.CurrentEnemyType == EnemyType.BigBoss ||
                    _enemyCore.CurrentEnemyType == EnemyType.MiniBoss)
                {
                    BossFightStart();

                    if (bossFightButton.gameObject.GetComponent<TutorialReceiver>() != null)
                    {
                        if (bossFightButton.gameObject.GetComponent<TutorialReceiver>().isTriggered)
                            TutorialManager.Instance.UpdateTutorialProgress(5);
                    }
                }

                RefreshBossFightButton();
            });
        }

        void loadEnemy(Action loadCallback)
        {
            enemyPool.GetEnemyWithType(_enemyCore.CurrentEnemyType, () =>
            {
                enemyObject = enemyPool.currentEnemy;

                if (enemyObject != null)
                {
                    enemyObject.SetActive(true);
                    enemy = (Enemy) enemyObject.GetComponent(typeof(Enemy));

                    loadCallback();
                }
            });
        }

        public void RefreshGoldText()
        {
            gold = AOMStoreInventory.GetGoldBalance();
            goldString = UnitConverter.ConverterDoubleToString(gold);

            if (goldText.text != goldString)
            {
                goldText.text = goldString;
                float tmpGoldTextWidth = goldText.preferredWidth;
                if (tmpGoldTextWidth != currentGoldTextWidth)
                {
                    Vector3 goldImagePosition = goldImage.transform.localPosition;
                    goldImagePosition.x = goldText.transform.localPosition.x - (tmpGoldTextWidth / 2) -
                                          goldImage.rectTransform.sizeDelta.x;
                    goldImagePosition.y = goldText.transform.localPosition.y;
                    goldImage.transform.localPosition = goldImagePosition;
                    currentGoldTextWidth = tmpGoldTextWidth;
                }
            }
        }

        public void RefreshDiamondText()
        {
            diamond = UserManager.Instance.CurrentUserSummary != null
                ? UserManager.Instance.CurrentUserSummary.Gem
                : AOMStoreInventory.GetDiamondBalance();
            diamondString = UnitConverter.ConverterDoubleToString(diamond);

            if (diamondText.text != diamondString)
            {
                diamondText.text = diamondString;
                float tmpDiamondTextWidth = diamondText.preferredWidth;
                if (tmpDiamondTextWidth != currentDiamondTextWidth)
                {
                    Vector3 diamondImagePosition = diamondImage.transform.localPosition;
                    diamondImagePosition.x = diamondText.transform.localPosition.x - (tmpDiamondTextWidth / 2) -
                                             diamondImage.rectTransform.sizeDelta.x;
                    diamondImagePosition.y = diamondText.transform.localPosition.y - 5;
                    diamondImage.transform.localPosition = diamondImagePosition;
                    currentDiamondTextWidth = tmpDiamondTextWidth;
                }
            }
        }

        public void GameStart()
        {
            StartCoroutine(PreloadMenu());
            GamePlayStatManager.Instance.CheckPendingRewards();
        }

        void GameOver()
        {
            gameStarted = false;
            HeroManager.instance.SetHeroesIdel();
        }

        public void ResetGame()
        {
            PlayerPrefs.DeleteAll();
            Soomla.KeyValueStorage.Purge();
            AOMStoreInventory.ResetGold(0);
            GameOver();
            GameStart();
            EventManager.ChangeLanguage();
        }

        void checkTouch(Vector2 pos)
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(pos);
            Vector2 touchPos = new Vector2(wp.x, wp.y);
            Collider2D[] colliders = Physics2D.OverlapPointAll(touchPos);
            if (colliders != null)
            {
                for (int i = 0; i < colliders.Length; i++)
                {
                    Collider2D hit = colliders[i];

                    if (hit != null)
                        hit.transform.gameObject.SendMessage("OnTap");
                }
            }
        }

        void CoinDrop()
        {
            goldPool.GetComponent<GoldPool>().GoldDrop(_goldDropCore.CurrentEnemyGoldDrop());
        }

        GameObject offlineGoldDropTips;

        public void OfflineGoldDrop()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (offlineGoldDropTips == null)
            {
                offlineGoldDropTips = _uiViewLoader.LoadTipsView("OfflineGoldDropTips", true);
            }
        }

        void RefreshStageHUD()
        {
            if (enemyObject != null)
                enemyName.GetComponent<Text>().text = LanguageManager.Instance.GetTextValue(enemyObject.name + "_name");

            // Check Stage over limited
            if (_enemyCore.Stage > _enemyCore.MaxStage)
                _enemyCore.Stage = _enemyCore.MaxStage;

            currentStageText.text = _enemyCore.Stage.ToString();

            RefreshWaveHUD();
        }

        void RefreshWaveHUD()
        {
            sb.Append(_enemyCore.Wave);
            sb.Append(" / ");
            sb.Append(_enemyCore.WavePerStage);
            // _enemyCore.Wave / _enemyCore.WavePerStage
            currentWaveText.text = sb.ToString();
            sb.Clear();
        }

        public void ShowStoryIntro()
        {
            GameObject storyIntroView = _uiViewLoader.LoadOverlayView("StoryIntroView", null, false, false);
        }

        public void ShowEndOfWorld()
        {
            GameObject endOfWorldOverlayView =
                _uiViewLoader.LoadOverlayView("EndOfWorldOverlayView", null, false, false);
        }

        public void ShowLeaderboard()
        {
            if (_socialManager != null)
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                _socialManager.ShowLeaderboard();
            }
        }

        public void ShowAchievementTable()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            GameObject achievementTableView = _uiViewLoader.LoadOverlayView("AchievementMenuView", null, false, true);
        }

        void ShowNewAchievementIcon(StatType type)
        {
            achievementButton.GetComponent<Image>().sprite =
                achievementButton.GetComponent<Button>().spriteState.pressedSprite;
        }

        public void HideNewAchievementIcon()
        {
            achievementButton.GetComponent<Image>().sprite =
                achievementButton.GetComponent<Button>().spriteState.highlightedSprite;
        }

        public void SavePlayingDuration()
        {
            GamePlayStatManager.Instance.AddPlayingDuration((DateTime.Now - gameStartTime).TotalSeconds);
        }

        void AutoSave()
        {
            AOMStoreInventory.SaveGoldValue();

            if (SocialManager.CheckForInternetConnection())
            {
                if (_socialManager.parseSignedIn)
                {
                    if (!autoSaving)
                    {
                        autoSaving = true;

                        Action saveProgressToParseSuccessCallback = () => { autoSaving = false; };

                        Action saveProgressToParseFailCallback = () => { autoSaving = false; };

                        _cloudDataManager.SaveProgressToParseUser(false, saveProgressToParseSuccessCallback,
                            saveProgressToParseFailCallback);
                    }
                }
                else
                {
                    Action parseSignUpSuccessCallback = () =>
                    {
                        autoSaving = true;

                        Action saveProgressToParseSuccessCallback = () => { autoSaving = false; };

                        Action saveProgressToParseFailCallback = () => { autoSaving = false; };

                        _cloudDataManager.SaveProgressToParseUser(false, saveProgressToParseSuccessCallback,
                            saveProgressToParseFailCallback);
                    };

                    _socialManager.ParseSignUp(parseSignUpSuccessCallback);
                }
            }
        }

        public void ShowSettingMenu()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            // Check Mailbox data
            if (SocialManager.CheckForInternetConnection())
            {
                if (_socialManager.parseSignedIn)
                {
                    StartCoroutine(LoadParseData());
                }
            }

            GameObject settingMenuView = _uiViewLoader.LoadOverlayView("SettingMainMenuView", null, false, true);
        }

        public void ShowFacebookInvitationMenu(bool showAchievementTable)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (showAchievementTable)
                ShowAchievementTable();

            GameObject facebookInvitationView = _uiViewLoader.LoadOverlayView("FacebookInvitationMenuView", null, false,
                true);
        }

        public void SwitchMusicOnOff(Text t)
        {
            SoundManager.Instance.SwitchMusicOnOff(t);
        }

        public void SwitchSoundOnOff(Text t)
        {
            SoundManager.Instance.SwitchSoundOnOff(t);
        }

        void OnArtifactVGBalanceChange(ArtifactVG avg, int balance)
        {
            if (avg.ItemId.Equals(ArtifactAssets.Artifact030.ItemId))
            {
                ReduceRechargingTime();
            }
            else if (avg.ItemId.Equals(ArtifactAssets.Artifact031.ItemId))
            {
                IncreaseTouchCreditAmount();
            }
        }

        void ReduceRechargingTime()
        {
            _galleryCore.RecalCreditRechargeTime((1 - (float) ArtifactAssets.Artifact030.EffectValue));
            _galleryCore.RecalCreditRechargeTime((1 - (float) ArtifactAssets.MoeSpirit013.EffectValue));
        }

        void IncreaseTouchCreditAmount()
        {
            _galleryCore.RecalMaxTouchCredit((int) ((float) (ArtifactAssets.Artifact031.EffectValue * 100)));
        }

        public void ShowAdMob()
        {
            if (AdMobManager.Instance != null)
            {
                AdMobManager.Instance.ShowInterstitial();
            }
        }

        void OnParseUserInvalidToken()
        {
            _socialManager.ParseUserTokenExpired = true;

            var allOverlayInParentContainer =
                GameObject.Find("Overlay_Container_Group").GetComponentsInChildren<UIViewController>();

            foreach (var overlayInParentContainer in allOverlayInParentContainer)
            {
                overlayInParentContainer.GetComponent<UIOverlayViewController>().CloseView();
            }

            GameObject msgPopupView = _uiViewLoader.LoadOverlayView("MsgPopupOverlay", null, false, true);
            MsgPopupViewController msgPopupViewController = msgPopupView.GetComponent<MsgPopupViewController>();
            msgPopupViewController.InputMsg("logout_title", "logout_msg", "logout_btn", () =>
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_IOS || UNITY_ANDROID
                    Application.Quit();
#endif
            });
        }

        public void LoadPvPScene()
        {
            loadingScene.SetActive(true);
            loadingScene.GetComponent<TipsManager>().ChangeTips();

            AutoSave();

            Application.LoadLevelAsync("PvPScene");
        }
    }
}