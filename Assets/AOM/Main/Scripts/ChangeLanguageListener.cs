﻿using UnityEngine;
using System.Collections;
using SmartLocalization;
using UnityEngine.UI;

namespace Ignite.AOM
{
	public class ChangeLanguageListener : MonoBehaviour
	{
		private static Font jaFont;//ラノベPOP
		private static Font zhFont;
		private static Font enFont;
		private static Font koFont;
		private static Font numberFont;

		public bool isImage;

		public int enFontSize;
		public int zhFontSize;
		public int jaFontSize;
		public int koFontSize;

		public string id;

		// Use this for initialization
		void Awake ()
		{
			if (isImage)
			{
				EventManager.OnLanguageChange += ChangeImage;

				ChangeImage();
			}
			else
			{
				jaFont = (Font)Resources.Load ("Fonts/ja");
				enFont = (Font)Resources.Load ("Fonts/en");
				zhFont = (Font)Resources.Load ("Fonts/zh3");
				koFont = (Font)Resources.Load ("Fonts/ko2");
				numberFont = (Font)Resources.Load ("Fonts/en");
				
				EventManager.OnLanguageChange += ChangeLanguage;
				
				if (enFontSize == 0)
					enFontSize = this.GetComponent<Text>().fontSize;
				
				if (zhFontSize == 0)
					zhFontSize = this.GetComponent<Text>().fontSize;
				
				if (jaFontSize == 0)
					jaFontSize = this.GetComponent<Text>().fontSize;

				if (koFontSize == 0)
					koFontSize = this.GetComponent<Text>().fontSize;
				
				ChangeLanguage ();
			}
		}

		void ChangeImage ()
		{
			if (id != "") {
				if (this.GetComponent<Image>() != null) {
					this.GetComponent<Image>().sprite = Resources.Load<Sprite>( LanguageManager.Instance.GetTextValue(id) );
				}
			}
		}

		void ChangeLanguage ()
		{
			// Change font
			Font font = enFont;
			int fontSize = enFontSize;
			float fontLineSpacing = 1;

			if (LanguageManager.Instance.LoadedLanguage == "en") {
				font = enFont;
				fontSize = enFontSize;
				fontLineSpacing = 1f;
			}
			if (LanguageManager.Instance.LoadedLanguage == "zh-cht" || LanguageManager.Instance.LoadedLanguage == "zh-CN") {
				font = zhFont;
				fontSize = zhFontSize;
				fontLineSpacing = 1f;
			}
			if (LanguageManager.Instance.LoadedLanguage == "ja") {
				font = jaFont;
				fontSize = jaFontSize;
				fontLineSpacing = 1.3f;
			}
			if (LanguageManager.Instance.LoadedLanguage == "ko") {
				font = koFont;
				fontSize = koFontSize;
				fontLineSpacing = 1.3f;
			}

			this.GetComponent<Text>().font = font;
			this.GetComponent<Text>().fontSize = fontSize;
			this.GetComponent<Text>().lineSpacing = fontLineSpacing;



//			if (this.GetComponent<Text>().alignment == TextAnchor.LowerCenter || this.GetComponent<Text>().alignment == TextAnchor.MiddleCenter || this.GetComponent<Text>().alignment == TextAnchor.UpperCenter)
//				this.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
//			else if (this.GetComponent<Text>().alignment == TextAnchor.LowerRight || this.GetComponent<Text>().alignment == TextAnchor.MiddleRight || this.GetComponent<Text>().alignment == TextAnchor.UpperRight)
//				this.GetComponent<Text>().alignment = TextAnchor.MiddleRight;
//			else if (this.GetComponent<Text>().alignment == TextAnchor.LowerLeft || this.GetComponent<Text>().alignment == TextAnchor.MiddleLeft || this.GetComponent<Text>().alignment == TextAnchor.UpperLeft)
//				this.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;



			if (id != "") {
				if (this.GetComponent<Text>() != null) {
					this.GetComponent<Text>().text = LanguageManager.Instance.GetTextValue( id );
				}
			}
		}

		void OnDestroy ()
		{
			EventManager.OnLanguageChange -= ChangeLanguage;

			EventManager.OnLanguageChange -= ChangeImage;
		}
	}
}