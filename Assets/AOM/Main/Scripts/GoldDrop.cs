﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Ignite.AOM
{
    public class GoldDrop : CurrencyDrop
    {
        protected override void AfterReturn()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.CoinCollect_1);
            returning = false;
            gameObject.SetActive(false);
            GoldBalanceEventBuffer.instance.AddValue(dropAmount);
        }

        protected override void OnCollect()
        {
            if (!GameManager.Instance.inPrestige)
            {
                GameObject labelObject = textObjectPool.GetPooledObject();
                TapDamageText dmgLabel = (TapDamageText) labelObject.GetComponent<TapDamageText>();

                //				labelObject.GetComponent<Text>().fontSize = 70;
                //				labelObject.GetComponent<Text>().color = new Color(1, 165/255.0f, 0);
                //				labelObject.GetComponent<Outline>().effectDistance = new Vector2(3, 3);


                labelObject.GetComponent<RectTransform>().transform.position = new Vector3(StartPoint.x, StartPoint.y, 0);

                dmgLabel.textObject.fontSize = 70;
                dmgLabel.textObject.color = new Color(1, 165 / 255.0f, 0);
                dmgLabel.textObject.GetComponent<Outline>().effectDistance = new Vector2(3, 3);
                dmgLabel.textObject.text = UnitConverter.ConverterDoubleToString(dropAmount);

                dmgLabel.SpawnForMoneyText(StartPoint);

                labelObject.SetActive(true);
            }
        }

        protected override void PauseReturn()
        {
        }
    }
}