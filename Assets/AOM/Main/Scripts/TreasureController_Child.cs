﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Ignite.AOM
{
	public class TreasureController_Child : MonoBehaviour
	{
		public TreasureController treasureController;

		public void OnTap ()
		{
			if (treasureController.fairyTrigger == false) {
				treasureController.DropTreasureBox ();
			}
		}

		public void FairyLeave ()
		{
			treasureController.FairyLeave ();
		}

	}
}