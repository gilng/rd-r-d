﻿using UnityEngine;
using System.Collections;
using Ignite.AOM;
using UnityEngine.UI;
using Soomla.Store;
using System.Collections.Generic;
using SmartLocalization;
using UnityEngine.Analytics;
using System;
using Zenject;

namespace Ignite.AOM
{
    public enum MenuCategoryType
    {
        Avatar,
        Hero,
        Artifact,
        Diamond
    }

    public class HiddenMenu : MonoBehaviour
    {
        private UIViewLoader _uiViewLoader;

        private AvatarCore _avatarCore;
        private HeroCore _heroCore;
        private ArtifactCore _artifactCore;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader, AvatarCore avatarCore, HeroCore heroCore, ArtifactCore artifactCore)
        {
            _uiViewLoader = uiViewLoader;

            _avatarCore = avatarCore;
            _heroCore = heroCore;
            _artifactCore = artifactCore;
        }

        private static HiddenMenu _instance;
        public static HiddenMenu Instance
        {
            get
            {
                return _instance;
            }
        }

        public GameObject hiddenMenu;

        public GameObject[] menuButton;

        public GameObject[] tables;

        public GameObject[] balanceLabelGroups;

        public Text[] dpsLabelGroups;

        public int currentType = -1;

        private double prevousGold;

        public GameObject artifactScrollView, artifactMsg;

        void Awake()
        {
            _instance = this;

            EventManager.OnTriggerTutorial += OnTutorialTriggered;
//            EventManager.OnTutorialCompleted += OnTutorialCompleted;

            EventManager.OnGoldPriceUpdated += RefreshTable;
            EventManager.OnGoldBalanceUpdated += RefreshTable;
            EventManager.OnLanguageChange += UpdateMenuLanguage;
            EventManager.OnRelicBalanceUpdated += UpdateRelicBalance;
            EventManager.OnDiamondBalanceUpdated += UpdateDiamondBalance;
            EventManager.OnArtifactAllDamageBuffTotalChange += UpdateArtifactAllDamageLabel;

            EventManager.OnTableRefresh += FullTableRefresh;

            //StoreEvents.OnMarketPurchase += onMarketPurchase;
        }

        void OnDestroy()
        {
            EventManager.OnTriggerTutorial -= OnTutorialTriggered;
//            EventManager.OnTutorialCompleted -= OnTutorialCompleted;

            EventManager.OnGoldPriceUpdated -= RefreshTable;
            EventManager.OnGoldBalanceUpdated -= RefreshTable;
            EventManager.OnLanguageChange -= UpdateMenuLanguage;
            EventManager.OnRelicBalanceUpdated -= UpdateRelicBalance;
            EventManager.OnDiamondBalanceUpdated -= UpdateDiamondBalance;
            EventManager.OnArtifactAllDamageBuffTotalChange -= UpdateArtifactAllDamageLabel;

            EventManager.OnTableRefresh -= FullTableRefresh;

            //StoreEvents.OnMarketPurchase -= onMarketPurchase;
        }

        // Use this for initialization
        void Start()
        {
            TutorialEventSetup();
            UpdateMenuLanguage();
        }

        void TutorialEventSetup()
        {
            if (!TutorialManager.Instance.CheckTutorialCompletedById(1))
            {
                menuButton[0].AddComponent<TutorialReceiver>().TutorialEventSetup(1);
            }

            if (!TutorialManager.Instance.CheckTutorialCompletedById(3))
            {
                menuButton[1].AddComponent<TutorialReceiver>().TutorialEventSetup(3);
            }

            foreach (GameObject button in menuButton)
            {
                if (button.GetComponent<TutorialReceiver>() == null)
                {
                    if (button.transform.FindChild("TutorialNotify_Symbol") != null)
                        button.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);
                }
            }
        }

        void UpdateRelicBalance()
        {
            if (balanceLabelGroups[1].activeInHierarchy && balanceLabelGroups[1].GetComponentInChildren<Text>() != null)
            {
                balanceLabelGroups[1].GetComponentInChildren<Text>().text = UserManager.Instance.CurrentUserSummary != null ? UnitConverter.ConverterDoubleToString(UserManager.Instance.CurrentUserSummary.Moecrystal) : UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetRelicBalance());
            }
        }

        void UpdateDiamondBalance()
        {
            if (balanceLabelGroups[2].activeInHierarchy && balanceLabelGroups[2].GetComponentInChildren<Text>() != null)
            {
                //balanceLabelGroups[2].GetComponentInChildren<Text>().text = UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetDiamondBalance());
                balanceLabelGroups[2].GetComponentInChildren<Text>().text = UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString();
            }
        }

        void UpdateArtifactAllDamageLabel(double dmg)
        {
            if (tables[2].activeInHierarchy && tables[2].GetComponentInChildren<ArtifactTableViewController>() != null && tables[2].GetComponentInChildren<ArtifactTableViewController>().m_tableView.dataSource != null)
            {
                dpsLabelGroups[2].text = string.Format(artifactAllDMG, (dmg * 100));
            }
        }

        void RefreshTableInBackground(bool fullReload)
        {
            bool goldBalanceIncreased = AOMStoreInventory.GetGoldBalance() > prevousGold ? true : false;

            if (tables[0].activeInHierarchy && tables[0].GetComponentInChildren<AvatarTableViewController>() != null && tables[0].GetComponentInChildren<AvatarTableViewController>().m_tableView.dataSource != null)
            {
                tables[0].GetComponentInChildren<AvatarTableViewController>().m_tableView.FastReloadData(goldBalanceIncreased);

                if (balanceLabelGroups[0].GetComponentInChildren<Text>().text != UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetGoldBalance()))
                    balanceLabelGroups[0].GetComponentInChildren<Text>().text = UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetGoldBalance());

                if (dpsLabelGroups[0].text != UnitConverter.ConverterDoubleToString(_avatarCore.TapDamage) + " " + avatarMenuDPS)
                    dpsLabelGroups[0].text = UnitConverter.ConverterDoubleToString(_avatarCore.TapDamage) + " " + avatarMenuDPS;
            }
            else if (tables[1].activeInHierarchy && tables[1].GetComponentInChildren<HeroTableViewController>() != null && tables[1].GetComponentInChildren<HeroTableViewController>().m_tableView.dataSource != null)
            {
                tables[1].GetComponentInChildren<HeroTableViewController>().m_tableView.FastReloadData(goldBalanceIncreased);

                if (balanceLabelGroups[0].GetComponentInChildren<Text>().text != UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetGoldBalance()))
                    balanceLabelGroups[0].GetComponentInChildren<Text>().text = UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetGoldBalance());

                if (dpsLabelGroups[1].text != UnitConverter.ConverterDoubleToString(_heroCore.TotalHeroDPS) + " " + heroMenuDPS)
                    dpsLabelGroups[1].text = UnitConverter.ConverterDoubleToString(_heroCore.TotalHeroDPS) + " " + heroMenuDPS;
            }
            else if (fullReload)
            {
                if (tables[2].activeInHierarchy && tables[2].GetComponentInChildren<ArtifactTableViewController>() != null && tables[2].GetComponentInChildren<ArtifactTableViewController>().m_tableView.dataSource != null)
                {
                    tables[2].GetComponentInChildren<ArtifactTableViewController>().m_tableView.FastReloadData(goldBalanceIncreased);
                    balanceLabelGroups[1].GetComponentInChildren<Text>().text = UserManager.Instance.CurrentUserSummary != null ? UnitConverter.ConverterDoubleToString(UserManager.Instance.CurrentUserSummary.Moecrystal) : UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetRelicBalance());
                    dpsLabelGroups[2].text = string.Format(artifactAllDMG, (_artifactCore.AllDamageBuffTotal * 100));

                    CheckMoetifactOwned();
                }
                else if (tables[3].activeInHierarchy && tables[3].GetComponentInChildren<DiamondTableViewController>() != null && tables[3].GetComponentInChildren<DiamondTableViewController>().m_tableView.dataSource != null)
                {
                    tables[3].GetComponentInChildren<DiamondTableViewController>().m_tableView.FastReloadData(goldBalanceIncreased);
                    balanceLabelGroups[2].GetComponentInChildren<Text>().text = UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString();
                }
            }

            prevousGold = AOMStoreInventory.GetGoldBalance();

        }

        void OnTutorialTriggered(int triggeredTutorialId)
        {
            switch (triggeredTutorialId)
            {
                case 1:
                    if(hiddenMenu.activeSelf)
                        CloseHiddenMenu(false);
                    break;
                case 2:
                    if(!hiddenMenu.activeSelf)
                        ShowHiddenMenu(0);
                    break;
                case 3:
                    if(hiddenMenu.activeSelf)
                        CloseHiddenMenu(false);
                    break;
                case 4:
                    if(!hiddenMenu.activeSelf)
                        ShowHiddenMenu(1);
                    break;
            }
        }

        void ResetAvatarAndHeroTable()
        {
            RefreshTableInBackground(false);
        }

        void RefreshTable()
        {
            if (hiddenMenu.activeSelf)
            {
                RefreshTableInBackground(false);
            }
        }

        void FullTableRefresh()
        {
            if (hiddenMenu.activeSelf)
            {
                RefreshTableInBackground(true);
            }
        }

        string avatarMenuDPS;
        string heroMenuDPS;
        string artifactAllDMG;
        void UpdateMenuLanguage()
        {
            avatarMenuDPS = LanguageManager.Instance.GetTextValue("avatarMenu_dps");
            dpsLabelGroups[0].text = UnitConverter.ConverterDoubleToString(_avatarCore.TapDamage) + " " + avatarMenuDPS;

            heroMenuDPS = LanguageManager.Instance.GetTextValue("heroMenu_dps");
            dpsLabelGroups[1].text = UnitConverter.ConverterDoubleToString(_heroCore.TotalHeroDPS) + " " + heroMenuDPS;

            artifactAllDMG = LanguageManager.Instance.GetTextValue("Artifact_all_dmg");
            dpsLabelGroups[2].text = string.Format(artifactAllDMG, (_artifactCore.AllDamageBuffTotal * 100));

            if (hiddenMenu.activeSelf)
            {
                if (tables[2].activeInHierarchy && tables[2].GetComponentInChildren<ArtifactTableViewController>() != null && tables[2].GetComponentInChildren<ArtifactTableViewController>().m_tableView.dataSource != null)
                {
                    tables[2].GetComponentInChildren<ArtifactTableViewController>().m_tableView.FastReloadData(false);
                }

                if (tables[3].activeInHierarchy && tables[3].GetComponentInChildren<DiamondTableViewController>() != null && tables[3].GetComponentInChildren<DiamondTableViewController>().m_tableView.dataSource != null)
                {
                    tables[3].GetComponentInChildren<DiamondTableViewController>().m_tableView.FastReloadData(false);
                }
            }
        }

        public void PlayButtonSound() // Use by 4 tabs
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
        }

        public void ShowHiddenMenu(int type)
        {
            if (!hiddenMenu.activeSelf)
            {
                hiddenMenu.SetActive(true);
                EventManager.HiddenMenuShow(true);
            }

            if (currentType != type)
            {
                for (int i = 0; i < tables.Length; i++)
                {
                    if (i == type)
                    {
                        if (tables[i] != null)
                            tables[i].SetActive(true);

                        if (menuButton[i].gameObject.GetComponent<TutorialReceiver>() != null)
                        {
                            if (menuButton[i].gameObject.GetComponent<TutorialReceiver>().isTriggered)
                            {
                                TutorialManager.Instance.UpdateTutorialProgress(menuButton[i].gameObject.GetComponent<TutorialReceiver>().tutorialId);

                                switch (menuButton[i].gameObject.GetComponent<TutorialReceiver>().tutorialId)
                                {
                                    case 1: // Tutorial Event 02 Trigger
                                        if (!TutorialManager.Instance.CheckTutorialCompletedById(2))
                                        {
                                            TutorialManager.Instance.TriggerTutorial(2);
                                        }
                                        break;

                                    case 3: // Tutorial Event 04 Trigger
                                        if (!TutorialManager.Instance.CheckTutorialCompletedById(4))
                                        {
                                            TutorialManager.Instance.TriggerTutorial(4);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (tables[i] != null)
                            tables[i].SetActive(false);
                    }
                }

                balanceLabelGroups[0].GetComponentInChildren<Text>().text = UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetGoldBalance());
                balanceLabelGroups[1].GetComponentInChildren<Text>().text = UserManager.Instance.CurrentUserSummary != null ? UnitConverter.ConverterDoubleToString(UserManager.Instance.CurrentUserSummary.Moecrystal) : UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetRelicBalance());
                balanceLabelGroups[2].GetComponentInChildren<Text>().text = UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString();

                balanceLabelGroups[0].SetActive(type == 0 || type == 1);
                balanceLabelGroups[1].SetActive(type == 2);
                balanceLabelGroups[2].SetActive(type == 3);

                dpsLabelGroups[0].gameObject.SetActive(type == 0);
                dpsLabelGroups[1].gameObject.SetActive(type == 1);
                dpsLabelGroups[2].gameObject.SetActive(type == 2);

                for (int i = 0; i < menuButton.Length; i++)
                {
                    menuButton[i].GetComponent<Image>().sprite = (type == i) ? menuButton[i].GetComponent<Button>().spriteState.pressedSprite : menuButton[i].GetComponent<Button>().spriteState.highlightedSprite;
                }

                currentType = type;
            }

            FullTableRefresh();
        }

        public void CloseHiddenMenu(bool SFX = true)
        {
            if (hiddenMenu.activeSelf)
            {
                CurrentPlayerValuePanel.Instance.UpdateTapDamage(UnitConverter.ConverterDoubleToString(_avatarCore.TapDamage));
                CurrentPlayerValuePanel.Instance.UpdateHeroDPS(UnitConverter.ConverterDoubleToString(_heroCore.TotalHeroDPS));

                if (SFX)
                    if (SoundManager.Instance != null)
                        SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                hiddenMenu.SetActive(false);
                EventManager.HiddenMenuShow(false);

                menuButton[0].GetComponent<Image>().sprite = menuButton[0].GetComponent<Button>().spriteState.highlightedSprite;
                menuButton[1].GetComponent<Image>().sprite = menuButton[1].GetComponent<Button>().spriteState.highlightedSprite;
                menuButton[2].GetComponent<Image>().sprite = menuButton[2].GetComponent<Button>().spriteState.highlightedSprite;
                menuButton[3].GetComponent<Image>().sprite = menuButton[3].GetComponent<Button>().spriteState.highlightedSprite;
            }
        }

        public void OpenShopMenu()
        {
            _uiViewLoader.LoadOverlayView("ShopMenu", null, false, false);
        }

        public void OpenPvPMenu()
        {
            CloseHiddenMenu();

            if (SocialManager.CheckForInternetConnection())
            {
                if(GameManager.Instance.blockScene.activeSelf)
                    GameManager.Instance.blockScene.SetActive(false);

                if(GameManager.Instance.breakScene.activeSelf)
                    GameManager.Instance.breakScene.SetActive(false);

                menuButton[4].GetComponent<PvPMenuButtonController>().ReadNotification();

                if (PlayerPrefs.GetInt("PVP_INTRO_VIEWED", 0) == 0)
                    _uiViewLoader.LoadOverlayView("PvPIntroView", null, false, true);
                else
                    _uiViewLoader.LoadOverlayView("PvPMenu", null, false, true);
            }
            else
            {
                ShowNoInternetPopup();
            }
        }

        public void OpenMoeSpiritMenu()
        {
            CloseHiddenMenu();

            if(GameManager.Instance.blockScene.activeSelf)
                GameManager.Instance.blockScene.SetActive(false);

            if(GameManager.Instance.breakScene.activeSelf)
                GameManager.Instance.breakScene.SetActive(false);

            _uiViewLoader.LoadOverlayView("MoeSpiritTableMenuView", null, false, true);
        }

        public void UpdatePvPButtonIndicator(int receivedCount)
        {
            menuButton[4].GetComponent<PvPMenuButtonController>().UpdateNotificationIndicator(receivedCount);
        }

        public void CheckMoetifactOwned()
        {
            if (_artifactCore.FindOwnedArtifact().Count > 0)
            {
                if (artifactMsg.activeSelf != false)
                    artifactMsg.SetActive(false);

                if (artifactScrollView.GetComponent<ScrollRect>().vertical != true)
                    artifactScrollView.GetComponent<ScrollRect>().vertical = true;
            }
            else
            {
                if (artifactMsg.activeSelf != true)
                    artifactMsg.SetActive(true);

                if (artifactScrollView.GetComponent<ScrollRect>().vertical != false)
                    artifactScrollView.GetComponent<ScrollRect>().vertical = false;
            }
        }

        public void ShowNoInternetPopup()
        {
            var msgPopupView = _uiViewLoader.LoadOverlayView("MsgPopupOverlay", null, false,
                Time.timeScale == 0);
            var msgPopupViewController = msgPopupView.GetComponent<MsgPopupViewController>();
            msgPopupViewController.InputMsg(LanguageManager.Instance.GetTextValue("saveLoadWarning_title"),
                LanguageManager.Instance.GetTextValue("pvp_errorMsg_NoInternet"), "btn_ok");
        }
    }
}
