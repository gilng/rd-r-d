﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Ignite.AOM
{
    public class Relic : MonoBehaviour
    {
        private float startX = 0.59f;
        private float startY = 0.77f;

        private float CurveX;
        private float CurveY;
        private float ControlPointX;
        private float ControlPointY;

        private float BezierTime = 0;
        private float speed = 3f;

        private float waitForReturn = 4f;
        private float waitingTimer = 0f;
        private bool collected = false;

        public bool canCollect = false;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if (canCollect)
            {
                CurveX = (((1 - BezierTime) * (1 - BezierTime)) * startX) +
                         (2 * BezierTime * (1 - BezierTime) * ControlPointX) +
                         ((BezierTime * BezierTime) * GameManager.Instance.relicIcon.transform.position.x);
                CurveY = (((1 - BezierTime) * (1 - BezierTime)) * startY) +
                         (2 * BezierTime * (1 - BezierTime) * ControlPointY) +
                         ((BezierTime * BezierTime) * GameManager.Instance.relicIcon.transform.position.y);

                waitingTimer += Time.deltaTime;
                if (waitingTimer > waitForReturn)
                {
                    BezierTime = BezierTime + Time.deltaTime * speed;
                    if (BezierTime >= 1)
                    {
                        AfterReturn();
                    }
                    else
                    {
                        if (!collected)
                        {
                            OnCollect();
                            collected = true;
                        }
                        transform.position = new Vector3(CurveX, CurveY, 0);
                    }
                }
            }
        }

        public void Appear()
        {
//			Debug.Log ("RelicDrop - Appear()");
            gameObject.SetActive(true);
            transform.localPosition = new Vector2(startX, startY);
        }

        public void Disappear()
        {
            //			Debug.Log ("RelicDrop - Disappear()");
            canCollect = false;
            BezierTime = 0;
            waitingTimer = 0;
            gameObject.SetActive(false);
        }

        public void Unlock()
        {
            //			Debug.Log ("RelicDrop - Unlock()");
            canCollect = true;
        }

        private void AfterReturn()
        {
            Disappear();
            UserManager.Instance.GiveMoecrystals(1, null, false);
        }

        void OnCollect()
        {
            //			Debug.Log ("RelicDrop - OnCollect()");
            //TODO: play relic balance increase animation

            //			if(!GameManager.instance.inPrestige){
            //				GameObject labelObject = GameManager.instance.coinTextObjectPool.GetPooledObject();
            //				TapDamageText dmgLabel = (TapDamageText)labelObject.GetComponent<TapDamageText>();
            //				labelObject.GetComponent<Text>().fontSize = 70;
            //				labelObject.GetComponent<Text>().color = new Color(1, 0, 79/255.0f);
            //				labelObject.GetComponent<Outline>().effectDistance = new Vector2(3, 3);
            //
            //				dmgLabel.textObject.text = UnitConverter.ConverterDoubleToString(1);
            //				dmgLabel.SpawnForMoneyText(new Vector2(startX, startY));
            //
            //				labelObject.SetActive(true);
            //			}
        }

        public void OnTap()
        {
            if (canCollect)
            {
                waitingTimer = waitForReturn;
                ControlPointX = Random.Range(-2.5f, 2.5f);
                ControlPointY = 2.5f;
            }
        }
    }
}