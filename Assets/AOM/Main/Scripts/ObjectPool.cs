﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Ignite.AOM
{
    public class ObjectPool : MonoBehaviour
    {

        public GameObject pooledObject;
        public int poolSize = 20;
        public bool autoIncrease = true;
        public bool autoDelay = false;
        public float appearPerSecond = 0;
        private double lastPooledTime = 0;
        public List<GameObject> objectPool;
        private float pullObjectCooldown = 0;
        public float releaseObjectTime = 5f;

        // Use this for initialization
        void Start()
        {
            objectPool = new List<GameObject>();
            for (int i = 0; i < poolSize; i++)
            {
                GameObject obj = (GameObject)Instantiate(pooledObject);
                //				obj.transform.parent=transform;
                obj.transform.SetParent(transform, false);
                obj.SetActive(false);
                objectPool.Add(obj);
            }
        }

        public GameObject GetPooledObject()
        {
            if (!autoDelay)
            {
                pullObjectCooldown = 0;
                for (int i = 0; i < objectPool.Count; i++)
                {
                    if (!objectPool[i].activeInHierarchy)
                    {
                        return objectPool[i];
                    }
                }
                if (autoIncrease)
                {
                    //GameObject obj = (GameObject) Instantiate(pooledObject);
                    //obj.transform.parent = transform;
                    for (int i = 0; i < objectPool.Count; i++)
                    {
                        if (objectPool[i].activeInHierarchy)
                        {
                            GameObject obj = objectPool[i];
                            obj.transform.SetParent(transform, false);
                            obj.SetActive(false);
                            //objectPool.Add(obj);
                            return obj;
                        }
                    }
                }
            }
            else
            {
                if (UnitConverter.ConvertToTimestamp(System.DateTime.Now) - lastPooledTime > (double)(1 / appearPerSecond))
                {
                    pullObjectCooldown = 0;
                    for (int i = 0; i < objectPool.Count; i++)
                    {
                        if (!objectPool[i].activeInHierarchy)
                        {
                            lastPooledTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                            return objectPool[i];
                        }
                    }
                    if (autoIncrease)
                    {
                        //GameObject obj = (GameObject) Instantiate(pooledObject);
                        ////				obj.transform.parent=transform;
                        //obj.transform.SetParent(transform, false);
                        //obj.SetActive(false);
                        //objectPool.Add(obj);
                        //lastPooledTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                        //return obj;

                        for (int i = 0; i < objectPool.Count; i++)
                        {
                            if (objectPool[i].activeInHierarchy)
                            {
                                GameObject obj = objectPool[i];
                                obj.transform.SetParent(transform, false);
                                obj.SetActive(false);
                                //objectPool.Add(obj);
                                lastPooledTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                                return obj;
                            }
                        }
                    }


                }
            }
            return null;
        }

        public void CollectAllObjects()
        {
            pullObjectCooldown = 0;
            for (int i = 0; i < objectPool.Count; i++)
            {
                objectPool[i].SetActive(false);
            }
        }


        // Update is called once per frame
        //void Update () {
        //    pullObjectCooldown+=Time.deltaTime;
        //    if(pullObjectCooldown>releaseObjectTime){
        //        if(objectPool.Count > poolSize){
        //            int removedObjCount=0;
        //            for(int i=0;i<objectPool.Count;i++){
        //                if(removedObjCount>=objectPool.Count-poolSize){
        //                    break;
        //                }
        //                if(!objectPool[i].activeInHierarchy){
        //                    removedObjCount++;
        //                    GameObject obj= objectPool[i];
        //                    objectPool.Remove(obj);
        //                    Destroy(obj);
        //                }
        //            }	
        //        }
        //    }
        //}
    }
}
