﻿using UnityEngine;

namespace Ignite.AOM
{
    public class DiamondDrop : CurrencyDrop
    {
        protected override void AfterReturn()
        {
            returning = false;
            gameObject.SetActive(false);
            Destroy(this);
            Destroy(this.gameObject.GetComponent<CircleCollider2D>());

            GameManager.Instance.diamondPool.GetComponent<DiamondPool>().RemoveFromDroppedList(this);
        }

        protected override void OnCollect()
        {
            //GameManager.Instance.ShowDiamondIcon();
        }

        protected override void PauseReturn()
        {

        }
    }
}
