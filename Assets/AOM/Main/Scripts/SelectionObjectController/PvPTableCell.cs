﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Soomla.Store;
using Parse;
using Zenject;
using Random = UnityEngine.Random;

namespace Ignite.AOM
{
    public enum PvPStatus
    {
        AvailableChallenge,
        ChallengeExpireCD,
        NextChallengeCD,
        ReceiveChallenge,
        Result_Win,
        Result_Lose
    }

    public class PvPTableCell : SelectionObjectController
    {
        public bool isNPC = false;

        public PvPStatus pvpStatus;

        public GameObject posCentent;

        public Text nameLabel;
        public Text[] resultLabel_friend, resultLabel_me;

        public Button btnChallenge;
        public Image circleTimebar, circleTimebar_bg;

        private PvPMenuViewController container;

        public ParseUser user;
        public ParseUser friendUser;

        private int latestScore_User;
        private int latestScore_Friend;
        private string latestInvitedAt;
        private string latestChallengedAt;
        private string latestChallengeStatus;

        private double challengeExpireHours;
        private double nextChallengeHours;

        private TimeSpan remain_challengeExprie, remain_nextChallenge;

        public Button btnRemoveFriend;

        public FriendRelation friendRelation;
        UserSummary _friendUserSummary;
        BattleLog _challengeRecord;

        public GameObject updatingOverlay;

        bool isSender = false;

        void Awake()
        {
            updatingOverlay.SetActive(true);

            challengeExpireHours = 4;
            nextChallengeHours = 4 * (1 - ArtifactAssets.MoeSpirit022.EffectValue);
        }

        public void InitFriendRelation(PvPMenuViewController cellContainer, FriendRelation friendRelationData,
            UserSummary friendUserSummaryData)
        {
            this.container = cellContainer;
            this.friendRelation = friendRelationData;
            this._friendUserSummary = friendUserSummaryData;

            this.user = ParseUser.CurrentUser;
            this.friendUser = this._friendUserSummary.User;

            this.nameLabel.text = this._friendUserSummary.DisplayName;

            this.isSender = true;
        }

        public void UpdateLatestBattleRecord(BattleLog latestSentData)
        {
            this._challengeRecord = latestSentData;

            RefreshCellData();
        }

        public void InitReceivedChallenge(PvPMenuViewController cellContainer, UserSummary friendUserSummaryData, BattleLog receivedData)
        {
            this.container = cellContainer;

            this._friendUserSummary = friendUserSummaryData;
            this._challengeRecord = receivedData;

            this.user = _challengeRecord.Receiver;
            this.friendUser = _challengeRecord.Sender;

            this.nameLabel.text = this._friendUserSummary.DisplayName;

            this.isSender = false;

            RefreshCellData();
        }

        public void InitNPC(PvPMenuViewController cellContainer)
        {
            isNPC = true;

            this.container = cellContainer;

            this.nameLabel.text = LanguageManager.Instance.GetTextValue("pvp_dummy_player");

            this.latestScore_User = PlayerPrefs.GetInt("PVP_DUMMY_SCORE_USER", 0);
            this.latestScore_Friend = PlayerPrefs.GetInt("PVP_DUMMY_SCORE_NPC", 0);
            this.latestInvitedAt = DateTime.MinValue.ToString();
            this.latestChallengedAt = PlayerPrefs.GetString("PVP_DUMMY_CHALLENGE_TIME", DateTime.MinValue.ToString());
            this.latestChallengeStatus = PlayerPrefs.GetString("PVP_DUMMY_STATUS", PvPStatus.AvailableChallenge.ToString());

            RefreshCellData();
        }

        void RefreshCellData()
        {
            if (!isNPC)
            {
                if (_challengeRecord != null)
                {
                    this.latestInvitedAt = _challengeRecord.CreatedAt.ToString();
                    this.latestChallengedAt = _challengeRecord.ReceivedAt.ToString();

                    this.latestScore_User = isSender ? _challengeRecord.SenderScore : _challengeRecord.ReceiverScore;
                    this.latestScore_Friend = isSender ? _challengeRecord.ReceiverScore : _challengeRecord.SenderScore;

                    if (isSender)
                    {
                        if (_challengeRecord.Expired)
                        {
                            if (!_challengeRecord.SenderClaim)
                            {
                                if (_challengeRecord.SenderWin && _challengeRecord.ReceivedAt != DateTime.MinValue)
                                    this.latestChallengeStatus = "Result_Win";
                                else if (!_challengeRecord.SenderWin && _challengeRecord.ReceivedAt != DateTime.MinValue)
                                    this.latestChallengeStatus = "Result_Lose";
                                else
                                    this.latestChallengeStatus = "AvailableChallenge";
                            }
                            else
                            {
                                this.latestChallengeStatus = "AvailableChallenge";
                            }
                        }
                        else
                        {
                            if (!_challengeRecord.SenderClaim)
                            {
                                if (_challengeRecord.SenderWin && _challengeRecord.ReceivedAt != DateTime.MinValue)
                                {
                                    this.latestChallengeStatus = "Result_Win";
                                }
                                else if (!_challengeRecord.SenderWin && _challengeRecord.ReceivedAt != DateTime.MinValue)
                                {
                                    this.latestChallengeStatus = "Result_Lose";
                                }
                                else
                                {
                                    if (_challengeRecord.ReceivedAt == DateTime.MinValue)
                                        this.latestChallengeStatus = "ChallengeExpireCD";
                                    else
                                        this.latestChallengeStatus = "NextChallengeCD";
                                }
                            }
                            else
                            {
                                this.latestChallengeStatus = "NextChallengeCD";
                            }
                        }
                    }
                    else
                    {
                        if (_challengeRecord.Expired)
                        {
                            if (!_challengeRecord.ReceiverClaim)
                            {
                                if (!_challengeRecord.SenderWin && _challengeRecord.ReceivedAt != DateTime.MinValue)
                                    this.latestChallengeStatus = "Result_Win";
                                else if (_challengeRecord.SenderWin && _challengeRecord.ReceivedAt != DateTime.MinValue)
                                    this.latestChallengeStatus = "Result_Lose";
                            }
                        }
                        else
                        {
                            if (!_challengeRecord.ReceiverClaim)
                            {
                                if (!_challengeRecord.SenderWin && _challengeRecord.ReceivedAt != DateTime.MinValue)
                                    this.latestChallengeStatus = "Result_Win";
                                else if (_challengeRecord.SenderWin && _challengeRecord.ReceivedAt != DateTime.MinValue)
                                    this.latestChallengeStatus = "Result_Lose";
                                else
                                    this.latestChallengeStatus = "ReceiveChallenge";
                            }
                        }
                    }
                }
                else
                {
                    this.latestInvitedAt = DateTime.MinValue.ToString();
                    this.latestChallengedAt = DateTime.MinValue.ToString();

                    this.latestScore_User = 0;
                    this.latestScore_Friend = 0;

                    this.latestChallengeStatus = "AvailableChallenge";
                }
            }

            InitialSetup();

            updatingOverlay.SetActive(false);
        }

        void InitialSetup()
        {
            this.posCentent.transform.localPosition = new Vector3(0, -5, 0);
            this.btnRemoveFriend.gameObject.SetActive(false);

            char[] _friendScore = this.latestScore_Friend.ToString("000").ToCharArray();
            char[] _myScore = this.latestScore_User.ToString("000").ToCharArray();

            for (int i = 0; i < 3; i++)
            {
                this.resultLabel_friend[i].text = _friendScore[i].ToString();
                this.resultLabel_me[i].text = _myScore[i].ToString();
            }

            if (this.latestChallengeStatus != "" && this.latestChallengeStatus != null)
            {
                ChangePvPStatus((PvPStatus) Enum.Parse(typeof(PvPStatus), this.latestChallengeStatus));

                if (this.pvpStatus == PvPStatus.ChallengeExpireCD)
                    if (this.latestInvitedAt != "" && DateTime.Parse(this.latestInvitedAt).AddHours(challengeExpireHours) >= DateTime.UtcNow)
                        this.circleTimebar.fillAmount = (float)((DateTime.Parse(this.latestInvitedAt).AddHours(challengeExpireHours) - DateTime.UtcNow).TotalSeconds / 14400);

                if (this.pvpStatus == PvPStatus.NextChallengeCD)
                    if (this.latestChallengedAt != "" && DateTime.Parse(this.latestChallengedAt).AddHours(nextChallengeHours) >= DateTime.UtcNow)
                        this.circleTimebar.fillAmount = (float)((DateTime.Parse(this.latestChallengedAt).AddHours(nextChallengeHours) - DateTime.UtcNow).TotalSeconds / 14400);
            }
        }

        void ChangePvPStatus(PvPStatus _status)
        {
            this.pvpStatus = _status;

            this.btnChallenge.GetComponent<Image>().sprite = this.container.sp_actionBtn[_status];

            if (this.isNPC)
                PlayerPrefs.SetString("PVP_DUMMY_STATUS", this.pvpStatus.ToString());

            if (this.pvpStatus == PvPStatus.ChallengeExpireCD || this.pvpStatus == PvPStatus.NextChallengeCD)
            {
                if (!this.circleTimebar_bg.gameObject.activeSelf)
                {
                    this.circleTimebar_bg.gameObject.SetActive(true);
                }
            }
            else
            {
                if (this.circleTimebar_bg.gameObject.activeSelf)
                {
                    this.circleTimebar_bg.gameObject.SetActive(false);
                    this.circleTimebar.fillAmount = 0;
                }
            }
        }

        void Update()
        {
            if (this.pvpStatus == PvPStatus.ChallengeExpireCD)
            {
                if (this.latestInvitedAt == "")
                {
                    ChangePvPStatus(PvPStatus.AvailableChallenge);
                }
                else
                {
                    DateTime _latestInvitedAt = DateTime.Parse(this.latestInvitedAt).AddHours(challengeExpireHours);

                    if (_latestInvitedAt >= DateTime.UtcNow)
                    {
                        this.remain_challengeExprie = _latestInvitedAt - DateTime.UtcNow;

                        this.circleTimebar.fillAmount = (float) (this.remain_challengeExprie.TotalSeconds / 14400);
                    }
                    else
                    {
                        ResetChallengeCD();
                    }
                }
            }

            if (this.pvpStatus == PvPStatus.NextChallengeCD)
            {
                if (this.latestChallengedAt == "")
                {
                    ChangePvPStatus(PvPStatus.AvailableChallenge);
                }
                else
                {
                    DateTime _latestChallengedAt = DateTime.Parse(this.latestChallengedAt).AddHours(nextChallengeHours);

                    if (_latestChallengedAt >= DateTime.UtcNow)
                    {
                        this.remain_nextChallenge = _latestChallengedAt - DateTime.UtcNow;

                        this.circleTimebar.fillAmount = (float) (remain_nextChallenge.TotalSeconds / 14400);
                    }
                    else
                    {
                        ResetChallengeCD();
                    }
                }
            }
        }

        // Button Press
        public void PvP_Action()
        {
            switch (pvpStatus)
            {
                case PvPStatus.AvailableChallenge:

                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                    this.container.InitChallenge(this.friendUser, this.nameLabel.text, null);

                    break;

                case PvPStatus.ChallengeExpireCD:

                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                    this.container.OpenWaitingReceiveCDPanel(remain_challengeExprie);

                    break;

                case PvPStatus.NextChallengeCD:

                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                    this.container.OpenResetChallengeCDPanel(remain_nextChallenge, this);

                    break;

                case PvPStatus.ReceiveChallenge:

                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                    this.container.InitChallenge(this.friendUser, this.nameLabel.text, this._challengeRecord);

                    break;

                case PvPStatus.Result_Win:
                case PvPStatus.Result_Lose:

                    if (SocialManager.CheckForInternetConnection())
                    {
                        updatingOverlay.SetActive(true);

                        if (pvpStatus == PvPStatus.Result_Win)
                        {
                            container.UpdateUserFP(10 + (int)(ArtifactAssets.MoeSpirit020.EffectValue * 100));
                        }
                        else
                        {
                            container.UpdateUserFP(5 + (int)(ArtifactAssets.MoeSpirit021.EffectValue * 100));
                        }

                        SoundManager.Instance.PlaySoundWithType(SoundType.GainFP);

                        GameObject gainFP = (GameObject) Instantiate(Resources.Load("UI/Prefab/EffectObject/GainFP_effect"));
                        gainFP.transform.SetParent(container.transform);

                        GainFPAnimationEffect gainFP_eff = gainFP.GetComponent<GainFPAnimationEffect>();
                        gainFP_eff.InitEffect(btnChallenge.transform.position, container.fpPos.position, new Vector2(0.7f, 1.5f));

                        if (!isNPC)
                        {
                            this.container.AcceptChallengeResult(_challengeRecord.ObjectId, isSender);

                            if (isSender)
                            {
                                if (DateTime.Parse(this.latestChallengedAt).AddHours(nextChallengeHours) <= DateTime.UtcNow)
                                {
                                    ResetChallengeCD();
                                }
                                else
                                {
                                    ChangePvPStatus(PvPStatus.NextChallengeCD);
                                }
                            }
                            else
                            {
                                this.container.RemoveBattleRecord(this);
                            }
                        }
                        else
                        {
                            if (DateTime.Parse(this.latestChallengedAt).AddHours(nextChallengeHours) <= DateTime.UtcNow)
                            {
                                ChangePvPStatus(PvPStatus.AvailableChallenge);
                            }
                            else
                            {
                                ChangePvPStatus(PvPStatus.NextChallengeCD);
                            }
                        }

                        updatingOverlay.SetActive(false);
                    }
                    else
                    {
                        this.container.ShowNoInternetPopup();
                    }

                    break;
            }
        }

        public void ResetChallengeCD()
        {
            if (isNPC)
            {
                ChangePvPStatus(PvPStatus.AvailableChallenge);
                PlayerPrefs.SetString("PVP_DUMMY_CHALLENGE_TIME", DateTime.MinValue.ToString());
            }
            else
            {
                if (SocialManager.CheckForInternetConnection())
                {
                    ChangePvPStatus(PvPStatus.AvailableChallenge);
                    this.container.SetChallengeExpired(_challengeRecord.ObjectId);
                }
            }
        }

        public void ShowRemoveButton()
        {
            if (!isNPC)
            {
                if (!this.btnRemoveFriend.gameObject.activeSelf)
                {
                    this.btnRemoveFriend.gameObject.SetActive(true);
                    this.posCentent.transform.localPosition = new Vector3(50, -5, 0);
                }
                else
                {
                    this.btnRemoveFriend.gameObject.SetActive(false);
                    this.posCentent.transform.localPosition = new Vector3(0, -5, 0);
                }
            }
        }

        public void RemoveThisFriend()
        {
            container.OpenRemoveFriendPanel(this);
        }
    }
}