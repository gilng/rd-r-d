﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Soomla.Store;
using SmartLocalization;

namespace Ignite.AOM
{
    public class PromotionalPackTableCell : SelectionObjectController
    {
        public PromotionalOfferViewController containerTableViewController;

        public Text itemName;
        public Text itemDesc;
        public Image icon;

        private StringBuilder sb = new StringBuilder();

        public void Init(PromotionalOfferViewController containerTableViewController, string itemId)
        {
            this.containerTableViewController = containerTableViewController;

            sb.Clear();
            sb.Append(itemId);
            sb.Append("_name");
            itemName.text = LanguageManager.Instance.GetTextValue(sb.ToString());

            sb.Clear();

            if (itemId.Contains("AR"))
            {
                ArtifactVG moetifactItem = ArtifactAssets.Artifacts.First(a => a.ItemId == itemId);

                sb.Append(string.Format(LanguageManager.Instance.GetTextValue(itemId + "_skill_desc"), (moetifactItem.BaseEffectValue * 100).ToString()));
                sb.Append("\n");
                sb.Append(string.Format(LanguageManager.Instance.GetTextValue("Artifact_all_dmg"), (moetifactItem.BaseAllDamageValue * 100).ToString()));

                itemDesc.text = sb.ToString();
            }
            else
            {
                
                sb.Append(itemId);
                sb.Append("_desc");

                itemDesc.text = LanguageManager.Instance.GetTextValue(sb.ToString());
            }

            string iconName = "";

            if (itemId.Contains("diamond"))
                iconName = "Gems";
            else if (itemId.Contains("gold"))
                iconName = "Gold";
            else if (itemId.Contains("moeCrystal"))
                iconName = "MoeCrystal";
            else if (itemId.Contains("no_ads"))
                iconName = "AdFree";
            else if (itemId.Contains("AR"))
                iconName = itemId;

            icon.sprite = containerTableViewController.thumbnail[iconName];
        }
    }
}
