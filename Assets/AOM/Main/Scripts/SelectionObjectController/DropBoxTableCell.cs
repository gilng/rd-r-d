﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Soomla.Store;
using SmartLocalization;
using Parse;
using Zenject;

namespace Ignite.AOM
{
    public enum DropBoxItemType
    {
        AdFree,
        MoetanPackages,
        Moetifacts,
        Gold,
        Gems,
        MoeCrystal
    }

    public class DropBoxTableCell : SelectionObjectController
    {
        private ArtifactCore _artifactCore;

        [Inject]
        public void ConstructorSafeAttribute(ArtifactCore artifactCore)
        {
            _artifactCore = artifactCore;
        }

        private DropBoxTableViewController container;
        private DropBoxItem data;

        public DropBoxItem Data { get { return data; } }

        private DropBoxItemType itemType;

        public Text itemTitle;
        public Text itemDesc;
        public Text itemTimeRemain;
        public Text itemBtnLabel;

        public Image itemIcon;

        public Button btnCollect;

        private Sprite[] thumbnailIcon;
        private string[] thumbnailIconName;



        public void Init(DropBoxTableViewController container, DropBoxItem data)
        {
            this.container = container;
            this.data = data;

            itemType = (DropBoxItemType)System.Enum.Parse(typeof(DropBoxItemType), data.Type);

            itemTitle.text = (data.Title != null) ? data.Title[LanguageManager.Instance.LoadedLanguage] : LanguageManager.Instance.GetTextValue("dropbox_item_freeGift");

            TimeSpan ts = DateTime.Parse(data.CreatedAt.ToString()).AddDays(7).Subtract(DateTime.UtcNow);

            itemTimeRemain.text = string.Format(LanguageManager.Instance.GetTextValue("dropbox_item_timeRemain"), ts.Days, ts.Hours.ToString("00"), ts.Minutes.ToString("00"), ts.Seconds.ToString("00"));

            itemBtnLabel.text = LanguageManager.Instance.GetTextValue((data.Msg == null || data.Msg == "") ? "dropbox_item_collectBtn" : "dropbox_item_collectBtn_view");

            switch (itemType)
            {
                case DropBoxItemType.AdFree:
                    LoadThumbnailIcon("Sprites/table_ui_dropbox/table_ui_dropbox");
                    itemIcon.sprite = thumbnailIcon[Array.IndexOf(thumbnailIconName, data.Type)];
                    itemDesc.text = LanguageManager.Instance.GetTextValue("dropbox_item_" + itemType);
                    break;

                case DropBoxItemType.Gems:
                case DropBoxItemType.Gold:
                case DropBoxItemType.MoeCrystal:
                    LoadThumbnailIcon("Sprites/table_ui_dropbox/table_ui_dropbox");
                    itemIcon.sprite = thumbnailIcon[Array.IndexOf(thumbnailIconName, data.Type)];
                    itemDesc.text = string.Format(LanguageManager.Instance.GetTextValue("dropbox_item_" + itemType), data.Value);
                    break;

                case DropBoxItemType.MoetanPackages:
                    LoadThumbnailIcon("Sprites/table_ui_diamond/table_ui_diamond");
                    itemIcon.sprite = thumbnailIcon[Array.IndexOf(thumbnailIconName, data.Value)];
                    itemDesc.text = string.Format(LanguageManager.Instance.GetTextValue("dropbox_item_" + itemType), LanguageManager.Instance.GetTextValue(data.Value + "_name"));
                    break;

                case DropBoxItemType.Moetifacts:
                    LoadThumbnailIcon("Sprites/overlay_icon_artifact/overlay_icon_artifact");
                    itemIcon.sprite = thumbnailIcon[Array.IndexOf(thumbnailIconName, data.Value)];
                    itemDesc.text = string.Format(LanguageManager.Instance.GetTextValue("dropbox_item_" + itemType), LanguageManager.Instance.GetTextValue(data.Value + "_name"));
                    break;
            }

            btnCollect.GetComponent<Image>().sprite = (data.Msg == null || data.Msg == "") ? btnCollect.spriteState.highlightedSprite : btnCollect.spriteState.pressedSprite;
        }

        void LoadThumbnailIcon(string path)
        {
            thumbnailIcon = Resources.LoadAll<Sprite>(path);
            thumbnailIconName = new string[thumbnailIcon.Length];

            for (int i = 0; i < thumbnailIconName.Length; i++)
            {
                thumbnailIconName[i] = thumbnailIcon[i].name;
            }
        }

        public void CollectItem()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (data.Msg == null || data.Msg == "")
            {
                GiveItem();
            }
            else
            {
                container.LoadAttachment(this, itemIcon.sprite, data.Msg);
            }
        }

        public void GiveItem()
        {
            if (container.CollectItemChecking())
            {
                switch (itemType)
                {
                    case DropBoxItemType.AdFree:
                        AdMobManager.Instance.ShouldNoADs = true;

                        ShowConsumableItemCollectedPopup(itemIcon.sprite, itemDesc.text);

                        container.RemoveParseData(this, data);

                        EventManager.TableRefresh();
                        break;

                    case DropBoxItemType.Gems:
                        UserManager.Instance.GiveGems(int.Parse(data.Value), () =>
                        {
                            ShowConsumableItemCollectedPopup(itemIcon.sprite, itemDesc.text);

                            container.RemoveParseData(this, data);

                            EventManager.TableRefresh();
                        });
                        break;

                    case DropBoxItemType.Gold:
                        AOMStoreInventory.GiveGold(Double.Parse(data.Value));

                        ShowConsumableItemCollectedPopup(itemIcon.sprite, itemDesc.text);

                        container.RemoveParseData(this, data);

                        EventManager.TableRefresh();
                        break;

                    case DropBoxItemType.MoeCrystal:
                        UserManager.Instance.GiveMoecrystals(int.Parse(data.Value), () =>
                        {
                            ShowConsumableItemCollectedPopup(itemIcon.sprite, itemDesc.text);

                            container.RemoveParseData(this, data);

                            EventManager.TableRefresh();
                        });
                        break;

                    case DropBoxItemType.MoetanPackages:
                        GameItemVG packageKeyVG = AOMAssets.GetGameItemVGFromItemId(data.Value);
                        packageKeyVG.Give(1);

                        if (GameManager.Instance != null)
                        {
                            GameManager.Instance.enemyPool.KeyUnlock(data.Value);
                        }

                        ShowGameItemCollectedPopup(packageKeyVG, GachaType.Purchase);

                        container.RemoveParseData(this, data);

                        EventManager.TableRefresh();
                        break;

                    case DropBoxItemType.Moetifacts:
                        ArtifactVG moetifactVG = _artifactCore.GiveById(data.Value);

                        ShowGameItemCollectedPopup(moetifactVG, GachaType.Moetifact);

                        container.RemoveParseData(this, data);

                        EventManager.TableRefresh();
                        break;
                }
            }
        }

        public void ShowConsumableItemCollectedPopup(Sprite itemIcon, string itemDesc = "")
        {
            GachaResultViewController itemViewController = container.LoadOverlayViewController("GachaResultMenuView", false, Time.timeScale == 0) as GachaResultViewController;

            itemViewController.ShowGachaEffect(itemIcon, itemDesc);
        }

        public void ShowGameItemCollectedPopup(AOMCachedBalanceVG itemVG, GachaType gachaType)
        {
            GachaResultViewController itemViewController = container.LoadOverlayViewController("GachaResultMenuView", false, Time.timeScale == 0) as GachaResultViewController;

            itemViewController.SetItemId(itemVG, gachaType);
        }
    }
}
