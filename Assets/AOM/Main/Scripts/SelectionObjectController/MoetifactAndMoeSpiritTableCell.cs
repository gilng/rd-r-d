﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Zenject;

namespace Ignite.AOM
{
    public class MoetifactAndMoeSpiritTableCell : SelectionObjectController
    {
        private UIViewLoader _uiViewLoader;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader)
        {
            _uiViewLoader = uiViewLoader;
        }

        public GameObject levelGroup;
        public Text levelText;

        public GameObject rareLevelStar1;
        public GameObject rareLevelStar2;
        public GameObject rareLevelStar3;

        public ArtifactVG item;
        public bool isMoetifact;

        public Image rareStarBorder;

        public void InitItem()
        {
            levelGroup.SetActive(this.item.GetBalance() > 0);
            levelText.text = (this.item.GetBalance() == this.item.MaxLevel) ? "LV.Max" : "LV." + this.item.GetBalance().ToString();

            rareLevelStar1.SetActive(this.item.RareLevel > 0);
            rareLevelStar2.SetActive(this.item.RareLevel > 1);
            rareLevelStar3.SetActive(this.item.RareLevel > 2);

            if (this.item.RareLevel == 1)
            {
                rareLevelStar1.transform.localPosition = new Vector3(0, 25, 0);
            }
            else if (this.item.RareLevel == 2)
            {
                rareLevelStar1.transform.localPosition = new Vector3(-40, 25, 0);
                rareLevelStar2.transform.localPosition = new Vector3(40, 25, 0);
            }
            else if (this.item.RareLevel == 3)
            {
                rareLevelStar1.transform.localPosition = new Vector3(-70, 25, 0);
                rareLevelStar2.transform.localPosition = new Vector3(0, 25, 0);
                rareLevelStar3.transform.localPosition = new Vector3(70, 25, 0);
            }

            rareLevelStar1.GetComponent<Image>().color = this.item.GetBalance() > 0 ? new Color(1, 1, 1, 1) : new Color(0.5f, 0.5f, 0.5f, 1);
            rareLevelStar2.GetComponent<Image>().color = this.item.GetBalance() > 0 ? new Color(1, 1, 1, 1) : new Color(0.5f, 0.5f, 0.5f, 1);
            rareLevelStar3.GetComponent<Image>().color = this.item.GetBalance() > 0 ? new Color(1, 1, 1, 1) : new Color(0.5f, 0.5f, 0.5f, 1);

            rareStarBorder.enabled = isMoetifact;
        }

        public void OpenDetailMenu()
        {
            if (item != null)
            {
                GameObject detailMenuView = _uiViewLoader.LoadOverlayView("MoetifactAndMoeSpiritDetailView", null, false, Time.timeScale == 0);
                detailMenuView.GetComponent<MoetifactAndMoeSpiritDetailViewController>().SetDisplayItem(item, isMoetifact, true);
            }
        }
    }
}