﻿using UnityEngine;
using System;
using Tacticsoft;
using Zenject;

namespace Ignite.AOM
{
    public class GlobalInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindAllInterfacesAndSelf<AOMCachedBalanceVG>().To<AOMCachedBalanceVG>().AsSingle();
            Container.BindAllInterfacesAndSelf<AOMStoreInventory>().To<AOMStoreInventory>().AsSingle();

            Container.BindAllInterfacesAndSelf<AvatarCore>().To<AvatarCore>().AsSingle();
            Container.BindAllInterfacesAndSelf<AvatarSkillCore>().To<AvatarSkillCore>().AsSingle();

            Container.BindAllInterfacesAndSelf<HeroCore>().To<HeroCore>().AsSingle();
            Container.BindAllInterfacesAndSelf<EnemyCore>().To<EnemyCore>().AsSingle();
            Container.BindAllInterfacesAndSelf<ArtifactCore>().To<ArtifactCore>().AsSingle();

            Container.BindAllInterfacesAndSelf<GoldDropCore>().To<GoldDropCore>().AsSingle();

            Container.BindAllInterfacesAndSelf<GalleryCore>().To<GalleryCore>().AsSingle();
            Container.BindAllInterfacesAndSelf<GameItemCore>().To<GameItemCore>().AsSingle();

            Container.BindFactory<GameObject, Enemy, Enemy.Factory>().FromFactory<EnemyFactory>();
            Container.BindFactory<string, EffectController, EffectController.Factory>().FromFactory<EffectControllerFactory>();

            Container.BindFactory<string, UIViewController, UIViewController.Factory>().FromFactory<UIViewControllerFactory>();

            Container.BindFactory<GameObject, TableViewCell, TableViewCell.Factory>().FromFactory<TableViewCellFactory>();

            Container.BindFactory<string, SelectionObjectController, SelectionObjectController.Factory>().FromFactory<SelectionObjectControllerFactory>();

            Container.BindAllInterfacesAndSelf<UIViewLoader>().To<UIViewLoader>().AsSingle();

            Container.Bind<AsyncProcessor>().FromGameObject().AsSingle();

            Container.Bind<IUserService>().To<UserService>().AsSingle();

            Container.Bind<IFriendService>().To<FriendService>().AsSingle();

            Container.Bind<IPvPChallengeService>().To<PvPChallengeService>().AsSingle();

            Container.BindAllInterfacesAndSelf<VersionCheckerManager>().To<VersionCheckerManager>().AsSingle();

            Container.BindAllInterfacesAndSelf<SocialManager>().To<SocialManager>().AsSingle();

            Container.BindAllInterfacesAndSelf<SaveDataService>().To<SaveDataService>().AsSingle();
            Container.BindAllInterfacesAndSelf<CloudDataManager>().To<CloudDataManager>().AsSingle();

            Container.BindAllInterfacesAndSelf<InAppPurchaseManager>().To<InAppPurchaseManager>().AsSingle();

            Container.BindAllInterfacesAndSelf<SoomlaInitializer>().To<SoomlaInitializer>().AsSingle();

            Container.BindAllInterfacesAndSelf<InitManager>().To<InitManager>().AsSingle();

            Container.BindAllInterfacesAndSelf<FriendManager>().To<FriendManager>().AsSingle();

            Debug.Log("GlobalInstaller");
        }
    }
}
