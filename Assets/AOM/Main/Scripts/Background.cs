﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class Background : MonoBehaviour
    {
        public static Background instance;

        public GameObject sky;

        public GameObject mountainA;
        public SpriteRenderer wall_a;

        public GameObject mountainB;
        public SpriteRenderer wall_b1;
        public SpriteRenderer wall_b2;

        public GameObject mountainC;
        public SpriteRenderer wall_c1;
        public SpriteRenderer wall_c2;

        public GameObject enemyPool;

        private Vector2 skyOP, mountainAOP, mountainBOP, mountainCOP, enemyPoolOP;
        private Vector2 skyEndPoint, mountainAEndPoint, mountainBEndPoint, mountainCEndPoint, enemyPoolEndPoint;
        public float duration, skyMove, mountainAMove, mountainBMove, mountainCMove, enemyPoolMove;

        private float startTime;
        private bool action = false;

        // Use this for initialization
        void Awake()
        {
            instance = this;
        }

        void Start()
        {
            skyOP = sky.transform.localPosition;
            mountainAOP = mountainA.transform.localPosition;
            mountainBOP = mountainB.transform.localPosition;
            mountainCOP = mountainC.transform.localPosition;
            enemyPoolOP = enemyPool.transform.localPosition;

            EventManager.OnHiddenMenuShow += HiddenMenuShow;
        }

        void OnDestroy()
        {
            EventManager.OnHiddenMenuShow -= HiddenMenuShow;
        }

        // Update is called once per frame
        void Update()
        {
            if (action)
            {
                sky.transform.localPosition = Vector3.Lerp(sky.transform.localPosition, skyEndPoint, (Time.time - startTime) / duration);
                mountainA.transform.localPosition = Vector3.Lerp(mountainA.transform.localPosition, mountainAEndPoint, (Time.time - startTime) / duration);
                mountainB.transform.localPosition = Vector3.Lerp(mountainB.transform.localPosition, mountainBEndPoint, (Time.time - startTime) / duration);
                mountainC.transform.localPosition = Vector3.Lerp(mountainC.transform.localPosition, mountainCEndPoint, (Time.time - startTime) / duration);
                enemyPool.transform.localPosition = Vector3.Lerp(enemyPool.transform.localPosition, enemyPoolEndPoint, (Time.time - startTime) / duration);
            }
        }

        void HiddenMenuShow(bool showing)
        {
            if (showing)
            {
                skyEndPoint = new Vector2(skyOP.x, skyOP.y + skyMove);
                mountainAEndPoint = new Vector2(mountainAOP.x, mountainAOP.y + mountainAMove);
                mountainBEndPoint = new Vector2(mountainBOP.x, mountainBOP.y + mountainBMove);
                mountainCEndPoint = new Vector2(mountainCOP.x, mountainCOP.y + mountainCMove);
                enemyPoolEndPoint = new Vector2(enemyPoolOP.x, enemyPoolOP.y + enemyPoolMove);

                startTime = Time.time;
                action = true;

                StartCoroutine(StopAction());
            }
            else
            {
                skyEndPoint = skyOP;
                mountainAEndPoint = mountainAOP;
                mountainBEndPoint = mountainBOP;
                mountainCEndPoint = mountainCOP;
                enemyPoolEndPoint = enemyPoolOP;

                startTime = Time.time;
                action = true;

                StartCoroutine(StopAction());
            }
        }

        private IEnumerator StopAction()
        {
            yield return new WaitForSeconds(duration);

            action = false;
        }

        public void ChangeBackground(int bgId, int fgId)
        {
            AssetBundleLoader.Instance.LoadSpriteAsset("background-bundle", "bg" + bgId.ToString("000"), sky.transform.GetComponent<SpriteRenderer>(), () => { });

            AssetBundleLoader.Instance.LoadSpriteAsset("background-bundle", "bg_wall" + fgId.ToString("000") + "_a", wall_a, () => { });
            AssetBundleLoader.Instance.LoadSpriteAsset("background-bundle", "bg_wall" + fgId.ToString("000") + "_b1", wall_b1, () => { });
            AssetBundleLoader.Instance.LoadSpriteAsset("background-bundle", "bg_wall" + fgId.ToString("000") + "_b2", wall_b2, () => { });
            AssetBundleLoader.Instance.LoadSpriteAsset("background-bundle", "bg_wall" + fgId.ToString("000") + "_c1", wall_c1, () => { });
            AssetBundleLoader.Instance.LoadSpriteAsset("background-bundle", "bg_wall" + fgId.ToString("000") + "_c2", wall_c2, () => { });
        }
    }
}
