using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using Zenject;

namespace Ignite.AOM
{
    public class Avatar : MonoBehaviour
    {
        private AvatarCore _avatarCore;
        private GoldDropCore _goldDropCore;

        [Inject]
        public void ConstructorSafeAttribute(AvatarCore avatarCore, GoldDropCore goldDropCore)
        {
            _avatarCore = avatarCore;
            _goldDropCore = goldDropCore;
        }

        public static Avatar instance;
        public Animator avatarAnimator;
        private bool isAtt;

        public GameObject[] heavenlyStrikeEffects;
        public GameObject shadowCloneEffect;
        public GameObject criticalShadowCloneEffect;
        public bool isUsingShadowClone;
        public bool isUsingHandOfMidas;
        public GameObject criticalStrikeEffect;
        public GameObject berserkerRageEffect;
        public GameObject[] doomEffects;

        public ObjectPool[] attackEffectPools;
        public ObjectPool[] shadowCloneEffectPools;
        public ObjectPool[] powerOfHoldingPools;

        int autoTapCount = 0;
        private bool autoTap = false;
        private double autoTapTime = 120 * 1000;
        private double remainingAutoTapTime;
        private bool checkLongPressed;
        private DateTime ptrDownTime;
        private bool longHoldStarted;

        Vector3 originalCameraPosition;
        float shakeAmt = 3.0f;
        public Camera mainCamera;

        public GameObject levelUpEffect;

        void Awake()
        {
            instance = this;
            avatarAnimator = (Animator)GetComponent("Animator");
        }

        void Start()
        {
            originalCameraPosition = mainCamera.transform.position;

            EventManager.OnPrestige += ResetAutoTap;
            EventManager.OnSuperPrestige += ResetAutoTap;
            EventManager.OnPowerOfHoldingItemActive += StartAutoTap;
            EventManager.OnDoomItemActive += StartDoomAttack;
        }

        void OnDestroy()
        {
            EventManager.OnPrestige -= ResetAutoTap;
            EventManager.OnSuperPrestige -= ResetAutoTap;
            EventManager.OnPowerOfHoldingItemActive -= StartAutoTap;
            EventManager.OnDoomItemActive -= StartDoomAttack;
        }
        void Update()
        {
            if (isAtt)
            {
                if (!autoTap)
                {
                    SoundManager.Instance.PlaySoundWithType(SoundType.AvatarAttack_1);
                    avatarAnimator.SetTrigger("atk");
                    avatarAnimator.SetInteger("atk_cnt", (avatarAnimator.GetInteger("atk_cnt") == 2 ? 0 : avatarAnimator.GetInteger("atk_cnt") + 1));
                }

                isAtt = false;
            }

            if (checkLongPressed && ptrDownTime.AddSeconds(0.125) < DateTime.Now && !longHoldStarted)
            {
                longHoldStarted = true;
                if (remainingAutoTapTime > 0)
                {
                    avatarAnimator.Play("pl_atk01_r");
                    StartCoroutine(AutoTap());
                }
            }

            if (longHoldStarted && autoTap)
            {
                remainingAutoTapTime -= (DateTime.Now - ptrDownTime).TotalMilliseconds;
                ptrDownTime = DateTime.Now;
            }
        }

        //Camera shake effect for heavenly strike
        void CameraShake()
        {
            if (shakeAmt > 0)
            {
                float quakeAmt = UnityEngine.Random.value * shakeAmt * 2 - shakeAmt;
                Vector3 pp = mainCamera.transform.position;
                pp.y += quakeAmt; // can also add to x and/or z
                mainCamera.transform.position = pp;
            }
        }

        void StopShaking()
        {
            CancelInvoke("CameraShake");
            mainCamera.transform.position = originalCameraPosition;
        }

        public void StartDoomAttack()
        {
            avatarAnimator.Play("pl_doom");
            StartCoroutine(DoomAttack());
        }

        IEnumerator DoomAttack()
        {
            yield return new WaitForSeconds(1.55f);

            shakeAmt = 0.1f;
            InvokeRepeating("CameraShake", 0, .01f);
            Invoke("StopShaking", 0.3f);
            Enemy enemy = GameManager.Instance.enemy;
            enemy.TakeDamage(enemy.MaxHp, true);
        }

        public void DoomEvent(int state)
        {
            doomEffects[state - 1].SetActive(true);

            if (state == 2)
                SoundManager.Instance.PlaySoundWithType(SoundType.doom);
        }

        public void StartAutoTap()
        {
            autoTap = true;
            remainingAutoTapTime = autoTapTime;
        }

        IEnumerator AutoTap()
        {
            while (longHoldStarted && autoTap && remainingAutoTapTime > 0)
            {
                AutoTapSountEffect();
                AttackEnemy();
                yield return new WaitForSeconds(1 / 30);
            }
            if (remainingAutoTapTime <= 0)
            {
                autoTap = false;
                autoTapCount = 0;
                avatarAnimator.Play("pl_stand");
                EventManager.GameItemVGBalabceChange();
            }
        }

        void AutoTapSountEffect()
        {
            autoTapCount++;
            if (autoTapCount > 3)
            {
                autoTapCount = 0;
                SoundManager.Instance.PlaySoundWithType(SoundType.powerOfHolding);
            }
        }

        public void ResetAutoTap()
        {
            checkLongPressed = false;
            longHoldStarted = false;
            if (autoTap)
                avatarAnimator.Play("pl_stand");
        }

        public void GameStatTapCount()
        {
            GamePlayStatManager.Instance.Tap();
        }

        public void AttackEnemy()
        {
            isAtt = true;

            ptrDownTime = DateTime.Now;
            checkLongPressed = true;

            if (isUsingHandOfMidas)
            {
                GameObject goldObject = GameManager.Instance.goldPool.GetComponent<ObjectPool>().GetPooledObject();
                goldObject.SetActive(true);
                goldObject.GetComponent<GoldDrop>().TextObjectPool = GameManager.Instance.coinTextObjectPool;
                goldObject.GetComponent<GoldDrop>().StartDrop(0, 0, -1.5f, 1.5f, 3.8f, 3.8f,
                                                              GameManager.Instance.goldImage.transform.position.x,
                                                              GameManager.Instance.goldImage.transform.position.y,
                                                              _goldDropCore.HandOfMidasGoldAmount());
            }

            if (GameManager.Instance.enemy.IsDamageable)
            {
                int criticalChance = UnityEngine.Random.Range(1, 101);
                bool isCritical = false;
                isCritical = criticalChance <= _avatarCore.CriticalChance * 100;

                if (isCritical)
                {
                    GamePlayStatManager.Instance.CriticalHit();

                    //Taking Critical Damage
                    GameManager.Instance.enemy.TakeDamage(_avatarCore.CriticalDamage, true, true);

                    CurrentPlayerValuePanel.Instance.AddCurrentDamage(_avatarCore.CriticalDamage);
                }
                else
                {
                    //Taking Normal Damage
                    GameManager.Instance.enemy.TakeDamage(_avatarCore.TapDamage, true, true);

                    CurrentPlayerValuePanel.Instance.AddCurrentDamage(_avatarCore.TapDamage);
                }
            }
        }

        public void HeavenlyStrikeEvent(int state)
        {
            heavenlyStrikeEffects[state - 1].SetActive(true);
        }

        public void HandOfMidasEnable(bool enable)
        {
            isUsingHandOfMidas = enable;
        }

        public void ShadowCloneEnable(bool enable)
        {
            isUsingShadowClone = enable;
            if (enable)
            {
                shadowCloneEffect.GetComponent<SpriteRenderer>()
                    .material.SetColor("_FlashColor", ArtifactAssets.MoeSpirit028.GetBalance() > 0 ? new Color(0.5f, 0.4f, 0f) : Color.black);

                shadowCloneEffect.SetActive(true);

                StartCoroutine(ShadowCloneAttack());
            }
            else
            {
                criticalShadowCloneEffect.SetActive(false);

                shadowCloneEffect.GetComponent<Animator>().SetBool("end", true);

                StartCoroutine(ShadowCloneEnd());
            }
        }

        IEnumerator ShadowCloneEnd()
        {
            yield return new WaitForSeconds(0.5f);
            shadowCloneEffect.SetActive(false);
        }

        IEnumerator ShadowCloneAttack()
        {
            int times = DamageFormulas.ActiveSkillShadowCloneTapsPerSecondForSkillLevel(AOMAssets.ShadowCloneASVG.GetBalance());
            double shadowCloneDamage = _avatarCore.TapDamage * (1 + ArtifactAssets.MoeSpirit027.EffectValue);

            while (isUsingShadowClone)
            {
                yield return new WaitForSeconds(1.0f / times);

                criticalShadowCloneEffect.SetActive(ArtifactAssets.MoeSpirit028.GetBalance() > 0);

                bool isCritical = false;

                if (ArtifactAssets.MoeSpirit028.GetBalance() > 0)
                {
                    int criticalChance = UnityEngine.Random.Range(1, 101);

                    isCritical = criticalChance <= _avatarCore.CriticalChance * 100;

                    if (isCritical)
                    {
                        double criticalDamage = Math.Round(
                            (shadowCloneDamage * (1 + ArtifactAssets.MoeSpirit028.EffectValue)) *
                            UnityEngine.Random.Range(3, 11), MidpointRounding.AwayFromZero);

                        CurrentPlayerValuePanel.Instance.AddCurrentDamage(criticalDamage);

                        if (isUsingShadowClone)
                            if (GameManager.Instance.enemy != null)
                                if (GameManager.Instance.enemy.IsDamageable)
                                    GameManager.Instance.enemy.TakeDamage(criticalDamage, true);
                    }
                }

                if (!isCritical)
                {
                    CurrentPlayerValuePanel.Instance.AddCurrentDamage(shadowCloneDamage);

                    if (isUsingShadowClone)
                        if (GameManager.Instance.enemy != null)
                            if (GameManager.Instance.enemy.IsDamageable)
                                GameManager.Instance.enemy.TakeDamage(shadowCloneDamage, true);
                }
            }
        }

        public void ShowCriticalStrikeEffect(bool show)
        {
            criticalStrikeEffect.SetActive(show);
        }

        public void ShowBerserkerRageEffect(bool show)
        {
            berserkerRageEffect.SetActive(show);
        }

        public bool UsingPowerOfHolding
        {
            get { return autoTap; }
        }

        public string PowerOfHoldingRemain()
        {

            int min = (int)((remainingAutoTapTime / 1000) / 60);
            int sec = (int)((remainingAutoTapTime / 1000) % 60);

            return string.Format("{0:00}:{1:00}", min, sec);
        }
    }
}
