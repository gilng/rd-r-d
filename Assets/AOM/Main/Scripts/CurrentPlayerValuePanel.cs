﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Ignite.AOM;
using UnityEngine.UI;
using Zenject;

namespace Ignite.AOM
{
    public class CurrentPlayerValuePanel : MonoBehaviour
    {
        private AvatarCore _avatarCore;
        private HeroCore _heroCore;

        [Inject]
        public void ConstructorSafeAttribute(AvatarCore avatarCore, HeroCore heroCore)
        {
            _avatarCore = avatarCore;
            _heroCore = heroCore;
        }

        public Text currentDPS, tapDamage, heroDPS;

        private static CurrentPlayerValuePanel _instance;
        public static CurrentPlayerValuePanel Instance
        {
            get
            {
                return _instance;
            }
        }

        List<double> currentDamageList;

        float timer = 0;

        int partsIndex = 10;

        void Awake()
        {
            _instance = this;

            currentDamageList = new List<double>();

            for (int i = 0; i < partsIndex; i++)
            {
                currentDamageList.Add(_heroCore.TotalHeroDPS / partsIndex);
            }
        }

        // Use this for initialization
        void Start()
        {
            RefreshAllLabel();

            EventManager.OnHeroDead += HeroDeadListener;
            EventManager.OnHeroRevive += HeroReviveListener;

            EventManager.OnPrestigeComplete += RefreshAllLabel;
        }

        void OnDestroy()
        {
            EventManager.OnHeroDead -= HeroDeadListener;
            EventManager.OnHeroRevive -= HeroReviveListener;

            EventManager.OnPrestigeComplete -= RefreshAllLabel;
        }

        void Update()
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                timer = 0.2f;

                double currentDamage = 0;

                for (int i = 0; i < currentDamageList.Count; i++)
                {
                    currentDamage += currentDamageList[i];
                }

                currentDPS.text = UnitConverter.ConverterDoubleToString(currentDamage);

                currentDamageList.RemoveAt(0);
                currentDamageList.Add(_heroCore.TotalHeroDPS / partsIndex);
            }
        }

        public void AddCurrentDamage(double inputDamage)
        {
            for (int i = 0; i < partsIndex; i++)
            {
                currentDamageList[i] += inputDamage / partsIndex;
            }
        }

        public void UpdateTapDamage(string value)
        {
            tapDamage.text = value;
        }

        public void UpdateHeroDPS(string value)
        {
            heroDPS.text = value;
        }

        public void HeroDeadListener(int heroIdx, DateTime reviveTime)
        {
            RefreshAllLabel();
        }

        public void HeroReviveListener(int heroIdx)
        {
            RefreshAllLabel();
        }

        public void RefreshAllLabel()
        {
            currentDPS.text = UnitConverter.ConverterDoubleToString(_heroCore.TotalHeroDPS);
            tapDamage.text = UnitConverter.ConverterDoubleToString(_avatarCore.TapDamage);
            heroDPS.text = UnitConverter.ConverterDoubleToString(_heroCore.TotalHeroDPS);
        }
    }
}