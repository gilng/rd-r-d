﻿using UnityEngine;
using System;
using System.Collections;

namespace Ignite.AOM
{
    public class DamageFormulas
    {
        public static double AvatarBaseDamageForLevel(int lv)
        {
            return lv * Math.Pow(1.05, lv);
        }

        public static int ActiveSkillShadowCloneTapsPerSecondForSkillLevel(int skillLV)
        {
            return (3 * (skillLV - 1)) + 7;
        }

        public static double ActiveSkillWarCryIncreaseForSkillLevel(int skillLV)
        {
            return ((double)((50 * (skillLV - 1)) + 150)) / 100;
        }

        public static double ActiveSkillBerserkerRageIncreaseForSkillLevel(int skillLV)
        {
            return ((double)((30 * (skillLV - 1) + 70)) / 100);
        }

        public static double ActiveSkillHandOfMidasForSkillLevel(int skillLV)
        {
            return ((double)((5 * (skillLV - 1)) + 15)) / 100;
        }

        public static double BuffCriticalChance()
        {
            return 0; //TODO: get the value of items which affect critical chance from store
        }

        public static double HeroBaseDPS(int heroId, int lv, double baseCost)
        {
            //          Debug.Log("hero : "+ heroId+" lv: "+lv +" HeroDPS:"+ Math.Floor(((baseCost*Math.Pow(1.075f, lv-1)* (Math.Pow(1.075f, lv) - 1)) / (0.075f) * (Math.Pow(0.904f, lv-1) * Math.Pow((1 - (0.019f *  Math.Min(heroId,15))),heroId))) * 0.1f) );
            return ((baseCost * Math.Pow(1.075, lv - 1) * (Math.Pow(1.075, lv) - 1)) / (0.075) * (Math.Pow(0.904, lv - 1) * Math.Pow((1 - (0.019 * Math.Min(heroId, 15))), heroId))) * 0.1;
        }
    }
}
