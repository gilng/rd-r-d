﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Zenject;

namespace Ignite.AOM
{
    public class Formulas
    {
        //	    public static int LevelToStage(int lv) {
        //	        return Mathf.CeilToInt(lv / 10f);
        //	    }

        //		public static int LevelToWave(int lv){
        //			return lv % 10; //TODO: artiface will affect the number of waves for each level
        //		}

        //		public static EnemyType EnemyTypeForLevel(int lv) {
        //			EnemyType t = EnemyType.Normal;
        //
        //			if(lv % 50 == 0) {
        //				t = EnemyType.BigBoss;
        //			} else if(lv % 10 == 0) {
        //				t = EnemyType.MiniBoss;
        //			}
        //			return t;
        //		}

        public static EnemyType EnemyTypeForStage(int stage, int wave, int targetWave, bool isInfinillyLoop)
        {
            EnemyType t = EnemyType.Normal;
            if (wave == targetWave && !isInfinillyLoop)
            {
                if (stage % 5 == 0)
                {
                    t = EnemyType.BigBoss;
                }
                else
                {
                    t = EnemyType.MiniBoss;
                }
            }
            return t;
        }

        public static double EnemyHPForWave(int stage, int wave, int targetWave, bool isInfinillyLoop, double afBoosHpEffect = 0)
        {
            double hp = 0;
            EnemyType t = EnemyTypeForStage(stage, wave, targetWave, isInfinillyLoop);
            //        Debug.Log ("EnemyType: " + t);
            hp = NormalEnemyHPForStage(stage);
            if (EnemyType.MiniBoss == t || EnemyType.BigBoss == t)
            {
                // TODO Different calculation for MiniBoss and BigBoss?
                hp = hp * ((stage - 1) % 5 + 1) * 2;
                hp = hp * (1 - afBoosHpEffect);
            }
            return Math.Floor(hp);
        }
        //		public static double EnemyHPForLevel(int lv) {
        //			double hp = 0;
        //			
        //			int stage = LevelToStage(lv);
        //			//        Debug.Log ("Stage: "+stage);
        //			EnemyType t = EnemyTypeForLevel(lv);
        //			//        Debug.Log ("EnemyType: " + t);
        //			hp = NormalEnemyHPForStage(stage);
        //			if(t != EnemyType.Normal) {
        //				// TODO Different calculation for MiniBoss and BigBoss?
        //				hp = hp * ((stage - 1) % 5 + 1) * 2;
        //			}
        //			
        //			return hp;
        //		} 

        //		public static double EnemyHPForLevel(int lv) {
        //	        double hp = 0;
        //
        //	        int stage = LevelToStage(lv);
        //	//        Debug.Log ("Stage: "+stage);
        //	        EnemyType t = EnemyTypeForLevel(lv);
        //	//        Debug.Log ("EnemyType: " + t);
        //	        hp = NormalEnemyHPForStage(stage);
        //	        if(t != EnemyType.Normal) {
        //	            // TODO Different calculation for MiniBoss and BigBoss?
        //	            hp = hp * ((stage - 1) % 5 + 1) * 2;
        //	        }
        //
        //	        return hp;
        //		} 

        private static double NormalEnemyHPForStage(int stage)
        {
            return 18.5 * Math.Pow(1.57, Math.Min(stage, 150)) * Math.Pow((double)1.17, Math.Max(stage - 150, 0));
        }

        public static double EnemyBasicGold(int stage)
        {
            double normalEnemyHP = NormalEnemyHPForStage(stage);
            //			Debug.LogFormat("Lv: {0}, normalEnemyHP: {1}", lv, normalEnemyHP);
            return Math.Ceiling(normalEnemyHP * (0.02 + (0.00045 * Math.Min(stage, 150))));
        }

        public static double EnemyGoldDropForLevel(int stage, EnemyType type, double heroSkillGoldDrop = 0, double afAllGoldDrop = 0, double afOnGameGoldDrop = 0,
                                                   double afBossGoldDrop = 0, double afMosterGoldDrop = 0, double af10XGold = 0
                                                   , double heroSkillTreasureChestGoldDrop = 0, double afTreasureChestGoldDrop = 0)
        {
            double gold = EnemyBasicGold(stage);
            double multiplier = 1;

            if (EnemyType.Normal == type)
            {
                gold = gold * (1 + afMosterGoldDrop);
            }
            if (EnemyType.MiniBoss == type || EnemyType.BigBoss == type)
            {
                gold = gold * ((stage - 1) % 5 + 1) * 2 * (1 + afBossGoldDrop);
            }
            if (EnemyType.TreasureChest == type)
            {
                gold = gold * 10 * (1 + heroSkillTreasureChestGoldDrop) * (1 + afTreasureChestGoldDrop);
            }
            if (EnemyType.Normal == type || EnemyType.TreasureChest == type)
            {
                if (UnityEngine.Random.Range(0.0f, 1f) <= af10XGold)
                {
                    multiplier = 10;
                }
            }
            return gold * Math.Ceiling(heroSkillGoldDrop + afAllGoldDrop + 1) * (1 + afOnGameGoldDrop) * multiplier;
        }

        public static double OfflineGoldDrop(double offlineSec, int stage, double totalHeroDPS, double heroSkillGoldDrop = 0, double afAllGoldDrop = 0)
        {
            double normalEnemyHP = NormalEnemyHPForStage(stage);
            // Offline gold maximum waiting time is 120s
            // Hero DPS large, waiting time will reduce!
            return Math.Floor(Math.Floor(Math.Min(1, totalHeroDPS / normalEnemyHP) * (Math.Min(21600, offlineSec) - 20)) * EnemyBasicGold(stage) * (1 + heroSkillGoldDrop + afAllGoldDrop));
        }

        public static double GoldToUpgradeCharacterToLevel(int lv, double baseCost, double afReduceUpgradeCost = 0)
        {
            double gold = baseCost * Math.Pow(1.075, lv - 1);
            double roundedGold = Math.Round(gold * (1 - afReduceUpgradeCost), MidpointRounding.AwayFromZero);
            //            Debug.Log (gold + ", " +roundedGold);
            return roundedGold;
        }

        public static double GoldToUpgradeAvatarActiveSkillForLevel(int lv, double baseCost, double multiplier, double afReduceUpgradeCost = 0)
        {
            double gold = baseCost * Math.Pow(multiplier, lv) * (1 - afReduceUpgradeCost);
            //            Debug.Log (gold);
            return Math.Round(gold, MidpointRounding.AwayFromZero);
        }

        public static int GoldToUpgradeArtifactForLevel(int lv, double vectorX, double vectorY)
        {
            return (int)Math.Round(vectorX * Math.Pow(lv + 1, vectorY), MidpointRounding.AwayFromZero);
        }

        public static IList<int> PrestigeRelic(int totalHeroLevel, int stage, double afBonus = 0)
        {
            List<int> result = new List<int>();
            result.Add((int)Math.Floor((Math.Floor((double)totalHeroLevel / 1000)) * (1 + afBonus)));
            result.Add((int)Math.Floor(((Math.Floor((double)(Math.Max(75, stage) - 75) / 15) * (Math.Floor((double)(Math.Max(75, stage) - 75) / 15) + 1) / 2)) * (1 + afBonus)));
            return result;
        }

    }
}
