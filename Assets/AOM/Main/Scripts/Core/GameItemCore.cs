﻿using UnityEngine;
using System.Collections;
using System;
using Zenject;

namespace Ignite.AOM
{
    public class GameItemCore : IInitializable
    {
        private DateTime guardianShieldTime;
        private const string PP_ITEM_GS_LAST_USE = "PP_ITEM_GS_LAST_USE";
        private GameItemCore()
        {
            ReadGuardianShieldTime();
        }

        // Use this for initialization
        public void Initialize()
        {
            ReadGuardianShieldTime();
        }

        private void ReadGuardianShieldTime()
        {
            double lastUseGSTimestamp = double.Parse(PlayerPrefs.GetString(PP_ITEM_GS_LAST_USE, "0"));
            guardianShieldTime = UnitConverter.ConvertToDateTime(lastUseGSTimestamp).AddHours(24);
        }

        public void UseItem(string itemId)
        {
            if (AOMAssets.MakeItRainGIVG.ItemId.Equals(itemId))
            {
                GameManager.Instance.fairyTreasureGroup.GetComponent<TreasureGroupController>().FairyGroupStart();
                AOMAssets.MakeItRainGIVG.Take(1);
            }

            if (AOMAssets.SkillRefreshGIVG.ItemId.Equals(itemId))
            {
                EventManager.RefreshSkillItemActive();
                AOMAssets.SkillRefreshGIVG.Take(1);
            }

            if (AOMAssets.PowerOfHoldingGIVG.ItemId.Equals(itemId))
            {
                EventManager.PowerOfHoldingItemActive();
                AOMAssets.PowerOfHoldingGIVG.Take(1);
            }

            if (AOMAssets.DoomGIVG.ItemId.Equals(itemId))
            {
                EventManager.DoomItemActive();
                AOMAssets.DoomGIVG.Take(1);
            }

            if (AOMAssets.GuardianShieldGIVG.ItemId.Equals(itemId))
            {
                PlayerPrefs.SetString(PP_ITEM_GS_LAST_USE, UnitConverter.ConvertToTimestamp(DateTime.Now).ToString());
                ReadGuardianShieldTime();
                AOMAssets.GuardianShieldGIVG.Take(1);
            }

            if (itemId.Contains("key_"))
            {
                // Moetan Package Key
                GameItemVG packageKeyVG = AOMAssets.GetGameItemVGFromItemId(itemId);
                packageKeyVG.Take(1);
            }

            //// Using Diamond to buy "Another world key"
            //if (AOMAssets.AnotherWorldKeyGIVG.ItemId.Equals(itemId))
            //{
            //    AOMAssets.AnotherWorldKeyGIVG.Take(1);
            //}

            //// Using Diamond to buy "School key"
            //if (AOMAssets.SchoolKeyGIVG.ItemId.Equals(itemId))
            //{
            //    AOMAssets.SchoolKeyGIVG.Take(1);
            //}

            //// Using Diamond to buy "Forest key"
            //if (AOMAssets.ForestKeyGIVG.ItemId.Equals(itemId))
            //{
            //    AOMAssets.ForestKeyGIVG.Take(1);
            //}

            //// Using Diamond to buy "Water World key"
            //if (AOMAssets.SummerKeyGIVG.ItemId.Equals(itemId))
            //{
            //    AOMAssets.SummerKeyGIVG.Take(1);
            //}

            //// Using Diamond to buy "Halloween key"
            //if (AOMAssets.HalloweenKeyGIVG.ItemId.Equals(itemId))
            //{
            //    AOMAssets.HalloweenKeyGIVG.Take(1);
            //}
        }

        public DateTime GuardianShieldTime
        {
            get { return guardianShieldTime; }
        }
    }
}