﻿using UnityEngine;
using System;
using System.Collections;
using Soomla.Store;
using Zenject;

namespace Ignite.AOM
{
    public class AvatarSkillCore : IInitializable, IDisposable
    {
        private double heavenlyStrikeLastUsed = 0;
        private double shadowCloneLastUsed = 0;
        private double criticalStrikeLastUsed = 0;
        private double warCryLastUsed = 0;
        private double berserkerRageLastUsed = 0;
        private double handOfMidasLastUsed = 0;

        private double shadowCloneUsing = 0;
        private double criticalStrikeUsing = 0;
        private double warCryUsing = 0;
        private double berserkerRageUsing = 0;
        private double handOfMidasUsing = 0;

        private bool isShadowCloneTriggerByFairy = false;

        private const string PP_AS_HS_LAST_USE = "PP_AS_HS_LAST_USE";
        private const string PP_AS_SC_LAST_USE = "PP_AS_SC_LAST_USE";
        private const string PP_AS_CS_LAST_USE = "PP_AS_CS_LAST_USE";
        private const string PP_AS_WC_LAST_USE = "PP_AS_WC_LAST_USE";
        private const string PP_AS_BR_LAST_USE = "PP_AS_BR_LAST_USE";
        private const string PP_AS_HM_LAST_USE = "PP_AS_HM_LAST_USE";

        // Use this for initialization
        public void Initialize()
        {
            EventManager.OnRefreshSkillItemActive += OnRefreshSkillItemActive;

            // Load AS Last Use timestamp from PlayerPrefs
            heavenlyStrikeLastUsed = Double.Parse(PlayerPrefs.GetString(PP_AS_HS_LAST_USE, "0"));
            shadowCloneLastUsed = Double.Parse(PlayerPrefs.GetString(PP_AS_SC_LAST_USE, "0"));
            criticalStrikeLastUsed = Double.Parse(PlayerPrefs.GetString(PP_AS_CS_LAST_USE, "0"));
            warCryLastUsed = Double.Parse(PlayerPrefs.GetString(PP_AS_WC_LAST_USE, "0"));
            berserkerRageLastUsed = Double.Parse(PlayerPrefs.GetString(PP_AS_BR_LAST_USE, "0"));
            handOfMidasLastUsed = Double.Parse(PlayerPrefs.GetString(PP_AS_HM_LAST_USE, "0"));
        }

        public void Dispose()
        {
            EventManager.OnRefreshSkillItemActive -= OnRefreshSkillItemActive;
        }

        private void OnRefreshSkillItemActive()
        {
            RefreshSkill(false);
        }

        public void RefreshSkill(bool isPrestige)
        {
            if (HeavenlyStrikeDuration() <= 0 || isPrestige)
                HeavenlyStrikeLastUse = 0;
            if (ShadowCloneRemainDuration() <= 0 || isPrestige)
                ShadowCloneLastUse = 0;
            if (CriticalStrikeRemainDuration() <= 0 || isPrestige)
                CriticalStrikeLastUse = 0;
            if (WarCryRemainDuration() <= 0 || isPrestige)
                WarCryLastUse = 0;
            if (BerserkerRageRemainDuration() <= 0 || isPrestige)
                BerserkerRageLastUse = 0;
            if (HandOfMidasRemainDuration() <= 0 || isPrestige)
                HandOfMidasLastUse = 0;
        }

        public float HeavenlyStrikeDuration()
        {
            return AOMAssets.HeavenlyStrikeASVG.Duration;
        }

        public float HeavenlyStrikeCooldown()
        {
            return AOMAssets.HeavenlyStrikeASVG.Cooldown * (1 - (float)ArtifactAssets.Artifact011.EffectValue);
        }

        public float ShadowCloneDuration()
        {
            return AOMAssets.ShadowCloneASVG.Duration * (1 + (float)ArtifactAssets.Artifact012.EffectValue);
        }

        public float ShadowCloneCooldown()
        {
            return AOMAssets.ShadowCloneASVG.Cooldown * (1 - (float)ArtifactAssets.Artifact006.EffectValue);
        }

        public float CriticalStrikeDuration()
        {
            return AOMAssets.CriticalStrikeASVG.Duration * (1 + (float)ArtifactAssets.Artifact013.EffectValue);
        }

        public float CriticalStrikeCooldown()
        {
            return AOMAssets.CriticalStrikeASVG.Cooldown * (1 - (float)ArtifactAssets.Artifact007.EffectValue);
        }

        public float WarCryDuration()
        {
            return AOMAssets.WarCryASVG.Duration * (1 + (float)ArtifactAssets.Artifact014.EffectValue);
        }

        public float WarCryCooldown()
        {
            return AOMAssets.WarCryASVG.Cooldown * (1 - (float)ArtifactAssets.Artifact008.EffectValue);
        }

        public float BerserkerRageDuration()
        {
            return AOMAssets.BerserkerRageASVG.Duration * (1 + (float)ArtifactAssets.Artifact016.EffectValue);
        }

        public float BerserkerRageCooldown()
        {
            return AOMAssets.BerserkerRageASVG.Cooldown * (1 - (float)ArtifactAssets.Artifact010.EffectValue);
        }

        public float HandOfMidasDuration()
        {
            return AOMAssets.HandOfMidasASVG.Duration * (1 + (float)ArtifactAssets.Artifact015.EffectValue);
        }

        public float HandOfMidasCooldown()
        {
            return AOMAssets.HandOfMidasASVG.Cooldown * (1 - (float)ArtifactAssets.Artifact009.EffectValue);
        }

        private double HeavenlyStrikeLastUse
        {
            get { return heavenlyStrikeLastUsed; }
            set
            {
                heavenlyStrikeLastUsed = value;
                PlayerPrefs.SetString(PP_AS_HS_LAST_USE, value.ToString());
            }
        }

        public double ShadowCloneLastUse
        {
            get { return shadowCloneLastUsed; }
            set
            {
                shadowCloneUsing = value;
                shadowCloneLastUsed = value;
                PlayerPrefs.SetString(PP_AS_SC_LAST_USE, value.ToString());
            }
        }

        public double CriticalStrikeLastUse
        {
            get { return criticalStrikeLastUsed; }
            set
            {
                criticalStrikeUsing = value;
                criticalStrikeLastUsed = value;
                PlayerPrefs.SetString(PP_AS_CS_LAST_USE, value.ToString());
            }
        }

        public double WarCryLastUse
        {
            get { return warCryLastUsed; }
            set
            {
                warCryUsing = value;
                warCryLastUsed = value;
                PlayerPrefs.SetString(PP_AS_WC_LAST_USE, value.ToString());
            }
        }

        public double BerserkerRageLastUse
        {
            get { return berserkerRageLastUsed; }
            set
            {
                berserkerRageUsing = value;
                berserkerRageLastUsed = value;
                PlayerPrefs.SetString(PP_AS_BR_LAST_USE, value.ToString());
            }
        }

        public double HandOfMidasLastUse
        {
            get { return handOfMidasLastUsed; }
            set
            {
                handOfMidasUsing = value;
                handOfMidasLastUsed = value;
                PlayerPrefs.SetString(PP_AS_HM_LAST_USE, value.ToString());
            }
        }

        public bool ActivateHeavenlyStrike()
        {
            bool skillTriggered = false;
            if (AOMAssets.HeavenlyStrikeASVG.GetBalance() > 0 && HeavenlyStrikeRemainCD() <= 0)
            {
                HeavenlyStrikeLastUse = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                skillTriggered = true;

                GamePlayStatManager.Instance.JumpAttack();
            }
            return skillTriggered;
        }

        public float HeavenlyStrikeRemainCD()
        {
            float remainCD = 0;

            if (HeavenlyStrikeLastUse > 0)
            {
                float elapsedTime = (float)(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - HeavenlyStrikeLastUse);
                //                Debug.LogFormat ("elapsedTime: {0}", elapsedTime);
                remainCD = HeavenlyStrikeCooldown() - elapsedTime;

                if (remainCD <= 0)
                {
                    HeavenlyStrikeLastUse = 0;
                }
            }

            return remainCD;
        }

        public bool ActivateShadowClone(bool isTriggerByFairy)
        {

            bool skillTriggered = false;
            if (AOMAssets.ShadowCloneASVG.GetBalance() > 0 && ShadowCloneRemainCD() <= 0 && !isTriggerByFairy)
            {
                ShadowCloneLastUse = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                skillTriggered = true;
                IsShadowCloneTriggerByFairy = false;
            }
            else
            {
                shadowCloneUsing = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                skillTriggered = true;
                IsShadowCloneTriggerByFairy = true;
            }
            return skillTriggered;
        }

        public bool IsShadowCloneTriggerByFairy
        {
            get { return isShadowCloneTriggerByFairy; }
            set { isShadowCloneTriggerByFairy = value; }
        }

        public float ShadowCloneRemainCD()
        {
            float remainCD = 0;

            if (ShadowCloneLastUse > 0)
            {
                float elapsedTime = (float)(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - ShadowCloneLastUse);
                //                Debug.LogFormat ("elapsedTime: {0}", elapsedTime);
                remainCD = ShadowCloneCooldown() - elapsedTime;

                if (remainCD <= 0)
                {
                    ShadowCloneLastUse = 0;
                }
            }

            return remainCD;
        }

        public float ShadowCloneRemainDuration()
        {
            float remainDuration = 0;
            if (shadowCloneUsing > 0)
            {
                float elapsedTime = (float)(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - shadowCloneUsing);
                remainDuration = (IsShadowCloneTriggerByFairy ? ShadowCloneDuration() / 2 : ShadowCloneDuration()) - elapsedTime;

                if (remainDuration <= 0)
                {
                    shadowCloneUsing = 0;
                    IsShadowCloneTriggerByFairy = false;
                }
            }
            return remainDuration;
        }

        public bool ActivateCriticalStrike()
        {
            bool skillTriggered = false;
            if (AOMAssets.CriticalStrikeASVG.GetBalance() > 0 && CriticalStrikeRemainCD() <= 0)
            {
                CriticalStrikeLastUse = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                skillTriggered = true;
            }
            return skillTriggered;
        }

        public float CriticalStrikeRemainCD()
        {
            float remainCD = 0;

            if (CriticalStrikeLastUse > 0)
            {
                float elapsedTime = (float)(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - CriticalStrikeLastUse);
                //                Debug.LogFormat ("elapsedTime: {0}", elapsedTime);
                remainCD = CriticalStrikeCooldown() - elapsedTime;
                if (remainCD <= 0)
                {
                    CriticalStrikeLastUse = 0;
                }
            }

            return remainCD;
        }

        public float CriticalStrikeRemainDuration()
        {
            float remainDuration = 0;
            if (CriticalStrikeLastUse > 0)
            {
                float elapsedTime = (float)(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - criticalStrikeUsing);
                remainDuration = CriticalStrikeDuration() - elapsedTime;
            }
            return remainDuration;
        }



        public bool ActivateWarCry()
        {
            bool skillTriggered = false;
            if (AOMAssets.WarCryASVG.GetBalance() > 0 && WarCryRemainCD() <= 0)
            {
                WarCryLastUse = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                skillTriggered = true;
            }
            return skillTriggered;
        }

        public float WarCryRemainCD()
        {
            float remainCD = 0;

            if (WarCryLastUse > 0)
            {
                float elapsedTime = (float)(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - WarCryLastUse);
                //                Debug.LogFormat ("elapsedTime: {0}", elapsedTime);
                remainCD = WarCryCooldown() - elapsedTime;
                if (remainCD <= 0)
                {
                    WarCryLastUse = 0;
                }
            }

            return remainCD;
        }

        public float WarCryRemainDuration()
        {
            float remainDuration = 0;
            if (WarCryLastUse > 0)
            {
                float elapsedTime = (float)(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - warCryUsing);
                remainDuration = WarCryDuration() - elapsedTime;
            }
            return remainDuration;
        }

        public bool ActivateBerserkerRage()
        {
            bool skillTriggered = false;
            if (AOMAssets.BerserkerRageASVG.GetBalance() > 0 && BerserkerRageRemainCD() <= 0)
            {
                BerserkerRageLastUse = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                skillTriggered = true;
            }
            return skillTriggered;
        }

        public float BerserkerRageRemainCD()
        {
            float remainCD = 0;

            if (BerserkerRageLastUse > 0)
            {
                float elapsedTime = (float)(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - BerserkerRageLastUse);
                //                Debug.LogFormat ("elapsedTime: {0}", elapsedTime);
                remainCD = BerserkerRageCooldown() - elapsedTime;

                if (remainCD <= 0)
                {
                    BerserkerRageLastUse = 0;
                }
            }

            return remainCD;
        }

        public float BerserkerRageRemainDuration()
        {
            float remainDuration = 0;
            if (BerserkerRageLastUse > 0)
            {
                float elapsedTime = (float)(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - berserkerRageUsing);
                remainDuration = BerserkerRageDuration() - elapsedTime;
            }
            return remainDuration;
        }

        public bool ActivateHandOfMidas()
        {
            bool skillTriggered = false;
            if (AOMAssets.HandOfMidasASVG.GetBalance() > 0 && HandOfMidasRemainCD() <= 0)
            {
                HandOfMidasLastUse = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                skillTriggered = true;
            }
            return skillTriggered;
        }

        public float HandOfMidasRemainCD()
        {
            float remainCD = 0;

            if (HandOfMidasLastUse > 0)
            {
                float elapsedTime = (float)(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - HandOfMidasLastUse);
                remainCD = HandOfMidasCooldown() - elapsedTime;

                if (remainCD <= 0)
                {
                    HandOfMidasLastUse = 0;
                }
            }

            return remainCD;
        }

        public float HandOfMidasRemainDuration()
        {
            float remainDuration = 0;
            if (HandOfMidasLastUse > 0)
            {
                float elapsedTime = (float)(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - handOfMidasUsing);
                remainDuration = HandOfMidasDuration() - elapsedTime;
            }
            return remainDuration;
        }

        public float TotalCooldownTime()
        {
            float result = 0;
            result += HeavenlyStrikeRemainCD();
            if (ShadowCloneRemainDuration() <= 0)
                result += ShadowCloneRemainCD();
            if (CriticalStrikeRemainDuration() <= 0)
                result += CriticalStrikeRemainCD();
            if (WarCryRemainDuration() <= 0)
                result += WarCryRemainCD();
            if (BerserkerRageRemainDuration() <= 0)
                result += BerserkerRageRemainCD();
            if (HandOfMidasRemainDuration() <= 0)
                result += HandOfMidasRemainCD();
            return result;
        }
    }
}
