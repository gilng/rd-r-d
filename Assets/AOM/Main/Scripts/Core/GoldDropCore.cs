using System;
using System.Diagnostics;
using Zenject;

namespace Ignite.AOM
{
    public class GoldDropCore
    {
        private readonly HeroCore _heroCore;
        private readonly EnemyCore _enemyCore;

        [Inject]
        public GoldDropCore(HeroCore heroCore, EnemyCore enemyCore)
        {
            _heroCore = heroCore;
            _enemyCore = enemyCore;
        }

        private double currentEnemyGoldDrop;

        public double CurrentEnemyGoldDrop()
        {
            EnemyCore enemyCore = _enemyCore;

            return Formulas.EnemyGoldDropForLevel(enemyCore.Stage,
                enemyCore.CurrentEnemyType,
                _heroCore.EffectValue[EffectType.GoldDropAmount],
                ArtifactAssets.Artifact020.EffectValue,
                ArtifactAssets.Artifact027.EffectValue,
                ArtifactAssets.Artifact001.EffectValue + ArtifactAssets.MoeSpirit006.EffectValue,
                ArtifactAssets.Artifact002.EffectValue + ArtifactAssets.MoeSpirit002.EffectValue,
                ArtifactAssets.Artifact021.EffectValue,
                _heroCore.EffectValue[EffectType.TreasureChestGoldAmount],
                ArtifactAssets.Artifact019.EffectValue);
        }

        public double HandOfMidasGoldAmount()
        {
            return Formulas.EnemyGoldDropForLevel(_enemyCore.Stage,
                       EnemyType.Normal,
                       _heroCore.EffectValue[EffectType.GoldDropAmount],
                       ArtifactAssets.Artifact020.EffectValue,
                       ArtifactAssets.Artifact027.EffectValue,
                       ArtifactAssets.Artifact001.EffectValue + ArtifactAssets.MoeSpirit006.EffectValue,
                       ArtifactAssets.Artifact002.EffectValue + ArtifactAssets.MoeSpirit002.EffectValue,
                       ArtifactAssets.Artifact021.EffectValue,
                       _heroCore.EffectValue[EffectType.TreasureChestGoldAmount],
                       ArtifactAssets.Artifact019.EffectValue) * AOMAssets.HandOfMidasASVG.EffectValue;
        }

        //public double CurrentEnemyGoldDrop
        //{
        //    get { return this.currentEnemyGoldDrop; }
        //}
    }
}