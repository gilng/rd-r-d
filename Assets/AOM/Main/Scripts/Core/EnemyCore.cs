﻿using UnityEngine;
using System.Collections;
using Soomla.Store;
using System;
using Zenject;

namespace Ignite.AOM
{
    public enum EnemyType
    {
        Normal,
        MiniBoss,
        BigBoss,
        TreasureChest
    }

    public class EnemyCore : IInitializable, IDisposable
    {
        private readonly GameItemCore _gameItemCore;

        [Inject]
        public EnemyCore(GameItemCore gameItemCore)
        {
            _gameItemCore = gameItemCore;
        }

        //public const string PP_MAX_STAGE = "PP_MAX_STAGE";
        public const string PP_STAGE = "PP_STAGE";
        public const string PP_WAVE = "PP_WAVE";
        public const string PP_IS_INFINITY_LOOP = "PP_IS_INFINITY_LOOP";

//      private static readonly float[] BOSS_KILL_CHANCES = new float[] { 1f, 1.9f, 2.9f, 3.8f, 4.8f, 5.7f, 6.7f, 7.6f, 8.6f, 9.5f, 10.5f, 11.4f, 12.4f, 13.3f, 14.3f, 15.2f, 16.2f, 17.1f, 18.1f, 19f, 20f };
        private static readonly float[] BOSS_KILL_CHANCES = new float[] { 1f, 1.4f, 1.8f, 2.2f, 2.6f, 3.0f, 3.4f, 3.7f, 4.1f, 4.5f, 4.9f, 5.3f, 5.7f, 6.0f, 6.4f, 6.8f, 7.2f, 7.6f, 8.0f, 8.3f, 8.7f };
        private static readonly float[] MINI_BOSS_KILL_CHANCES = new float[] { 0.2f, 0.5f, 0.7f, 1f, 1.2f, 1.4f, 1.7f, 1.9f, 2.1f, 2.4f, 2.6f, 2.9f, 3.1f, 3.3f, 3.6f, 3.8f, 4f, 4.3f, 4.5f, 4.8f, 5f };

        private int _systemMaxStage = 2300;
        private int _maxStage = 2000;
        private int _currentStage;
        private int _currentWave;
        private int wavePerStage = 10;
        private bool isInfinityLoop;
        private static EnemyCore instance;
        private EnemyType _currentEnemyType;

        // Use this for initialization
        public void Initialize()
        {
            
        }

        public void InitCore()
        {
            EventManager.OnPrestige += ResetStage;
            EventManager.OnArtifactVGBalanceChange += OnArtifactVGBalanceChange;
        }

        public void Dispose()
        {
            EventManager.OnPrestige -= ResetStage;
            EventManager.OnArtifactVGBalanceChange -= OnArtifactVGBalanceChange;
        }

        private void RecalWavePerStage(bool ar025Recal = false)
        {
            wavePerStage = 10 - (int)(ArtifactAssets.Artifact025.EffectValue * 100);

            if (ar025Recal && Wave >= wavePerStage && CurrentEnemyType != EnemyType.MiniBoss && CurrentEnemyType != EnemyType.BigBoss)
            {
                Wave = wavePerStage - 1;
            }
        }

        public void RecalAll()
        {
            if (PlayerPrefs.GetInt(PP_STAGE, 1) > MaxStage)
                PlayerPrefs.SetInt(PP_STAGE, MaxStage);

            _currentStage = PlayerPrefs.GetInt(PP_STAGE, 1);
            _currentWave = PlayerPrefs.GetInt(PP_WAVE, 0);
            isInfinityLoop = PlayerPrefs.GetInt(PP_IS_INFINITY_LOOP, 0) == 1;

            RecalWavePerStage();
            RecalEnemyType();
        }

        private void OnArtifactVGBalanceChange(ArtifactVG vg, int balance)
        {
            if (vg.ItemId.Equals(ArtifactAssets.Artifact025.ItemId))
            {
                RecalWavePerStage(true);
            }
        }

        private void RecalEnemyType()
        {
            EnemyType t = Formulas.EnemyTypeForStage(_currentStage, _currentWave, wavePerStage, isInfinityLoop);

            if (EnemyType.MiniBoss != t && EnemyType.BigBoss != t && _currentStage > 10)
            {
                float chestChance = (float)(2 + (2 * (1 + ArtifactAssets.Artifact018.EffectValue)));
                float chestRoll = UnityEngine.Random.Range(0f, 100f);

                //                Debug.LogFormat("TreasureChestAppearChance: {0}, chestChance: {1}", chestRoll, chestChance);
                if (chestRoll < chestChance)
                {
                    t = EnemyType.TreasureChest;
                }
            }
            CurrentEnemyType = t;
        }

        //GameManager will call this function to indicate the current enemy has been killed
        // and start to fight next enemy
        public void NextEnemy(bool repeatLevel)
        {
            //			EventManager.ClearStage();

            if (!isInfinityLoop && !repeatLevel)
            {
                if (_currentWave >= wavePerStage)
                {

                    // Set max stage
                    if (Stage < MaxStage)
                    {
                        Stage++;
                    }
                    else
                    {
                        // Show "End Of World"
                        if (PlayerPrefs.GetInt("Arrived_EndOfWorld", 0) == 0)
                        {
                            PlayerPrefs.SetInt("Arrived_EndOfWorld", 1);
                            //GameManager.instance.endOfWorld.GetComponent<EndOfWorldManager>().PlayAnimation();

                            GameManager.Instance.ShowEndOfWorld();
                        }
                    }

                    Wave = 0;

                    if (OfflineGoldManager.instance != null)
                    {
                        if (!OfflineGoldManager.instance.noticeBoardShowing)
                        {
                            EventManager.PromotionalPackOfferTime(true, Stage);
                        }
                    }
                }
                else
                {
                    Wave += 1;
                }
            }

            IsInfinityLoop = repeatLevel;

//			CurrentEnemyType = Formulas.EnemyTypeForStage(_currentStage, _currentWave, wavePerStage, isInfinityLoop);
            RecalEnemyType();

//            if (EnemyType.BigBoss == _currentEnemyType && _currentStage >= 80 && _currentStage % 10 == 0)
//            {
//				Debug.LogError("EnemyShouldDropRelic (Type: " + _currentEnemyType + " - Stage: " + _currentStage + ")" );
//				EventManager.EnemyShouldDropRelic();
//            }

			if (EnemyType.BigBoss == _currentEnemyType && _currentStage == 80)
			{
				Debug.LogError("EnemyShouldDropRelic (Type: " + _currentEnemyType + " - Stage: " + _currentStage + ")" );
				EventManager.EnemyShouldDropRelic();
			}
        }

        public bool ShouldKillHero(int sec)
        {
            if (sec >= 5 && sec <= 25 && (EnemyType.MiniBoss == CurrentEnemyType || EnemyType.BigBoss == CurrentEnemyType) && Stage > 50 && DateTime.Now > _gameItemCore.GuardianShieldTime)
            {
                float[] chances = EnemyType.MiniBoss == CurrentEnemyType ? MINI_BOSS_KILL_CHANCES : BOSS_KILL_CHANCES;

                // Count 1s
                float chance_dice = UnityEngine.Random.Range(0f, 100f);
                double chance_boss = (chances[sec - 5] * (1 - ArtifactAssets.Artifact028.EffectValue - ArtifactAssets.MoeSpirit007.EffectValue));

                return chance_dice <= chance_boss;
            }
            return false;
        }


        public double SpawnEnemy()
        {
//			GoldDropCore.Instance.RecalEnemyGoldDrop(currentStage,currentEnemyType);
            return Formulas.EnemyHPForWave(_currentStage, _currentWave, wavePerStage, isInfinityLoop, ArtifactAssets.Artifact003.EffectValue);
        }

        public void SyncCloudStageProgress(SaveData loadedSaveData)
        {
            if (loadedSaveData.stage > 0)
            {
                Stage = loadedSaveData.stage;

                Wave = loadedSaveData.wave;
            }
        }

        public void ResetStage()
        {
            Stage = 1;
            Wave = 0;
            isInfinityLoop = false;
            PlayerPrefs.SetInt(PP_IS_INFINITY_LOOP, this.isInfinityLoop ? 1 : 0);
            CurrentEnemyType = Formulas.EnemyTypeForStage(_currentStage, _currentWave, wavePerStage, isInfinityLoop);
            RecalWavePerStage();
        }

        public EnemyType CurrentEnemyType
        {
            get { return _currentEnemyType; }
            private set
            {
                EnemyType prevType = _currentEnemyType;
                _currentEnemyType = value;

                if (prevType != _currentEnemyType)
                {
                    EventManager.EnemyTypeChange(_currentEnemyType);
                }
            }
        }

        public int Stage
        {
            get { return this._currentStage; }
            set
            {
                this._currentStage = value;
                PlayerPrefs.SetInt(PP_STAGE, _currentStage);

                GamePlayStatManager.Instance.ReachHighestStage(this._currentStage);
            }
        }

        public int MaxStage
        {
            get { return this._maxStage; }
        }

        public void IncreaseMaxStage(int lv)
        {
            if (lv <= this._systemMaxStage)
            {
                this._maxStage = lv;
                //PlayerPrefs.SetInt(PP_MAX_STAGE, this._maxStage);
                PlayerPrefs.SetInt("Arrived_EndOfWorld", 0);
            }
        }

        public int Wave
        {
            get { return this._currentWave; }
            private set
            {
                _currentWave = value;
                PlayerPrefs.SetInt(PP_WAVE, _currentWave);
            }
        }

        public int WavePerStage
        {
            get { return this.wavePerStage; }
        }

        public bool IsInfinityLoop
        {
            private set
            {
                this.isInfinityLoop = value;
                PlayerPrefs.SetInt(PP_IS_INFINITY_LOOP, this.isInfinityLoop ? 1 : 0);
            }
            get { return this.isInfinityLoop; }
        }

        public float BossFightCountDown
        {
            get { return (float)(30.0f * (1 + ArtifactAssets.Artifact005.EffectValue)); }
        }
    }
}