﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Ignite.AOM
{
    public class GalleryCore
    {
        public static Gallery Girl001 = new Gallery("001", 1, 1, 5, 20, 80);
        public static Gallery Girl002 = new Gallery("002", 7, 7, 0, 40, 140);
        public static Gallery Girl003 = new Gallery("003", 8, 8, 0, 100, 200);
        public static Gallery Girl004 = new Gallery("004", 1, 1, 60, 160, 260);
        public static Gallery Girl005 = new Gallery("005", 7, 7, 120, 220, 320);
        public static Gallery Girl006 = new Gallery("006", 8, 8, 180, 280, 380);
        public static Gallery Girl007 = new Gallery("007", 1, 1, 240, 340, 440);
        public static Gallery Girl008 = new Gallery("008", 7, 7, 300, 400, 500);
        public static Gallery Girl009 = new Gallery("009", 8, 8, 360, 460, 560);
        public static Gallery Girl010 = new Gallery("010", 8, 8, 420, 520, 620);

        public static Gallery Girl011 = new Gallery("011", 5, 5);
        public static Gallery Girl012 = new Gallery("012", 5, 5);
        public static Gallery Girl013 = new Gallery("013", 5, 5);

        public static Gallery Girl014 = new Gallery("014", 3, 3);
        public static Gallery Girl015 = new Gallery("015", 3, 3);
        public static Gallery Girl016 = new Gallery("016", 3, 3);
        public static Gallery Girl017 = new Gallery("017", 3, 3);
        public static Gallery Girl018 = new Gallery("018", 3, 3);

        public static Gallery Girl019 = new Gallery("019", 4, 4);
        public static Gallery Girl020 = new Gallery("020", 4, 4);
        public static Gallery Girl021 = new Gallery("021", 4, 4);

        public static Gallery Girl022 = new Gallery("022", 2, 2);
        public static Gallery Girl023 = new Gallery("023", 2, 2);
        public static Gallery Girl024 = new Gallery("024", 2, 2);

        public static Gallery Girl025 = new Gallery("025", 6, 6);
        public static Gallery Girl026 = new Gallery("026", 6, 6);
        public static Gallery Girl027 = new Gallery("027", 6, 6);

        public static Gallery Girl028 = new Gallery("028", 6, 6);
        public static Gallery Girl029 = new Gallery("029", 4, 4);
        public static Gallery Girl030 = new Gallery("030", 5, 5);

        public static Gallery Girl097 = new Gallery("097", 6, 6);
		public static Gallery Girl098 = new Gallery("098", 6, 6);

		public static int[] PvPBossId = new int[] {98, 97};

        public static List<Gallery> Girls = new List<Gallery> {
			Girl001, Girl002, Girl003, Girl004, Girl005, Girl006, Girl007, Girl008, Girl009, Girl010, 
			Girl011, Girl012, Girl013, Girl014, Girl015, Girl016, Girl017, Girl018, Girl019, Girl020, 
			Girl021, Girl022, Girl023, Girl024, Girl025, Girl026, Girl027,
            Girl097, Girl098,
            Girl028, Girl029, Girl030
        };

        private const string PP_GALLERY_GIRL_TOUCH_CREDIT = "PP_GALLERY_GIRL_TOUCH_CREDIT";
        private const string PP_GALLERY_GIRL_TOUCH_CREDIT_MAX = "PP_GALLERY_GIRL_TOUCH_CREDIT_MAX";
        private const string PP_GALLERY_GIRL_LAST_TOUCH = "PP_GALLERY_GIRL_LAST_TOUCH";

        private static int touchCredit = 5;
        private static int touchCredit_Max = 5;
        private static double lastTouch = 0;
        private int creditRechargeTime = 3600;

        public void RecalAll()
        {
            touchCredit = PlayerPrefs.GetInt(PP_GALLERY_GIRL_TOUCH_CREDIT, touchCredit_Max);

            if (ArtifactAssets.Artifact031.GetBalance() > 0)
                RecalMaxTouchCredit((int)((float)(ArtifactAssets.Artifact031.EffectValue * 100)));
            else
                touchCredit_Max = 5;

            lastTouch = Double.Parse(PlayerPrefs.GetString(PP_GALLERY_GIRL_LAST_TOUCH, "0"));

            if (ArtifactAssets.Artifact030.GetBalance() > 0 || ArtifactAssets.MoeSpirit013.GetBalance() > 0)
            {
                RecalCreditRechargeTime((1 - (float)ArtifactAssets.Artifact030.EffectValue));
                RecalCreditRechargeTime((1 - (float)ArtifactAssets.MoeSpirit013.EffectValue));
            }
            else
            {
                creditRechargeTime = 3600;
            }
        }

        public int MaxTouchCredit
        {
            set
            {
                touchCredit_Max = value;
                PlayerPrefs.SetInt(PP_GALLERY_GIRL_TOUCH_CREDIT_MAX, touchCredit_Max);
            }
            get { return touchCredit_Max; }
        }

        public int TouchCredit
        {
            set
            {
                //touchCredit = Math.Min(value, touchCredit_Max);
                touchCredit = value;
                PlayerPrefs.SetInt(PP_GALLERY_GIRL_TOUCH_CREDIT, touchCredit);
            }
            get { return touchCredit; }
        }

        public double LastTouch
        {
            set
            {
                lastTouch = value;
                PlayerPrefs.SetString(PP_GALLERY_GIRL_LAST_TOUCH, value.ToString());
            }
            get { return lastTouch; }
        }

        public int CreditRechargeTime
        {
            set
            {
                creditRechargeTime = value;
            }
            get { return creditRechargeTime; }
        }

        public string RecalTouchCredit()
        {
            if (LastTouch != 0 && TouchCredit < MaxTouchCredit)
            {
                if (UnitConverter.ConvertToTimestamp(System.DateTime.Now) - LastTouch >= CreditRechargeTime)
                {
                    TouchCredit += (int)(UnitConverter.ConvertToTimestamp(System.DateTime.Now) - LastTouch) / CreditRechargeTime;
                    LastTouch += CreditRechargeTime;

                    if (TouchCredit >= MaxTouchCredit)
                        LastTouch = 0;

                    TouchCredit = (TouchCredit >= MaxTouchCredit) ? MaxTouchCredit : TouchCredit;
                }
            }
            else
            {
                if (TouchCredit >= MaxTouchCredit)
                    LastTouch = 0;
            }

            return UnitConverter.SecondsToMinsAndSeconds(CreditRechargeTime - ((UnitConverter.ConvertToTimestamp(System.DateTime.Now) - lastTouch) % CreditRechargeTime));
        }

        public bool CanTouch()
        {
            return TouchCredit > 0;
        }

        public void Touch()
        {
            if (CanTouch())
            {
                TouchCredit -= 1;

                if (TouchCredit < MaxTouchCredit)
                {
                    if (LastTouch == 0)
                        LastTouch = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                }
                else
                {
                    LastTouch = 0;
                }
            }
        }

		public Gallery GetGirlWithID(string id)
		{
			Gallery g = null;
			for (int i = 0; i < Girls.Count; i++)
			{
				if (Girls[i].girlId.Contains(id))
				{
					g = Girls[i];
					break;
				}
			}
			return g;
		}

        public Gallery GetGirlWithIndex(int index)
        {
            return Girls[index];
        }

        public bool OwnedGirls()
        {
            bool haveGirls = false;

            for (int i = 0; i < Girls.Count; i++)
            {
                if (Girls[i].Status == MonsterGirlStatus.Killed)
                {
                    haveGirls = true;
                    break;
                }
            }

            return haveGirls;
        }

        public void RechargingTouchCredit(int val, bool isByGems = false)
        {
            TouchCredit = isByGems ? val + TouchCredit : TouchCredit < MaxTouchCredit ? val + TouchCredit : TouchCredit;

            if (TouchCredit >= MaxTouchCredit)
                LastTouch = 0;
        }

        // AR030: Reduce time
        public void RecalCreditRechargeTime(float val)
        {
            creditRechargeTime = Mathf.RoundToInt(creditRechargeTime * val);

            // For test
            //creditRechargeTime = 10;

            GameManager.Instance.UpdateTouchCredit();
        }

        // AR031: +Max CP
        public void RecalMaxTouchCredit(int val)
        {
            MaxTouchCredit = val + 5;

            GameManager.Instance.UpdateTouchCredit();
		}



		public static int GetRealBossId(int id, int val = 0)
		{
			int realBossId = id;
			for (int i=0; i<PvPBossId.Length; i++)
			{
				if (FieldGuideManager.Instance != null)
				if (id == FieldGuideManager.Instance.bossCharacterPool.bosses.Count - val - i)
					realBossId = PvPBossId[i] - val;
			}
			return realBossId;
		}

    }
}