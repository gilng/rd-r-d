using UnityEngine;
using System;
using System.Collections.Generic;
using Soomla.Store;
using Zenject;
using Debug = System.Diagnostics.Debug;

namespace Ignite.AOM
{
    public class AvatarCore : IInitializable, IDisposable
    {
        private readonly HeroCore _heroCore;
        private readonly EnemyCore _enemyCore;
        private readonly ArtifactCore _artifactCore;
        private readonly AvatarSkillCore _avatarSkillCore;

        [Inject]
        public AvatarCore(HeroCore heroCore, EnemyCore enemyCore, ArtifactCore artifactCore,
            AvatarSkillCore avatarSkillCore)
        {
            _heroCore = heroCore;
            _enemyCore = enemyCore;
            _artifactCore = artifactCore;
            _avatarSkillCore = avatarSkillCore;
        }

        //private static AvatarCore instance;
        private double tapDamage;

        private double criticalChance;
        private double criticalDamage;
        private double baseDamage;

        private double tapNextLVDamage;
        private double baseNextLVDamage;

        private bool berserkerRageEnabled = false;
        private bool criticalStrikeASEnabled = false;
        private int prestigeMinLevel = 600;

        public static readonly double BASE_CRITICAL_CHANCE = 0.01;

        public bool BerserkerRageEnabled
        {
            get { return berserkerRageEnabled; }

            set
            {
                this.berserkerRageEnabled = value;
                RecalTapDamage();
            }
        }

        public bool CriticalStrikeASEnabled
        {
            get { return criticalStrikeASEnabled; }
            set
            {
                this.criticalStrikeASEnabled = value;
                RecalCriticalChance();
            }
        }

        public double BaseDamage
        {
            get { return this.baseDamage; }
        }

        public double TapDamage
        {
            get { return this.tapDamage; }
        }

        public double TapNextLVDamage
        {
            get { return this.tapNextLVDamage; }
        }

        public double CriticalChance
        {
            get { return criticalChance; }
        }

        public double CriticalDamage
        {
            get { return Math.Round(criticalDamage * UnityEngine.Random.Range(3, 11), MidpointRounding.AwayFromZero); }
        }

        public double HeavenlyStrikeDamage
        {
            get
            {
                double heavenlyStrikeDamage = TapDamage * AOMAssets.HeavenlyStrikeASVG.EffectValue;
                double berserkerRageIncrease = (UnitConverter.ConvertToTimestamp(System.DateTime.Now) - _avatarSkillCore.BerserkerRageLastUse < AOMAssets.BerserkerRageASVG.Duration ? DamageFormulas.ActiveSkillBerserkerRageIncreaseForSkillLevel(AOMAssets.BerserkerRageASVG.GetBalance()): 1);
                return heavenlyStrikeDamage * berserkerRageIncrease;
            }
        }

        public int PrestigeMinLevel
        {
            get { return this.prestigeMinLevel; }
        }

        // Use this for initialization
        public void Initialize()
        {
        }

        public void InitCore()
        {
            if (AOMAssets.AvatarLevelVG.GetBalance() > AOMAssets.AvatarLevelVG.MaxLevel)
                AOMAssets.AvatarLevelVG.ResetBalance(AOMAssets.AvatarLevelVG.MaxLevel);

            for (int i = 0; i < AOMAssets.AvatarActiveSkillVGs.Count; i++)
            {
                if (AOMAssets.AvatarActiveSkillVGs[i].GetBalance() > AOMAssets.AvatarActiveSkillVGs[i].MaxLevel)
                    AOMAssets.AvatarActiveSkillVGs[i].ResetBalance(AOMAssets.AvatarActiveSkillVGs[i].MaxLevel, false);
            }

            //StoreEvents.OnSoomlaStoreInitialized += RecalAll;
            EventManager.OnLevelUpVGBalanceChange += OnLevelChanged;
            EventManager.OnArtifactVGBalanceChange += OnArtifactVGBalanceChange;
            EventManager.OnHeroSkillBuffTotalChange += OnHeroSkillBuffTotalChange;
            EventManager.OnEnemyTypeChange += OnEnemyTypeChange;

            //            RecalAll();
        }

        public void Dispose()
        {
            //StoreEvents.OnSoomlaStoreInitialized -= RecalAll;
            EventManager.OnLevelUpVGBalanceChange -= OnLevelChanged;
            EventManager.OnArtifactVGBalanceChange -= OnArtifactVGBalanceChange;
            EventManager.OnHeroSkillBuffTotalChange -= OnHeroSkillBuffTotalChange;
            EventManager.OnEnemyTypeChange -= OnEnemyTypeChange;
        }

        private void OnArtifactVGBalanceChange(ArtifactVG avg, int balance)
        {
            if (avg.ItemId.Equals(ArtifactAssets.Artifact004.ItemId))
            {
                RecalCriticalChance();
            }
            else if (avg.ItemId.Equals(ArtifactAssets.Artifact017.ItemId))
            {
                RecalCriticalDamage();
            }
            else if (avg.ItemId.Equals(ArtifactAssets.Artifact029.ItemId))
            {
                RecalTapDamage();
            }
        }

        private void OnHeroSkillBuffTotalChange(EffectType t, double newVal)
        {
            switch (t)
            {
                case EffectType.BossDamage:
                case EffectType.AllDamage:
                case EffectType.TapDamage:
                case EffectType.TotalDPSTapDamage:
                    //                Debug.Log("OnHeroSkillBuffTotalChange: " + t);
                    RecalTapDamage();
                    break;
                case EffectType.CriticalChance:
                    RecalCriticalChance();
                    break;
                case EffectType.CriticalDamage:
                    RecalCriticalDamage();
                    break;
            }
        }

        private void OnEnemyTypeChange(EnemyType t)
        {
            RecalTapDamage();
        }

        private void OnLevelChanged(LevelUpVG good, int balance)
        {
            if (good.ItemId.Equals(AOMAssets.AvatarLevelVG.ItemId))
            {
                RecalBaseDamage();
            }
            else if (berserkerRageEnabled && good.ItemId.Equals(AOMAssets.BerserkerRageASVG.ItemId))
            {
                RecalTapDamage();
            }
            else if (criticalStrikeASEnabled && good.ItemId.Equals(AOMAssets.CriticalStrikeASVG.ItemId))
            {
                RecalCriticalChance();
            }
        }

        public void RecalAll()
        {
            //            RecalHeroAllDamageBuff();
            RecalCriticalChance();
            RecalBaseDamage();
        }

        private void RecalBaseDamage()
        {
            int avatarLv = AOMAssets.AvatarLevelVG.GetBalance();
            baseDamage = DamageFormulas.AvatarBaseDamageForLevel(avatarLv);
            baseNextLVDamage = DamageFormulas.AvatarBaseDamageForLevel(avatarLv + 1);
            RecalTapDamage();
        }

        public IList<int> RelicForPrestige()
        {
            return Formulas.PrestigeRelic(_heroCore.TotalAliveHeroLevel, _enemyCore.Stage,
                ArtifactAssets.Artifact022.EffectValue + ArtifactAssets.MoeSpirit004.EffectValue);
        }


        public int Prestige()
        {
            int relics = 0;
            if (AOMAssets.AvatarLevelVG.GetBalance() >= prestigeMinLevel)
            {
                IList<int> result = Formulas.PrestigeRelic(_heroCore.TotalAliveHeroLevel, _enemyCore.Stage,
                    ArtifactAssets.Artifact022.EffectValue + ArtifactAssets.MoeSpirit004.EffectValue);
                relics = (result[0] + result[1]) * (_heroCore.AllHeroAlive ? 2 : 1);

                EventManager.Prestige();

                //TODO Reset Avatar level and skill
                AOMAssets.AvatarLevelVG.ResetBalance(1, false);

                if (AOMAssets.AvatarActiveSkillVGs != null)
                {
                    for (int i = 0; i < AOMAssets.AvatarActiveSkillVGs.Count; i++)
                    {
                        AvatarActiveSkillVG skill = AOMAssets.AvatarActiveSkillVGs[i];
                        skill.ResetBalance(0, false);
                    }
                }

                RecalAll();

                //TODO stop all active skill
                _avatarSkillCore.RefreshSkill(true);
                EventManager.PrestigeComplete();
                GamePlayStatManager.Instance.Prestige();

                AOMStoreInventory.ResetGold(0);
            }

            return relics;
        }

        public int SuperPrestige()
        {
            int relics = 0;
            if (AOMAssets.AvatarLevelVG.GetBalance() >= prestigeMinLevel)
            {
                IList<int> result = Formulas.PrestigeRelic(_heroCore.TotalAliveHeroLevel, _enemyCore.Stage,
                    ArtifactAssets.Artifact022.EffectValue + ArtifactAssets.MoeSpirit004.EffectValue);
                relics = (result[0] + result[1]) * (_heroCore.AllHeroAlive ? 2 : 1);

                EventManager.SuperPrestige();

                RecalAll();

                //TODO stop all active skill
                _avatarSkillCore.RefreshSkill(true);
                EventManager.PrestigeComplete();
                GamePlayStatManager.Instance.Prestige();
            }
            return relics;
        }

        private void RecalCriticalChance()
        {
            double heroCriticalChanceBuff = 0;
            _heroCore.EffectValue.TryGetValue(EffectType.CriticalChance, out heroCriticalChanceBuff);
            // Artifact004 - +2% Critical Chance per artifact level
            double chance = BASE_CRITICAL_CHANCE + heroCriticalChanceBuff + ArtifactAssets.Artifact004.EffectValue;

            if (CriticalStrikeASEnabled)
            {
                chance += AOMAssets.CriticalStrikeASVG.EffectValue;
            }

            criticalChance = chance;
        }

        private void RecalTapDamage()
        {
            HeroCore heroCore = _heroCore;

            double heroAllDamageBuff = 0;
            double heroTapDamageBuff = 0;
            double heroBossDamageBuff = 0;
            double heroDPSTapDamageBuff = 0;

            heroCore.EffectValue.TryGetValue(EffectType.AllDamage, out heroAllDamageBuff);
            heroCore.EffectValue.TryGetValue(EffectType.TapDamage, out heroTapDamageBuff);
            heroCore.EffectValue.TryGetValue(EffectType.BossDamage, out heroBossDamageBuff);
            heroCore.EffectValue.TryGetValue(EffectType.TotalDPSTapDamage, out heroDPSTapDamageBuff);

            double artifactEffectValue = (1 + ArtifactAssets.Artifact029.EffectValue) *
                                         (1 + ArtifactAssets.MoeSpirit001.EffectValue) *
                                         (1 + ArtifactAssets.MoeSpirit009.EffectValue) *
                                         (1 + ArtifactAssets.MoeSpirit024.EffectValue);

            tapDamage = (baseDamage * (1 + heroAllDamageBuff) + heroDPSTapDamageBuff) * (1 + heroTapDamageBuff) * (1 + _artifactCore.AllDamageBuffTotal) * artifactEffectValue;

            tapNextLVDamage = (baseNextLVDamage * (1 + heroAllDamageBuff) + heroDPSTapDamageBuff) * (1 + heroTapDamageBuff) * (1 + _artifactCore.AllDamageBuffTotal) * artifactEffectValue;

            if (_enemyCore.CurrentEnemyType == EnemyType.BigBoss || _enemyCore.CurrentEnemyType == EnemyType.MiniBoss)
            {
                tapDamage *= (1 + heroBossDamageBuff);
                tapNextLVDamage *= (1 + heroBossDamageBuff);
            }

            if (berserkerRageEnabled)
            {
                tapDamage *= (1 + DamageFormulas.ActiveSkillBerserkerRageIncreaseForSkillLevel(AOMAssets.BerserkerRageASVG.GetBalance()));
                tapNextLVDamage *= (1 + DamageFormulas.ActiveSkillBerserkerRageIncreaseForSkillLevel(AOMAssets.BerserkerRageASVG.GetBalance()));
            }

            tapDamage = Math.Round(tapDamage, MidpointRounding.AwayFromZero);
            tapNextLVDamage = Math.Round(tapNextLVDamage, MidpointRounding.AwayFromZero);
            RecalCriticalDamage();
        }

        private void RecalCriticalDamage()
        {
            double heroCriticalDamageBuff = 0;
            _heroCore.EffectValue.TryGetValue(EffectType.CriticalDamage, out heroCriticalDamageBuff);

            criticalDamage = tapDamage * (1 + heroCriticalDamageBuff + ArtifactAssets.Artifact017.EffectValue + ArtifactAssets.MoeSpirit012.EffectValue);
        }
    }
}