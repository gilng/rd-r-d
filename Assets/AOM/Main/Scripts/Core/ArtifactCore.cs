﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using Soomla.Store;
using UnityEngine.Analytics;
using Zenject;

namespace Ignite.AOM
{
    public class ArtifactCore : IInitializable, IDisposable
    {
        public const string PP_PURCHASED_AF = "PP_PURCHASED_AF";

        private double allDamageBuffTotal = 0;

        public double AllDamageBuffTotal
        {
            get
            {
                return this.allDamageBuffTotal;
            }
        }

        // Use this for initialization
        public void Initialize()
        {
            
        }

        public void InitCore()
        {
            EventManager.OnArtifactVGBalanceChange += OnArtifactVGBalanceChange;
        }

        public void Dispose()
        {
            EventManager.OnArtifactVGBalanceChange -= OnArtifactVGBalanceChange;
        }

        public void RecalAll()
        {
            RecalArtifactAllDamageBuff();
        }

        private void OnArtifactVGBalanceChange(ArtifactVG vg, int lv)
        {
            RecalArtifactAllDamageBuff();
        }

        private void RecalArtifactAllDamageBuff()
        {
            double newTotal = 0;

            List<ArtifactVG> ownedItems = new List<ArtifactVG>();
            ownedItems.AddRange( FindOwnedArtifact() );
            ownedItems.AddRange( FindOwnedMoeSpirit() );

            for (int i = 0; i < ownedItems.Count; i++)
            {
                ArtifactVG vg = ownedItems[i];
                newTotal += vg.AllDamageValue;
            }

            newTotal += ArtifactAssets.MoeSpirit010.EffectValue;

            allDamageBuffTotal = newTotal;

            //if (instance != null)
            //{ // To Avoid Infinity Loop
                EventManager.ArtifactAllDamageBuffTotalChange(newTotal);
            //}
        }

        public int GachaCost()
        {
            return GachaCost(ArtifactNotOwned());
        }

        public int SpentGachaCost()
        {
            int result = 0;
            int i = FindOwnedArtifact().Count;
            while (i > 0)
            {
                result += Mathf.FloorToInt(i * Mathf.Pow(1.35f, i));
                i--;
            }
            return result;
        }

        public int SalvageCost(ArtifactVG artifactVG)
        {
            int gemsCost = 140;

            //return Mathf.Max(Mathf.RoundToInt((SpentGachaCost() + artifactVG.SpentCostOnUpgrade()) * 0.2f), 10);
            return gemsCost;
        }

        public int RelicEarnFromSalvage(ArtifactVG artifactVG)
        {
            int cac = ArtifactAssets.Artifacts.Count - ArtifactNotOwned().Count;
            return Mathf.Max(Mathf.FloorToInt((GachaCost(cac) + artifactVG.SpentCostOnUpgrade()) * 0.8f), 1);
        }

        public void Salvage(ArtifactVG artifactVG)
        {
            int cost = 140;

            RemoveAFFromPurchasedList(artifactVG.ItemId);

            artifactVG.ResetBalance(0);

            RecalArtifactAllDamageBuff();

            EventManager.DiamondBalanceUpdated();
            EventManager.SalvageUpdated();

            // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.CustomEvent("useGems", new Dictionary<string, object>
                {
                    { "type", "Salvage Moetifact" },
                });
#endif
        }

        // Gacha
        private int GachaCost(int cac)
        {
            return Mathf.FloorToInt(cac * Mathf.Pow(1.35f, cac));
        }

        private int GachaCost(IList<ArtifactVG> artifactNotOwned)
        {
            int cac = ArtifactAssets.Artifacts.Count - artifactNotOwned.Count + 1;
            return GachaCost(cac);
        }

        public bool CanAffordGacha()
        {
            return CanAffordGacha(ArtifactNotOwned());
        }

        public bool CanAffordGacha(IList<ArtifactVG> artifactNotOwned)
        {
            bool canAffordGacha = false;

            if (UserManager.Instance.CurrentUserSummary != null)
                canAffordGacha = artifactNotOwned.Count > 0 && GachaCost(artifactNotOwned) <=
                                 UserManager.Instance.CurrentUserSummary.Moecrystal;

            return canAffordGacha;
        }

        // Moetifact
        public IList<ArtifactVG> FindOwnedArtifact()
        {
            return ArtifactAssets.Artifacts.Where(a => a.GetBalance() > 0).ToList();
        }

        private IList<ArtifactVG> ArtifactNotOwned()
        {
            return ArtifactAssets.Artifacts.Where(a => a.GetBalance() == 0).ToList();
        }

        private IList<ArtifactVG> ArtifactNotLvMax()
        {
            return ArtifactAssets.Artifacts.Where(a => a.GetBalance() != a.MaxLevel).ToList();
        }

        public IList<ArtifactVG> ArtifactRareFilter(int rareLevel)
        {
            IList<ArtifactVG> targetArtifactList = rareLevel > 0 ? ArtifactAssets.Artifacts.Where(a => a.RareLevel == rareLevel).ToList() : ArtifactAssets.Artifacts.OrderBy(a => a.RareLevel).ToList();
            return targetArtifactList;
        }

        // MoeSpirit
        public IList<ArtifactVG> FindOwnedMoeSpirit()
        {
            return ArtifactAssets.MoeSpirits.Where(a => a.GetBalance() > 0).ToList();
        }

        public IList<ArtifactVG> MoeSpiritRareFilter(int rareLevel)
        {
            IList<ArtifactVG> targetMoeSpiritList = rareLevel > 0 ? ArtifactAssets.MoeSpirits.Where(a => a.RareLevel == rareLevel).ToList() : ArtifactAssets.MoeSpirits.OrderBy(a => a.RareLevel).ToList();
            return targetMoeSpiritList;
        }

        /// <summary>
        /// Method to buy a random artifact
        /// </summary>
        public void Gacha(bool withDuplicate, int gemsCost = 0, Action<ArtifactVG> successCallback = null)
        {
            IList<ArtifactVG> targetArtifactList = withDuplicate ? ArtifactNotLvMax() : ArtifactNotOwned();

            List<ArtifactVG> targetArtifactList_temp = new List<ArtifactVG>();
            targetArtifactList_temp.AddRange(targetArtifactList);

            // Chance ( 1/4 : remove moetifact from random list by each )
            if (targetArtifactList_temp.Count > 3)
            {
                int chance = 25;

                if ( targetArtifactList_temp.Contains(ArtifactAssets.Artifact030) && UnityEngine.Random.Range(0, 100) >= chance )
                    targetArtifactList_temp.Remove(ArtifactAssets.Artifact030);

                if ( targetArtifactList_temp.Contains(ArtifactAssets.Artifact031) && UnityEngine.Random.Range(0, 100) >= chance )
                    targetArtifactList_temp.Remove(ArtifactAssets.Artifact031);

                if ( targetArtifactList_temp.Contains(ArtifactAssets.Artifact032) && UnityEngine.Random.Range(0, 100) >= chance )
                    targetArtifactList_temp.Remove(ArtifactAssets.Artifact032);
            }

			// Remove Moetiface which cannot get from gacha
            foreach(ArtifactVG removeVG in ArtifactAssets.Artifacts)
            {
                if (targetArtifactList_temp.Contains(removeVG) && !removeVG.CanGacha())
                    targetArtifactList_temp.Remove(removeVG);
            }

            ArtifactVG vg = targetArtifactList_temp[UnityEngine.Random.Range(0, targetArtifactList_temp.Count)];

            if (withDuplicate)
            {
                UserManager.Instance.TakeGems(gemsCost, () =>
                {
                    if (vg.GetBalance() == 0)
                        PlayerPrefs.SetString(PP_PURCHASED_AF, PlayerPrefs.GetString(PP_PURCHASED_AF, "") + vg.ItemId + ",");

                    vg.Give(1);

                    GamePlayStatManager.Instance.OwnArtifact(FindOwnedArtifact().Count);

                    // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                    Analytics.CustomEvent("useGems", new Dictionary<string, object>
                    {
                        { "type", "Gacha Moetifact" },
                    });
#endif

                    successCallback(vg);
                });
            }
            else
            {
                UserManager.Instance.TakeMoecrystals(GachaCost(targetArtifactList), () =>
                {
                    if (vg.GetBalance() == 0)
                        PlayerPrefs.SetString(PP_PURCHASED_AF, PlayerPrefs.GetString(PP_PURCHASED_AF, "") + vg.ItemId + ",");

                    vg.Give(1);

                    GamePlayStatManager.Instance.OwnArtifact(FindOwnedArtifact().Count);

                    successCallback(vg);
                });

            }
        }

        public ArtifactVG BuySpecifyOne(string id, int giveLevel = 0)
        {
            ArtifactVG vg = null;

            IList<ArtifactVG> artifactNotOwned = ArtifactNotOwned();

            foreach (ArtifactVG afvg in artifactNotOwned)
            {
                if (afvg.ItemId == id)
                {
                    vg = afvg;

                    PlayerPrefs.SetString(PP_PURCHASED_AF, PlayerPrefs.GetString(PP_PURCHASED_AF, "") + vg.ItemId + ",");

                    vg.Give(giveLevel > 0 ? giveLevel : vg.MaxLevel);

                    GamePlayStatManager.Instance.OwnArtifact(FindOwnedArtifact().Count);
                }
            }
            return vg;
        }

        public ArtifactVG GiveById(string id, int Amount = 1)
        {
            ArtifactVG vg = null;

            foreach (ArtifactVG afvg in ArtifactAssets.Artifacts)
            {
                if (afvg.ItemId == id)
                {
                    vg = afvg;

                    if (vg.GetBalance() != vg.MaxLevel)
                    {
                        if (vg.GetBalance() == 0)
                            PlayerPrefs.SetString(PP_PURCHASED_AF, PlayerPrefs.GetString(PP_PURCHASED_AF, "") + vg.ItemId + ",");

                        vg.Give(Amount);

                        GamePlayStatManager.Instance.OwnArtifact(FindOwnedArtifact().Count);
                    }
                }
            }

            return vg;
        }

        public ArtifactVG RestorePurchasedArtifact(string id, int lv = 1)
        {
            ArtifactVG vg = null;

            IList<ArtifactVG> artifactNotOwned = ArtifactNotOwned();

            foreach (ArtifactVG afvg in artifactNotOwned)
            {
                if (afvg.ItemId == id)
                {
                    vg = afvg;
                    PlayerPrefs.SetString(PP_PURCHASED_AF, PlayerPrefs.GetString(PP_PURCHASED_AF, "") + vg.ItemId + ",");
                    vg.ResetBalance(lv);
                }
            }
            return vg;
        }

        public IList<string> PurchasedAFIds()
        {
            IList<string> ids = new List<string>();
            string idString = PlayerPrefs.GetString(PP_PURCHASED_AF, "");
            string[] idStringList = idString.Split(',');
            for (int i = 0; i < idStringList.Length; i++)
            {
                string s = idStringList[i];
                if (s != null && s.Length > 0)
                {
                    ids.Add(s);
                }
            }
            return ids;
        }

        private void RemoveAFFromPurchasedList(string itemId)
        {
            string idString = PlayerPrefs.GetString(PP_PURCHASED_AF, "");
            idString = idString.Replace(itemId + ",", "");
            PlayerPrefs.SetString(PP_PURCHASED_AF, idString);
        }
    }
}