using UnityEngine;
using System.Collections;
using Soomla.Store;
using Ignite.AOM;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using UnityEngine.Analytics;
using Zenject;

namespace Ignite.AOM
{
    public class HeroCore : IInitializable, IDisposable
    {
        private readonly EnemyCore _enemyCore;
        private readonly ArtifactCore _artifactCore;

        [Inject]
        public HeroCore(EnemyCore enemyCore, ArtifactCore artifactCore)
        {
            _enemyCore = enemyCore;
            _artifactCore = artifactCore;
        }

        private const string PP_HERO_REVIVE_TIME = "PP_HERO_REVIVE_TIME";

        private double totalHeroDPS;
        private double[] heroDPS;
        private double[] heroDPSOnNextLevel;
        private bool[] heroAlive;
        private IDictionary<EffectType, double> effectValue;
        private double boostSpeed = 1;//war cry
//		private bool bossFight=false;
        private int totalHeroLevel;
        private int totalAliveHeroLevel;
        private IDictionary<int, DateTime> heroReviveTime;

        private void ReadHeroReviveTime()
        {
            string data = PlayerPrefs.GetString(PP_HERO_REVIVE_TIME);

            if (data == "")
            {
                PlayerPrefs.SetString(PP_HERO_REVIVE_TIME, ",");
                data = PlayerPrefs.GetString(PP_HERO_REVIVE_TIME);
                //                Debug.Log(data);
            }

            if (data != "")
            {
                try
                {
                    // For Revive Bug Fix
                    if (data[0] != ',')
                    {
                        string fixedData = data.Insert(0, ",");
                        PlayerPrefs.SetString(PP_HERO_REVIVE_TIME, fixedData);
                        data = fixedData;

                        Debug.Log("Bug Fixed - " + data);
                    }
                    //

                    String[] herosReviveTime = data.Split(',');
                    for (int i = 0; i < herosReviveTime.Length; i++)
                    {
                        if (herosReviveTime[i].Length > 2)
                        {
                            string[] heroReviveTimePair = herosReviveTime[i].Split('_');

                            int heroIdx = int.Parse(heroReviveTimePair[0]);
                            double heroReviveTimestamp = double.Parse(heroReviveTimePair[1]);

                            DateTime reviveTime = UnitConverter.ConvertToDateTime(heroReviveTimestamp);
                            heroReviveTime[heroIdx] = reviveTime;
                            heroAlive[heroIdx] = false;
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError("Bug Fixed - Index Out Of Range Exception");
                    PlayerPrefs.SetString(PP_HERO_REVIVE_TIME, ",");
                }
            }
        }

        private void RemoveHeroReviveTime(int heroIdx)
        {
            heroAlive[heroIdx] = true;
            heroReviveTime.Remove(heroIdx);
            string data = PlayerPrefs.GetString(PP_HERO_REVIVE_TIME, ",");
            int startPos = data.IndexOf("," + heroIdx + "_");
            int endPos = data.IndexOf(",", startPos + 1);
            data = data.Remove(startPos, endPos - startPos);
            //			Debug.Log (heroIdx);
            //			Debug.Log (startPos + " " + endPos);
            //			Debug.Log (data);
            PlayerPrefs.SetString(PP_HERO_REVIVE_TIME, data);
        }

        private void ClearHeroReviveTime()
        {
            PlayerPrefs.SetString(PP_HERO_REVIVE_TIME, ",");
        }

        private void AddHeroReviveTime(int heroIdx, DateTime reviveTime)
        {
            heroAlive[heroIdx] = false;
            heroReviveTime[heroIdx] = reviveTime;
            StringBuilder sb = new StringBuilder();
            string data = PlayerPrefs.GetString(PP_HERO_REVIVE_TIME, ",");
            sb.Append(data);
            sb.Append(heroIdx.ToString());
            sb.Append("_");
            sb.Append(UnitConverter.ConvertToTimestamp(reviveTime).ToString());
            sb.Append(",");
            PlayerPrefs.SetString(PP_HERO_REVIVE_TIME, sb.ToString());
            sb.Clear();
        }

        // Use this for initialization
        public void Initialize()
        {
            heroReviveTime = new Dictionary<int, DateTime>();
            heroDPS = new double[HeroAssets.Heroes.Count];
            heroDPSOnNextLevel = new double[HeroAssets.Heroes.Count];
            heroAlive = new bool[HeroAssets.Heroes.Count];

            for (int i = 0; i < HeroAssets.Heroes.Count; i++)
            {
                heroDPS[i] = 0;
                heroDPSOnNextLevel[i] = 0;
                heroAlive[i] = true;
            }

            effectValue = new Dictionary<EffectType, double>();
        }

        public void InitCore()
        {
            //TODO read hero status
            for (int i = 0; i < HeroAssets.Heroes.Count; i++)
            {
                if (HeroAssets.Heroes[i].GetBalance() > HeroAssets.Heroes[i].MaxLevel)
                    HeroAssets.Heroes[i].ResetBalance(HeroAssets.Heroes[i].MaxLevel, false);
            }

            EventManager.OnLevelUpVGBalanceChange += OnLevelChanged;
            EventManager.OnArtifactVGBalanceChange += OnArtifactVGBalanceChange;
            EventManager.OnArtifactAllDamageBuffTotalChange += OnArtifaceAllDamageBuffChange;
            EventManager.OnPrestige += ResetHerosData;
            EventManager.OnEnemyTypeChange += OnEnemyTypeChange;
            EventManager.OnHeroDead += HeroDeadListener;
            EventManager.OnHeroRevive += HeroReviveListener;
        }

        public void Dispose()
        {
            EventManager.OnLevelUpVGBalanceChange -= OnLevelChanged;
            EventManager.OnArtifactVGBalanceChange -= OnArtifactVGBalanceChange;
            EventManager.OnArtifactAllDamageBuffTotalChange -= OnArtifaceAllDamageBuffChange;
            EventManager.OnPrestige -= ResetHerosData;
            EventManager.OnEnemyTypeChange -= OnEnemyTypeChange;
            EventManager.OnHeroDead -= HeroDeadListener;
            EventManager.OnHeroRevive -= HeroReviveListener;
        }

        public void HeroDeadListener(int heroIdx, DateTime reviveTime)
        {
            RecalAll();
        }

        public void HeroReviveListener(int heroIdx)
        {
            RecalAll();
        }

        public void RecalAll()
        {
            RecalAllSkillEffect();
            RecalAllHerosDPS();
            RecalTotalHeroLevel();
        }

        private void RecalAllSkillEffect()
        {
            foreach (EffectType type in Enum.GetValues(typeof(EffectType)).Cast<EffectType>())
            {
                if (type != EffectType.AvatarActiveSkill)
                {
                    RecalSkillEffect(type);
                }
            }
        }

        private void RecalSkillEffect(EffectType type)
        {
            double value = 0;
            if (HeroAssets.HeroSkillsByEffectType.ContainsKey(type))
            {
                IList<HeroPassiveSkillVG> skills = HeroAssets.HeroSkillsByEffectType[type];
                for (int i = 0; i < skills.Count; i++)
                {
                    HeroPassiveSkillVG s = skills[i];
                    if (s.GetBalance() > 0 && heroAlive[int.Parse(s.Character.ItemId.Replace("HE", "")) - 1])
                    {
                        value += s.EffectValue;//*s.GetBalance() for hero evolve;
                    }
                }
            }
            effectValue[type] = value;

            if (EffectType.TotalDPSTapDamage == type)
            {
                effectValue[type] *= totalHeroDPS;
            }

            EventManager.HeroSkillBuffTotalChange(type, value);
        }

        private void OnArtifactVGBalanceChange(ArtifactVG artifact, int lv)
        {
            if (artifact == ArtifactAssets.Artifact026)
            {
                RecalAllHerosDPS();
            }
        }

        private void OnArtifaceAllDamageBuffChange(double newVal)
        {
            RecalAllHerosDPS();
        }

        private void RecalTotalHeroDPS()
        {
            totalHeroDPS = 0;
            for (int i = 0; i < heroDPS.Length; i++)
            {
                double dps = heroDPS[i];
                if (heroAlive[i])
                {
                    totalHeroDPS += dps;
                }
            }

            GamePlayStatManager.Instance.ReachHeroDPS(totalHeroDPS / BoostSpeed);

            RecalSkillEffect(EffectType.TotalDPSTapDamage);
        }

        private void RecalAllHerosDPS()
        {
            for (int i = 0; i < HeroAssets.Heroes.Count; i++)
            {
                RecalHeroDPS(i, false);
            }
            RecalTotalHeroDPS();
        }

        private void RecalHeroDPS(int heroIdx, bool recalTotalDPS = true)
        {
            //			heroDPS[heroIdx]=0;
            if (heroAlive[heroIdx] && heroIdx < HeroAssets.Heroes.Count)
            {
                CharacterLevelVG hero = HeroAssets.Heroes[heroIdx];
                if (hero.GetBalance() >= 0)
                {
                    double baseDPS = DamageFormulas.HeroBaseDPS(heroIdx + 1, hero.GetBalance(), hero.baseCost);
                    double nextLevelBaseDPS = DamageFormulas.HeroBaseDPS(heroIdx + 1, hero.GetBalance() + 1, hero.baseCost);
                    double heroDamage = 1;

                    if (HeroAssets.HeroSkillsByHeroId.ContainsKey(hero.ItemId))
                    {
                        for (int i = 0; i < HeroAssets.HeroSkillsByHeroId[hero.ItemId].Count; i++)
                        {
                            HeroPassiveSkillVG s = HeroAssets.HeroSkillsByHeroId[hero.ItemId][i];
                            if (s.GetBalance() > 0 && s.EffectType == EffectType.HeroDamage)
                            {
                                heroDamage += s.EffectValue;//*s.GetBalance() for hero evolve;
                            }
                        }
                    }

                    heroDamage += ArtifactAssets.MoeSpirit011.EffectValue;
                    heroDamage += ArtifactAssets.MoeSpirit019.EffectValue;
                    heroDamage += ArtifactAssets.MoeSpirit029.EffectValue;

                    double allDamageEffectValue = 0;
                    if (effectValue.ContainsKey(EffectType.AllDamage))
                    {
                        allDamageEffectValue = effectValue[EffectType.AllDamage];
                    }

                    double bossDamageEffectValue = 0;

                    EnemyType enemyType = _enemyCore.CurrentEnemyType;
                    bool bossFight = (enemyType == EnemyType.MiniBoss || enemyType == EnemyType.BigBoss);

                    if (effectValue.ContainsKey(EffectType.BossDamage) && bossFight)
                    {
                        bossDamageEffectValue = effectValue[EffectType.BossDamage];
                    }

                    double artifactBuff = 1 + _artifactCore.AllDamageBuffTotal + ArtifactAssets.Artifact026.EffectValue;
                    double newDps = baseDPS * heroDamage * (allDamageEffectValue + 1) * (bossDamageEffectValue + 1) * artifactBuff * boostSpeed;
                    double nextLevelDps = nextLevelBaseDPS * heroDamage * (allDamageEffectValue + 1) * (bossDamageEffectValue + 1) * artifactBuff * boostSpeed;
                    heroDPS[heroIdx] = Math.Floor(newDps);
                    heroDPSOnNextLevel[heroIdx] = Math.Floor(nextLevelDps);
                    //				Debug.Log("*set hero:"+heroIdx+" dps :"+heroDPS[heroIdx] );
                }
            }

            if (recalTotalDPS)
            {
                RecalTotalHeroDPS();
            }
        }

        private void RecalTotalHeroLevel()
        {
            int levels = 0;
            int aliveHeroLevel = 0;
            if (HeroAssets.Heroes != null && HeroAssets.Heroes.Count > 0)
            {
                int i = 0;
                for (int k = 0; k < HeroAssets.Heroes.Count; k++)
                {
                    CharacterLevelVG hero = HeroAssets.Heroes[k];
                    levels += hero.GetBalance();
                    aliveHeroLevel += heroAlive[i] ? hero.GetBalance() : 0;
                    i++;
                }
            }
            totalAliveHeroLevel = aliveHeroLevel;
            totalHeroLevel = levels;

            ReadHeroReviveTime();
        }

        public void KillHero(int stage)
        {
            int heroIdx = UnityEngine.Random.Range(0, HeroAssets.Heroes.Count);
            while (HeroAssets.Heroes[heroIdx].GetBalance() < 1 || !heroAlive[heroIdx])
            {
                heroIdx = UnityEngine.Random.Range(0, HeroAssets.Heroes.Count);
            }
            DateTime reviveTime = DateTime.Now;
            reviveTime = reviveTime.AddHours((double)Math.Min(24, Math.Ceiling((stage - 50d) / 10d)) * (1 - ArtifactAssets.Artifact023.EffectValue));
            HeroDead(heroIdx, reviveTime);
        }

        private void HeroDead(int heroIdx, DateTime reviveTime)
        {
            RecalAll();
            AddHeroReviveTime(heroIdx, reviveTime);
            EventManager.HeroDead(heroIdx, reviveTime);
        }

        public void ReviveHero(int heroIdx, int reduceDiamond = 0)
        {
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                if (UserManager.Instance.CurrentUserSummary.Gem >= reduceDiamond)
//                {
                    UserManager.Instance.TakeGems(reduceDiamond, () =>
                    {
                        HeroRevive(heroIdx);

                        // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                    Analytics.CustomEvent("useGems", new Dictionary<string, object>
                    {
                        { "type", "Revive Hero" },
                    });
#endif
                    });
//                }
//            }
        }

        private void HeroRevive(int heroIdx)
        {
            RecalAll();
            RemoveHeroReviveTime(heroIdx);
            EventManager.HeroRevive(heroIdx);
        }

        public void OnEnemyTypeChange(EnemyType t)
        {
            RecalAllHerosDPS();
        }

        //		public void OnClearStage(){
        //			bossFight=false;
        //			RecalAllHerosDPS();
        //		}

        private void OnLevelChanged(LevelUpVG good, int balance)
        {
            if (good.GetType() == typeof(CharacterLevelVG) && !good.ItemId.Equals(AOMAssets.AvatarLevelVG.ItemId))
            {
                RecalHeroDPS(int.Parse(good.ItemId.Replace("HE", "")) - 1);
                RecalTotalHeroLevel();
            }
            else if (good.GetType() == typeof(HeroPassiveSkillVG))
            {
                HeroPassiveSkillVG hpsvg = (HeroPassiveSkillVG)good;
                RecalSkillEffect(hpsvg.EffectType);
                if (hpsvg.EffectType == EffectType.AllDamage || hpsvg.EffectType == EffectType.BossDamage)
                {
                    RecalAllHerosDPS();
                }
                else if (hpsvg.EffectType == EffectType.HeroDamage)
                {
                    RecalHeroDPS(int.Parse(hpsvg.Character.ItemId.Replace("HE", "")) - 1);
                }
            }
        }

        private void ResetHerosData()
        {
            ClearHeroReviveTime();
            //			bossFight=false;
            heroDPS = new double[HeroAssets.Heroes.Count];
            heroAlive = new bool[HeroAssets.Heroes.Count];
            for (int i = 0; i < heroDPS.Length; i++)
            {
                heroDPS[i] = 0;
                heroDPSOnNextLevel[i] = 0;
                heroAlive[i] = true;
                HeroAssets.Heroes[i].ResetBalance(0, false);
                if (HeroAssets.HeroSkillsByHeroId.ContainsKey(HeroAssets.Heroes[i].ItemId))
                {
                    for (int y = 0; y < HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[i].ItemId].Count; y++)
                    {
                        HeroPassiveSkillVG skill = HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[i].ItemId][y];
                        skill.ResetBalance(0, false);
                    }
                }
            }
            effectValue.Clear();
            RecalAll();
        }

        public void SyncCloudHeroAliveState(SaveData loadedSaveData)
        {
            if (loadedSaveData.aliveHeroList.Count > 0)
            {
                for (int i = 0; i < loadedSaveData.aliveHeroList.Count; i++)
                {
                    if (loadedSaveData.aliveHeroList[i])
                    {
                        heroAlive[i] = true;
                    }
                    else
                    {
                        if (loadedSaveData.heroReviveTimeList.Count > 0)
                            AddHeroReviveTime(i, loadedSaveData.heroReviveTimeList[i]);
                    }
                }

                PlayerPrefs.SetString(PP_HERO_REVIVE_TIME, loadedSaveData.pp_ReviveTime);
            }
        }

        public IDictionary<EffectType, double> EffectValue
        {
            get { return this.effectValue; }
        }

        public double[] HeroDPS
        {
            get { return this.heroDPS; }
        }

        public double[] HeroDPSOnNextLevel
        {
            get { return this.heroDPSOnNextLevel; }
        }

        public bool[] HeroAlive
        {
            get { return this.heroAlive; }
        }

        public double TotalHeroDPS
        {
            get { return this.totalHeroDPS; }
        }

        public double BoostSpeed
        {
            get { return this.boostSpeed; }
        }

        public void UpdateBoostSpeed(double val)
        {
            this.boostSpeed = val;
            RecalAllHerosDPS();
        }

        public int TotalHeroLevel
        {
            get { return this.totalHeroLevel; }
        }

        public int TotalAliveHeroLevel
        {
            get { return this.totalAliveHeroLevel; }
        }

        public bool AllHeroAlive
        {
            get
            {
                for (int i = 0; i < heroAlive.Length; i++)
                {
                    if (!heroAlive[i])
                        return false;
                }
                return true;
            }
        }

        public IDictionary<int, DateTime> HeroReviveTime
        {
            get { return heroReviveTime; }
        }

        public string PP_HeroReviveTime
        {
            get { return PlayerPrefs.GetString(PP_HERO_REVIVE_TIME, ","); }
        }
    }
}
