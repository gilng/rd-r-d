﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

namespace Ignite.AOM
{
    public class UIViewLoader : IInitializable
    {
        private UIViewController.Factory _uiViewControllerFactory;

        [Inject]
        public void ConstructorSafeAttribute(UIViewController.Factory uiViewControllerFactory)
        {
            _uiViewControllerFactory = uiViewControllerFactory;
        }

        protected GameObject pageContainerParent;
        protected GameObject overlayContainerParent;
        protected GameObject tipsContainerParent;

        // Use this for initialization
        public void Initialize()
        {
            this.pageContainerParent = GameObject.Find("Page_Container_Group");
            this.overlayContainerParent = GameObject.Find("Overlay_Container_Group");
            this.tipsContainerParent = GameObject.Find("Tips_Container_Group");
        }

        // Load Page View
        public GameObject LoadPageView(string pageViewName, UIViewController previousUIViewController)
        {
            GameObject pagePanel = LoadUIView(UIViewController.UIViewType.PAGE, pageViewName, previousUIViewController, true, false);

            if (this.pageContainerParent == null)
                this.pageContainerParent = GameObject.Find("Page_Container_Group");

            if (this.pageContainerParent != null && pagePanel != null)
                pagePanel.transform.SetParent(this.pageContainerParent.transform, false);

            return pagePanel;
        }

        // Load Overlay View
        public GameObject LoadOverlayView(string overlayViewName, UIViewController previousUIViewController, bool closeCurrentView, bool pause)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.PopupShow_1);

            GameObject overlayPanel = LoadUIView(UIViewController.UIViewType.OVERLAY, overlayViewName, previousUIViewController, closeCurrentView, pause);

            if (this.overlayContainerParent == null)
                this.overlayContainerParent = GameObject.Find("Overlay_Container_Group");

            if (this.overlayContainerParent != null && overlayPanel != null)
            {
                overlayPanel.transform.SetParent(this.overlayContainerParent.transform, false);
            }

            return overlayPanel;
        }

        // Load Tips View
        public GameObject LoadTipsView(string tipsViewName, bool pause)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.PopupShow_1);

            GameObject tipsPanel = LoadUIView(UIViewController.UIViewType.TIPS, tipsViewName, null, false, pause);

            if (this.tipsContainerParent == null)
                this.tipsContainerParent = GameObject.Find("Tips_Container_Group");

            if (this.tipsContainerParent != null && tipsPanel != null)
                tipsPanel.transform.SetParent(this.tipsContainerParent.transform, false);

            return tipsPanel;
        }

        // Loading View
        UIViewController loadingViewController;
        
        public void EnableLoadingView()
        {
            loadingViewController = _uiViewControllerFactory.Create("Overlay/LoadingOverlayView") as UIViewController;

            GameObject loadingPanel = LoadUIView(UIViewController.UIViewType.OVERLAY, "LoadingOverlayView", null, false, false);

            if (this.overlayContainerParent == null)
                this.overlayContainerParent = GameObject.Find("Overlay_Container_Group");

            if (this.overlayContainerParent != null && loadingPanel != null)
                loadingPanel.transform.SetParent(this.overlayContainerParent.transform, false);
        }

        public void DisableLoadingView()
        {
            if (loadingViewController != null)
            {
                loadingViewController.CloseView();
                loadingViewController = null;
            }
        }

        // Load UI View
        GameObject LoadUIView(UIViewController.UIViewType viewType, string pageViewName, UIViewController previousUIViewController, bool closeCurrentView, bool pause)
        {
            GameObject viewPanel;

            string prefabPath = "";

            switch (viewType)
            {
                case UIViewController.UIViewType.PAGE:
                    prefabPath = "Page/" + pageViewName;
                    break;

                case UIViewController.UIViewType.OVERLAY:
                    prefabPath = "Overlay/" + pageViewName;
                    break;

                case UIViewController.UIViewType.TIPS:
                    prefabPath = "Tips/" + pageViewName;
                    break;
            }

            UIViewController viewController = _uiViewControllerFactory.Create(prefabPath) as UIViewController;

            viewPanel = viewController.gameObject;

            if (viewPanel)
            {
                if (previousUIViewController != null)
                {
                    switch (previousUIViewController.currentUIViewType)
                    {
                        case UIViewController.UIViewType.PAGE:

                            if (viewType == UIViewController.UIViewType.PAGE)
                            {
                                var allPagesInParentContainer = this.pageContainerParent.GetComponentsInChildren<UIViewController>();

                                foreach (var pageInParentContainer in allPagesInParentContainer)
                                {
                                    if (pageInParentContainer.gameObject != viewPanel)
                                    {
                                        previousUIViewController.CloseView();
                                        viewController.InitView(pageInParentContainer, pause);
                                    }
                                }
                            }
                            else 
                            {
                                viewController.InitView(null, pause);
                            }

                            break;

                        case UIViewController.UIViewType.OVERLAY:
                        case UIViewController.UIViewType.TIPS:

                            if (viewType == UIViewController.UIViewType.PAGE)
                            {
                                var allOverlaysInParentContainer = this.overlayContainerParent.GetComponentsInChildren<UIViewController>();

                                foreach (var overlayInParentContainer in allOverlaysInParentContainer)
                                {
                                    if (overlayInParentContainer.gameObject != viewPanel)
                                    {
                                        overlayInParentContainer.CloseView();
                                    }
                                }

                                var allPagesInParentContainer = this.pageContainerParent.GetComponentsInChildren<UIViewController>();

                                foreach (var pageInParentContainer in allPagesInParentContainer)
                                {
                                    if (pageInParentContainer.gameObject != viewPanel)
                                    {
                                        pageInParentContainer.CloseView();
                                        viewController.InitView(pageInParentContainer, pause);
                                    }
                                }
                            }
                            else if (viewType == UIViewController.UIViewType.OVERLAY || viewType == UIViewController.UIViewType.TIPS)
                            {
                                if (closeCurrentView)
                                    previousUIViewController.CloseView();

                                viewController.InitView(previousUIViewController, pause);
                            }

                            break;
                    }
                }
                else
                {
                    viewController.InitView(null, pause);
                }
            }
            else
            {
                Debug.LogError("UIViewLoader - " + pageViewName + " isn't available");
            }

            return viewPanel;
        }
    }
}
