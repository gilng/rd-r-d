﻿using UnityEngine;
using System.Collections;
using System;

namespace Ignite.AOM
{
    public enum MonsterGirlStatus
    {
        NeverShow = 0,
        Showed,
        Killed
    }

    public class Gallery
    {
        private const string PP_GALLERY_GIRL_EXP = "PP_GALLERY_GIRL_EXP_";
        private const string PP_GALLERY_GIRL_STATUS = "PP_GALLERY_GIRL_STATUS_";
        private const string PP_GALLERY_GIRL_REACHED_BREAK_LEVEL = "PP_GALLERY_GIRL_REACHED_BREAK_LEVEL_";

        private static readonly int MAX_EXP = 200;

        public static readonly int LEVEL_1_EXP = 0;
        public static readonly int LEVEL_2_EXP = 5;
        public static readonly int LEVEL_3_EXP = 35;
        public static readonly int LEVEL_4_EXP = 60;
        public static readonly int LEVEL_5_EXP = 100;

        public int[] levels = { LEVEL_1_EXP, LEVEL_2_EXP, LEVEL_3_EXP, LEVEL_4_EXP, LEVEL_5_EXP };

        public string girlId;
        public int bgId;
        public int fgId;
        private int exp;
        private int status;
        private int reachedBreakLevel;

        private int[] breakLevel;

		public Gallery(string girlId, int bgId = 1, int fgId = 1, int breakLv1 = -1, int breakLv2 = 0, int breakLv3 = 0)
        {
            this.girlId = girlId;
            this.bgId = bgId;
            this.fgId = fgId;
            exp = PlayerPrefs.GetInt(PP_GALLERY_GIRL_EXP + girlId, 0);
            status = PlayerPrefs.GetInt(PP_GALLERY_GIRL_STATUS + girlId, 0);
            reachedBreakLevel = PlayerPrefs.GetInt(PP_GALLERY_GIRL_REACHED_BREAK_LEVEL + girlId, 1);

            breakLevel = new int[] {breakLv1, breakLv2, breakLv3};
        }

        public int Exp
        {
            set
            {
                exp = Math.Min(MAX_EXP, value);
                PlayerPrefs.SetInt(PP_GALLERY_GIRL_EXP + girlId, exp);
            }
            get
            {
                return exp;
            }
        }

        public int CalExp
        {
            get
            {
                int calEXP = exp;
                for (int i = 0; i < levels.Length; i++)
                {
                    if (calEXP - levels[i] >= 0)
                        calEXP -= levels[i];
                }

                return calEXP;
            }
        }

        public MonsterGirlStatus Status
        {
            set
            {
                if (status == (int)MonsterGirlStatus.Showed && value == MonsterGirlStatus.Killed)
                {
                    PlayerPrefs.SetInt(PP_GALLERY_GIRL_REACHED_BREAK_LEVEL + girlId + "_NEW", 1);
                    EventManager.NewGalleryReach();
                }

                status = (int)value;

                PlayerPrefs.SetInt(PP_GALLERY_GIRL_STATUS + girlId, (int)value);
            }
            get
            {
                switch (status)
                {
                    case (int)MonsterGirlStatus.NeverShow:
                        return MonsterGirlStatus.NeverShow;
                    case (int)MonsterGirlStatus.Showed:
                        return MonsterGirlStatus.Showed;
                    case (int)MonsterGirlStatus.Killed:
                        return MonsterGirlStatus.Killed;
                    default:
                        return MonsterGirlStatus.NeverShow;
                }
            }
        }

        public int ReachedBreakLevel
        {
            set
            {
                if (value != reachedBreakLevel)
                {
                    EventManager.NewGalleryReach();

                    reachedBreakLevel = value;
                    PlayerPrefs.SetInt(PP_GALLERY_GIRL_REACHED_BREAK_LEVEL + girlId, value);
                }
            }
            get { return reachedBreakLevel; }
        }

        public int[] BreakLevel
        {
            set { breakLevel = value; }
            get { return breakLevel; }
        }



        public void RestoreGirlsStatus(int status, int exp, int reachedBreakLevel)
        {
            switch (status)
            {
                case (int)MonsterGirlStatus.NeverShow:
                    Status = MonsterGirlStatus.NeverShow;
                    break;
                case (int)MonsterGirlStatus.Showed:
                    Status = MonsterGirlStatus.Showed;
                    break;
                case (int)MonsterGirlStatus.Killed:
                    Status = MonsterGirlStatus.Killed;
                    break;
                default:
                    Status = MonsterGirlStatus.NeverShow;
                    break;
            }

            Exp = exp;

            ReachedBreakLevel = reachedBreakLevel;
        }

        public bool CheckNewGallery()
        {
            return PlayerPrefs.GetInt(PP_GALLERY_GIRL_REACHED_BREAK_LEVEL + girlId + "_NEW", 0) > 0;
        }

        public void ReachedNewGallery()
        {
            if (PlayerPrefs.GetInt(PP_GALLERY_GIRL_REACHED_BREAK_LEVEL + girlId + "_NEW", 0) > 0)
                PlayerPrefs.SetInt(PP_GALLERY_GIRL_REACHED_BREAK_LEVEL + girlId + "_NEW", 0);
        }

        public int Level()
        {
            int lv = 1;
            int calEXP = exp;

            for (int i = 0; i < levels.Length; i++)
            {
                calEXP -= levels[i];
                if (calEXP >= 0)
                    lv = i + 1;
            }

            return lv;
        }
    }
}
