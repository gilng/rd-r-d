﻿using System;

namespace Ignite.AOM.Event
{
    public class GameStateEvents
    {
        public static event Action GameReady;

        public static void OnGameReady()
        {
            if (GameReady != null)
            {
                GameReady();
            }
        }
    }
}