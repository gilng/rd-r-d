using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class Effect01 : StateMachineBehaviour
    {
        public ObjectPool pool;
        public GameObject particles;
        //	    private Transform particlesTransform;       // Reference to the instantiated prefab's transform.
        //	    private ParticleSystem particleSystem;      // Reference to the instantiated prefab's particle system.

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            //	        if (particlesTransform != null)
            //	            return;

            // Otherwise instantiate the particles and set up references to their components.
            //			pool = (ObjectPool)GameObject.Find(particles.name + "_pool").GetComponent<ObjectPool>();

            int index = int.Parse("" + particles.name[particles.name.Length - 1]);

            if (Avatar.instance != null)
            {
                if (particles.name.Contains("sp02"))
                    pool = Avatar.instance.shadowCloneEffectPools[index - 1];
                else if (particles.name.Contains("poh"))
                    pool = Avatar.instance.powerOfHoldingPools[index - 1];
                else
                    pool = Avatar.instance.attackEffectPools[index - 1];
            }
            else if (PvPAvatar.instance != null)
            {
                pool = PvPAvatar.instance.attackEffectPools[index - 1];
            }


            //			Debug.Log("object pool: " + pool);
            GameObject particlesInstance = pool.GetPooledObject();//Instantiate(particles);
            particlesInstance.SetActive(true);
            //	        particlesTransform = particlesInstance.transform;
            //	        particleSystem = particlesInstance.GetComponent<ParticleSystem>();
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        // override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //particleSystem.Stop();
        // }

        // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
        //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}

        // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
        //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}
    }
}