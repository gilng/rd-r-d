﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Zenject;

namespace Ignite.AOM
{
    public class SoulAnimationEffect : EffectController
    {
        private GalleryCore _galleryCore;

        [Inject]
        public void ConstructorSafeAttribute(GalleryCore galleryCore)
        {
            _galleryCore = galleryCore;
        }

        public GameObject eff_1, eff_2, eff_3;

        Vector3 goalPoint;

        float timer;
        float curveX, curveY;

        bool isPlayingEffect, isEndEffect;

        public void Wake()
        {
            this.gameObject.SetActive(true);

            timer = 0;

            PlayEffect();
        }

        void PlayEffect()
        {
            eff_1.SetActive(true);
            eff_2.SetActive(true);

            isPlayingEffect = true;
            isEndEffect = false;

            SoundManager.Instance.PlaySoundWithType(SoundType.BossDead_1);
        }

        public void EndEffect()
        {
            isPlayingEffect = false;
            isEndEffect = false;

            eff_1.SetActive(false);
            eff_2.SetActive(false);
            eff_3.SetActive(false);

            eff_2.transform.position = Vector3.zero;
            eff_3.transform.position = Vector3.zero;

            if (GameManager.Instance != null)
                goalPoint = GameManager.Instance.galleryButton.GetComponent<Image>().rectTransform.transform.position;

            this.gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            if (isPlayingEffect)
            {
                if (eff_2.activeSelf)
                {
                    timer += Time.deltaTime;

                    curveX = ((2 * timer * (1 - timer) * 1.5f) + (timer * timer) * goalPoint.x);
                    curveY = ((2 * timer * (1 - timer) * 1.5f) + (timer * timer) * goalPoint.y);

                    if (timer < 1)
                    {
                        eff_2.transform.position = new Vector3(curveX, curveY, 0);
                    }
                    else
                    {
                        eff_2.SetActive(false);
                        isEndEffect = true;
                    }
                }
                else
                {
                    if (eff_3.transform.position != goalPoint)
                        eff_3.transform.position = goalPoint;

                    eff_3.SetActive(true);
                    isPlayingEffect = false;

                    _galleryCore.RechargingTouchCredit(1);

                    if (GameManager.Instance != null)
                    {
                        GameManager.Instance.UpdateTouchCredit();

                        if (PlayerPrefs.GetInt("PP_GALLERY_GIRL_TOUCH_CREDIT", 5) < PlayerPrefs.GetInt("PP_GALLERY_GIRL_TOUCH_CREDIT_MAX", 5))
                        {
                            GameManager.Instance.rechargeAnime.transform.FindChild("Point/Text").gameObject.GetComponent<Text>().text = "+1";
                        }
                        else
                        {
                            GameManager.Instance.rechargeAnime.transform.FindChild("Point/Text").gameObject.GetComponent<Text>().text = "Max";
                        }

                        GameManager.Instance.rechargeAnime.SetActive(true);
                    }

                    SoundManager.Instance.PlaySoundWithType(SoundType.BossDead_2);
                }
            }
            else
            {
                if (isEndEffect && !eff_3.activeSelf)
                {
                    EndEffect();
                }
            }
        }
    }
}