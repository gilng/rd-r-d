﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class GoldBalanceEventBuffer : EventBuffer<double>
    {
        public static GoldBalanceEventBuffer instance;

        public override void SendToEventManager()
        {
            double val = 0;
            for (int i = 0; i < values.Count; i++)
            {
                val += values[i];
            }
            values.Clear();
            AOMStoreInventory.GiveGold(val);
        }

        void Awake()
        {
            instance = this;
        }
    }
}