using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using Zenject;

namespace Ignite.AOM
{
    public class ActiveSkillPanel : MonoBehaviour
    {
        private HeroCore _heroCore;
        private AvatarCore _avatarCore;
        private AvatarSkillCore _avatarSkillCore;

        [Inject]
        public void ConstructorSafeAttribute(HeroCore heroCore, AvatarCore avatarCore, AvatarSkillCore avatarSkillCore)
        {
            _heroCore = heroCore;
            _avatarCore = avatarCore;
            _avatarSkillCore = avatarSkillCore;
        }

        public static ActiveSkillPanel instance;

        Vector3 originalCameraPosition;
        float shakeAmt = 3.0f;
        public Camera mainCamera;

        public GameManager gameManager;
        public Avatar avatar;

        public Button heavenlyStrikeButton;
        public Button shadowCloneButton;
        public Button criticalStrikeButton;
        public Button warCryButton;
        public Button berserkerRageButton;
        public Button handOfMidasButton;

        public List<Text> timer_Text;
        public List<Image> timer_RemainCD;
        public List<Image> timer_RemainDuration;

        bool activeSkillTutorialCompleted = false;

        void Awake()
        {
            instance = this;

            for (int i = 0; i < timer_Text.Count; i++)
            {
                timer_Text[i].text = "";
                timer_Text[i].gameObject.SetActive(false);
            }

            for (int i = 0; i < timer_RemainCD.Count; i++)
            {
                timer_RemainCD[i].gameObject.SetActive(false);
                timer_RemainCD[i].transform.parent.GetComponent<Image>().color = Color.white;
            }

            for (int i = 0; i < timer_RemainDuration.Count; i++)
            {
                timer_RemainDuration[i].gameObject.SetActive(false);
            }

            // Checker - Active skill(s) have time remain
            if (_avatarSkillCore.ShadowCloneRemainDuration() > 0)
            {
                avatar.ShadowCloneEnable(true);
            }

            if (_avatarSkillCore.CriticalStrikeRemainDuration() > 0)
            {
                _avatarCore.CriticalStrikeASEnabled = true;
                avatar.ShowCriticalStrikeEffect(true);
            }

            if (_avatarSkillCore.WarCryRemainDuration() > 0)
            {
                double warCryIncrease = DamageFormulas.ActiveSkillWarCryIncreaseForSkillLevel(AOMAssets.WarCryASVG.GetBalance());
                _heroCore.UpdateBoostSpeed(warCryIncrease);
            }

            if (_avatarSkillCore.BerserkerRageRemainDuration() > 0)
            {
                _avatarCore.BerserkerRageEnabled = true;
                avatar.ShowBerserkerRageEffect(true);
            }

            if (_avatarSkillCore.HandOfMidasRemainDuration() > 0)
            {
                avatar.HandOfMidasEnable(true);
            }
        }

        // Use this for initialization
        void Start()
        {

            TutorialEventSetup();

            RefreshActiveSkillButton();
            EventManager.OnPrestigeComplete += RefreshActiveSkillButton;
            EventManager.OnPrestige += DisableAllEnabledSkill;
            EventManager.OnSuperPrestige += DisableAllEnabledSkill;

            originalCameraPosition = mainCamera.transform.position;
        }

        void DisableAllEnabledSkill()
        {
            if (avatar.isUsingShadowClone)
                avatar.ShadowCloneEnable(false);

            if (_avatarCore.CriticalStrikeASEnabled)
            {
                _avatarCore.CriticalStrikeASEnabled = false;
                avatar.ShowCriticalStrikeEffect(false);
            }

            if (_heroCore.BoostSpeed != 1)
            {
                _heroCore.UpdateBoostSpeed(1);
            }

            if (_avatarCore.BerserkerRageEnabled)
            {
                _avatarCore.BerserkerRageEnabled = false;
                avatar.ShowBerserkerRageEffect(false);
            }

            if (avatar.isUsingHandOfMidas)
                avatar.HandOfMidasEnable(false);
        }

        void TutorialEventSetup()
        {
            activeSkillTutorialCompleted = TutorialManager.Instance.CheckTutorialCompletedById(6);

            if (!activeSkillTutorialCompleted)
            {
                heavenlyStrikeButton.gameObject.AddComponent<TutorialReceiver>().TutorialEventSetup(6);
                shadowCloneButton.gameObject.AddComponent<TutorialReceiver>().TutorialEventSetup(6);
                criticalStrikeButton.gameObject.AddComponent<TutorialReceiver>().TutorialEventSetup(6);
                warCryButton.gameObject.AddComponent<TutorialReceiver>().TutorialEventSetup(6);
                berserkerRageButton.gameObject.AddComponent<TutorialReceiver>().TutorialEventSetup(6);
                handOfMidasButton.gameObject.AddComponent<TutorialReceiver>().TutorialEventSetup(6);
            }
            else
            {
                heavenlyStrikeButton.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);
                shadowCloneButton.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);
                criticalStrikeButton.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);
                warCryButton.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);
                berserkerRageButton.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);
                handOfMidasButton.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);
            }
        }

        void OnDestroy()
        {
            EventManager.OnPrestigeComplete -= RefreshActiveSkillButton;
            EventManager.OnPrestige -= DisableAllEnabledSkill;
            EventManager.OnSuperPrestige -= DisableAllEnabledSkill;
        }

        public void RefreshActiveSkillButton()
        {

            if (AOMAssets.HeavenlyStrikeASVG.GetBalance() > 0)
            {
                heavenlyStrikeButton.gameObject.SetActive(true);

                // Tutorial Event 06 Trigger
                if (!TutorialManager.Instance.CheckTutorialCompletedById(6))
                    TutorialManager.Instance.TriggerTutorial(6);
            }
            else
            {
                heavenlyStrikeButton.gameObject.SetActive(false);
            }

            if (AOMAssets.ShadowCloneASVG.GetBalance() > 0)
            {
                shadowCloneButton.gameObject.SetActive(true);

                // Tutorial Event 06 Trigger
                if (!TutorialManager.Instance.CheckTutorialCompletedById(6))
                    TutorialManager.Instance.TriggerTutorial(6);
            }
            else
            {
                shadowCloneButton.gameObject.SetActive(false);
            }

            if (AOMAssets.CriticalStrikeASVG.GetBalance() > 0)
            {
                criticalStrikeButton.gameObject.SetActive(true);

                // Tutorial Event 06 Trigger
                if (!TutorialManager.Instance.CheckTutorialCompletedById(6))
                    TutorialManager.Instance.TriggerTutorial(6);
            }
            else
            {
                criticalStrikeButton.gameObject.SetActive(false);
            }

            if (AOMAssets.WarCryASVG.GetBalance() > 0)
            {
                warCryButton.gameObject.SetActive(true);

                // Tutorial Event 06 Trigger
                if (!TutorialManager.Instance.CheckTutorialCompletedById(6))
                    TutorialManager.Instance.TriggerTutorial(6);
            }
            else
            {
                warCryButton.gameObject.SetActive(false);
            }

            if (AOMAssets.BerserkerRageASVG.GetBalance() > 0)
            {
                berserkerRageButton.gameObject.SetActive(true);

                // Tutorial Event 06 Trigger
                if (!TutorialManager.Instance.CheckTutorialCompletedById(6))
                    TutorialManager.Instance.TriggerTutorial(6);
            }
            else
            {
                berserkerRageButton.gameObject.SetActive(false);
            }

            if (AOMAssets.HandOfMidasASVG.GetBalance() > 0)
            {
                handOfMidasButton.gameObject.SetActive(true);

                // Tutorial Event 06 Trigger
                if (!TutorialManager.Instance.CheckTutorialCompletedById(6))
                    TutorialManager.Instance.TriggerTutorial(6);
            }
            else
            {
                handOfMidasButton.gameObject.SetActive(false);
            }
        }


        void Timer_Duration(int index, int duration, float duration_fillAmount)
        {
            timer_Text[index].gameObject.SetActive(true);
            timer_Text[index].text = String.Format("{0:00}:{1:00}", Mathf.Floor(duration / 60), duration % 60);

            timer_RemainCD[index].gameObject.SetActive(false);
            timer_RemainCD[index].transform.parent.GetComponent<Image>().color = new Color32(75, 75, 75, 255);

            timer_RemainDuration[index].gameObject.SetActive(true);
            timer_RemainDuration[index].fillAmount = duration_fillAmount;
        }

        void Timer_CoolDown(int index, int coolDown, float coolDown_fillAmount)
        {
            timer_Text[index].gameObject.SetActive(true);
            timer_Text[index].text = String.Format("{0:00}:{1:00}", Mathf.Floor(coolDown / 60), coolDown % 60);

            timer_RemainDuration[index].gameObject.SetActive(false);

            timer_RemainCD[index].gameObject.SetActive(true);
            timer_RemainCD[index].transform.parent.GetComponent<Image>().color = new Color32(75, 75, 75, 255);
            timer_RemainCD[index].fillAmount = coolDown_fillAmount;
        }

        void Timer_Reset(int index)
        {
            timer_Text[index].text = "";
            timer_Text[index].gameObject.SetActive(false);

            timer_RemainDuration[index].gameObject.SetActive(false);
            timer_RemainDuration[index].fillAmount = 1;

            timer_RemainCD[index].gameObject.SetActive(false);
            timer_RemainCD[index].transform.parent.GetComponent<Image>().color = Color.white;
            timer_RemainCD[index].fillAmount = 1;
        }


        // Update is called once per frame
        void Update()
        {

            // ------------------------------ HeavenlyStrike : 0
            if (_avatarSkillCore.HeavenlyStrikeRemainCD() > 0)
            {
                heavenlyStrikeButton.enabled = false;

                int coolDown = (int)_avatarSkillCore.HeavenlyStrikeRemainCD();
                float coolDown_fillAmount = _avatarSkillCore.HeavenlyStrikeRemainCD() / _avatarSkillCore.HeavenlyStrikeCooldown();

                Timer_CoolDown(0, coolDown, coolDown_fillAmount);
            }
            else
            {
                if (!heavenlyStrikeButton.enabled)
                {
                    Timer_Reset(0);
                    heavenlyStrikeButton.enabled = true;
                }
            }


            // ------------------------------ ShadowClone : 1
            //			if(!_avatarSkillCore.IsShadowCloneTriggerByFairy)
            //			{
            if (_avatarSkillCore.ShadowCloneRemainCD() > 0)
            {
                shadowCloneButton.enabled = false;

                if (_avatarSkillCore.ShadowCloneRemainDuration() > 0)
                {
                    int duration = (int)_avatarSkillCore.ShadowCloneRemainDuration();
                    float duration_fillAmount = _avatarSkillCore.ShadowCloneRemainDuration() / _avatarSkillCore.ShadowCloneDuration();

                    Timer_Duration(1, duration, duration_fillAmount);
                }
                else
                {
                    if (avatar.isUsingShadowClone)
                        avatar.ShadowCloneEnable(false);

                    int coolDown = (int)_avatarSkillCore.ShadowCloneRemainCD();
                    float coolDown_fillAmount = _avatarSkillCore.ShadowCloneRemainCD() / _avatarSkillCore.ShadowCloneCooldown();

                    Timer_CoolDown(1, coolDown, coolDown_fillAmount);
                }
            }
            else
            {
                if (!shadowCloneButton.enabled)
                {
                    Timer_Reset(1);
                    shadowCloneButton.enabled = true;

                    // Confirm off "Shadow Clone"
                    avatar.ShadowCloneEnable(false);
                }
            }
            //			}
            //			else
            //			{
            //				if(_avatarSkillCore.ShadowCloneRemainCD() > 0)
            //				{
            //					int coolDown = (int) _avatarSkillCore.ShadowCloneRemainCD();
            //					float coolDown_fillAmount = _avatarSkillCore.ShadowCloneRemainCD() / _avatarSkillCore.ShadowCloneCooldown();
            //					
            //					timer_Text[1].gameObject.SetActive(true);
            //					timer_Text[1] = String.Format("{0:0}:{1:00}", Mathf.Floor( coolDown / 60), coolDown % 60 );
            //					
            //					timer_RemainDuration[1].gameObject.SetActive(false);
            //					
            //					timer_RemainCD[1].gameObject.SetActive(true);
            //					timer_RemainCD[1].fillAmount = coolDown_fillAmount;
            //
            //				}
            //
            //				if(_avatarSkillCore.ShadowCloneRemainDuration() > 0)
            //				{
            //					shadowCloneButton.enabled = false;
            //
            //					int duration = (int)_avatarSkillCore.ShadowCloneRemainDuration();
            //					float duration_fillAmount = _avatarSkillCore.ShadowCloneRemainDuration() / _avatarSkillCore.ShadowCloneDuration();
            //					
            //					timer_Text[1].gameObject.SetActive(true);
            //					timer_Text[1] = String.Format("{0:0}:{1:00}", Mathf.Floor( duration / 60), duration % 60 );
            //					
            //					timer_RemainDuration[1].gameObject.SetActive(true);
            //					timer_RemainDuration[1].fillAmount = duration_fillAmount;
            //				}
            //				else
            //				{
            //					if(avatar.isUsingShadowClone)
            //						avatar.ShadowCloneEnable(false);
            //
            //					shadowCloneButton.enabled = true;
            //				}
            //			}

            // ------------------------------ CriticalStrike : 2
            if (_avatarSkillCore.CriticalStrikeRemainCD() > 0)
            {
                criticalStrikeButton.enabled = false;

                if (_avatarSkillCore.CriticalStrikeRemainDuration() > 0)
                {
                    int duration = (int)_avatarSkillCore.CriticalStrikeRemainDuration();
                    float duration_fillAmount = _avatarSkillCore.CriticalStrikeRemainDuration() / _avatarSkillCore.CriticalStrikeDuration();

                    Timer_Duration(2, duration, duration_fillAmount);
                }
                else
                {
                    if (_avatarCore.CriticalStrikeASEnabled)
                    {
                        _avatarCore.CriticalStrikeASEnabled = false;
                        avatar.ShowCriticalStrikeEffect(false);
                    }

                    int coolDown = (int)_avatarSkillCore.CriticalStrikeRemainCD();
                    float coolDown_fillAmount = _avatarSkillCore.CriticalStrikeRemainCD() / _avatarSkillCore.CriticalStrikeCooldown();

                    Timer_CoolDown(2, coolDown, coolDown_fillAmount);
                }
            }
            else
            {
                if (!criticalStrikeButton.enabled)
                {
                    Timer_Reset(2);
                    criticalStrikeButton.enabled = true;
                }
            }

            // ------------------------------ WarCry : 3
            if (_avatarSkillCore.WarCryRemainCD() > 0)
            {
                warCryButton.enabled = false;

                if (_avatarSkillCore.WarCryRemainDuration() > 0)
                {
                    int duration = (int)_avatarSkillCore.WarCryRemainDuration();
                    float duration_fillAmount = _avatarSkillCore.WarCryRemainDuration() / _avatarSkillCore.WarCryDuration();

                    Timer_Duration(3, duration, duration_fillAmount);
                }
                else
                {
                    if (_heroCore.BoostSpeed != 1)
                    {
                        _heroCore.UpdateBoostSpeed(1);
                    }

                    int coolDown = (int)_avatarSkillCore.WarCryRemainCD();
                    float coolDown_fillAmount = _avatarSkillCore.WarCryRemainCD() / _avatarSkillCore.WarCryCooldown();

                    Timer_CoolDown(3, coolDown, coolDown_fillAmount);
                }
            }
            else
            {
                if (!warCryButton.enabled)
                {
                    Timer_Reset(3);
                    warCryButton.enabled = true;
                }
            }

            // ------------------------------ BerserkerRage : 4
            if (_avatarSkillCore.BerserkerRageRemainCD() > 0)
            {
                berserkerRageButton.enabled = false;

                if (_avatarSkillCore.BerserkerRageRemainDuration() > 0)
                {
                    int duration = (int)_avatarSkillCore.BerserkerRageRemainDuration();
                    float duration_fillAmount = _avatarSkillCore.BerserkerRageRemainDuration() / _avatarSkillCore.BerserkerRageDuration();

                    Timer_Duration(4, duration, duration_fillAmount);
                }
                else
                {
                    if (_avatarCore.BerserkerRageEnabled)
                    {
                        _avatarCore.BerserkerRageEnabled = false;
                        avatar.ShowBerserkerRageEffect(false);
                    }

                    int coolDown = (int)_avatarSkillCore.BerserkerRageRemainCD();
                    float coolDown_fillAmount = _avatarSkillCore.BerserkerRageRemainCD() / _avatarSkillCore.BerserkerRageCooldown();

                    Timer_CoolDown(4, coolDown, coolDown_fillAmount);
                }
            }
            else
            {
                if (!berserkerRageButton.enabled)
                {
                    Timer_Reset(4);
                    berserkerRageButton.enabled = true;
                }
            }

            // ------------------------------ HandOfMidas : 5
            if (_avatarSkillCore.HandOfMidasRemainCD() > 0)
            {
                handOfMidasButton.enabled = false;

                if (_avatarSkillCore.HandOfMidasRemainDuration() > 0)
                {
                    int duration = (int)_avatarSkillCore.HandOfMidasRemainDuration();
                    float duration_fillAmount = _avatarSkillCore.HandOfMidasRemainDuration() / _avatarSkillCore.HandOfMidasDuration();

                    Timer_Duration(5, duration, duration_fillAmount);
                }
                else
                {
                    if (avatar.isUsingHandOfMidas)
                        avatar.HandOfMidasEnable(false);

                    int coolDown = (int)_avatarSkillCore.HandOfMidasRemainCD();
                    float coolDown_fillAmount = _avatarSkillCore.HandOfMidasRemainCD() / _avatarSkillCore.HandOfMidasCooldown();

                    Timer_CoolDown(5, coolDown, coolDown_fillAmount);
                }
            }
            else
            {
                if (!handOfMidasButton.enabled)
                {
                    Timer_Reset(5);
                    handOfMidasButton.enabled = true;
                }
            }
        }

        //Camera shake effect for heavenly strike
        void CameraShake()
        {
            if (shakeAmt > 0)
            {
                float quakeAmt = UnityEngine.Random.value * shakeAmt * 2 - shakeAmt;
                Vector3 pp = mainCamera.transform.position;
                pp.y += quakeAmt; // can also add to x and/or z
                mainCamera.transform.position = pp;
            }
        }

        void StopShaking()
        {
            CancelInvoke("CameraShake");
            mainCamera.transform.position = originalCameraPosition;
        }

        private int HeavenlyStrikeHit = 1;

        private IEnumerator HeavenlyStrike()
        {
            yield return new WaitForSeconds(1.5f);

            shakeAmt = 0.3f;
            InvokeRepeating("CameraShake", 0, .01f);
            Invoke("StopShaking", 0.3f);
            gameManager.enemy.TakeDamage(_avatarCore.HeavenlyStrikeDamage, true);
        }

        private IEnumerator HeavenlyStrikeRun()
        {
            avatar.avatarAnimator.Play("pl_sp01", 0, 0);
            StartCoroutine(HeavenlyStrike());

            HeavenlyStrikeHit--;

            if (HeavenlyStrikeHit > 0)
            {
                yield return new WaitForSeconds(1.65f);

                StartCoroutine(HeavenlyStrikeRun());
            }
            else
            {
                yield return true;
            }
        }

        public void HeavenlyStrikeButtonPressed()
        {
            if (_avatarSkillCore.ActivateHeavenlyStrike())
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.trigger);

                HeavenlyStrikeHit = 1 + (int) (ArtifactAssets.MoeSpirit098.EffectValue * 100);

                StartCoroutine(HeavenlyStrikeRun());

                if (!activeSkillTutorialCompleted)
                {
                    if (heavenlyStrikeButton.gameObject.GetComponent<TutorialReceiver>().isTriggered)
                    {
                        TutorialManager.Instance.UpdateTutorialProgress(6);
                    }
                }
            }
        }

        public void ShadowCloneButtonPressed()
        {
            if (_avatarSkillCore.ActivateShadowClone(false))
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.trigger);
                avatar.ShadowCloneEnable(true);

                if (!activeSkillTutorialCompleted)
                {
                    if (shadowCloneButton.gameObject.GetComponent<TutorialReceiver>().isTriggered)
                    {
                        TutorialManager.Instance.UpdateTutorialProgress(6);
                    }
                }
            }
        }

        //		public void ShadowCloneTriggleByFairy(){
        //			if(_avatarSkillCore.ActivateShadowClone(true))
        //			{
        //				avatar.ShadowCloneEnable(true);
        //			}
        //		}

        public void CriticalStrikeButtonPressed()
        {
            if (_avatarSkillCore.ActivateCriticalStrike())
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.trigger);
                _avatarCore.CriticalStrikeASEnabled = true;
                avatar.ShowCriticalStrikeEffect(true);

                if (!activeSkillTutorialCompleted)
                {
                    if (criticalStrikeButton.gameObject.GetComponent<TutorialReceiver>().isTriggered)
                    {
                        TutorialManager.Instance.UpdateTutorialProgress(6);
                    }
                }
            }
        }

        public void WarCryButtonPressed()
        {
            if (_avatarSkillCore.ActivateWarCry())
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.trigger);
                double warCryIncrease = DamageFormulas.ActiveSkillWarCryIncreaseForSkillLevel(AOMAssets.WarCryASVG.GetBalance());
                _heroCore.UpdateBoostSpeed(warCryIncrease);

                if (!activeSkillTutorialCompleted)
                {
                    if (warCryButton.gameObject.GetComponent<TutorialReceiver>().isTriggered)
                    {
                        TutorialManager.Instance.UpdateTutorialProgress(6);
                    }
                }
            }
        }

        public void BerserkerRageButtonPressed()
        {
            if (_avatarSkillCore.ActivateBerserkerRage())
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.trigger);
                _avatarCore.BerserkerRageEnabled = true;
                avatar.ShowBerserkerRageEffect(true);

                if (!activeSkillTutorialCompleted)
                {
                    if (berserkerRageButton.gameObject.GetComponent<TutorialReceiver>().isTriggered)
                    {
                        TutorialManager.Instance.UpdateTutorialProgress(6);
                    }
                }
            }
        }

        public void HandOfMidasButtonPressed()
        {
            if (_avatarSkillCore.ActivateHandOfMidas())
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.trigger);
                avatar.HandOfMidasEnable(true);

                if (!activeSkillTutorialCompleted)
                {
                    if (handOfMidasButton.gameObject.GetComponent<TutorialReceiver>().isTriggered)
                    {
                        TutorialManager.Instance.UpdateTutorialProgress(6);
                    }
                }
            }
        }
    }
}
