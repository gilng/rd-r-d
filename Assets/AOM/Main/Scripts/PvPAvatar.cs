﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using Zenject;

namespace Ignite.AOM
{
    public class PvPAvatar : MonoBehaviour
    {
        private AvatarCore _avatarCore;
        private GoldDropCore _goldDropCore;

        [Inject]
        public void ConstructorSafeAttribute(AvatarCore avatarCore, GoldDropCore goldDropCore)
        {
            _avatarCore = avatarCore;
            _goldDropCore = goldDropCore;
        }

        public static PvPAvatar instance;
        public Animator pvpAvatarAnimator;
        private bool isAtt;

        public ObjectPool[] attackEffectPools;

        void Awake()
        {
            instance = this;
            pvpAvatarAnimator = (Animator)GetComponent("Animator");
        }

        void Update()
        {
            if (isAtt)
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.AvatarAttack_1);
                pvpAvatarAnimator.SetTrigger("atk");
                pvpAvatarAnimator.SetInteger("atk_cnt", (pvpAvatarAnimator.GetInteger("atk_cnt") == 2 ? 0 : pvpAvatarAnimator.GetInteger("atk_cnt") + 1));

                isAtt = false;
            }
        }

        public void AttackEnemy()
        {
            if (PvPSceneManager.Instance.BattleStarted)
            {
                isAtt = true;

                if (PvPSceneManager.Instance.bossCharacter != null)
                {
                    PvPSceneManager.Instance.bossCharacter.TouchAnimation();
                }

                EventManager.ChallengeScoreBalanceUpdated();
            }
        }
    }
}
