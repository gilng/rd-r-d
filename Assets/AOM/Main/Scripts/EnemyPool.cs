using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Zenject;
using Random = UnityEngine.Random;

namespace Ignite.AOM
{
    public class EnemyPool : MonoBehaviour
    {
        private EffectController.Factory _effectFactory;

        private Enemy.Factory _enemyFactory;

        private EnemyCore _enemyCore;

        private GalleryCore _galleryCore;

        [Inject]
        public void ConstructorSafeAttribute(EffectController.Factory effectFactory, Enemy.Factory enemyFactory, EnemyCore enemyCore, GalleryCore galleryCore)
        {
            _effectFactory = effectFactory;
            _enemyFactory = enemyFactory;
            _enemyCore = enemyCore;
            _galleryCore = galleryCore;
        }

        [System.Serializable]
        public class Boss
        {
            public string bossId;
            public GameObject boss;
            public int[] breakLevel;
        }

        public List<Boss> bosses;

        [System.Serializable]
        public class MiniBoss
        {
            public string miniBossId;
            public GameObject miniBoss;
        }

        public List<MiniBoss> miniBosses;

        int totalEnemies = 32;
        public GameObject currentEnemy;

        int currentEnemyIndex = 1;

        public Vector3 bossScale, miniBossScale, treasureChestScale, enemyScale;

        public GameObject moetanDead_effect;
        public GameObject[] enemyDead_effects;

        public void InitDeadEffect()
        {
            SoulAnimationEffect soulAnimationEffect = _effectFactory.Create("Monster/Prefab/Boss/soul_effect") as SoulAnimationEffect;

            moetanDead_effect = soulAnimationEffect.gameObject;

            if (moetanDead_effect != null)
            {
                moetanDead_effect.transform.SetParent(this.transform, false);
                soulAnimationEffect.EndEffect();

                enemyDead_effects = new GameObject[3];

                for (int i = 0; i < enemyDead_effects.Length; i++)
                {
                    EffectController enemyExplosionEffectController = _effectFactory.Create("Monster/Prefab/Enemy/enemy_explosion02");
                    enemyDead_effects[i] = enemyExplosionEffectController.gameObject;
                    enemyDead_effects[i].transform.SetParent(this.transform, false);
                    enemyDead_effects[i].SetActive(false);
                }
            }
        }

        public void LoadMoetanById(string bossId, bool isGallary, Action successCallback)
        {
            int index = 0;

            for (int i = 0; 0 < bosses.Count; i++)
            {
                if (bosses[i].bossId == bossId)
                {
                    index = i;
                    break;
                }
            }

            Enemy moetan = _enemyFactory.Create(null);
            bosses[index].boss = moetan.gameObject;

            if (bosses[index].boss != null)
            {
                bosses[index].boss.name = bosses[index].bossId;
                bosses[index].boss.transform.SetParent(this.transform, false);

                if (!isGallary)
                {
                    bosses[index].boss.SetActive(false);

                    if (bosses[index].boss.transform.FindChild("EnemyBody").gameObject.GetComponent<PolygonCollider2D>() != null)
                        bosses[index].boss.transform.FindChild("EnemyBody").gameObject.GetComponent<PolygonCollider2D>().enabled = false;
                }

                currentEnemy = bosses[index].boss;

                successCallback();
            }

//            Action loadedCallback = () =>
//            {
//                Enemy moetan = _enemyFactory.Create(AssetBundleLoader.LoadedAssetObj);
//                bosses[index].boss = moetan.gameObject;
//
//                if (bosses[index].boss != null)
//                {
//                    bosses[index].boss.name = bosses[index].bossId;
//                    bosses[index].boss.transform.SetParent(this.transform, false);
//
//                    if (!isGallary)
//                    {
//                        bosses[index].boss.SetActive(false);
//
//                        if (bosses[index].boss.transform.FindChild("EnemyBody").gameObject.GetComponent<PolygonCollider2D>() != null)
//                            bosses[index].boss.transform.FindChild("EnemyBody").gameObject.GetComponent<PolygonCollider2D>().enabled = false;
//                    }
//
//                    currentEnemy = bosses[index].boss;
//
//                    successCallback();
//                }
//            };
//
//            AssetBundleLoader.Instance.LoadAsset(AOMAssetBundlesConstant.MoetanBundleNameById(int.Parse(bossId.Replace("Boss_", ""))), bossId, loadedCallback);
        }

        public void RemoveBossById(string bossId)
        {
            for (int i = 0; i < bosses.Count; i++)
            {
                if (bosses[i].bossId == bossId)
                {
                    if (bosses[i].boss != null)
                    {
                        AssetBundleLoader.Instance.RemoveLoadedAsset();

                        Destroy(bosses[i].boss);
                        bosses[i].boss = null;

                        currentEnemy = null;

                        break;
                    }
                }
            }
        }

        public void RemoveBossByIndex(int index)
        {
            if (bosses[index].boss != null)
            {
                AssetBundleLoader.Instance.RemoveLoadedAsset();

                Destroy(bosses[index].boss);
                bosses[index].boss = null;

                currentEnemy = null;
            }
        }

        public void LoadMiniMoetanByIndex(int index, Action successCallback)
        {
            Action loadedCallback = () =>
            {
                Enemy miniMoetan = _enemyFactory.Create(AssetBundleLoader.LoadedAssetObj);
                miniBosses[index].miniBoss = AssetBundleLoader.LoadedAssetObj;

                if (miniBosses[index].miniBoss != null)
                {
                    miniBosses[index].miniBoss.name = miniBosses[index].miniBoss.name.Replace("(Clone)", "");
                    miniBosses[index].miniBoss.transform.SetParent(this.transform, false);
                    miniBosses[index].miniBoss.SetActive(false);

                    currentEnemy = miniBosses[index].miniBoss;

                    successCallback();
                }
            };

            AssetBundleLoader.Instance.LoadAsset("mini_moetan-bundle", "Mt_" + (index + 1).ToString("000"), loadedCallback);
        }

        public void RemoveMiniBossById(string miniBossId)
        {
            // Clear Mini Boss Pool
            for (int i = 0; i < miniBosses.Count; i++)
            {
                if (miniBosses[i].miniBoss != null)
                {
                    if (miniBosses[i].miniBoss.name == miniBossId)
                    {
                        AssetBundleLoader.Instance.RemoveLoadedAsset();

                        Destroy(miniBosses[i].miniBoss);
                        miniBosses[i].miniBoss = null;

                        currentEnemy = null;

                        break;
                    }
                }
            }
        }

        public void LoadEnemyByIndex(int index, Action successCallback)
        {
            Action loadedCallback = () =>
            {
                Enemy enemy = _enemyFactory.Create(AssetBundleLoader.LoadedAssetObj);

                if(enemy != null)
                    currentEnemy = enemy.gameObject;

                if (currentEnemy != null)
                {
                    currentEnemy.name = currentEnemy.name.Replace("(Clone)", "");
                    currentEnemy.transform.SetParent(this.transform, false);
                    currentEnemy.SetActive(false);

                    successCallback();
                }
            };

            string enemyBundleName = AOMAssetBundlesConstant.EnemyBundleNameById(index);
            AssetBundleLoader.Instance.LoadAsset(enemyBundleName, "Enemy_" + index.ToString("000"), loadedCallback);
        }

        public void RemoveEnemy()
        {
            AssetBundleLoader.Instance.RemoveLoadedAsset();

            Destroy(currentEnemy);

            currentEnemy = null;
        }

        public Boss GetNextMoetan(bool moetanDefeated)
        {
            Boss bossInfo = null;

            int nextMoetanStage = moetanDefeated ? _enemyCore.Stage + 1 : _enemyCore.Stage;

            if (nextMoetanStage % 5 != 0)
            {
                do
                {
                    nextMoetanStage++;
                } while (nextMoetanStage % 5 != 0 && nextMoetanStage < _enemyCore.MaxStage);
            }

            bool fixedBossFound = false;

            for (int i = 0; i < bosses.Count; i++)
            {
                for (int j = 0; j < bosses[i].breakLevel.Length; j++)
                {
                    if (nextMoetanStage == bosses[i].breakLevel[j])
                    {
                        fixedBossFound = true;

                        bossInfo = bosses[i];

                        break;
                    }
                }

                if (fixedBossFound)
                    break;
            }

            if (!fixedBossFound)
            {
                List<Boss> neverShowBoss = new List<Boss>();

                if (nextMoetanStage >= _enemyCore.MaxStage)
                {
                    for (int i = 0; i < bosses.Count; i++)
                    {
                        Gallery bossGallery = _galleryCore.GetGirlWithID(bosses[i].bossId.Replace("Boss_", ""));

                        if (bossGallery != null)
                        {
                            if (bossGallery.Status == MonsterGirlStatus.NeverShow)
                            {
                                neverShowBoss.Add(bosses[i]);
                            }
                        }
                    }
                }

                if (neverShowBoss.Count == 0)
                {
                    bool isSameBG = false;

                    do
                    {
                        int index = UnityEngine.Random.Range(0, bosses.Count);
                        bossInfo = bosses[index];

                        isSameBG = _galleryCore.GetGirlWithID(bossInfo.bossId.Replace("Boss_", "")).bgId == StageManager.Instance.bgId;

                    } while (bossInfo.breakLevel[0] >= nextMoetanStage || isSameBG);
                }
                else
                {
                    bossInfo = neverShowBoss[0];
                }
            }

            return bossInfo;
        }

        public void GetEnemyWithType(EnemyType type, Action getEnemyCallback)
        {
            if (_enemyCore.CurrentEnemyType == EnemyType.BigBoss && !_enemyCore.IsInfinityLoop)
            {
                GameObject boss = null;

                int currentStage = _enemyCore.Stage;

                int index = Random.Range(0, 2);

                LoadMoetanById(bosses[index].bossId, false, () =>
                {
                    boss = bosses[index].boss;
                    boss.GetComponent<Enemy>().breakLevel = 0;

                    currentEnemy = boss;

                    getEnemyCallback();
                });

//                int currentStage = _enemyCore.Stage;
//
//                bool fixedBossFound = false;
//
//                for (int i = 0; i < bosses.Count; i++)
//                {
//                    if (bosses[i].bossId == StageManager.Instance.nextMoetanId)
//                    {
//                        for (int j = 0; j < bosses[i].breakLevel.Length; j++)
//                        {
//                            if (currentStage == bosses[i].breakLevel[j])
//                            {
//                                fixedBossFound = true;
//
//                                LoadMoetanById(bosses[i].bossId, false, () =>
//                                {
//                                    boss = bosses[i].boss;
//                                    boss.GetComponent<Enemy>().breakLevel = j;
//
//                                    currentEnemy = boss;
//
//                                    getEnemyCallback();
//                                });
//
//                                break;
//                            }
//                        }
//
//                        if (!fixedBossFound)
//                        {
//                            for (int k = bosses[i].breakLevel.Length - 1; k >= 0; k--)
//                            {
//                                if (bosses[i].breakLevel[k] <= currentStage)
//                                {
//                                    LoadMoetanById(bosses[i].bossId, false, () =>
//                                    {
//                                        boss = bosses[i].boss;
//                                        boss.GetComponent<Enemy>().breakLevel = k;
//
//                                        currentEnemy = boss;
//
//                                        getEnemyCallback();
//                                    });
//
//                                    break;
//                                }
//                            }
//                        }
//
//                        break;
//                    }
//                }

                transform.localScale = bossScale;
            }
            else if (_enemyCore.CurrentEnemyType == EnemyType.MiniBoss && !_enemyCore.IsInfinityLoop)
            {
                int index = UnityEngine.Random.Range(0, miniBosses.Count);

                LoadMiniMoetanByIndex(index, () =>
                            {
                                GameObject temp_miniBoss = miniBosses[index].miniBoss;
                                transform.localScale = miniBossScale;

                                currentEnemy = temp_miniBoss;

                                getEnemyCallback();
                            });
            }
            else if (_enemyCore.CurrentEnemyType == EnemyType.TreasureChest)
            {
                LoadEnemyByIndex(0, () =>
                    {
                        transform.localScale = treasureChestScale;

                        getEnemyCallback();
                    });
            }
            else //Normal
            {
                int index = 1;

                do
                {
                    index = UnityEngine.Random.Range(1, totalEnemies);
                } while (index == currentEnemyIndex);

                LoadEnemyByIndex(index, () =>
                {
                    currentEnemyIndex = index;
                    transform.localScale = enemyScale;

                    getEnemyCallback();
                });
            }
        }

        public GameObject GetBossCharacterByIndex(int index)
        {
            return bosses[index].boss;
        }

        public void KeyUnlock(string keyID)
        {
            if (string.Equals(keyID, AOMAssets.AnotherWorldKeyGIVG.ItemId))
            {
                // Add new Moetan & mini Moetan
                AddNewEnemyToList(EnemyType.BigBoss, 11);
                AddNewEnemyToList(EnemyType.BigBoss, 12);
                AddNewEnemyToList(EnemyType.BigBoss, 13);

                AddNewEnemyToList(EnemyType.MiniBoss, 11);
                AddNewEnemyToList(EnemyType.MiniBoss, 12);
                AddNewEnemyToList(EnemyType.MiniBoss, 13);

                _enemyCore.IncreaseMaxStage(_enemyCore.MaxStage + 100);
            }

            if (string.Equals(keyID, AOMAssets.SchoolKeyGIVG.ItemId))
            {
                // Add new Moetan & mini Moetan
                AddNewEnemyToList(EnemyType.BigBoss, 14);
                AddNewEnemyToList(EnemyType.BigBoss, 15);
                AddNewEnemyToList(EnemyType.BigBoss, 16);
                AddNewEnemyToList(EnemyType.BigBoss, 17);
                AddNewEnemyToList(EnemyType.BigBoss, 18);

                _enemyCore.IncreaseMaxStage(_enemyCore.MaxStage + 100);
            }

            if (string.Equals(keyID, AOMAssets.ForestKeyGIVG.ItemId))
            {
                // Add new Moetan & mini Moetan
                AddNewEnemyToList(EnemyType.BigBoss, 19);
                AddNewEnemyToList(EnemyType.BigBoss, 20);
                AddNewEnemyToList(EnemyType.BigBoss, 21);

                _enemyCore.IncreaseMaxStage(_enemyCore.MaxStage + 100);
            }

            if (string.Equals(keyID, AOMAssets.SummerKeyGIVG.ItemId))
            {
                // Add new Moetan & mini Moetan
                AddNewEnemyToList(EnemyType.BigBoss, 22);
                AddNewEnemyToList(EnemyType.BigBoss, 23);
                AddNewEnemyToList(EnemyType.BigBoss, 24);
            }

            if (string.Equals(keyID, AOMAssets.HalloweenKeyGIVG.ItemId))
            {
                // Add new Moetan & mini Moetan
                AddNewEnemyToList(EnemyType.BigBoss, 25);
                AddNewEnemyToList(EnemyType.BigBoss, 26);
                AddNewEnemyToList(EnemyType.BigBoss, 27);
            }

            if (string.Equals(keyID, AOMAssets.FukubukuroKeyGIVG.ItemId))
            {
                // Add new Moetan & mini Moetan
                AddNewEnemyToList(EnemyType.BigBoss, 28);
                AddNewEnemyToList(EnemyType.BigBoss, 29);
                AddNewEnemyToList(EnemyType.BigBoss, 30);
            }

            if (string.Equals(keyID, AOMAssets.PvPKey001GIVG.ItemId))
            {
                // Add new Moetan & mini Moetan
                AddNewEnemyToList(EnemyType.BigBoss, 97, false);
            }

            if (string.Equals(keyID, AOMAssets.PvPKey002GIVG.ItemId))
            {
                // Add new Moetan & mini Moetan
                AddNewEnemyToList(EnemyType.BigBoss, 98, false);
            }

            EventManager.QuickReloadTebleRow();
        }

        void AddNewEnemyToList(EnemyType type, int id, bool addToBossList = true)
        {
            if (type == EnemyType.BigBoss)
            {
                for (int i = 0; i < bosses.Count; i++)
                {
                    if ("Boss_" + (id).ToString("000") == bosses[i].bossId)
                        return;
                }

                Boss b = new Boss();

                //int listId = id

                /*for (int i = 0; i < GalleryCore.PvPBossId.Length; i++)
                {
                    if (listId == GalleryCore.PvPBossId[i])
                        listId = GalleryCore.Girls.Count - i;
                }*/

                int index = 0;

                for (int i = 0; i < GalleryCore.Girls.Count; i++)
                {
                    if (id.ToString("000") == GalleryCore.Girls[i].girlId)
                        index = i;
                }

                GalleryCore.Girls[index].BreakLevel = AddBreakLevels(id);

				b.breakLevel = AddBreakLevels(id);

                b.bossId = "Boss_" + (id).ToString("000");

                if (_enemyCore.Stage > b.breakLevel[0])
                {
                    Gallery bossGallery = _galleryCore.GetGirlWithID((id).ToString("000"));

                    if (bossGallery != null)
                    {
                        if (bossGallery.Status != MonsterGirlStatus.Killed)
                        {
                            bossGallery.Status = MonsterGirlStatus.Killed;
                            bossGallery.ReachedBreakLevel++;
                        }
                    }
                }

                if (addToBossList)
                    bosses.Add(b);
            }
            else if (type == EnemyType.MiniBoss)
            {
                for (int i = 0; i < miniBosses.Count; i++)
                {
                    if ("MiniBoss_" + (id).ToString("000") == miniBosses[i].miniBossId)
                        return;
                }

                MiniBoss mb = new MiniBoss();

                mb.miniBossId = "MiniBoss_" + (id).ToString("000");

                miniBosses.Add(mb);
            }
            else if (type == EnemyType.Normal)
            {
                if (id > totalEnemies - 1)
                    totalEnemies++;
            }
        }

        public int[] AddBreakLevels(int id)
        {
            int[] breakLevel = new int[3];

            switch (id)
            {
                // Another world
                case 11:
                    //breakLevel = new int[] { 480, 580, 680 };
                    breakLevel = new int[] { -1, 580, 680 };
                    break;
                case 12:
                    //breakLevel = new int[] { 540, 640, 740 };
                    breakLevel = new int[] { -1, 640, 740 };
                    break;
                case 13:
                    //breakLevel = new int[] { 600, 700, 800 };
                    breakLevel = new int[] { -1, 700, 800 };
                    break;

                // School
                case 14:
                    //breakLevel = new int[] { 610, 670, 780 };
                    breakLevel = new int[] { -1, 670, 780 };
                    break;
                case 15:
                    //breakLevel = new int[] { 650, 710, 820 };
                    breakLevel = new int[] { -1, 710, 820 };
                    break;
                case 16:
                    //breakLevel = new int[] { 690, 750, 860 };
                    breakLevel = new int[] { -1, 750, 860 };
                    break;
                case 17:
                    //breakLevel = new int[] { 730, 790, 900 };
                    breakLevel = new int[] { -1, 790, 900 };
                    break;
                case 18:
                    //breakLevel = new int[] { 770, 830, 940 };
                    breakLevel = new int[] { -1, 830, 940 };
                    break;

                // Forest
                case 19:
                    //breakLevel = new int[] { 810, 870, 980 };
                    breakLevel = new int[] { -1, 870, 980 };
                    break;
                case 20:
                    //breakLevel = new int[] { 850, 910, 1020 };
                    breakLevel = new int[] { -1, 910, 1020 };
                    break;
                case 21:
                    //breakLevel = new int[] { 890, 950, 1060 };
                    breakLevel = new int[] { -1, 950, 1060 };
                    break;

                // Summer
                case 22:
                    //breakLevel = new int[] { 930, 990, 1080 };
                    breakLevel = new int[] { -1, 990, 1080 };
                    break;
                case 23:
                    //breakLevel = new int[] { 970, 1030, 1120 };
                    breakLevel = new int[] { -1, 1030, 1120 };
                    break;
                case 24:
                    //breakLevel = new int[] { 1010, 1070, 1160 };
                    breakLevel = new int[] { -1, 1070, 1160 };
                    break;

                // Halloween
                case 25:
                    //breakLevel = new int[] { 1050, 1110, 1200 };
                    breakLevel = new int[] { -1, 1110, 1200 };
                    break;
                case 26:
                    //breakLevel = new int[] { 1090, 1150, 1240 };
                    breakLevel = new int[] { -1, 1150, 1240 };
                    break;
                case 27:
                    //breakLevel = new int[] { 1130, 1190, 1280 };
                    breakLevel = new int[] { -1, 1190, 1280 };
                    break;

                // PvP
                case 97:
                    breakLevel = new int[] { -10, 0, 0 };
                    break;

                case 98:
                    breakLevel = new int[] { -10, 0, 0 };
                    break;

                // Fukubukuro
                case 28:
                    //breakLevel = new int[] { 1170, 1230, 1320 };
                    breakLevel = new int[] { -1, 1230, 1320 };
                    break;
                case 29:
                    //breakLevel = new int[] { 1210, 1270, 1360  };
                    breakLevel = new int[] { -1, 1270, 1360 };
                    break;
                case 30:
                    //breakLevel = new int[] { 1250, 1310, 1400 };
                    breakLevel = new int[] { -1, 1310, 1400 };
                    break;
            }

            return breakLevel;
        }

        //        public int[] GenerateBreakLevels()
        //        {
        //            int[] breakLevel = new int[3];
        //            int currNum = 0;
        //
        //            List<int> breakLevelList = new List<int>();
        //
        //            for (int i = 0; i < bosses.Count; i++)
        //            {
        //                if (bosses[i].breakLevel[0] != 0)
        //                    breakLevelList.Add(bosses[i].breakLevel[0]);
        //                breakLevelList.Add(bosses[i].breakLevel[1]);
        //                breakLevelList.Add(bosses[i].breakLevel[2]);
        //            }
        //
        //            breakLevelList.Sort();
        //
        //            int nextBossStage = _enemyCore.Stage / 10 * 10 + 10;
        //
        //            if (nextBossStage < 30 || nextBossStage >= _enemyCore.MaxStage)
        //                nextBossStage = 30;
        //
        //            while (currNum < 3)
        //            {
        //                bool repeatBreakLevel = false;
        //
        //                for (int i = 0; i < breakLevelList.Count; i++)
        //                {
        //                    if (nextBossStage == breakLevelList[i])
        //                    {
        //                        repeatBreakLevel = true;
        //                    }
        //                }
        //
        //                if (repeatBreakLevel == false)
        //                {
        //                    breakLevel[currNum] = nextBossStage;
        //                    nextBossStage += 100;
        //                    currNum++;
        //                }
        //                else
        //                {
        //                    nextBossStage += 10;
        //                }
        //            }
        //
        //            return breakLevel;
        //        }
    }
}
