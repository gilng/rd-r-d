﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class BreakScene : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        //		// Update is called once per frame
        //		void Update () {
        //		
        //		}

        public void BreakStep(int step)
        {
            if (GameManager.Instance != null)
            {
                if (GameManager.Instance.enemy.breakCharPosX.Count > 0 && GameManager.Instance.enemy.breakCharPosY.Count > 0)
                {
                    SoundManager.Instance.PlaySoundWithType(step != 3 ? SoundType.boss_Clothes_Ripped_Snap : SoundType.boss_Clothes_Ripped_Snap_Last);

                    GameManager.Instance.breakCharPosition.transform.localPosition = new Vector2(0 - (GameManager.Instance.enemy.breakCharPosX[step] * GameManager.Instance.breakCharPosition.transform.localScale.x), 0 - (GameManager.Instance.enemy.breakCharPosY[step] * GameManager.Instance.breakCharPosition.transform.localScale.y));
                }
            }
            else if(AssetEditorManager.instance != null)
            {
                if (AssetEditorManager.instance.enemy.breakCharPosX.Count > 0 && AssetEditorManager.instance.enemy.breakCharPosY.Count > 0)
                {
                    AssetEditorManager.instance.breakCharPosition.transform.localPosition = new Vector2(0 - (AssetEditorManager.instance.enemy.breakCharPosX[step] * AssetEditorManager.instance.breakCharPosition.transform.localScale.x), 0 - (AssetEditorManager.instance.enemy.breakCharPosY[step] * AssetEditorManager.instance.breakCharPosition.transform.localScale.y));
                }
            }
        }
    }
}
