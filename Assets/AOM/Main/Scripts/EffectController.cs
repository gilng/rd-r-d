﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Zenject;

namespace Ignite.AOM
{
    public class EffectController : MonoBehaviour
    {
        public class Factory : Factory<string, EffectController>
        {
        }
    }

    public class EffectControllerFactory : IFactory<string, EffectController>
    {
        private readonly DiContainer _container;

        public EffectControllerFactory(DiContainer container)
        {
            _container = container;
        }

        public EffectController Create(string prefabPath)
        {
            GameObject effect = _container.InstantiatePrefabResource(prefabPath);
            EffectController effectController = effect.GetComponent<EffectController>();

            return effectController;
        }
    }
}
