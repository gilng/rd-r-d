﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{

    public class CharacterLevelVG : LevelUpVG
    {

        public double baseCost;

        /// <summary>
        /// Initializes a new instance of the <see cref="Ignite.AOM.CharacterLevelVG"/> class.
        /// </summary>
        /// <param name="name">Name of the character.</param>
        /// <param name="description">Description.</param>
        /// <param name="itemId">Character ID.</param>
        /// <param name="minLevel">Minimum level of the character (e.g. 0 for heroes, 1 for Avatar).</param>
        /// <param name="baseCost">Base cost of the character.</param>
        public CharacterLevelVG(string itemId, int minLevel, double baseCost)
            : base(itemId, minLevel)
        {
            this.baseCost = baseCost;
            //            RefreshPrice(true);
        }

        public override double PriceForUpgradingToLevel(int lv)
        {
            // TODO Avatar should use different formula
            return Formulas.GoldToUpgradeCharacterToLevel(lv, baseCost, ArtifactAssets.Artifact024.EffectValue);
        }

        public override int Give(int amount, bool notify)
        {
            GamePlayStatManager.Instance.LevelUpHero(amount);
            int result = base.Give(amount, notify);
            if (AOMAssets.AvatarLevelVG.ItemId != ItemId)
            {
                EventManager.HeroLevelUp(ItemId, GetBalance());
            }

            return result;
        }
    }

}