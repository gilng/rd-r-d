using UnityEngine;
using System.Collections;

namespace Ignite.AOM {

    public abstract class AvatarActiveSkillVG : CharacterSkillVG
    {   
        private float cooldown;
        private float duration;
        
        public float Cooldown {
            get {
                return this.cooldown;
            }
        }
        
        public float Duration {
            get {
                return this.duration;
            }
        }

        public AvatarActiveSkillVG(string itemId, 
                                CharacterLevelVG character, int characterMinLevel, 
                                EffectType effectType, float cooldown, float duration)
            : base(itemId, character, characterMinLevel, effectType) {
            this.cooldown = cooldown;
            this.duration = duration;
        }
    }
}
