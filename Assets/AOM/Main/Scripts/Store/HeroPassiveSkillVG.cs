using UnityEngine;
using System.Collections;
using System;
namespace Ignite.AOM
{

    public class HeroPassiveSkillVG : CharacterSkillVG
    {
        private double effectValue;

        public override double EffectValue
        {
            get { return effectValue; }
        }

        public HeroPassiveSkillVG(string itemId, CharacterLevelVG character, int characterMinLevel, EffectType effectType, double effectValue)
            : base(itemId, character, characterMinLevel, effectType)
        {
            this.effectValue = effectValue;
            //			RefreshPrice(true);
        }

        public override double PriceForUpgradingToLevel(int lv)
        {
            // TODO Check with Nein on the Unlock cost
            if (Character == null)
            {
                return 1;
            }
            return Math.Floor(Character.baseCost * Math.Pow(1.075, CharacterMinLevel)) * 5;
        }

        public override int Give(int amount, bool notify)
        {
            int result = base.Give(amount, notify);
            EventManager.HeroUnlockSkill(Character.ItemId, ItemId);
            return result;
        }
    }
}
