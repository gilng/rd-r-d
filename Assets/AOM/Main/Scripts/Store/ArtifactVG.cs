using UnityEngine;
using System;
using System.Collections;
using Soomla.Store;

namespace Ignite.AOM
{
    public class ArtifactVG : AOMCachedBalanceVG
    {
        private int rareLevel;
        private int maxLevel;
        private double baseAllDamageValue;
        private double baseEffectValue;
        private float priceVectorX;
        private float priceVectorY;
        private int priceForOne = -1;
        private bool priceIsDirty = true;

        private bool canGacha = true;

        public double PriceForOne
        {
            get
            {
                if (priceIsDirty)
                {
                    RefreshPrice(true);
                    priceIsDirty = false;
                }
                return this.priceForOne;
            }
        }

        public int RareLevel
        {
            get { return this.rareLevel; }
        }

        public int MaxLevel
        {
            get { return this.maxLevel; }
        }

        public double BaseAllDamageValue
        {
            get { return this.baseAllDamageValue; }
        }

        public double AllDamageValue
        {
            get { return baseAllDamageValue * (GetBalance() + 1); }
        }

        public double BaseEffectValue
        {
            get
            {
                double _d = this.baseEffectValue;

                if (ItemId == "AR033" && GetBalance() == 0)
                {
                    _d += this.baseEffectValue;
                }

                return _d;
            }
        }

        public double EffectValue
        {
            get
            {
                double _d = baseEffectValue * GetBalance();

                if (ItemId == "AR004")
                {
                    _d *= (1 + ArtifactAssets.MoeSpirit016.EffectValue);
                }
                else if (ItemId == "AR024")
                {
                    _d *= (1 + ArtifactAssets.MoeSpirit018.EffectValue);
                }
                else if (ItemId == "AR033")
                {
                    _d += baseEffectValue;
                }

                return _d;
            }
        }

        public ArtifactVG(string itemId, int rareLevel, int maxLevel, double baseAllDamageValue, double baseEffectValue,
            float priceVectorX, float priceVectorY, bool canGacha = true)
            : base("", "", itemId, new PurchaseWithVirtualItem(AOMStoreConstant.RELIC_CURRENCY_ID, 0))
        {
            this.rareLevel = rareLevel;

            this.maxLevel = maxLevel;
            this.baseAllDamageValue = baseAllDamageValue;
            this.baseEffectValue = baseEffectValue;
            this.priceVectorX = priceVectorX;
            this.priceVectorY = priceVectorY;

            this.canGacha = canGacha;
            //RefreshPrice(true);
        }

        public override int ResetBalance(int balance, bool notify)
        {
            balance = Math.Min(balance, maxLevel);
            int result = base.ResetBalance(balance, notify);
            if (ArtifactAssets.Artifact024.ItemId.Equals(ItemId))
            {
                EventManager.UpgradeCostReduced();
            }
            if (notify)
            {
                EventManager.ArtifactVGBalanceChange(this, balance);
            }

            return result;
        }

        public override int Give(int amount, bool notify)
        {
            int balance = base.Give(amount, notify);
            if (balance > maxLevel)
            {
                balance = maxLevel;
                ResetBalance(maxLevel, false);
            }
            if (ArtifactAssets.Artifact024.ItemId.Equals(ItemId))
            {
                EventManager.UpgradeCostReduced();
            }

            if (notify)
            {
                EventManager.ArtifactVGBalanceChange(this, balance);
            }
            RefreshPrice(notify);
            return balance;
        }

        public override int Take(int amount, bool notify)
        {
            int balance = base.Take(amount, notify);

            if (notify)
            {
                EventManager.ArtifactVGBalanceChange(this, balance);
            }

            return balance;
        }

        protected int PriceForNextLevel()
        {
            return Formulas.GoldToUpgradeArtifactForLevel(GetBalance(), this.priceVectorX, this.priceVectorY);
        }

        public int SpentCostOnUpgrade()
        {
            int result = 0;
            int i = GetBalance() - 1;
            while (i > -1)
            {
                result += Formulas.GoldToUpgradeArtifactForLevel(i, this.priceVectorX, this.priceVectorY);
                i--;
            }
            return result;
        }

        public bool CanAffordOne()
        {
            bool canAffordOne = false;

            if (UserManager.Instance.CurrentUserSummary != null)
                canAffordOne = UserManager.Instance.CurrentUserSummary.Moecrystal >= PriceForOne && GetBalance() < this.MaxLevel;

            return canAffordOne;
        }

        protected void RefreshPrice(bool notify)
        {
            priceForOne = PriceForNextLevel();
            if (notify)
                EventManager.RelicPriceUpdated();
        }

        public void BuyOne()
        {
//            if (CanAffordOne())
//            {
                UserManager.Instance.TakeMoecrystals(priceForOne, () =>
                {
                    Give(1);
                });
//            }
        }

        public bool CanGacha()
        {
            return canGacha;
        }
    }
}