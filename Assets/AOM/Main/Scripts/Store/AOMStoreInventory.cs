﻿using UnityEngine;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;
using Soomla.Store;
using System;
using Zenject;

namespace Ignite.AOM
{
    public class AOMStoreInventory : StoreInventory
    {
        private static double currentGold = -1;
        private static int currentDiamond = -1;
        private static int currentRelic = -1;
//        private static int currentRuby = -1;

        // Gold
        public static void ResetGold(double amount)
        {
            currentGold = amount;
            EventManager.GoldBalanceUpdated();
        }

        public static void GiveGold(double amount)
        {
            currentGold = GetGoldBalance() + amount;
            GamePlayStatManager.Instance.CollectGold(GetGoldBalance());
            EventManager.GoldBalanceUpdated();
        }

        public static void SyncCloudGoldBalance(SaveData loadedSaveData)
        {
            if (loadedSaveData.golds > 0)
            {
                //Debug.Log("Sync Gold");
                PlayerPrefs.SetString(AOMStoreConstant.GOLD_CURRENCY_ID, loadedSaveData.golds.ToString());
            }

            string balance = PlayerPrefs.GetString(AOMStoreConstant.GOLD_CURRENCY_ID);

            if (balance == null || balance.Length < 1)
            {
                currentGold = 0;
            }
            else
            {
                double gold = double.Parse(PlayerPrefs.GetString(AOMStoreConstant.GOLD_CURRENCY_ID));
                if (gold < 0)
                {
                    gold = 0;
                    PlayerPrefs.SetString(AOMStoreConstant.GOLD_CURRENCY_ID, gold.ToString());
                }
                currentGold = gold;
            }
        }

        public static double GetGoldBalance()
        {
            if (currentGold == -1)
            {
                string balance = PlayerPrefs.GetString(AOMStoreConstant.GOLD_CURRENCY_ID);

                if (balance == null || balance.Length < 1)
                {
                    currentGold = 0;
                }
                else
                {
                    double gold = double.Parse(balance);

                    if (gold < 0)
                    {
                        gold = 0;
                        PlayerPrefs.SetString(AOMStoreConstant.GOLD_CURRENCY_ID, gold.ToString());
                    }

                    currentGold = gold;

                    return currentGold;
                }
            }

            return currentGold;
        }

        public static void TakeGold(double amount)
        {
            double gold = GetGoldBalance() - amount;
            currentGold = gold > 0 ? gold : 0;

            //EventManager.GoldBalanceUpdated ();
        }

        public static void SaveGoldValue()
        {
            if (currentGold != -1)
            {
                if (currentGold.ToString() != PlayerPrefs.GetString(AOMStoreConstant.GOLD_CURRENCY_ID, "0"))
                {
                    try
                    {
                        //Debug.LogError("Save Golds: " + currentGold);
                        PlayerPrefs.SetString(AOMStoreConstant.GOLD_CURRENCY_ID, currentGold.ToString());
                        PlayerPrefs.Save();
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Error: UserSaveGold_Fail");

                        // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                    Analytics.CustomEvent("UserSaveGold_Fail", new Dictionary<string, object>
                                          {
                        { "Try_Save_Gold", currentGold.ToString() }
                    });
#endif
                    }
                }
            }
            else
            {
                Debug.LogError("Error: currentGold = -1");
            }
        }

        // Diamond
        public static void GiveDiamond(int amount)
        {
            StoreInventory.GiveItem(AOMStoreConstant.DIAMOND_CURRENCY_ID, amount);
            UpdateDiamondBalance(amount);
        }

        public static void UpdateDiamondBalance(int amount)
        {
            currentDiamond = GetDiamondBalance() + amount;
            EventManager.DiamondBalanceUpdated();
        }

        public static void SyncCloudDiamondBalance(SaveData loadedSaveData)
        {
            if (loadedSaveData.diamonds > 0 && currentDiamond != loadedSaveData.diamonds)
            {
                //Debug.Log("Sync Diamond");
                ResetDiamond(loadedSaveData.diamonds);
            }

            currentDiamond = StoreInfo.GetItemByItemId(AOMStoreConstant.DIAMOND_CURRENCY_ID).GetBalance();
        }

        public static int GetDiamondBalance()
        {
            if (currentDiamond == -1)
            {
                currentDiamond = StoreInfo.GetItemByItemId(AOMStoreConstant.DIAMOND_CURRENCY_ID).GetBalance();
            }

            return currentDiamond;
        }

        public static void TakeDiamond(int amount)
        {
            currentDiamond = GetDiamondBalance() - amount;
            StoreInventory.TakeItem(AOMStoreConstant.DIAMOND_CURRENCY_ID, amount);
            //EventManager.DiamondBalanceUpdated ();
        }

        public static void ResetDiamond(int amount)
        {
            currentDiamond = amount;
            VirtualItem diamond = StoreInfo.GetItemByItemId(AOMStoreConstant.DIAMOND_CURRENCY_ID);
            diamond.ResetBalance(amount);
            EventManager.DiamondBalanceUpdated();
        }

        // Relic
        public static void GiveRelic(int amount)
        {
            currentRelic = GetRelicBalance() + amount;
            GamePlayStatManager.Instance.CollectRelic(GetRelicBalance());
            StoreInventory.GiveItem(AOMStoreConstant.RELIC_CURRENCY_ID, amount);
            EventManager.RelicBalanceUpdated();
        }

        public static void SyncCloudRelicBalance(SaveData loadedSaveData)
        {
            if (loadedSaveData.relics > 0 && currentRelic != loadedSaveData.relics)
            {
                //Debug.Log("Sync Relic");
                ResetRelic(loadedSaveData.relics);
            }

            currentRelic = StoreInfo.GetItemByItemId(AOMStoreConstant.RELIC_CURRENCY_ID).GetBalance();
        }

        public static int GetRelicBalance()
        {
            if (currentRelic == -1)
            {
                currentRelic = StoreInfo.GetItemByItemId(AOMStoreConstant.RELIC_CURRENCY_ID).GetBalance();
            }

            return currentRelic;
        }

        public static void TakeRelic(int amount)
        {
            currentRelic = GetRelicBalance() - amount;
            StoreInventory.TakeItem(AOMStoreConstant.RELIC_CURRENCY_ID, amount);
            //EventManager.RelicBalanceUpdated ();
        }

        public static void ResetRelic(int amount)
        {
            currentRelic = amount;
            VirtualItem relics = StoreInfo.GetItemByItemId(AOMStoreConstant.RELIC_CURRENCY_ID);
            relics.ResetBalance(amount);
            EventManager.RelicBalanceUpdated();
        }

//        // Ruby
//        public static void GiveRuby(int amount)
//        {
//            currentRuby = GetRubyBalance() + amount;
//            //GamePlayStatManager.Instance.CollectRuby(GetRubyBalance());
//            StoreInventory.GiveItem(AOMStoreConstant.RUBY_CURRENCY_ID, amount);
//            EventManager.RubyBalanceUpdated();
//        }
//
//        public static void SyncCloudRubyBalance(SaveData loadedSaveData)
//        {
//            if (loadedSaveData.rubys > 0)
//            {
//                //Debug.Log("Sync Ruby");
//                ResetRuby(loadedSaveData.rubys);
//            }
//
//            currentRuby = StoreInfo.GetItemByItemId(AOMStoreConstant.RUBY_CURRENCY_ID).GetBalance();
//        }
//
//        public static int GetRubyBalance()
//        {
//            if (currentRuby == -1)
//            {
//                currentRuby = StoreInfo.GetItemByItemId(AOMStoreConstant.RUBY_CURRENCY_ID).GetBalance();
//            }
//
//            return currentRuby;
//        }
//
//        public static void TakeRuby(int amount)
//        {
//            currentRuby = GetRubyBalance() - amount;
//            StoreInventory.TakeItem(AOMStoreConstant.RUBY_CURRENCY_ID, amount);
//            //EventManager.RubyBalanceUpdated ();
//        }
//
//        public static void ResetRuby(int amount)
//        {
//            currentRuby = amount;
//            VirtualItem rubys = StoreInfo.GetItemByItemId(AOMStoreConstant.RUBY_CURRENCY_ID);
//            rubys.ResetBalance(amount);
//            EventManager.RubyBalanceUpdated();
//        }
    }
}