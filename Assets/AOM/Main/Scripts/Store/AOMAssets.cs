using UnityEngine;
using System.Collections;

using Soomla.Store;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Ignite.AOM
{
    public class AOMAssets : IStoreAssets
    {
        private const string APP_ID = "game.ignite.aom";

        /// <summary>
        /// see parent.
        /// </summary>
        public int GetVersion()
        {
            // 2016-3-10 -- Kit
            return 20;
        }

        /** Virtual Currencies **/

        readonly static VirtualCurrency GOLD_CURRENCY = new VirtualCurrency(
            "Golds",                                      // name
            "",                                             // description
            AOMStoreConstant.GOLD_CURRENCY_ID                         // item id
        );
        readonly static VirtualCurrency RELIC_CURRENCY = new VirtualCurrency(
            "Relics",                                      // name
            "",                                             // description
            AOMStoreConstant.RELIC_CURRENCY_ID                         // item id
        );
        readonly static VirtualCurrency DIAMOND_CURRENCY = new VirtualCurrency(
            "Diamonds",                                      // name
            "",                                             // description
            AOMStoreConstant.DIAMOND_CURRENCY_ID                         // item id
        );
//        readonly static VirtualCurrency RUBY_CURRENCY = new VirtualCurrency(
//            "Rubys",                                      // name
//            "",                                             // description
//            AOMStoreConstant.RUBY_CURRENCY_ID                         // item id
//        );

        /// <summary>
        /// see parent.
        /// </summary>
        public VirtualCurrency[] GetCurrencies()
        {
            return new VirtualCurrency[] {
            GOLD_CURRENCY,
            RELIC_CURRENCY,
            DIAMOND_CURRENCY,
//            RUBY_CURRENCY,
            };
        }

        /** Virtual Currency Packs **/
        public readonly static VirtualCurrencyPack DIAMOND_25PACK = new VirtualCurrencyPack(
                "Free 25 Diamond",                                // Name
                "Free 25 Diamond",                                // Description
                "diamonds_25",                                         // Item id
                25,                                                    // Number of currency units in this pack
                AOMStoreConstant.DIAMOND_CURRENCY_ID,                   // Associated currency with this pack
                new PurchaseWithVirtualItem(DIAMOND_CURRENCY.ItemId, 0) // Purchase type
        );
        public readonly static VirtualCurrencyPack DIAMOND_180PACK = new VirtualCurrencyPack(
                "180 Diamonds & No Ads",                                // Name
                "180 Diamonds & No Ads",                                // Description
                "diamonds_180",                                         // Item id
                180,                                                    // Number of currency units in this pack
                AOMStoreConstant.DIAMOND_CURRENCY_ID,                   // Associated currency with this pack
                new PurchaseWithMarket(APP_ID + ".diamond_180pack", 2.99) // Purchase type
        );
        public readonly static VirtualCurrencyPack DIAMOND_500PACK = new VirtualCurrencyPack(
                "500 Diamonds +11% & No Ads",                             // Name
                "500 Diamonds +11% & No Ads",                        // Description
                "diamonds_500",                             // Item id
                500,                                            // Number of currency units in this pack
                AOMStoreConstant.DIAMOND_CURRENCY_ID,                           // Associated currency with this pack
                new PurchaseWithMarket(APP_ID + ".diamond_500pack", 5.99)                     // Purchase type
        );
        public readonly static VirtualCurrencyPack DIAMOND_1200PACK = new VirtualCurrencyPack(
                "1200 Diamonds +33% & No Ads",                             // Name
                "1200 Diamonds +33% & No Ads",                       // Description
                "diamonds_1200",                             // Item id
                1200,                                           // Number of currency units in this pack
                AOMStoreConstant.DIAMOND_CURRENCY_ID,                           // Associated currency with this pack
                new PurchaseWithMarket(APP_ID + ".diamond_1200pack", 9.99) // Purchase type
        );
        public readonly static VirtualCurrencyPack DIAMOND_3100PACK = new VirtualCurrencyPack(
                "3100 Diamonds +38% & No Ads",                             // Name
                "3100 Diamonds +38% & No Ads",                       // Description
                "diamonds_3100",                             // Item id
                3100,                                           // Number of currency units in this pack
                AOMStoreConstant.DIAMOND_CURRENCY_ID,                           // Associated currency with this pack
                new PurchaseWithMarket(APP_ID + ".diamond_3100pack", 24.99)
        );
        public readonly static VirtualCurrencyPack DIAMOND_6500PACK = new VirtualCurrencyPack(
                "6500 Diamonds +44% & No Ads",                             // Name
                "6500 Diamonds +44% & No Ads",                       // Description
                "diamonds_6500",                             // Item id
                6500,                                           // Number of currency units in this pack
                AOMStoreConstant.DIAMOND_CURRENCY_ID,                           // Associated currency with this pack
                new PurchaseWithMarket(APP_ID + ".diamond_6500pack", 49.99)
        );
        public readonly static VirtualCurrencyPack DIAMOND_14000PACK = new VirtualCurrencyPack(
                "14000 Diamonds +56% & No Ads",                             // Name
                "14000 Diamonds +56% & No Ads",                      // Description
                "diamonds_14000",                             // Item id
                14000,                                          // Number of currency units in this pack
                AOMStoreConstant.DIAMOND_CURRENCY_ID,                           // Associated currency with this pack
                new PurchaseWithMarket(APP_ID + ".diamond_14000pack", 99.99)
        );

        public readonly static VirtualCurrencyPack PROMOTIONAL_GOLDPACK = new VirtualCurrencyPack(
                "3500 Diamonds & Oberon's Treasure x5",                             // Name
                "3500 Diamonds & Oberon's Treasure x5",                      // Description
                "promotional_goldpack",                             // Item id
                3500,                                          // Number of currency units in this pack
                AOMStoreConstant.DIAMOND_CURRENCY_ID,                           // Associated currency with this pack
                new PurchaseWithMarket(APP_ID + ".promotional_goldpack", 39.99)
        );

        public readonly static VirtualCurrencyPack PROMOTIONAL_SILVERPACK = new VirtualCurrencyPack(
                "2000 Diamonds & Oberon's Treasure x5",                             // Name
                "2000 Diamonds & Oberon's Treasure x5",                      // Description
                "promotional_silverpack",                             // Item id
                2000,                                          // Number of currency units in this pack
                AOMStoreConstant.DIAMOND_CURRENCY_ID,                           // Associated currency with this pack
                new PurchaseWithMarket(APP_ID + ".promotional_silverpack", 24.99)
        );

        public readonly static VirtualCurrencyPack PROMOTIONAL_BRONZEPACK = new VirtualCurrencyPack(
                "1000 Diamonds & Oberon's Treasure x2",                             // Name
                "1000 Diamonds & Oberon's Treasure x2",                      // Description
                "promotional_bronzepack",                             // Item id
                1000,                                          // Number of currency units in this pack
                AOMStoreConstant.DIAMOND_CURRENCY_ID,                           // Associated currency with this pack
                new PurchaseWithMarket(APP_ID + ".promotional_bronzepack", 9.99)
        );

        public readonly static VirtualCurrencyPack[] DiamondPack = {
			DIAMOND_180PACK,
			DIAMOND_500PACK,
			DIAMOND_1200PACK,
			DIAMOND_3100PACK,
			DIAMOND_6500PACK,
			DIAMOND_14000PACK
		};

        /// <summary>
        /// see parent.
        /// </summary>
        public VirtualCurrencyPack[] GetCurrencyPacks()
        {
            List<VirtualCurrencyPack> currencyPacks = new List<VirtualCurrencyPack>();
            currencyPacks.AddRange(DiamondPack);

            currencyPacks.Add(PROMOTIONAL_GOLDPACK);
            currencyPacks.Add(PROMOTIONAL_SILVERPACK);
            currencyPacks.Add(PROMOTIONAL_BRONZEPACK);

            // TODO: Currency Pack
            return currencyPacks.ToArray();
        }


        /** Virtual Goods **/
        // TODO Define Avatar Skills, Heroes, Hero Skills, Artifacts, etc...

        // ** Avatar Level **/
        public readonly static CharacterLevelVG AvatarLevelVG = new CharacterLevelVG("AVATAR_LEVELS", 1, 5);

        /** Avatar Active Skills **/
        public readonly static AvatarActiveSkillVG HeavenlyStrikeASVG = new HeavenlyStrikeASVG(AvatarLevelVG);
        public readonly static AvatarActiveSkillVG ShadowCloneASVG = new ShadowCloneASVG(AvatarLevelVG);
        public readonly static AvatarActiveSkillVG CriticalStrikeASVG = new CriticalStrikeASVG(AvatarLevelVG);
        public readonly static AvatarActiveSkillVG WarCryASVG = new WarCryASVG(AvatarLevelVG);
        public readonly static AvatarActiveSkillVG BerserkerRageASVG = new BerserkerRageASVG(AvatarLevelVG);
        public readonly static AvatarActiveSkillVG HandOfMidasASVG = new HandOfMidasASVG(AvatarLevelVG);
        public readonly static IList<AvatarActiveSkillVG> AvatarActiveSkillVGs = new List<AvatarActiveSkillVG> {
			HeavenlyStrikeASVG, ShadowCloneASVG, CriticalStrikeASVG, WarCryASVG, BerserkerRageASVG, HandOfMidasASVG
        };

        /** GameItem **/
        public readonly static GameItemVG MakeItRainGIVG = new GameItemVG("IT001", 100);
        public readonly static GameItemVG DoomGIVG = new GameItemVG("IT002", 100);
        public readonly static GameItemVG PowerOfHoldingGIVG = new PowerOfHoldingGIVG("IT003", 50);
        public readonly static GameItemVG GuardianShieldGIVG = new GuardianShieldGIVG("IT004", 0);
        public readonly static GameItemVG SkillRefreshGIVG = new SkillRefreshGIVG("IT005", 19);
        public readonly static GameItemVG AnotherWorldKeyGIVG = new GameItemVG("key_001", 1200);
        public readonly static GameItemVG SchoolKeyGIVG = new GameItemVG("key_002", 1200);
        public readonly static GameItemVG ForestKeyGIVG = new GameItemVG("key_003", 1200);
        public readonly static GameItemVG SummerKeyGIVG = new GameItemVG("key_004", 1200);
        public readonly static GameItemVG HalloweenKeyGIVG = new GameItemVG("key_005", 1200);
        public readonly static GameItemVG FukubukuroKeyGIVG = new GameItemVG("key_006", 1200);

        public readonly static GameItemVG PvPKey001GIVG = new GameItemVG("key_pvp_001", 0);
        public readonly static GameItemVG PvPKey002GIVG = new GameItemVG("key_pvp_002", 0);

        public readonly static IList<GameItemVG> GameItemVGs = new List<GameItemVG> {
            FukubukuroKeyGIVG,
            HalloweenKeyGIVG,
            SummerKeyGIVG,
            ForestKeyGIVG,
            SchoolKeyGIVG,
            AnotherWorldKeyGIVG, 
            MakeItRainGIVG, 
            DoomGIVG, 
            PowerOfHoldingGIVG, 
            GuardianShieldGIVG, 
            SkillRefreshGIVG
		};

        public readonly static IList<GameItemVG> PvPGiftGameItemVGs = new List<GameItemVG> {
			PvPKey001GIVG,
			PvPKey002GIVG
		};

        public static GameItemVG GetGameItemVGFromItemId(string itemId)
        {
            foreach (GameItemVG gameItemVG in AOMAssets.GameItemVGs)
            {
                if (gameItemVG.ItemId == itemId)
                {
                    GameItemVG gameItemVGFromItemId = gameItemVG;

                    return gameItemVGFromItemId;
                }
            }

            return null;
        }

        public static VirtualGood KEY_OF_WORLD_001 = new LifetimeVG(
            "The Key Of the Dark World",                              				// Name
            "The key dropped from Xavier. It unlocks the gate of the Dark World and frees the Moetans of the Darkness.",                       // Description
            "key_001",														// Item ID
            new PurchaseWithMarket(                  						// Purchase type (with real money $)
#if UNITY_IOS
				APP_ID + ".keyofworld_001",                                 // Product ID 
#elif UNITY_ANDROID
                APP_ID + ".key_001",                                        // Product ID 
#endif
                6.99                                      					// Price (in real money $)
            )
        );

        public static VirtualGood KEY_OF_WORLD_002 = new LifetimeVG(
            "The Uniform Of Moe College",                              				// Name
            "The uniform of Moe College. It unlocks the door of school and the Moetans can leave.",                       // Description
            "key_002",														// Item ID
            new PurchaseWithMarket(                  						// Purchase type (with real money $)
#if UNITY_IOS
				APP_ID + ".keyofworld_002",                                 // Product ID 
#elif UNITY_ANDROID
                APP_ID + ".key_002",	                                 	// Product ID 
#endif
                6.99                                      					// Price (in real money $)
            )
        );

        public static VirtualGood KEY_OF_WORLD_003 = new LifetimeVG(
            "The Door Of the Forest",                              				// Name
            "The Door Of the Forest. It unlocks the forest and frees the Moetans of the forest.",                       // Description
            "key_003",														// Item ID
            new PurchaseWithMarket(                  						// Purchase type (with real money $)
#if UNITY_IOS
				APP_ID + ".keyofworld_003",                                 // Product ID 
#elif UNITY_ANDROID
                APP_ID + ".key_003",	                                 	// Product ID 
#endif
                6.99                                      					// Price (in real money $)
            )
        );

        public static VirtualGood KEY_OF_WORLD_004 = new LifetimeVG(
            "The Water World Ticket",                              				// Name
            "The Water World Ticket. It unlocks the water world and frees the Moetans of the water world.",                       // Description
            "key_004",														// Item ID
            new PurchaseWithMarket(                  						// Purchase type (with real money $)
#if UNITY_IOS
				APP_ID + ".keyofworld_004",                                 // Product ID 
#elif UNITY_ANDROID
                APP_ID + ".key_004",	                                 	// Product ID 
#endif
                6.99                                      					// Price (in real money $)
            )
        );

        public static VirtualGood KEY_OF_WORLD_005 = new LifetimeVG(
            "The Smile Pumpkin Lantern",                              				// Name
            "The Smile Pumpkin Lantern. It unlocks the new world and frees the Moetans of the new world.",                       // Description
            "key_005",														// Item ID
            new PurchaseWithMarket(                  						// Purchase type (with real money $)
#if UNITY_IOS
				APP_ID + ".keyofworld_005",                                 // Product ID 
#elif UNITY_ANDROID
                APP_ID + ".key_005",	                                 	// Product ID 
#endif
                6.99                                      					// Price (in real money $)
            )
        );

        public static VirtualGood KEY_OF_WORLD_006 = new LifetimeVG(
            "The Fukubukuro",                              				// Name
            "The Fukubukuro. It unlocks the new world and frees the Moetans of the new world.",                       // Description
            "key_006",														// Item ID
            new PurchaseWithMarket(                  						// Purchase type (with real money $)
#if UNITY_IOS
				APP_ID + ".keyofworld_006",                                 // Product ID
#elif UNITY_ANDROID
                APP_ID + ".key_006",	                                 	// Product ID
#endif
                6.99                                      					// Price (in real money $)
            )
        );

        public readonly static List<VirtualGood> NonConsumableItems = new List<VirtualGood> {
            KEY_OF_WORLD_006,
            KEY_OF_WORLD_005,
            KEY_OF_WORLD_004, 
            KEY_OF_WORLD_003, 
            KEY_OF_WORLD_002, 
            KEY_OF_WORLD_001 
		};

        /// <summary>
        /// see parent.
        /// </summary>
        public VirtualGood[] GetGoods()
        {
            List<VirtualGood> goods = new List<VirtualGood>();

            goods.Add(AvatarLevelVG);

            goods.AddRange(AvatarActiveSkillVGs.OfType<VirtualGood>());

            goods.AddRange(HeroAssets.Heroes.OfType<VirtualGood>());

            // Hero Skills
            goods.AddRange(HeroAssets.HeroSkills.OfType<VirtualGood>());

            // Artifacts
            goods.AddRange(ArtifactAssets.Artifacts.OfType<VirtualGood>());

            // MoeSpirit
            goods.AddRange(ArtifactAssets.MoeSpirits.OfType<VirtualGood>());

            // Achievement
            goods.AddRange(AchievementAssets.Achievements.OfType<VirtualGood>());

            // World Key
            goods.Add(KEY_OF_WORLD_006);
            goods.Add(KEY_OF_WORLD_005);
            goods.Add(KEY_OF_WORLD_004);
            goods.Add(KEY_OF_WORLD_003);
            goods.Add(KEY_OF_WORLD_002);
            goods.Add(KEY_OF_WORLD_001);

            // Game Item
            goods.AddRange(GameItemVGs.OfType<VirtualGood>());

            // PvPGiftGameItems
            goods.AddRange(PvPGiftGameItemVGs.OfType<VirtualGood>());

            //goods.Add(NO_ADS);

            return goods.ToArray();
        }



        /** Virtual Goods Categories **/
        public static VirtualCategory AvatarLevelCategory =
            new VirtualCategory(
                "Avatar Level Category",
                new List<string> { AvatarLevelVG.ItemId }
        );
        public static VirtualCategory AvatarActiveSkilLCategory =
            new VirtualCategory(
                "Avatar Active Skill Category",
                AvatarActiveSkillVGs.Select(vg => vg.ItemId).ToList()
        );
        public static VirtualCategory HeroLevelCategory =
            new VirtualCategory(
                "Hero Level Category",
                HeroAssets.Heroes.Select(vg => vg.ItemId).ToList()
        );

        /// <summary>
        /// see parent.
        /// </summary>
        public VirtualCategory[] GetCategories()
        {
            return new VirtualCategory[] {
				AvatarLevelCategory,
				AvatarActiveSkilLCategory,
				HeroLevelCategory
			};
        }

        public static double CurrencyPackMarketPrice(string itemId)
        {
            VirtualCurrencyPack pvi = null;

            for (int i = 0; i < StoreInfo.CurrencyPacks.Count; i++)
            {
                if (StoreInfo.CurrencyPacks[i].ItemId == itemId)
                    pvi = StoreInfo.CurrencyPacks[i];
            }

            MarketItem mi = ((PurchaseWithMarket)pvi.PurchaseType).MarketItem;

            return mi.Price;
        }

        public static string CurrencyPackMarketPriceAndCurrency(string itemId)
        {
            VirtualCurrencyPack pvi = null;

            for (int i = 0; i < StoreInfo.CurrencyPacks.Count; i++)
            {
                if (StoreInfo.CurrencyPacks[i].ItemId == itemId)
                    pvi = StoreInfo.CurrencyPacks[i];
            }

            MarketItem mi = ((PurchaseWithMarket)pvi.PurchaseType).MarketItem;

            return mi.MarketPriceAndCurrency;
        }

        public static double ProductMarketPrice(string itemId)
        {
            string productId = APP_ID + "." + itemId;

            PurchasableVirtualItem pvi = StoreInfo.GetPurchasableItemWithProductId(productId);

            MarketItem mi = ((PurchaseWithMarket)pvi.PurchaseType).MarketItem;

            return mi.Price;
        }

        public static string ProductMarketPriceAndCurrency(string itemId)
        {
            string productId = APP_ID + "." + itemId;

            PurchasableVirtualItem pvi = StoreInfo.GetPurchasableItemWithProductId(productId);

            MarketItem mi = ((PurchaseWithMarket)pvi.PurchaseType).MarketItem;

            return mi.MarketPriceAndCurrency;
        }

    }
}
