﻿using UnityEngine;
using System.Collections;
using Soomla.Store;

namespace Ignite.AOM
{
    public class AchievementVG : AOMCachedBalanceVG
    {
        private double priceForOne = -1;
        private int[] rewardDiamonds = new int[] { 15, 25, 50, 100, 200 };
        private int[] rewardRelics = new int[] { 5, 10, 25, 50, 100 };
        private double[] goals;
        private StatType statType;
        private int rewards;
        private bool isDiamond = true;
        private bool priceIsDirty = true;

        public AchievementVG(string itemId, StatType type, double goal1, double goal2, double goal3, double goal4, double goal5, bool isDiamond = true)
            : base("", "", itemId, new PurchaseWithVirtualItem(AOMStoreConstant.GOLD_CURRENCY_ID, 0))
        {
            goals = new double[] { goal1, goal2, goal3, goal4, goal5 };
            this.statType = type;
            this.isDiamond = isDiamond;
            //			RefreshPrice (true);
        }

        protected void RefreshPrice(bool notify)
        {
            priceForOne = PriceForNextLevel();
            if (Completed())
            {
                rewards = 0;
            }
            else
            {
                if (isDiamond)
                    rewards = rewardDiamonds[GetBalance()];
                else
                    rewards = rewardRelics[GetBalance()];
            }
            //			if(notify)
            //				EventManager.RelicPriceUpdated();
        }

        public override int ResetBalance(int balance, bool notify)
        {
            int result = base.ResetBalance(balance, notify);
            RefreshPrice(notify);
            if (notify)
            {
                EventManager.AchievementVGBalanceChange();
            }

            return result;
        }

        public override int Give(int amount, bool notify)
        {
            int balance = base.Give(amount, notify);
            RefreshPrice(notify);
            if (notify)
            {
                EventManager.AchievementVGBalanceChange();
            }

            return balance;
        }

        public override int Take(int amount, bool notify)
        {
            int balance = base.Take(amount, notify);
            RefreshPrice(notify);
            if (notify)
            {
                EventManager.AchievementVGBalanceChange();
            }

            return balance;
        }

        protected double PriceForNextLevel()
        {
            if (Completed())
            {
                return goals[goals.Length - 1];
            }
            return goals[GetBalance()];
        }

        public bool Completed()
        {
            return GetBalance() >= goals.Length;
        }

        public bool CanAffordOne()
        {
            return GamePlayStatManager.Instance.Stat[statType] >= PriceForOne && !Completed();
        }

        public void BuyOne()
        {
            if (CanAffordOne())
            {
                Give(1);
            }
        }

        public StatType StatType
        {
            get { return statType; }
        }

        public double PriceForOne
        {
            get
            {
                if (priceIsDirty)
                {
                    RefreshPrice(true);
                    priceIsDirty = false;
                }
                return priceForOne;
            }
        }

        public int Rewards
        {
            get { return rewards; }
        }

    }
}