﻿using UnityEngine;
using System.Collections;
using Soomla.Store;

namespace Ignite.AOM
{

    public abstract class AOMCachedBalanceVG : SingleUseVG
    {

        private bool isDirty = true;
        private int _cachedBalance;

        public AOMCachedBalanceVG(string name, string description, string itemId, PurchaseType purchaseType)
            : base(name, description, itemId, purchaseType)
        {
        }

        public override int GetBalance()
        {
            if (isDirty)
            {
                isDirty = false;
                _cachedBalance = base.GetBalance();
            }
            return _cachedBalance;
        }

        //call this function after soomla init to set a correct balance
        //		public virtual void ReadBalanceFromDB(){
        //			_cachedBalance = base.GetBalance();
        //			RefreshPrice(true);
        //		}

        public override int ResetBalance(int balance, bool notify)
        {
            _cachedBalance = base.ResetBalance(balance, notify);
            return _cachedBalance;
        }

        public override int Give(int amount, bool notify)
        {
            _cachedBalance = base.Give(amount, notify);
            return _cachedBalance;
        }

        public override int Take(int amount, bool notify)
        {
            _cachedBalance = base.Take(amount, notify);
            return _cachedBalance;
        }

        //		protected abstract void RefreshPrice (bool notify);

    }
}
