﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class PowerOfHoldingGIVG : GameItemVG
    {
        public PowerOfHoldingGIVG(string itemId, int price)
            : base(itemId, price)
        {
        }

        public override bool CanAffordOne()
        {
            return base.CanAffordOne() && !Avatar.instance.UsingPowerOfHolding;
        }
    }
}