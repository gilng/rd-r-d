
using UnityEngine;
namespace Ignite.AOM
{

    // TODO Move EffectType down to PassiveSkill subclass?
    public enum EffectType
    {
        HeroDamage,                 // Increase hero DPS
        AllDamage,                  // Buff for all damage (i.e. avatar tap damage, hero damage, etc...)
        CriticalDamage,             // Increase avatar critical strike damage
        GoldDropAmount,             // Increase gold drop amount from normal enemy, boss and fairy chest. Remark: Exclude Treasure Chest Monster
        TapDamage,                  // Buff for boosting avatar tap damage
        TotalDPSTapDamage,          // Buff for boosting avatar tap damage base of x% of total hero DPS
        TreasureChestGoldAmount,    // Increase treasure chest monster gold drop amount
        CriticalChance,             // Increase avatar critical strike chance
        BossDamage,                 // Increase avatar and hero damage (both basic attack and skill) to boss
        AvatarActiveSkill,          // Avatar active skills which has its own implementation
        HeroEvolve					// TODO: Implement Hero Evolve in future release
    }


    public abstract class CharacterSkillVG : LevelUpVG
    {

        private CharacterLevelVG character;
        private int characterMinLevel;

        private EffectType effectType;
        //        private double effectValue;

        public CharacterLevelVG Character
        {
            get
            {
                return this.character;
            }
        }

        public int CharacterMinLevel
        {
            get
            {
                return this.characterMinLevel;
            }
        }

        public EffectType EffectType
        {
            get
            {
                return this.effectType;
            }
        }

        public abstract double EffectValue
        {
            get;
        }

        public CharacterSkillVG(string itemId,
                                CharacterLevelVG character, int characterMinLevel,
                                EffectType effectType)
            : base(itemId, 0, false, false)
        {

            this.character = character;
            this.characterMinLevel = characterMinLevel;
            this.effectType = effectType;
        }

        public bool CanBuy()
        {
            return character.GetBalance() >= characterMinLevel;
        }

        //public override double PriceForUpgradingToLevel(int lv)
        //{
        //    // TODO Chase Nein for the correct formula
        //    return 1;
        //} 
    }
}