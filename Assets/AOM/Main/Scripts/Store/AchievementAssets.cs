﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Ignite.AOM
{
    public class AchievementAssets
    {
        public readonly static AchievementVG Achievement000 = new AchievementVG("AH000", StatType.InviteFriends, 1, 5, 10, 25, 50, false);
        public readonly static AchievementVG Achievement001 = new AchievementVG("AH001", StatType.KillMonster, 100, 1000, 10000, 100000, 1000000);
        public readonly static AchievementVG Achievement002 = new AchievementVG("AH002", StatType.CollectGold, 100000, 1 * Math.Pow(10, 15), 1 * Math.Pow(10, 30), 1 * Math.Pow(10, 48), 1 * Math.Pow(10, 78));
        public readonly static AchievementVG Achievement003 = new AchievementVG("AH003", StatType.ReachHighestStage, 15, 100, 150, 300, 500);
        public readonly static AchievementVG Achievement004 = new AchievementVG("AH004", StatType.CollectRelic, 5, 50, 500, 5000, 50000);
        public readonly static AchievementVG Achievement005 = new AchievementVG("AH005", StatType.OwnArtifact, 2, 8, 15, 20, 29);
        public readonly static AchievementVG Achievement006 = new AchievementVG("AH006", StatType.ReachHeroDPS, 1000, Math.Pow(10, 10), Math.Pow(10, 19), Math.Pow(10, 50), Math.Pow(10, 100));
        public readonly static AchievementVG Achievement007 = new AchievementVG("AH007", StatType.KillBoss, 10, 100, 1000, 10000, 100000);
        public readonly static AchievementVG Achievement008 = new AchievementVG("AH008", StatType.Tap, 1000, 100000, 1000000, 10000000, 100000000);
        public readonly static AchievementVG Achievement009 = new AchievementVG("AH009", StatType.Prestige, 1, 5, 25, 125, 600);
        public readonly static AchievementVG Achievement010 = new AchievementVG("AH010", StatType.LevelUpHeroTimes, 200, 2000, 5000, 15000, 50000);
        public readonly static AchievementVG Achievement011 = new AchievementVG("AH011", StatType.OpenChest, 10, 100, 1000, 10000, 100000);
        public readonly static AchievementVG Achievement012 = new AchievementVG("AH012", StatType.GetFairyPresent, 5, 50, 500, 5000, 50000);
        public readonly static AchievementVG Achievement013 = new AchievementVG("AH013", StatType.UseJumpAttack, 3, 30, 300, 3000, 30000);
        public readonly static AchievementVG Achievement014 = new AchievementVG("AH014", StatType.GetCriticalHit, 10, 5000, 5000000, 5 * Math.Pow(10, 9), 5 * Math.Pow(10, 12));

        public readonly static List<AchievementVG> Achievements = new List<AchievementVG> {
			Achievement000, Achievement001, Achievement002, Achievement003, Achievement004, 
			Achievement005,	Achievement006, Achievement007, Achievement008, Achievement009, 
			Achievement010,	Achievement011, Achievement012, Achievement013, Achievement014
		};
        public readonly static IDictionary<StatType, AchievementVG> AchievementByStatType
            = new Dictionary<StatType, AchievementVG>();

        static AchievementAssets()
        {
            foreach (AchievementVG a in Achievements)
            {
                AchievementByStatType.Add(a.StatType, a);
            }
        }
    }
}