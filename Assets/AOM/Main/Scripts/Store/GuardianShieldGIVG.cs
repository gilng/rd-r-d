﻿using UnityEngine;
using System.Collections;
using System;

namespace Ignite.AOM
{
    public class GuardianShieldGIVG : GameItemVG
    {
        //		protected int cachedPriceForOne = -1;
        private int prevStage = -1;

        public GuardianShieldGIVG(string itemId, int price)
            : base(itemId, price)
        {
            //			RefreshPrice(true);
        }

        //		protected void RefreshPrice(bool notify) {
        //			priceForOne = PriceToBuyOne();
        //			if(notify && priceForOne!=cachedPriceForOne){
        //				cachedPriceForOne=priceForOne;
        //				EventManager.DiamondPriceUpdated();
        //			}
        //		}

        public void CheckPriceUpdated(int stage)
        {
            int currStage = stage;
            if (prevStage != currStage)
            {
                price = Math.Max(100, currStage);
                prevStage = currStage;
                EventManager.DiamondPriceUpdated();
            }
        }
    }
}