﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{

    public class HeavenlyStrikeASVG : AvatarActiveSkillVG
    {
        private static readonly double BASE_COST = 2780;
        private static readonly double MULTIPLIER = 106000;

        public HeavenlyStrikeASVG(CharacterLevelVG character)
            : base("active_skill_1", character, 50, EffectType.AvatarActiveSkill, 600, 0)
        {
        }

        /// <summary>
        /// Return the buff percentage
        /// </summary>
        public override double EffectValue
        {
            get
            {
                // May need to add the artifact logic here later
                return 70 * (GetBalance() + 1) * (1 + ArtifactAssets.MoeSpirit015.EffectValue);
            }
        }

        public override double PriceForUpgradingToLevel(int lv)
        {
            return Formulas.GoldToUpgradeAvatarActiveSkillForLevel(lv - 1, BASE_COST, MULTIPLIER, ArtifactAssets.Artifact024.EffectValue);
        }
    }
}
