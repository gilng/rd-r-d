﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class BerserkerRageASVG : AvatarActiveSkillVG
    {
        private static readonly double BASE_COST = 274000000000000;
        private static readonly double MULTIPLIER = 12100;

        public BerserkerRageASVG(CharacterLevelVG character) :
            base("active_skill_5", character, 400, EffectType.AvatarActiveSkill, 3600, 30)
        {
        }

        /// <summary>
        /// Return the buff percentage
        /// </summary>
        public override double EffectValue
        {
            get
            {
                // May need to add the artifact logic here later
                return DamageFormulas.ActiveSkillBerserkerRageIncreaseForSkillLevel(GetBalance());
            }
        }

        public override double PriceForUpgradingToLevel(int lv)
        {
            return Formulas.GoldToUpgradeAvatarActiveSkillForLevel(lv - 1, BASE_COST, MULTIPLIER, ArtifactAssets.Artifact024.EffectValue);
        }
    }
}
