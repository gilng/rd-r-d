﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class WarCryASVG : AvatarActiveSkillVG
    {
        private static readonly double BASE_COST = 198000000000;
        private static readonly double MULTIPLIER = 2850;

        public WarCryASVG(CharacterLevelVG character) :
            base("active_skill_4", character, 300, EffectType.AvatarActiveSkill, 1800, 30)
        {
        }

        /// <summary>
        /// Return the buff percentage
        /// </summary>
        public override double EffectValue
        {
            get
            {
                // May need to add the artifact logic here later
                return DamageFormulas.ActiveSkillWarCryIncreaseForSkillLevel(GetBalance());
            }
        }

        public override double PriceForUpgradingToLevel(int lv)
        {
            return Formulas.GoldToUpgradeAvatarActiveSkillForLevel(lv - 1, BASE_COST, MULTIPLIER, ArtifactAssets.Artifact024.EffectValue);
        }
    }
}
