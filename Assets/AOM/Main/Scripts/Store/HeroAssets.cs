using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
namespace Ignite.AOM
{

    public class HeroAssets
    {
        private const int HERO_SKILL1_LEVEL = 10;
        private const int HERO_SKILL2_LEVEL = 25;
        private const int HERO_SKILL3_LEVEL = 50;
        private const int HERO_SKILL4_LEVEL = 100;
        private const int HERO_SKILL5_LEVEL = 200;
        private const int HERO_SKILL6_LEVEL = 400;
        private const int HERO_SKILL7_LEVEL = 800;
        private const int HERO_SKILL8_LEVEL = 1000;
        /** Heroes **/
        public readonly static CharacterLevelVG Hero001 = new CharacterLevelVG("HE001", 0, 50);
        public readonly static CharacterLevelVG Hero002 = new CharacterLevelVG("HE002", 0, 175);
        public readonly static CharacterLevelVG Hero003 = new CharacterLevelVG("HE003", 0, 674);
        public readonly static CharacterLevelVG Hero004 = new CharacterLevelVG("HE004", 0, 2850);
        public readonly static CharacterLevelVG Hero005 = new CharacterLevelVG("HE005", 0, 13300);
        public readonly static CharacterLevelVG Hero006 = new CharacterLevelVG("HE006", 0, 68100);
        public readonly static CharacterLevelVG Hero007 = new CharacterLevelVG("HE007", 0, 384000);
        public readonly static CharacterLevelVG Hero008 = new CharacterLevelVG("HE008", 0, 2.38 * Math.Pow(10, 6));
        public readonly static CharacterLevelVG Hero009 = new CharacterLevelVG("HE009", 0, 23.8 * Math.Pow(10, 6));
        public readonly static CharacterLevelVG Hero010 = new CharacterLevelVG("HE010", 0, 143 * Math.Pow(10, 6));
        public readonly static CharacterLevelVG Hero011 = new CharacterLevelVG("HE011", 0, 943 * Math.Pow(10, 6));
        public readonly static CharacterLevelVG Hero012 = new CharacterLevelVG("HE012", 0, 6.84 * Math.Pow(10, 9));
        public readonly static CharacterLevelVG Hero013 = new CharacterLevelVG("HE013", 0, 54.7 * Math.Pow(10, 9));
        public readonly static CharacterLevelVG Hero014 = new CharacterLevelVG("HE014", 0, 820 * Math.Pow(10, 9));
        public readonly static CharacterLevelVG Hero015 = new CharacterLevelVG("HE015", 0, 8.2 * Math.Pow(10, 12));
        public readonly static CharacterLevelVG Hero016 = new CharacterLevelVG("HE016", 0, 164 * Math.Pow(10, 12));
        public readonly static CharacterLevelVG Hero017 = new CharacterLevelVG("HE017", 0, 1.64 * Math.Pow(10, 15));
        public readonly static CharacterLevelVG Hero018 = new CharacterLevelVG("HE018", 0, 49.2 * Math.Pow(10, 15));
        public readonly static CharacterLevelVG Hero019 = new CharacterLevelVG("HE019", 0, 2.46 * Math.Pow(10, 18));
        public readonly static CharacterLevelVG Hero020 = new CharacterLevelVG("HE020", 0, 73.8 * Math.Pow(10, 18));
        public readonly static CharacterLevelVG Hero021 = new CharacterLevelVG("HE021", 0, 2.44 * Math.Pow(10, 21));
        public readonly static CharacterLevelVG Hero022 = new CharacterLevelVG("HE022", 0, 244 * Math.Pow(10, 21));
        public readonly static CharacterLevelVG Hero023 = new CharacterLevelVG("HE023", 0, 48.7 * Math.Pow(10, 24));
        public readonly static CharacterLevelVG Hero024 = new CharacterLevelVG("HE024", 0, 19.5 * Math.Pow(10, 27));
        public readonly static CharacterLevelVG Hero025 = new CharacterLevelVG("HE025", 0, 21.4 * Math.Pow(10, 30));
        public readonly static CharacterLevelVG Hero026 = new CharacterLevelVG("HE026", 0, 2.36 * Math.Pow(10, 36));
        public readonly static CharacterLevelVG Hero027 = new CharacterLevelVG("HE027", 0, 25.9 * Math.Pow(10, 45));
        public readonly static CharacterLevelVG Hero028 = new CharacterLevelVG("HE028", 0, 28.5 * Math.Pow(10, 60));
        public readonly static CharacterLevelVG Hero029 = new CharacterLevelVG("HE029", 0, 3.14 * Math.Pow(10, 81));
        public readonly static CharacterLevelVG Hero030 = new CharacterLevelVG("HE030", 0, 3.14e+96);
        public readonly static CharacterLevelVG Hero031 = new CharacterLevelVG("HE031", 0, 3.76e+101);
        public readonly static CharacterLevelVG Hero032 = new CharacterLevelVG("HE032", 0, 4.14e+121);
        public readonly static CharacterLevelVG Hero033 = new CharacterLevelVG("HE033", 0, 4.56e+141);

        public readonly static List<CharacterLevelVG> Heroes = new List<CharacterLevelVG> {
			Hero001, Hero002,Hero003,Hero004,Hero005,Hero006,Hero007,Hero008,Hero009,Hero010,
			Hero011, Hero012,Hero013,Hero014,Hero015,Hero016,Hero017,Hero018,Hero019,Hero020,
			Hero021, Hero022,Hero023,Hero024,Hero025,Hero026,Hero027,Hero028,Hero029,Hero030,
			Hero031, Hero032,Hero033
		};

        /** Hero Skills **/
        // --------------- Hero 001 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero001Skill001 =
            new HeroPassiveSkillVG("HE001_skill_1",
                                   Hero001, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 0.5);
        public readonly static HeroPassiveSkillVG Hero001Skill002 =
            new HeroPassiveSkillVG("HE001_skill_2",
                                   Hero001, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 1);
        public readonly static HeroPassiveSkillVG Hero001Skill003 =
            new HeroPassiveSkillVG("HE001_skill_3",
                                   Hero001, HERO_SKILL3_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero001Skill004 =
            new HeroPassiveSkillVG("HE001_skill_4",
                                   Hero001, HERO_SKILL4_LEVEL, EffectType.CriticalDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero001Skill005 =
            new HeroPassiveSkillVG("HE001_skill_5",
                                   Hero001, HERO_SKILL5_LEVEL, EffectType.HeroDamage, 10);
        public readonly static HeroPassiveSkillVG Hero001Skill006 =
            new HeroPassiveSkillVG("HE001_skill_6",
                                   Hero001, HERO_SKILL6_LEVEL, EffectType.AllDamage, 0.25);
        public readonly static HeroPassiveSkillVG Hero001Skill007 =
            new HeroPassiveSkillVG("HE001_skill_7",
                                   Hero001, HERO_SKILL7_LEVEL, EffectType.HeroDamage, 100);

        // --------------- Hero 002 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero002Skill001 =
            new HeroPassiveSkillVG("HE002_skill_1",
                                   Hero002, HERO_SKILL1_LEVEL, EffectType.TapDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero002Skill002 =
            new HeroPassiveSkillVG("HE002_skill_2",
                                   Hero002, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 1);
        public readonly static HeroPassiveSkillVG Hero002Skill003 =
            new HeroPassiveSkillVG("HE002_skill_3",
                                   Hero002, HERO_SKILL3_LEVEL, EffectType.HeroDamage, 10);
        public readonly static HeroPassiveSkillVG Hero002Skill004 =
            new HeroPassiveSkillVG("HE002_skill_4",
                                 Hero002, HERO_SKILL4_LEVEL, EffectType.TotalDPSTapDamage, 0.004);
        public readonly static HeroPassiveSkillVG Hero002Skill005 =
            new HeroPassiveSkillVG("HE002_skill_5",
                                   Hero002, HERO_SKILL5_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero002Skill006 =
            new HeroPassiveSkillVG("HE002_skill_6",
                                   Hero002, HERO_SKILL6_LEVEL, EffectType.GoldDropAmount, 0.1);
        public readonly static HeroPassiveSkillVG Hero002Skill007 =
            new HeroPassiveSkillVG("HE002_skill_7",
                                   Hero002, HERO_SKILL7_LEVEL, EffectType.HeroDamage, 100);

        // --------------- Hero 003 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero003Skill001 =
            new HeroPassiveSkillVG("HE003_skill_1",
                                   Hero003, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 1.5);
        public readonly static HeroPassiveSkillVG Hero003Skill002 =
            new HeroPassiveSkillVG("HE003_skill_2",
                                   Hero003, HERO_SKILL2_LEVEL, EffectType.GoldDropAmount, 0.1);
        public readonly static HeroPassiveSkillVG Hero003Skill003 =
            new HeroPassiveSkillVG("HE003_skill_3",
                                   Hero003, HERO_SKILL3_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero003Skill004 =
            new HeroPassiveSkillVG("HE003_skill_4",
                                   Hero003, HERO_SKILL4_LEVEL, EffectType.TotalDPSTapDamage, 0.004);
        public readonly static HeroPassiveSkillVG Hero003Skill005 =
            new HeroPassiveSkillVG("HE003_skill_5",
                                   Hero003, HERO_SKILL5_LEVEL, EffectType.TreasureChestGoldAmount, 0.2);
        public readonly static HeroPassiveSkillVG Hero003Skill006 =
            new HeroPassiveSkillVG("HE003_skill_6",
                                   Hero003, HERO_SKILL6_LEVEL, EffectType.CriticalChance, 0.01);
        public readonly static HeroPassiveSkillVG Hero003Skill007 =
            new HeroPassiveSkillVG("HE003_skill_7",
                                   Hero003, HERO_SKILL7_LEVEL, EffectType.AllDamage, 0.3);

        // --------------- Hero 004 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero004Skill001 =
            new HeroPassiveSkillVG("HE004_skill_1",
                                   Hero004, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 1);
        public readonly static HeroPassiveSkillVG Hero004Skill002 =
            new HeroPassiveSkillVG("HE004_skill_2",
                                   Hero004, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 8);
        public readonly static HeroPassiveSkillVG Hero004Skill003 =
            new HeroPassiveSkillVG("HE004_skill_3",
                                   Hero004, HERO_SKILL3_LEVEL, EffectType.GoldDropAmount, 0.06);
        public readonly static HeroPassiveSkillVG Hero004Skill004 =
            new HeroPassiveSkillVG("HE004_skill_4",
                                   Hero004, HERO_SKILL4_LEVEL, EffectType.HeroDamage, 5);
        public readonly static HeroPassiveSkillVG Hero004Skill005 =
            new HeroPassiveSkillVG("HE004_skill_5",
                                   Hero004, HERO_SKILL5_LEVEL, EffectType.CriticalDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero004Skill006 =
            new HeroPassiveSkillVG("HE004_skill_6",
                                   Hero004, HERO_SKILL6_LEVEL, EffectType.AllDamage, 0.2);
        public readonly static HeroPassiveSkillVG Hero004Skill007 =
            new HeroPassiveSkillVG("HE004_skill_7",
                                   Hero004, HERO_SKILL7_LEVEL, EffectType.TreasureChestGoldAmount, 0.2);

        // --------------- Hero 005 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero005Skill001 =
            new HeroPassiveSkillVG("HE005_skill_1",
                                   Hero005, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 3);
        public readonly static HeroPassiveSkillVG Hero005Skill002 =
            new HeroPassiveSkillVG("HE005_skill_2",
                                   Hero005, HERO_SKILL2_LEVEL, EffectType.GoldDropAmount, 0.1);
        public readonly static HeroPassiveSkillVG Hero005Skill003 =
            new HeroPassiveSkillVG("HE005_skill_3",
                                   Hero005, HERO_SKILL3_LEVEL, EffectType.TotalDPSTapDamage, 0.004);
        public readonly static HeroPassiveSkillVG Hero005Skill004 =
            new HeroPassiveSkillVG("HE005_skill_4",
                                Hero005, HERO_SKILL4_LEVEL, EffectType.GoldDropAmount, 0.15);
        public readonly static HeroPassiveSkillVG Hero005Skill005 =
            new HeroPassiveSkillVG("HE005_skill_5",
                                   Hero005, HERO_SKILL5_LEVEL, EffectType.TreasureChestGoldAmount, 0.2);
        public readonly static HeroPassiveSkillVG Hero005Skill006 =
            new HeroPassiveSkillVG("HE005_skill_6",
                                   Hero005, HERO_SKILL6_LEVEL, EffectType.TapDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero005Skill007 =
            new HeroPassiveSkillVG("HE005_skill_7",
                                   Hero005, HERO_SKILL7_LEVEL, EffectType.HeroDamage, 100);

        // --------------- Hero 006 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero006Skill001 =
            new HeroPassiveSkillVG("HE006_skill_1",
                                   Hero006, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 2);
        public readonly static HeroPassiveSkillVG Hero006Skill002 =
            new HeroPassiveSkillVG("HE006_skill_2",
                                   Hero006, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 7);
        public readonly static HeroPassiveSkillVG Hero006Skill003 =
            new HeroPassiveSkillVG("HE006_skill_3",
                                   Hero006, HERO_SKILL3_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero006Skill004 =
            new HeroPassiveSkillVG("HE006_skill_4",
                                   Hero006, HERO_SKILL4_LEVEL, EffectType.AllDamage, 0.2);
        public readonly static HeroPassiveSkillVG Hero006Skill005 =
            new HeroPassiveSkillVG("HE006_skill_5",
                                   Hero006, HERO_SKILL5_LEVEL, EffectType.CriticalDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero006Skill006 =
            new HeroPassiveSkillVG("HE006_skill_6",
                                   Hero006, HERO_SKILL6_LEVEL, EffectType.CriticalDamage, 0.2);
        public readonly static HeroPassiveSkillVG Hero006Skill007 =
            new HeroPassiveSkillVG("HE006_skill_7",
                                   Hero006, HERO_SKILL7_LEVEL, EffectType.HeroDamage, 100);

        // --------------- Hero 007 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero007Skill001 =
            new HeroPassiveSkillVG("HE007_skill_1",
                                   Hero007, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 2);
        public readonly static HeroPassiveSkillVG Hero007Skill002 =
            new HeroPassiveSkillVG("HE007_skill_2",
                                   Hero007, HERO_SKILL2_LEVEL, EffectType.BossDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero007Skill003 =
            new HeroPassiveSkillVG("HE007_skill_3",
                                   Hero007, HERO_SKILL3_LEVEL, EffectType.BossDamage, 0.07);
        public readonly static HeroPassiveSkillVG Hero007Skill004 =
            new HeroPassiveSkillVG("HE007_skill_4",
                                   Hero007, HERO_SKILL4_LEVEL, EffectType.HeroDamage, 6);
        public readonly static HeroPassiveSkillVG Hero007Skill005 =
            new HeroPassiveSkillVG("HE007_skill_5",
                                   Hero007, HERO_SKILL5_LEVEL, EffectType.TapDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero007Skill006 =
            new HeroPassiveSkillVG("HE007_skill_6",
                                   Hero007, HERO_SKILL6_LEVEL, EffectType.TreasureChestGoldAmount, 0.2);
        public readonly static HeroPassiveSkillVG Hero007Skill007 =
            new HeroPassiveSkillVG("HE007_skill_7",
                                   Hero007, HERO_SKILL7_LEVEL, EffectType.AllDamage, 0.3);

        // --------------- Hero 008 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero008Skill001 =
            new HeroPassiveSkillVG("HE008_skill_1",
                                   Hero008, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 2);
        public readonly static HeroPassiveSkillVG Hero008Skill002 =
            new HeroPassiveSkillVG("HE008_skill_2",
                                   Hero008, HERO_SKILL2_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero008Skill003 =
            new HeroPassiveSkillVG("HE008_skill_3",
                                   Hero008, HERO_SKILL3_LEVEL, EffectType.TotalDPSTapDamage, 0.004);
        public readonly static HeroPassiveSkillVG Hero008Skill004 =
            new HeroPassiveSkillVG("HE008_skill_4",
                                   Hero008, HERO_SKILL4_LEVEL, EffectType.GoldDropAmount, 0.15);
        public readonly static HeroPassiveSkillVG Hero008Skill005 =
            new HeroPassiveSkillVG("HE008_skill_5",
                                   Hero008, HERO_SKILL5_LEVEL, EffectType.TreasureChestGoldAmount, 0.2);
        public readonly static HeroPassiveSkillVG Hero008Skill006 =
            new HeroPassiveSkillVG("HE008_skill_6",
                                   Hero008, HERO_SKILL6_LEVEL, EffectType.HeroDamage, 19);
        public readonly static HeroPassiveSkillVG Hero008Skill007 =
            new HeroPassiveSkillVG("HE008_skill_7",
                                   Hero008, HERO_SKILL7_LEVEL, EffectType.AllDamage, 0.2);

        // --------------- Hero 009 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero009Skill001 =
            new HeroPassiveSkillVG("HE009_skill_1",
                                   Hero009, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 1.5);
        public readonly static HeroPassiveSkillVG Hero009Skill002 =
            new HeroPassiveSkillVG("HE009_skill_2",
                                   Hero009, HERO_SKILL2_LEVEL, EffectType.BossDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero009Skill003 =
            new HeroPassiveSkillVG("HE009_skill_3",
                                   Hero009, HERO_SKILL3_LEVEL, EffectType.AllDamage, 0.3);
        public readonly static HeroPassiveSkillVG Hero009Skill004 =
            new HeroPassiveSkillVG("HE009_skill_4",
                                   Hero009, HERO_SKILL4_LEVEL, EffectType.CriticalDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero009Skill005 =
            new HeroPassiveSkillVG("HE009_skill_5",
                                   Hero009, HERO_SKILL5_LEVEL, EffectType.HeroDamage, 50);
        public readonly static HeroPassiveSkillVG Hero009Skill006 =
            new HeroPassiveSkillVG("HE009_skill_6",
                                   Hero009, HERO_SKILL6_LEVEL, EffectType.AllDamage, 0.25);
        public readonly static HeroPassiveSkillVG Hero009Skill007 =
            new HeroPassiveSkillVG("HE009_skill_7",
                                   Hero009, HERO_SKILL7_LEVEL, EffectType.HeroDamage, 100);
        // --------------- Hero 010 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero010Skill001 =
            new HeroPassiveSkillVG("HE010_skill_1",
                                   Hero010, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 1.5);
        public readonly static HeroPassiveSkillVG Hero010Skill002 =
            new HeroPassiveSkillVG("HE010_skill_2",
                                   Hero010, HERO_SKILL2_LEVEL, EffectType.CriticalDamage, 0.01);
        public readonly static HeroPassiveSkillVG Hero010Skill003 =
            new HeroPassiveSkillVG("HE010_skill_3",
                                   Hero010, HERO_SKILL3_LEVEL, EffectType.BossDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero010Skill004 =
            new HeroPassiveSkillVG("HE010_skill_4",
                                   Hero010, HERO_SKILL4_LEVEL, EffectType.GoldDropAmount, 0.15);
        public readonly static HeroPassiveSkillVG Hero010Skill005 =
            new HeroPassiveSkillVG("HE010_skill_5",
                                   Hero010, HERO_SKILL5_LEVEL, EffectType.TreasureChestGoldAmount, 0.2);
        public readonly static HeroPassiveSkillVG Hero010Skill006 =
            new HeroPassiveSkillVG("HE010_skill_6",
                                   Hero010, HERO_SKILL6_LEVEL, EffectType.TreasureChestGoldAmount, 0.25);
        public readonly static HeroPassiveSkillVG Hero010Skill007 =
            new HeroPassiveSkillVG("HE010_skill_7",
                                   Hero010, HERO_SKILL7_LEVEL, EffectType.AllDamage, 0.15);

        // --------------- Hero 011 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero011Skill001 =
            new HeroPassiveSkillVG("HE011_skill_1",
                                   Hero011, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 2);
        public readonly static HeroPassiveSkillVG Hero011Skill002 =
            new HeroPassiveSkillVG("HE011_skill_2",
                                   Hero011, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 8.5);
        public readonly static HeroPassiveSkillVG Hero011Skill003 =
            new HeroPassiveSkillVG("HE011_skill_3",
                                   Hero011, HERO_SKILL3_LEVEL, EffectType.TapDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero011Skill004 =
            new HeroPassiveSkillVG("HE011_skill_4",
                                   Hero011, HERO_SKILL4_LEVEL, EffectType.TotalDPSTapDamage, 0.004);
        public readonly static HeroPassiveSkillVG Hero011Skill005 =
            new HeroPassiveSkillVG("HE011_skill_5",
                                   Hero011, HERO_SKILL5_LEVEL, EffectType.GoldDropAmount, 0.15);
        public readonly static HeroPassiveSkillVG Hero011Skill006 =
            new HeroPassiveSkillVG("HE011_skill_6",
                                   Hero011, HERO_SKILL6_LEVEL, EffectType.CriticalChance, 0.01);
        public readonly static HeroPassiveSkillVG Hero011Skill007 =
            new HeroPassiveSkillVG("HE011_skill_7",
                                   Hero011, HERO_SKILL7_LEVEL, EffectType.HeroDamage, 38);

        // --------------- Hero 012 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero012Skill001 =
            new HeroPassiveSkillVG("HE012_skill_1",
                                   Hero012, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 2.5);
        public readonly static HeroPassiveSkillVG Hero012Skill002 =
            new HeroPassiveSkillVG("HE012_skill_2",
                                   Hero012, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 13);
        public readonly static HeroPassiveSkillVG Hero012Skill003 =
            new HeroPassiveSkillVG("HE012_skill_3",
                                   Hero012, HERO_SKILL3_LEVEL, EffectType.BossDamage, 0.07);
        public readonly static HeroPassiveSkillVG Hero012Skill004 =
            new HeroPassiveSkillVG("HE012_skill_4",
                                   Hero012, HERO_SKILL4_LEVEL, EffectType.CriticalDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero012Skill005 =
            new HeroPassiveSkillVG("HE012_skill_5",
                                   Hero012, HERO_SKILL5_LEVEL, EffectType.TotalDPSTapDamage, 0.004);
        public readonly static HeroPassiveSkillVG Hero012Skill006 =
            new HeroPassiveSkillVG("HE012_skill_6",
                                   Hero012, HERO_SKILL6_LEVEL, EffectType.TapDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero012Skill007 =
            new HeroPassiveSkillVG("HE012_skill_7",
                                   Hero012, HERO_SKILL7_LEVEL, EffectType.GoldDropAmount, 0.2);

        // --------------- Hero 013 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero013Skill001 =
            new HeroPassiveSkillVG("HE013_skill_1",
                                   Hero013, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 1.5);
        public readonly static HeroPassiveSkillVG Hero013Skill002 =
            new HeroPassiveSkillVG("HE013_skill_2",
                                   Hero013, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 8.5);
        public readonly static HeroPassiveSkillVG Hero013Skill003 =
            new HeroPassiveSkillVG("HE013_skill_3",
                                   Hero013, HERO_SKILL3_LEVEL, EffectType.TapDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero013Skill004 =
            new HeroPassiveSkillVG("HE013_skill_4",
                                   Hero013, HERO_SKILL4_LEVEL, EffectType.AllDamage, 0.2);
        public readonly static HeroPassiveSkillVG Hero013Skill005 =
            new HeroPassiveSkillVG("HE013_skill_5",
                                   Hero013, HERO_SKILL5_LEVEL, EffectType.AllDamage, 0.3);
        public readonly static HeroPassiveSkillVG Hero013Skill006 =
            new HeroPassiveSkillVG("HE013_skill_6",
                                   Hero013, HERO_SKILL6_LEVEL, EffectType.CriticalDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero013Skill007 =
            new HeroPassiveSkillVG("HE013_skill_7",
                                   Hero013, HERO_SKILL7_LEVEL, EffectType.HeroDamage, 120);

        // --------------- Hero 014 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero014Skill001 =
            new HeroPassiveSkillVG("HE014_skill_1",
                                   Hero014, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 2);
        public readonly static HeroPassiveSkillVG Hero014Skill002 =
            new HeroPassiveSkillVG("HE014_skill_2",
                                   Hero014, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 11);
        public readonly static HeroPassiveSkillVG Hero014Skill003 =
            new HeroPassiveSkillVG("HE014_skill_3",
                                   Hero014, HERO_SKILL3_LEVEL, EffectType.TotalDPSTapDamage, 0.004);
        public readonly static HeroPassiveSkillVG Hero014Skill004 =
            new HeroPassiveSkillVG("HE014_skill_4",
                                   Hero014, HERO_SKILL4_LEVEL, EffectType.HeroDamage, 4);
        public readonly static HeroPassiveSkillVG Hero014Skill005 =
            new HeroPassiveSkillVG("HE014_skill_5",
                                   Hero014, HERO_SKILL5_LEVEL, EffectType.GoldDropAmount, 0.1);
        public readonly static HeroPassiveSkillVG Hero014Skill006 =
            new HeroPassiveSkillVG("HE014_skill_6",
                                   Hero014, HERO_SKILL6_LEVEL, EffectType.CriticalDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero014Skill007 =
            new HeroPassiveSkillVG("HE014_skill_7",
                                   Hero014, HERO_SKILL7_LEVEL, EffectType.GoldDropAmount, 0.2);

        // --------------- Hero 015 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero015Skill001 =
            new HeroPassiveSkillVG("HE015_skill_1",
                                   Hero015, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 3);
        public readonly static HeroPassiveSkillVG Hero015Skill002 =
            new HeroPassiveSkillVG("HE015_skill_2",
                                   Hero015, HERO_SKILL2_LEVEL, EffectType.AllDamage, 0.4);
        public readonly static HeroPassiveSkillVG Hero015Skill003 =
            new HeroPassiveSkillVG("HE015_skill_3",
                                   Hero015, HERO_SKILL3_LEVEL, EffectType.BossDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero015Skill004 =
            new HeroPassiveSkillVG("HE015_skill_4",
                                   Hero015, HERO_SKILL4_LEVEL, EffectType.CriticalChance, 0.02);
        public readonly static HeroPassiveSkillVG Hero015Skill005 =
            new HeroPassiveSkillVG("HE015_skill_5",
                                   Hero015, HERO_SKILL5_LEVEL, EffectType.CriticalDamage, 0.15);
        public readonly static HeroPassiveSkillVG Hero015Skill006 =
            new HeroPassiveSkillVG("HE015_skill_6",
                                   Hero015, HERO_SKILL6_LEVEL, EffectType.TreasureChestGoldAmount, 0.2);
        public readonly static HeroPassiveSkillVG Hero015Skill007 =
            new HeroPassiveSkillVG("HE015_skill_7",
                                   Hero015, HERO_SKILL7_LEVEL, EffectType.HeroDamage, 100);

        // --------------- Hero 016 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero016Skill001 =
            new HeroPassiveSkillVG("HE016_skill_1",
                                   Hero016, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 3.5);
        public readonly static HeroPassiveSkillVG Hero016Skill002 =
            new HeroPassiveSkillVG("HE016_skill_2",
                                   Hero016, HERO_SKILL2_LEVEL, EffectType.TreasureChestGoldAmount, 0.25);
        public readonly static HeroPassiveSkillVG Hero016Skill003 =
            new HeroPassiveSkillVG("HE016_skill_3",
                                   Hero016, HERO_SKILL3_LEVEL, EffectType.GoldDropAmount, 0.2);
        public readonly static HeroPassiveSkillVG Hero016Skill004 =
            new HeroPassiveSkillVG("HE016_skill_4",
                                   Hero016, HERO_SKILL4_LEVEL, EffectType.BossDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero016Skill005 =
            new HeroPassiveSkillVG("HE016_skill_5",
                                   Hero016, HERO_SKILL5_LEVEL, EffectType.BossDamage, 0.07);
        public readonly static HeroPassiveSkillVG Hero016Skill006 =
            new HeroPassiveSkillVG("HE016_skill_6",
                                   Hero016, HERO_SKILL6_LEVEL, EffectType.AllDamage, 0.15);
        public readonly static HeroPassiveSkillVG Hero016Skill007 =
            new HeroPassiveSkillVG("HE016_skill_7",
                                   Hero016, HERO_SKILL7_LEVEL, EffectType.AllDamage, 0.2);

        // --------------- Hero 017 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero017Skill001 =
            new HeroPassiveSkillVG("HE017_skill_1",
                                   Hero017, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 1.5);
        public readonly static HeroPassiveSkillVG Hero017Skill002 =
            new HeroPassiveSkillVG("HE017_skill_2",
                                   Hero017, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 9);
        public readonly static HeroPassiveSkillVG Hero017Skill003 =
            new HeroPassiveSkillVG("HE017_skill_3",
                                   Hero017, HERO_SKILL3_LEVEL, EffectType.GoldDropAmount, 0.1);
        public readonly static HeroPassiveSkillVG Hero017Skill004 =
            new HeroPassiveSkillVG("HE017_skill_4",
                                   Hero017, HERO_SKILL4_LEVEL, EffectType.GoldDropAmount, 0.1);
        public readonly static HeroPassiveSkillVG Hero017Skill005 =
            new HeroPassiveSkillVG("HE017_skill_5",
                                   Hero017, HERO_SKILL5_LEVEL, EffectType.TapDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero017Skill006 =
            new HeroPassiveSkillVG("HE017_skill_6",
                                   Hero017, HERO_SKILL6_LEVEL, EffectType.CriticalDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero017Skill007 =
            new HeroPassiveSkillVG("HE017_skill_7",
                                   Hero017, HERO_SKILL7_LEVEL, EffectType.GoldDropAmount, 0.25);

        // --------------- Hero 018 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero018Skill001 =
            new HeroPassiveSkillVG("HE018_skill_1",
                                   Hero018, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 4);
        public readonly static HeroPassiveSkillVG Hero018Skill002 =
            new HeroPassiveSkillVG("HE018_skill_2",
                                   Hero018, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 5);
        public readonly static HeroPassiveSkillVG Hero018Skill003 =
            new HeroPassiveSkillVG("HE018_skill_3",
                                   Hero018, HERO_SKILL3_LEVEL, EffectType.BossDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero018Skill004 =
            new HeroPassiveSkillVG("HE018_skill_4",
                                   Hero018, HERO_SKILL4_LEVEL, EffectType.HeroDamage, 4.5);
        public readonly static HeroPassiveSkillVG Hero018Skill005 =
            new HeroPassiveSkillVG("HE018_skill_5",
                                   Hero018, HERO_SKILL5_LEVEL, EffectType.TapDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero018Skill006 =
            new HeroPassiveSkillVG("HE018_skill_6",
                                   Hero018, HERO_SKILL6_LEVEL, EffectType.TreasureChestGoldAmount, 0.2);
        public readonly static HeroPassiveSkillVG Hero018Skill007 =
            new HeroPassiveSkillVG("HE018_skill_7",
                                   Hero018, HERO_SKILL7_LEVEL, EffectType.AllDamage, 0.15);

        // --------------- Hero 019 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero019Skill001 =
            new HeroPassiveSkillVG("HE019_skill_1",
                                   Hero019, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 2);
        public readonly static HeroPassiveSkillVG Hero019Skill002 =
            new HeroPassiveSkillVG("HE019_skill_2",
                                   Hero019, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 10);
        public readonly static HeroPassiveSkillVG Hero019Skill003 =
            new HeroPassiveSkillVG("HE019_skill_3",
                                   Hero019, HERO_SKILL3_LEVEL, EffectType.TotalDPSTapDamage, 0.005);
        public readonly static HeroPassiveSkillVG Hero019Skill004 =
            new HeroPassiveSkillVG("HE019_skill_4",
                                   Hero019, HERO_SKILL4_LEVEL, EffectType.TapDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero019Skill005 =
            new HeroPassiveSkillVG("HE019_skill_5",
                                   Hero019, HERO_SKILL5_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero019Skill006 =
            new HeroPassiveSkillVG("HE019_skill_6",
                                   Hero019, HERO_SKILL6_LEVEL, EffectType.GoldDropAmount, 0.1);
        public readonly static HeroPassiveSkillVG Hero019Skill007 =
            new HeroPassiveSkillVG("HE019_skill_7",
                                   Hero019, HERO_SKILL7_LEVEL, EffectType.AllDamage, 0.1);

        // --------------- Hero 020 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero020Skill001 =
            new HeroPassiveSkillVG("HE020_skill_1",
                                   Hero020, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 2.5);
        public readonly static HeroPassiveSkillVG Hero020Skill002 =
            new HeroPassiveSkillVG("HE020_skill_2",
                                   Hero020, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 6);
        public readonly static HeroPassiveSkillVG Hero020Skill003 =
            new HeroPassiveSkillVG("HE020_skill_3",
                                   Hero020, HERO_SKILL3_LEVEL, EffectType.CriticalDamage, 0.2);
        public readonly static HeroPassiveSkillVG Hero020Skill004 =
            new HeroPassiveSkillVG("HE020_skill_4",
                                   Hero020, HERO_SKILL4_LEVEL, EffectType.HeroDamage, 4.5);
        public readonly static HeroPassiveSkillVG Hero020Skill005 =
            new HeroPassiveSkillVG("HE020_skill_5",
                                   Hero020, HERO_SKILL5_LEVEL, EffectType.TotalDPSTapDamage, 0.004);
        public readonly static HeroPassiveSkillVG Hero020Skill006 =
            new HeroPassiveSkillVG("HE020_skill_6",
                                   Hero020, HERO_SKILL6_LEVEL, EffectType.TapDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero020Skill007 =
            new HeroPassiveSkillVG("HE020_skill_7",
                                   Hero020, HERO_SKILL7_LEVEL, EffectType.GoldDropAmount, 0.1);

        // --------------- Hero 021 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero021Skill001 =
            new HeroPassiveSkillVG("HE021_skill_1",
                                   Hero021, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 2);
        public readonly static HeroPassiveSkillVG Hero021Skill002 =
            new HeroPassiveSkillVG("HE021_skill_2",
                                   Hero021, HERO_SKILL2_LEVEL, EffectType.TapDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero021Skill003 =
            new HeroPassiveSkillVG("HE021_skill_3",
                                   Hero021, HERO_SKILL3_LEVEL, EffectType.AllDamage, 0.3);
        public readonly static HeroPassiveSkillVG Hero021Skill004 =
            new HeroPassiveSkillVG("HE021_skill_4",
                                   Hero021, HERO_SKILL4_LEVEL, EffectType.CriticalChance, 0.02);
        public readonly static HeroPassiveSkillVG Hero021Skill005 =
            new HeroPassiveSkillVG("HE021_skill_5",
                                   Hero021, HERO_SKILL5_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero021Skill006 =
            new HeroPassiveSkillVG("HE021_skill_6",
                                   Hero021, HERO_SKILL6_LEVEL, EffectType.TreasureChestGoldAmount, 0.2);
        public readonly static HeroPassiveSkillVG Hero021Skill007 =
            new HeroPassiveSkillVG("HE021_skill_7",
                                   Hero021, HERO_SKILL7_LEVEL, EffectType.HeroDamage, 100);

        // --------------- Hero 022 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero022Skill001 =
            new HeroPassiveSkillVG("HE022_skill_1",
                                   Hero022, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 2.5);
        public readonly static HeroPassiveSkillVG Hero022Skill002 =
            new HeroPassiveSkillVG("HE022_skill_2",
                                   Hero022, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 7.5);
        public readonly static HeroPassiveSkillVG Hero022Skill003 =
            new HeroPassiveSkillVG("HE022_skill_3",
                                   Hero022, HERO_SKILL3_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero022Skill004 =
            new HeroPassiveSkillVG("HE022_skill_4",
                                   Hero022, HERO_SKILL4_LEVEL, EffectType.HeroDamage, 5);
        public readonly static HeroPassiveSkillVG Hero022Skill005 =
            new HeroPassiveSkillVG("HE022_skill_5",
                                   Hero022, HERO_SKILL5_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero022Skill006 =
            new HeroPassiveSkillVG("HE022_skill_6",
                                   Hero022, HERO_SKILL6_LEVEL, EffectType.CriticalDamage, 0.3);
        public readonly static HeroPassiveSkillVG Hero022Skill007 =
            new HeroPassiveSkillVG("HE022_skill_7",
                                   Hero022, HERO_SKILL7_LEVEL, EffectType.AllDamage, 0.2);

        // --------------- Hero 023 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero023Skill001 =
            new HeroPassiveSkillVG("HE023_skill_1",
                                   Hero023, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 3);
        public readonly static HeroPassiveSkillVG Hero023Skill002 =
            new HeroPassiveSkillVG("HE023_skill_2",
                                   Hero023, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 8);
        public readonly static HeroPassiveSkillVG Hero023Skill003 =
            new HeroPassiveSkillVG("HE023_skill_3",
                                   Hero023, HERO_SKILL3_LEVEL, EffectType.TotalDPSTapDamage, 0.004);
        public readonly static HeroPassiveSkillVG Hero023Skill004 =
            new HeroPassiveSkillVG("HE023_skill_4",
                                   Hero023, HERO_SKILL4_LEVEL, EffectType.CriticalDamage, 0.2);
        public readonly static HeroPassiveSkillVG Hero023Skill005 =
            new HeroPassiveSkillVG("HE023_skill_5",
                                   Hero023, HERO_SKILL5_LEVEL, EffectType.TapDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero023Skill006 =
            new HeroPassiveSkillVG("HE023_skill_6",
                                   Hero023, HERO_SKILL6_LEVEL, EffectType.CriticalChance, 0.02);
        public readonly static HeroPassiveSkillVG Hero023Skill007 =
            new HeroPassiveSkillVG("HE023_skill_7",
                                   Hero023, HERO_SKILL7_LEVEL, EffectType.HeroDamage, 100);

        // --------------- Hero 024 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero024Skill001 =
            new HeroPassiveSkillVG("HE024_skill_1",
                                   Hero024, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 2);
        public readonly static HeroPassiveSkillVG Hero024Skill002 =
            new HeroPassiveSkillVG("HE024_skill_2",
                                   Hero024, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 5);
        public readonly static HeroPassiveSkillVG Hero024Skill003 =
            new HeroPassiveSkillVG("HE024_skill_3",
                                   Hero024, HERO_SKILL3_LEVEL, EffectType.HeroDamage, 12);
        public readonly static HeroPassiveSkillVG Hero024Skill004 =
            new HeroPassiveSkillVG("HE024_skill_4",
                                   Hero024, HERO_SKILL4_LEVEL, EffectType.GoldDropAmount, 0.15);
        public readonly static HeroPassiveSkillVG Hero024Skill005 =
            new HeroPassiveSkillVG("HE024_skill_5",
                                   Hero024, HERO_SKILL5_LEVEL, EffectType.TreasureChestGoldAmount, 0.2);
        public readonly static HeroPassiveSkillVG Hero024Skill006 =
            new HeroPassiveSkillVG("HE024_skill_6",
                                   Hero024, HERO_SKILL6_LEVEL, EffectType.HeroDamage, 90);
        public readonly static HeroPassiveSkillVG Hero024Skill007 =
            new HeroPassiveSkillVG("HE024_skill_7",
                                   Hero024, HERO_SKILL7_LEVEL, EffectType.AllDamage, 0.15);

        // --------------- Hero 025 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero025Skill001 =
            new HeroPassiveSkillVG("HE025_skill_1",
                                   Hero025, HERO_SKILL1_LEVEL, EffectType.TapDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero025Skill002 =
            new HeroPassiveSkillVG("HE025_skill_2",
                                   Hero025, HERO_SKILL2_LEVEL, EffectType.TapDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero025Skill003 =
            new HeroPassiveSkillVG("HE025_skill_3",
                                   Hero025, HERO_SKILL3_LEVEL, EffectType.TotalDPSTapDamage, 0.004);
        public readonly static HeroPassiveSkillVG Hero025Skill004 =
            new HeroPassiveSkillVG("HE025_skill_4",
                                   Hero025, HERO_SKILL4_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero025Skill005 =
            new HeroPassiveSkillVG("HE025_skill_5",
                                   Hero025, HERO_SKILL5_LEVEL, EffectType.GoldDropAmount, 0.15);
        public readonly static HeroPassiveSkillVG Hero025Skill006 =
            new HeroPassiveSkillVG("HE025_skill_6",
                                   Hero025, HERO_SKILL6_LEVEL, EffectType.CriticalChance, 0.02);
        public readonly static HeroPassiveSkillVG Hero025Skill007 =
            new HeroPassiveSkillVG("HE025_skill_7",
                                   Hero025, HERO_SKILL7_LEVEL, EffectType.HeroDamage, 150);

        // --------------- Hero 026 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero026Skill001 =
            new HeroPassiveSkillVG("HE026_skill_1",
                                   Hero026, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 3.5);
        public readonly static HeroPassiveSkillVG Hero026Skill002 =
            new HeroPassiveSkillVG("HE026_skill_2",
                                   Hero026, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 6.5);
        public readonly static HeroPassiveSkillVG Hero026Skill003 =
            new HeroPassiveSkillVG("HE026_skill_3",
                                   Hero026, HERO_SKILL3_LEVEL, EffectType.TotalDPSTapDamage, 0.004);
        public readonly static HeroPassiveSkillVG Hero026Skill004 =
            new HeroPassiveSkillVG("HE026_skill_4",
                                   Hero026, HERO_SKILL4_LEVEL, EffectType.BossDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero026Skill005 =
            new HeroPassiveSkillVG("HE026_skill_5",
                                   Hero026, HERO_SKILL5_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero026Skill006 =
            new HeroPassiveSkillVG("HE026_skill_6",
                                   Hero026, HERO_SKILL6_LEVEL, EffectType.BossDamage, 0.05);
        public readonly static HeroPassiveSkillVG Hero026Skill007 =
            new HeroPassiveSkillVG("HE026_skill_7",
                                   Hero026, HERO_SKILL7_LEVEL, EffectType.GoldDropAmount, 0.12);

        // --------------- Hero 027 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero027Skill001 =
            new HeroPassiveSkillVG("HE027_skill_1",
                                   Hero027, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 3);
        public readonly static HeroPassiveSkillVG Hero027Skill002 =
            new HeroPassiveSkillVG("HE027_skill_2",
                                   Hero027, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 7);
        public readonly static HeroPassiveSkillVG Hero027Skill003 =
            new HeroPassiveSkillVG("HE027_skill_3",
                                   Hero027, HERO_SKILL3_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero027Skill004 =
            new HeroPassiveSkillVG("HE027_skill_4",
                                   Hero027, HERO_SKILL4_LEVEL, EffectType.BossDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero027Skill005 =
            new HeroPassiveSkillVG("HE027_skill_5",
                                   Hero027, HERO_SKILL5_LEVEL, EffectType.CriticalChance, 0.02);
        public readonly static HeroPassiveSkillVG Hero027Skill006 =
            new HeroPassiveSkillVG("HE027_skill_6",
                                   Hero027, HERO_SKILL6_LEVEL, EffectType.CriticalDamage, 0.3);
        public readonly static HeroPassiveSkillVG Hero027Skill007 =
            new HeroPassiveSkillVG("HE027_skill_7",
                                   Hero027, HERO_SKILL7_LEVEL, EffectType.TreasureChestGoldAmount, 0.2);

        // --------------- Hero 028 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero028Skill001 =
            new HeroPassiveSkillVG("HE028_skill_1",
                                   Hero028, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 3.5);
        public readonly static HeroPassiveSkillVG Hero028Skill002 =
            new HeroPassiveSkillVG("HE028_skill_2",
                                   Hero028, HERO_SKILL2_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero028Skill003 =
            new HeroPassiveSkillVG("HE028_skill_3",
                                   Hero028, HERO_SKILL3_LEVEL, EffectType.HeroDamage, 4);
        public readonly static HeroPassiveSkillVG Hero028Skill004 =
            new HeroPassiveSkillVG("HE028_skill_4",
                                   Hero028, HERO_SKILL4_LEVEL, EffectType.HeroDamage, 6);
        public readonly static HeroPassiveSkillVG Hero028Skill005 =
            new HeroPassiveSkillVG("HE028_skill_5",
                                   Hero028, HERO_SKILL5_LEVEL, EffectType.CriticalDamage, 0.2);
        public readonly static HeroPassiveSkillVG Hero028Skill006 =
            new HeroPassiveSkillVG("HE028_skill_6",
                                   Hero028, HERO_SKILL6_LEVEL, EffectType.CriticalChance, 0.03);
        public readonly static HeroPassiveSkillVG Hero028Skill007 =
            new HeroPassiveSkillVG("HE028_skill_7",
                                   Hero028, HERO_SKILL7_LEVEL, EffectType.AllDamage, 0.15);

        // --------------- Hero 029 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero029Skill001 =
            new HeroPassiveSkillVG("HE029_skill_1",
                                   Hero029, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 3.3);
        public readonly static HeroPassiveSkillVG Hero029Skill002 =
            new HeroPassiveSkillVG("HE029_skill_2",
                                   Hero029, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 5.5);
        public readonly static HeroPassiveSkillVG Hero029Skill003 =
            new HeroPassiveSkillVG("HE029_skill_3",
                                   Hero029, HERO_SKILL3_LEVEL, EffectType.GoldDropAmount, 0.1);
        public readonly static HeroPassiveSkillVG Hero029Skill004 =
            new HeroPassiveSkillVG("HE029_skill_4",
                                   Hero029, HERO_SKILL4_LEVEL, EffectType.TapDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero029Skill005 =
            new HeroPassiveSkillVG("HE029_skill_5",
                                   Hero029, HERO_SKILL5_LEVEL, EffectType.GoldDropAmount, 0.2);
        public readonly static HeroPassiveSkillVG Hero029Skill006 =
            new HeroPassiveSkillVG("HE029_skill_6",
                                   Hero029, HERO_SKILL6_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero029Skill007 =
            new HeroPassiveSkillVG("HE029_skill_7",
                                   Hero029, HERO_SKILL7_LEVEL, EffectType.GoldDropAmount, 0.3);

        // --------------- Hero 030 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero030Skill001 =
            new HeroPassiveSkillVG("HE030_skill_1",
                                   Hero030, HERO_SKILL1_LEVEL, EffectType.CriticalDamage, 0.4);
        public readonly static HeroPassiveSkillVG Hero030Skill002 =
            new HeroPassiveSkillVG("HE030_skill_2",
                                   Hero030, HERO_SKILL2_LEVEL, EffectType.TapDamage, 0.2);
        public readonly static HeroPassiveSkillVG Hero030Skill003 =
            new HeroPassiveSkillVG("HE030_skill_3",
                                   Hero030, HERO_SKILL3_LEVEL, EffectType.TotalDPSTapDamage, 0.01);
        public readonly static HeroPassiveSkillVG Hero030Skill004 =
            new HeroPassiveSkillVG("HE030_skill_4",
                                   Hero030, HERO_SKILL4_LEVEL, EffectType.GoldDropAmount, 0.6);
        public readonly static HeroPassiveSkillVG Hero030Skill005 =
            new HeroPassiveSkillVG("HE030_skill_5",
                                   Hero030, HERO_SKILL5_LEVEL, EffectType.AllDamage, 0.2);
        public readonly static HeroPassiveSkillVG Hero030Skill006 =
            new HeroPassiveSkillVG("HE030_skill_6",
                                   Hero030, HERO_SKILL6_LEVEL, EffectType.AllDamage, 0.3);
        public readonly static HeroPassiveSkillVG Hero030Skill007 =
            new HeroPassiveSkillVG("HE030_skill_7",
                                   Hero030, HERO_SKILL7_LEVEL, EffectType.AllDamage, 0.4);

        // --------------- Hero 031 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero031Skill001 =
            new HeroPassiveSkillVG("HE031_skill_1",
                                   Hero031, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 9);
        public readonly static HeroPassiveSkillVG Hero031Skill002 =
            new HeroPassiveSkillVG("HE031_skill_2",
                                   Hero031, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 20);
        public readonly static HeroPassiveSkillVG Hero031Skill003 =
            new HeroPassiveSkillVG("HE031_skill_3",
                                   Hero031, HERO_SKILL3_LEVEL, EffectType.CriticalChance, 0.01);
        public readonly static HeroPassiveSkillVG Hero031Skill004 =
            new HeroPassiveSkillVG("HE031_skill_4",
                                   Hero031, HERO_SKILL4_LEVEL, EffectType.TapDamage, 0.6);
        public readonly static HeroPassiveSkillVG Hero031Skill005 =
            new HeroPassiveSkillVG("HE031_skill_5",
                                   Hero031, HERO_SKILL5_LEVEL, EffectType.TreasureChestGoldAmount, 0.25);
        public readonly static HeroPassiveSkillVG Hero031Skill006 =
            new HeroPassiveSkillVG("HE031_skill_6",
                                   Hero031, HERO_SKILL6_LEVEL, EffectType.AllDamage, 0.1);
        public readonly static HeroPassiveSkillVG Hero031Skill007 =
            new HeroPassiveSkillVG("HE031_skill_7",
                                   Hero031, HERO_SKILL7_LEVEL, EffectType.GoldDropAmount, 0.15);

        // --------------- Hero 032 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero032Skill001 =
            new HeroPassiveSkillVG("HE032_skill_1",
                                   Hero032, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 0.4);
        public readonly static HeroPassiveSkillVG Hero032Skill002 =
            new HeroPassiveSkillVG("HE032_skill_2",
                                   Hero032, HERO_SKILL2_LEVEL, EffectType.HeroDamage, 0.2);
        public readonly static HeroPassiveSkillVG Hero032Skill003 =
            new HeroPassiveSkillVG("HE032_skill_3",
                                   Hero032, HERO_SKILL3_LEVEL, EffectType.GoldDropAmount, 0.25);
        public readonly static HeroPassiveSkillVG Hero032Skill004 =
            new HeroPassiveSkillVG("HE032_skill_4",
                                   Hero032, HERO_SKILL4_LEVEL, EffectType.TapDamage, 0.6);
        public readonly static HeroPassiveSkillVG Hero032Skill005 =
            new HeroPassiveSkillVG("HE032_skill_5",
                                   Hero032, HERO_SKILL5_LEVEL, EffectType.CriticalChance, 0.02);
        public readonly static HeroPassiveSkillVG Hero032Skill006 =
            new HeroPassiveSkillVG("HE032_skill_6",
                                   Hero032, HERO_SKILL6_LEVEL, EffectType.AllDamage, 0.3);
        public readonly static HeroPassiveSkillVG Hero032Skill007 =
            new HeroPassiveSkillVG("HE032_skill_7",
                                   Hero032, HERO_SKILL7_LEVEL, EffectType.BossDamage, 0.1);

        // --------------- Hero 033 Skills ------------------- //
        public readonly static HeroPassiveSkillVG Hero033Skill001 =
            new HeroPassiveSkillVG("HE033_skill_1",
                                   Hero033, HERO_SKILL1_LEVEL, EffectType.HeroDamage, 20);
        public readonly static HeroPassiveSkillVG Hero033Skill002 =
            new HeroPassiveSkillVG("HE033_skill_2",
                                   Hero033, HERO_SKILL2_LEVEL, EffectType.TapDamage, 0.2);
        public readonly static HeroPassiveSkillVG Hero033Skill003 =
            new HeroPassiveSkillVG("HE033_skill_3",
                                   Hero033, HERO_SKILL3_LEVEL, EffectType.TotalDPSTapDamage, 0.01);
        public readonly static HeroPassiveSkillVG Hero033Skill004 =
            new HeroPassiveSkillVG("HE033_skill_4",
                                   Hero033, HERO_SKILL4_LEVEL, EffectType.GoldDropAmount, 0.25);
        public readonly static HeroPassiveSkillVG Hero033Skill005 =
            new HeroPassiveSkillVG("HE033_skill_5",
                                   Hero033, HERO_SKILL5_LEVEL, EffectType.AllDamage, 0.2);
        public readonly static HeroPassiveSkillVG Hero033Skill006 =
            new HeroPassiveSkillVG("HE033_skill_6",
                                   Hero033, HERO_SKILL6_LEVEL, EffectType.AllDamage, 0.3);
        public readonly static HeroPassiveSkillVG Hero033Skill007 =
            new HeroPassiveSkillVG("HE033_skill_7",
                                   Hero033, HERO_SKILL7_LEVEL, EffectType.AllDamage, 0.4);

        public readonly static IList<HeroPassiveSkillVG> HeroSkills = new List<HeroPassiveSkillVG> {
			Hero001Skill001, Hero001Skill002, Hero001Skill003, Hero001Skill004, Hero001Skill005, Hero001Skill006, Hero001Skill007,
			Hero002Skill001, Hero002Skill002, Hero002Skill003, Hero002Skill004, Hero002Skill005, Hero002Skill006, Hero002Skill007,
			Hero003Skill001, Hero003Skill002, Hero003Skill003, Hero003Skill004, Hero003Skill005, Hero003Skill006, Hero003Skill007,
			Hero004Skill001, Hero004Skill002, Hero004Skill003, Hero004Skill004, Hero004Skill005, Hero004Skill006, Hero004Skill007,
			Hero005Skill001, Hero005Skill002, Hero005Skill003, Hero005Skill004, Hero005Skill005, Hero005Skill006, Hero005Skill007,
			Hero006Skill001, Hero006Skill002, Hero006Skill003, Hero006Skill004, Hero006Skill005, Hero006Skill006, Hero006Skill007,
			Hero007Skill001, Hero007Skill002, Hero007Skill003, Hero007Skill004, Hero007Skill005, Hero007Skill006, Hero007Skill007,
			Hero008Skill001, Hero008Skill002, Hero008Skill003, Hero008Skill004, Hero008Skill005, Hero008Skill006, Hero008Skill007,
			Hero009Skill001, Hero009Skill002, Hero009Skill003, Hero009Skill004, Hero009Skill005, Hero009Skill006, Hero009Skill007,
			Hero010Skill001, Hero010Skill002, Hero010Skill003, Hero010Skill004, Hero010Skill005, Hero010Skill006, Hero010Skill007,
			Hero011Skill001, Hero011Skill002, Hero011Skill003, Hero011Skill004, Hero011Skill005, Hero011Skill006, Hero011Skill007,
			Hero012Skill001, Hero012Skill002, Hero012Skill003, Hero012Skill004, Hero012Skill005, Hero012Skill006, Hero012Skill007,
			Hero013Skill001, Hero013Skill002, Hero013Skill003, Hero013Skill004, Hero013Skill005, Hero013Skill006, Hero013Skill007,
			Hero014Skill001, Hero014Skill002, Hero014Skill003, Hero014Skill004, Hero014Skill005, Hero014Skill006, Hero014Skill007,
			Hero015Skill001, Hero015Skill002, Hero015Skill003, Hero015Skill004, Hero015Skill005, Hero015Skill006, Hero015Skill007,
			Hero016Skill001, Hero016Skill002, Hero016Skill003, Hero016Skill004, Hero016Skill005, Hero016Skill006, Hero016Skill007,
			Hero017Skill001, Hero017Skill002, Hero017Skill003, Hero017Skill004, Hero017Skill005, Hero017Skill006, Hero017Skill007,
			Hero018Skill001, Hero018Skill002, Hero018Skill003, Hero018Skill004, Hero018Skill005, Hero018Skill006, Hero018Skill007,
			Hero019Skill001, Hero019Skill002, Hero019Skill003, Hero019Skill004, Hero019Skill005, Hero019Skill006, Hero019Skill007,
			Hero020Skill001, Hero020Skill002, Hero020Skill003, Hero020Skill004, Hero020Skill005, Hero020Skill006, Hero020Skill007,
			Hero021Skill001, Hero021Skill002, Hero021Skill003, Hero021Skill004, Hero021Skill005, Hero021Skill006, Hero021Skill007,
			Hero022Skill001, Hero022Skill002, Hero022Skill003, Hero022Skill004, Hero022Skill005, Hero022Skill006, Hero022Skill007,
			Hero023Skill001, Hero023Skill002, Hero023Skill003, Hero023Skill004, Hero023Skill005, Hero023Skill006, Hero023Skill007,
			Hero024Skill001, Hero024Skill002, Hero024Skill003, Hero024Skill004, Hero024Skill005, Hero024Skill006, Hero024Skill007,
			Hero025Skill001, Hero025Skill002, Hero025Skill003, Hero025Skill004, Hero025Skill005, Hero025Skill006, Hero025Skill007,
			Hero026Skill001, Hero026Skill002, Hero026Skill003, Hero026Skill004, Hero026Skill005, Hero026Skill006, Hero026Skill007,
			Hero027Skill001, Hero027Skill002, Hero027Skill003, Hero027Skill004, Hero027Skill005, Hero027Skill006, Hero027Skill007,
			Hero028Skill001, Hero028Skill002, Hero028Skill003, Hero028Skill004, Hero028Skill005, Hero028Skill006, Hero028Skill007,
			Hero029Skill001, Hero029Skill002, Hero029Skill003, Hero029Skill004, Hero029Skill005, Hero029Skill006, Hero029Skill007,
			Hero030Skill001, Hero030Skill002, Hero030Skill003, Hero030Skill004, Hero030Skill005, Hero030Skill006, Hero030Skill007,
			Hero031Skill001, Hero031Skill002, Hero031Skill003, Hero031Skill004, Hero031Skill005, Hero031Skill006, Hero031Skill007,
			Hero032Skill001, Hero032Skill002, Hero032Skill003, Hero032Skill004, Hero032Skill005, Hero032Skill006, Hero032Skill007,
			Hero033Skill001, Hero033Skill002, Hero033Skill003, Hero033Skill004, Hero033Skill005, Hero033Skill006, Hero033Skill007
		};

        public readonly static Dictionary<string, IList<HeroPassiveSkillVG>> HeroSkillsByHeroId
            = new Dictionary<string, IList<HeroPassiveSkillVG>>();
        public readonly static Dictionary<EffectType, IList<HeroPassiveSkillVG>> HeroSkillsByEffectType
            = new Dictionary<EffectType, IList<HeroPassiveSkillVG>>();


        static HeroAssets()
        {
            foreach (HeroPassiveSkillVG s in HeroSkills)
            {
                // Construct Hero Skill by Hero ID Dictionary
                string heroId = s.Character.ItemId;
                IList<HeroPassiveSkillVG> skillByHeroList;
                if (HeroSkillsByHeroId.ContainsKey(heroId))
                {
                    skillByHeroList = HeroSkillsByHeroId[heroId];
                }
                else
                {
                    skillByHeroList = new List<HeroPassiveSkillVG>();
                    HeroSkillsByHeroId.Add(heroId, skillByHeroList);
                }
                skillByHeroList.Add(s);

                // Construct Hero Skill by Type Dictionary
                IList<HeroPassiveSkillVG> skillByTypeList;
                if (HeroSkillsByEffectType.ContainsKey(s.EffectType))
                {
                    skillByTypeList = HeroSkillsByEffectType[s.EffectType];
                }
                else
                {
                    skillByTypeList = new List<HeroPassiveSkillVG>();
                    HeroSkillsByEffectType.Add(s.EffectType, skillByTypeList);
                }
                skillByTypeList.Add(s);
            }
        }

    }
}