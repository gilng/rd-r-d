using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class CriticalStrikeASVG : AvatarActiveSkillVG
    {
        private static readonly double BASE_COST = 143000000;
        private static readonly double MULTIPLIER = 25000;

        public CriticalStrikeASVG(CharacterLevelVG character) :
            base("active_skill_3", character, 200, EffectType.AvatarActiveSkill, 1800, 30)
        {
        }

        /// <summary>
        /// Return the buff percentage
        /// </summary>
        public override double EffectValue
        {
            get
            {
                return 0.03 * (GetBalance() - 1) + 0.17;
            }
        }

        public override double PriceForUpgradingToLevel(int lv)
        {
            return Formulas.GoldToUpgradeAvatarActiveSkillForLevel(lv - 1, BASE_COST, MULTIPLIER, ArtifactAssets.Artifact024.EffectValue);
        }
    }
}
