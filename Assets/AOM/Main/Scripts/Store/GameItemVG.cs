﻿using UnityEngine;
using System;
using System.Collections;

using Soomla.Store;
using UnityEngine.Analytics;
using System.Collections.Generic;

namespace Ignite.AOM
{

    public class GameItemVG : AOMCachedBalanceVG
    {
        //		protected int priceForOne = -1;
        protected int price = -1;

        public int Price
        {
            get { return this.price; }
        }

        public GameItemVG(string itemId, int price)
            : base("", "", itemId, new PurchaseWithVirtualItem(AOMStoreConstant.DIAMOND_CURRENCY_ID, 0))
        {
            this.price = price;
            //			RefreshPrice(true);
        }

        public override int ResetBalance(int balance, bool notify)
        {
            int result = base.ResetBalance(balance, notify);
            if (notify)
            {
                EventManager.GameItemVGBalabceChange();
            }
            return result;
        }

        public override int Give(int amount, bool notify)
        {
            int balance = base.Give(amount, notify);
            if (notify)
            {
                EventManager.GameItemVGBalabceChange();
            }
            //			RefreshPrice(notify);
            return balance;
        }

        public override int Take(int amount, bool notify)
        {
            int balance = base.Take(amount, notify);
            if (notify)
            {
                EventManager.GameItemVGBalabceChange();
            }
            return balance;
        }

        //		protected virtual int PriceToBuyOne() {
        //			return price;
        ////			return Formulas.GoldToUpgradeArtifactForLevel(GetBalance(), this.priceVectorX, this.priceVectorY);
        //		}

        public virtual bool CanAffordOne()
        {
            bool canAffordOne = false;

            if(UserManager.Instance.CurrentUserSummary != null)
                canAffordOne = Price > 0 && UserManager.Instance.CurrentUserSummary.Gem >= Price;

            return canAffordOne;
        }

        //		protected void RefreshPrice(bool notify) {
        //			priceForOne = PriceToBuyOne();
        //			if(notify)
        //				EventManager.DiamondPriceUpdated();
        //		}

        public virtual void BuyOne(Action successCallback = null)
        {
//            if (CanAffordOne())
//            {
                UserManager.Instance.TakeGems(Price , () =>
                {
                    Give(1);

                    if (successCallback != null)
                    {
                        successCallback();
                    }

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                        Analytics.CustomEvent("useGems", new Dictionary<string, object>
                        {
                            { "type", ItemId },
                        });
#endif
                });

//                AOMStoreInventory.TakeDiamond(Price);
//                Give(1);
//                EventManager.DiamondBalanceUpdated();
//
//                // Analytics
//#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
//                Analytics.CustomEvent("useGems", new Dictionary<string, object>
//                {
//                    { "type", ItemId },
//                });
//#endif
//            }
        }
    }

}