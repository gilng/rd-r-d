using System.Collections.Generic;

namespace Ignite.AOM
{
    public class ArtifactAssets
    {
        public static readonly ArtifactVG Artifact001 = new ArtifactVG("AR001", 2, int.MaxValue, 0.3, 1, 0.7f, 1.5f);
        public static readonly ArtifactVG Artifact002 = new ArtifactVG("AR002", 2, int.MaxValue, 0.25, 0.1, 0.7f, 2f);
        public static readonly ArtifactVG Artifact003 = new ArtifactVG("AR003", 2, 25, 0.15, 0.02, 0.5f, 2f);
        public static readonly ArtifactVG Artifact004 = new ArtifactVG("AR004", 1, 25, 0.15, 0.02, 0.5f, 2f);
        public static readonly ArtifactVG Artifact005 = new ArtifactVG("AR005", 3, 25, 0.15, 0.1, 0.5f, 1.7f);
        public static readonly ArtifactVG Artifact006 = new ArtifactVG("AR006", 1, 10, 0.35, 0.05, 0.4f, 1.5f);
        public static readonly ArtifactVG Artifact007 = new ArtifactVG("AR007", 1, 10, 0.35, 0.05, 0.4f, 1.5f);
        public static readonly ArtifactVG Artifact008 = new ArtifactVG("AR008", 1, 10, 0.6, 0.05, 0.4f, 1.5f);
        public static readonly ArtifactVG Artifact009 = new ArtifactVG("AR009", 1, 10, 0.35, 0.05, 0.7f, 1.5f);
        public static readonly ArtifactVG Artifact010 = new ArtifactVG("AR010", 1, 10, 0.35, 0.05, 0.4f, 1.5f);
        public static readonly ArtifactVG Artifact011 = new ArtifactVG("AR011", 1, 10, 0.35, 0.05, 0.3f, 1.5f);
        public static readonly ArtifactVG Artifact012 = new ArtifactVG("AR012", 1, int.MaxValue, 0.35, 0.1, 0.5f, 1.7f);
        public static readonly ArtifactVG Artifact013 = new ArtifactVG("AR013", 1, int.MaxValue, 0.35, 0.1, 0.5f, 1.7f);
        public static readonly ArtifactVG Artifact014 = new ArtifactVG("AR014", 1, int.MaxValue, 0.6, 0.1, 0.5f, 1.7f);
        public static readonly ArtifactVG Artifact015 = new ArtifactVG("AR015", 1, int.MaxValue, 0.35, 0.1, 0.7f, 1.7f);
        public static readonly ArtifactVG Artifact016 = new ArtifactVG("AR016", 1, int.MaxValue, 0.35, 0.1, 0.5f, 1.7f);
        public static readonly ArtifactVG Artifact017 = new ArtifactVG("AR017", 1, int.MaxValue, 0.15, 0.2, 0.7f, 1.7f);
        public static readonly ArtifactVG Artifact018 = new ArtifactVG("AR018", 2, int.MaxValue, 0.2, 0.2, 1.0f, 1.5f);
        public static readonly ArtifactVG Artifact019 = new ArtifactVG("AR019", 2, int.MaxValue, 0.2, 0.2, 1.0f, 1.5f);
        public static readonly ArtifactVG Artifact020 = new ArtifactVG("AR020", 3, int.MaxValue, 0.15, 0.05, 0.7f, 2.0f);
        public static readonly ArtifactVG Artifact021 = new ArtifactVG("AR021", 2, int.MaxValue, 0.15, 0.005, 0.7f, 1.7f);
        public static readonly ArtifactVG Artifact022 = new ArtifactVG("AR022", 3, int.MaxValue, 0.15, 0.05, 0.7f, 2.0f);
        public static readonly ArtifactVG Artifact023 = new ArtifactVG("AR023", 1, 10, 0.35, 0.05, 1.0f, 2.2f);
        public static readonly ArtifactVG Artifact024 = new ArtifactVG("AR024", 2, 25, 0.15, 0.02, 0.5f, 1.7f);
        public static readonly ArtifactVG Artifact025 = new ArtifactVG("AR025", 3, 5, 1.5, 0.01, 0.6f, 3.0f);
        public static readonly ArtifactVG Artifact026 = new ArtifactVG("AR026", 2, int.MaxValue, 0.05, 0.05, 0.6f, 2.5f);
        public static readonly ArtifactVG Artifact027 = new ArtifactVG("AR027", 1, int.MaxValue, 0.2, 0.15, 0.5f, 1.8f);
        public static readonly ArtifactVG Artifact028 = new ArtifactVG("AR028", 2, 10, 0.35, 0.05, 1.0f, 2.2f);
        public static readonly ArtifactVG Artifact029 = new ArtifactVG("AR029", 1, int.MaxValue, 0.3, 0.02, 0.5f, 1.7f);

        public static readonly ArtifactVG Artifact030 = new ArtifactVG("AR030", 2, 10, 0.35, 0.05, 1, 2.2f);
        public static readonly ArtifactVG Artifact031 = new ArtifactVG("AR031", 1, 5, 2.0, 0.01, 1, 4);
        public static readonly ArtifactVG Artifact032 = new ArtifactVG("AR032", 3, 2, 2.0, 0.01, 250, 2);

		public static readonly ArtifactVG Artifact033 = new ArtifactVG("AR033", 3, 3, 2.5, 0.25, 125, 9, false); // PvP

        public readonly static IList<ArtifactVG> Artifacts = new List<ArtifactVG> { 
            Artifact001, Artifact002, Artifact003, Artifact004, Artifact005, Artifact006, Artifact007, Artifact008, Artifact009, Artifact010, 
            Artifact011, Artifact012, Artifact013, Artifact014, Artifact015, Artifact016, Artifact017, Artifact018, Artifact019, Artifact020,
            Artifact021, Artifact022, Artifact023, Artifact024, Artifact025, Artifact026, Artifact027, Artifact028, Artifact029, 

            Artifact030, Artifact031, Artifact032,

            Artifact033
        };

        // Moetan Lv5 MoeSpirit
        public static readonly ArtifactVG MoeSpirit001 = new ArtifactVG("MS001", 1, 3, 0.8, 2, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit002 = new ArtifactVG("MS002", 1, 3, 0.8, 0.3, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit003 = new ArtifactVG("MS003", 1, 3, 0.8, 0.05, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit004 = new ArtifactVG("MS004", 2, 3, 1.2, 0.2, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit005 = new ArtifactVG("MS005", 1, 3, 0.8, 0.5, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit006 = new ArtifactVG("MS006", 2, 3, 1.2, 5, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit007 = new ArtifactVG("MS007", 1, 3, 0.8, 0.05, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit008 = new ArtifactVG("MS008", 1, 3, 0.8, 0.01, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit009 = new ArtifactVG("MS009", 2, 3, 1.2, 5, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit010 = new ArtifactVG("MS010", 3, 3, 1.8, 2, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit011 = new ArtifactVG("MS011", 2, 3, 1.2, 1, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit012 = new ArtifactVG("MS012", 2, 3, 1.2, 1, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit013 = new ArtifactVG("MS013", 2, 3, 1.2, 0.1, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit014 = new ArtifactVG("MS014", 2, 3, 1.2, 0.5, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit015 = new ArtifactVG("MS015", 2, 3, 1.2, 1, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit016 = new ArtifactVG("MS016", 2, 3, 1.2, 0.25, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit017 = new ArtifactVG("MS017", 2, 3, 1.2, 0.5, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit018 = new ArtifactVG("MS018", 2, 3, 1.2, 0.2, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit019 = new ArtifactVG("MS019", 3, 3, 1.8, 10, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit020 = new ArtifactVG("MS020", 2, 3, 1.2, 0.01, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit021 = new ArtifactVG("MS021", 2, 3, 1.2, 0.01, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit022 = new ArtifactVG("MS022", 2, 3, 1.2, 0.05, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit023 = new ArtifactVG("MS023", 3, 3, 1.8, 0.5, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit024 = new ArtifactVG("MS024", 3, 3, 1.8, 8, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit025 = new ArtifactVG("MS025", 3, 3, 1.8, 0.2, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit026 = new ArtifactVG("MS026", 2, 3, 1.2, 0.3, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit027 = new ArtifactVG("MS027", 3, 3, 1.8, 0.3, 0, 0, false);

        public static readonly ArtifactVG MoeSpirit028 = new ArtifactVG("MS028", 3, 3, 1.8, 0.05, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit029 = new ArtifactVG("MS029", 3, 3, 1.8, 5, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit030 = new ArtifactVG("MS030", 3, 3, 1.8, 0.01, 0, 0, false);

        public static readonly ArtifactVG MoeSpirit097 = new ArtifactVG("MS097", 2, 3, 1.2, 0.003, 0, 0, false);
        public static readonly ArtifactVG MoeSpirit098 = new ArtifactVG("MS098", 3, 3, 1.8, 0.01, 0, 0, false);

        public readonly static IList<ArtifactVG> MoeSpirits = new List<ArtifactVG>
        {
            MoeSpirit001, MoeSpirit002, MoeSpirit003, MoeSpirit004, MoeSpirit005, MoeSpirit006, MoeSpirit007, MoeSpirit008, MoeSpirit009, MoeSpirit010,
            MoeSpirit011, MoeSpirit012, MoeSpirit013, MoeSpirit014, MoeSpirit015, MoeSpirit016, MoeSpirit017, MoeSpirit018, MoeSpirit019, MoeSpirit020,
            MoeSpirit021, MoeSpirit022, MoeSpirit023, MoeSpirit024, MoeSpirit025, MoeSpirit026, MoeSpirit027,

            MoeSpirit097, MoeSpirit098,
            MoeSpirit028, MoeSpirit029, MoeSpirit030
        };


        // string itemId				>>> "AR000"
        // int rareLevel				>>> 1 / 2 / 3
        // int maxLevel					>>> (column F)
        // double baseAllDamageValue	>>> (column J)
        // double baseEffectValue		>>> (column K)
        // float priceVectorX			>>> (column C)
        // float priceVectorY			>>> (column D)
        // bool canGacha                >>> (Default) True
    }

}
