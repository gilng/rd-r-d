﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class ShadowCloneASVG : AvatarActiveSkillVG
    {
        private static readonly double BASE_COST = 104000;
        private static readonly double MULTIPLIER = 1380;

        public ShadowCloneASVG(CharacterLevelVG character) :
            base("active_skill_2", character, 100, EffectType.AvatarActiveSkill, 600, 30)
        {
        }

        /// <summary>
        /// Return the buff percentage
        /// </summary>
        public override double EffectValue
        {
            get
            {
                // May need to add the artifact logic here later
                return DamageFormulas.ActiveSkillShadowCloneTapsPerSecondForSkillLevel(GetBalance());
            }
        }

        public override double PriceForUpgradingToLevel(int lv)
        {
            return Formulas.GoldToUpgradeAvatarActiveSkillForLevel(lv - 1, BASE_COST, MULTIPLIER, ArtifactAssets.Artifact024.EffectValue);
        }
    }
}
