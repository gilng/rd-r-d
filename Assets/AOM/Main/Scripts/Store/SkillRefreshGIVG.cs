﻿using UnityEngine;
using System.Collections;
using System;

namespace Ignite.AOM
{
    public class SkillRefreshGIVG : GameItemVG
    {
        //		protected int cachedPriceForOne = -1;
        private int prevPrice = -1;

        public SkillRefreshGIVG(string itemId, int price)
            : base(itemId, price)
        {
            //			RefreshPrice(true);
        }

        //		protected void RefreshPrice(bool notify) {
        //			priceForOne = PriceToBuyOne();
        //			if(notify && priceForOne!=cachedPriceForOne){
        //				cachedPriceForOne=priceForOne;
        //				EventManager.DiamondPriceUpdated();
        //			}
        //		}

        public void CheckPriceUpdated(float totalCooldownTime)
        {

            float time = totalCooldownTime;
            int newPrice = (int)Math.Ceiling(time / 60) * 10;
            if (prevPrice != newPrice)
            {
                //                Debug.LogFormat ("SkillRefreshGIVG Price Change, Prev: {0}, New: {1}", prevPrice, newPrice);
                price = newPrice;
                prevPrice = newPrice;
                EventManager.DiamondPriceUpdated();
            }
        }
    }
}