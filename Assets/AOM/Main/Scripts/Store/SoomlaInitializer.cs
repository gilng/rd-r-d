using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using Soomla.Store;
using Zenject;

namespace Ignite.AOM
{
    public class SoomlaInitializer : IInitializable
    {
        AOMAssets assets;

        // Use this for initialization
        public void Initialize()
        {
            assets = new AOMAssets();
        }

        public void InitSoomlaStore()
        {
            SoomlaStore.Initialize(assets);
        }
    }

}