﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class AOMStoreConstant
    {
        public const string GOLD_CURRENCY_ID = "CURRENCY_GOLD";
        public const string RELIC_CURRENCY_ID = "CURRENCY_RELIC";
        public const string DIAMOND_CURRENCY_ID = "CURRENCY_DIAMOND";
//        public const string RUBY_CURRENCY_ID = "CURRENCY_RUBY";
    }
}