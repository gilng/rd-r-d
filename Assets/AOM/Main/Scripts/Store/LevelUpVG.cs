using UnityEngine;
using System.Collections;
using Soomla.Store;

namespace Ignite.AOM
{
    public abstract class LevelUpVG : AOMCachedBalanceVG
    {
        private int minLevel;
        private double priceForOne = -1;
        private double priceForTen = -1;
        private bool buyTenEnabled = true;
        private double priceForHundred = -1;
        private bool buyHundredEnabled = true;
        private bool priceIsDirty = false;


        public LevelUpVG(string itemId, int minLevel)
            : this(itemId, minLevel, true, true)
        {
        }


        public LevelUpVG(string itemId, int minLevel, bool buyTenEnabled, bool buyHundredEnabled) : base("", "", itemId,
            new PurchaseWithVirtualItem(AOMStoreConstant.GOLD_CURRENCY_ID, 0))
        {
            this.buyTenEnabled = buyTenEnabled;
            this.buyHundredEnabled = buyHundredEnabled;

            this.minLevel = minLevel;
//            if(GetBalance() < minLevel) {
            // All Level Up  level 1
//                ResetBalance(minLevel);
//            }
            EventManager.OnUpgradeCostReduced += ReduceUpgradCost;
//            StoreEvents.OnGoodBalanceChanged += OnLevelChanged;
//            RefreshPrice(true);
            priceIsDirty = true;
        }

        /// <summary>
        /// To be overriden by subclass to provide the price for upgrading this LevelUpVG to the specified level
        /// </summary>
        public abstract double PriceForUpgradingToLevel(int lv);

        public override int GetBalance()
        {
            if (base.GetBalance() < minLevel)
            {
                ResetBalance(minLevel, false);
            }
            return base.GetBalance();
        }

        public override int ResetBalance(int balance, bool notify)
        {
            if (balance < minLevel)
            {
                // Avatar level should be greater than 0
                balance = minLevel;
            }

            int result = base.ResetBalance(balance, notify);

            if (notify)
            {
                EventManager.LevelUpVGBalanceChange(this, balance);
            }
            RefreshPrice(notify);
            return result;
        }

        private void ReduceUpgradCost()
        {
            priceIsDirty = true;
        }

        public override int Give(int amount, bool notify)
        {
            int balance = base.Give(amount, notify);

            if (notify)
            {
                EventManager.LevelUpVGBalanceChange(this, balance);
            }
            RefreshPrice(notify);
            return balance;
        }

        public override int Take(int amount, bool notify)
        {
            int balance = base.Take(amount, notify);

            if (notify)
            {
                EventManager.LevelUpVGBalanceChange(this, balance);
            }
            RefreshPrice(notify);
            return balance;
        }

        public double PriceForOne
        {
            get
            {
                if (priceIsDirty)
                {
                    RefreshPrice(false);
                    priceIsDirty = false;
                }

                return this.priceForOne;
            }
        }

        public double PriceForTen
        {
            get
            {
                if (priceIsDirty)
                {
                    RefreshPrice(false);
                    priceIsDirty = false;
                }

                return this.priceForTen;
            }
        }

        public double PriceForHundred
        {
            get
            {
                if (priceIsDirty)
                {
                    RefreshPrice(false);
                    priceIsDirty = false;
                }

                return this.priceForHundred;
            }
        }

        public int MinLevel
        {
            get { return this.minLevel; }
        }

        public int MaxLevel
        {
            get { return 1000; }
        }

        public bool CanAffordOne()
        {
            return AOMStoreInventory.GetGoldBalance() >= PriceForOne; // && (GetBalance() + 1 <= MaxLevel);
        }

        public bool CanAffordTen()
        {
            return buyTenEnabled &&
                   AOMStoreInventory.GetGoldBalance() >= PriceForTen; // && (GetBalance() + 10 <= MaxLevel);
        }

        public bool CanAffordHundred()
        {
            return buyHundredEnabled &&
                   AOMStoreInventory.GetGoldBalance() >= PriceForHundred; // && (GetBalance() + 100 <= MaxLevel);
        }

        public void BuyOne()
        {
            if (CanAffordOne())
            {
                AOMStoreInventory.TakeGold(PriceForOne);
                Give(1);
                EventManager.GoldBalanceUpdated();
            }
        }

        //if cant afford ten, will buy mix instead
        public void BuyTen()
        {
            if (CanAffordTen())
            {
                AOMStoreInventory.TakeGold(PriceForTen);
                Give(10);
                EventManager.GoldBalanceUpdated();
            }
            else
            {
                for (int i = 9; i > 0; i--)
                {
                    double price = PriceForNextNLevel(i);
                    if (price <= AOMStoreInventory.GetGoldBalance()) // && (GetBalance() + i <= MaxLevel))
                    {
                        AOMStoreInventory.TakeGold(price);
                        Give(i);
                        EventManager.GoldBalanceUpdated();
                        break;
                    }
                }
            }
        }

        //if cant afford hundred, will buy mix instead
        public void BuyHundred()
        {
            if (CanAffordHundred())
            {
                AOMStoreInventory.TakeGold(PriceForHundred);
                Give(100);
                EventManager.GoldBalanceUpdated();
            }
            else
            {
                for (int i = 99; i > 0; i--)
                {
                    double price = PriceForNextNLevel(i);
                    if (price <= AOMStoreInventory.GetGoldBalance()) // && (GetBalance() + i <= MaxLevel))
                    {
                        AOMStoreInventory.TakeGold(price);
                        Give(i);
                        EventManager.GoldBalanceUpdated();
                        break;
                    }
                }
            }
        }

        protected void RefreshPrice(bool notify)
        {
            priceForOne = PriceForNextNLevel(1);
            if (buyTenEnabled)
            {
                priceForTen = PriceForNextNLevel(10);
            }
            if (buyHundredEnabled)
            {
                priceForHundred = PriceForNextNLevel(100);
            }
//            Debug.Log("Price refreshed");
            if (notify)
                EventManager.GoldPriceUpdated();
        }

        protected double PriceForNextNLevel(int n)
        {
            double cost = 0;
            int currentLv = GetBalance();
            for (int lv = currentLv; lv < currentLv + n; lv++)
            {
                cost += PriceForUpgradingToLevel(lv + 1);
            }

            return cost;
        }
    }
}