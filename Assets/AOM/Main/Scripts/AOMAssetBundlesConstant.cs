﻿using AssetBundles;

public class AOMAssetBundlesConstant
{
    public static readonly string[] ASSET_BUNDLE_LIST = { Utility.GetPlatformName(), "background-bundle", "enemy-bundle", "enemy-bundle_2", "mini_moetan-bundle", "moetan-bundle", "summer_moetan-bundle", "halloween_moetan-bundle", "fukubukuro_moetan-bundle", "uvf_moetan-bundle", "moespirit_icon-bundle" };

    public static string MoetanBundleNameById(int moetanID)
    {
        string moetanBundleName = "moetan-bundle";

        if (moetanID >= 22 && moetanID <= 24)
            moetanBundleName = "summer_moetan-bundle";
        else if (moetanID >= 25 && moetanID <= 27)
            moetanBundleName = "halloween_moetan-bundle";
        else if (moetanID >= 28 && moetanID <= 30)
            moetanBundleName = "fukubukuro_moetan-bundle";
        else if (moetanID >= 97 && moetanID <= 99)
            moetanBundleName = "uvf_moetan-bundle";

        return moetanBundleName;
    }

    public static string EnemyBundleNameById(int enemyID)
    {
        string enemyBundleName = "enemy-bundle";

        if (enemyID >= 19 && enemyID <= 31)
            enemyBundleName = "enemy-bundle_2";

        return enemyBundleName;
    }
}