﻿using System.Text;

namespace Ignite.AOM
{
	public static class AOMStringBuilderExtensions
	{
		public static void Clear(this StringBuilder value)
		{
			value.Length = 0;
			value.Capacity = 0;
		}
	}   
}
