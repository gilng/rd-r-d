﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Ignite.AOM
{
    public class TutorialReceiver : MonoBehaviour
    {
        public int tutorialId;
        public GameObject tutorialNotify_Symbol;

        public bool isTriggered = false;
        public bool isCompleted = false;

        public void TutorialEventSetup(int givenTutorialId)
        {
            tutorialId = givenTutorialId;
            tutorialNotify_Symbol = transform.FindChild("TutorialNotify_Symbol").gameObject;

            if (tutorialId > 0 && tutorialNotify_Symbol != null)
            {
                AddListener();
            }

            isCompleted = TutorialManager.Instance.CheckTutorialCompletedById(tutorialId);

            isTriggered = isCompleted;

            tutorialNotify_Symbol.SetActive(isTriggered && !isCompleted);
        }

        void AddListener()
        {
            EventManager.OnTriggerTutorial += OnTutorialTriggered;
            EventManager.OnTutorialCompleted += OnTutorialFinished;
        }

        void RemoveListener()
        {
            EventManager.OnTriggerTutorial -= OnTutorialTriggered;
            EventManager.OnTutorialCompleted -= OnTutorialFinished;
        }

        void OnTutorialTriggered(int triggeredTutorialId)
        {
            isTriggered = triggeredTutorialId == tutorialId;

            if (tutorialNotify_Symbol != null)
                tutorialNotify_Symbol.SetActive(isTriggered);
        }

        void OnTutorialFinished(int finishedTutorialId)
        {
            if (finishedTutorialId == tutorialId && isTriggered && !isCompleted)
            {
                isCompleted = true;

                if (tutorialNotify_Symbol != null)
                    tutorialNotify_Symbol.SetActive(false);

                RemoveListener();
            }
        }
    }
}