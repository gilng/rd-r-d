﻿namespace Ignite.AOM
{
    public class DiamondDropAnimation : RelicDropAnimation
    {
        protected override void AfterReturn()
        {
            if (dropAmount > 0)
                UserManager.Instance.GiveGems((int) dropAmount, null, false);

            gameObject.SetActive(false);
            Destroy(this);
        }
    }
}