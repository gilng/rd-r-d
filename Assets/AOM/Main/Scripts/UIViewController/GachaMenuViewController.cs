﻿using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class GachaMenuViewController : UIOverlayViewController
    {
        private ArtifactCore _artifactCore;

        [Inject]
        public void ConstructorSafeAttribute(ArtifactCore artifactCore)
        {
            _artifactCore = artifactCore;
        }

        public Button gachaWithMoeCrystalBtn;
        public Text gachaWithMoeCrystalCostLabel;
        public Text currentMoeCrystalsLabel;

        public Button gachaWithMoeGemsBtn;
        public Text gachaWithGemsCostLabel;
        public Text currentGemsLabel;

        int gachaWithGemCost = 280;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            AddListener();

            if (_artifactCore.CanAffordGacha())
            {
                gachaWithMoeCrystalBtn.interactable = true;
                gachaWithMoeCrystalBtn.GetComponent<Image>().sprite =
                    gachaWithMoeCrystalBtn.spriteState.highlightedSprite;
            }
            else
            {
                gachaWithMoeCrystalBtn.interactable = false;
                gachaWithMoeCrystalBtn.GetComponent<Image>().sprite = gachaWithMoeCrystalBtn.spriteState.pressedSprite;
            }

            gachaWithMoeCrystalCostLabel.text = UnitConverter.ConverterDoubleToString(_artifactCore.GachaCost());

            RefreshOwnedMoeCrystalsDisplay();

            gachaWithGemsCostLabel.text = gachaWithGemCost.ToString();

            RefreshOwnedGemsDisplay();
        }

        void AddListener()
        {
            EventManager.OnRelicBalanceUpdated += RefreshOwnedMoeCrystalsDisplay;
            EventManager.OnDiamondBalanceUpdated += RefreshOwnedGemsDisplay;
        }

        void RemoveListener()
        {
            EventManager.OnRelicBalanceUpdated -= RefreshOwnedMoeCrystalsDisplay;
            EventManager.OnDiamondBalanceUpdated -= RefreshOwnedGemsDisplay;
        }

        void RefreshOwnedMoeCrystalsDisplay()
        {
            currentMoeCrystalsLabel.text = string.Format(
                LanguageManager.Instance.GetTextValue("gacha_current_moecrystals"),
                UserManager.Instance.CurrentUserSummary != null ? UnitConverter.ConverterDoubleToString(UserManager.Instance.CurrentUserSummary.Moecrystal) : UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetRelicBalance()));
        }

        void RefreshOwnedGemsDisplay()
        {
            currentGemsLabel.text = string.Format(LanguageManager.Instance.GetTextValue("current_gems"), UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString());
        }

        public void GachaWithGems()
        {
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                if (UserManager.Instance.CurrentUserSummary.Gem >= gachaWithGemCost)
//                {
                    //SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);
                    Gacha(true, gachaWithGemCost);
//                }
//                else
//                {
//                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
//                    LoadOverlayView("ShopMenu", false, false);
//                }
//            }
        }

        public void GachaWithMoeCrystal()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (_artifactCore.CanAffordGacha())
            {
                Gacha(false);
            }
        }

        public void Gacha(bool withDuplicate, int gemsCost = 0)
        {
            _artifactCore.Gacha(withDuplicate, gemsCost, afVG =>
            {
                if (afVG != null)
                {
                    if (HiddenMenu.Instance.artifactMsg.activeSelf)
                        HiddenMenu.Instance.artifactMsg.SetActive(false);

                    if (!HiddenMenu.Instance.artifactScrollView.GetComponent<ScrollRect>().vertical)
                        HiddenMenu.Instance.artifactScrollView.GetComponent<ScrollRect>().vertical = true;

                    GachaResultViewController gachaResultViewController =
                        LoadOverlayViewController("GachaResultMenuView", true,
                            Time.timeScale == 0) as GachaResultViewController;
                    gachaResultViewController.SetItemId(afVG, GachaType.Moetifact);
                }
            });
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            RemoveListener();

            base.CloseView();
        }
    }
}