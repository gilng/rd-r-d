﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class HeroSkillDetailViewController : UIOverlayViewController
	{
        private HeroCore _heroCore;

        [Inject]
        public void ConstructorSafeAttribute(HeroCore heroCore)
        {
            _heroCore = heroCore;
        }

        public Image heroIcon;
        public GameObject heroName;
        public GameObject heroLevel;
        public GameObject heroDPS;
        public GameObject description;
        public List<GameObject> skill;
        private static int heroIdx = -1;
        Sprite[] heroThumbnailIcon, heroSkillIcon;
        string[] heroThumbnailIconName, heroSkillIconName;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            AddListener();
        }

        void AddListener()
        {
            EventManager.OnLanguageChange += RenderSkillDetail;
        }

        void RemoveListener()
        {
            EventManager.OnLanguageChange -= RenderSkillDetail;
        }

        public void SetHero(int idx)
        {
            heroIdx = idx;

            RenderSkillDetail();
        }

        void RenderSkillDetail()
        {
            if (heroThumbnailIcon == null && heroThumbnailIconName == null)
            {
                heroThumbnailIcon = Resources.LoadAll<Sprite>("Heros/Texture/hero_sprite");
                heroThumbnailIconName = new string[heroThumbnailIcon.Length];

                for (int i = 0; i < heroThumbnailIconName.Length; i++)
                {
                    heroThumbnailIconName[i] = heroThumbnailIcon[i].name;
                }
            }

            if (heroSkillIcon == null && heroSkillIconName == null)
            {
                heroSkillIcon = Resources.LoadAll<Sprite>("Sprites/overlay_icon_hero/overlay_icon_hero");
                heroSkillIconName = new string[heroSkillIcon.Length];

                for (int i = 0; i < heroSkillIconName.Length; i++)
                {
                    heroSkillIconName[i] = heroSkillIcon[i].name;
                }
            }

            if (heroIdx > -1)
            {
                string heroId = HeroAssets.Heroes[heroIdx].ItemId;
                int lv = HeroAssets.Heroes[heroIdx].GetBalance();

                heroIcon.GetComponent<Image>().sprite = heroThumbnailIcon[Array.IndexOf(heroThumbnailIconName, heroId.ToLower() + "_01")];

                heroName.GetComponent<Text>().text = LanguageManager.Instance.GetTextValue(heroId + "_name");
                heroLevel.GetComponent<Text>().text = "" + HeroAssets.Heroes[heroIdx].GetBalance();
                heroDPS.GetComponent<Text>().text = UnitConverter.ConverterDoubleToString(_heroCore.HeroDPS[int.Parse(heroId.Replace("HE", "")) - 1]);
                description.GetComponent<Text>().text = LanguageManager.Instance.GetTextValue(heroId + "_description");

                for (int i = 0; i < skill.Count && i < 7; i++)
                {
                    skill[i].transform.FindChild("SkillIcon").gameObject.GetComponent<Image>().sprite = heroSkillIcon[Array.IndexOf(heroSkillIconName, HeroAssets.HeroSkillsByHeroId[heroId][i].EffectType.ToString())];
                    skill[i].transform.FindChild("SkillName").gameObject.GetComponent<Text>().text = LanguageManager.Instance.GetTextValue(heroId + "_skill_" + i + "_name");
                    skill[i].transform.FindChild("SkillDescription").gameObject.GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("effect_type_" + HeroAssets.HeroSkillsByHeroId[heroId][i].EffectType.ToString()) + " " + (HeroAssets.HeroSkillsByHeroId[heroId][i].EffectValue * 100) + "%";

                    if (HeroAssets.HeroSkillsByHeroId.ContainsKey(heroId))
                    {
                        if (HeroAssets.HeroSkillsByHeroId[heroId].Count > i)
                        {
                            HeroPassiveSkillVG heroSkill = HeroAssets.HeroSkillsByHeroId[heroId][i];
                            int targetBalance = lv >= heroSkill.CharacterMinLevel ? 1 : 0;
                            skill[i].transform.Find("UnlockShadow").gameObject.SetActive(heroSkill.GetBalance() < targetBalance || lv < heroSkill.CharacterMinLevel);
                        }
                        else
                        {
                            skill[i].transform.Find("UnlockShadow").gameObject.SetActive(true);
                        }
                    }
                    else
                    {
                        skill[i].transform.Find("UnlockShadow").gameObject.SetActive(true);
                    }
                }
            }
        }

        public override void CloseView()
        {
            RemoveListener();

            base.CloseView();
        }
	}
}
