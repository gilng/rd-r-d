﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;
using System;
using Zenject;

namespace Ignite.AOM
{
    public class StatisticsViewController : UIOverlayViewController
    {
        private AvatarCore _avatarCore;

        [Inject]
        public void ConstructorSafeAttribute(AvatarCore avatarCore)
        {
            _avatarCore = avatarCore;
        }

        // value
        public Text currentTotalHeroLevel;
        public Text currentCriticalTapChance;
        public Text currentDPSMultiplier;
        public Text currentCriticalHitMultiplier;
        public Text currentTotalGoldMultiplier;
        public Text currentTimeSinceLastPrestige;
        public Text totalGold;
        public Text totalHit;
        public Text totalMonsterKill;
        public Text totalBossKill;
        public Text totalCriticals;
        public Text totalChestOpen;
        public Text highestLevel;
        public Text totalPrestige;
        public Text totalGoldLostFromPrestige;
        public Text totalDPSLostFromPrestige;
        public Text daysSinceFirstPlay;
        public Text playTime;
        public Text relics;
        public Text fairies;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            UpdateStatistics();
        }

        void UpdateStatistics()
        {
            //value
            currentTotalHeroLevel.text = ((int)GamePlayStatManager.Instance.Stat[StatType.CurrentTotalHeroLevel]).ToString();
            currentCriticalTapChance.text = _avatarCore.CriticalChance.ToString();
            DateTime lastPreStigeTime = UnitConverter.ConvertToDateTime(GamePlayStatManager.Instance.Stat[StatType.LastPrestigeTime]);
            currentTimeSinceLastPrestige.text = UnitConverter.SecondsToTime((DateTime.Now - lastPreStigeTime).TotalSeconds);
            totalGold.text = UnitConverter.ConverterDoubleToString(GamePlayStatManager.Instance.Stat[StatType.CollectGold]);
            totalHit.text = UnitConverter.ConverterDoubleToString(GamePlayStatManager.Instance.Stat[StatType.Tap]);
            totalMonsterKill.text = UnitConverter.ConverterDoubleToString(GamePlayStatManager.Instance.Stat[StatType.KillMonster]);
            totalBossKill.text = UnitConverter.ConverterDoubleToString(GamePlayStatManager.Instance.Stat[StatType.KillBoss]);
            totalCriticals.text = UnitConverter.ConverterDoubleToString(GamePlayStatManager.Instance.Stat[StatType.GetCriticalHit]);
            totalChestOpen.text = UnitConverter.ConverterDoubleToString(GamePlayStatManager.Instance.Stat[StatType.OpenChest]);
            highestLevel.text = UnitConverter.ConverterDoubleToString(GamePlayStatManager.Instance.Stat[StatType.ReachHighestStage]);
            totalPrestige.text = UnitConverter.ConverterDoubleToString(GamePlayStatManager.Instance.Stat[StatType.Prestige]);
            DateTime firstDate = UnitConverter.ConvertToDateTime(GamePlayStatManager.Instance.Stat[StatType.FirstPlayTime]);
            daysSinceFirstPlay.text = ((int)(DateTime.Now - firstDate).TotalDays).ToString();
            playTime.text = UnitConverter.SecondsToTime(GamePlayStatManager.Instance.Stat[StatType.PlayingDuration]);
            relics.text = UnitConverter.ConverterDoubleToString(GamePlayStatManager.Instance.Stat[StatType.CollectRelic]);
            fairies.text = UnitConverter.ConverterDoubleToString(GamePlayStatManager.Instance.Stat[StatType.GetFairyPresent]);
        }

        public override void CloseView()
        {
            LoadOverlayView("SettingMainMenuView", false, Time.timeScale == 0);

            base.CloseView();
        }
    }
}
