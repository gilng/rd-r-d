﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using Zenject;

namespace Ignite.AOM
{
    public class UIPageViewController : UIViewController
    {
        public string parentPageViewName = "";
        public string previousPageViewName = "";

        // Use this for Start
        protected override void Start()
        {
            currentUIViewType = UIViewType.PAGE;

            base.Start();
        }

        // Use this for initialization
        public override void InitView(UIViewController previousUIView = null, bool pause = false)
        {
            base.InitView(previousUIView, pause);

            if (previousUIView != null)
            {
                if (previousUIView.currentUIViewType == UIViewType.PAGE)
                    previousPageViewName = previousUIView.gameObject.name;
            }
        }

        protected virtual void Update()
        {

        }

        public virtual void BackToPreviousPage()
        {
            if (previousPageViewName != "")
            {
                LoadPageView(previousPageViewName);
            }
            else
            {
                if (parentPageViewName != "")
                {
                    LoadPageView(parentPageViewName);
                }
            }

        }

        public override void CloseView()
        {
            base.CloseView();
        }
    }
}
