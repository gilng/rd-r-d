﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class PromotionViewContoller : UIOverlayViewController
	{
	    public List<Sprite> icons;
        public List<GameObject> btns;

        public Image icon;
        public Toggle dontShowToggle;

        int promoteType;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            // 0: rate our game
            // 1: like facebook
            // 2: follow twitter

            List<int> randomList = new List<int>();

            // Rate game
            if (PlayerPrefs.GetInt("DONT_SHOW_RATE_OUR_GAME", 0) == 0)
                randomList.Add(0);

            // Facebook
            if (PlayerPrefs.GetInt("DONT_SHOW_FACEBOOK_LIKED", 0) == 0)
                randomList.Add(1);

            // Twitter
            if (PlayerPrefs.GetInt("DONT_SHOW_TWITTER_FOLLOWED", 0) == 0)
                randomList.Add(2);

            if (randomList.Count > 0)
            {
                promoteType = randomList[UnityEngine.Random.Range(0, randomList.Count)];

                icon.sprite = icons[promoteType];

                for (int i = 0; i < btns.Count; i++)
                {
                    btns[i].SetActive(promoteType == i);
                }
            }

            GameManager.Instance.showingPromotionBoard = true;
        }

        public void PressPromotion()
        {
            switch (promoteType)
            {
                case 0:
                    RateOurGame();
                    break;
                case 1:
                    LikeFB();
                    break;
                case 2:
                    FollowTwitter();
                    break;
            }

            UserManager.Instance.GiveGems(25);

            CloseView();
        }

        void RateOurGame()
        {
#if UNITY_EDITOR
            Application.OpenURL("https://play.google.com/store/apps/details?id=game.ignite.aom");
#elif UNITY_ANDROID
			//https://play.google.com/store/apps/details?id=game.ignite.aom
			Application.OpenURL("market://details?id=game.ignite.aom");
#elif UNITY_IPHONE
			Application.OpenURL("itms-apps://itunes.apple.com/app/id1059991997");
#endif

            PlayerPrefs.SetInt("APP_RATED", 1);
            PlayerPrefs.SetInt("DONT_SHOW_RATE_OUR_GAME", 1);
        }

        void LikeFB()
        {
#if UNITY_EDITOR
            Application.OpenURL("http://facebook.com/aomzh");
#elif UNITY_IPHONE
			float startTime;
			startTime = Time.timeSinceLevelLoad;

			//open the facebook app
			Application.OpenURL("fb://profile/224859004521486");

			if (Time.timeSinceLevelLoad - startTime <= 1f)
			{
			//fail. Open safari.
			Application.OpenURL("http://facebook.com/aomzh");
			}
#elif UNITY_ANDROID
			if(checkPackageAppIsPresent("com.facebook.katana"))
			{
			Application.OpenURL("fb://page/224859004521486");
			}
			else
			{
			Application.OpenURL("http://facebook.com/aomzh");
			}
#endif

            PlayerPrefs.SetInt("FACEBOOK_LIKED", 1);
            PlayerPrefs.SetInt("DONT_SHOW_FACEBOOK_LIKED", 1);
        }

        void FollowTwitter()
        {
#if UNITY_EDITOR
            Application.OpenURL("https://twitter.com/Attack_on_Moe");
#elif UNITY_IPHONE
			float startTime;
			startTime = Time.timeSinceLevelLoad;

			//open the twitter app
			Application.OpenURL("twitter:///user?screen_name=Attack_on_Moe");

			if (Time.timeSinceLevelLoad - startTime <= 1f)
			{
			//fail. Open safari.
			Application.OpenURL("https://twitter.com/Attack_on_Moe");
			}
#elif UNITY_ANDROID
			if(checkPackageAppIsPresent("com.twitter.android"))
			{
			Application.OpenURL("twitter://user?user_id=701743743548977152");
			}
			else
			{
			Application.OpenURL("https://twitter.com/Attack_on_Moe");
			}
#endif

            PlayerPrefs.SetInt("TWITTER_FOLLOWED", 1);
            PlayerPrefs.SetInt("DONT_SHOW_TWITTER_FOLLOWED", 1);
        }

#if UNITY_ANDROID
        private bool checkPackageAppIsPresent(string package)
        {
            AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");

            //take the list of all packages on the device
            AndroidJavaObject appList = packageManager.Call<AndroidJavaObject>("getInstalledPackages", 0);
            int num = appList.Call<int>("size");
            for (int i = 0; i < num; i++)
            {
                AndroidJavaObject appInfo = appList.Call<AndroidJavaObject>("get", i);
                string packageNew = appInfo.Get<string>("packageName");
                if (packageNew.CompareTo(package) == 0)
                {
                    return true;
                }
            }
            return false;
        }
#endif

        public void DontShowNotice()
        {
            switch (promoteType)
            {
                case 0:
                    PlayerPrefs.SetInt("DONT_SHOW_RATE_OUR_GAME", dontShowToggle.isOn ? 1 : 0);
                    break;
                case 1:
                    PlayerPrefs.SetInt("DONT_SHOW_FACEBOOK_LIKED", dontShowToggle.isOn ? 1 : 0);
                    break;
                case 2:
                    PlayerPrefs.SetInt("DONT_SHOW_TWITTER_FOLLOWED", dontShowToggle.isOn ? 1 : 0);
                    break;
            }
        }

        public override void CloseView()
        {
            base.CloseView();

            GameManager.Instance.showingPromotionBoard = false;
        }
	}
}
