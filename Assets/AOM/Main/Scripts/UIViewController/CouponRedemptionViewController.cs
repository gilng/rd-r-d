﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;
using System;
using Zenject;

namespace Ignite.AOM
{
    public class CouponRedemptionViewController : UIOverlayViewController
    {
        private IUserService _userService;

        [Inject]
        public void ConstructorSafeAttribute(IUserService userService)
        {
            _userService = userService;
        }

        public InputField codeField;
        public Text placeHolder_Text;
        public Button confirmButton;

        public GameObject goInit, goLoading, goResult, goReward;

        public Text errorMessage, diamondLabel, relicLabel, otherRewardLabel;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            codeField.text = "";
            placeHolder_Text.text = "";
            diamondLabel.text = "";
            relicLabel.text = "";

            confirmButton.gameObject.SetActive(true);

            goInit.SetActive(true);
            goLoading.SetActive(false);
            goResult.SetActive(false);
        }

        public void ComfirmSetting()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (codeField.text.Length > 0)
            {

                //if (codeField.text == "CCPW:A1B2C3")
                //{
                //    GameObject resetBtn = GameObject.Find("Canvas/Center/SettingMainMenu/Panel/ResetBtn");
                //    resetBtn.SetActive(true);

                //    GameObject changeSetting = GameObject.Find("Canvas/Center/SettingMainMenu/Panel/ChangeSetting");
                //    changeSetting.SetActive(true);
                //}

                StartCoroutine(AOMCouponService.Instance.RedemCoupon(codeField.text, RedeemCouponCallBack));

                confirmButton.gameObject.SetActive(false);
                goLoading.SetActive(true);
            }
        }

        void RedeemCouponCallBack(AOMCouponService.RedeemCouponResult result)
        {
            if (result != null)
            {
                Debug.Log("RedeemCouponCallBack");

                goInit.SetActive(false);
                goLoading.SetActive(false);
                goResult.SetActive(true);

                goReward.SetActive(result.success);
                errorMessage.gameObject.SetActive(!result.success);

                if (result.success)
                {
                    diamondLabel.text = "x" + result.gems;
                    relicLabel.text = "x" + result.relics;
                    otherRewardLabel.text = result.unlockAdFree ? "Ads Free Unlocked" : "";

                    _userService.CurrentUserSummary.Gem += result.gems;
                    _userService.CurrentUserSummary.Moecrystal += result.relics;

                    _userService.UpdateUserSummary(_userService.CurrentUserSummary, (e) =>
                    {
                        if (e != null)
                        {
                            Debug.LogError("Update UserSummary fail");
                        }
                        else
                        {
                            AOMStoreInventory.ResetDiamond(_userService.CurrentUserSummary.Gem);
                            AOMStoreInventory.ResetRelic(_userService.CurrentUserSummary.Moecrystal);
                        }
                    });

                    // Cloas Ad
                    if (result.unlockAdFree)
                    {
                        AdMobManager.Instance.ShouldNoADs = true;
                    }
                }
                else
                {
                    errorMessage.text = LanguageManager.Instance.GetTextValue(result.error);
                }
            }
            else
            {
                Debug.Log("RedeemCouponCallBack: fail");

                goInit.SetActive(false);
                goLoading.SetActive(false);
                goResult.SetActive(true);
                goReward.SetActive(false);

                errorMessage.gameObject.SetActive(true);

                errorMessage.text = "Server disconnect";
            }
        }

        public override void CloseView()
        {
            LoadOverlayView("SettingMainMenuView", false, true);

            base.CloseView();
        }

    }
}
