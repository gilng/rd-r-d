﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;

namespace Ignite.AOM
{
    public class StoryIntroViewController : UIOverlayViewController
    {
        int currentStep;
        int currentScene;
        bool allowNextStep;
        int storyIntro_string_currentNum;
        string[] storyIntro_string = {
			"story_intro_1a",
			"story_intro_1b",
			"story_intro_2",
			"story_intro_3",
			"story_intro_4a",
			"story_intro_4b"
		};
        int storyIntro_open_item_step;
        public Animator animator;
        public Text storyText;
        public List<GameObject> scene;
        public GameObject scene4_item, stepBtn, fadeObj, bg;
        public string currentPlayerName;
        public InputField nameField;
        public Text placeHolder_Text;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            bg.SetActive(true);

            stepBtn.SetActive(true);
            fadeObj.SetActive(true);

            storyText.text = "";
            storyText.gameObject.SetActive(true);

            currentStep = 0;
            currentScene = 0;
            allowNextStep = true;

            scene4_item.SetActive(false);

            storyIntro_string_currentNum = 0;
            storyIntro_open_item_step = 0;

            for (int i = 0; i < scene.Count; i++)
            {
                scene[i].SetActive(i == 0);
            }

            animator.Play("Play_ChangeScene_End");

            SoundManager.Instance.PlayBgmWithType(SoundType.bgm_storyIntro_1);
        }

        public void NextStep()
        {
            if (allowNextStep)
            {
                allowNextStep = false;

                switch (currentStep)
                {
                    case 1: // Scene 1 - 1b
                        animator.Play("Play_ChangeText_Start");
                        break;

                    case 2: // Scene 2
                        currentScene = 1;
                        animator.Play("Play_ChangeScene_Start");
                        SoundManager.Instance.FadeOutBGM(10, 0.05f);
                        break;

                    case 3: // Scene 2 - 2
                        SoundManager.Instance.PlayBgmWithType(SoundType.bgm_storyIntro_2n3);
                        animator.Play("Play_ChangeText_Start");
                        break;

                    case 4: // Scene 3
                        currentScene = 2;
                        animator.Play("Play_ChangeScene_Start");
                        break;

                    case 5: // Scene 3 - 3
                        animator.Play("Play_ChangeText_Start");
                        break;

                    case 6: // Scene 4
                        currentScene = 3;
                        animator.Play("Play_ChangeScene_Start");
                        break;

                    case 7: // Scene 4 - 4a
                        animator.Play("Play_ChangeText_Start");
                        break;

                    case 8:
                        currentScene = 4;
                        animator.Play("Play_ChangeScene_Start");
                        break;

                    case 9: // Scene 4 - 4b
                        animator.Play("Play_ChangeText_Start");
                        break;

                    case 10:
                        currentScene = 999;
                        SoundManager.Instance.FadeOutBGM(10, 0.05f);
                        animator.Play("Play_ChangeText_Start");
                        break;
                }

                currentStep++;
            }
        }

        // For Animation
        public void ChangeText_Start_Finish()
        {
            if (storyIntro_string_currentNum < storyIntro_string.Length)
            {
                storyText.text = LanguageManager.Instance.GetTextValue(storyIntro_string[storyIntro_string_currentNum]);
                storyIntro_string_currentNum++;
            }

            if (currentScene != 999)
            {
                animator.Play("Play_ChangeText_End");
            }
            else
            {
                // Ending
                SoundManager.Instance.PlayBgmWithType(SoundType.bgm);

                scene4_item.SetActive(true);

                stepBtn.SetActive(false);
                fadeObj.SetActive(false);

                storyText.text = "";
                storyText.gameObject.SetActive(false);

                currentPlayerName = PlayerPrefs.GetString("PLAYER_NAME", "Hero");
                placeHolder_Text.text = "Your Name Here!";
            }
        }

        public void ChangeText_End_Finish()
        {
            allowNextStep = true;
        }

        public void ChangeScene_Start_Finish()
        {
            if (currentScene != 999)
            {
                storyText.text = "";

                scene[currentScene - 1].SetActive(false);
                scene[currentScene].SetActive(true);
            }
            else
            {
                // Hide All - For ending fade out!
                scene4_item.SetActive(false);

                bg.SetActive(false);

                for (int i = 0; i < scene.Count; i++)
                {
                    scene[i].SetActive(false);
                }
            }

            animator.Play("Play_ChangeScene_End");
        }

        public void ChangeScene_End_Finish()
        {
            allowNextStep = true;

            if (currentScene != 999)
            {
                if (currentStep == 0)
                {
                    currentStep++;
                    animator.Play("Play_ChangeText_Start");
                }
                else
                {
                    NextStep();
                }
            }
            else
            {
                CloseView();
            }
        }

        public void ComfirmSetting()
        {
            fadeObj.SetActive(true);

            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            try
            {
                if (nameField.text.Length > 0)
                    PlayerPrefs.SetString("PLAYER_NAME", nameField.text);
                else
                    PlayerPrefs.SetString("PLAYER_NAME", currentPlayerName);

                PlayerPrefs.Save();

                currentPlayerName = PlayerPrefs.GetString("PLAYER_NAME", currentPlayerName);
            }
            catch
            {

            }

            animator.Play("Play_ChangeScene_Start");
        }
    }
}
