﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;
using System;
using System.Collections.Generic;
using UnityEngine.Analytics;
using Zenject;

namespace Ignite.AOM
{
    public class PrestigeDetailViewController : UIOverlayViewController
    {
        private AvatarCore _avatarCore;
        private HeroCore _heroCore;

        [Inject]
        public void ConstructorSafeAttribute(AvatarCore avatarCore, HeroCore heroCore)
        {
            _avatarCore = avatarCore;
            _heroCore = heroCore;
        }

        public GameObject prestigeBtn;
        public Button superPrestigeBtn;
        public Text heroLevelBonus;
        public Text stageCompletionBonus;
        public Text fullTeamBonus;
        public Text totalRelics;

        public GameObject superPrestige_desc;
        public GameObject superPrestige_resetDesc;

        string superPrestigeResetTime_label;
        Double superPrestigeResetTime;
        float updateTimer;

        bool countDown = false;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            IList<int> result = _avatarCore.RelicForPrestige();

            heroLevelBonus.text = result[0].ToString();
            stageCompletionBonus.text = result[1].ToString();
            fullTeamBonus.text = (_heroCore.AllHeroAlive ? result[0] + result[1] : 0).ToString();
            totalRelics.text = ((result[0] + result[1]) * (_heroCore.AllHeroAlive ? 2 : 1)).ToString();

            superPrestigeResetTime_label = LanguageManager.Instance.GetTextValue("prestige_btn_label_3c");
            superPrestigeResetTime = Double.Parse(PlayerPrefs.GetString("PP_SUPER_PRESTIGE_RESET_TIME", "0"));

            countDown = superPrestigeResetTime - UnitConverter.ConvertToTimestamp(System.DateTime.Now) > 0;

            if (countDown)
            {
                superPrestige_desc.SetActive(false);
                superPrestige_resetDesc.SetActive(true);

                superPrestige_resetDesc.GetComponent<Text>().text = superPrestigeResetTime_label + " : " + UnitConverter.SecondsToTime(superPrestigeResetTime - UnitConverter.ConvertToTimestamp(System.DateTime.Now));
            }

            updateTimer = 0;
        }

        public void Prestige()
        {
            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                UserManager.Instance.ShowNetworkNotReachablePopup();
                return;
            }

            CloseView();

            if (HiddenMenu.Instance != null)
                HiddenMenu.Instance.CloseHiddenMenu();

            if (GameManager.Instance != null)
                GameManager.Instance.PlayPrestigeAnimation();

            PlayerPrefs.SetString("PP_SUPER_PRESTIGE_RESET_TIME", "0");

            countDown = false;


#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
			Analytics.CustomEvent("Prestige", new Dictionary<string, object>
			{
				{ "type", "Reborn" }
			});
#endif
        }

        public void SuperPrestige()
        {
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                if (UserManager.Instance.CurrentUserSummary.Gem >= 500)
//                {
                    //SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                    UserManager.Instance.TakeGems(500, () =>
                    {
                        CloseView();

                        if (HiddenMenu.Instance != null)
                            HiddenMenu.Instance.CloseHiddenMenu();

                        if (GameManager.Instance != null)
                            GameManager.Instance.PlayPrestigeAnimation(false);

                        PlayerPrefs.SetString("PP_SUPER_PRESTIGE_RESET_TIME",
                            ((UnitConverter.ConvertToTimestamp(System.DateTime.Now) + 86400).ToString()));

                        // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                        Analytics.CustomEvent("Prestige", new Dictionary<string, object>
                        {
                            { "type", "Chrono's gate" }
                        });

                        Analytics.CustomEvent("useGems", new Dictionary<string, object>
                        {
                            { "type", "Use Chrono's Gate" },
                        });
#endif
                    });
//                }
//                else
//                {
//                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
//
//                    LoadOverlayView("ShopMenu", false, Time.timeScale == 0);
//                }
//            }
        }

        void Update()
        {
            if (countDown)
            {
                if (superPrestigeBtn.interactable)
                {
                    superPrestigeBtn.interactable = false;
                    superPrestigeBtn.GetComponent<Image>().sprite = superPrestigeBtn.GetComponent<Button>().spriteState.disabledSprite;

                    superPrestige_desc.SetActive(false);
                    superPrestige_resetDesc.SetActive(true);
                }

                updateTimer += Time.deltaTime;

                if (updateTimer >= 1)
                {
                    superPrestige_resetDesc.GetComponent<Text>().text = superPrestigeResetTime_label + " : " + UnitConverter.SecondsToTime(superPrestigeResetTime - UnitConverter.ConvertToTimestamp(System.DateTime.Now));
                    updateTimer = 0;
                }

                countDown = superPrestigeResetTime - UnitConverter.ConvertToTimestamp(System.DateTime.Now) > 0;
            }
            else
            {
                if (!superPrestigeBtn.interactable)
                {
                    superPrestigeBtn.interactable = true;
                    superPrestigeBtn.GetComponent<Image>().sprite = superPrestigeBtn.GetComponent<Button>().spriteState.pressedSprite;

                    superPrestige_desc.SetActive(true);
                    superPrestige_resetDesc.SetActive(false);
                }
            }
        }

        public override void CloseView()
        {
            base.CloseView();
        }


    }
}
