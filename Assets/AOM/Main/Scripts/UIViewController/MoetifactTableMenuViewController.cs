﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class MoetifactTableMenuViewController : UIOverlayViewController
    {
        private SelectionObjectController.Factory _selectionObjectControllerFactory;
        private ArtifactCore _artifactCore;

        [Inject]
        public void ConstructorSafeAttribute(SelectionObjectController.Factory selectionObjectControllerFactory,
            ArtifactCore artifactCore)
        {
            _selectionObjectControllerFactory = selectionObjectControllerFactory;
            _artifactCore = artifactCore;
        }

        public GridLayoutGroup objListLayoutGrid;
        public RectTransform objListLayoutRect;
        public RectTransform objListScrollViewRect;

        List<GameObject> selectionObjList;

        private Dictionary<string, Sprite> thumbnail;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            InitTable();

            EventManager.OnSalvageHandler += RefreshTable;
            EventManager.OnArtifactVGBalanceChange += OnArtifactVGBalanceChange;
        }

        void InitTable()
        {
            selectionObjList = new List<GameObject>();

            if (thumbnail == null)
            {
                thumbnail = new Dictionary<string, Sprite>();

                Sprite[] _sprites = Resources.LoadAll<Sprite>("Sprites/overlay_icon_artifact/overlay_icon_artifact");

                foreach (Sprite _sprite in _sprites)
                {
                    thumbnail.Add(_sprite.name, _sprite);
                }
            }

            InitTableContent();

            RefreshTable();
        }

        void InitTableContent(int rareLevel = 0)
        {
            if (selectionObjList.Count > 0)
            {
                foreach (GameObject selectionObj in selectionObjList)
                {
                    Destroy(selectionObj);
                }

                selectionObjList.Clear();
            }

            for (int i = 0; i < _artifactCore.ArtifactRareFilter(rareLevel).Count; i++)
            {
                MoetifactAndMoeSpiritTableCell mfTableCellController = _selectionObjectControllerFactory.Create("MoetifactAndMoeSpiritTableCell") as MoetifactAndMoeSpiritTableCell;

                ArtifactVG artifactVG = _artifactCore.ArtifactRareFilter(rareLevel)[i];

                mfTableCellController.gameObject.name = artifactVG.ItemId;
                mfTableCellController.gameObject.GetComponent<Image>().sprite = thumbnail[artifactVG.ItemId];
                mfTableCellController.gameObject.transform.SetParent(objListLayoutGrid.transform, false);

                mfTableCellController.item = artifactVG;
                mfTableCellController.isMoetifact = true;
                mfTableCellController.InitItem();

                selectionObjList.Add(mfTableCellController.gameObject);
            }

            // Reset table size
            float objListLayoutRectSizeX = (objListLayoutGrid.cellSize.x * 4);

            int rowCount = (int) objListLayoutGrid.cellSize.y * ((selectionObjList.Count / 4) + (selectionObjList.Count % 4 != 0 ? 1 : 0));
            float objListLayoutRectSizeY = rowCount >= objListLayoutGrid.cellSize.y * 4 ? rowCount : objListLayoutGrid.cellSize.y * 4;

            if (objListScrollViewRect.sizeDelta.x > objListLayoutRectSizeX)
                objListLayoutRectSizeX = objListScrollViewRect.sizeDelta.x;

            objListLayoutRect.sizeDelta = new Vector2(objListLayoutRectSizeX, objListLayoutRectSizeY);
            objListLayoutRect.transform.localPosition = new Vector3(0, 0, 0);
        }

        void RefreshTable()
        {
            IList<ArtifactVG> ownedArtifactList = _artifactCore.FindOwnedArtifact();

            for (int i = 0; i < selectionObjList.Count; i++)
            {
                selectionObjList[i].GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.3f, 1);

                for (int j = 0; j < ownedArtifactList.Count; j++)
                {
                    if (selectionObjList[i].name == ownedArtifactList[j].ItemId)
                        selectionObjList[i].GetComponent<Image>().color = new Color(1, 1, 1, 1);
                }

                selectionObjList[i].GetComponent<MoetifactAndMoeSpiritTableCell>().InitItem();
            }
        }

        private void OnArtifactVGBalanceChange(ArtifactVG vg, int balance)
        {
            RefreshTable();
        }

        public void RareFilter(int rareLevel)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            InitTableContent(rareLevel);

            RefreshTable();
        }

        public void ShowGachaOptionOverlay()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            LoadOverlayView("GachaMenuView", false, true);
        }

        public void OpenShopMenu()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            LoadOverlayView("ShopMenu", false, Time.timeScale == 0);
        }

        public override void CloseView()
        {
            EventManager.OnSalvageHandler -= RefreshTable;
            EventManager.OnArtifactVGBalanceChange -= OnArtifactVGBalanceChange;

            base.CloseView();

            if (Time.timeScale == 0)
                Time.timeScale = 1;
        }
    }
}