﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

namespace Ignite.AOM
{
    public enum GachaType
    {
        Moetifact,
        MoeSpirit,
        Purchase
    }

    public class GachaResultViewController : UIOverlayViewController
    {
        public Animator gachaAnimation;
        public ParticleSystem gachaParticleSystem;

        public Image gachaIcon;
        public Text gachaName;

        public GameObject rareLevelStar1;
        public GameObject rareLevelStar2;
        public GameObject rareLevelStar3;

        private float timer, time = 10;
        private bool scriptOn = false;

        AOMCachedBalanceVG itemVG;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);
        }

        // Update is called once per frame
        void Update()
        {
            if (scriptOn)
            {
                if (timer > 0)
                {
                    timer -= Time.deltaTime;
                }
                else
                {
                    OffGachaResult();
                }

                if (Time.timeScale < 0.01f)
                {
                    gachaParticleSystem.Simulate(Time.unscaledDeltaTime, true, false);
                }
            }
        }

        public void SetItemId(AOMCachedBalanceVG itemVG, bool isMoetifactItem)
        {
            this.itemVG = itemVG;

            if (this.itemVG != null)
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.GachaResult);

                timer = time;

                if (isMoetifactItem)
                {
                    ArtifactVG moetifactVG = this.itemVG as ArtifactVG;

                    rareLevelStar1.SetActive(moetifactVG.RareLevel > 0);
                    rareLevelStar2.SetActive(moetifactVG.RareLevel > 1);
                    rareLevelStar3.SetActive(moetifactVG.RareLevel > 2);

                    if (moetifactVG.RareLevel == 1)
                    {
                        rareLevelStar1.transform.localPosition = new Vector3(0, -65, 0);
                    }
                    else if (moetifactVG.RareLevel == 2)
                    {
                        rareLevelStar1.transform.localPosition = new Vector3(-40, -65, 0);
                        rareLevelStar2.transform.localPosition = new Vector3(40, -65, 0);
                    }
                    else if (moetifactVG.RareLevel == 3)
                    {
                        rareLevelStar1.transform.localPosition = new Vector3(-60, -65, 0);
                        rareLevelStar2.transform.localPosition = new Vector3(0, -65, 0);
                        rareLevelStar3.transform.localPosition = new Vector3(60, -65, 0);
                    }

                    Sprite[] _sprites = Resources.LoadAll<Sprite>("Sprites/overlay_icon_artifact/overlay_icon_artifact");

                    foreach (Sprite _sprite in _sprites)
                    {
                        if (_sprite.name == this.itemVG.ItemId)
                            gachaIcon.sprite = _sprite;
                    }
                }
                else
                {
                    rareLevelStar1.SetActive(false);
                    rareLevelStar2.SetActive(false);
                    rareLevelStar3.SetActive(false);

                    Sprite[] _sprites = Resources.LoadAll<Sprite>("Sprites/table_ui_diamond/table_ui_diamond");

                    foreach (Sprite _sprite in _sprites)
                    {
                        if (_sprite.name == this.itemVG.ItemId)
                            gachaIcon.sprite = _sprite;
                    }
                }

                gachaName.text = LanguageManager.Instance.GetTextValue(this.itemVG.ItemId + "_name");

                gachaAnimation.Play("ArtifactGachaResult_Start");

                scriptOn = true;
            }
        }

        public void SetItemId(AOMCachedBalanceVG itemVG, GachaType gachaType)
        {
            this.itemVG = itemVG;

            if (this.itemVG != null)
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.GachaResult);

                timer = time;

                if (gachaType == GachaType.Moetifact || gachaType == GachaType.MoeSpirit)
                {
                    ArtifactVG moetifactVG = this.itemVG as ArtifactVG;

                    rareLevelStar1.SetActive(moetifactVG.RareLevel > 0);
                    rareLevelStar2.SetActive(moetifactVG.RareLevel > 1);
                    rareLevelStar3.SetActive(moetifactVG.RareLevel > 2);

                    if (moetifactVG.RareLevel == 1)
                    {
                        rareLevelStar1.transform.localPosition = new Vector3(0, -65, 0);
                    }
                    else if (moetifactVG.RareLevel == 2)
                    {
                        rareLevelStar1.transform.localPosition = new Vector3(-40, -65, 0);
                        rareLevelStar2.transform.localPosition = new Vector3(40, -65, 0);
                    }
                    else if (moetifactVG.RareLevel == 3)
                    {
                        rareLevelStar1.transform.localPosition = new Vector3(-60, -65, 0);
                        rareLevelStar2.transform.localPosition = new Vector3(0, -65, 0);
                        rareLevelStar3.transform.localPosition = new Vector3(60, -65, 0);
                    }

                    string path = (gachaType == GachaType.Moetifact)
                        ? "Sprites/overlay_icon_artifact/overlay_icon_artifact"
                        : "Sprites/overlay_icon_moeSpirit/overlay_icon_moeSpirit";

                    Sprite[] _sprites = Resources.LoadAll<Sprite>(path);

                    foreach (Sprite _sprite in _sprites)
                    {
                        if (_sprite.name == this.itemVG.ItemId)
                            gachaIcon.sprite = _sprite;
                    }
                }
                else
                {
                    rareLevelStar1.SetActive(false);
                    rareLevelStar2.SetActive(false);
                    rareLevelStar3.SetActive(false);

                    Sprite[] _sprites = Resources.LoadAll<Sprite>("Sprites/table_ui_diamond/table_ui_diamond");

                    foreach (Sprite _sprite in _sprites)
                    {
                        if (_sprite.name == this.itemVG.ItemId)
                            gachaIcon.sprite = _sprite;
                    }
                }

                gachaName.text = LanguageManager.Instance.GetTextValue(this.itemVG.ItemId + "_name");

                gachaAnimation.Play("ArtifactGachaResult_Start");

                scriptOn = true;
            }
        }

        public void ShowGachaEffect(Sprite itemIcon, string itemDesc = "")
        {
            rareLevelStar1.SetActive(false);
            rareLevelStar2.SetActive(false);
            rareLevelStar3.SetActive(false);

            SoundManager.Instance.PlaySoundWithType(SoundType.GachaResult);

            timer = time;

            gachaIcon.sprite = itemIcon;

            gachaName.text = itemDesc;

            gachaAnimation.Play("ArtifactGachaResult_Start");

            scriptOn = true;
        }

        public void OffGachaResult()
        {
            SoundManager.Instance.StopSound();

            timer = time;
            scriptOn = false;

            gachaAnimation.Play("ArtifactGachaResult_End");
        }
    }
}