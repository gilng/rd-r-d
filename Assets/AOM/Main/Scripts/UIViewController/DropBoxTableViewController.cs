﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Soomla.Store;
using SmartLocalization;
using Parse;
using Zenject;

namespace Ignite.AOM
{
    public class DropBoxTableViewController : UIOverlayViewController
    {
        private SelectionObjectController.Factory _selectionObjectControllerFactory;
        private SocialManager _socialManager;

        [Inject]
        public void ConstructorSafeAttribute(SelectionObjectController.Factory selectionObjectControllerFactory, SocialManager socialManager)
        {
            _selectionObjectControllerFactory = selectionObjectControllerFactory;
            _socialManager = socialManager;
        }

        public Text title;

        public GameObject msgPanel;
        public Text msgTxt;
        public Button btnRetry;

        public GridLayoutGroup tableLayoutGrid;
        public RectTransform tableLayoutRect;
        public RectTransform tableScrollViewRect;

        public GameObject attachmentPanel;
        public Image attachmentIcon;
        public Text attachmentMsg;
        private DropBoxTableCell attachCell;

        private List<DropBoxTableCell> allDropBoxObj = new List<DropBoxTableCell>();
        private IList<DropBoxItem> dataList;



        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            if (attachmentPanel.activeSelf)
                attachmentPanel.SetActive(false);

            LoadData();
        }

        public void LoadData()
        {
            if (!msgPanel.activeSelf)
                msgPanel.SetActive(true);

            if (SocialManager.CheckForInternetConnection())
            {
                if (btnRetry.gameObject.activeSelf)
                    btnRetry.gameObject.SetActive(false);

                if (_socialManager.parseSignedIn)
                {
                    msgTxt.text = LanguageManager.Instance.GetTextValue("dropbox_loading");

                    StartCoroutine(LoadParseData(list =>
                        {
                            if (list == null)
                            {
                                msgTxt.text = LanguageManager.Instance.GetTextValue("dropbox_errorMsg_LoadParseFail");

                                if (!btnRetry.gameObject.activeSelf)
                                    btnRetry.gameObject.SetActive(true);
                            }
                            else
                            {
                                if (btnRetry.gameObject.activeSelf)
                                    btnRetry.gameObject.SetActive(false);

                                List<DropBoxItem> newList = new List<DropBoxItem>();

                                for (int i = 0; i < list.Count; i++)
                                {
                                    TimeSpan ts = DateTime.Parse(list[i].CreatedAt.ToString()).AddDays(7).Subtract(DateTime.UtcNow);

                                    if (ts.TotalSeconds > 0)
                                    {
                                        newList.Add(list[i]);
                                    }
                                    else
                                    {
                                        Task deleteTask = list[i].DeleteAsync();
                                    }
                                }

                                if (newList.Count == 0)
                                    msgTxt.text = LanguageManager.Instance.GetTextValue("dropbox_msg_empty");
                                else
                                    CreateTable(newList);
                            }
                        }));
                }
                else
                {
                    // Create Account
                    Action successCallback = () =>
                    {
                        msgTxt.text = LanguageManager.Instance.GetTextValue("dropbox_msg_empty");

                        if (btnRetry.gameObject.activeSelf)
                            btnRetry.gameObject.SetActive(false);
                    };

                    Action failCallback = () =>
                    {
                        LoadData();
                    };

                    _socialManager.ParseSignUp(successCallback, failCallback);
                }
            }
            else
            {
                msgTxt.text = LanguageManager.Instance.GetTextValue("dropbox_errorMsg_NoInternet");

                if (!btnRetry.gameObject.activeSelf)
                    btnRetry.gameObject.SetActive(true);
            }
        }

        IEnumerator LoadParseData(Action<IList<DropBoxItem>> callback)
        {
            ParseQuery<DropBoxItem> query = new ParseQuery<DropBoxItem>().WhereEqualTo("user", ParseUser.CurrentUser).OrderBy("updatedAt");

            Task<IEnumerable<DropBoxItem>> t = query.FindAsync();

            while (!t.IsCompleted)
            {
                yield return null;
            }

            if (t.IsFaulted || t.IsCanceled)
            {
                foreach (ParseException err in t.Exception.InnerExceptions)
                {
                    Debug.LogError("error message: " + err.Message + " - error code: " + err.Code + " - error stacktrace: " + err.StackTrace);
                }
                callback(null);
            }
            else if (t.IsCompleted)
            {
                IEnumerable<DropBoxItem> result = t.Result;

                callback(result.ToList());
            }
        }

        void CreateTable(List<DropBoxItem> dataList)
        {
            this.dataList = dataList;

            for (int i = 0; i < dataList.Count; i++)
            {
                DropBoxTableCell dropBoxTableCellController = _selectionObjectControllerFactory.Create("DropBoxTableCell") as DropBoxTableCell;

                dropBoxTableCellController.gameObject.name = "item_" + (i + 1);
                dropBoxTableCellController.gameObject.transform.SetParent(tableLayoutGrid.transform, false);
                dropBoxTableCellController.gameObject.transform.localPosition = Vector3.zero;

                dropBoxTableCellController.Init(this, dataList[i]);

                allDropBoxObj.Add(dropBoxTableCellController);
            }

            ResizeTableLayoutRect();

            msgPanel.SetActive(false);

            UpdateMailCount();
        }

        void ResizeTableLayoutRect()
        {
            float tableLayoutRectSizeX = tableLayoutGrid.cellSize.x;
            float tableLayoutRectSizeY = (tableLayoutGrid.transform.childCount * tableLayoutGrid.cellSize.y) + ((tableLayoutGrid.transform.childCount - 1) * tableLayoutGrid.spacing.y);

            if (tableScrollViewRect.sizeDelta.y > tableLayoutRectSizeY)
                tableLayoutRectSizeY = tableScrollViewRect.sizeDelta.y;

            tableLayoutRect.sizeDelta = new Vector2(tableLayoutRectSizeX, tableLayoutRectSizeY);
            tableLayoutRect.transform.localPosition = new Vector3(tableLayoutRect.transform.localPosition.x, 0, tableLayoutRect.transform.localPosition.z);
        }

        public void LoadAttachment(DropBoxTableCell _attachCell, Sprite _sprite, string _msg)
        {
            this.attachCell = _attachCell;
            attachmentIcon.sprite = _sprite;
            attachmentMsg.text = _msg;
            attachmentPanel.SetActive(true);
        }

        public void CollectAttachment()
        {
            attachmentPanel.SetActive(false);
            attachCell.GiveItem();
        }

        public void RemoveParseData(DropBoxTableCell tableCell, DropBoxItem data)
        {
            foreach (DropBoxItem dbi in dataList)
            {
                if (DropBoxItem.Equals(dbi, data))
                {
                    Task deleteTask = dbi.DeleteAsync();

                    // Reset table
                    if (allDropBoxObj.Contains(tableCell))
                    {
                        allDropBoxObj.Remove(tableCell);

                        tableCell.gameObject.transform.SetParent(null);

                        Destroy(tableCell.gameObject);
                    }

                    ResizeTableLayoutRect();
                }
            }

            UpdateMailCount();
        }

        public bool CollectItemChecking()
        {
            bool isConnected = SocialManager.CheckForInternetConnection();

            if (!isConnected)
            {
                if (!msgPanel.activeSelf)
                    msgPanel.SetActive(true);

                if (!btnRetry.gameObject.activeSelf)
                    btnRetry.gameObject.SetActive(true);

                msgTxt.text = LanguageManager.Instance.GetTextValue("dropbox_errorMsg_NoInternet");

                foreach (DropBoxTableCell c in allDropBoxObj)
                {
                    c.gameObject.transform.SetParent(null);
                    Destroy(c.gameObject);
                }

                allDropBoxObj.Clear();

                //				ResizeTableLayoutRect();
            }

            return isConnected;
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            UpdateMailCount();

            base.CloseView();

            if (Time.timeScale == 0)
                Time.timeScale = 1;
        }

        void UpdateMailCount()
        {
            if (allDropBoxObj.Count == 0)
            {
                msgPanel.SetActive(true);
                msgTxt.text = LanguageManager.Instance.GetTextValue("dropbox_msg_empty");
            }

            GameManager.Instance.mailTextObj.SetActive(allDropBoxObj.Count > 0);
            GameManager.Instance.mailText.text = (allDropBoxObj.Count == 0) ? "" : ((allDropBoxObj.Count >= 10) ? "9+" : allDropBoxObj.Count.ToString());
        }
    }

    [ParseClassName("DropBox")]
    public class DropBoxItem : ParseObject
    {
        [ParseFieldName("type")]
        public string Type
        {
            get { return GetProperty<string>("Type"); }
        }

        [ParseFieldName("title")]
        public IDictionary<string, string> Title
        {
            get { return GetProperty<IDictionary<string, string>>("Title"); }
        }

        [ParseFieldName("value")]
        public string Value
        {
            get { return GetProperty<string>("Value"); }
        }

        [ParseFieldName("msg")]
        public string Msg
        {
            get { return GetProperty<string>("Msg"); }
        }
    }

}
