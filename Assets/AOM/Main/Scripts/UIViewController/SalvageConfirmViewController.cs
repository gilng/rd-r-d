﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class SalvageConfirmViewController : UIOverlayViewController
    {
        private ArtifactCore _artifactCore;

        [Inject]
        public void ConstructorSafeAttribute(ArtifactCore artifactCore)
        {
            _artifactCore = artifactCore;
        }

        private ArtifactVG item;

        public Image icon;

        public Text subtitle, diamondPrice, currentGemsLabel;
        //public Text relicEarn;

        Sprite[] thumbnailIcon;
        string[] thumbnailIconName;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            AddListener();
        }

        void AddListener()
        {
            EventManager.OnDiamondBalanceUpdated += RefreshOwnedGemsDisplay;
        }

        void RemoveListener()
        {
            EventManager.OnDiamondBalanceUpdated -= RefreshOwnedGemsDisplay;
        }

        void RefreshOwnedGemsDisplay()
        {
            currentGemsLabel.text = string.Format(LanguageManager.Instance.GetTextValue("current_gems"),
                UserManager.Instance.CurrentUserSummary != null
                    ? UserManager.Instance.CurrentUserSummary.Gem.ToString()
                    : AOMStoreInventory.GetDiamondBalance().ToString());
        }

        public void SetMoetifactItem(ArtifactVG item)
        {
            this.item = item;

            if (item != null)
            {
                subtitle.text = LanguageManager.Instance.GetTextValue("SalvageMenu_Subtitle") + " " +
                                LanguageManager.Instance.GetTextValue(item.ItemId + "_name");

                diamondPrice.text = UnitConverter.ConverterDoubleToString(_artifactCore.SalvageCost(item));
                //relicEarn.text = UnitConverter.ConverterDoubleToString(_artifactCore.RelicEarnFromSalvage(item));

                RefreshOwnedGemsDisplay();

                if (thumbnailIcon == null && thumbnailIconName == null)
                {
                    thumbnailIcon = Resources.LoadAll<Sprite>("Sprites/overlay_icon_artifact/overlay_icon_artifact");
                    thumbnailIconName = new string[thumbnailIcon.Length];

                    for (int i = 0; i < thumbnailIconName.Length; i++)
                    {
                        thumbnailIconName[i] = thumbnailIcon[i].name;
                    }
                }

                icon.sprite = thumbnailIcon[Array.IndexOf(thumbnailIconName, item.ItemId)];
            }
        }

        public void ConfirmSalvageArtifact()
        {
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                if (UserManager.Instance.CurrentUserSummary.Gem >= _artifactCore.SalvageCost(this.item))
//                {
//                    //SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                    _artifactCore.Gacha(true, _artifactCore.SalvageCost(item), afVG =>
                    {
                        if (afVG != null)
                        {
                            _artifactCore.Salvage(item);

                            GachaResultViewController gachaResultViewController =
                                LoadOverlayViewController("GachaResultMenuView", true,
                                    Time.timeScale == 0) as GachaResultViewController;
                            gachaResultViewController.SetItemId(afVG, GachaType.Moetifact);

                            HiddenMenu.Instance.CheckMoetifactOwned();

                            this.previousUIView.CloseView();

                            RemoveListener();
                        }
                    });
//                }
//                else
//                {
//                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
//
//                    LoadOverlayView("ShopMenu", false, Time.timeScale == 0);
//                }
//            }
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            RemoveListener();

            base.CloseView();
        }
    }
}