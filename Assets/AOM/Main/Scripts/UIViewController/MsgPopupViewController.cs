﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using SmartLocalization;

namespace Ignite.AOM
{
    public class MsgPopupViewController : UIOverlayViewController
    {
        public Text txt_title, txt_message;

        public Text txt_btn;
        public Button btn;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);
        }

        public void InputMsg(string _title = "", string _message = "", string _btnTxt = "", Action _btnAction = null)
        {
            txt_title.text = (LanguageManager.Instance.HasKey(_title)) ? LanguageManager.Instance.GetTextValue(_title) : _title;
            txt_message.text = (LanguageManager.Instance.HasKey(_message)) ? LanguageManager.Instance.GetTextValue(_message) : _message;

            if (_btnTxt != "")
                txt_btn.text = (LanguageManager.Instance.HasKey(_btnTxt)) ? LanguageManager.Instance.GetTextValue(_btnTxt) : _btnTxt;
            else
                txt_btn.text = LanguageManager.Instance.GetTextValue("btn_ok");

            btn.onClick.AddListener(() =>
                {
                    if (_btnAction != null)
                    {
                        SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                        _btnAction();
                    }

                    CloseView();
                });
        }

		public void InputContent(string _content)
		{
			txt_message.text = _content;
		}

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            base.CloseView();
        }

    }
}
