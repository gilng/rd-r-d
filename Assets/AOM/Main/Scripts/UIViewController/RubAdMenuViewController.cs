﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;
using System;
using Zenject;

namespace Ignite.AOM
{
    public class RubAdMenuViewController : UIOverlayViewController
    {
        private GalleryCore _galleryCore;

        [Inject]
        public void ConstructorSafeAttribute(GalleryCore galleryCore)
        {
            _galleryCore = galleryCore;
        }

        public Text rubNotice;
        public GameObject watchBtn, backBtn, ConfirmBtn;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            watchBtn.SetActive(true);
            backBtn.SetActive(true);
            ConfirmBtn.SetActive(false);

            rubNotice.text = LanguageManager.Instance.GetTextValue("rubBonus_notice_1");
        }

        public void WatchAd()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            ADManager.Instance.ShowRewardedAd();

            watchBtn.SetActive(false);
            backBtn.SetActive(false);
            ConfirmBtn.SetActive(true);

            rubNotice.text = LanguageManager.Instance.GetTextValue("rubBonus_notice_2");
        }

        public void CollectReward()
        {
            // Add rub
            _galleryCore.RechargingTouchCredit(1);

            GameManager.Instance.UpdateTouchCredit();

            CloseView();
        }
    }
}
