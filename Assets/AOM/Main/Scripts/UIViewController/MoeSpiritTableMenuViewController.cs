﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class MoeSpiritTableMenuViewController : UIOverlayViewController
    {
        private SelectionObjectController.Factory _selectionObjectControllerFactory;
        private ArtifactCore _artifactCore;

        [Inject]
        public void ConstructorSafeAttribute(SelectionObjectController.Factory selectionObjectControllerFactory,
            ArtifactCore artifactCore)
        {
            _selectionObjectControllerFactory = selectionObjectControllerFactory;
            _artifactCore = artifactCore;
        }

        public GridLayoutGroup objListLayoutGrid;
        public RectTransform objListLayoutRect;
        public RectTransform objListScrollViewRect;

        List<GameObject> selectionObjList;

        private Dictionary<string, Sprite> thumbnail;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            InitTable();

            EventManager.OnArtifactVGBalanceChange += OnMoeSpiritBalanceChange;
        }

        void InitTable()
        {
            selectionObjList = new List<GameObject>();

            if (thumbnail == null)
            {
                thumbnail = new Dictionary<string, Sprite>();

                Sprite[] _sprites = Resources.LoadAll<Sprite>("Sprites/overlay_icon_moeSpirit/overlay_icon_moeSpirit");

                foreach (Sprite _sprite in _sprites)
                {
                    thumbnail.Add(_sprite.name, _sprite);
                }
            }

            InitTableContent();

            RefreshTable();
        }

        void InitTableContent(int rareLevel = 0)
        {
            if (selectionObjList.Count > 0)
            {
                foreach (GameObject selectionObj in selectionObjList)
                {
                    Destroy(selectionObj);
                }

                selectionObjList.Clear();
            }

            for (int i = 0; i < _artifactCore.MoeSpiritRareFilter(rareLevel).Count; i++)
            {
                MoetifactAndMoeSpiritTableCell msTableCellController = _selectionObjectControllerFactory.Create("MoetifactAndMoeSpiritTableCell") as MoetifactAndMoeSpiritTableCell;

                ArtifactVG artifactVG = _artifactCore.MoeSpiritRareFilter(rareLevel)[i];

                msTableCellController.gameObject.name = artifactVG.ItemId;
                msTableCellController.gameObject.GetComponent<Image>().sprite = thumbnail[artifactVG.ItemId];
                msTableCellController.gameObject.transform.SetParent(objListLayoutGrid.transform, false);

                msTableCellController.item = artifactVG;
                msTableCellController.isMoetifact = false;
                msTableCellController.InitItem();

                selectionObjList.Add(msTableCellController.gameObject);
            }

            // Reset table size
            float objListLayoutRectSizeX = (objListLayoutGrid.cellSize.x * 4);

            int rowCount = (int) objListLayoutGrid.cellSize.y *
                           ((selectionObjList.Count / 4) + (selectionObjList.Count % 4 != 0 ? 1 : 0));
            float objListLayoutRectSizeY = rowCount >= objListLayoutGrid.cellSize.y * 4
                ? rowCount
                : objListLayoutGrid.cellSize.y * 4;

            if (objListScrollViewRect.sizeDelta.x > objListLayoutRectSizeX)
                objListLayoutRectSizeX = objListScrollViewRect.sizeDelta.x;

            objListLayoutRect.sizeDelta = new Vector2(objListLayoutRectSizeX, objListLayoutRectSizeY);
            objListLayoutRect.transform.localPosition = new Vector3(0, 0, 0);
        }

        void RefreshTable()
        {
            IList<ArtifactVG> ownedMoeSpiritList = _artifactCore.FindOwnedMoeSpirit();

            for (int i = 0; i < selectionObjList.Count; i++)
            {
                selectionObjList[i].GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f, 1);

                for (int j = 0; j < ownedMoeSpiritList.Count; j++)
                {
                    if (selectionObjList[i].name == ownedMoeSpiritList[j].ItemId)
                        selectionObjList[i].GetComponent<Image>().color = new Color(1, 1, 1, 1);
                }

                selectionObjList[i].GetComponent<MoetifactAndMoeSpiritTableCell>().InitItem();
            }
        }

        private void OnMoeSpiritBalanceChange(ArtifactVG vg, int balance)
        {
            RefreshTable();
        }

        public void RareFilter(int rareLevel)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            InitTableContent(rareLevel);

            RefreshTable();
        }

        public override void CloseView()
        {
            EventManager.OnArtifactVGBalanceChange -= OnMoeSpiritBalanceChange;

            base.CloseView();

            if (Time.timeScale == 0)
                Time.timeScale = 1;
        }
    }
}