﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;
using System;

namespace Ignite.AOM
{
    public class RubInstructionViewController : UIOverlayViewController
	{
        public Text instructionTitle;
        public Text instructionText;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            instructionTitle.text = LanguageManager.Instance.GetTextValue("gallery_tutoral_title");
            instructionText.text = LanguageManager.Instance.GetTextValue("gallery_tutoral");
        }

        public override void CloseView()
        {
            base.CloseView();

            if (!TutorialManager.Instance.CheckTutorialCompletedById(9))
            {
                TutorialManager.Instance.UpdateTutorialProgress(9);
            }
        }
	}
}
