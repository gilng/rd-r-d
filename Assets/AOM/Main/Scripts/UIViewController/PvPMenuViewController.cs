﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Parse;
using UnityEngine.Analytics; // Device use
using Zenject;

namespace Ignite.AOM
{
    public class PvPMenuViewController : UIOverlayViewController
    {
        private SelectionObjectController.Factory _selectionObjectControllerFactory;
        private IUserService _userService;
        private IFriendService _friendService;
        private IPvPChallengeService _pvpChallengeService;
        private SocialManager _socialManager;
        private ArtifactCore _artifactCore;

        [Inject]
        public void ConstructorSafeAttribute(SelectionObjectController.Factory selectionObjectControllerFactory,
            IUserService userService, IFriendService friendService, IPvPChallengeService pvpChallengeService,
            SocialManager socialManager, ArtifactCore artifactCore)
        {
            _selectionObjectControllerFactory = selectionObjectControllerFactory;
            _userService = userService;
            _friendService = friendService;
            _pvpChallengeService = pvpChallengeService;
            _socialManager = socialManager;
            _artifactCore = artifactCore;
        }

        // Main panel
        private int currentFP;

        public Text nameLabel;
        public Text fpValueLabel;
        public Text rubyValueLabel;

        private readonly int[] _prizeNeededFp = {100, 400, 1000, 1800, 3000};
        public Slider fpSlider;
        public GameObject[] prizeLock, prizeUnlock;
        public Transform fpPos;

        public Image[] btnGift;

        public Button challengeTabButton;
        public Button receiveTabButton;
        public Text challengeTabLabel;
        public Text receiveTabLabel;
        public GameObject notificationIndicator;
        public Text receivedCountText;

        public Button refreshListBtn;

        public GridLayoutGroup tableLayoutGrid;
        public RectTransform tableLayoutRect;
        public RectTransform tableScrollViewRect;

        private readonly List<PvPTableCell> _allPvPCell = new List<PvPTableCell>();
        private readonly List<BattleLog> _allBattleLog = new List<BattleLog>();
        private bool battleLogListLoaded = false;

        public GameObject friendListGroup;

        public Text btnInviteLabel;
        private int friendList_count;
        private int friendList_max;

        public Text noReceiveLabel;

        public GameObject msgPanel;
        public Text msgLabel;
        public GameObject msgRetryBtn;

        public Dictionary<PvPStatus, Sprite> sp_actionBtn;

        // Referral Panel
        public GameObject referralPanel;

        public Text referral_playerId;
        public InputField search_FriendId;

        // Cost Panel
        private bool isChallengeDetail = false;

        private int costPanelContent = 0;

        public GameObject costPanel;
        public Text cost_title;
        public Text cost_subtitle;
        public Button costBtn;
        public Text costBtn_label;
        public Text costBtn_price;
        public Text cost_remainGems;

        private const int PriceExtendFriendList = 500;
        private const int PriceResetChallengeCd = 100;

        private TimeSpan remainCD;

        // Gift Panel
        public GameObject giftPanel;

        public Image giftIcon;
        public Text giftNeedFP;
        public Text giftDesc;
        public Button giftBtn;
        public Text giftBtn_label;

        private int giftId = 0;
        private bool collectGift = false;

        // Info Panel
        public GameObject infoPanel;

        // Remove Friend Panel
        private PvPTableCell selectedCell;

        public GameObject removeFriendPanel;
        public Text removeFriend_content;

        private int _currentTabType = 0;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            AddListener();

            if (_userService.CurrentUserSummary != null)
            {
                nameLabel.text = _userService.CurrentUserSummary.DisplayName;

                rubyValueLabel.text = _userService.CurrentUserSummary.Ruby.ToString("000000");

                RefreshFPBar(_userService.CurrentUserSummary.FriendPoint);
            }

            if (referralPanel.activeSelf)
                referralPanel.SetActive(false);

            if (costPanel.activeSelf)
                costPanel.SetActive(false);

            if (msgPanel.activeSelf)
                msgPanel.SetActive(false);

            noReceiveLabel.gameObject.SetActive(false);

            referral_playerId.text = _socialManager.CurrentParseUser.UserId;

            _currentTabType = _pvpChallengeService.SelectedChallenge != null ? 1 : 0;

            SwitchTab(_currentTabType);

            _pvpChallengeService.ResetSelectedData();
        }

        void AddListener()
        {
            EventManager.OnFriendTableRefresh += RefreshPvPData;
        }

        void RemoveListener()
        {
            EventManager.OnFriendTableRefresh -= RefreshPvPData;
        }

        public void RefreshPvPData()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            RefreshUserSummary();

            challengeTabButton.interactable = false;
            receiveTabButton.interactable = false;
            refreshListBtn.interactable = false;

            foreach (PvPTableCell cell in _allPvPCell)
            {
                Destroy(cell.gameObject);
            }

            _allPvPCell.Clear();

            LoadDataList(_currentTabType);
        }

        void RefreshUserSummary()
        {
            _userService.FindUserSummary(userSummary =>
            {
                if (this != null)
                {
                    nameLabel.text = userSummary.DisplayName;

                    rubyValueLabel.text = userSummary.Ruby.ToString("000000");

                    RefreshFPBar(userSummary.FriendPoint);

                    friendList_max = userSummary.FriendLimit;
                }
            });
        }

        void RefreshFPBar(int fpCount)
        {
            currentFP = fpCount;
            fpValueLabel.text = currentFP.ToString("000000");
            float displayFP = (currentFP > _prizeNeededFp[4]) ? (float) _prizeNeededFp[4] : (float) currentFP;

            int artifactBalance = ArtifactAssets.Artifact033.GetBalance();

            for (int i = 0; i < 5; i++)
            {
                if (i == 0)
                {
                    if (displayFP < _prizeNeededFp[i])
                        fpSlider.value = (displayFP / _prizeNeededFp[i]) * 0.2f;
                }
                else
                {
                    if (displayFP > _prizeNeededFp[i - 1] && displayFP <= _prizeNeededFp[i])
                    {
                        float _fp1 = displayFP - _prizeNeededFp[i - 1];
                        float _fp2 = _prizeNeededFp[i] - _prizeNeededFp[i - 1];

                        fpSlider.value = (float) ((0.2f * i) + (_fp1 / _fp2) * 0.2f);
                    }
                }

                prizeLock[i].SetActive(currentFP < _prizeNeededFp[i]);
                prizeUnlock[i].SetActive(currentFP >= _prizeNeededFp[i]);

                btnGift[i].color = currentFP >= _prizeNeededFp[i] && PlayerPrefs.GetInt("PvP_Gift_" + i, 0) != 0
                    ? Color.gray
                    : Color.white;

                // Check owned this gift
                if ((i == 0 && artifactBalance != 0) || (i == 2 && artifactBalance != 1) ||
                    (i == 4 && artifactBalance != 2) || (i == 1 && AOMAssets.PvPKey001GIVG.GetBalance() != 0) ||
                    (i == 3 && AOMAssets.PvPKey002GIVG.GetBalance() != 0))
                    btnGift[i].color = Color.gray;
            }
        }

        public void UpdateUserFP(int val)
        {
            _userService.UpdateFriendPoint(val, userSummary =>
            {
                if (userSummary != null)
                {
                    if (this != null)
                    {
                        RefreshFPBar(userSummary.FriendPoint);
                    }
                }
            });
        }

        void RefreshReceivedChallengeList()
        {
            _pvpChallengeService.FindAllReceivedChallenge(
                battleLogList =>
                {
                    if (this != null)
                    {
                        UpdateNotificationIndicator(battleLogList.Count);
                    }
                },
                error =>
                {
                    if (this != null)
                    {
                        UpdateNotificationIndicator(0);
                    }
                });
        }

        void UpdateNotificationIndicator(int receivedCount)
        {
            if (receivedCount > 0)
            {
                notificationIndicator.SetActive(true);
                receivedCountText.text = receivedCount > 9 ? "9+" : receivedCount.ToString();
            }
            else
            {
                notificationIndicator.SetActive(false);
            }
        }

        public void SwitchTab(int tabType = 0)
        {
            _currentTabType = tabType;

            // Challenge Tab
            challengeTabButton.interactable = false;
            challengeTabButton.GetComponent<Image>().sprite = (_currentTabType == 0)
                ? challengeTabButton.GetComponent<Button>().spriteState.highlightedSprite
                : challengeTabButton.GetComponent<Button>().spriteState.pressedSprite;
            challengeTabButton.gameObject.GetComponent<RectTransform>().anchoredPosition = (_currentTabType == 0)
                ? new Vector2(-365f, 958f)
                : new Vector2(-365f, 945f);

            challengeTabLabel.color = (_currentTabType == 0)
                ? new Color(1, 1, 1)
                : new Color(106f / 255f, 18f / 255f, 53f / 255f);

            // Receive Tab
            receiveTabButton.interactable = false;
            receiveTabButton.GetComponent<Image>().sprite = (_currentTabType == 1)
                ? receiveTabButton.GetComponent<Button>().spriteState.highlightedSprite
                : receiveTabButton.GetComponent<Button>().spriteState.pressedSprite;
            receiveTabButton.gameObject.GetComponent<RectTransform>().anchoredPosition = (_currentTabType == 1)
                ? new Vector2(-23.2f, 958f)
                : new Vector2(-23.2f, 945f);

            receiveTabLabel.color = (_currentTabType == 1)
                ? new Color(1, 1, 1)
                : new Color(18f / 255f, 84f / 255f, 106f / 255f);

            friendListGroup.SetActive(_currentTabType == 0);

            RefreshPvPData();
        }

        public void LoadDataList(int listType)
        {
            msgPanel.SetActive(true);

            _allBattleLog.Clear();
            battleLogListLoaded = false;

            if (SocialManager.CheckForInternetConnection())
            {
                msgLabel.text = LanguageManager.Instance.GetTextValue("pvp_loading");

                if (msgRetryBtn.activeSelf)
                    msgRetryBtn.SetActive(false);

                if (_socialManager.parseSignedIn)
                {
                    Action callback = () =>
                    {
                        if (this != null)
                        {
                            challengeTabButton.interactable = _currentTabType != 0;
                            receiveTabButton.interactable = _currentTabType != 1;

                            refreshListBtn.interactable = true;
                        }
                    };

                    Action<string> failCallback = errorCode =>
                    {
                        if (this != null)
                        {
                            msgLabel.text = LanguageManager.Instance.GetTextValue("pvp_errorMsg_LoadParseFail");

                            if (!msgRetryBtn.activeSelf)
                                msgRetryBtn.SetActive(true);

                            if (errorCode == "TimeOut")
                            {
                                Action retryAction = () => { LoadDataList(listType); };
                                ShowTimeOutPopup(retryAction);
                            }
                        }
                    };

                    if (listType == 0)
                    {
                        _friendService.FindAllFriendRelation(
                            friendRelations =>
                            {
                                if (this != null)
                                {
                                    List<ParseUser> friendUserDataList = new List<ParseUser>();

                                    foreach (var friendRelationData in friendRelations)
                                    {
                                        friendUserDataList.AddRange(
                                            friendRelationData.Users.Where(
                                                user => user.ObjectId != ParseUser.CurrentUser.ObjectId));
                                    }

                                    int increasedFriendCount = friendRelations.Count -
                                                               (int)
                                                               GamePlayStatManager.Instance.Stat[
                                                                   StatType.MaxPvPAddedFriend];
                                    if (increasedFriendCount > 0)
                                    {
                                        GamePlayStatManager.Instance.ReachedMaxPvPAddedFriend(friendRelations.Count);
                                        UpdateUserFP(increasedFriendCount * 10);
                                    }

                                    _friendService.FindAllFriendUserSummary(friendUserDataList, friendUserSummarys =>
                                        {
                                            if (this != null)
                                            {
                                                LoadFriendRelationSuccess(friendRelations.ToList(),
                                                    friendUserSummarys.ToList());

                                                callback();
                                            }
                                        },
                                        failCallback
                                    );
                                }
                            },
                            failCallback
                        );

                        _pvpChallengeService.FindLatestBattleLogPerFriend(battleLogList =>
                            {
                                if (this != null)
                                {
                                    battleLogListLoaded = true;
                                    _allBattleLog.AddRange(battleLogList);

                                    ApplyBattleLogToFriendRelationTable(_allBattleLog);

                                    callback();
                                }
                            },
                            failCallback);

                        RefreshReceivedChallengeList();
                    }
                    else if (listType == 1)
                    {
                        _pvpChallengeService.FindAllReceivedChallenge(battleLogList =>
                            {
                                if (this != null)
                                {
                                    battleLogListLoaded = true;
                                    _allBattleLog.AddRange(battleLogList);

                                    if (_allBattleLog.Count > 0)
                                    {
                                        List<ParseUser> receivedChallengeDataList =
                                            _allBattleLog.Select(receivedChallenge => receivedChallenge.Sender)
                                                .ToList();

                                        _friendService.FindAllFriendUserSummary(receivedChallengeDataList,
                                            friendUserSummarys =>
                                            {
                                                if (this != null)
                                                {
                                                    UpdateNotificationIndicator(_allBattleLog.ToList().Count);
                                                    LoadAllReceivedSuccess(_allBattleLog.ToList(),
                                                        friendUserSummarys.ToList());

                                                    callback();
                                                }
                                            },
                                            failCallback
                                        );
                                    }
                                    else
                                    {
                                        msgPanel.SetActive(false);

                                        noReceiveLabel.gameObject.SetActive(true);
                                        noReceiveLabel.text = LanguageManager.Instance.GetTextValue("pvp_no_received");

                                        callback();
                                    }
                                }
                            },
                            failCallback);
                    }
                }
                else
                {
                    // Create Account
                    Action successCallback = () =>
                    {
                        if (this != null)
                        {
                            LoadDataList(listType);
                        }
                    };

                    Action failCallback = () =>
                    {
                        if (this != null)
                        {
                            challengeTabButton.interactable = _currentTabType != 0;
                            receiveTabButton.interactable = _currentTabType != 1;
                            refreshListBtn.interactable = true;

                            msgLabel.text = LanguageManager.Instance.GetTextValue("pvp_errorMsg_LoadParseFail");

                            if (!msgRetryBtn.activeSelf)
                                msgRetryBtn.SetActive(true);
                        }
                    };

                    _socialManager.ParseSignUp(successCallback, failCallback);
                }
            }
            else
            {
                challengeTabButton.interactable = _currentTabType != 0;
                receiveTabButton.interactable = _currentTabType != 1;
                refreshListBtn.interactable = true;

                msgLabel.text = LanguageManager.Instance.GetTextValue("pvp_errorMsg_NoInternet");

                if (!msgRetryBtn.activeSelf)
                    msgRetryBtn.SetActive(true);
            }
        }

        void LoadFriendRelationSuccess(List<FriendRelation> friendRelationDataList,
            List<UserSummary> userSummaryDataList)
        {
            msgPanel.SetActive(false);

            noReceiveLabel.gameObject.SetActive(false);

            LoadCellActionButtonIcon();

            referral_playerId.text = _socialManager.CurrentParseUser.UserId;

            friendList_count = friendRelationDataList.Count;

            if (friendList_max == 0)
                friendList_max = _userService.CurrentUserSummary.FriendLimit;

            btnInviteLabel.text = string.Format(LanguageManager.Instance.GetTextValue("pvp_main_inviteBtn"),
                friendList_count, friendList_max);

            CreateFriendRelationTable(friendRelationDataList, userSummaryDataList);

            if (battleLogListLoaded)
                ApplyBattleLogToFriendRelationTable(_allBattleLog);

            ResizeTableLayoutRect();
        }

        void LoadAllReceivedSuccess(List<BattleLog> dataList, List<UserSummary> senderUserSummaryList)
        {
            msgPanel.SetActive(false);

            noReceiveLabel.gameObject.SetActive(false);

            LoadCellActionButtonIcon();

            CreateReceivedTable(dataList, senderUserSummaryList);

            ResizeTableLayoutRect();
        }

        void LoadCellActionButtonIcon()
        {
            Sprite[] thumbnailIcon;
            string[] thumbnailIconName;

            thumbnailIcon = Resources.LoadAll<Sprite>("Sprites/overlay_ui_pvpMenu/overlay_ui_pvpMenuCell");
            thumbnailIconName = new string[thumbnailIcon.Length];

            for (int i = 0; i < thumbnailIconName.Length; i++)
            {
                thumbnailIconName[i] = thumbnailIcon[i].name;
            }

            sp_actionBtn = new Dictionary<PvPStatus, Sprite>();
            sp_actionBtn.Add(PvPStatus.AvailableChallenge,
                thumbnailIcon[Array.IndexOf(thumbnailIconName, "action-btn-battle")]);
            sp_actionBtn.Add(PvPStatus.ChallengeExpireCD,
                thumbnailIcon[Array.IndexOf(thumbnailIconName, "action-btn-waiting-receive")]);
            sp_actionBtn.Add(PvPStatus.NextChallengeCD,
                thumbnailIcon[Array.IndexOf(thumbnailIconName, "action-btn-count-down")]);
            sp_actionBtn.Add(PvPStatus.ReceiveChallenge,
                thumbnailIcon[Array.IndexOf(thumbnailIconName, "action-btn-receive-challenge")]);
            sp_actionBtn.Add(PvPStatus.Result_Win,
                thumbnailIcon[Array.IndexOf(thumbnailIconName, "action-btn-result-win")]);
            sp_actionBtn.Add(PvPStatus.Result_Lose,
                thumbnailIcon[Array.IndexOf(thumbnailIconName, "action-btn-result-lose")]);
        }

        void CreateFriendRelationTable(IList<FriendRelation> friendRelationDataList,
            IList<UserSummary> friendUserSummaryDataList)
        {
            PvPTableCell pvpTableCell_PvP = _selectionObjectControllerFactory.Create("PvPTableCell") as PvPTableCell;

            pvpTableCell_PvP.gameObject.name = "Friend_NPC";
            pvpTableCell_PvP.gameObject.transform.SetParent(tableLayoutGrid.transform, false);
            pvpTableCell_PvP.gameObject.transform.localPosition = Vector3.zero;
            pvpTableCell_PvP.InitNPC(this);

            _allPvPCell.Add(pvpTableCell_PvP);

            foreach (var friendRelationData in friendRelationDataList)
            {
                PvPTableCell pvpTableCell = _selectionObjectControllerFactory.Create("PvPTableCell") as PvPTableCell;

                pvpTableCell.gameObject.name = friendRelationData.ObjectId;
                pvpTableCell.gameObject.transform.SetParent(tableLayoutGrid.transform, false);
                pvpTableCell.gameObject.transform.localPosition = Vector3.zero;

                UserSummary friendUserSummary = null;

                foreach (var currentFriendUserSummary in friendUserSummaryDataList)
                {
                    foreach (var user in friendRelationData.Users)
                    {
                        if (user.ObjectId == currentFriendUserSummary.User.ObjectId)
                        {
                            friendUserSummary = currentFriendUserSummary;

                            break;
                        }
                    }
                }

                pvpTableCell.InitFriendRelation(this, friendRelationData, friendUserSummary);

                _allPvPCell.Add(pvpTableCell);
            }
        }

        void ApplyBattleLogToFriendRelationTable(IList<BattleLog> sentChallengeDataList)
        {
            if (_allPvPCell.Count > 0)
            {
                foreach (var pvpCell in _allPvPCell)
                {
                    if (!pvpCell.isNPC)
                    {
                        BattleLog sentChallengeData =
                            sentChallengeDataList.FirstOrDefault(
                                challengeData => challengeData.Receiver.ObjectId == pvpCell.friendUser.ObjectId);

                        pvpCell.UpdateLatestBattleRecord(sentChallengeData);
                    }
                }
            }
        }

        void CreateReceivedTable(List<BattleLog> receivedDataList, List<UserSummary> senderUserSummaryList)
        {
            for (int i = 0; i < receivedDataList.Count; i++)
            {
                PvPTableCell pvpTableCell = _selectionObjectControllerFactory.Create("PvPTableCell") as PvPTableCell;

                pvpTableCell.gameObject.name = "Challenge_" + (i + 1);
                pvpTableCell.gameObject.transform.SetParent(tableLayoutGrid.transform, false);
                pvpTableCell.gameObject.transform.localPosition = Vector3.zero;

                foreach (var senderUserSummary in senderUserSummaryList)
                {
                    if (senderUserSummary.User.ObjectId == receivedDataList[i].Sender.ObjectId)
                    {
                        pvpTableCell.InitReceivedChallenge(this, senderUserSummary, receivedDataList[i]);
                        break;
                    }
                }

                _allPvPCell.Add(pvpTableCell);
            }
        }

        void ResizeTableLayoutRect()
        {
            float tableLayoutRectSizeX = tableLayoutGrid.cellSize.x;
            float tableLayoutRectSizeY = (tableLayoutGrid.transform.childCount * tableLayoutGrid.cellSize.y) +
                                         ((tableLayoutGrid.transform.childCount - 1) * tableLayoutGrid.spacing.y);

            if (tableScrollViewRect.sizeDelta.y > tableLayoutRectSizeY)
                tableLayoutRectSizeY = tableScrollViewRect.sizeDelta.y;

            tableLayoutRect.sizeDelta = new Vector2(tableLayoutRectSizeX, tableLayoutRectSizeY);
            tableLayoutRect.transform.localPosition = new Vector3(tableLayoutRect.transform.localPosition.x, 0,
                tableLayoutRect.transform.localPosition.z);
        }

        public void OpenWaitingReceiveCDPanel(TimeSpan _remain)
        {
            remainCD = _remain;

            string remainTime = string.Format("{0:00}:{1:00}:{2:00}", remainCD.Hours, remainCD.Minutes,
                remainCD.Seconds);

            cost_title.text = string.Format(LanguageManager.Instance.GetTextValue("pvp_waitingReceiveCD_msg"),
                PriceResetChallengeCd);
            cost_subtitle.text = string.Format(
                LanguageManager.Instance.GetTextValue("pvp_waitingReceiveCD_remainTime"), remainTime);
            cost_remainGems.text = "";
            costBtn.gameObject.SetActive(false);

            costPanelContent = 1;

            costPanel.SetActive(true);
        }

        public void OpenResetChallengeCDPanel(TimeSpan _remain, PvPTableCell _cell)
        {
            remainCD = _remain;

            string remainTime = string.Format("{0:00}:{1:00}:{2:00}", remainCD.Hours, remainCD.Minutes,
                remainCD.Seconds);

            cost_title.text = string.Format(LanguageManager.Instance.GetTextValue("pvp_resetChallengeCD_msg"),
                PriceResetChallengeCd);
            cost_subtitle.text = string.Format(
                LanguageManager.Instance.GetTextValue("pvp_resetChallengeCD_remainTime"), remainTime);
            cost_remainGems.text = string.Format(LanguageManager.Instance.GetTextValue("current_gems"),
                UserManager.Instance.CurrentUserSummary != null
                    ? UserManager.Instance.CurrentUserSummary.Gem.ToString()
                    : AOMStoreInventory.GetDiamondBalance().ToString());
            costBtn_label.text = LanguageManager.Instance.GetTextValue("pvp_resetChallengeCD_btn");
            costBtn_price.text = PriceResetChallengeCd.ToString();
            costBtn.gameObject.SetActive(true);
            costBtn.onClick.AddListener(() => CostGems_ResetChallengeCD(_cell));

            costPanelContent = 2;

            costPanel.SetActive(true);
        }

        void Update()
        {
            if (costPanel.activeSelf && costPanelContent != 0)
            {
                remainCD = TimeSpan.FromSeconds(remainCD.TotalSeconds - Time.unscaledDeltaTime);

                if (remainCD.TotalSeconds > 0)
                {
                    string remainTime = string.Format("{0:00}:{1:00}:{2:00}", remainCD.Hours, remainCD.Minutes,
                        remainCD.Seconds);
                    cost_subtitle.text =
                        string.Format(LanguageManager.Instance.GetTextValue("pvp_resetChallengeCD_remainTime"),
                            remainTime);
                }
                else
                {
                    costPanelContent = 0;
                    CloseInternalPanel(false);
                }
            }
        }

        void CostGems_ResetChallengeCD(PvPTableCell _cell)
        {
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                if (UserManager.Instance.CurrentUserSummary.Gem >= PriceResetChallengeCd)
//                {
                    //SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

//                    if (SocialManager.CheckForInternetConnection())
//                    {
                        UserManager.Instance.TakeGems(PriceResetChallengeCd, () =>
                        {
                            _cell.ResetChallengeCD();

                            // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                        Analytics.CustomEvent("useGems", new Dictionary<string, object>
                        {
                            { "type", "Reset PvP Challenge CD" },
                        });
#endif
                            costBtn.onClick.RemoveListener(() => CostGems_ResetChallengeCD(_cell));

                            CloseInternalPanel(true);
                        });
//                    }
//                    else
//                    {
//                        ShowNoInternetPopup();
//                    }
//                }
//            }
//            else
//            {
//                SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
//                LoadOverlayView("ShopMenu", false, Time.timeScale == 0);
//            }
        }

        public void OpenReferralPanel()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (friendList_count < friendList_max)
            {
                if (!referralPanel.activeSelf)
                    referralPanel.SetActive(true);
            }
            else
            {
                cost_title.text = string.Format(LanguageManager.Instance.GetTextValue("pvp_extendFriendList_msg"),
                    PriceExtendFriendList);
                cost_subtitle.text = friendList_max.ToString() + " > " + (friendList_max + 1).ToString();
                cost_remainGems.text = string.Format(LanguageManager.Instance.GetTextValue("current_gems"),
                    UserManager.Instance.CurrentUserSummary != null
                        ? UserManager.Instance.CurrentUserSummary.Gem.ToString()
                        : AOMStoreInventory.GetDiamondBalance().ToString());
                costBtn_label.text = LanguageManager.Instance.GetTextValue("pvp_extendFriendList_btn");
                costBtn_price.text = PriceExtendFriendList.ToString();
                costBtn.gameObject.SetActive(true);
                costBtn.onClick.AddListener(() => CostGems_ExtendFriendList());

                costPanelContent = 0;

                costPanel.SetActive(true);
            }
        }

        void CostGems_ExtendFriendList()
        {
            if (UserManager.Instance.CurrentUserSummary != null)
            {
                if (UserManager.Instance.CurrentUserSummary.Gem >= PriceResetChallengeCd)
                {
                    if (SocialManager.CheckForInternetConnection())
                    {
                        _userService.CurrentUserSummary.Gem -= PriceExtendFriendList;
                        _userService.CurrentUserSummary.FriendLimit += 1;

                        _userService.UpdateUserSummary(_userService.CurrentUserSummary, (e) =>
                        {
                            if (e != null)
                            {
                                _userService.CurrentUserSummary.Gem += PriceExtendFriendList;
                                _userService.CurrentUserSummary.FriendLimit -= 1;
                                Debug.LogError("Update UserSummary fail");
                            }
                            else
                            {
                                SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                                AOMStoreInventory.ResetDiamond(_userService.CurrentUserSummary.Gem);

                                if (this != null)
                                {
                                    friendList_max = _userService.CurrentUserSummary.FriendLimit;

                                    btnInviteLabel.text =
                                        string.Format(LanguageManager.Instance.GetTextValue("pvp_main_inviteBtn"),
                                            friendList_count.ToString(), friendList_max.ToString());
                                }

                                // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                                Analytics.CustomEvent("useGems", new Dictionary<string, object>
                                {
                                    { "type", "Extend PvP Friend List" },
                                });
#endif
                                costBtn.onClick.RemoveListener(() => CostGems_ExtendFriendList());

                                CloseInternalPanel(true);
                            }
                        });
                    }
                    else
                    {
                        ShowNoInternetPopup();
                    }
                }
            }
            else
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
                LoadOverlayView("ShopMenu", false, Time.timeScale == 0);
            }
        }

        public void PressReferralBtn(int index)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (SocialManager.CheckForInternetConnection() || index == 3)
            {
                BranchManager.ReferralLinkShareVia(referral_playerId.text, nameLabel.text, index);

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                Analytics.CustomEvent("inviteFriend", new Dictionary<string, object>
                {
                    {"type", "InviteByReferralLink"},
                });
#endif

                CloseInternalPanel(false);
            }
            else
            {
                ShowNoInternetPopup();
            }
        }

        public void CopyUserID()
        {
            BranchManager.CopyTextToClipboard(referral_playerId.text);
        }

        public void InviteFriend()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (search_FriendId.text == "") return;

            if (SocialManager.CheckForInternetConnection())
            {
                Action<FriendRelation> AddSuccessCallback = friendRelation =>
                {
                    if (this != null)
                    {
                        CloseInternalPanel(false);

                        EventManager.FriendTableRefresh();

                        foreach (var user in friendRelation.Users)
                        {
                            if (user.ObjectId == search_FriendId.text)
                            {
                                _friendService.FindFriendUserSummary(user, summary =>
                                {
                                    if (this != null)
                                    {
                                        var msgPopupViewController =
                                            LoadOverlayViewController("MsgPopupOverlay", false, Time.timeScale == 0) as
                                                MsgPopupViewController;
                                        msgPopupViewController.InputMsg("fds_title_add",
                                            string.Format(LanguageManager.Instance.GetTextValue("fds_msg_add_success"),
                                                summary.DisplayName != ""
                                                    ? summary.DisplayName
                                                    : search_FriendId.text));
                                    }

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                                Analytics.CustomEvent("inviteFriend", new Dictionary<string, object>
                                {
                                    { "type", "ByUserId" },
                                });
#endif
                                }, error =>
                                {
                                    if (this != null)
                                    {
                                        var msgPopupViewController =
                                            LoadOverlayViewController("MsgPopupOverlay", false, Time.timeScale == 0) as
                                                MsgPopupViewController;
                                        msgPopupViewController.InputMsg("fds_title_add",
                                            string.Format(LanguageManager.Instance.GetTextValue("fds_msg_add_success"),
                                                search_FriendId.text));
                                    }
                                });
                                break;
                            }
                        }
                    }
                };

                Action<string> AddFailCallback = errorCode =>
                {
                    if (this != null)
                    {
                        string errorMsgKey = "";

                        if (errorCode == "FriendRelationAlreadyExists")
                            errorMsgKey = "fds_msg_add_fail_alreadyExist";
                        else if (errorCode == "UserFriendListReachedMaximum")
                            errorMsgKey = "fds_msg_add_fail_userListMax";
                        else if (errorCode == "TargetUserFriendListReachedMaximum")
                            errorMsgKey = "fds_msg_add_fail_friendListMax";
                        else if (errorCode == "TimeOut")
                            errorMsgKey = "errorMsg_TimeOut";
                        else
                            errorMsgKey = "fds_msg_add_fail_invalidTarget";

                        var msgPopupViewController =
                            LoadOverlayViewController("MsgPopupOverlay", false, Time.timeScale == 0) as
                                MsgPopupViewController;
                        msgPopupViewController.InputMsg("fds_title_add",
                            string.Format(LanguageManager.Instance.GetTextValue(errorMsgKey), search_FriendId.text));
                    }
                };

                _friendService.AddFriend(search_FriendId.text, AddSuccessCallback, AddFailCallback);
            }
            else
            {
                ShowNoInternetPopup();
            }
        }

        public void OpenGiftPanel(int _id)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (!giftPanel.activeSelf)
                giftPanel.SetActive(true);

            giftId = _id;
            giftIcon.sprite = btnGift[giftId].sprite;

            giftNeedFP.text = (currentFP >= _prizeNeededFp[giftId])
                ? ("FP: " + _prizeNeededFp[giftId] + " / " + _prizeNeededFp[giftId])
                : ("FP: " + currentFP + " / " + _prizeNeededFp[giftId]);

            if (giftId == 0 || giftId == 2 || giftId == 4)
            {
                string id = ArtifactAssets.Artifact033.ItemId;

                giftDesc.alignment = TextAnchor.MiddleLeft;
                giftDesc.text = string.Format(LanguageManager.Instance.GetTextValue("pvp_gift_moetifactDesc"),
                                    LanguageManager.Instance.GetTextValue(id + "_name"),
                                    LanguageManager.Instance.GetTextValue(id + "_desc"))
                                + "\n\n" + string.Format(LanguageManager.Instance.GetTextValue(id + "_skill_desc"),
                                    (ArtifactAssets.Artifact033.BaseEffectValue * 100).ToString())
                                + "\n" + string.Format(LanguageManager.Instance.GetTextValue("Artifact_all_dmg"),
                                    (ArtifactAssets.Artifact033.BaseAllDamageValue * 100).ToString())
                                + "\n\n" + LanguageManager.Instance.GetTextValue("pvp_gift_AddFriendSlots");
            }
            else
            {
                giftDesc.alignment = TextAnchor.MiddleCenter;
                giftDesc.text = string.Format(LanguageManager.Instance.GetTextValue("pvp_gift_moetanDesc"),
                                    LanguageManager.Instance.GetTextValue((giftId == 1)
                                        ? "Boss_097_name"
                                        : "Boss_098_name"))
                                + "\n\n" + LanguageManager.Instance.GetTextValue("pvp_gift_AddFriendSlots");
            }

            collectGift = currentFP >= _prizeNeededFp[giftId] && PlayerPrefs.GetInt("PvP_Gift_" + giftId, 0) == 0;

            // Check owned this gift
            int artifactBalance = ArtifactAssets.Artifact033.GetBalance();

            if ((giftId == 0 && artifactBalance != 0) || (giftId == 2 && artifactBalance != 1) ||
                (giftId == 4 && artifactBalance != 2) || (giftId == 1 && AOMAssets.PvPKey001GIVG.GetBalance() != 0) ||
                (giftId == 3 && AOMAssets.PvPKey002GIVG.GetBalance() != 0))
            {
                collectGift = false;
                btnGift[giftId].color = Color.gray;
            }

            giftBtn_label.text =
                LanguageManager.Instance.GetTextValue(collectGift ? "pvp_gift_colletBtn" : "pvp_gift_closeBtn");
        }

        public void CloseGiftPanel()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (collectGift)
            {
                if (SocialManager.CheckForInternetConnection())
                {
                    PlayerPrefs.SetInt("PvP_Gift_" + giftId, 1);

                    GachaResultViewController itemViewController =
                        LoadOverlayViewController("GachaResultMenuView", false,
                            Time.timeScale == 0) as GachaResultViewController;

                    if (giftId == 0 || giftId == 2 || giftId == 4)
                    {
                        int artifactBalance = ArtifactAssets.Artifact033.GetBalance();

                        if ((giftId == 0 && artifactBalance == 0) || (giftId == 2 && artifactBalance == 1) ||
                            (giftId == 4 && artifactBalance == 2))
                        {
                            ArtifactVG moetifactVG = _artifactCore.GiveById("AR033");

                            itemViewController.SetItemId(moetifactVG, GachaType.Moetifact);
                        }
                    }
                    else if (giftId == 1)
                    {
                        if (AOMAssets.PvPKey001GIVG.GetBalance() == 0)
                        {
                            AOMAssets.PvPKey001GIVG.Give(1);

                            if (GameManager.Instance != null)
                                GameManager.Instance.enemyPool.KeyUnlock(AOMAssets.PvPKey001GIVG.ItemId);

                            itemViewController.ShowGachaEffect(btnGift[giftId].sprite,
                                LanguageManager.Instance.GetTextValue("Boss_097_name"));
                        }
                    }
                    else if (giftId == 3)
                    {
                        if (AOMAssets.PvPKey002GIVG.GetBalance() == 0)
                        {
                            AOMAssets.PvPKey002GIVG.Give(1);

                            if (GameManager.Instance != null)
                                GameManager.Instance.enemyPool.KeyUnlock(AOMAssets.PvPKey002GIVG.ItemId);

                            itemViewController.ShowGachaEffect(btnGift[giftId].sprite,
                                LanguageManager.Instance.GetTextValue("Boss_098_name"));
                        }
                    }

                    btnGift[giftId].color = Color.gray;

                    // Friend List maximum +2
                    _userService.UpdateFriendLimit(2, userSummary =>
                    {
                        if (userSummary != null)
                        {
                            if (this != null)
                            {
                                friendList_max = userSummary.FriendLimit;

                                btnInviteLabel.text =
                                    string.Format(LanguageManager.Instance.GetTextValue("pvp_main_inviteBtn"),
                                        friendList_count.ToString(), friendList_max.ToString());
                            }
                        }
                    });
                }
                else
                {
                    ShowNoInternetPopup();
                }
            }

            if (giftPanel.activeSelf)
                giftPanel.SetActive(false);
        }

        public void OpenInfoPanel()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (!infoPanel.activeSelf)
                infoPanel.SetActive(true);
        }

        public void CloseInternalPanel(bool isBtn)
        {
            if (isBtn)
                SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (referralPanel.activeSelf)
                referralPanel.SetActive(false);

            search_FriendId.text = "";

            if (costPanel.activeSelf)
            {
                costBtn.onClick.RemoveAllListeners();

                costPanelContent = 0;
                costPanel.SetActive(false);
            }

            if (giftPanel.activeSelf)
            {
                giftPanel.SetActive(false);
            }

            if (infoPanel.activeSelf)
                infoPanel.SetActive(false);

            if (removeFriendPanel.activeSelf)
            {
                selectedCell = null;
                removeFriendPanel.SetActive(false);
            }
        }

        public void ChallengeAllAvailableFriends()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (SocialManager.CheckForInternetConnection())
            {
                List<ParseUser> availableFriends = new List<ParseUser>();

                foreach (PvPTableCell cell in _allPvPCell)
                {
                    if (!cell.isNPC && cell.pvpStatus == PvPStatus.AvailableChallenge)
                    {
                        availableFriends.Add(cell.friendUser);
                    }
                }

                if (availableFriends.Count > 0)
                {
                    _pvpChallengeService.InitMultipleChallenge(availableFriends,
                        () => { GameManager.Instance.LoadPvPScene(); });
                }
            }
            else
            {
                ShowNoInternetPopup();
            }
        }

        public void InitChallenge(ParseUser selectedFriendUser, string displayName, BattleLog selectedChallengeRecord)
        {
            if (SocialManager.CheckForInternetConnection())
            {
                _pvpChallengeService.InitChallenge(selectedFriendUser, displayName, selectedChallengeRecord,
                    () => { GameManager.Instance.LoadPvPScene(); });
            }
            else
            {
                ShowNoInternetPopup();
            }
        }

        public void AcceptChallengeResult(string recordId, bool isSender, Action successCallback = null,
            Action failCallback = null)
        {
            _pvpChallengeService.AcceptChallengeResult(recordId, isSender, () =>
                {
                    if (successCallback != null)
                        successCallback();
                },
                error =>
                {
                    Debug.Log(error.ToString());

                    if (error == "TimeOut")
                        ShowTimeOutPopup();

                    if (failCallback != null)
                        failCallback();
                });
        }

        public void SetChallengeExpired(string recordId, Action successCallback = null, Action failCallback = null)
        {
            _pvpChallengeService.SetChallengeExpired(recordId, () =>
                {
                    if (successCallback != null)
                        successCallback();
                },
                error =>
                {
                    Debug.Log(error.ToString());

                    if (error == "TimeOut")
                        ShowTimeOutPopup();

                    if (failCallback != null)
                        failCallback();
                });
        }

        public void RemoveFriendMode()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            for (int i = 0; i < _allPvPCell.Count; i++)
            {
                _allPvPCell[i].ShowRemoveButton();
            }
        }

        public void OpenRemoveFriendPanel(PvPTableCell _cell)
        {
            removeFriendPanel.SetActive(true);

            selectedCell = _cell;

            removeFriend_content.text = string.Format(LanguageManager.Instance.GetTextValue("pvp_remove_content"),
                _cell.nameLabel.text);
        }

        public void RemoveFriend()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (SocialManager.CheckForInternetConnection())
            {
                if (selectedCell == null) return;

                _friendService.Remove(
                    selectedCell.friendRelation,
                    () =>
                    {
                        if (this != null)
                        {
                            var msgPopupViewController =
                                LoadOverlayViewController("MsgPopupOverlay", false, Time.timeScale == 0) as
                                    MsgPopupViewController;

                            if (msgPopupViewController != null)
                            {
                                msgPopupViewController.InputMsg(
                                    LanguageManager.Instance.GetTextValue("fds_title_remove"),
                                    string.Format(LanguageManager.Instance.GetTextValue("fds_msg_remove_success"),
                                        selectedCell.friendUser.ObjectId));
                            }

                            foreach (var cell in _allPvPCell)
                            {
                                if (cell != selectedCell || cell.friendUser == null) continue;

                                _allPvPCell.Remove(cell);
                                Destroy(cell.gameObject);
                                break;
                            }

                            ResizeTableLayoutRect();

                            selectedCell = null;

                            friendList_count = _allPvPCell.Count - 1;
                            friendList_max = _userService.CurrentUserSummary.FriendLimit;

                            btnInviteLabel.text = string.Format(
                                LanguageManager.Instance.GetTextValue("pvp_main_inviteBtn"),
                                friendList_count, friendList_max);

                            removeFriendPanel.SetActive(false);
                        }
                    },
                    errorCode =>
                    {
                        if (this != null)
                        {
                            var msgPopupViewController =
                                LoadOverlayViewController("MsgPopupOverlay", false, Time.timeScale == 0) as
                                    MsgPopupViewController;

                            if (msgPopupViewController != null)
                            {
                                msgPopupViewController.InputMsg(
                                    LanguageManager.Instance.GetTextValue("fds_title_remove"),
                                    string.Format(LanguageManager.Instance.GetTextValue("fds_msg_remove_fail"),
                                        selectedCell.friendUser.ObjectId));
                            }
                        }
                    }
                );
            }
            else
            {
                ShowNoInternetPopup();
            }
        }

        public void RemoveBattleRecord(PvPTableCell targetCell)
        {
            _allPvPCell.Remove(targetCell);
            Destroy(targetCell.gameObject);

            ResizeTableLayoutRect();

            if (_allPvPCell.Count == 0)
            {
                noReceiveLabel.gameObject.SetActive(true);
                noReceiveLabel.text = LanguageManager.Instance.GetTextValue("pvp_no_received");
            }

            _pvpChallengeService.ReceivedChallengeCount = _allPvPCell.Count;
            UpdateNotificationIndicator(_pvpChallengeService.ReceivedChallengeCount);
        }

        public void ShowNoInternetPopup()
        {
            MsgPopupViewController msgPopupViewController =
                LoadOverlayViewController("MsgPopupOverlay", false, Time.timeScale == 0) as MsgPopupViewController;

            if (msgPopupViewController != null)
            {
                msgPopupViewController.InputMsg(LanguageManager.Instance.GetTextValue("saveLoadWarning_title"),
                    LanguageManager.Instance.GetTextValue("pvp_errorMsg_NoInternet"), "btn_ok", RefreshPvPData);
            }
        }

        public void ShowTimeOutPopup(Action retryAction = null)
        {
            var msgPopupViewController =
                LoadOverlayViewController("MsgPopupOverlay", false, Time.timeScale == 0) as MsgPopupViewController;

            if (msgPopupViewController != null)
            {
                msgPopupViewController.InputMsg(LanguageManager.Instance.GetTextValue("saveLoadWarning_title"),
                    LanguageManager.Instance.GetTextValue("errorMsg_TimeOut"),
                    retryAction != null ? "dropbox_errorMsg_Btn_Retry" : "btn_ok", retryAction);
            }
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            RemoveListener();

            if (HiddenMenu.Instance != null)
                HiddenMenu.Instance.UpdatePvPButtonIndicator(_pvpChallengeService.ReceivedChallengeCount);

            base.CloseView();

            if (Time.timeScale == 0)
                Time.timeScale = 1;
        }

        void OnApplicationFocus(bool focusStatus)
        {
            if (focusStatus)
            {
                RefreshPvPData();
            }
        }
    }
}