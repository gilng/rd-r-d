﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;
using Parse;

namespace Ignite.AOM
{
    public class MainMenuViewController : UIPageViewController
    {
        private SocialManager _socialManager;
        private IUserService _userService;
        private InitManager _initManager;
        private CloudDataManager _cloudDataManager;

        [Inject]
        public void ConstructorSafeAttribute(SocialManager socialManager, IUserService userService,
            InitManager initManager, CloudDataManager cloudDataManager)
        {
            _socialManager = socialManager;
            _userService = userService;
            _initManager = initManager;
            _cloudDataManager = cloudDataManager;
        }

        public GameObject MainMenuGroup;

        public GameObject loadGameMenuGroup_Android;
        public GameObject loadGameMenuGroup_iOS;

        public GameObject parseLoginGroup;
        public InputField parseAccountField;
        public InputField parsePasswordField;

        public GameObject BackBtn;

        //public GameObject loadingIndicator;
        public GameObject loadingSlider;
        public GameObject retryBtn;

        public GameObject appUpdateMsgOverlay;

        bool onSessionTokenInvalid = false;

        // Use this for initialization
        protected override void Start()
        {
            base.Start();

            AddEventListener();

            AppVersionVerify();
        }

        void AddEventListener()
        {
            EventManager.OnAppVersionVerifed += OnAppVersionVerifyCallback;

            EventManager.OnParseUserInvalidToken += OnParseUserInvalidToken;

            EventManager.OnGoogleSignIn += OnGoogleSignInCallback;
            EventManager.OnParseSignIn += OnParseSignInCallback;

            EventManager.OnSyncGameProgress += OnSyncGameProgressCallback;

            EventManager.OnVerfiyAssetBundleVersion += OnVerfiyAssetBundleVersion;
            EventManager.OnUpdateAssetDownloadProgress += OnUpdateAssetDownloadProgress;
            EventManager.OnAssetBundleDownload += OnAssetBundleDownloadCallback;
        }

        void RemoveEventListener()
        {
            EventManager.OnAppVersionVerifed -= OnAppVersionVerifyCallback;

            EventManager.OnParseUserInvalidToken -= OnParseUserInvalidToken;

            EventManager.OnGoogleSignIn -= OnGoogleSignInCallback;
            EventManager.OnParseSignIn -= OnParseSignInCallback;

            EventManager.OnSyncGameProgress -= OnSyncGameProgressCallback;

            EventManager.OnVerfiyAssetBundleVersion -= OnVerfiyAssetBundleVersion;
            EventManager.OnUpdateAssetDownloadProgress -= OnUpdateAssetDownloadProgress;
            EventManager.OnAssetBundleDownload -= OnAssetBundleDownloadCallback;
        }

        void AppVersionVerify()
        {
//            if (!loadingIndicator.activeSelf)
//                loadingIndicator.SetActive(true);
//
//            loadingIndicator.GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("game_loading");

            _initManager.VerifyAppVersion();
        }

        void OnAppVersionVerifyCallback(bool pass)
        {
            if (pass)
            {
                if (_socialManager.ParseUserTokenExpired)
                {
                    _initManager.ResetProgress();

                    if (onSessionTokenInvalid)
                    {
                        MsgPopupViewController msgPopupViewController =
                            LoadOverlayViewController("MsgPopupOverlay", false, false) as MsgPopupViewController;
                        msgPopupViewController.InputMsg("logout_title", "logout_msg", "btn_ok");

                        onSessionTokenInvalid = false;
                    }

                    _socialManager.ParseUserTokenExpired = false;
                }

                if (_initManager.NewInstall)
                {
                    if (_socialManager.parseSignedIn)
                    {
                        Action parseLogoutSuccessCallback = () =>
                        {
//                            if (loadingIndicator.activeSelf)
//                                loadingIndicator.SetActive(false);

                            MainMenuGroup.SetActive(true);
                        };

                        _socialManager.ParseLogout(parseLogoutSuccessCallback);
                    }
                    else
                    {
//                        if (loadingIndicator.activeSelf)
//                            loadingIndicator.SetActive(false);

                        MainMenuGroup.SetActive(true);
                    }
                }
                else
                {
                    //// For Test
                    //if (ParseUser.CurrentUser != null)
                    //{
                    //    ParseUser.LogOutAsync();
                    //}

                    if (SocialManager.CheckForInternetConnection())
                    {
                        if (_socialManager.parseSignedIn)
                        {
                            _userService.FindUserSummary(userSummary =>
                            {
                                if (userSummary != null)
                                {
                                    bool needUpdateUserSummary = false;

                                    if (userSummary.Gem != AOMStoreInventory.GetDiamondBalance())
                                    {
                                        needUpdateUserSummary = true;
                                        userSummary.Gem = AOMStoreInventory.GetDiamondBalance();
                                    }
                                    if (userSummary.Moecrystal != AOMStoreInventory.GetRelicBalance())
                                    {
                                        needUpdateUserSummary = true;
                                        userSummary.Moecrystal = AOMStoreInventory.GetRelicBalance();
                                    }

                                    if (needUpdateUserSummary)
                                    {
                                        _userService.UpdateUserSummary(userSummary, (e) =>
                                        {
                                            if (e != null)
                                            {
                                                Debug.LogError("Sync Gems or Moecrystals to UserSummary Fail");
                                            }
                                        });
                                    }
                                }

                                _initManager.SaveLocalProgressToParse(true);
                            });

                            PushBehaviour.Instance.Init();
                        }
                        else
                        {
                            _initManager.ParseSignUp();
                        }
                    }
                    else
                    {
                        _initManager.VerfiyAssetBundleVersion();
                    }
                }
            }
            else
            {
                appUpdateMsgOverlay.SetActive(true);
            }
        }

        public void RedirectToAppStore()
        {
            string url = "";

#if UNITY_IOS
			url = "https://itunes.apple.com/hk/app/attack-on-moe/id1059991997?mt=8";
#else
            url = "https://play.google.com/store/apps/details?id=game.ignite.aom&hl=zh-TW";
#endif

            Application.OpenURL(url);
        }

        void OnParseUserInvalidToken()
        {
            onSessionTokenInvalid = true;

            _socialManager.ParseUserTokenExpired = true;

            _initManager.NewInstall = true;
        }

        public void NewGame()
        {
            MainMenuGroup.SetActive(false);

//            if (!loadingIndicator.activeSelf)
//                loadingIndicator.SetActive(true);
//
//            loadingIndicator.GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("game_loading");

            if (SocialManager.CheckForInternetConnection())
            {
                _initManager.ParseSignUp();
            }
            else
            {
                _initManager.VerfiyAssetBundleVersion();
            }
        }

        public void LoadGame()
        {
            MainMenuGroup.SetActive(false);

#if UNITY_ANDROID
            loadGameMenuGroup_Android.SetActive(true);
            loadGameMenuGroup_iOS.SetActive(false);
#elif UNITY_IOS || UNITY_EDITOR
            loadGameMenuGroup_Android.SetActive(false);
            loadGameMenuGroup_iOS.SetActive(true);
#endif

            BackBtn.SetActive(true);
        }

        public void BackToPreviousGroup()
        {
            if (loadGameMenuGroup_Android.activeSelf || loadGameMenuGroup_iOS.activeSelf)
            {
                MainMenuGroup.SetActive(true);

#if UNITY_ANDROID
                loadGameMenuGroup_Android.SetActive(false);
#elif UNITY_IOS || UNITY_EDITOR
                loadGameMenuGroup_iOS.SetActive(false);
#endif

                BackBtn.SetActive(false);
            }
            else if (parseLoginGroup.activeSelf)
            {
#if UNITY_ANDROID
                loadGameMenuGroup_Android.SetActive(true);
                loadGameMenuGroup_iOS.SetActive(false);
#elif UNITY_IOS || UNITY_EDITOR
                loadGameMenuGroup_Android.SetActive(false);
                loadGameMenuGroup_iOS.SetActive(true);
#endif

                parseLoginGroup.SetActive(false);

                BackBtn.SetActive(true);
            }
        }

        public void ShowParseSignInGroup()
        {
#if UNITY_ANDROID
            loadGameMenuGroup_Android.SetActive(false);
#elif UNITY_IOS || UNITY_EDITOR
            loadGameMenuGroup_iOS.SetActive(false);
#endif

            parseLoginGroup.SetActive(true);

            BackBtn.SetActive(true);
        }

        public void ParseSignIn()
        {
            parseLoginGroup.SetActive(false);

            BackBtn.SetActive(false);

            _initManager.ParseSignIn(parseAccountField.text, parsePasswordField.text);
        }

        void OnParseSignInCallback(bool success)
        {
//            if (!loadingIndicator.activeSelf)
//                loadingIndicator.SetActive(true);
//
//            loadingIndicator.GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("game_loading");

            if (success)
            {
                if (_initManager.NewInstall)
                {
                    _initManager.LoadProgressFromParse();
                }
                else
                {
                    _initManager.VerfiyAssetBundleVersion();
                }
            }
            else
            {
                if (_socialManager.googleSignedIn)
                {
                    _initManager.LoadProgressFromGoogleCloud();
                }
                else
                {
//                    if (loadingIndicator.activeSelf)
//                        loadingIndicator.SetActive(false);

                    parseLoginGroup.SetActive(true);

                    BackBtn.SetActive(true);
                }
            }
        }

        public void GoogleSignIn()
        {
#if UNITY_ANDROID
            loadGameMenuGroup_Android.SetActive(false);
#elif UNITY_IOS || UNITY_EDITOR
            loadGameMenuGroup_iOS.SetActive(false);
#endif

            BackBtn.SetActive(false);

            _initManager.GoogleSignIn();
        }

        void OnGoogleSignInCallback(bool success)
        {
            if (success)
            {
                if (loadGameMenuGroup_Android.activeSelf || loadGameMenuGroup_iOS.activeSelf)
                {
#if UNITY_ANDROID
                    loadGameMenuGroup_Android.SetActive(false);
#elif UNITY_IOS || UNITY_EDITOR
                    loadGameMenuGroup_iOS.SetActive(false);
#endif
                    BackBtn.SetActive(false);
                }

//                if (!loadingIndicator.activeSelf)
//                    loadingIndicator.SetActive(true);
//
//                loadingIndicator.GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("game_loading");
            }
            else
            {
//                if (loadingIndicator.activeSelf)
//                    loadingIndicator.SetActive(false);

#if UNITY_ANDROID
                loadGameMenuGroup_Android.SetActive(true);
                loadGameMenuGroup_iOS.SetActive(false);
#elif UNITY_IOS || UNITY_EDITOR
                loadGameMenuGroup_Android.SetActive(false);
                loadGameMenuGroup_iOS.SetActive(true);
#endif

                BackBtn.SetActive(true);
            }
        }

        void OnSyncGameProgressCallback(bool success, bool allowSyncFail)
        {
            if (loadGameMenuGroup_Android.activeSelf || loadGameMenuGroup_iOS.activeSelf)
            {
#if UNITY_ANDROID
                loadGameMenuGroup_Android.SetActive(false);
#elif UNITY_IOS || UNITY_EDITOR
                loadGameMenuGroup_iOS.SetActive(false);
#endif
                BackBtn.SetActive(false);
            }

            if (success)
            {
                bool needUpdateUserSummary = false;

                if (_userService.CurrentUserSummary != null && _cloudDataManager.CurrentSaveData != null)
                {
                    if (_cloudDataManager.CurrentSaveData.diamonds > 0 && _userService.CurrentUserSummary.Gem != _cloudDataManager.CurrentSaveData.diamonds)
                    {
                        needUpdateUserSummary = true;
                        _userService.CurrentUserSummary.Gem = _cloudDataManager.CurrentSaveData.diamonds;
                    }
                    if (_cloudDataManager.CurrentSaveData.relics > 0 && _userService.CurrentUserSummary.Moecrystal != _cloudDataManager.CurrentSaveData.relics)
                    {
                        needUpdateUserSummary = true;
                        _userService.CurrentUserSummary.Moecrystal = _cloudDataManager.CurrentSaveData.relics;
                    }

                    if (needUpdateUserSummary)
                    {
                        _userService.UpdateUserSummary(_userService.CurrentUserSummary, (e) =>
                        {
                            if (e != null)
                            {
                                Debug.LogError("Sync Gems or Moecrystals to UserSummary Fail");
                            }
                            else
                            {
                                AOMStoreInventory.ResetDiamond(_userService.CurrentUserSummary.Gem);
                                AOMStoreInventory.ResetRelic(_userService.CurrentUserSummary.Moecrystal);

                                _cloudDataManager.CurrentSaveData.diamonds = 0;
                                _cloudDataManager.CurrentSaveData.relics = 0;
                                _initManager.SaveCurrentSaveDataToParse(false);
                            }
                        });
                    }
                }
                else if (_userService.CurrentUserSummary == null && _cloudDataManager.CurrentSaveData != null)
                {
                    _userService.FindUserSummary(UserSummary =>
                    {
                        if (UserSummary != null)
                        {
                            if (_cloudDataManager.CurrentSaveData.diamonds > 0 && UserSummary.Gem != _cloudDataManager.CurrentSaveData.diamonds)
                            {
                                needUpdateUserSummary = true;
                                UserSummary.Gem = _cloudDataManager.CurrentSaveData.diamonds;
                            }
                            if (_cloudDataManager.CurrentSaveData.relics > 0 && UserSummary.Moecrystal != _cloudDataManager.CurrentSaveData.relics)
                            {
                                needUpdateUserSummary = true;
                                UserSummary.Moecrystal = _cloudDataManager.CurrentSaveData.relics;
                            }

                            if (needUpdateUserSummary)
                            {
                                _userService.UpdateUserSummary(UserSummary, (e) =>
                                {
                                    if (e != null)
                                    {
                                        Debug.LogError("Sync Gems or Moecrystals to UserSummary Fail");
                                    }
                                    else
                                    {
                                        AOMStoreInventory.ResetDiamond(UserSummary.Gem);
                                        AOMStoreInventory.ResetRelic(UserSummary.Moecrystal);

                                        _cloudDataManager.CurrentSaveData.diamonds = 0;
                                        _cloudDataManager.CurrentSaveData.relics = 0;
                                        _initManager.SaveCurrentSaveDataToParse(false);
                                    }
                                });
                            }
                        }
                    });
                }

                _initManager.VerfiyAssetBundleVersion();
            }
            else
            {
                if (allowSyncFail)
                {
                    _initManager.VerfiyAssetBundleVersion();
                }
                else
                {
//                    if (loadingIndicator.activeSelf)
//                        loadingIndicator.SetActive(false);

#if UNITY_ANDROID
                    loadGameMenuGroup_Android.SetActive(true);
                    loadGameMenuGroup_iOS.SetActive(false);
#elif UNITY_IOS || UNITY_EDITOR
                    loadGameMenuGroup_Android.SetActive(false);
                    loadGameMenuGroup_iOS.SetActive(true);
#endif
                    BackBtn.SetActive(true);
                }
            }
        }

        void OnVerfiyAssetBundleVersion()
        {
            if (loadGameMenuGroup_Android.activeSelf || loadGameMenuGroup_iOS.activeSelf)
            {
#if UNITY_ANDROID
                loadGameMenuGroup_Android.SetActive(false);
#elif UNITY_IOS || UNITY_EDITOR
                loadGameMenuGroup_iOS.SetActive(false);
#endif
                BackBtn.SetActive(false);
            }

//            if (!loadingIndicator.activeSelf)
//                loadingIndicator.SetActive(true);
//
//            loadingIndicator.GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("update_checking");

            loadingSlider.SetActive(false);
            retryBtn.SetActive(false);
        }

        void OnUpdateAssetDownloadProgress(int loadedBundleCount, float progress)
        {
            if (AssetBundleLoader.Instance != null)
            {
//                if (!loadingIndicator.activeSelf)
//                    loadingIndicator.SetActive(true);
//
//                loadingIndicator.GetComponent<Text>().text =
//                    LanguageManager.Instance.GetTextValue("asset_bundle_downloading") + " (" + loadedBundleCount + "/" +
//                    (AssetBundleLoader.Instance.assetBundleList.Length - 1) + ")";

                if (!loadingSlider.activeSelf)
                    loadingSlider.SetActive(true);

                loadingSlider.GetComponent<Slider>().value = progress;
            }
        }

        void OnAssetBundleDownloadCallback(bool success)
        {
            if (success)
            {
//                if (!loadingIndicator.activeSelf)
//                    loadingIndicator.SetActive(true);
//
//                loadingIndicator.GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("game_loading");

                loadingSlider.SetActive(false);
                retryBtn.SetActive(false);

                RemoveEventListener();

                _initManager.LoadMainScene();
            }
            else
            {
//                if (!loadingIndicator.activeSelf)
//                    loadingIndicator.SetActive(true);
//
//                loadingIndicator.GetComponent<Text>().text =
//                    LanguageManager.Instance.GetTextValue("asset_bundle_lost_connection");

                loadingSlider.SetActive(false);
                retryBtn.SetActive(true);
            }
        }

        public void RetryLoadAssetBundle()
        {
            _initManager.VerfiyAssetBundleVersion();
        }
    }
}