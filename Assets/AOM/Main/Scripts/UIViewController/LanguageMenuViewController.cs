﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;

namespace Ignite.AOM
{
    public class LanguageMenuViewController : UIOverlayViewController
	{
        public static readonly string Language_PP_KEY = "PP_LANGUAGE";

        public void ChangeLanguage(string language)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            LanguageManager.Instance.ChangeLanguage(language);
            PlayerPrefs.SetString(Language_PP_KEY, language);

            EventManager.ChangeLanguage();
        }

        public override void CloseView()
        {
            LoadOverlayView("SettingMainMenuView", false, Time.timeScale == 0);

            base.CloseView();
        }
	}
}
