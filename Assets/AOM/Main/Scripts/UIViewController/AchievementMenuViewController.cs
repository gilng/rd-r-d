﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;
using System;

namespace Ignite.AOM
{
    public class AchievementMenuViewController : UIOverlayViewController
    {
        public AchievementTableViewController table;

        public Text diamondBalance, relicBalance;
        public Text diamondAdd, relicAdd;
        public Animator diamondAnimator, relicAnimator;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            RefreshDiamondBalance();
            RefreshRelicBalance();

            if (table.m_tableView.dataSource != null)
                table.m_tableView.FastReloadData(false);

            EventManager.OnDiamondBalanceUpdated += RefreshDiamondBalance;
            EventManager.OnRelicBalanceUpdated += RefreshRelicBalance;

            EventManager.OnDiamondValueHandler += AddDiamondValue;
            EventManager.OnRelicValueHandler += AddRelicValue;
        }

        void AddDiamondValue(int value)
        {
            diamondAdd.text = "+" + value.ToString();

            SoundManager.Instance.PlaySoundWithType(SoundType.DiamondCollected);
        }

        void AddRelicValue(int value)
        {
            relicAdd.text = "+" + value.ToString();

            SoundManager.Instance.PlaySoundWithType(SoundType.DiamondCollected);
        }

        void RefreshDiamondBalance()
        {
            //diamondBalance.text = UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetDiamondBalance());
            diamondBalance.text = UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString();
            diamondAnimator.Play("AchievementsLabelIcon_Play", -1, 0);
        }

        void RefreshRelicBalance()
        {
            relicBalance.text = UserManager.Instance.CurrentUserSummary != null ? UnitConverter.ConverterDoubleToString(UserManager.Instance.CurrentUserSummary.Moecrystal) : UnitConverter.ConverterDoubleToString(AOMStoreInventory.GetRelicBalance());
            relicAnimator.Play("AchievementsLabelIcon_Play", -1, 0);
        }

        public override void CloseView()
        {
            for (int i = 0; i < AchievementAssets.Achievements.Count; i++)
            {
                AchievementVG vg = AchievementAssets.Achievements[i];
                if (vg.CanAffordOne())
                {
                    break;
                }
            }

            GameManager.Instance.HideNewAchievementIcon();

            EventManager.OnDiamondBalanceUpdated -= RefreshDiamondBalance;
            EventManager.OnRelicBalanceUpdated -= RefreshRelicBalance;

            EventManager.OnDiamondValueHandler -= AddDiamondValue;
            EventManager.OnRelicValueHandler -= AddRelicValue;

            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            base.CloseView();

            if (Time.timeScale == 0)
                Time.timeScale = 1;
        }
    }
}
