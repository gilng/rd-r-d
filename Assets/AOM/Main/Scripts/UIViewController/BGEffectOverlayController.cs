﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;

namespace Ignite.AOM
{
    public class BGEffectOverlayController : MonoBehaviour
    {
        public Text bgNameLabel;

        bool enabled = false;

        public void OverlayEnable(string bgId)
        {
            enabled = true;

            bgNameLabel.text = LanguageManager.Instance.GetTextValue(bgId);

            if (!this.gameObject.activeSelf)
                this.gameObject.SetActive(true);
        }

        public void OverlayDisable()
        {
            enabled = false;

            bgNameLabel.text = "";

            if (this.gameObject.activeSelf)
                this.gameObject.SetActive(false);
			
			if (GameManager.Instance != null)
				GameManager.Instance.GetMoetanDefeatedMoeCrystal();
        }

        public void BGSwitchCallBack()
        {
            if (enabled)
            {
                if(StageManager.Instance != null)
                    EventManager.BGSwitch();
            }
        }
    }
}
