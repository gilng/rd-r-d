﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;

namespace Ignite.AOM
{
    public class AvatarSkillDetailViewController : UIOverlayViewController
    {
        public Image skillIcon;
        public Text skillName;
        public Text skillLv;
        public Text skillDesc;
        public Text skillDuration;
        public Text skillCooldown;

        public Sprite heavenlyStrikeIcon;
        public Sprite shadowCloneIcon;
        public Sprite criticalStrikeIcon;
        public Sprite warCryIcon;
        public Sprite berserkerRageIcon;
        public Sprite handOfMidasIcon;

        private static AvatarActiveSkillVG skill;
        private static System.Text.StringBuilder sb = new System.Text.StringBuilder();

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            AddListener();
        }

        void AddListener()
        {
            EventManager.OnLanguageChange += RenderSkillDetail;
        }

        void RemoveListener()
        {
            EventManager.OnLanguageChange -= RenderSkillDetail;
        }

        public void SetSkill(AvatarActiveSkillVG vg)
        {
            skill = vg;

            RenderSkillDetail();
        }

        void RenderSkillDetail()
        {
            if (AOMAssets.HeavenlyStrikeASVG == skill)
                skillIcon.sprite = heavenlyStrikeIcon;
            else if (AOMAssets.ShadowCloneASVG == skill)
                skillIcon.sprite = shadowCloneIcon;
            else if (AOMAssets.CriticalStrikeASVG == skill)
                skillIcon.sprite = criticalStrikeIcon;
            else if (AOMAssets.WarCryASVG == skill)
                skillIcon.sprite = warCryIcon;
            else if (AOMAssets.BerserkerRageASVG == skill)
                skillIcon.sprite = berserkerRageIcon;
            else if (AOMAssets.HandOfMidasASVG == skill)
                skillIcon.sprite = handOfMidasIcon;

            skillName.text = LanguageManager.Instance.GetTextValue(skill.ItemId);
            skillLv.text = skill.GetBalance().ToString();

            sb.Append(skill.ItemId);
            sb.Append("_desc");
            string effectValueString;

            if (skill.GetBalance() != 0)
            {
                if (skill == AOMAssets.CriticalStrikeASVG || skill == AOMAssets.WarCryASVG || skill == AOMAssets.BerserkerRageASVG || skill == AOMAssets.HandOfMidasASVG)
                    effectValueString = (skill.EffectValue * 100).ToString();
                else
                    effectValueString = UnitConverter.ConverterDoubleToString(skill.EffectValue);
            }
            else
            {
                if (skill == AOMAssets.HeavenlyStrikeASVG)
                    effectValueString = "140";
                else if (skill == AOMAssets.ShadowCloneASVG)
                    effectValueString = "7";
                else if (skill == AOMAssets.CriticalStrikeASVG)
                    effectValueString = "17";
                else if (skill == AOMAssets.WarCryASVG)
                    effectValueString = "150";
                else if (skill == AOMAssets.BerserkerRageASVG)
                    effectValueString = "70";
                else if (skill == AOMAssets.HandOfMidasASVG)
                    effectValueString = "15";
                else
                    effectValueString = "";
            }

            skillDesc.text = string.Format(LanguageManager.Instance.GetTextValue(sb.ToString()), effectValueString);
            sb.Clear();

            skillDuration.text = skill.Duration + LanguageManager.Instance.GetTextValue("second");
            skillCooldown.text = skill.Cooldown + LanguageManager.Instance.GetTextValue("second");
        }

        public override void CloseView()
        {
            RemoveListener();

            base.CloseView();
        }
    }
}
