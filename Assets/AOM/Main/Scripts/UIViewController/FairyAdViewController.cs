﻿using System;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using Zenject;

namespace Ignite.AOM
{
    public class FairyAdViewController : UIOverlayViewController
    {
        private EnemyCore _enemyCore;

        [Inject]
        public void ConstructorSafeAttribute(EnemyCore enemyCore)
        {
            _enemyCore = enemyCore;
        }

        public Button closeButton, watchButton, collectButton, doubleButton;
        public Text descriptionText, bonusText, adVideoSkippedMessageText, priceLabel, doubleRewardAmountLabel, currentGemsLabel;

        private VideoAdRewardType currentType;

        private double bonusAmount = 0;
        private int costGems = 30;
        private int costGems_Ruby = 100;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            AddListener();

            closeButton.gameObject.SetActive(true);
            watchButton.gameObject.SetActive(true);

            collectButton.gameObject.SetActive(false);
            doubleButton.gameObject.SetActive(false);
            currentGemsLabel.gameObject.SetActive(false);

            priceLabel.text = costGems.ToString();
            currentGemsLabel.text = string.Format(LanguageManager.Instance.GetTextValue("current_gems"), UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString());

            adVideoSkippedMessageText.gameObject.SetActive(false);
        }

        public void SetFairyRewardType(VideoAdRewardType type)
        {
            this.currentType = type;
            bonusText.gameObject.SetActive(true);
            bonusText.text = LanguageManager.Instance.GetTextValue("fairy_title_bonus") + " ";

            if (currentType == VideoAdRewardType.UltimateGold) // Gold
            {
                descriptionText.text = LanguageManager.Instance.GetTextValue("fairy_type_ultimate_gold");

                bonusAmount = Formulas.EnemyGoldDropForLevel(_enemyCore.Stage, EnemyType.Normal) * 50 * (1 + ArtifactAssets.MoeSpirit005.EffectValue);
                bonusText.text += UnitConverter.ConverterDoubleToString(bonusAmount) + " " + LanguageManager.Instance.GetTextValue("currency_gold");

                doubleRewardAmountLabel.text = UnitConverter.ConverterDoubleToString(bonusAmount * 2) + " " + LanguageManager.Instance.GetTextValue("currency_gold");
            }
            else if (currentType == VideoAdRewardType.UltimateDiamond) // Diamond
            {
                descriptionText.text = LanguageManager.Instance.GetTextValue("fairy_type_ultimate_diamond");

                bonusAmount = (int)(5 * (1 + ArtifactAssets.MoeSpirit017.EffectValue));
                bonusText.text += UnitConverter.ConverterDoubleToString(bonusAmount) + " " + LanguageManager.Instance.GetTextValue("currency_diamond");
            }
            else if (currentType == VideoAdRewardType.UltimateRubQuota) // CP
            {
                descriptionText.text = LanguageManager.Instance.GetTextValue("fairy_type_ultimate_rub_quota");

                bonusAmount = 2 + (int)(ArtifactAssets.MoeSpirit008.EffectValue * 100);
                bonusText.text += UnitConverter.ConverterDoubleToString(bonusAmount) + " " + LanguageManager.Instance.GetTextValue("currency_rubQuota");

                doubleRewardAmountLabel.text = UnitConverter.ConverterDoubleToString(bonusAmount * 2) + " " + LanguageManager.Instance.GetTextValue("currency_rubQuota");
            }
            else if (currentType == VideoAdRewardType.UltimateRuby) // Ruby
            {
                descriptionText.text = LanguageManager.Instance.GetTextValue("fairy_type_ultimate_ruby");

                bonusAmount = ArtifactAssets.MoeSpirit026.EffectValue * 100;
                bonusText.text += UnitConverter.ConverterDoubleToString(bonusAmount) + " " + LanguageManager.Instance.GetTextValue("currency_ruby");

                doubleRewardAmountLabel.text = UnitConverter.ConverterDoubleToString(bonusAmount * 2) + " " + LanguageManager.Instance.GetTextValue("currency_ruby");

                priceLabel.text = costGems_Ruby.ToString();
            }
        }

        void AddListener()
        {
            EventManager.OnVideoAdFinished += VideoAdReward;
            EventManager.OnVideoAdSkipped += VideoAdSkip;

            EventManager.OnDiamondBalanceUpdated += RefreshOwnedGemsDisplay;
        }

        void RemoveListener()
        {
            EventManager.OnVideoAdFinished -= VideoAdReward;
            EventManager.OnVideoAdSkipped -= VideoAdSkip;

            EventManager.OnDiamondBalanceUpdated -= RefreshOwnedGemsDisplay;
        }

        void VideoAdReward()
        {
            // Show Reward Collect Button
            collectButton.gameObject.SetActive(true);
            doubleButton.gameObject.SetActive(currentType != VideoAdRewardType.UltimateDiamond);
            currentGemsLabel.gameObject.SetActive(currentType != VideoAdRewardType.UltimateDiamond);
        }

        void VideoAdSkip()
        {
            adVideoSkippedMessageText.gameObject.SetActive(true);

            watchButton.gameObject.SetActive(true);
            closeButton.gameObject.SetActive(true);
        }

        public void WatchVideo()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            ADManager.Instance.ShowRewardedAd();

            if (adVideoSkippedMessageText.gameObject.activeSelf)
                adVideoSkippedMessageText.gameObject.SetActive(false);

            watchButton.gameObject.SetActive(false);
            closeButton.gameObject.SetActive(false);

            // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.CustomEvent("watchUnityAd", new Dictionary<string, object>
            {
                { "rewardType", currentType },
                { "rewardAmount", bonusAmount}
            });
#endif
        }

        public void CollectReward()
        {
            EventManager.CollectReward(currentType, false);

            CloseView();
        }

        public void CollectDoubleReward()
        {
            int targetGems = (currentType == VideoAdRewardType.UltimateRuby) ? costGems_Ruby : costGems;

//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                if (UserManager.Instance.CurrentUserSummary.Gem >= targetGems)
//                {
                    //SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                    UserManager.Instance.TakeGems(targetGems, () =>
                    {
                        EventManager.CollectReward(currentType, true);

                        // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                        Analytics.CustomEvent("useGems", new Dictionary<string, object>
                        {
                            { "type", "Double FairyAD Reward" },
                        });
#endif

                        CloseView();
                    });
//                }
//                else
//                {
//                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
//
//                    LoadOverlayView("ShopMenu", false, false);
//                }
//            }
        }

        void RefreshOwnedGemsDisplay()
        {
            currentGemsLabel.text = string.Format(LanguageManager.Instance.GetTextValue("current_gems"), UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString());
        }

        public void IgnoreReward()
        {
            EventManager.IgnoredReward();

            CloseView();
        }

        public override void CloseView()
        {
            RemoveListener();

            base.CloseView();
        }
    }
}
