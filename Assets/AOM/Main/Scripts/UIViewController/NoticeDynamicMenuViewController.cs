﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;
using Facebook.MiniJSON;


namespace Ignite.AOM
{
    [System.Serializable]
    public class NoticeDynamicData
    {
        public Sprite sprite;
        public IDictionary<string, string> subtitle;
        public string url;
    }

    public class NoticeDynamicMenuViewController : UIOverlayViewController
    {
        public GameObject contentImage;
        public Text contentText;

        public GameObject loadingText;

        NoticeDynamicData[] dynamicData;
        int index;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            loadingText.SetActive(true);
        }

        public void LoadContent(NoticeDynamicData[] _d)
        {
            if (_d == null)
            {
                GoToNoticeBoard();
            }
            else
            {
                dynamicData = _d;

                index = Random.Range(0, dynamicData.Length);

                contentImage.SetActive(true);
                contentImage.GetComponent<Image>().sprite = dynamicData[index].sprite;
                contentText.text = dynamicData[index].subtitle[LanguageManager.Instance.LoadedLanguage];
            }
        }

        public void GotoURL()
        {
            if (dynamicData != null)
                Application.OpenURL(dynamicData[index].url);

            // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.CustomEvent("DynamicNoticeBoard_GotoURL", new Dictionary<string, object>
            {
                { "ViewURL", "View" }
            });
#endif
        }

        public void ButtonPress()
        {
            EndOfNotice();
        }

        public void EndOfNotice()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            GoToNoticeBoard();
        }

        public void GoToNoticeBoard()
        {
            LoadOverlayView("NoticeMenuView", true, false);
        }

    }

    /*

    // Read all dynamic data and show all
	
    public class NoticeDynamicMenuViewController : UIOverlayViewController
    {

        public GameObject contentImage;
        public Text contentText;

        public GameObject loadingText;

        int pageNum, pageFinish, currentPage;

        bool loadedSuccess;

        NoticeDynamicData[] dynamicData;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            pageNum = 0;
            pageFinish = 0;
            currentPage = 0;

            loadedSuccess = false;
            loadingText.SetActive(true);

            contentImage.SetActive(false);
            contentText.text = "";
        }

        public void LoadContent(IList<Notice> notices)
        {
            pageNum = notices.Count;

            dynamicData = new NoticeDynamicData[pageNum];

            int order = 0;
            IList<Notice> notice_order = new List<Notice>();

            for (int i = 0; i < pageNum; i++)
            {
                foreach (Notice n in notices)
                {
                    if (n.Order == order)
                    {
                        notice_order.Add(n);
                        order++;
                    }
                }
            }

            for (int i = 0; i < pageNum; i++)
            {
                dynamicData[i] = new NoticeDynamicData();

                StartCoroutine(DownloadingContent(i, notice_order[i]));
            }
        }

        IEnumerator DownloadingContent(int index, Notice notice)
        {
            WWW www = new WWW(notice.ImageURL);

            yield return www;

            if (www.error == null)
            {
                // Image
                dynamicData[index].sprite = Sprite.Create(www.texture, new Rect(0, 0, 900, 921), new Vector2(0.5f, 0.5f), 100);

                // Text
                dynamicData[index].subtitle = notice.Text;

                // URL
                if (notice.URL != null)
                    dynamicData[index].url = notice.URL;

                pageFinish += 1;
                LoadingChecker();
            }
        }

        void LoadingChecker()
        {
            if (pageFinish == pageNum)
            {
                loadedSuccess = true;

                contentImage.SetActive(true);
                contentImage.GetComponent<Image>().sprite = dynamicData[currentPage].sprite;
                contentText.text = dynamicData[currentPage].subtitle[LanguageManager.Instance.LoadedLanguage];
            }
        }

        public void GotoURL()
        {
            if (dynamicData != null)
                Application.OpenURL(dynamicData[currentPage].url);

            // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.CustomEvent("DynamicNoticeBoard_GotoURL", new Dictionary<string, object>
            {
                { "ViewURL", "View" }
            });
#endif
        }

        public void ButtonPress()
        {
            if (loadedSuccess)
            {
                currentPage++;

                if (currentPage < pageNum)
                {
                    contentImage.GetComponent<Image>().sprite = dynamicData[currentPage].sprite;
                    contentText.text = dynamicData[currentPage].subtitle[LanguageManager.Instance.LoadedLanguage];
                }
                else
                {
                    EndOfNotice();
                }
            }
            else
            {
                EndOfNotice();
            }
        }

        public void EndOfNotice()
        {
            LoadOverlayView("NoticeMenuView", true, false);
        }

    }
    */
}