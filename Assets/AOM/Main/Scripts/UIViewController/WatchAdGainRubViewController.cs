﻿using UnityEngine;
using System.Collections;
using ChartboostSDK;
using UnityEngine.Analytics;
using System.Collections.Generic;
using UnityEngine.UI;
using Zenject;

namespace Ignite.AOM
{
    public class WatchAdGainRubViewController : UITipsViewController
    {
        private GalleryCore _galleryCore;

        [Inject]
        public void ConstructorSafeAttribute(GalleryCore galleryCore)
        {
            _galleryCore = galleryCore;
        }

        public Toggle dontShowToggle;

        public void GetCP()
        {
            FieldGuideManager.Instance.OpenCPShopOverlay();

            CloseView();
        }

        public void ViewAd()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (Chartboost.hasRewardedVideo(CBLocation.MainMenu))
            {
                Chartboost.showRewardedVideo(CBLocation.MainMenu);
            }
            else
            {
                // We don't have a cached video right now, but try to get one for next time
                Chartboost.cacheRewardedVideo(CBLocation.MainMenu);
            }

            // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.CustomEvent("viewAdResumeRubQuota", new Dictionary<string, object>
                {
                    { "numberOfView", "View" }
                });
#endif

            CloseView();
        }

        public void DontShowNotice()
        {
            PlayerPrefs.SetInt("DONT_SHOW_CP_TIPS", dontShowToggle.isOn ? 1 : 0);
        }

        // Update is called once per frame
        void Update()
        {
            if (_galleryCore.TouchCredit > 0)
            {
                CloseView();
            }
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            base.CloseView();

            Time.timeScale = 1f;
        }
    }
}