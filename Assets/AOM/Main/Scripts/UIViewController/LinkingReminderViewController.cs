﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Ignite.AOM
{
    public class LinkingReminderViewController : UIOverlayViewController
	{
        private SocialManager _socialManager;
        private CloudDataManager _cloudDataManager;

        [Inject]
        public void ConstructorSafeAttribute(SocialManager socialManager, CloudDataManager cloudDataManager)
        {
            _socialManager = socialManager;
            _cloudDataManager = cloudDataManager;
        }

        public GameObject panel_LinkingSelection;

        public GameObject button_LinkWithGoogle;

        public GameObject panel_EmailLinking;

        public InputField input_userEmail;
        public InputField input_userPw;

        public GameObject invalidInputMsg;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            panel_LinkingSelection.SetActive(true);

#if UNITY_IOS || UNITY_EDITOR
            button_LinkWithGoogle.SetActive(false);
#endif

            panel_EmailLinking.SetActive(false);
        }

        public void Linking_Google()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            CloudSavingOverlayController signInOverlayController = LoadOverlayViewController("CloudSavingOverlay", false, Time.timeScale == 0) as CloudSavingOverlayController;

            signInOverlayController.ShowSigninMessage();

            Action parseSignInSuccessCallback = () =>
            {
                if (signInOverlayController != null)
                    signInOverlayController.CloseView();

                CloseView();

                if (_cloudDataManager != null)
                    _cloudDataManager.ShowWarningOverlay("SAVE");
                
            };

            Action parseSignInFailCallback = () =>
            {
                if (!_socialManager.googleSignedIn)
                {
                    Action googleSignInSuccessCallback = () =>
                    {
                        if (signInOverlayController != null)
                            signInOverlayController.CloseView();

                        Linking_Google();
                    };

                    Action googleSignInFailCallback = () =>
                    {
                        if (signInOverlayController != null)
                            signInOverlayController.ShowSigninFailMessage();
                    };

                    _socialManager.GoogleSignIn(true, googleSignInSuccessCallback, googleSignInFailCallback);
                }
                else
                {
                    if (signInOverlayController != null)
                        signInOverlayController.ShowSigninFailMessage();
                }
            };

            _socialManager.ParseSignInWithGoogle(parseSignInSuccessCallback, parseSignInFailCallback);

        }

        public void Linking_Email()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            panel_LinkingSelection.SetActive(false);
            panel_EmailLinking.SetActive(true);

            input_userEmail.text = "";
            input_userPw.text = "";
            invalidInputMsg.SetActive(false);
        }

        public void LinkWtihEmail()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (input_userEmail.text != "" && input_userPw.text.Length == 8)
            {
                CloudSavingOverlayController signInOverlayController = LoadOverlayViewController("CloudSavingOverlay", false, Time.timeScale == 0) as CloudSavingOverlayController;

                signInOverlayController.ShowSigninMessage();

                Action linkSuccessCallback = () =>
                {
                    if (signInOverlayController != null)
                        signInOverlayController.CloseView();

                    CloseView();

                    if (_cloudDataManager != null)
                        _cloudDataManager.ShowWarningOverlay("SAVE");
                };

                Action linkFailCallback = () =>
                {
                    if (signInOverlayController != null)
                        signInOverlayController.ShowSigninFailMessage();

                    invalidInputMsg.SetActive(true);
                };

                _socialManager.ParseLinkWithEmail(input_userEmail.text, input_userPw.text, linkSuccessCallback, linkFailCallback);
            }
            else
            {
                invalidInputMsg.SetActive(true);
            }
        }

        public void BackToLinkingSelection()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            panel_LinkingSelection.SetActive(true);

#if UNITY_IOS || UNITY_EDITOR
            button_LinkWithGoogle.SetActive(false);
#endif

            panel_EmailLinking.SetActive(false);
        }
	}
}
