﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;
using Parse;
using Zenject;
using System.Threading.Tasks;

namespace Ignite.AOM
{
    public class UserIDViewController : UIOverlayViewController
    {
        private SocialManager _socialManager;

        [Inject]
        public void ConstructorSafeAttribute(SocialManager socialManager)
        {
            _socialManager = socialManager;
        }

        public GameObject panel_Base;
        public GameObject panel_LinkingSelection;
        public GameObject panel_EmailLinking;

        public Text userIdLable;
        public Text linkedLable;

        public GameObject googlePlusIcon;
        public GameObject emailIcon;

        public Button btnLinkingSelection;

        public InputField input_userEmail;
        public InputField input_userPw;

        public GameObject invalidInputMsg;

        private List<GameObject> panelHistory;


        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            if (!_socialManager.parseSignedIn)
            {
                // Check Connection
                if (SocialManager.CheckForInternetConnection())
                {
                    // Create Account
                    Action successCallback = () =>
                    {
                        InitDisplay();
                    };

                    Action failCallback = () =>
                    {
                        //ShowPopup("Error", "Fail create account");

                        CloudSavingOverlayController signInOverlayController = LoadOverlayViewController("CloudSavingOverlay", false, Time.timeScale == 0) as CloudSavingOverlayController;

                        signInOverlayController.ShowSigninFailMessage();

                        base.CloseView();
                    };

                    _socialManager.ParseSignUp(successCallback, failCallback);
                }
                else
                {
                    //ShowPopup("Error", "No internet connection");

                    CloudSavingOverlayController signInOverlayController = LoadOverlayViewController("CloudSavingOverlay", false, Time.timeScale == 0) as CloudSavingOverlayController;

                    signInOverlayController.ShowSigninFailMessage();

                    base.CloseView();
                }
            }
            else
            {
                InitDisplay();
            }
        }

        void InitDisplay()
        {
            ChangePanel(panel_Base);

            userIdLable.text = _socialManager.CurrentParseUser.UserId;

            if (_socialManager.CurrentParseUser.UserEmail != "")
            {
                linkedLable.text = _socialManager.CurrentParseUser.UserEmail;
            }
            else if (_socialManager.linkedWithGoogle)
            {
                linkedLable.text = ParseUser.CurrentUser["googleUserId"].ToString();
            }

            googlePlusIcon.SetActive(_socialManager.linkedWithGoogle);
            emailIcon.SetActive(!_socialManager.linkedWithGoogle && _socialManager.CurrentParseUser.UserEmail != "");

            btnLinkingSelection.gameObject.SetActive(linkedLable.text == "");
        }

        public void OpenLinkingSelectionPanel()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

#if UNITY_ANDROID
            ChangePanel(panel_LinkingSelection);
#elif UNITY_IOS || UNITY_EDITOR
            ChangePanel(panel_EmailLinking);;
#endif
        }

        public void Linking_Google()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            CloudSavingOverlayController signInOverlayController = LoadOverlayViewController("CloudSavingOverlay", false, Time.timeScale == 0) as CloudSavingOverlayController;

            signInOverlayController.ShowSigninMessage();

            Action parseSignInSuccessCallback = () =>
            {
                if (signInOverlayController != null)
                    signInOverlayController.CloseView();

                InitDisplay();
            };

            Action parseSignInFailCallback = () =>
            {
                if (!_socialManager.googleSignedIn)
                {
                    Action googleSignInSuccessCallback = () =>
                    {
                        if (signInOverlayController != null)
                            signInOverlayController.CloseView();

                        Linking_Google();
                    };

                    Action googleSignInFailCallback = () =>
                    {
                        if (signInOverlayController != null)
                            signInOverlayController.ShowSigninFailMessage();

                        //ShowPopup("Error", "Fail google sign in");
                    };

                    _socialManager.GoogleSignIn(true, googleSignInSuccessCallback, googleSignInFailCallback);
                }
                else
                {
                    if (signInOverlayController != null)
                        signInOverlayController.ShowSigninFailMessage();

                    //ShowPopup("Error", "Fail linking google");
                }
            };

            _socialManager.ParseSignInWithGoogle(parseSignInSuccessCallback, parseSignInFailCallback);
        }

        public void OpenInputPanel()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            ChangePanel(panel_EmailLinking);
        }

        public void Linking_Email()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (input_userEmail.text != "" && input_userPw.text.Length == 8)
            {
                CloudSavingOverlayController signInOverlayController = LoadOverlayViewController("CloudSavingOverlay", false, Time.timeScale == 0) as CloudSavingOverlayController;

                signInOverlayController.ShowSigninMessage();

                Action linkSuccessCallback = () =>
                {
                    if (signInOverlayController != null)
                        signInOverlayController.CloseView();

                    InitDisplay();
                };

                Action linkFailCallback = () =>
                {
                    if (signInOverlayController != null)
                        signInOverlayController.ShowSigninFailMessage();

                    invalidInputMsg.SetActive(true);
                };

                _socialManager.ParseLinkWithEmail(input_userEmail.text, input_userPw.text, linkSuccessCallback, linkFailCallback);
            }
            else
            {
                invalidInputMsg.SetActive(true);
            }
        }

        public void BackView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            panelHistory.RemoveAt(panelHistory.Count - 1);

            ChangePanel();
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            base.CloseView();
        }

        void ChangePanel(GameObject _currentPanel = null)
        {
            if (panelHistory == null)
                panelHistory = new List<GameObject>();

            if (_currentPanel != null)
                panelHistory.Add(_currentPanel);

            panel_Base.SetActive(panelHistory[panelHistory.Count - 1] == panel_Base);
            panel_LinkingSelection.SetActive(panelHistory[panelHistory.Count - 1] == panel_LinkingSelection);
            panel_EmailLinking.SetActive(panelHistory[panelHistory.Count - 1] == panel_EmailLinking);

            invalidInputMsg.SetActive(false);
        }

        void ShowPopup(string _title = "", string _message = "")
        {
            MsgPopupViewController popup = LoadOverlayViewController("MsgPopupOverlay", false, Time.timeScale == 0) as MsgPopupViewController;
            popup.InputMsg(_title, _message);
        }
	}
}
