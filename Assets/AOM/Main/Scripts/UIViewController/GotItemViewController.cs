﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;

namespace Ignite.AOM
{
    public class GotItemViewController : UIOverlayViewController
    {
        Sprite[] thumbnailIcon;
        string[] thumbnailIconName;

        public Animator effectAnimator;
        public Image itemIcon;
        public Text itemName;

        public GameObject rareLevelStar1;
        public GameObject rareLevelStar2;
        public GameObject rareLevelStar3;

        float timer, time = 10;
        bool scriptOn = false;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);
        }

        public void SetGotItem(string itemId, int itemRareLevel)
        {
            if (itemId.Contains("AR"))
            {
				thumbnailIcon = Resources.LoadAll<Sprite>("Sprites/overlay_icon_artifact/overlay_icon_artifact");
                thumbnailIconName = new string[thumbnailIcon.Length];
            }
            else
            {
                thumbnailIcon = Resources.LoadAll<Sprite>("Sprites/table_ui_diamond/table_ui_diamond");
                thumbnailIconName = new string[thumbnailIcon.Length];
            }

            for (int i = 0; i < thumbnailIconName.Length; i++)
            {
                thumbnailIconName[i] = thumbnailIcon[i].name;
            }

            itemIcon.sprite = thumbnailIcon[Array.IndexOf(thumbnailIconName, itemId)];

            itemName.text = LanguageManager.Instance.GetTextValue(itemId + "_name");

            rareLevelStar1.SetActive(itemRareLevel > 0);
            rareLevelStar2.SetActive(itemRareLevel > 1);
            rareLevelStar3.SetActive(itemRareLevel > 2);

            if (itemRareLevel == 1)
            {
                rareLevelStar1.transform.localPosition = new Vector3(0, -65, 0);
            }
            else if (itemRareLevel == 2)
            {
                rareLevelStar1.transform.localPosition = new Vector3(-40, -65, 0);
                rareLevelStar2.transform.localPosition = new Vector3(40, -65, 0);
            }
            else if (itemRareLevel == 3)
            {
                rareLevelStar1.transform.localPosition = new Vector3(-60, -65, 0);
                rareLevelStar2.transform.localPosition = new Vector3(0, -65, 0);
                rareLevelStar3.transform.localPosition = new Vector3(60, -65, 0);
            }

            SoundManager.Instance.PlaySoundWithType(SoundType.GachaResult);

            effectAnimator.Play("ArtifactGachaResult_Start");

            scriptOn = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (scriptOn)
            {
                if (timer > 0)
                {
                    timer -= Time.deltaTime;
                }
                else
                {
                    EffectEnd();
                }
            }
        }

        public void EffectEnd()
        {
            effectAnimator.Play("ArtifactGachaResult_End");

            SoundManager.Instance.StopSound();

            timer = time;
            scriptOn = false;
        }

        public override void CloseView()
        {
            base.CloseView();
        }
    }
}
