﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class ReparationViewController : UIOverlayViewController
    {
        private IUserService _userService;
        private VersionCheckerManager _versionCheckerManager;

        [Inject]
        public void ConstructorSafeAttribute(IUserService userService, VersionCheckerManager versionCheckerManager)
        {
            _userService = userService;
            _versionCheckerManager = versionCheckerManager;
        }

        public static readonly string Reparation_PP_KEY = "PP_REPARATION_RECEIVED_";

        public Text title, subtitle, gemText, relicText;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            //int diamondGain = 0;
            //int relicGain = 0;

            //int[,] diamondVal = new int[,] { { 10, 50, 75, 110 }, { 15, 75, 120, 180 } };
            //int[,] relicVal = new int[,] { { 1, 5, 25, 50 }, { 5, 20, 40, 100 } };

            //for (int i = 0; i < GalleryCore.Girls.Count; i++)
            //{
            //    if (GalleryCore.Girls[i].Status != MonsterGirlStatus.NeverShow)
            //    {
            //        int[] girlLevels = GalleryCore.Girls[i].levels;
            //        int girlEXP = GalleryCore.Girls[i].Exp;

            //        // Check girl set
            //        int gainValueSet = 0;

            //        if (int.Parse(GalleryCore.Girls[i].girlId) < 5)
            //            gainValueSet = 0;
            //        else if (int.Parse(GalleryCore.Girls[i].girlId) < 10)
            //            gainValueSet = 1;

            //        // Check level
            //        for (int j = 0; j < girlLevels.Length; j++)
            //        {
            //            if (girlEXP - girlLevels[j] >= 0)
            //            {
            //                girlEXP -= girlLevels[j];

            //                if (j > 0)
            //                {
            //                    diamondGain += diamondVal[gainValueSet, j - 1];
            //                    relicGain += relicVal[gainValueSet, j - 1];
            //                }
            //            }
            //        }
            //    }
            //}

            //if (_cloudDataManager != null && !_cloudDataManager.progressLoaded)
            //{
            //    if (ArtifactAssets.Artifact031.GetBalance() > 1 || ArtifactAssets.Artifact032.GetBalance() > 1)
            //    {
            //        diamondGain = 1000;

            //        if (diamondGain > 0)
            //        {
            //            gemText.text = "x" + diamondGain.ToString();
            //            AOMStoreInventory.GiveDiamond(diamondGain);
            //        }

            //        if (relicGain > 0)
            //        {
            //            relicText.text = "x" + relicGain.ToString();
            //            AOMStoreInventory.GiveRelic(relicGain);
            //        }
            //    }
            //    else
            //    {
            //        CloseView();
            //    }
            //}
            //else
            //{
            //    CloseView();
            //}

            ShowReparation();
        }

        public void ShowReparation()
        {
            int diamondGain = 0;
            int relicGain = 0;

            if (ArtifactAssets.Artifact031.GetBalance() > 1 || ArtifactAssets.Artifact032.GetBalance() > 1)
            {
                diamondGain = 1000;

                if (diamondGain > 0)
                {
                    gemText.text = "x" + diamondGain.ToString();
                    _userService.CurrentUserSummary.Gem += diamondGain;

                }

                if (relicGain > 0)
                {
                    relicText.text = "x" + relicGain.ToString();
                    _userService.CurrentUserSummary.Moecrystal += relicGain;
                }

                _userService.UpdateUserSummary(_userService.CurrentUserSummary, (e) =>
                {
                    if (e != null)
                    {
                        Debug.LogError("Update UserSummary fail");
                    }
                    else
                    {
                        AOMStoreInventory.ResetDiamond(_userService.CurrentUserSummary.Gem);
                        AOMStoreInventory.ResetRelic(_userService.CurrentUserSummary.Moecrystal);
                    }
                });
            }
            else
            {
                CloseView();
            }
        }

        public override void CloseView()
        {
            PlayerPrefs.SetInt(Reparation_PP_KEY + _versionCheckerManager.CurrentVersion, 1);

            base.CloseView();
        }
    }
}

