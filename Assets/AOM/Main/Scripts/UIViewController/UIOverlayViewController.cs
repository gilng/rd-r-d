﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Ignite.AOM
{
    public class UIOverlayViewController : UIViewController
    {
        public string previousOverlayViewName = "";

        // Use this for Start
        protected override void Start()
        {
            currentUIViewType = UIViewType.OVERLAY;

            base.Start();
        }

        // Use this for initialization
        public override void InitView(UIViewController previousUIView = null, bool pause = false)
        {
            base.InitView(previousUIView, pause);

            if (previousUIView != null)
            {
                if (previousUIView.currentUIViewType == UIViewType.OVERLAY)
                    previousOverlayViewName = previousUIView.gameObject.name;
            }
        }

        public virtual void BackToPreviousOverlay(bool pause)
        {
            if (previousOverlayViewName != "")
            {
                LoadOverlayView(previousOverlayViewName, true, pause);
            }
        }

        public override void CloseView()
        {
            base.CloseView();
        }
    }
}
