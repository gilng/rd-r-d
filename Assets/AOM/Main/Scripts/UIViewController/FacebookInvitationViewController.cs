﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;
using System;

namespace Ignite.AOM
{
    public class FacebookInvitationViewController : UIOverlayViewController
	{
        public Text friendsInviteNumLabel;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            friendsInviteNumLabel.text = string.Format(LanguageManager.Instance.GetTextValue("friends_label_InvitedNumber"), GamePlayStatManager.Instance.Stat[AchievementAssets.Achievements[0].StatType]);

            PlayerPrefs.SetString("TryToInviteFriends", "Yes");

            GameManager.Instance.friendsButton.transform.FindChild("TutorialNotify_Symbol").gameObject.SetActive(false);
        }

        public void InviteFriend()
        {
            FBManager.Instance.InviteFriend();

            CloseView();
        }

        public override void CloseView()
        {
			SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            base.CloseView();		
        }
	}
}
