﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using UnityEngine;
using Soomla.Store;
using SmartLocalization;
using Zenject;


namespace Ignite.AOM
{
    public class PromotionalOfferViewController : UIOverlayViewController
    {
        private SelectionObjectController.Factory _selectionObjectControllerFactory;
        private ArtifactCore _artifactCore;

        [Inject]
        public void ConstructorSafeAttribute(SelectionObjectController.Factory selectionObjectControllerFactory, ArtifactCore artifactCore)
        {
            _selectionObjectControllerFactory = selectionObjectControllerFactory;
            _artifactCore = artifactCore;
        }

        public Animator promotionalOfferAnimator;

        public Text packName;

        public Dictionary<string, Sprite> thumbnail;

        public GridLayoutGroup itemListLayoutGrid;
        public RectTransform itemListLayoutRect;
        public RectTransform itemListScrollViewRect;

        public Text offerPrice;

        public Text offerRemainingTimer;

        List<string> itemIdList = new List<string>();

        List<PromotionalPackTableCell> allPackItemObj = new List<PromotionalPackTableCell>();

        string promotionalCurrencyPackId = "";

        float showTime = 60, showTimer = 60;

        bool offerInited = false;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            AddListener();

            if (thumbnail == null)
            {
                thumbnail = new Dictionary<string, Sprite>();

                string[] _path = new string[2];
                _path[0] = "Sprites/overlay_icon_artifact/overlay_icon_artifact";
                _path[1] = "Sprites/table_ui_dropbox/table_ui_dropbox";

                for (int i = 0; i < 2; i++)
                {
                    Sprite[] _sprites = Resources.LoadAll<Sprite>(_path[i]);

                    foreach (var _s in _sprites)
                    {
                        thumbnail.Add(_s.name, _s);
                    }
                }
            }

            offerInited = true;
        }

        void Update()
        {
            if (offerInited)
            {
                if (showTimer > 0)
                {
                    showTimer -= Time.deltaTime;
                    offerRemainingTimer.text = string.Format(LanguageManager.Instance.GetTextValue("offer_remainingTime"), (int)(showTimer / 60), showTimer);
                }
                else
                {
                    showTimer = showTime;

                    promotionalOfferAnimator.Play("PromotionalOfferAnimator_Close");

                }
            }
        }

        void AddListener()
        {
            EventManager.OnPromotionalPackPurchased += OnPromotionalOfferPurchase;
        }

        void RemoveListener()
        {
            EventManager.OnPromotionalPackPurchased -= OnPromotionalOfferPurchase;
        }

        public void InitPromotionalItemPack(string promotionalPackId)
        {
            promotionalCurrencyPackId = promotionalPackId;

            packName.text = LanguageManager.Instance.GetTextValue(promotionalCurrencyPackId + "_name");

            switch (promotionalCurrencyPackId)
            {
                case "promotional_goldpack":

                    itemIdList.Add("goldPack_diamonds");
                    itemIdList.Add("goldPack_moeCrystals");

                    itemIdList.Add("AR024");
                    itemIdList.Add("AR025");
                    itemIdList.Add("AR026");

                    itemIdList.Add("no_ads");

                    break;

                case "promotional_silverpack":

                    itemIdList.Add("silverPack_diamonds");
                    itemIdList.Add("silverPack_moeCrystals");

                    itemIdList.Add("AR011");
                    itemIdList.Add("AR012");
                    itemIdList.Add("AR021");

                    itemIdList.Add("no_ads");

                    break;

                case "promotional_bronzepack":

                    itemIdList.Add("bronzePack_diamonds");
                    itemIdList.Add("bronzePack_moeCrystals");

                    itemIdList.Add("AR002");

                    itemIdList.Add("no_ads");

                    break;
            }

            for (int i = 0; i < itemIdList.Count; i++)
            {
                PromotionalPackTableCell tableCellController = _selectionObjectControllerFactory.Create("PromotionalPackTableCell") as PromotionalPackTableCell;

                tableCellController.gameObject.name = itemIdList[i];

                tableCellController.gameObject.transform.SetParent(itemListLayoutGrid.transform, false);

                tableCellController.gameObject.transform.localPosition = Vector3.zero;

                tableCellController.Init(this, itemIdList[i]);

                allPackItemObj.Add(tableCellController);
            }

            ResizeItemListLayoutRect();

            offerPrice.text = AOMAssets.CurrencyPackMarketPriceAndCurrency(promotionalPackId);
        }

        void ResizeItemListLayoutRect()
        {
            float itemListLayoutRectSizeX = itemListLayoutGrid.cellSize.x;
            float itemListLayoutRectSizeY = (itemListLayoutGrid.transform.childCount * itemListLayoutGrid.cellSize.y) + ((itemListLayoutGrid.transform.childCount - 1) * itemListLayoutGrid.spacing.y);

            if (itemListScrollViewRect.sizeDelta.y > itemListLayoutRectSizeY)
                itemListLayoutRectSizeY = itemListScrollViewRect.sizeDelta.y;

            itemListScrollViewRect.sizeDelta = new Vector2(itemListLayoutRectSizeX, itemListLayoutRectSizeY);

            itemListLayoutRect.sizeDelta = new Vector2(itemListLayoutRectSizeX, itemListLayoutRectSizeY);
            itemListLayoutRect.transform.localPosition = new Vector3(itemListLayoutRect.transform.localPosition.x, 0, itemListLayoutRect.transform.localPosition.z);
        }

        public void Buy()
        {
            try
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.Purchase_CashierBell);

                AOMStoreInventory.BuyItem(promotionalCurrencyPackId);
            }
            catch (Exception e)
            {
            }
        }

        void OnPromotionalOfferPurchase(string promotionalPackId)
        {
            switch (promotionalPackId)
            {
                case "promotional_goldpack":
                    UserManager.Instance.GiveMoecrystals(300);
                    break;

                case "promotional_silverpack":
                    UserManager.Instance.GiveMoecrystals(200);
                    break;

                case "promotional_bronzepack":
                    UserManager.Instance.GiveMoecrystals(50);
                    break;
            }

            for (int i = 0; i < itemIdList.Count; i++)
            {
                if (itemIdList[i].Contains("AR"))
                {
                    _artifactCore.BuySpecifyOne(itemIdList[i], 1);
                }
            }

            EventManager.TableRefresh();

            PlayerPrefs.SetInt(promotionalPackId + "_PURCHASED", 1);

            CloseView();
        }

        public void TimeOutAnimationCallBack()
        {
            CloseView();
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            RemoveListener();

            EventManager.PromotionalPackOfferTime(false);

            base.CloseView();
        }
    }
}
