﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Ignite.AOM
{
    public class PvPMenuButtonController : MonoBehaviour
    {
        private IPvPChallengeService _pvpChallengeService;

        [Inject]
        public void ConstructorSafeAttribute(IPvPChallengeService pvpChallengeService)
        {
            _pvpChallengeService = pvpChallengeService;
        }

        public GameObject notificationIndicator;
        public Text receivedCountText;

        float updateTime = 600, updateTimer = 0;

        void Update()
        {
            if (updateTimer > 0)
            {
                updateTimer -= Time.deltaTime;
            }
            else
            {
                if (SocialManager.CheckForInternetConnection())
                {
                    _pvpChallengeService.FindAllReceivedChallenge(battleLogList =>
                        {
                            updateTimer = updateTime;
                            UpdateNotificationIndicator(battleLogList.Count);
                        },
                        error =>
                        {
                            updateTimer = updateTime;
                            UpdateNotificationIndicator(0);
                        });
                }
                else
                {
                    updateTimer = updateTime;
                    UpdateNotificationIndicator(0);
                }
            }
        }

        public void  UpdateNotificationIndicator(int receivedCount)
        {
            if (receivedCount > 0)
            {
                notificationIndicator.SetActive(true);
                receivedCountText.text = receivedCount > 9 ? "9+" : receivedCount.ToString();
            }
            else
            {
                notificationIndicator.SetActive(false);
            }
        }

        public void ReadNotification()
        {
            if(notificationIndicator.activeSelf)
                notificationIndicator.SetActive(false);
        }
    }
}