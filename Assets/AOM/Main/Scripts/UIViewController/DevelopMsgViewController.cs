﻿using UnityEngine.UI;

namespace Ignite.AOM
{
    public class DevelopMsgViewController : UIOverlayViewController
    {
        public Text msgText;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);
        }

        public void InputMsg(string _msg)
        {
            msgText.text = _msg;
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            base.CloseView();
        }
    }
}
