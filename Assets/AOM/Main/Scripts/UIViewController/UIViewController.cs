﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

namespace Ignite.AOM
{
    public class UIViewController : MonoBehaviour
    {
        private UIViewLoader _uiViewLoader;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader)
        {
            _uiViewLoader = uiViewLoader;
        }

        public enum UIViewType
        {
            PAGE,
            OVERLAY,
            TIPS
        }

        public UIViewType currentUIViewType;

        protected GameObject pageContainerParent;
        protected GameObject overlayContainerParent;
        protected GameObject tipsContainerParent;

        protected UIViewController previousUIView;

        protected bool inited;

        // Use this for Start
        protected virtual void Start()
        {
            this.gameObject.name = this.gameObject.name.Replace("(Clone)", "");

            //this.pageContainerParent = GameObject.Find("Page_Container_Group");
            //this.overlayContainerParent = GameObject.Find("Overlay_Container_Group");
            //this.tipsContainerParent = GameObject.Find("Tips_Container_Group");

            //switch (this.currentUIViewType)
            //{
            //    case UIViewType.PAGE:

            //        if (pageContainerParent != null)
            //            this.transform.SetParent(pageContainerParent.transform, false);

            //        break;

            //    case UIViewType.OVERLAY:

            //        SoundManager.Instance.PlaySoundWithType(SoundType.PopupShow_1);

            //        if (overlayContainerParent != null)
            //            this.transform.SetParent(overlayContainerParent.transform, false);

            //        break;

            //    case UIViewType.TIPS:

            //        if (tipsContainerParent != null)
            //            this.transform.SetParent(tipsContainerParent.transform, false);

            //        break;
            //}
        }

        // Use this for initialization
        public virtual void InitView(UIViewController previousUIView = null, bool pause = false)
        {
            if (previousUIView != null)
                this.previousUIView = previousUIView;

            if (pause)
                Time.timeScale = 0f;
            else
                Time.timeScale = 1f;

            this.inited = true;
        }

        // Load Page View
        public virtual void LoadPageView(string pageViewName)
        {
            _uiViewLoader.LoadPageView(pageViewName, this);
        }

        public virtual UIViewController LoadPageViewController(string pageViewName)
        {
            GameObject pagePanel = _uiViewLoader.LoadPageView(pageViewName, this);
            UIViewController pageController = pagePanel.GetComponent<UIViewController>();

            return pageController;
        }
            
        // Load Overlay View
        public virtual void LoadOverlayView(string overlayViewName, bool closeCurrentOverlay, bool pause)
        {
            _uiViewLoader.LoadOverlayView(overlayViewName, this, closeCurrentOverlay, pause);
        }

        public virtual UIViewController LoadOverlayViewController(string overlayViewName, bool closeCurrentOverlay, bool pause)
        {
            GameObject overlayPanel = _uiViewLoader.LoadOverlayView(overlayViewName, this, closeCurrentOverlay, pause);
            UIViewController overlayController = overlayPanel.GetComponent<UIViewController>();

            return overlayController;
        }

        // Load Tips View
        public virtual void LoadTipsView(string tipsViewName, bool pause)
        {
            _uiViewLoader.LoadTipsView(tipsViewName, pause);
        }

        public virtual UIViewController LoadTipsViewController(string tipsViewName, bool pause)
        {
            GameObject tipsPanel = _uiViewLoader.LoadTipsView(tipsViewName, pause);
            UIViewController tipsController = tipsPanel.GetComponent<UIViewController>();

            return tipsController;
        }

        // Loading View
        public void EnableLoadingView()
        {
            _uiViewLoader.EnableLoadingView();
        }

        public void DisableLoadingView()
        {
            _uiViewLoader.DisableLoadingView();
        }

        public virtual void CloseView()
        {
            Destroy(this.gameObject);
        }

        public class Factory : Factory<string, UIViewController>
        {
        }
    }

    public class UIViewControllerFactory : IFactory<string, UIViewController>
    {
        private readonly DiContainer _container;

        public UIViewControllerFactory(DiContainer container)
        {
            _container = container;
        }

        public UIViewController Create(string prefabPath)
        {
            GameObject pageView = _container.InstantiatePrefabResource("UI/Prefab/" + prefabPath);
            UIViewController viewController = pageView.GetComponent<UIViewController>();

            return viewController;
        }
    }
}
