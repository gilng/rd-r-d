﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class GameParamMenuViewController : UIOverlayViewController
    {
        private IUserService _userService;
        private EnemyCore _enemyCore;

        [Inject]
        public void ConstructorSafeAttribute(IUserService userService, EnemyCore enemyCore)
        {
            _userService = userService;
            _enemyCore = enemyCore;
        }

        public InputField goldBalance;
        public InputField stage;
        public InputField relic;
        public InputField diamond;

        public InputField fp;
        public InputField ruby;

        public void ChangeSetting()
        {
            try
            {
                if (goldBalance.text.Length > 0)
                    AOMStoreInventory.ResetGold(double.Parse(goldBalance.text));
            }
            catch
            {
            }

            try
            {
                if (diamond.text.Length > 0)
                    _userService.CurrentUserSummary.Gem = int.Parse(diamond.text);
            }
            catch
            {
            }

            try
            {
                if (relic.text.Length > 0)
                    _userService.CurrentUserSummary.Moecrystal = int.Parse(relic.text);
            }
            catch
            {
            }

            try
            {
                if (fp.text.Length > 0)
                    _userService.CurrentUserSummary.FriendPoint += int.Parse(fp.text);
            }
            catch
            {
            }

            try
            {
                if (ruby.text.Length > 0)
                    _userService.CurrentUserSummary.Ruby += int.Parse(ruby.text);
            }
            catch
            {
            }

            try
            {
                if (stage.text.Length > 0)
                {
                    _enemyCore.Stage = int.Parse(stage.text);

                    GameManager.Instance.enemyObject.SetActive(false);
                    GameManager.Instance.LoadNextLevel(false);

                    if (StageManager.Instance != null)
                        StageManager.Instance.ChangeStageWorld(false);

                    // For jump stage, when current stage large than their break level [0], auto-open gallery
                    for (int i = 0; i < GameManager.Instance.enemyPool.bosses.Count; i++)
                    {
                        if (_enemyCore.Stage > GameManager.Instance.enemyPool.bosses[i].breakLevel[0])
                        {
                            if (GalleryCore.Girls[i].Status != MonsterGirlStatus.Killed)
                            {
                                GalleryCore.Girls[i].Status = MonsterGirlStatus.Killed;
                                GalleryCore.Girls[i].ReachedBreakLevel++;
                            }
                        }
                    }
                }
            }
            catch
            {
            }

            if (diamond.text.Length > 0 || relic.text.Length > 0 || ruby.text.Length > 0 || fp.text.Length > 0)
            {
                if (SocialManager.CheckForInternetConnection())
                {
                    _userService.UpdateUserSummary(_userService.CurrentUserSummary, (e) =>
                    {
                        if (e != null)
                        {
                            Debug.LogError("Update UserSummary fail");
                        }
                        else
                        {
                            if (diamond.text.Length > 0)
                                AOMStoreInventory.ResetDiamond(int.Parse(diamond.text));

                            if (relic.text.Length > 0)
                                AOMStoreInventory.ResetRelic(int.Parse(relic.text));
                        }
                    });
                }
            }
        }

        public override void CloseView()
        {
            LoadOverlayView("SettingMainMenuView", false, Time.timeScale == 0);

            base.CloseView();
        }
    }
}
