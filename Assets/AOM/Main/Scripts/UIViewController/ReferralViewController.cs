﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using SmartLocalization;
using System.Collections.Generic;

namespace Ignite.AOM
{
    public class ReferralViewController : UIOverlayViewController
    {
        public Text user_id, referralMsg;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            user_id.text = "";
        }

        public void ShareReferralCode()
        {
            BranchUniversalObject universalObject = new BranchUniversalObject();
            universalObject.canonicalIdentifier = "ignite/attackonmoe";
            universalObject.title = "Attack On Moe";
            universalObject.contentDescription = "Good Game !!";
            universalObject.metadata.Add("IGNITE_CHECK", "TRUE");
			universalObject.metadata.Add("REFERRAL_USER_ID", user_id.text);

            BranchLinkProperties linkProperties = new BranchLinkProperties();
            linkProperties.feature = "sharing";
            linkProperties.channel = "facebook";



            //			Branch.getShortURL (universalObject, linkProperties, (url, error) => {
            //				if (error != null) {
            //					Debug.LogError ("Branch.getShortURL failed: " + error);
            //				} else {
            //					NativeShare.Share (universalObject.title, universalObject.title + "\n" + url);
            //				}
            //			});



#if UNITY_ANDROID
            Branch.getShortURL(universalObject, linkProperties, (url, error) =>
            {
                if (error != null)
                {
                    Debug.LogError("Branch.getShortURL failed: " + error);
                }
                else
                {
                    NativeShare.Share(universalObject.title, universalObject.title + "\n" + url);
                }
            });
#elif UNITY_IOS
			Branch.shareLink(universalObject, linkProperties, universalObject.title, (url, error) => {

				if (error != null) {
//					referralMsg.text += "Referral Error!\nBranch.shareLink failed:"+ error;
				} else {
//					referralMsg.text += "Referral Success!\nBranch.shareLink shared params: " + url;
				}
			});
#endif

        }

        public override void CloseView()
        {
            base.CloseView();
        }

    }
}
