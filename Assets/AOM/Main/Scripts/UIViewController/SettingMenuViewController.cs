﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Soomla.Store;
using Zenject;

namespace Ignite.AOM
{
    public class SettingMenuViewController : UIOverlayViewController
    {
        private VersionCheckerManager _versionCheckerManager;
        private SocialManager _socialManager;
        private CloudDataManager _cloudDataManager;

        [Inject]
        public void ConstructorSafeAttribute(VersionCheckerManager versionCheckerManager, SocialManager socialManager, CloudDataManager cloudDataManager)
        {
            _versionCheckerManager = versionCheckerManager;

            _socialManager = socialManager;

            _cloudDataManager = cloudDataManager;
        }

        public Text musicText, soundText, googleStatusText, versionText, mailNumText;
        public GameObject purchaseRestoreBtn;
        public GameObject developGroup;
        public GameObject mailNumObj;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            UpdateGameMusicStatus();

            UpdateGameSoundStatus();

            UpdateGoogleStatus();

            if (versionText != null)
                versionText.text = "v" + _versionCheckerManager.CurrentVersion;

            if (purchaseRestoreBtn != null)
            {
#if UNITY_ANDROID || UNITY_EDITOR
                purchaseRestoreBtn.SetActive(false);
#elif UNITY_IOS
                purchaseRestoreBtn.SetActive(true);
#endif
            }

            developGroup.SetActive(ExtraParseInitialization.CurrentDevelopMode != DevelopMode.Prod);

            mailNumObj.SetActive(GameManager.Instance.mailText.text != "");
            mailNumText.text = GameManager.Instance.mailText.text;
        }

        void UpdateGameMusicStatus()
        {
            if (musicText != null)
            {
                if (PlayerPrefs.GetInt(SoundManager.PP_BGM_SETTING, 0) == 1)
                    musicText.text = LanguageManager.Instance.GetTextValue("setting_menu_music") + " : " + LanguageManager.Instance.GetTextValue("setting_OFF");//"Music: OFF";
                else
                    musicText.text = LanguageManager.Instance.GetTextValue("setting_menu_music") + " : " + LanguageManager.Instance.GetTextValue("setting_ON");//"Music: ON ";
            }
        }

        void UpdateGameSoundStatus()
        {
            if (soundText != null)
            {
                if (PlayerPrefs.GetInt(SoundManager.PP_SFX_SETTING, 1) == 1)
                    soundText.text = LanguageManager.Instance.GetTextValue("setting_menu_sound") + " : " + LanguageManager.Instance.GetTextValue("setting_ON");//"Sound: ON ";"Sound: ON";
                else
                    soundText.text = LanguageManager.Instance.GetTextValue("setting_menu_sound") + " : " + LanguageManager.Instance.GetTextValue("setting_OFF");//"Sound: OFF";
            }
        }

        public void SwitchMusicOnOff()
        {
            if (musicText != null)
            {
                SoundManager.Instance.SwitchMusicOnOff(musicText);

                UpdateGameMusicStatus();
            }
        }

        public void SwitchSoundOnOff()
        {
            if (soundText != null)
            {
                SoundManager.Instance.SwitchSoundOnOff(soundText);

                UpdateGameSoundStatus();
            }
        }

        public void LanguageSetting()
        {
            LoadOverlayView("SettingLanguageMenuView", true, Time.timeScale == 0);
        }

        public void GameStatistic()
        {
            LoadOverlayView("StatisticsMenuView", true, Time.timeScale == 0);
        }

        public void RestorePurchase()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (!SoomlaStore.TransactionsAlreadyRestored())
            {
                SoomlaStore.RestoreTransactions();
            }
        }

        public void SaveGame()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            PlayerPrefs.Save();

            if (SocialManager.CheckForInternetConnection())
            {
                if (_socialManager.parseSignedIn)
                {
                    if (_socialManager.linkedWithEmail || _socialManager.linkedWithGoogle)
                    {
                        if (_cloudDataManager != null)
                        {
                            _cloudDataManager.ShowWarningOverlay("SAVE");
                        }
                    }
                    else
                    {
                        // To Do Implement
                        LinkingReminderViewController linkingReminderViewController = LoadOverlayViewController("LinkingReminderViewOverlay", false, Time.timeScale == 0) as LinkingReminderViewController;
                    }
                }
                else
                {
                    CloudSavingOverlayController signUpOverlayController = LoadOverlayViewController("CloudSavingOverlay", false, Time.timeScale == 0) as CloudSavingOverlayController;

                    signUpOverlayController.ShowSigninMessage();

                    Action parseSignUpSuccessCallback = () =>
                    {
                        if (_socialManager.linkedWithEmail || _socialManager.linkedWithGoogle)
                        {
                            if (_cloudDataManager != null)
                            {
                                _cloudDataManager.ShowWarningOverlay("SAVE");
                            }
                        }
                        else
                        {
                            // To Do Implement
                            LinkingReminderViewController linkingReminderViewController = LoadOverlayViewController("LinkingReminderViewOverlay", false, Time.timeScale == 0) as LinkingReminderViewController;
                        }
                    };

                    Action parseSignUpFailCallback = () =>
                    {
                        signUpOverlayController.ShowSigninFailMessage();
                    };

                    _socialManager.ParseSignUp(parseSignUpSuccessCallback, parseSignUpFailCallback);
                }
            }
            else
            {
                CloudSavingOverlayController saveProgressOverlayController = LoadOverlayViewController("CloudSavingOverlay", false, Time.timeScale == 0) as CloudSavingOverlayController;

                saveProgressOverlayController.ShowSaveFailMessage();
            }

            //if (_socialManager.parseSignedIn)
            //{
            //    if (_cloudDataManager != null)
            //    {
            //        signInOverlayController.ShowSavingMessage();
            //
            //        Action SaveProgressToParseSuccessCallback = () =>
            //        {
            //            if (signInOverlayController != null)
            //                signInOverlayController.ShowSavedMessage();
            //        };
            //
            //        Action SaveProgressToParseFailCallback = () =>
            //        {
            //            if (signInOverlayController != null)
            //                signInOverlayController.ShowSaveFailMessage();
            //        };
            //
            //        _cloudDataManager.SaveProgressToParseUser(false, SaveProgressToParseSuccessCallback, SaveProgressToParseFailCallback);
            //    }
            //}
            //else
            //{
            //    signInOverlayController.ShowSigninMessage();
            //
            //    Action googleSignInSuccessCallback = () =>
            //    {
            //        Action parseSignInSuccessCallback = () =>
            //        {
            //            signInOverlayController.ShowSavingMessage();
            //
            //            Action SaveProgressToParseSuccessCallback = () =>
            //            {
            //                if (signInOverlayController != null)
            //                    signInOverlayController.ShowSavedMessage();
            //            };
            //
            //            Action SaveProgressToParseFailCallback = () =>
            //            {
            //                if (signInOverlayController != null)
            //                    signInOverlayController.ShowSaveFailMessage();
            //            };
            //
            //            _cloudDataManager.SaveProgressToParseUser(false, SaveProgressToParseSuccessCallback, SaveProgressToParseFailCallback);
            //        };
            //
            //        Action parseSignInFailCallback = () =>
            //        {
            //            if (signInOverlayController != null)
            //                signInOverlayController.ShowSigninFailMessage();
            //        };
            //
            //        _socialManager.ParseSignInWithGoogle(parseSignInSuccessCallback, parseSignInFailCallback);
            //    };
            //
            //    Action googleSignInFailCallback = () =>
            //    {
            //        if (signInOverlayController != null)
            //            signInOverlayController.ShowSigninFailMessage();
            //    };
            //
            //    _socialManager.GoogleSignIn(true, googleSignInSuccessCallback, googleSignInFailCallback);
            //}
        }

        public void ResetGame()
        {
            GameManager.Instance.ResetGame();

            CloseView();
        }

        public void GameParameterSetting()
        {
            LoadOverlayView("SettingGameParamMenuView", true, Time.timeScale == 0);
        }

        public void TweetMessage()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (TwitterManager.Instance != null)
            {
                TwitterManager.Instance.TweetMessage();
            }
        }

        public void ManualAddFriend()
        {
            LoadOverlayView("ManualInviteFriendViewOverlay", false, Time.timeScale == 0);
        }

        void UpdateGoogleStatus()
        {
            if (googleStatusText != null)
            {
                if (!_socialManager.googleSignedIn)
                    googleStatusText.text = LanguageManager.Instance.GetTextValue("setting_menu_google_signIn");
                else
                    googleStatusText.text = LanguageManager.Instance.GetTextValue("setting_menu_google_signOut");
            }
        }

        public void GoogleSignIn()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            bool signIn = !_socialManager.googleSignedIn;

            CloudSavingOverlayController signInOverlayController = null;

            if (signIn)
            {
                signInOverlayController = LoadOverlayViewController("CloudSavingOverlay", false, Time.timeScale == 0) as CloudSavingOverlayController;
                signInOverlayController.ShowSigninMessage();
            }

            Action signInSuccessCallback = () =>
            {
                if (signInOverlayController != null)
                    signInOverlayController.CloseView();

                UpdateGoogleStatus();
            };

            Action signInFailCallback = () =>
            {
                if (signInOverlayController != null)
                    signInOverlayController.ShowSigninFailMessage();

                UpdateGoogleStatus();
            };

            _socialManager.GoogleSignIn(signIn, signInSuccessCallback, signInFailCallback);
        }

        public void ViewUserID()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            LoadOverlayView("UserIDViewOverlay", false, Time.timeScale == 0);
        }

        public void DropBox()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            LoadOverlayView("DropBoxMenu", false, Time.timeScale == 0);

            base.CloseView();
        }

        public void PrivacyPolicy()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            Application.OpenURL("http://ignite-ga.me/privacy-policy.html");
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            base.CloseView();

            if (Time.timeScale == 0)
                Time.timeScale = 1;
        }
    }
}
