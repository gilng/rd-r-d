﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using SmartLocalization;

namespace Ignite.AOM
{
    public class PvPResultViewController : UIOverlayViewController
    {
        public Text txt_title, txt_message, txt_score, txt_ruby;

        public Text txt_btn;
        public Button btn;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);
        }

        public void InputResult(string message = "", int score = 0, int ruby = 0, Action btnAction = null)
        {
            txt_title.text = LanguageManager.Instance.GetTextValue("pvp_result_title");
            txt_message.text = message;
            txt_score.text = score.ToString();
            txt_ruby.text = ruby.ToString();

            txt_btn.text = LanguageManager.Instance.GetTextValue("pvp_result_btn");

            btn.onClick.AddListener(() =>
            {
                if (btnAction != null)
                {
                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                    btnAction();
                }

                CloseView();
            });
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            base.CloseView();
        }
    }
}