﻿using System;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class ManualInviteFriendViewController : UIOverlayViewController
    {
        private SocialManager _socialManager;
        private IFriendService _friendService;

        [Inject]
        public void ConstructorSafeAttribute(SocialManager socialManager, IFriendService friendService)
        {
            _socialManager = socialManager;
            _friendService = friendService;
        }

        public InputField input_userId;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            if (!_socialManager.parseSignedIn)
            {
                // Check Connection
                if (SocialManager.CheckForInternetConnection())
                {
                    // Create Account
                    Action successCallback = () => { };

                    Action failCallback = () =>
                    {
                        CloudSavingOverlayController signInOverlayController =
                            LoadOverlayViewController("CloudSavingOverlay", false, Time.timeScale == 0) as
                                CloudSavingOverlayController;

                        signInOverlayController.ShowSigninFailMessage();

                        base.CloseView();
                    };

                    _socialManager.ParseSignUp(successCallback, failCallback);
                }
                else
                {
                    CloudSavingOverlayController signInOverlayController =
                        LoadOverlayViewController("CloudSavingOverlay", false, Time.timeScale == 0) as
                            CloudSavingOverlayController;

                    signInOverlayController.ShowSigninFailMessage();

                    base.CloseView();
                }
            }
        }

        public void Invite()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (input_userId.text == "") return;

            Action<FriendRelation> AddSuccessCallback = friendRelation =>
            {
                CloseView();

                var msgPopupViewController = LoadOverlayViewController("MsgPopupOverlay", false, Time.timeScale == 0) as MsgPopupViewController;
                msgPopupViewController.InputMsg("fds_title_add", string.Format(LanguageManager.Instance.GetTextValue("fds_msg_add_success"), input_userId.text));
            };

            Action<string> AddFailCallback = errorCode =>
            {
                string errorMsgKey = "";

                if (errorCode == "FriendRelationAlreadyExists")
                    errorMsgKey = "fds_msg_add_fail_alreadyExist";
                else if (errorCode == "UserFriendListReachedMaximum")
                    errorMsgKey = "fds_msg_add_fail_userListMax";
                else if (errorCode == "TargetUserFriendListReachedMaximum")
                    errorMsgKey = "fds_msg_add_fail_friendListMax";
                else if (errorCode == "TimeOut")
                    errorMsgKey = "errorMsg_TimeOut";
                else
                    errorMsgKey = "fds_msg_add_fail_invalidTarget";

                var msgPopupViewController = LoadOverlayViewController("MsgPopupOverlay", false, Time.timeScale == 0) as MsgPopupViewController;
                msgPopupViewController.InputMsg("fds_title_add", string.Format(LanguageManager.Instance.GetTextValue(errorMsgKey), input_userId.text));
            };

            _friendService.AddFriend(input_userId.text, AddSuccessCallback, AddFailCallback);
        }

        public void Remove()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (input_userId.text != "")
            {
            }
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            base.CloseView();
        }
    }
}