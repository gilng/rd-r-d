﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;
using System;
using System.Collections.Generic;
using System.Text;
using Zenject;

namespace Ignite.AOM
{
    public class HeroDeadViewController : UIOverlayViewController
    {
        private HeroCore _heroCore;

        [Inject]
        public void ConstructorSafeAttribute(HeroCore heroCore)
        {
            _heroCore = heroCore;
        }

        public SpriteRenderer heroIcon;
        public Text diedHeroNameLabel;

        public Text dps, dpsLabel;

        public Text allDamage;
        public Text criticalDamage;
        public Text goldDropAmount;
        public Text tapDamage;
        public Text totalDPSTapDamage;
        public Text treasureChestGoldAmount;
        public Text criticalChance;
        public Text bossDamage;

        public Text reviveTime;
        public Text revivePrice;
        public Button reviveButton;

        private int heroIdx = -1;
        private float updateTimer = 0;

        Sprite[] thumbnailIcon;
        string[] thumbnailIconName;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);
        }

        void Update()
        {
            UpdateReviveTime();
        }

        public void SetHeroIdx(int heroIdx)
        {
            this.heroIdx = heroIdx;

            if (heroIdx != -1)
            {
                dps.text = UnitConverter.ConverterDoubleToString(_heroCore.HeroDPS[this.heroIdx] * -1);
                allDamage.gameObject.SetActive(false);
                criticalDamage.gameObject.SetActive(false);
                goldDropAmount.gameObject.SetActive(false);
                tapDamage.gameObject.SetActive(false);
                totalDPSTapDamage.gameObject.SetActive(false);
                treasureChestGoldAmount.gameObject.SetActive(false);
                criticalChance.gameObject.SetActive(false);
                bossDamage.gameObject.SetActive(false);

                IDictionary<EffectType, double> effectValue = new Dictionary<EffectType, double>();
                for (int i = 0; i < HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[this.heroIdx].ItemId].Count; i++)
                {
                    if (HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[this.heroIdx].ItemId][i].GetBalance() > 0)
                    {
                        EffectType t = HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[this.heroIdx].ItemId][i].EffectType;
                        if (!effectValue.ContainsKey(t))
                        {
                            effectValue[t] = 0;
                        }
                        effectValue[t] += HeroAssets.HeroSkillsByHeroId[HeroAssets.Heroes[this.heroIdx].ItemId][i].EffectValue;
                    }
                }
                StringBuilder sb = new StringBuilder();
                foreach (KeyValuePair<EffectType, double> e in effectValue)
                {
                    sb.Clear();
                    sb.Append((e.Value * -100).ToString());
                    sb.Append("%");
                    Text text = null;
                    if (EffectType.AllDamage == e.Key)
                    {
                        allDamage.gameObject.SetActive(true);
                        text = allDamage;
                    }
                    if (EffectType.CriticalDamage == e.Key)
                    {
                        criticalDamage.gameObject.SetActive(true);
                        text = criticalDamage;
                    }
                    if (EffectType.GoldDropAmount == e.Key)
                    {
                        goldDropAmount.gameObject.SetActive(true);
                        text = goldDropAmount;
                    }
                    if (EffectType.TapDamage == e.Key)
                    {
                        tapDamage.gameObject.SetActive(true);
                        text = tapDamage;
                    }
                    if (EffectType.TotalDPSTapDamage == e.Key)
                    {
                        totalDPSTapDamage.gameObject.SetActive(true);
                        text = totalDPSTapDamage;
                    }
                    if (EffectType.TreasureChestGoldAmount == e.Key)
                    {
                        treasureChestGoldAmount.gameObject.SetActive(true);
                        text = treasureChestGoldAmount;
                    }
                    if (EffectType.CriticalChance == e.Key)
                    {
                        criticalChance.gameObject.SetActive(true);
                        text = criticalChance;
                    }
                    if (EffectType.BossDamage == e.Key)
                    {
                        bossDamage.gameObject.SetActive(true);
                        text = bossDamage;
                    }
                    if (text != null)
                        text.text = sb.ToString();
                }

                if (thumbnailIcon == null && thumbnailIconName == null)
                {
                    thumbnailIcon = Resources.LoadAll<Sprite>("Heros/Texture/hero_sprite");
                    thumbnailIconName = new string[thumbnailIcon.Length];

                    for (int i = 0; i < thumbnailIconName.Length; i++)
                    {
                        thumbnailIconName[i] = thumbnailIcon[i].name;
                    }
                }

                heroIcon.sprite = thumbnailIcon[Array.IndexOf(thumbnailIconName, HeroAssets.Heroes[this.heroIdx].ItemId.ToLower() + "_01")];

                diedHeroNameLabel.text = LanguageManager.Instance.GetTextValue(HeroAssets.Heroes[this.heroIdx].ItemId + "_name");

                updateTimer = 1;
                UpdateReviveTime();
            }
        }

        void UpdateReviveTime()
        {
            if (!_heroCore.HeroAlive[heroIdx])
            {
                updateTimer += Time.deltaTime;
                if (updateTimer >= 1)
                {
                    TimeSpan waitingTime = TimeSpan.FromSeconds((_heroCore.HeroReviveTime[heroIdx] - DateTime.Now).TotalSeconds);
                    reviveTime.text = string.Format("{0:D2}:{1:D2}:{2:D2}", waitingTime.Hours, waitingTime.Minutes, waitingTime.Seconds);
                    revivePrice.text = (Math.Ceiling(waitingTime.TotalHours) * 4).ToString();
                    if (UserManager.Instance.CurrentUserSummary != null)
                    {
                        if (UserManager.Instance.CurrentUserSummary.Gem >= int.Parse(revivePrice.text))
                        {
                            reviveButton.GetComponent<Image>().sprite = reviveButton.GetComponent<Button>()
                                .spriteState.highlightedSprite;
                            //reviveButton.interactable = true;
                        }
                        else
                        {
                            reviveButton.GetComponent<Image>().sprite = reviveButton.GetComponent<Button>()
                                .spriteState.pressedSprite;
                            //reviveButton.interactable = false;
                        }
                    }
                    else
                    {
                        reviveButton.GetComponent<Image>().sprite = reviveButton.GetComponent<Button>()
                            .spriteState.pressedSprite;
                        //reviveButton.interactable = false;
                    }

                    updateTimer = 0;
                }
            }
        }

        public void ReviveHero()
        {
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                if (UserManager.Instance.CurrentUserSummary.Gem >= int.Parse(revivePrice.text))
//                {
                    //SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                    _heroCore.ReviveHero(heroIdx, int.Parse(revivePrice.text));
                    CloseView();
//                }
//                else
//                {
//                    OpenShopMenu();
//                }
//            }
        }

        public void OpenShopMenu()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            LoadOverlayView("ShopMenu", false, false);
        }
    }
}
