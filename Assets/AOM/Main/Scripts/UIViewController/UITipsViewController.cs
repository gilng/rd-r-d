﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Ignite.AOM
{
    public class UITipsViewController : UIViewController
    {
        // Use this for Start
        protected override void Start()
        {
            currentUIViewType = UIViewType.TIPS;

            base.Start();
        }

        // Use this for initialization
        public override void InitView(UIViewController previousUIView = null, bool pause = false)
        {
            base.InitView(previousUIView, pause);
        }

        public override void CloseView()
        {
            base.CloseView();
        }
    }
}
