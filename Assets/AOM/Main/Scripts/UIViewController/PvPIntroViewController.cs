﻿using SmartLocalization;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Ignite.AOM
{
    public class PvPIntroViewController : UIOverlayViewController
    {
        private IUserService _userService;

        [Inject]
        public void ConstructorSafeAttribute(IUserService userService)
        {
            _userService = userService;
        }

        public Animator animator;
        public Text pvpText;
        public GameObject[] pages;
        public Text nameField;

        private int currentStep;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

//            currentStep = 0;
//            ChangeStep();
        }

        public void NextStep()
        {
//            switch (currentStep)
//            {
//                case 0:
//                    // Do nothing
//                    break;
//                case 1:
//                    animator.Play("Play_ChangeScene");
//                    break;
//                case 2:
//                    animator.Play("Play_InputName");
//                    break;
//            }
        }

        public void ChangeStep()
        {
//            for (int i = 0; i < pages.Length; i++)
//            {
//                pages[i].SetActive(false);
//            }
//
//            if (currentStep < pages.Length)
//                pages[currentStep].SetActive(true);
//
//            pvpText.text = "";
//
//            currentStep++;
        }

        public void ChangeNameStep()
        {
            if (nameField.text != "")
            {
                if (SocialManager.CheckForInternetConnection())
                {
                    // Check input name is unique in server
                    _userService.UpdateUserDisplayName(nameField.text, userSummary =>
                    {
                        if (userSummary != null)
                        {
//                          animator.Play("Play_End");
                            animator.Play("Play_OneShot_End");
                        }
                        else
                        {
                            ShowNoInternetPopup();
                        }
                    });
                }
                else
                {
                    ShowNoInternetPopup();
                }
            }
        }

        public void EndStep()
        {
            IntroEnd();
        }

        public void IntroEnd()
        {
            PlayerPrefs.SetInt("PVP_INTRO_VIEWED", 1);

            LoadOverlayViewController("PvPMenu", false, true);

            CloseView();
        }

        public void ShowNoInternetPopup()
        {
            MsgPopupViewController msgPopupViewController = LoadOverlayViewController("MsgPopupOverlay", false, Time.timeScale == 0) as MsgPopupViewController;
            msgPopupViewController.InputMsg(LanguageManager.Instance.GetTextValue("saveLoadWarning_title"),
                LanguageManager.Instance.GetTextValue("pvp_errorMsg_NoInternet"), "btn_ok", () =>
                {
                    Time.timeScale = 1;
                    CloseView();
                });
        }
    }
}