﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class OfflineGoldDropViewController : UITipsViewController
    {
        public Text descLabel;
        public Text doubleGoldPriceLabel;
        public Text doubleGoldLabel;

        public GameObject doubleGoldBtn;
        public GameObject singleGoldBtn;

        int costGems = 30;

        public override void InitView(UIViewController previousUIView = null, bool pause = false)
        {
            base.InitView(previousUIView, pause);

            string gold_x1 = UnitConverter.ConverterDoubleToString(OfflineGoldManager.instance.OfflineGoldDropAmount);

            descLabel.text = string.Format(LanguageManager.Instance.GetTextValue("offline_gold_drop_label"), gold_x1);

            doubleGoldLabel.text = UnitConverter.ConverterDoubleToString(OfflineGoldManager.instance.OfflineGoldDropAmount * 2);

            doubleGoldPriceLabel.text = costGems.ToString();
        }

        public void GetDoubleGold()
        {
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                if (UserManager.Instance.CurrentUserSummary.Gem >= costGems)
//                {
                    //SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                    UserManager.Instance.TakeGems(costGems, () =>
                    {
                        OfflineGoldManager.instance.OfflineGoldDrop(2);

                        // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                        Analytics.CustomEvent("OfflineGold", new Dictionary<string, object>
                        {
                            { "type", "GetDoubleGold" }
                        });

                        Analytics.CustomEvent("useGems", new Dictionary<string, object>
                        {
                            { "type", "Double Offline Gold" },
                        });
#endif
                        CloseView();
                    });
//                }
//                else
//                {
//                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
//
//                    LoadOverlayView("ShopMenu", false, Time.timeScale == 0);
//                }
//            }
        }

        public void GetGold()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            OfflineGoldManager.instance.OfflineGoldDrop(1);
            CloseView();

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.CustomEvent("OfflineGold", new Dictionary<string, object>
            {
                { "type", "GetGold" }
            });
#endif
        }

        public override void CloseView()
        {
            base.CloseView();

            Time.timeScale = 1f;
        }
    }
}