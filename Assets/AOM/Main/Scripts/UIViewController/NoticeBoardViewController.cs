﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;
using System;
using System.Collections.Generic;
using Zenject;

namespace Ignite.AOM
{
    public class NoticeBoardViewController : UIOverlayViewController
    {
        int currentPage = 0;
        int showPage = 0;

        public List<Toggle> dontShowToggle;
        public List<GameObject> page;
        public Text t1, t2, t3, t4;

        private VersionCheckerManager _versionCheckerManager;

        [Inject]
        public void ConstructorSafeAttribute(VersionCheckerManager versionCheckerManager)
        {
            _versionCheckerManager = versionCheckerManager;
        }

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            //string current_version = PlayerPrefs.GetString("CURRENT_VERSION", "0.0.0");
            string current_version = _versionCheckerManager.CurrentVersion;
            string previous_version = PlayerPrefs.GetString("PREVIOUS_VERSION", "0.0.0");

            if (!string.Equals(current_version, previous_version))
            {
                PlayerPrefs.SetString("PREVIOUS_VERSION", current_version);

                for (int i = 0; i < page.Count; i++)
                {
                    if (PlayerPrefs.HasKey("DONT_SHOW_NOTICE_" + i))
                        PlayerPrefs.DeleteKey("DONT_SHOW_NOTICE_" + i);
                }
            }

            t1.text = AOMAssets.CurrencyPackMarketPriceAndCurrency("diamonds_1200");
            t2.text = AOMAssets.CurrencyPackMarketPriceAndCurrency("diamonds_3100");
            t3.text = AOMAssets.CurrencyPackMarketPriceAndCurrency("diamonds_6500");
            t4.text = AOMAssets.CurrencyPackMarketPriceAndCurrency("diamonds_14000");

            // Close another page
            for (int i = 0; i < page.Count; i++)
            {
                if (PlayerPrefs.GetInt("DONT_SHOW_NOTICE_" + (i + 1), 0) == 0)
                    showPage++;
            }

            if (showPage > 0)
                ChangeContent();
            else
                CloseView();
        }

        public void ChangeContent()
        {
            bool openPage = false;
            bool closePage = false;

            do
            {
                if (currentPage <= page.Count - 1)
                {
                    if (currentPage != 0)
                        SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                    if (PlayerPrefs.GetInt("DONT_SHOW_NOTICE_" + (currentPage + 1), 0) == 0)
                    {
                        openPage = true;

                        for (int i = 0; i < page.Count; i++)
                        {
                            page[i].SetActive(currentPage == i);
                        }
                    }

                    currentPage++;
                }
                else
                {
                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                    closePage = true;
                }

            } while (openPage == false && closePage == false);

            if (closePage)
                CloseView();
        }

        public void DontShowNotice(int page)
        {
            PlayerPrefs.SetInt("DONT_SHOW_NOTICE_" + page, dontShowToggle[page - 1].isOn ? 1 : 0);
        }

        public override void CloseView()
        {
            if (_versionCheckerManager.ReparationAvailable)
            {
                if (PlayerPrefs.GetInt(ReparationViewController.Reparation_PP_KEY + _versionCheckerManager.CurrentVersion, 0) == 0)
                {
                    LoadOverlayView("ReparationMenuView", false, false);
                }
            }

            if (OfflineGoldManager.instance != null)
            {
                OfflineGoldManager.instance.noticeBoardLastShowTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
                OfflineGoldManager.instance.noticeBoardShowing = false;
            }

            base.CloseView();
        }
    }
}
