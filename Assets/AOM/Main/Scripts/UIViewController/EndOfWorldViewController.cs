﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;

namespace Ignite.AOM
{
    public class EndOfWorldViewController : UIOverlayViewController
	{
        public Animator eowAnimator;
        public GameObject stopAnimeBtn;

        public bool timerOn = false;

        float timer, time = 5;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            timer = time;

            eowAnimator.Play("EndOfWorldAnimator_FadeIn");
        }

        // Update is called once per frame
        void Update()
        {
            if (timerOn)
            {
                if (timer > 0)
                {
                    timer -= Time.deltaTime;
                }
                else
                {
                    FadeOutAnimation();
                }
            }
        }

        // Animation use
        public void WaitingPress()
        {
            timerOn = true;

            stopAnimeBtn.SetActive(true);
        }

        public void FadeOutAnimation()
        {
            timerOn = false;
            stopAnimeBtn.SetActive(false);

            eowAnimator.Play("EndOfWorldAnimator_FadeOut");
        }
	}
}
