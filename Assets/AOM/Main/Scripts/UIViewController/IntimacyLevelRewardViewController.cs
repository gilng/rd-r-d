﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;
using System;
using Zenject;

namespace Ignite.AOM
{
    public class IntimacyLevelRewardViewController : UIOverlayViewController
    {
        private GalleryCore _galleryCore;

        [Inject]
        public void ConstructorSafeAttribute(GalleryCore galleryCore)
        {
            _galleryCore = galleryCore;
        }

        public Text rewardTitle;
        public Text intimacyLvLabel;
        public Text rewardLabel;
        public Text rewardDesc_1;
        public Text[] rewardDesc_Diamond;
        public Text[] rewardDesc_Relic;

        Gallery currCharacterGallery;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            currCharacterGallery = _galleryCore.GetGirlWithIndex(FieldGuideManager.Instance.currentSelectedBossIndex);

            rewardTitle.text = LanguageManager.Instance.GetTextValue("gallery_reward_title");
            intimacyLvLabel.text = LanguageManager.Instance.GetTextValue("gallery_reward_intimacyLvLabel");
            rewardLabel.text = LanguageManager.Instance.GetTextValue("gallery_reward_rewardLabel");
            rewardDesc_1.text = LanguageManager.Instance.GetTextValue("gallery_reward_rewardDesc");

            int gainValueSet = 0;

            if (int.Parse(currCharacterGallery.girlId) < 5)
                gainValueSet = 0;
            else
                gainValueSet = 1;

            for (int i = 0; i < 4; i++)
            {
                rewardDesc_Diamond[i].text = "x" + FieldGuideManager.Instance.diamondVal[gainValueSet, i].ToString();
                rewardDesc_Relic[i].text = "x" + FieldGuideManager.Instance.relicVal[gainValueSet, i].ToString();
            }
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            base.CloseView();
        }
    }
}
