﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using System;
using System.Collections.Generic;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class MoetifactAndMoeSpiritDetailViewController : UIOverlayViewController
    {
        private ArtifactCore _artifactCore;
        private IUserService _userService;
        private GalleryCore _galleryCore;

        [Inject]
        public void ConstructorSafeAttribute(ArtifactCore artifactCore, IUserService userService, GalleryCore galleryCore)
        {
            _artifactCore = artifactCore;
            _userService = userService;
            _galleryCore = galleryCore;
        }

        private ArtifactVG item;
        private bool isMoetifact;
        private bool openByTableCell;

        public Text title;
        public Text name;
        public Image icon;

        public GameObject rareLevelStar1;
        public GameObject rareLevelStar2;
        public GameObject rareLevelStar3;

        public Text levelText;
        public Text currentEffectValue;
        public Text currentAllDamage;

        public GameObject subContentBG;
        public Text baseEffectValue;
        public Text baseAllDamage;

        public Text artifactDescription;

        public Image lockedImage;
        public Image ownedImage;

        public Button salvageBtn;
        public Text salvagePrice;
        public Text salvage_currentGemsLabel;

        public Button rubyUnlockBtn;
        public Text rubyUnlockPrice;
        public Text unlock_currentRubyLabel;

        public Button gemUnlockBtn;
        public Text gemUnlockPrice;
        public Text unlock_currentGemsLabel;

        public Button rubyMoeSpiritLevelUpBtn;
        public Text rubyMoeSpiritLevelUpPrice;
        public Text moeSpiritLevelUp_currentRubyLabel;

        public Button gemMoeSpiritLevelUpBtn;
        public Text gemMoeSpiritLevelUpPrice;
        public Text moeSpiritLevelUp_currentGemsLabel;

        private int[] unlockRubyVal = new int[] {1000, 2000, 3000, 4000};
        private int[] unlockGemsVal = new int[] {400, 600, 800};

        private int rubyTier = 0;

        private int current_ruby = -1;


        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            AddListener();
        }

        void AddListener()
        {
            EventManager.OnLanguageChange += RenderMoetifactDetail;
            EventManager.OnDiamondBalanceUpdated += RefreshOwnedGemsDisplay;
        }

        void RemoveListener()
        {
            EventManager.OnLanguageChange -= RenderMoetifactDetail;
            EventManager.OnDiamondBalanceUpdated -= RefreshOwnedGemsDisplay;
        }

        public void SetDisplayItem(ArtifactVG item, bool isMoetifact = true, bool openByTableCell = false)
        {
            this.item = item;
            this.isMoetifact = isMoetifact;
            this.openByTableCell = openByTableCell;

            if (isMoetifact)
            {
                title.text = LanguageManager.Instance.GetTextValue("artifact_menu_title");
                RenderMoetifactDetail();
            }
            else
            {
                if (_userService.CurrentUserSummary != null)
                {
                    current_ruby = _userService.CurrentUserSummary.Ruby;
                }

                title.text = LanguageManager.Instance.GetTextValue("artifact_menu_title_MoeSpirit");
                RenderMoeSpiritDetail();
            }
        }

        void RenderMoetifactDetail()
        {
            rubyTier = item.RareLevel - 1;

            lockedImage.gameObject.SetActive(!(item.GetBalance() > 0));
            ownedImage.gameObject.SetActive(false);
            rubyMoeSpiritLevelUpBtn.gameObject.SetActive(false);
            gemMoeSpiritLevelUpBtn.gameObject.SetActive(false);

            rubyUnlockBtn.gameObject.SetActive(false);
            gemUnlockBtn.gameObject.SetActive(false);

            salvageBtn.gameObject.SetActive(item.GetBalance() > 0);
            salvageBtn.GetComponent<Image>().sprite =
                (UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem >= _artifactCore.SalvageCost(item) : AOMStoreInventory.GetDiamondBalance() >= _artifactCore.SalvageCost(item))
                    ? salvageBtn.spriteState.highlightedSprite
                    : salvageBtn.spriteState.pressedSprite;
            salvagePrice.text = UnitConverter.ConverterDoubleToString(_artifactCore.SalvageCost(item));

            CommonRenderDetail("Sprites/overlay_icon_artifact/overlay_icon_artifact");
        }

        void RenderMoeSpiritDetail()
        {
            rubyTier = (item == ArtifactAssets.MoeSpirit028 || item == ArtifactAssets.MoeSpirit029 || item == ArtifactAssets.MoeSpirit030) ? item.RareLevel : item.RareLevel - 1;
            if (openByTableCell)
            {
                lockedImage.gameObject.SetActive(item.GetBalance() == 0);

                rubyUnlockBtn.gameObject.SetActive(false);
                gemUnlockBtn.gameObject.SetActive(false);
            }
            else
            {
                lockedImage.gameObject.SetActive(false);

                rubyUnlockBtn.gameObject.SetActive(item.GetBalance() == 0);
                rubyUnlockBtn.GetComponent<Image>().sprite =
                    (current_ruby != -1 && current_ruby > unlockRubyVal[rubyTier])
                        ? rubyUnlockBtn.spriteState.highlightedSprite
                        : rubyUnlockBtn.spriteState.pressedSprite;
                rubyUnlockBtn.interactable = (current_ruby != -1 && current_ruby > unlockRubyVal[rubyTier]);
                rubyUnlockPrice.text = unlockRubyVal[rubyTier].ToString();

                unlock_currentRubyLabel.text = string.Format(LanguageManager.Instance.GetTextValue("current_ruby"),
                    UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Ruby.ToString() : ((current_ruby == -1) ? "0" : current_ruby.ToString()));

                gemUnlockBtn.gameObject.SetActive(item.GetBalance() == 0);
                gemUnlockBtn.GetComponent<Image>().sprite =
                    (UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem >= unlockGemsVal[item.RareLevel - 1] : AOMStoreInventory.GetDiamondBalance() >= unlockGemsVal[item.RareLevel - 1])
                        ? gemUnlockBtn.spriteState.highlightedSprite
                        : gemUnlockBtn.spriteState.pressedSprite;
                gemUnlockPrice.text = unlockGemsVal[item.RareLevel - 1].ToString();
            }

            ownedImage.gameObject.SetActive(item.GetBalance() > 0);

            rubyMoeSpiritLevelUpBtn.gameObject.SetActive(item.GetBalance() > 0 && item.GetBalance() < item.MaxLevel);

            rubyMoeSpiritLevelUpBtn.GetComponent<Image>().sprite =
                ((UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Ruby >= unlockRubyVal[rubyTier] : current_ruby >= unlockRubyVal[rubyTier]) && item.GetBalance() < item.MaxLevel)
                    ? rubyMoeSpiritLevelUpBtn.spriteState.highlightedSprite
                    : rubyMoeSpiritLevelUpBtn.spriteState.pressedSprite;

            rubyMoeSpiritLevelUpPrice.text = item.GetBalance() < item.MaxLevel ? ((item.GetBalance() + 1) * unlockRubyVal[rubyTier]).ToString() : "--";

            moeSpiritLevelUp_currentRubyLabel.text = string.Format(LanguageManager.Instance.GetTextValue("current_ruby"),
                UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Ruby.ToString() : ((current_ruby == -1) ? "0" : current_ruby.ToString()));

            gemMoeSpiritLevelUpBtn.gameObject.SetActive(item.GetBalance() > 0 && item.GetBalance() < item.MaxLevel);

            gemMoeSpiritLevelUpBtn.GetComponent<Image>().sprite =
                ((UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem >= unlockGemsVal[item.RareLevel - 1] : AOMStoreInventory.GetDiamondBalance() >= unlockGemsVal[item.RareLevel - 1]) && item.GetBalance() < item.MaxLevel)
                    ? gemMoeSpiritLevelUpBtn.spriteState.highlightedSprite
                    : gemMoeSpiritLevelUpBtn.spriteState.pressedSprite;

            gemMoeSpiritLevelUpPrice.text = item.GetBalance() < item.MaxLevel ? unlockGemsVal[item.RareLevel - 1].ToString() : "--";

            moeSpiritLevelUp_currentGemsLabel.text = item.GetBalance() < item.MaxLevel ? string.Format(LanguageManager.Instance.GetTextValue("current_gems"),
                UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString()) : "";

            salvageBtn.gameObject.SetActive(false);

            CommonRenderDetail("Sprites/overlay_icon_moeSpirit/overlay_icon_moeSpirit");
        }

        void CommonRenderDetail(string _path)
        {
            Sprite[] _sprites = Resources.LoadAll<Sprite>(_path);

            foreach (Sprite _sprite in _sprites)
            {
                if (_sprite.name == item.ItemId)
                    icon.sprite = _sprite;
            }

            name.text = LanguageManager.Instance.GetTextValue(item.ItemId + "_name");
            levelText.text = item.GetBalance() + " " + (item.GetBalance() == item.MaxLevel ? "Max Level" : "");

            rareLevelStar1.SetActive(this.item.RareLevel > 0);
            rareLevelStar2.SetActive(this.item.RareLevel > 1);
            rareLevelStar3.SetActive(this.item.RareLevel > 2);

            if (this.item.RareLevel == 1)
            {
                rareLevelStar1.transform.localPosition = new Vector3(0, -160, 0);
            }
            else if (this.item.RareLevel == 2)
            {
                rareLevelStar1.transform.localPosition = new Vector3(-50, -160, 0);
                rareLevelStar2.transform.localPosition = new Vector3(50, -160, 0);
            }
            else if (this.item.RareLevel == 3)
            {
                rareLevelStar1.transform.localPosition = new Vector3(-100, -160, 0);
                rareLevelStar2.transform.localPosition = new Vector3(0, -160, 0);
                rareLevelStar3.transform.localPosition = new Vector3(100, -160, 0);
            }

            currentEffectValue.text = string.Format(LanguageManager.Instance.GetTextValue(item.ItemId + "_skill_desc"),
                (item.GetBalance() > 0) ? (item.EffectValue * 100).ToString() : "0");
            currentAllDamage.text = string.Format(LanguageManager.Instance.GetTextValue("Artifact_all_dmg"),
                (item.GetBalance() > 0) ? (item.AllDamageValue * 100).ToString() : "0");

            baseEffectValue.text = string.Format(LanguageManager.Instance.GetTextValue(item.ItemId + "_skill_desc"),
                (item.GetBalance() > 0) ? ((item.BaseEffectValue * 100) * (item.GetBalance() + 1)).ToString() : (item.BaseEffectValue * 100).ToString());
            baseAllDamage.text = string.Format(LanguageManager.Instance.GetTextValue("Artifact_all_dmg"),
                (item.BaseAllDamageValue * 100).ToString());

            artifactDescription.text = LanguageManager.Instance.GetTextValue(item.ItemId + "_desc");

            subContentBG.SetActive(item.GetBalance() != item.MaxLevel);

            RefreshOwnedGemsDisplay();
        }

        void RefreshOwnedRubyDisplay()
        {
            if (!isMoetifact)
            {
                if (item.GetBalance() > 0)
                {
                    moeSpiritLevelUp_currentRubyLabel.text = string.Format(LanguageManager.Instance.GetTextValue("current_ruby"),
                        UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Ruby.ToString() : ((current_ruby == -1) ? "0" : current_ruby.ToString()));

                    rubyMoeSpiritLevelUpBtn.GetComponent<Image>().sprite =
                        ((UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Ruby >= (item.GetBalance() + 1) * unlockRubyVal[rubyTier] : current_ruby >= (item.GetBalance() + 1) * unlockRubyVal[rubyTier]) && item.GetBalance() < item.MaxLevel)
                            ? rubyMoeSpiritLevelUpBtn.spriteState.highlightedSprite
                            : rubyMoeSpiritLevelUpBtn.spriteState.pressedSprite;
                }
                else
                {
                    unlock_currentRubyLabel.text = string.Format(LanguageManager.Instance.GetTextValue("current_ruby"),
                        UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Ruby.ToString() : ((current_ruby == -1) ? "0" : current_ruby.ToString()));

                    rubyUnlockBtn.GetComponent<Image>().sprite =
                        (UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Ruby >= unlockRubyVal[rubyTier] : current_ruby >= unlockRubyVal[rubyTier])
                            ? rubyUnlockBtn.spriteState.highlightedSprite
                            : rubyUnlockBtn.spriteState.pressedSprite;
                }
            }
        }

        void RefreshOwnedGemsDisplay()
        {
            if (isMoetifact)
            {
                salvage_currentGemsLabel.text = string.Format(LanguageManager.Instance.GetTextValue("current_gems"),
                    UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString());
                salvageBtn.GetComponent<Image>().sprite =
                    (UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem >= _artifactCore.SalvageCost(item) : AOMStoreInventory.GetDiamondBalance() >= _artifactCore.SalvageCost(item))
                        ? salvageBtn.spriteState.highlightedSprite
                        : salvageBtn.spriteState.pressedSprite;
            }
            else
            {
                if (item.GetBalance() > 0)
                {
                    moeSpiritLevelUp_currentGemsLabel.text = string.Format(LanguageManager.Instance.GetTextValue("current_gems"),
                        UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString());

                    gemMoeSpiritLevelUpBtn.GetComponent<Image>().sprite =
                        ((UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem >= unlockGemsVal[item.RareLevel - 1] : AOMStoreInventory.GetDiamondBalance() >= unlockGemsVal[item.RareLevel - 1]) && item.GetBalance() < item.MaxLevel)
                            ? gemMoeSpiritLevelUpBtn.spriteState.highlightedSprite
                            : gemMoeSpiritLevelUpBtn.spriteState.pressedSprite;
                }
                else
                {
                    unlock_currentGemsLabel.text = string.Format(LanguageManager.Instance.GetTextValue("current_gems"),
                        UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString());

                    gemUnlockBtn.GetComponent<Image>().sprite =
                        (UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem >= unlockGemsVal[item.RareLevel - 1] : AOMStoreInventory.GetDiamondBalance() >= unlockGemsVal[item.RareLevel - 1])
                            ? gemUnlockBtn.spriteState.highlightedSprite
                            : gemUnlockBtn.spriteState.pressedSprite;
                }
            }
        }

        public void SalvageMoetifact()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (UserManager.Instance.CurrentUserSummary != null)
            {
                if (UserManager.Instance.CurrentUserSummary.Gem >= _artifactCore.SalvageCost(item))
                {
                    SalvageConfirmViewController salvageConfirmViewController =
                        LoadOverlayViewController("SalvageConfirmMenuView", false,
                            Time.timeScale == 0) as SalvageConfirmViewController;
                    salvageConfirmViewController.SetMoetifactItem(item);
                }
                else
                {
                    LoadOverlayView("ShopMenu", false, Time.timeScale == 0);
                }
            }
        }

        public void UnlockMoeSpiritWithRuby()
        {
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                rubyUnlockBtn.interactable = false;
//                gemUnlockBtn.interactable = false;

//                if (UserManager.Instance.CurrentUserSummary.Ruby >= unlockRubyVal[rubyTier])
//                {
                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                    UserManager.Instance.TakeRuby(unlockRubyVal[rubyTier], () =>
                    {
                        item.Give(1);

                        GachaResultViewController gachaResultViewController =
                            LoadOverlayViewController("GachaResultMenuView", true,
                                Time.timeScale == 0) as GachaResultViewController;
                        gachaResultViewController.SetItemId(item, GachaType.MoeSpirit);

                        UnlockedMoeSpirit();

                        CloseView();

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                    Analytics.CustomEvent("useRuby", new Dictionary<string, object>
                        {
                            { "item", item.ItemId },
                            { "cost", unlockRubyVal[item.RareLevel - 1].ToString() }
                        });
#endif
                    });
//                }
//            }
        }

        public void UnlockMoeSpiritWithGems()
        {
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                rubyUnlockBtn.interactable = false;
//                gemUnlockBtn.interactable = false;

//                if (UserManager.Instance.CurrentUserSummary.Gem >= unlockGemsVal[item.RareLevel - 1])
//                {
                    //SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                    UserManager.Instance.TakeGems(unlockGemsVal[item.RareLevel - 1], () =>
                    {
                        item.Give(1);

                        GachaResultViewController gachaResultViewController =
                            LoadOverlayViewController("GachaResultMenuView", true,
                                Time.timeScale == 0) as GachaResultViewController;
                        gachaResultViewController.SetItemId(item, GachaType.MoeSpirit);

                        UnlockedMoeSpirit();

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                        Analytics.CustomEvent("useGems", new Dictionary<string, object>
                        {
                            { "type", "Unlock MoeSpirit" },
                        });
#endif
                        CloseView();
                    });
//                }
//                else
//                {
//                    rubyUnlockBtn.interactable = true;
//                    gemUnlockBtn.interactable = true;
//
//                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
//
//                    LoadOverlayView("ShopMenu", false, Time.timeScale == 0);
//                }
//            }
        }

        public void LevelUpMoeSpiritWithRuby()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            int rubyCost = (item.GetBalance() + 1) * unlockRubyVal[rubyTier];
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                if (UserManager.Instance.CurrentUserSummary.Ruby >= rubyCost)
//                {
                    UserManager.Instance.TakeRuby(rubyCost, () =>
                    {
                        item.Give(1);

                        UnlockedMoeSpirit();

                        RenderMoeSpiritDetail();

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                    Analytics.CustomEvent("useRuby", new Dictionary<string, object>
                        {
                            { "item", item.ItemId },
                            { "cost", rubyCost.ToString() }
                        });
#endif
                    });
//                }
//            }
        }

        public void LevelUpMoeSpiritWithGems()
        {
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                if (UserManager.Instance.CurrentUserSummary.Gem >= unlockGemsVal[item.RareLevel - 1])
//                {
                    //SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                    UserManager.Instance.TakeGems(unlockGemsVal[item.RareLevel - 1], () =>
                    {
                        item.Give(1);

                        UnlockedMoeSpirit();

                        RenderMoeSpiritDetail();

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                        Analytics.CustomEvent("useGems", new Dictionary<string, object>
                        {
                            { "type", "Level Up MoeSpirit" },
                        });
#endif
                    });
//                }
//                else
//                {
//                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
//
//                    LoadOverlayView("ShopMenu", false, Time.timeScale == 0);
//                }
//            }
        }

        void UnlockedMoeSpirit()
        {
            if (item == ArtifactAssets.MoeSpirit013)
            {
                _galleryCore.RecalAll();
            }

            EventManager.CallGalleryMoeSpiritUpdate();
        }

//        public void ShowNoInternetPopup(Action retryCallback)
//        {
//            MsgPopupViewController msgPopupViewController =
//                LoadOverlayViewController("MsgPopupOverlay", false, Time.timeScale == 0) as MsgPopupViewController;
//            msgPopupViewController.InputMsg(LanguageManager.Instance.GetTextValue("saveLoadWarning_title"),
//                LanguageManager.Instance.GetTextValue("pvp_errorMsg_NoInternet"), "btn_ok", () =>
//                {
//                    Time.timeScale = 1;
//                    CloseView();
//                });
//        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            RemoveListener();

            base.CloseView();
        }
    }
}