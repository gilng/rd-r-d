﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ChartboostSDK;
using SmartLocalization;
using UnityEngine.Analytics;
using System.Collections.Generic;
using Zenject;

namespace Ignite.AOM
{
    public class CPShopMenuViewController : UIOverlayViewController
    {
        private GalleryCore _galleryCore;

        [Inject]
        public void ConstructorSafeAttribute(GalleryCore galleryCore)
        {
            _galleryCore = galleryCore;
        }

        public Button watchADButton;

        public Text currentGemsLabel;
        public Text buyCPBtn_1, cpCost_1, buyCPBtn_2, cpCost_2, buyCPBtn_3, cpCost_3;

        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);

            AddListener();

            CheckRewardedVideo();

            RefreshOwnedGemsDisplay();

            buyCPBtn_1.text = string.Format(LanguageManager.Instance.GetTextValue("cpShop_btn_buyCP"), "10");
            cpCost_1.text = "200";
            buyCPBtn_2.text = string.Format(LanguageManager.Instance.GetTextValue("cpShop_btn_buyCP"), "20");
            cpCost_2.text = "300";
            buyCPBtn_3.text = string.Format(LanguageManager.Instance.GetTextValue("cpShop_btn_buyCP"), "30");
            cpCost_3.text = "400";
        }

        void AddListener()
        {
            EventManager.OnDiamondBalanceUpdated += RefreshOwnedGemsDisplay;
        }

        void RemoveListener()
        {
            EventManager.OnDiamondBalanceUpdated -= RefreshOwnedGemsDisplay;
        }

        // Update is called once per frame
        void Update()
        {
            CheckRewardedVideo();
        }

        void RefreshOwnedGemsDisplay()
        {
            currentGemsLabel.text = string.Format(LanguageManager.Instance.GetTextValue("current_gems"), UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString());
        }

        void CheckRewardedVideo()
        {
            if (Chartboost.hasRewardedVideo(CBLocation.MainMenu) && _galleryCore.TouchCredit < _galleryCore.MaxTouchCredit)
            {
                if (!watchADButton.interactable)
                {
                    watchADButton.interactable = true;
                    watchADButton.GetComponent<Image>().sprite = watchADButton.spriteState.highlightedSprite;
                }
            }
            else
            {
                if (watchADButton.interactable)
                {
                    watchADButton.interactable = false;
                    watchADButton.GetComponent<Image>().sprite = watchADButton.spriteState.pressedSprite;
                }
            }
        }

        public void ViewAd()
        {
            if (Chartboost.hasRewardedVideo(CBLocation.MainMenu))
            {
                Chartboost.showRewardedVideo(CBLocation.MainMenu);
            }
            else
            {
                // We don't have a cached video right now, but try to get one for next time
                Chartboost.cacheRewardedVideo(CBLocation.MainMenu);
            }

            CloseView();
        }

        public void GetCPPack(int packCost)
        {
//            if (UserManager.Instance.CurrentUserSummary != null)
//            {
//                if (UserManager.Instance.CurrentUserSummary.Gem >= packCost)
//                {
                    //SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                    int gainCP = 0;

                    switch (packCost)
                    {
                        case 200:
                            gainCP = 10;
                            break;
                        case 300:
                            gainCP = 20;
                            break;
                        case 400:
                            gainCP = 30;
                            break;
                    }

                    UserManager.Instance.TakeGems(packCost, () =>
                    {
                        _galleryCore.RechargingTouchCredit(gainCP, true);

                        // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                        Analytics.CustomEvent("useGems", new Dictionary<string, object>
                        {
                            { "type", "Buy CP Package " + packCost },
                        });
#endif
                        RemoveListener();

                        base.CloseView();
                    });
//                }
//                else
//                {
//                    SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
//
//                    LoadOverlayView("ShopMenu", false, false);
//                }
//            }
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            RemoveListener();

            base.CloseView();
        }
    }
}
