﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class RelicDropAnimation : MonoBehaviour
    {
        private float startX;
        private float startY;
        private float endX;
        private float endY;
        private float curveX;
        private float curveY;
        private float controlPointX;
        private float controlPointY;
        private float bezierTime = 0;
        private float speed = 2f;
        protected int dropAmount;

        public void StartDrop(float dropStartPointX, float dropStartPointY, float dropControlPointX,
            float dropControlPointY,
            float dropEndPointX, float dropEndPointY, int dropAmount)
        {
            startX = dropStartPointX;
            startY = dropStartPointY;
            endX = dropEndPointX;
            endY = dropEndPointY;
            controlPointX = dropControlPointX;
            controlPointY = dropControlPointY;
            this.dropAmount = dropAmount;
        }

        void Update()
        {
            curveX = (((1 - bezierTime) * (1 - bezierTime)) * startX) +
                     (2 * bezierTime * (1 - bezierTime) * controlPointX) + ((bezierTime * bezierTime) * endX);
            curveY = (((1 - bezierTime) * (1 - bezierTime)) * startY) +
                     (2 * bezierTime * (1 - bezierTime) * controlPointY) + ((bezierTime * bezierTime) * endY);
            transform.position = new Vector3(curveX, curveY, 0);
            bezierTime += Time.deltaTime;
            if (bezierTime >= 1)
            {
                AfterReturn();
            }
        }

        protected virtual void AfterReturn()
        {
            UserManager.Instance.GiveMoecrystals((int) dropAmount, null, false);
            Destroy(gameObject);
        }
    }
}