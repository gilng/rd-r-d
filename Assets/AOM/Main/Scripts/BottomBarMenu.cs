using UnityEngine;
using System.Collections;
namespace Ignite.AOM
{
	public class BottomBarMenu : MonoBehaviour {

		public GameObject newSkillLabel;
		public GameObject newHeroLabel;

		void Awake(){
			EventManager.OnGoldPriceUpdated += RefreshMenu;
			EventManager.OnGoldBalanceUpdated += RefreshMenu;
		}

		void OnDestroy()
		{
			EventManager.OnGoldPriceUpdated -= RefreshMenu;
			EventManager.OnGoldBalanceUpdated -= RefreshMenu;
		}

		void RefreshMenu() {

            bool newSkillUnlock = false;

            if (AOMAssets.AvatarLevelVG != null && AOMAssets.AvatarLevelVG.CanAffordOne())
            {
                // Tutorial Event 01 Trigger
                if (!TutorialManager.Instance.CheckTutorialCompletedById(1))
                    TutorialManager.Instance.TriggerTutorial(1);
            }

            if (AOMAssets.AvatarActiveSkillVGs != null && AOMAssets.AvatarActiveSkillVGs.Count > 0)
            {
                foreach (AvatarActiveSkillVG skill in AOMAssets.AvatarActiveSkillVGs)
                {
                    if (skill.CanBuy() && skill.GetBalance() < 1 && skill.CanAffordOne())
                    {
                        newSkillUnlock = true;

                        break;
                    }
                    else
                    {
                        newSkillUnlock = false;
                    }
                }
            }
            else
            {
                newSkillUnlock = false;
            }

            if (newSkillLabel.activeSelf != newSkillUnlock)
                newSkillLabel.SetActive(newSkillUnlock);

            bool newHeroUnlock = false;

            if (HeroAssets.Heroes != null && HeroAssets.Heroes.Count > 0)
            {
                foreach (CharacterLevelVG hero in HeroAssets.Heroes)
                {
                    if (hero.GetBalance() < 1 && hero.CanAffordOne())
                    {
                        newHeroUnlock = true;

                        // Tutorial Event 03 Trigger
                        if (!TutorialManager.Instance.CheckTutorialCompletedById(3))
                            TutorialManager.Instance.TriggerTutorial(3);

                        break;
                    }
                    else
                    {
                        newHeroUnlock = false;
                    }
                }
            }
            else
            {
                newHeroUnlock = false;
            }

            if (newHeroLabel.activeSelf != newHeroUnlock)
                newHeroLabel.SetActive(newHeroUnlock);
		}
	}
}