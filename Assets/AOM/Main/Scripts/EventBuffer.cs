﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Ignite.AOM
{
    abstract public class EventBuffer<T> : MonoBehaviour
    {

        protected IList<T> values = new List<T>();

        //protected float bufTimeOut = 0.5f;
        //protected float bufTime = 0.3f;

        protected float bufTimeOut = 0.25f;
        protected float bufTime = 0.15f;

        private float currentBufTime = 0f;
        private float firstBufTime = 0;

        // Update is called once per frame
        void Update()
        {
            if (values.Count > 0)
            {
                firstBufTime += Time.deltaTime;
                currentBufTime += Time.deltaTime;
                if (currentBufTime >= bufTime || firstBufTime >= bufTimeOut)
                {
                    SendToEventManager();
                    currentBufTime = 0;
                    firstBufTime = 0;
                }
            }

        }

        //Reset buf time to 0
        public void AddValue(T value)
        {
            values.Add(value);
            currentBufTime = 0;
        }

        public void RemoveAllValue()
        {
            values.Clear();
            currentBufTime = 0;
        }

        //Send event to event manager
        public abstract void SendToEventManager();
    }
}