﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Ignite.AOM
{
	public class TapDamageText_Animation : MonoBehaviour {
		
		public GameObject textOutline;

		Color c;
		float t1, t2;

		// Use this for initialization
		public void InitOutline(float endTime)
		{
			t1 = endTime;

			c = textOutline.GetComponent<Outline>().effectColor;

			textOutline.GetComponent<Outline>().effectColor = new Color(c.r, c.g, c.b, 1);
		}
		
		public void StartInvisible(float currentTime)
		{
			t2 = currentTime;
		}

		public void RunInvisible(float inputTime)
		{
			float aVal = (t1 - inputTime) / t2;

			textOutline.GetComponent<Outline>().effectColor = new Color(c.r, c.g, c.b, aVal);
		}


		public void EndInvisible()
		{
			textOutline.GetComponent<Outline>().effectColor = new Color(c.r, c.g, c.b, 0);

			gameObject.SetActive(false);
		}



	}
}
