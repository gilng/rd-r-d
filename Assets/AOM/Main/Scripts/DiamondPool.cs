﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Ignite.AOM
{
    public class DiamondPool : MonoBehaviour
    {
        private ObjectPool diamondPool;

        public List<DiamondDrop> droppedDiamonds;

        private int totalDroppedDiamondCache = 0;

        // Use this for initialization
        void Start()
        {
            diamondPool = GetComponent<ObjectPool>();
            droppedDiamonds = new List<DiamondDrop>();
        }

        public GameObject GetDiamond()
        {
            return diamondPool.GetPooledObject();
        }

        public void DiamondDropFromFairy(int numberOfDiamond, float boxPositionX, float boxPositionY)
        {
            for (int i = 0; i < numberOfDiamond; i++)
            {
                GameObject diamond = GetDiamond();
                diamond.SetActive(true);

                diamond.GetComponent<SpriteRenderer>().sortingLayerName = "Coins";

                if (diamond.GetComponent<DiamondDrop>() == null)
                {
                    DiamondDrop dd = diamond.AddComponent<DiamondDrop>();

                    if (diamond.GetComponent<CircleCollider2D>() == null)
                    {
                        CircleCollider2D collider = diamond.AddComponent<CircleCollider2D>();
                        collider.radius = 0.47f;
                    }

                    dd.StartDrop(boxPositionX,
                        boxPositionY + 0.5f,
                        boxPositionX - 1.5f,
                        boxPositionX + 1.5f,
                        boxPositionY + 3.8f,
                        boxPositionY + 3.8f,
                        GameManager.Instance.diamondImage.GetComponent<RectTransform>().transform.position.x,
                        GameManager.Instance.diamondImage.GetComponent<RectTransform>().transform.position.y,
                        1);

                    droppedDiamonds.Add(dd);
                    totalDroppedDiamondCache += 1;
                }
            }
        }

        public void RemoveFromDroppedList(DiamondDrop diamond)
        {
            if (droppedDiamonds.Contains(diamond))
            {
                droppedDiamonds.Remove(diamond);
            }

            if (droppedDiamonds.Count == 0 && totalDroppedDiamondCache > 0)
            {
                UserManager.Instance.GiveGems(totalDroppedDiamondCache, () => { totalDroppedDiamondCache = 0; }, false);
            }
        }
    }
}