﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public class AnimationManager : MonoBehaviour
    {
        public void EndAnime()
        {
            this.gameObject.SetActive(false);
        }
    }
}