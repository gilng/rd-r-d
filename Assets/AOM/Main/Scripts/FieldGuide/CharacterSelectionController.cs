﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;
using System.Collections.Generic;

namespace Ignite.AOM
{
    public class CharacterSelectionController : SelectionObjectController
    {
        private EnemyCore _enemyCore;

        [Inject]
        public void ConstructorSafeAttribute(EnemyCore enemyCore)
        {
            _enemyCore = enemyCore;
        }

        public Image thm;
        public Text thmName;
        public GameObject new_Symbol, tutorialNotify_Symbol, btnKeyLock;
        public Image keyLockIcon;

        int id;
        UIPageViewController containerViewController;

        void Start()
        {
            EventManager.OnGalleryCharacterButtonUpdate += UpdateButton;
        }

        void OnDestroy()
        {
            EventManager.OnGalleryCharacterButtonUpdate -= UpdateButton;
        }

        public void InitButton(int id, UIPageViewController containerViewController)
        {
            this.id = id;
            this.containerViewController = containerViewController;

            UpdateButton(id);
        }

        public void SelectCharacter()
        {
            if (id < FieldGuideManager.Instance.bossCharacterPool.bosses.Count)
            {
                if (GalleryCore.Girls[id].Status == MonsterGirlStatus.Killed)
                {
                    SoundManager.Instance.PlaySoundWithType(SoundType.SelectGirl_1);

                    FieldGuideManager.Instance.LoadBossCharacter(id, () =>
                        {
                            containerViewController.LoadPageView("CharacterView");
                        });

                    // Tutorial Event 08 Trigger
                    if (!TutorialManager.Instance.CheckTutorialCompletedById(8))
                    {
                        TutorialManager.Instance.UpdateTutorialProgress(8);
                    }
                }
            }
        }

        public void OpenKeyShop(string id)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (containerViewController.gameObject.GetComponent<FieldGuideViewController>().IsChangingField == false)
                FieldGuideManager.Instance.OpenShopOverlay(id);
        }

        void UpdateButton(int id)
        {
            EnemyPool enemyPool = FieldGuideManager.Instance.bossCharacterPool;

			//int realBossId = GalleryCore.GetRealBossId(id, 1);

            if (this.id == id)
            {
                int realBossId = enemyPool.bosses.Count > id ? int.Parse(enemyPool
                    .bosses[id]
                    .bossId.Replace("Boss_", "")) : id;

                bool characterAvailable = id < enemyPool.bosses.Count;
                bool characterUnlocked = characterAvailable ? (GalleryCore.Girls[id].Status == MonsterGirlStatus.Killed || (_enemyCore.Stage > enemyPool.bosses[id].breakLevel[0] && enemyPool.bosses[id].breakLevel[0] > 0)) : false;

                if (characterAvailable)
                {
					AssetBundleLoader.Instance.LoadImageAsset(AOMAssetBundlesConstant.MoetanBundleNameById(realBossId), !characterUnlocked ? "mo_" + (realBossId).ToString("000") + "_thm_loc" : "mo_" + (realBossId).ToString("000") + "_thm_unloc", thm);
                }
                else
                {
                    thm.sprite = Resources.Load<Sprite>("Monster/Texture/thm-coming");
                }

                if (characterAvailable)
                {
                    if (characterUnlocked)
                    {
                        //thmName.text = LanguageManager.Instance.GetTextValue(enemyPool.bosses[id] + "_name");

                        btnKeyLock.SetActive(false);

                        if (!TutorialManager.Instance.CheckTutorialCompletedById(8))
                        {
                            this.gameObject.AddComponent<TutorialReceiver>().TutorialEventSetup(8);

                            // Tutorial Event 08 Triggers
                            if (!this.gameObject.AddComponent<TutorialReceiver>().isTriggered)
                            {
                                TutorialManager.Instance.TriggerTutorial(8);
                            }
                        }
                    }
                    else
                    {
                        // Default turn off "KeyLock"
                        btnKeyLock.SetActive(false);

                        if (enemyPool.bosses[id].breakLevel[0] == 0)
                        {
                            thmName.text = LanguageManager.Instance.GetTextValue("gallery_unlockState_temp");
                        }
                        else if (enemyPool.bosses[id].breakLevel[0] == -1)
                        {
                            thmName.text = "";

                            //if (id == 10 || id == 11 || id == 12)
                            if (realBossId == 11 || realBossId == 12 || realBossId == 13)
                            {
                                keyLockIcon.sprite = Resources.Load<Sprite>("Sprites/Gallery/gallery_fieldGuideView/moetan-lock-btn-1");
                                btnKeyLock.SetActive(AOMAssets.AnotherWorldKeyGIVG.GetBalance() == 0);

                                if (btnKeyLock.activeInHierarchy)
                                    btnKeyLock.GetComponent<Button>().onClick.AddListener(() => OpenKeyShop(AOMAssets.AnotherWorldKeyGIVG.ItemId));
                            }

                            //if (id == 13 || id == 14 || id == 15 || id == 16 || id == 17)
                            if (realBossId == 14 || realBossId == 15 || realBossId == 16 || realBossId == 17 || realBossId == 18)
                            {
                                keyLockIcon.sprite = Resources.Load<Sprite>("Sprites/Gallery/gallery_fieldGuideView/moetan-lock-btn-2");
                                btnKeyLock.SetActive(AOMAssets.SchoolKeyGIVG.GetBalance() == 0);

                                if (btnKeyLock.activeInHierarchy)
                                    btnKeyLock.GetComponent<Button>().onClick.AddListener(() => OpenKeyShop(AOMAssets.SchoolKeyGIVG.ItemId));
                            }

                            //if (id == 18 || id == 19 || id == 20)
                            if (realBossId == 19 || realBossId == 20 || realBossId == 21)
                            {
                                keyLockIcon.sprite = Resources.Load<Sprite>("Sprites/Gallery/gallery_fieldGuideView/moetan-lock-btn-3");
                                btnKeyLock.SetActive(AOMAssets.ForestKeyGIVG.GetBalance() == 0);

                                if (btnKeyLock.activeInHierarchy)
                                    btnKeyLock.GetComponent<Button>().onClick.AddListener(() => OpenKeyShop(AOMAssets.ForestKeyGIVG.ItemId));
                            }

                            //if (id == 21 || id == 22 || id == 23)
                            if (realBossId == 22 || realBossId == 23 || realBossId == 24)
                            {
                                keyLockIcon.sprite = Resources.Load<Sprite>("Sprites/Gallery/gallery_fieldGuideView/moetan-lock-btn-4");
                                btnKeyLock.SetActive(AOMAssets.SummerKeyGIVG.GetBalance() == 0);

                                if (btnKeyLock.activeInHierarchy)
                                    btnKeyLock.GetComponent<Button>().onClick.AddListener(() => OpenKeyShop(AOMAssets.SummerKeyGIVG.ItemId));
                            }

                            //if (id == 24 || id == 25 || id == 26)
                            if (realBossId == 25 || realBossId == 26 || realBossId == 27)
                            {
                                keyLockIcon.sprite = Resources.Load<Sprite>("Sprites/Gallery/gallery_fieldGuideView/moetan-lock-btn-5");
                                btnKeyLock.SetActive(AOMAssets.HalloweenKeyGIVG.GetBalance() == 0);

                                if (btnKeyLock.activeInHierarchy)
                                    btnKeyLock.GetComponent<Button>().onClick.AddListener(() => OpenKeyShop(AOMAssets.HalloweenKeyGIVG.ItemId));
                            }

                            //if (id == 29 || id == 30 || id == 31)
                            if (realBossId == 28 || realBossId == 29 || realBossId == 30)
                            {
                                keyLockIcon.sprite = Resources.Load<Sprite>("Sprites/Gallery/gallery_fieldGuideView/moetan-lock-btn-6");
                                btnKeyLock.SetActive(AOMAssets.FukubukuroKeyGIVG.GetBalance() == 0);

                                if (btnKeyLock.activeInHierarchy)
                                    btnKeyLock.GetComponent<Button>().onClick.AddListener(() => OpenKeyShop(AOMAssets.FukubukuroKeyGIVG.ItemId));
                            }

                            // PvP
							for (int i=0; i<GalleryCore.PvPBossId.Length; i++)
							{
								if (realBossId == GalleryCore.PvPBossId[i])
								{
									GameItemVG[] _pvpKeys = new GameItemVG[] {AOMAssets.PvPKey002GIVG, AOMAssets.PvPKey001GIVG};

									keyLockIcon.sprite = Resources.Load<Sprite>("Sprites/Gallery/gallery_fieldGuideView/moetan-lock-btn-1");
									btnKeyLock.SetActive(_pvpKeys[i].GetBalance() == 0);
								}
							}
                        }
                        else
                        {
                            thmName.text = string.Format(LanguageManager.Instance.GetTextValue("gallery_unlockState"), enemyPool.bosses[id].breakLevel[0]);
                        }
                    }

                    new_Symbol.SetActive(GalleryCore.Girls[id].CheckNewGallery());
                }
                else
                {
                    thmName.text = "";
                    new_Symbol.SetActive(false);
                    btnKeyLock.SetActive(false);
                }

                if (this.gameObject.GetComponent<TutorialReceiver>() == null)
                {
                    tutorialNotify_Symbol.SetActive(false);
                }
            }
        }
    }
}