﻿using UnityEngine;
using SmartLocalization;
using System;
using System.Collections;
using ChartboostSDK;
using Zenject;

namespace Ignite.AOM
{
    public class FieldGuideManager : MonoBehaviour
    {
        private UIViewLoader _uiViewLoader;

        private EnemyCore _enemyCore;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader, EnemyCore enemyCore)
        {
            _uiViewLoader = uiViewLoader;
            _enemyCore = enemyCore;
        }

        private static FieldGuideManager _instance;

        public static FieldGuideManager Instance
        {
            get { return _instance; }
        }

        public int currentSelectedBossIndex;
        public Enemy bossCharacter;

        public EnemyPool bossCharacterPool;

        public GameObject loadingOverlay_Group;
        public GameObject bgm;

        public int[,] diamondVal;
        public int[,] relicVal;

        public int currPage = 0;

        // Use this for initialization
        void Awake()
        {
            _instance = this;

            SoundManager.Instance.bgm = this.bgm.GetComponent<AudioSource>();
            SoundManager.Instance.FixBGM();

            for (int i = 0; i < GalleryCore.Girls.Count; i++)
            {
                int[] breakLevel = GalleryCore.Girls[i].BreakLevel;

                bossCharacterPool.bosses[i].breakLevel[0] = breakLevel[0];
                bossCharacterPool.bosses[i].breakLevel[1] = breakLevel[1];
                bossCharacterPool.bosses[i].breakLevel[2] = breakLevel[2];
            }

            diamondVal = new int[,] {{10, 50, 75, 110}, {15, 75, 120, 180}};
            relicVal = new int[,] {{1, 5, 25, 50}, {5, 20, 40, 100}};

            ShowLoadingOverlay(true);

            Chartboost.cacheRewardedVideo(CBLocation.MainMenu);
        }

        void Start()
        {
            CheckLanguageVersion();

            StartCoroutine(LoadFieldGuideView());
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Home))
            {
                if (bossCharacter == null)
                {
                    LoadBattleScene();
                }
            }
        }

        void OnDestroy()
        {
            SoundManager.Instance.bgm = null;

            Resources.UnloadUnusedAssets();

            System.GC.Collect(0);
        }

        IEnumerator LoadFieldGuideView()
        {
            GameObject fieldGuideViewPage = _uiViewLoader.LoadPageView("FieldGuideView", null);

            yield return null;
        }

        public void CheckLanguageVersion()
        {
            string lang = PlayerPrefs.GetString(LanguageMenuViewController.Language_PP_KEY, "");
            if (lang != "")
            {
                LanguageManager.Instance.ChangeLanguage(lang);
                EventManager.ChangeLanguage();
            }
            else
            {
                if (Application.systemLanguage == SystemLanguage.ChineseTraditional ||
                    Application.systemLanguage == SystemLanguage.ChineseSimplified)
                    LanguageManager.Instance.ChangeLanguage("zh-cht");
                else if (Application.systemLanguage == SystemLanguage.Japanese)
                    LanguageManager.Instance.ChangeLanguage("ja");
                else if (Application.systemLanguage == SystemLanguage.Korean)
                    LanguageManager.Instance.ChangeLanguage("ko");
                else
                    LanguageManager.Instance.ChangeLanguage("en");

                EventManager.ChangeLanguage();
            }
        }

        public void LoadBossCharacter(int index, Action successCallback)
        {
            currentSelectedBossIndex = index;

            bossCharacterPool.LoadMoetanById(bossCharacterPool.bosses[index].bossId, true, () =>
            {
                bossCharacter = (Enemy)bossCharacterPool.GetBossCharacterByIndex(currentSelectedBossIndex).GetComponent(typeof(Enemy));

                bossCharacter.SpawnAnimation();

                successCallback();
            });
        }

        public void UnloadSelectedBossCharacter()
        {
            bossCharacter = null;
            bossCharacterPool.RemoveBossByIndex(currentSelectedBossIndex);
        }

        public void LoadBattleScene()
        {
            ShowLoadingOverlay(true, true);

            Application.LoadLevel("MainScene");
        }

        public void ShowLoadingOverlay(bool active, bool isChangeTips = false)
        {
            if (isChangeTips)
                loadingOverlay_Group.GetComponent<TipsManager>().ChangeTips();

            loadingOverlay_Group.SetActive(active);
        }

        public void OpenShopOverlay(string id)
        {
            GameObject buyItemOverlay = _uiViewLoader.LoadOverlayView("BuyItemOverlay", null, false, false);
            buyItemOverlay.GetComponent<BuyItemOverlayController>().OpenKeyShop(id);
        }

        public void OpenCPShopOverlay()
        {
            GameObject cpShopMenuView = _uiViewLoader.LoadOverlayView("CPShopMenuOverlay", null, false, false);
        }
    }
}