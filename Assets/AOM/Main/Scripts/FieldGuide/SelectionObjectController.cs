﻿using UnityEngine;
using Zenject;

namespace Ignite.AOM
{
    public class SelectionObjectController : MonoBehaviour
    {
        public class Factory : Factory<string, SelectionObjectController>
        {
        }
    }

    public class SelectionObjectControllerFactory : IFactory<string, SelectionObjectController>
    {
        private readonly DiContainer _container;

        public SelectionObjectControllerFactory(DiContainer container)
        {
            _container = container;
        }

        public SelectionObjectController Create(string prefabPath)
        {
            GameObject selectionObject = _container.InstantiatePrefabResource("UI/Prefab/SelectionObject/" + prefabPath);
            SelectionObjectController selectionObjectController = selectionObject.GetComponent<SelectionObjectController>();

            return selectionObjectController;
        }
    }
}
