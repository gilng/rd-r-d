﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace Ignite.AOM
{
    public class FieldSelectionController : SelectionObjectController, IPointerUpHandler, IPointerDownHandler
    {
        private SelectionObjectController.Factory _selectionObjectControllerFactory;

        [Inject]
        public void ConstructorSafeAttribute(SelectionObjectController.Factory selectionObjectControllerFactory)
        {
            _selectionObjectControllerFactory = selectionObjectControllerFactory;
        }

        public UIPageViewController containerViewController;

        public int fieldIndex;

        public GameObject characterContainer;

        public List<GameObject> characterSelectionList = new List<GameObject>();

        public GridLayoutGroup characterLayoutGrid;

        public GameObject characterButtonPrefab;

        public void Init(UIPageViewController containerViewController, int currFieldIndex)
        {
            this.containerViewController = containerViewController;

            fieldIndex = currFieldIndex;
            LoadCharacterList();
        }

        void LoadCharacterList()
        {
            int beginCharacterIndex = fieldIndex * 9;
            int bossCount = FieldGuideManager.Instance.bossCharacterPool.bosses.Count;
            int maxCharacter = ((bossCount / 9) + ((bossCount % 9 == 0) ? 0 : 1)) * 9;
            int lastCharacterIndex = maxCharacter >= beginCharacterIndex + 9 ? beginCharacterIndex + 9 : maxCharacter;

            for (int i = beginCharacterIndex; i < lastCharacterIndex; i++)
            {
                CharacterSelectionController characterSelectionController =
                    _selectionObjectControllerFactory.Create("CharacterSelectionObj") as CharacterSelectionController;
                characterSelectionController.InitButton(i, containerViewController);
                characterSelectionController.gameObject.name = "Boss_" + (i + 1).ToString("000");
                characterSelectionController.gameObject.transform.SetParent(characterLayoutGrid.transform, false);

                characterSelectionList.Add(characterSelectionController.gameObject);
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnPointUp();
        }

        protected virtual void OnPointUp()
        {
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            OnPointDown();
        }

        protected virtual void OnPointDown()
        {
        }

        public void ActiveAllButton(bool active)
        {
            foreach (GameObject characterSelectButton in characterSelectionList)
            {
                characterSelectButton.GetComponent<Button>().interactable = active;
            }
        }
    }
}