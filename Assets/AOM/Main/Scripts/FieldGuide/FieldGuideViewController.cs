﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using  UnityEngine.Analytics;
using SmartLocalization;
using ChartboostSDK;
using Zenject;

namespace Ignite.AOM
{
    public class FieldGuideViewController : UIPageViewController
    {
        private SelectionObjectController.Factory _selectionObjectControllerFactory;

        private EnemyCore _enemyCore;

        private GalleryCore _galleryCore;

        [Inject]
        public void ConstructorSafeAttribute(SelectionObjectController.Factory selectionObjectControllerFactory,
            EnemyCore enemyCore, GalleryCore galleryCore)
        {
            _selectionObjectControllerFactory = selectionObjectControllerFactory;
            _enemyCore = enemyCore;
            _galleryCore = galleryCore;
        }

        public Text title;

        public GridLayoutGroup fieldGuideLayoutGrid;
        public RectTransform fieldGuideLayoutRect;
        public RectTransform fieldGuideScrollViewRect;

        List<FieldSelectionController> allFieldSelectionObj = new List<FieldSelectionController>();
        int selectedIndex = 0;

        Vector2 clickV2; // for PC / Mac
        Vector2 touchV2; // for Mobile

        float touchMin;

        bool isChangingField = false;

        public bool IsChangingField
        {
            get { return isChangingField; }
        }

        bool selectedCurrentField = false;

        public GameObject rubQuota_Group;
        public Text timeText, timeMaxText, countText;

        List<GameObject> paginationList = new List<GameObject>();
        public GameObject paginationObj;

        // Use this for initialization
        protected override void Start()
        {
            base.Start();

            AddListener();

            title.text = LanguageManager.Instance.GetTextValue("gallery_title");

            StartCoroutine(LoadFieldSelectionPage());

            countText.text = (_galleryCore.TouchCredit + "/" + _galleryCore.MaxTouchCredit).ToString();

            if (_galleryCore.TouchCredit < _galleryCore.MaxTouchCredit)
            {
                if (!timeText.gameObject.activeSelf)
                    timeText.gameObject.SetActive(true);

                if (timeMaxText.gameObject.activeSelf)
                    timeMaxText.gameObject.SetActive(false);

                timeText.text = _galleryCore.RecalTouchCredit();
            }
            else
            {
                if (timeText.gameObject.activeSelf)
                    timeText.gameObject.SetActive(false);

                if (!timeMaxText.gameObject.activeSelf)
                    timeMaxText.gameObject.SetActive(true);
            }
        }

        // Update is called once per frame
        protected override void Update()
        {
            base.Update();

            if (!isChangingField && selectedCurrentField)
            {
                switch (Application.platform)
                {
                    case RuntimePlatform.OSXEditor:
                    case RuntimePlatform.OSXPlayer:
                    case RuntimePlatform.WindowsEditor:
                    case RuntimePlatform.WindowsPlayer:
                        touchMin = 100.0f;
                        MouseControl();
                        break;
                    case RuntimePlatform.IPhonePlayer:
                    case RuntimePlatform.Android:
                        touchMin = 100.0f;
                        TouchControl();
                        break;
                }
            }
            else
            {
                if (isChangingField)
                {
                    SnapToSelectedField();
                }
                else
                {
                    if (!selectedCurrentField)
                    {
                        SelectCurrentField();
                    }
                }
            }

            if (_galleryCore.OwnedGirls())
            {
                if (!rubQuota_Group.activeSelf)
                    rubQuota_Group.SetActive(true);
            }
            else
            {
                if (rubQuota_Group.activeSelf)
                    rubQuota_Group.SetActive(false);
            }

            // Update touch credit count & time
            if (rubQuota_Group.activeSelf)
            {
                if (_galleryCore.TouchCredit < _galleryCore.MaxTouchCredit)
                {
                    if (!timeText.gameObject.activeSelf)
                        timeText.gameObject.SetActive(true);

                    if (timeMaxText.gameObject.activeSelf)
                        timeMaxText.gameObject.SetActive(false);

                    timeText.text = _galleryCore.RecalTouchCredit();
                }
                else
                {
                    if (timeText.gameObject.activeSelf)
                        timeText.gameObject.SetActive(false);

                    if (!timeMaxText.gameObject.activeSelf)
                        timeMaxText.gameObject.SetActive(true);
                }

                if (countText.text != (_galleryCore.TouchCredit + "/" + _galleryCore.MaxTouchCredit).ToString())
                    countText.text = (_galleryCore.TouchCredit + "/" + _galleryCore.MaxTouchCredit).ToString();
            }
        }

        #region Control

        void MouseControl()
        {
            if (Input.GetMouseButtonDown(0))
            {
                clickV2 = Input.mousePosition;
            }

            // Check null
            if (clickV2 == new Vector2(0, 0))
            {
                clickV2 = Input.mousePosition;
            }

            if (Input.GetMouseButton(0))
            {
                bool scrolled = false;
                float disX = Mathf.Abs(Input.mousePosition.x - clickV2.x);

                if (scrolled == false)
                {
                    if (disX > touchMin)
                    {
                        if (Input.mousePosition.x > clickV2.x)
                        {
                            ChangeField(false);
                            scrolled = true;
                        }

                        if (Input.mousePosition.x < clickV2.x)
                        {
                            ChangeField(true);
                            scrolled = true;
                        }

                        if (scrolled == false)
                        {
                            clickV2 = Input.mousePosition;
                        }
                    }
                }

                // Check "pass = true" must be next to checking direction & distance
                if (scrolled)
                {
                    clickV2 = Input.mousePosition;
                }
            }
        }

        void TouchControl()
        {
            Touch touch;

            if (Input.touchCount == 1)
            {
                touch = Input.GetTouch(0);

                // Check null
                if (touchV2 == new Vector2(-1, -1))
                {
                    touchV2 = touch.position;
                }

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        touchV2 = touch.position;
                        break;

                    case TouchPhase.Moved:
                        bool scrolled = false;
                        float disX = Mathf.Abs(touch.position.x - touchV2.x);

                        if (scrolled == false)
                        {
                            if (disX > touchMin)
                            {
                                if (touch.position.x > touchV2.x)
                                {
                                    ChangeField(false);
                                    scrolled = true;
                                }

                                if (touch.position.x < touchV2.x)
                                {
                                    ChangeField(true);
                                    scrolled = true;
                                }

                                if (scrolled == false)
                                {
                                    touchV2 = touch.position;
                                }
                            }
                        }

                        // Check "pass = true" must be next to checking direction & distance
                        if (scrolled)
                        {
                            touchV2 = touch.position;
                        }
                        break;
                }
            }
        }

        #endregion

        IEnumerator LoadFieldSelectionPage()
        {
            int bossCount = FieldGuideManager.Instance.bossCharacterPool.bosses.Count;
            int fieldSelectionCount = (bossCount / 9) + ((bossCount % 9 > 0) ? 1 : 0);

            for (int i = 0; i < fieldSelectionCount; i++)
            {
                FieldSelectionController fieldSelectionController =
                    _selectionObjectControllerFactory.Create("FieldGuideSelectionObj") as FieldSelectionController;
                fieldSelectionController.gameObject.name = "field_page_" + (i + 1);
                fieldSelectionController.gameObject.transform.SetParent(fieldGuideLayoutGrid.transform, false);
                fieldSelectionController.gameObject.transform.localPosition = Vector3.zero;
                fieldSelectionController.Init(this, i);

                allFieldSelectionObj.Add(fieldSelectionController);

                // Pagination - Part 1 
                GameObject paginationListItem =
                    (GameObject) Instantiate(Resources.Load("UI/Prefab/SelectionObject/PaginationObj"));
                paginationListItem.name = "page_" + (i + 1);
                paginationListItem.transform.SetParent(paginationObj.transform, false);

                paginationList.Add(paginationListItem);
                // Pagination - Part 1 End
            }

            // Pagination - Part 2 (Point)
            int half = paginationList.Count / -2;

            for (int i = 0; i < paginationList.Count; i++)
            {
                paginationList[i].transform.localPosition =
                    new Vector3(half * 80 + ((paginationList.Count % 2 == 0) ? 40 : 0), 90, 0);
                half++;
            }
            // Pagination - Part 2 End

            ResizeFieldLayoutRect(); // [ NOTE ] Here! Will set "allFieldSelectionObj" position!!

            yield return new WaitForSeconds(0.5f);

            selectedIndex = FieldGuideManager.Instance.currPage;
            SelectCurrentField(true);

            FieldGuideManager.Instance.ShowLoadingOverlay(false);
        }

        void ResizeFieldLayoutRect()
        {
            float fieldGuideLayoutRectSizeX = (fieldGuideLayoutGrid.transform.childCount *
                                               fieldGuideLayoutGrid.cellSize.x) +
                                              ((fieldGuideLayoutGrid.transform.childCount - 1) *
                                               fieldGuideLayoutGrid.spacing.x);
            float fieldGuideLayoutRectSizeY = fieldGuideLayoutGrid.cellSize.y;

            if (fieldGuideScrollViewRect.sizeDelta.x > fieldGuideLayoutRectSizeX)
                fieldGuideLayoutRectSizeX = fieldGuideScrollViewRect.sizeDelta.x;

            fieldGuideLayoutRect.sizeDelta = new Vector2(fieldGuideLayoutRectSizeX, fieldGuideLayoutRectSizeY);

            fieldGuideLayoutRect.transform.localPosition =
                new Vector3((fieldGuideLayoutRect.sizeDelta.x / fieldGuideLayoutGrid.transform.childCount),
                    fieldGuideLayoutRect.transform.localPosition.y, fieldGuideLayoutRect.transform.localPosition.z);
        }

        public void ChangeField(bool forward)
        {
            isChangingField = true;

            allFieldSelectionObj[selectedIndex].transform.localPosition =
                new Vector3(allFieldSelectionObj[selectedIndex].transform.localPosition.x,
                    allFieldSelectionObj[selectedIndex].transform.localPosition.y, 0f);

            for (int i = 0; i < allFieldSelectionObj.Count; i++)
            {
                allFieldSelectionObj[i].ActiveAllButton(false);
            }

            if (forward)
            {
                if (selectedIndex < allFieldSelectionObj.Count - 1)
                    selectedIndex += 1;
            }
            else
            {
                if (selectedIndex > 0)
                    selectedIndex -= 1;
            }

            FieldGuideManager.Instance.currPage = selectedIndex;
        }

        void SnapToSelectedField()
        {
            if (!allFieldSelectionObj[selectedIndex].characterContainer.activeInHierarchy)
                allFieldSelectionObj[selectedIndex].characterContainer.SetActive(true);

            if (fieldGuideLayoutRect.transform.localPosition.x <
                (-1 * allFieldSelectionObj[selectedIndex].transform.localPosition.x) - 100.0f)
            {
                if (!allFieldSelectionObj[selectedIndex + 1].characterContainer.activeInHierarchy)
                    allFieldSelectionObj[selectedIndex + 1].characterContainer.SetActive(true);

                fieldGuideLayoutRect.transform.Translate(2000 * Time.deltaTime, 0.0f, 0.0f);
            }
            else if (fieldGuideLayoutRect.transform.localPosition.x >
                     (-1 * allFieldSelectionObj[selectedIndex].transform.localPosition.x) + 100.0f)
            {
                if (!allFieldSelectionObj[selectedIndex - 1].characterContainer.activeInHierarchy)
                    allFieldSelectionObj[selectedIndex - 1].characterContainer.SetActive(true);

                fieldGuideLayoutRect.transform.Translate(-2000 * Time.deltaTime, 0.0f, 0.0f);
            }
            else
            {
                if (isChangingField)
                {
                    isChangingField = false;

                    SelectCurrentField();
                }
            }
        }

        void SelectCurrentField(bool isInit = false)
        {
//			if (isInit) {
//				int dir = (selectedIndex - ((paginationList.Count / 2) + (paginationList.Count % 2)) != 0) ? -1 : 0;
//				fieldGuideLayoutRect.transform.localPosition = new Vector3(dir * allFieldSelectionObj[selectedIndex].transform.localPosition.x, 0, 0);
//			} else {
//				fieldGuideLayoutRect.transform.localPosition = new Vector3(-1 * allFieldSelectionObj[selectedIndex].transform.localPosition.x, 0, 0);
//			}

            fieldGuideLayoutRect.transform.localPosition =
                new Vector3(-1 * allFieldSelectionObj[selectedIndex].transform.localPosition.x, 0, 0);

            allFieldSelectionObj[selectedIndex].transform.localPosition =
                new Vector3(allFieldSelectionObj[selectedIndex].transform.localPosition.x,
                    allFieldSelectionObj[selectedIndex].transform.localPosition.y, 0f);

            for (int i = 0; i < paginationList.Count; i++)
            {
                if (paginationList[i].transform.GetChild(0).gameObject.activeSelf)
                    paginationList[i].transform.GetChild(0).gameObject.SetActive(false);
            }

            paginationList[selectedIndex].transform.GetChild(0).gameObject.SetActive(true);

            for (int i = 0; i < allFieldSelectionObj.Count; i++)
            {
                allFieldSelectionObj[i].characterContainer.SetActive(i == selectedIndex);
                allFieldSelectionObj[i].ActiveAllButton(i == selectedIndex);
            }

            selectedCurrentField = true;
        }

        public void BackToBattleScene()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            RemoveListener();

            FieldGuideManager.Instance.LoadBattleScene();
        }

        // Load Page 
        public override void LoadPageView(string pageName)
        {
            RemoveListener();
            FieldGuideManager.Instance.ShowLoadingOverlay(true, true);
            base.LoadPageView(pageName);
        }

        void AddListener()
        {
            EventManager.OnGalleryUpdate += UpdateGalleryCell;

            Chartboost.didFailToLoadRewardedVideo += didFailToLoadRewardedVideo;
            Chartboost.didDismissRewardedVideo += didDismissRewardedVideo;
            Chartboost.didCloseRewardedVideo += didCloseRewardedVideo;
            Chartboost.didClickRewardedVideo += didClickRewardedVideo;
            Chartboost.didCacheRewardedVideo += didCacheRewardedVideo;
            Chartboost.didCompleteRewardedVideo += didCompleteRewardedVideo;
            Chartboost.didDisplayRewardedVideo += didDisplayRewardedVideo;
        }

        void RemoveListener()
        {
            EventManager.OnGalleryUpdate -= UpdateGalleryCell;

            Chartboost.didFailToLoadRewardedVideo -= didFailToLoadRewardedVideo;
            Chartboost.didDismissRewardedVideo -= didDismissRewardedVideo;
            Chartboost.didCloseRewardedVideo -= didCloseRewardedVideo;
            Chartboost.didClickRewardedVideo -= didClickRewardedVideo;
            Chartboost.didCacheRewardedVideo -= didCacheRewardedVideo;
            Chartboost.didCompleteRewardedVideo -= didCompleteRewardedVideo;
            Chartboost.didDisplayRewardedVideo -= didDisplayRewardedVideo;
        }

        void UpdateGalleryCell(string id)
        {
            List<GameObject> bossField = new List<GameObject>();

            for (int i = 0; i < allFieldSelectionObj.Count; i++)
            {
                for (int j = 0; j < allFieldSelectionObj[i].characterSelectionList.Count; j++)
                {
                    bossField.Add(allFieldSelectionObj[i].characterSelectionList[j]);
                }
            }

            EnemyPool enemyPool = FieldGuideManager.Instance.bossCharacterPool;

            for (int i = 0; i <= bossField.Count; i++)
            {
                bool isTargetBoss = false;

                if ((i == 10 || i == 11 || i == 12) && string.Equals(AOMAssets.AnotherWorldKeyGIVG.ItemId, id))
                    isTargetBoss = true;

                if ((i == 13 || i == 14 || i == 15 || i == 16 || i == 17) &&
                    string.Equals(AOMAssets.SchoolKeyGIVG.ItemId, id))
                    isTargetBoss = true;

                if ((i == 18 || i == 19 || i == 20) && string.Equals(AOMAssets.ForestKeyGIVG.ItemId, id))
                    isTargetBoss = true;

                if ((i == 21 || i == 22 || i == 23) && string.Equals(AOMAssets.SummerKeyGIVG.ItemId, id))
                    isTargetBoss = true;

                if ((i == 24 || i == 25 || i == 26) && string.Equals(AOMAssets.HalloweenKeyGIVG.ItemId, id))
                    isTargetBoss = true;

                if ((i == 29 || i == 30 || i == 31) && string.Equals(AOMAssets.FukubukuroKeyGIVG.ItemId, id))
                    isTargetBoss = true;

                if (isTargetBoss)
                {
                    int bossId = int.Parse(enemyPool.bosses[i].bossId.Replace("Boss_", ""));
                    int[] breakLevelList = enemyPool.AddBreakLevels(bossId);

                    enemyPool.bosses[i].breakLevel = breakLevelList;

                    if (bossField[i] != null)
                    {
                        // Check stage
                        if (_enemyCore.Stage > enemyPool.bosses[i].breakLevel[0])
                        {
//                            Gallery bossGallery = _galleryCore.GetGirlWithID((i + 1).ToString("000"));

                            Gallery bossGallery = _galleryCore.GetGirlWithID(enemyPool.bosses[i].bossId.Replace("Boss_", ""));

                            if (bossGallery != null)
                            {
                                if (bossGallery.Status != MonsterGirlStatus.Killed)
                                {
                                    bossGallery.Status = MonsterGirlStatus.Killed;
                                    bossGallery.ReachedBreakLevel++;
                                }
                            }
                        }
                    }

                    EventManager.CallGalleryCharacterButtonUpdate(i);
                }
            }
        }

        public void GetCP()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            FieldGuideManager.Instance.OpenCPShopOverlay();
        }

        void VideoAdReward()
        {
            // Add rub
            _galleryCore.RechargingTouchCredit(1);
        }

        public void ShowRubAdMenuOverlay()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (Chartboost.hasRewardedVideo(CBLocation.MainMenu))
            {
                Chartboost.showRewardedVideo(CBLocation.MainMenu);
            }
            else
            {
                // We don't have a cached video right now, but try to get one for next time
                Chartboost.cacheRewardedVideo(CBLocation.MainMenu);
            }

            // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
			Analytics.CustomEvent("viewAdResumeRubQuota", new Dictionary<string, object>
			{
			{ "numberOfView", "View" }
			});
#endif
        }

        void didFailToLoadRewardedVideo(CBLocation location, CBImpressionError error)
        {
            Debug.Log(string.Format("didFailToLoadRewardedVideo: {0} at location {1}", error, location));
        }

        void didDismissRewardedVideo(CBLocation location)
        {
            Debug.Log("didDismissRewardedVideo: " + location);
        }

        void didCloseRewardedVideo(CBLocation location)
        {
            Debug.Log("didCloseRewardedVideo: " + location);
        }

        void didClickRewardedVideo(CBLocation location)
        {
            Debug.Log("didClickRewardedVideo: " + location);
        }

        void didCacheRewardedVideo(CBLocation location)
        {
            Debug.Log("didCacheRewardedVideo: " + location);
        }

        void didCompleteRewardedVideo(CBLocation location, int reward)
        {
            Debug.Log(string.Format("didCompleteRewardedVideo: reward {0} at location {1}", reward, location));
            VideoAdReward();
        }

        void didDisplayRewardedVideo(CBLocation location)
        {
            Debug.Log("didDisplayRewardedVideo: " + location);
        }
    }
}