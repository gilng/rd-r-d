﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;
using ChartboostSDK;
using Zenject;

namespace Ignite.AOM
{
    public class BossCharacterViewController : UIPageViewController
    {
        private GalleryCore _galleryCore;
        private UIViewLoader _uiViewLoader;
        private IUserService _userService;

        [Inject]
        public void ConstructorSafeAttribute(GalleryCore galleryCore, UIViewLoader uiViewLoader, IUserService userService)
        {
            _galleryCore = galleryCore;
            _uiViewLoader = uiViewLoader;
            _userService = userService;
        }

        public Text bossCharacter_Name_Text;
        public Text pointLevel_Text;
        public Text point_Text;
        public Text reward_Text_Diamond;
        public Text reward_Text_Relic;
        public Animator pointLevelUp_Animator;
        public GameObject pointLevelUp_Effect;
        public Animator pointIncrease_Animator;
        public GameObject characterPointSliderPanel;
        public Slider characterPointSlider;
        public GameObject characterExpSliderPanel;
        public Slider characterCurrentExpSlider;
        public GameObject touchCredit_Group;
        public Text touchCredit_Text;
        public Text touchCreditRecoverTimer_Text;
        public GameObject maxCredit;
        public Button[] levelBtn;
        public GameObject handCursor;
        public ObjectPool heartObjectPool;
        public GameObject heartEffectLayout_Group;

        public Image rewardImageDiamond;
        public Image rewardImageRelic;
        public GameObject infoRewardTable;

        Vector2 clickV2; // for PC / Mac
        Vector2 touchV2; // for Mobile

        float touchMin;
        int currPointLevel;
        int currSelectedIndex = 1;
        bool interactable = true;
        Gallery currCharacterGallery;
        GameObject background;
        float effectTime = 0.1f;
        float effectTimer = 0;

        int touchExp;

        GameObject watchAdGainRubTips;
        bool openedTips = false;

        public GameObject moeSpirit_Container;
        public Image moeSpirit_Sprite;

        // Develop Use
        public GameObject DEVELOP_MODE;

        public Text DEVELOP_MODE_TEXT;
        bool quick_Rub = false;


        // Use this for initialization
        protected override void Start()
        {
            base.Start();

            // Only For Develop Mode !!!
            DEVELOP_MODE.SetActive(ExtraParseInitialization.CurrentDevelopMode == DevelopMode.Dev);

            AddListener();

            parentPageViewName = "FieldGuideView";

            heartObjectPool = this.transform.GetComponentInChildren<ObjectPool>();

            background = (GameObject) Instantiate(Resources.Load("Background/CharacterView_Background"));

            touchExp = ArtifactAssets.Artifact032.GetBalance() > 0
                ? (int) (1 + ArtifactAssets.Artifact032.EffectValue * 100)
                : 1;

            StartCoroutine(LoadSelectedBossCharacter());
        }

        void AddListener()
        {
            Chartboost.didFailToLoadRewardedVideo += didFailToLoadRewardedVideo;
            Chartboost.didDismissRewardedVideo += didDismissRewardedVideo;
            Chartboost.didCloseRewardedVideo += didCloseRewardedVideo;
            Chartboost.didClickRewardedVideo += didClickRewardedVideo;
            Chartboost.didCacheRewardedVideo += didCacheRewardedVideo;
            Chartboost.didCompleteRewardedVideo += didCompleteRewardedVideo;
            Chartboost.didDisplayRewardedVideo += didDisplayRewardedVideo;

            EventManager.OnGalleryMoeSpiritUpdate += UpdateMoeSpiritStatus;
        }

        void RemoveListener()
        {
            Chartboost.didFailToLoadRewardedVideo -= didFailToLoadRewardedVideo;
            Chartboost.didDismissRewardedVideo -= didDismissRewardedVideo;
            Chartboost.didCloseRewardedVideo -= didCloseRewardedVideo;
            Chartboost.didClickRewardedVideo -= didClickRewardedVideo;
            Chartboost.didCacheRewardedVideo -= didCacheRewardedVideo;
            Chartboost.didCompleteRewardedVideo -= didCompleteRewardedVideo;
            Chartboost.didDisplayRewardedVideo -= didDisplayRewardedVideo;

            EventManager.OnGalleryMoeSpiritUpdate -= UpdateMoeSpiritStatus;
        }

        public void DevelopFeature_QuickRub()
        {
            if (ExtraParseInitialization.CurrentDevelopMode == DevelopMode.Dev)
            {
                if (quick_Rub)
                {
                    quick_Rub = false;
                    DEVELOP_MODE_TEXT.text = "OFF";
                }
                else
                {
                    quick_Rub = true;
                    DEVELOP_MODE_TEXT.text = "ON";
                }
            }
        }

        public void DevelopFeature_Add_1_Rub()
        {
            if (ExtraParseInitialization.CurrentDevelopMode == DevelopMode.Dev)
            {
                _galleryCore.TouchCredit += 1;
            }
        }

        void Update()
        {
            if (_galleryCore.TouchCredit > 0 || currPointLevel > 4)
            {
                if (pointLevelUp_Effect.activeInHierarchy == false)
                {
                    switch (Application.platform)
                    {
                        case RuntimePlatform.OSXEditor:
                        case RuntimePlatform.OSXPlayer:
                        case RuntimePlatform.WindowsEditor:
                        case RuntimePlatform.WindowsPlayer:
                            touchMin = 10.0f;
                            MouseControl(interactable);
                            break;
                        case RuntimePlatform.IPhonePlayer:
                        case RuntimePlatform.Android:
                            touchMin = 10.0f;
                            TouchControl(interactable);
                            break;
                    }
                }
            }

            if (_galleryCore.TouchCredit <= 0 && currPointLevel <= 4)
            {
                if (handCursor.activeInHierarchy)
                    handCursor.SetActive(false);

                if (characterExpSliderPanel.activeInHierarchy)
                    characterExpSliderPanel.SetActive(false);

                if (PlayerPrefs.GetInt("DONT_SHOW_CP_TIPS", 0) == 0)
                {
                    if (watchAdGainRubTips == null && openedTips == false)
                    {
                        openedTips = true;

                        LoadTipsView("WatchAdGainRubTips", false);
                    }
                }
            }
            else
            {
                openedTips = false;
            }

            if (touchCreditRecoverTimer_Text.gameObject.activeInHierarchy)
                touchCreditRecoverTimer_Text.text = _galleryCore.RecalTouchCredit();

            if (touchCredit_Text.text != (_galleryCore.TouchCredit + "/" + _galleryCore.MaxTouchCredit).ToString())
                UpdateCharacterTouchCredit();
        }

        void MouseControl(bool interactable)
        {
            if (Input.GetMouseButtonDown(0))
            {
                clickV2 = Input.mousePosition;

                handCursor.transform.position = clickV2;
            }

            // Check null
            if (clickV2 == new Vector2(0, 0))
            {
                clickV2 = Input.mousePosition;

                handCursor.transform.position = clickV2;
            }

            if (Input.GetMouseButton(0))
            {
                bool scrolled = false;
                float disX = Mathf.Abs(Input.mousePosition.x - clickV2.x);
                float disY = Mathf.Abs(Input.mousePosition.y - clickV2.y);

                Vector3 wp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector2 touchPos = new Vector2(wp.x, wp.y);
                RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero);

                if (hit != null && hit.collider != null)
                {
                    if (scrolled == false)
                    {
                        if (disX > touchMin)
                        {
                            if (Input.mousePosition.x != clickV2.x)
                            {
                                // Swipe
                                if (interactable)
                                    UpdateCharacterCurrentExp();

                                scrolled = true;
                            }
                        }

                        if (disY > touchMin)
                        {
                            if (Input.mousePosition.y != clickV2.y)
                            {
                                // Swipe
                                if (interactable)
                                    UpdateCharacterCurrentExp();

                                scrolled = true;
                            }
                        }

                        if (scrolled == false)
                        {
                            clickV2 = Input.mousePosition;
                        }
                        else
                        {
                            if (effectTimer >= effectTime)
                            {
                                effectTimer = 0;

                                SoundManager.Instance.PlaySoundWithType(SoundType.HandCaress_1);

                                FieldGuideManager.Instance.bossCharacter.TouchAnimation();

                                if (interactable)
                                {
                                    GameObject heartObject = heartObjectPool.GetPooledObject();

                                    if (heartObject)
                                    {
                                        HeartEffectObj heartEffect =
                                            (HeartEffectObj) heartObject.GetComponent<HeartEffectObj>();
                                        heartEffect.Spawn(pointLevel_Text.transform.position, this);
                                        heartObject.SetActive(true);
                                    }
                                }
                            }
                            else
                            {
                                effectTimer += Time.deltaTime;
                            }
                        }
                    }

                    // Check "pass = true" must be next to checking direction & distance
                    if (scrolled)
                    {
                        clickV2 = Input.mousePosition;
                    }

                    if (!handCursor.activeInHierarchy)
                        handCursor.SetActive(true);
                }

                handCursor.transform.position = Vector3.Slerp(handCursor.transform.position, Input.mousePosition,
                    20 * Time.deltaTime);
            }
            else
            {
                if (interactable)
                {
                    characterCurrentExpSlider.value = 0;
                    characterExpSliderPanel.SetActive(false);
                }

                handCursor.SetActive(false);
            }
        }

        void TouchControl(bool interactable)
        {
            Touch touch;

            if (Input.touchCount == 1)
            {
                touch = Input.GetTouch(0);

                // Check null
                if (touchV2 == new Vector2(-1, -1))
                {
                    touchV2 = touch.position;
                }

                Vector3 wp = Camera.main.ScreenToWorldPoint(touch.position);
                Vector2 touchPos = new Vector2(wp.x, wp.y);
                RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero);
                if (hit != null && hit.collider != null)
                {
                    switch (touch.phase)
                    {
                        case TouchPhase.Began:
                            touchV2 = touch.position;
                            handCursor.transform.position = touchV2;
                            break;

                        case TouchPhase.Moved:
                            bool scrolled = false;
                            float disX = Mathf.Abs(touch.position.x - touchV2.x);
                            float disY = Mathf.Abs(touch.position.y - touchV2.y);
                            if (scrolled == false)
                            {
                                if (disX > touchMin)
                                {
                                    if (touch.position.x != touchV2.x)
                                    {
                                        // Swipe
                                        if (interactable)
                                            UpdateCharacterCurrentExp();

                                        scrolled = true;
                                    }
                                }

                                if (disY > touchMin)
                                {
                                    if (touch.position.y != touchV2.y)
                                    {
                                        // Swipe
                                        if (interactable)
                                            UpdateCharacterCurrentExp();

                                        scrolled = true;
                                    }
                                }

                                if (scrolled == false)
                                {
                                    touchV2 = touch.position;
                                }
                                else
                                {
                                    if (effectTimer >= effectTime)
                                    {
                                        effectTimer = 0;

                                        SoundManager.Instance.PlaySoundWithType(SoundType.HandCaress_1);

                                        FieldGuideManager.Instance.bossCharacter.TouchAnimation();

                                        if (interactable)
                                        {
                                            GameObject heartObject = heartObjectPool.GetPooledObject();

                                            if (heartObject)
                                            {
                                                HeartEffectObj heartEffect =
                                                    (HeartEffectObj) heartObject.GetComponent<HeartEffectObj>();
                                                heartEffect.Spawn(pointLevel_Text.transform.position, this);
                                                heartObject.SetActive(true);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        effectTimer += Time.deltaTime;
                                    }
                                }
                            }
                            // Check "pass = true" must be next to checking direction & distance
                            if (scrolled)
                            {
                                touchV2 = touch.position;
                            }
                            break;
                    }

                    if (!handCursor.activeInHierarchy)
                        handCursor.SetActive(true);
                }

                handCursor.transform.position = Vector3.Slerp(handCursor.transform.position, touchV2,
                    20 * Time.deltaTime);
            }
            else if (Input.touchCount < 1)
            {
                if (interactable)
                {
                    characterCurrentExpSlider.value = 0;
                    characterExpSliderPanel.SetActive(false);
                }

                handCursor.SetActive(false);
            }
        }

        void UpdateCharacterTouchCredit()
        {
            touchCredit_Text.text = (_galleryCore.TouchCredit + "/" + _galleryCore.MaxTouchCredit).ToString();

            touchCreditRecoverTimer_Text.gameObject.SetActive(_galleryCore.TouchCredit < _galleryCore.MaxTouchCredit);

            maxCredit.SetActive(_galleryCore.TouchCredit >= _galleryCore.MaxTouchCredit);

            interactable = _galleryCore.TouchCredit > 0 && currPointLevel <= 4;
        }

        void UpdateCharacterCurrentExp()
        {
            characterExpSliderPanel.SetActive(true);

            characterCurrentExpSlider.value += 0.01f;

            if (characterCurrentExpSlider.value >= 1)
            {
                characterCurrentExpSlider.value = 0;

                int _touchExp = touchExp;

                if (ExtraParseInitialization.CurrentDevelopMode == DevelopMode.Dev)
                {
                    if (quick_Rub)
                    {
                        if (currCharacterGallery.Level() < 5)
                        {
                            if (currCharacterGallery.CalExp <
                                currCharacterGallery.levels[currCharacterGallery.Level()] - 1)
                            {
                                _touchExp = currCharacterGallery.levels[currCharacterGallery.Level()] -
                                            currCharacterGallery.CalExp - 1;
                            }
                            else
                            {
                                _touchExp = 1;
                            }
                        }
                    }
                }

                currCharacterGallery.Exp += _touchExp;

                if (currCharacterGallery.Level() > currPointLevel)
                {
                    BossCharacterPointLevelUp();
                }

                point_Text.text = LanguageManager.Instance.GetTextValue("gallery_intimacyExp") + " : ";
                point_Text.text += currPointLevel < 5
                    ? currCharacterGallery.CalExp + "/" + currCharacterGallery.levels[currPointLevel]
                    : "MAX";

                characterPointSlider.value = currPointLevel < 5
                    ? (float) currCharacterGallery.CalExp / (float) currCharacterGallery.levels[currPointLevel]
                    : 1;

                _galleryCore.Touch(); // Here! Touch credit will -1
                UpdateCharacterTouchCredit();

                // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                Analytics.CustomEvent("rubMoetan", new Dictionary<string, object>
                {
                    { "moetanId", currCharacterGallery.girlId }
                });
#endif
            }
        }

        void UpdateCharacterCurrentIndex(bool isChangeSprite)
        {
            if (isChangeSprite)
            {
                string moetanBundleName =
                    AOMAssetBundlesConstant.MoetanBundleNameById(int.Parse(currCharacterGallery.girlId));

                SelectBossCharacterIndex(currSelectedIndex);

                if (currSelectedIndex < 5)
                {
                    switch (currSelectedIndex)
                    {
                        case 1:
                            AssetBundleLoader.Instance.LoadSpriteAsset(moetanBundleName,
                                "mo_" + currCharacterGallery.girlId + "_base01",
                                FieldGuideManager.Instance.bossCharacter.spriteRenderer, () => { });
                            break;
                        case 2:
                            AssetBundleLoader.Instance.LoadSpriteAsset(moetanBundleName,
                                "mo_" + currCharacterGallery.girlId + "_base02",
                                FieldGuideManager.Instance.bossCharacter.spriteRenderer, () => { });
                            break;
                        case 3:
                            AssetBundleLoader.Instance.LoadSpriteAsset(moetanBundleName,
                                "mo_" + currCharacterGallery.girlId + "_base03",
                                FieldGuideManager.Instance.bossCharacter.spriteRenderer, () => { });
                            break;
                        case 4:
                            AssetBundleLoader.Instance.LoadSpriteAsset(moetanBundleName,
                                "mo_" + currCharacterGallery.girlId + "_base04",
                                FieldGuideManager.Instance.bossCharacter.spriteRenderer, () => { });
                            break;
                    }

                    FieldGuideManager.Instance.bossCharacter.gameObject.SetActive(true);
                    FieldGuideManager.Instance.bossCharacter.SpawnAnimation();

                    moeSpirit_Container.gameObject.SetActive(false);
                }
                else
                {
                    FieldGuideManager.Instance.bossCharacter.gameObject.SetActive(false);
                    FieldGuideManager.Instance.bossCharacter.gameObject.transform.localPosition = Vector3.zero;

                    UpdateMoeSpiritStatus();

                    moeSpirit_Container.gameObject.SetActive(true);
                    moeSpirit_Container.GetComponent<Animator>().Play("Start", 0, 0);
                }
            }

            interactable = _galleryCore.TouchCredit > 0 && currPointLevel <= 4;

            heartEffectLayout_Group.SetActive(currPointLevel <= 4);

            characterPointSliderPanel.SetActive(currPointLevel <= 4);

            infoRewardTable.SetActive(currPointLevel <= 4);

            characterCurrentExpSlider.value = 0;
            characterExpSliderPanel.SetActive(false);

            touchCredit_Group.SetActive(currPointLevel <= 4);
        }

        void BossCharacterPointLevelUp()
        {
            currPointLevel = currCharacterGallery.Level();

            pointLevel_Text.text = currPointLevel.ToString();

            pointLevelUp_Animator.Play("LoveLevelUp");

            SoundManager.Instance.PlaySoundWithType(SoundType.LoveLevelUp_1);

            int gainValueSet = (int.Parse(currCharacterGallery.girlId) < 5) ? 0 : 1;
            int diamondGain = FieldGuideManager.Instance.diamondVal[gainValueSet, currPointLevel - 2];
            int relicGain = FieldGuideManager.Instance.relicVal[gainValueSet, currPointLevel - 2];

            rewardImageRelic.gameObject.SetActive(relicGain == 0 ? false : true);
            reward_Text_Relic.gameObject.SetActive(relicGain == 0 ? false : true);

            reward_Text_Diamond.text = "x " + diamondGain.ToString();
            reward_Text_Relic.text = "x " + relicGain.ToString();

            _userService.CurrentUserSummary.Gem += diamondGain;
            _userService.CurrentUserSummary.Moecrystal += relicGain;

            _userService.UpdateUserSummary(_userService.CurrentUserSummary, (e) =>
            {
                if (e != null)
                {
                    Debug.LogError("Update UserSummary fail");
                }
                else
                {
                    AOMStoreInventory.ResetDiamond(_userService.CurrentUserSummary.Gem);
                    AOMStoreInventory.ResetRelic(_userService.CurrentUserSummary.Moecrystal);
                }
            });

            interactable = false;
        }

        public void CloseRewardOverlay()
        {
            pointLevelUp_Animator.Play("LoveLevelUp_Finish");

            interactable = _galleryCore.TouchCredit > 0 && currPointLevel <= 4;

            currSelectedIndex = currPointLevel;

            UpdateCharacterCurrentIndex(true);

            ChangeLevelBtnStatus();
        }

        IEnumerator LoadSelectedBossCharacter()
        {
            /*currCharacterGallery = _galleryCore.GetGirlWithID(GalleryCore
                .GetRealBossId(FieldGuideManager.Instance.currentSelectedBossIndex + 1)
                .ToString("000"));*/

            currCharacterGallery = _galleryCore.GetGirlWithID(FieldGuideManager.Instance.bossCharacterPool
                .bosses[FieldGuideManager.Instance.currentSelectedBossIndex]
                .bossId.Replace("Boss_", ""));

            currCharacterGallery.ReachedNewGallery();

            bossCharacter_Name_Text.text =
                LanguageManager.Instance.GetTextValue(FieldGuideManager.Instance.bossCharacter.name + "_name");

            currPointLevel = currCharacterGallery.Level();

            currSelectedIndex = (currPointLevel == 5) ? 4 : currPointLevel;

            pointLevel_Text.text = currPointLevel.ToString();

            point_Text.text = LanguageManager.Instance.GetTextValue("gallery_intimacyExp") + " : ";
            point_Text.text += currPointLevel < 5
                ? currCharacterGallery.CalExp + "/" + currCharacterGallery.levels[currPointLevel]
                : "MAX";

            characterPointSlider.value = currPointLevel < 5
                ? (float) currCharacterGallery.CalExp / (float) currCharacterGallery.levels[currPointLevel]
                : 1;

            ChangeLevelBtnStatus();

            // Load MoeSpirit spirte
            AssetBundleLoader.Instance.LoadImageAsset("moespirit_icon-bundle",
                FieldGuideManager.Instance.bossCharacter.name.Replace("Boss_", "MS"), moeSpirit_Sprite);

            moeSpirit_Container.gameObject.SetActive(false);

            UpdateCharacterCurrentIndex(true);

            UpdateCharacterTouchCredit();

            yield return null;

            FieldGuideManager.Instance.ShowLoadingOverlay(false);

            if (!TutorialManager.Instance.CheckTutorialCompletedById(9))
            {
                TutorialManager.Instance.TriggerTutorial(9);
                ShowRubInstructionOverlay();
            }
        }

        public void SelectBossCharacterIndex(int index)
        {
            if (index != currSelectedIndex && pointLevelUp_Effect.activeInHierarchy == false)
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                currSelectedIndex = index;

                UpdateCharacterCurrentIndex(true);

                ChangeLevelBtnStatus();
            }
        }

        void ChangeLevelBtnStatus()
        {
            for (int i = 0; i < levelBtn.Length; i++)
            {
                levelBtn[i].interactable = i < currPointLevel;

                Image levelBtnImage = levelBtn[i].transform.FindChild("ButtonIcon").GetComponent<Image>();

                // Set unlock image
                levelBtnImage.sprite = (i >= currPointLevel)
                    ? levelBtn[i].spriteState.disabledSprite
                    : levelBtn[i].spriteState.highlightedSprite;

                if (i > 0)
                    levelBtn[i].transform.FindChild("UnlockStage").GetComponent<Text>().text = (i >= currPointLevel)
                        ? (LanguageManager.Instance.GetTextValue("gallery_unlockLoveLV") + (i + 1).ToString())
                        : "";

                // Set sctive image
                if (i == currSelectedIndex - 1)
                {
                    levelBtnImage.sprite = levelBtn[i].spriteState.pressedSprite;
                }
            }
        }

        public void OpenMoeSpiritOverlay()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            foreach (ArtifactVG _ms in ArtifactAssets.MoeSpirits)
            {
                if (_ms.ItemId == "MS" + currCharacterGallery.girlId)
                {
                    GameObject msDetailView =
                        _uiViewLoader.LoadOverlayView("MoetifactAndMoeSpiritDetailView", null, false, false);
                    msDetailView.GetComponent<MoetifactAndMoeSpiritDetailViewController>().SetDisplayItem(_ms, false);
                }
            }
        }

        public void ShowRubInstructionOverlay()
        {
            LoadOverlayView("RubInstructionView", false, false);
        }

        public void ShowRewardTable()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            LoadOverlayView("IntimacyLevelRewardView", false, false);
        }

        void VideoAdReward()
        {
            // Add rub
            _galleryCore.RechargingTouchCredit(1);
        }

        public void GetCP()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            FieldGuideManager.Instance.OpenCPShopOverlay();
        }

        public void ShowRubAdMenuOverlay()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            if (Chartboost.hasRewardedVideo(CBLocation.MainMenu))
            {
                Chartboost.showRewardedVideo(CBLocation.MainMenu);
            }
            else
            {
                // We don't have a cached video right now, but try to get one for next time
                Chartboost.cacheRewardedVideo(CBLocation.MainMenu);
            }

            // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.CustomEvent("viewAdResumeRubQuota", new Dictionary<string, object>
                {
                    { "numberOfView", "View" }
                });
#endif
        }

        void didFailToLoadRewardedVideo(CBLocation location, CBImpressionError error)
        {
            Debug.Log(string.Format("didFailToLoadRewardedVideo: {0} at location {1}", error, location));
        }

        void didDismissRewardedVideo(CBLocation location)
        {
            Debug.Log("didDismissRewardedVideo: " + location);
        }

        void didCloseRewardedVideo(CBLocation location)
        {
            Debug.Log("didCloseRewardedVideo: " + location);
        }

        void didClickRewardedVideo(CBLocation location)
        {
            Debug.Log("didClickRewardedVideo: " + location);
        }

        void didCacheRewardedVideo(CBLocation location)
        {
            Debug.Log("didCacheRewardedVideo: " + location);
        }

        void didCompleteRewardedVideo(CBLocation location, int reward)
        {
            Debug.Log(string.Format("didCompleteRewardedVideo: reward {0} at location {1}", reward, location));
            VideoAdReward();
        }

        void didDisplayRewardedVideo(CBLocation location)
        {
            Debug.Log("didDisplayRewardedVideo: " + location);
        }

        void UpdateMoeSpiritStatus()
        {
            if (moeSpirit_Container != null)
            {
                foreach (ArtifactVG _ms in ArtifactAssets.MoeSpirits)
                {
                    if (_ms.ItemId == "MS" + currCharacterGallery.girlId)
                    {
                        moeSpirit_Sprite.color = (_ms.GetBalance() > 0)
                            ? new Color(1, 1, 1, 1)
                            : new Color(0.5f, 0.5f, 0.5f, 1);
                    }
                }
            }
        }

        // Load Page 
        public override void LoadPageView(string pageName)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            FieldGuideManager.Instance.ShowLoadingOverlay(true, true);
            FieldGuideManager.Instance.UnloadSelectedBossCharacter();

            RemoveListener();

            Destroy(background);

            base.LoadPageView(pageName);
        }
    }
}