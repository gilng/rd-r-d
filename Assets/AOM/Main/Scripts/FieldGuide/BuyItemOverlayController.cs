﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Soomla.Store;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class BuyItemOverlayController : UIOverlayViewController
    {
        public Image icon;
        public Text itemName, itemDesc, priceGems, priceMoney, currentGems;
        public Button btnGem;

        GameItemVG gameItem;
        VirtualGood virtualGood;

        Sprite[] thumbnailIcon;
        string[] thumbnailIconName;

        // Use this for initialization
        public override void InitView(UIViewController previousUIView = null, bool pause = false)
        {
            base.InitView(previousUIView, pause);

            AddListener();

            if (overlayContainerParent != null)
                this.transform.SetParent(overlayContainerParent.transform, false);

            thumbnailIcon = Resources.LoadAll<Sprite>("Sprites/table_ui_diamond/table_ui_diamond");
            thumbnailIconName = new string[thumbnailIcon.Length];

            for (int i = 0; i < thumbnailIconName.Length; i++)
            {
                thumbnailIconName[i] = thumbnailIcon[i].name;
            }
        }

        void AddListener()
        {
            EventManager.OnDiamondBalanceUpdated += RefreshGems;
            EventManager.OnGalleryCloseOverlay += CloseView;
        }

        void RemoveListener()
        {
            EventManager.OnDiamondBalanceUpdated -= RefreshGems;
            EventManager.OnGalleryCloseOverlay -= CloseView;
        }

        void RefreshGems()
        {
            currentGems.text = string.Format(LanguageManager.Instance.GetTextValue("current_gems"), UserManager.Instance.CurrentUserSummary != null ? UserManager.Instance.CurrentUserSummary.Gem.ToString() : AOMStoreInventory.GetDiamondBalance().ToString());

            if (gameItem != null)
            {
                if (gameItem.CanAffordOne())
                {
                    btnGem.GetComponent<Image>().sprite = btnGem.GetComponent<Button>().spriteState.highlightedSprite;
                }
                else
                {
                    btnGem.GetComponent<Image>().sprite = btnGem.GetComponent<Button>().spriteState.pressedSprite;
                }
            }
        }

        public void OpenKeyShop(string id)
        {
            // Icon
            icon.sprite = thumbnailIcon[Array.IndexOf(thumbnailIconName, id)];

			itemName.text = LanguageManager.Instance.GetTextValue(id + "_name");
			itemDesc.text = LanguageManager.Instance.GetTextValue(id + "_desc");

            // Gems
            foreach (GameItemVG givg in AOMAssets.GameItemVGs)
            {
                if (givg.ItemId == id)
                {
                    gameItem = givg;
                    priceGems.text = gameItem.Price.ToString();
                    RefreshGems();
                }
            }

            // Money
            foreach (VirtualGood vg in AOMAssets.NonConsumableItems)
            {
                if (vg.ItemId == id)
                {
                    virtualGood = vg;

#if UNITY_IOS
					priceMoney.text = AOMAssets.ProductMarketPriceAndCurrency(id.Replace("key", "keyofworld"));
#elif UNITY_ANDROID
                    priceMoney.text = AOMAssets.ProductMarketPriceAndCurrency(id);
#endif
                }
            }

            if (gameItem != null && virtualGood != null)
                this.gameObject.SetActive(true);
            else
                Debug.LogError("id: " + id + " - gameItem: " + (gameItem != null) + " - virtualGood: " + (virtualGood != null));
        }

        public void CostGems()
        {
            if (gameItem != null)
            {
                if (gameItem.CanAffordOne())
                {
                    //SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                    gameItem.BuyOne(() =>
                    {
                        EventManager.CallGalleryUpdate(gameItem.ItemId);

                        GachaResultViewController purchasedViewController = LoadOverlayViewController("GachaResultMenuView", true, Time.timeScale == 0) as GachaResultViewController;
                        purchasedViewController.SetItemId(gameItem, GachaType.Purchase);
                    });
                }
                else
                {
                    OpenShopMenu();
                }
            }
        }

        public void CostMoney()
        {
            if (virtualGood != null)
            {
                try
                {
                    SoundManager.Instance.PlaySoundWithType(SoundType.Purchase_CashierBell);

                    AOMStoreInventory.BuyItem(virtualGood.ItemId);

                    CloseView();
                }
                catch (Exception e)
                {
                }
            }
        }

        void OpenShopMenu()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            LoadOverlayView("ShopMenu", false, false);
        }

        public override void CloseView()
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            RemoveListener();

            base.CloseView();
        }
    }
}