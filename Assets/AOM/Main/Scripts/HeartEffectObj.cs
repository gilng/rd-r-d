﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Ignite.AOM
{
    public class HeartEffectObj : MonoBehaviour
    {
        protected float speed = 2f;

        Vector3 targetPos;

        BossCharacterViewController bossCharacterViewController;

        void Update()
        {
            if (this.gameObject.activeSelf)
            {
                transform.position = Vector3.Slerp(transform.position, targetPos, Time.deltaTime * speed);

                if (Vector3.Distance(transform.position, targetPos) < 10)
                {
                    this.gameObject.SetActive(false);

                    if (bossCharacterViewController != null)
                        bossCharacterViewController.pointIncrease_Animator.Play("Point_Increase");

					SoundManager.Instance.PlaySoundWithType(SoundType.HeartBubble_1);
                }
            }
        }

        public void Spawn(Vector3 targetPos, BossCharacterViewController parentViewController)
        {
            this.transform.localScale = Vector3.one;
            this.transform.localPosition = new Vector2(UnityEngine.Random.Range(-16, 15), 300);

            this.targetPos = targetPos;

            this.bossCharacterViewController = parentViewController;
        }
    }
}
