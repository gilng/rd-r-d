﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;
using Random = UnityEngine.Random;
using Zenject;

namespace Ignite.AOM
{
    public delegate void EnemyDieEventHandler();

    public class Enemy : MonoBehaviour
    {
        private EnemyCore _enemyCore;
        private HeroCore _heroCore;

        private GalleryCore _galleryCore;

        [Inject]
        public void ConstructorSafeAttribute(EnemyCore enemyCore, HeroCore heroCore, GalleryCore galleryCore)
        {
            _enemyCore = enemyCore;
            _heroCore = heroCore;
            _galleryCore = galleryCore;
        }

        private EnemyType currentEnemyType;
        private double maxHp;
        private double currentHp;
        private Animator animator;
        public SpriteRenderer spriteRenderer;
        private bool isDamageable = false;
        public event EnemyDieEventHandler OnDie;

        private float lastPlayAudioTime;

        public double MaxHp
        {
            get { return maxHp; }
        }

        public double CurrentHp
        {
            get { return currentHp; }
        }

        public bool IsDamageable
        {
            get { return isDamageable; }
        }

        public GameObject monsterGirl;
        public Animator monsterAnim;
        public GameObject breakEffect;
        public GameObject deadEffect;
        public int breakLevel;

        public Sprite originalSprite;

        //public Sprite[] breakSprite;
        public List<float> breakCharPosX;

        public List<float> breakCharPosY;

        public List<SoundType> BossOnHitSound;

        private bool willKillHero;
        private int killHeroTimer = 1;

        private int getHitTime_SFXCounter = 0;
        private float getHitTime_PowerOfHolding = 0;

        void Awake()
        {
            lastPlayAudioTime = 0;
            animator = GetComponent<Animator>();

            foreach (SpriteRenderer t in gameObject.GetComponentsInChildren<SpriteRenderer>())
            {
                if (t.material != null)
                {
                    t.material.shader = Shader.Find(t.material.shader.name);
                }
            }
        }

        private IEnumerator TryToKillHero()
        {
            while (willKillHero)
            {
                if (_enemyCore.ShouldKillHero(killHeroTimer))
                {
                    willKillHero = false;
                    _heroCore.KillHero(_enemyCore.Stage);
                    animator.Play("Attack", 0, 0);
                }
                yield return new WaitForSeconds(1f);
                killHeroTimer++;
            }
        }

        public void Spawn()
        {
            maxHp = _enemyCore.SpawnEnemy();
            //			Debug.Log("Enemy Spawn - HP: " + UnitConverter.ConverterDoubleToString(maxHp));
            currentEnemyType = _enemyCore.CurrentEnemyType;
            currentHp = maxHp;
            GameManager.Instance.enemyName.GetComponent<Text>().text =
                LanguageManager.Instance.GetTextValue(gameObject.name + "_name");
            RefreshEnemyHealthHUD();
            SoundManager.Instance.PlaySoundWithType(SoundType.EnemySpawn_1);

            //if (originalSprite != null)
            //    spriteRenderer.sprite = originalSprite;

            isDamageable = true;
            SpawnAnimation();
            killHeroTimer = 1;
            if (EnemyType.BigBoss == currentEnemyType || EnemyType.MiniBoss == currentEnemyType)
            {
                //				EventManager.FairyLeave(true);

                willKillHero = _heroCore.TotalAliveHeroLevel > 0;
                StartCoroutine(TryToKillHero());
            }

            if (EnemyType.BigBoss == currentEnemyType)
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.BossSpawn_1);

                Gallery bossGallery = _galleryCore.GetGirlWithID(gameObject.name.Replace("Boss_", ""));

                string moetanBundleName = AOMAssetBundlesConstant.MoetanBundleNameById(int.Parse(bossGallery.girlId));
//                AssetBundleLoader.Instance.LoadSpriteAsset(moetanBundleName, "mo_" + bossGallery.girlId + "_base01",
//                    spriteRenderer, () => { });

                spriteRenderer.sprite = Resources.Load<Sprite>(
                    "Moetan/Texture/mo" + bossGallery.girlId + "/mo_" + bossGallery.girlId + "_base01");

                if (bossGallery != null && bossGallery.Status != MonsterGirlStatus.Killed)
                    bossGallery.Status = MonsterGirlStatus.Showed;
            }
        }

        public void TakeDamage(double damage, bool showDamageLabel, bool playSound = false)
        {
            if (showDamageLabel)
            {
                GameObject dmgLabelObject = GameManager.Instance.dmgTextObjectPool.GetPooledObject();
                if (dmgLabelObject)
                {
                    TapDamageText dmgLabel = (TapDamageText) dmgLabelObject.GetComponent<TapDamageText>();
                    dmgLabel.textObject.text = UnitConverter.ConverterDoubleToString(damage);
                    dmgLabel.Spawn();
                    dmgLabelObject.SetActive(true);
                }
            }
            currentHp -= damage;

            if (currentEnemyType == EnemyType.BigBoss)
            {
                if (Time.time - lastPlayAudioTime >= 0.6 && playSound)
                {
                    if (getHitTime_SFXCounter == 0)
                    {
                        getHitTime_SFXCounter = Random.Range(15, 60);
                    }
                    else
                    {
                        getHitTime_SFXCounter--;

                        if (getHitTime_SFXCounter == 0)
                        {
                            lastPlayAudioTime = Time.time;
                            SoundManager.Instance.PlaySoundWithType(
                                BossOnHitSound[Random.Range(0, BossOnHitSound.Count)]);
                        }
                    }
                }
            }
            else
            {
                if (playSound)
                {
                    if (getHitTime_SFXCounter == 0)
                    {
                        getHitTime_SFXCounter = (currentEnemyType == EnemyType.MiniBoss)
                            ? Random.Range(15, 60)
                            : Random.Range(15, 80);
                    }
                    else
                    {
                        getHitTime_SFXCounter--;

                        if (getHitTime_SFXCounter == 0)
                        {
                            SoundManager.Instance.PlaySoundWithType(
                                BossOnHitSound[Random.Range(0, BossOnHitSound.Count)]);
                        }
                    }
                }
            }

            if (currentHp > 0)
            {
                if (showDamageLabel)
                {
                    bool allowPlayAnime = !(Avatar.instance.UsingPowerOfHolding);

                    if (Avatar.instance.UsingPowerOfHolding)
                    {
                        if (getHitTime_PowerOfHolding <= 0)
                        {
                            getHitTime_PowerOfHolding = 0.1f;
                            allowPlayAnime = true;
                        }
                        else
                        {
                            getHitTime_PowerOfHolding -= Time.deltaTime;
                            allowPlayAnime = false;
                        }
                    }

                    if (allowPlayAnime)
                    {
                        if (monsterAnim != null)
                            monsterAnim.Play("Hit", 0, 0);
                    }
                }
            }
            else
            {
                willKillHero = false;
                isDamageable = false;
                currentHp = 0;

                if (currentEnemyType == EnemyType.BigBoss) // || currentEnemyType == EnemyType.MiniBoss)
                {
                    Gallery bossGallery = _galleryCore.GetGirlWithID(gameObject.name.Replace("Boss_", ""));
                    if (bossGallery != null)
                    {
                        bossGallery.Status = MonsterGirlStatus.Killed;
                        if (bossGallery.ReachedBreakLevel < breakLevel + 1)
                            bossGallery.ReachedBreakLevel = breakLevel + 1;
                    }

                    GameManager.Instance.blockScene.SetActive(true);
                    GameManager.Instance.breakScene.SetActive(true);
                    //GameManager.Instance.breakSceneChar.sprite = breakSprite[breakLevel];

//                    AssetBundleLoader.Instance.LoadSpriteAsset(
//                        AOMAssetBundlesConstant.MoetanBundleNameById(int.Parse(bossGallery.girlId)),
//                        "mo_" + bossGallery.girlId + "_base" + (breakLevel + 2).ToString("00"),
//                        GameManager.Instance.breakSceneChar,
//                        () => { spriteRenderer.sprite = GameManager.Instance.breakSceneChar.sprite; });

                    GameManager.Instance.breakSceneChar.sprite = Resources.Load<Sprite>(
                        "Moetan/Texture/mo" + bossGallery.girlId + "/mo_" + bossGallery.girlId + "_base01");
                    spriteRenderer.sprite = GameManager.Instance.breakSceneChar.sprite;

                    //spriteRenderer.sprite = breakSprite[breakLevel];
                }
                else
                {
                    if (monsterAnim != null)
                        monsterAnim.Play("Dead", 0, 0);
                }

                SoundManager.Instance.PlaySoundWithType(SoundType.EnemyDead_1);

                // Boardcast Die event
                StartCoroutine(Die());
            }

            RefreshEnemyHealthHUD();
        }

        private IEnumerator Die()
        {
            if (currentEnemyType == EnemyType.BigBoss) // || currentEnemyType == EnemyType.MiniBoss)
            {
                yield return new WaitForSeconds(3.6f);

                //EventManager.FairyLeave(false);
            }
            else
            {
                bool findEff = false;

                foreach (GameObject eff in GameManager.Instance.enemyPool.enemyDead_effects)
                {
                    if (findEff == false)
                    {
                        if (eff.activeSelf == false)
                        {
                            eff.SetActive(true);
                            findEff = true;

                            yield return new WaitForSeconds(0.1f);

                            SoundManager.Instance.PlaySoundWithType(SoundType.EnemyExplosion);
                        }
                    }
                }

                yield return new WaitForSeconds(0.5f);

                //				EventManager.FairyLeave(false);
            }

            if (currentEnemyType == EnemyType.BigBoss) // || currentEnemyType == EnemyType.MiniBoss)
            {
                GameManager.Instance.blockScene.SetActive(false);
                GameManager.Instance.breakScene.SetActive(false);

                GameManager.Instance.enemyPool.moetanDead_effect.GetComponent<SoulAnimationEffect>().Wake();
            }

            if (OnDie != null)
                OnDie();
        }

        private void RefreshEnemyHealthHUD()
        {
            GameManager.Instance.enemyHPTextObject.GetComponent<Text>().text =
                UnitConverter.ConverterDoubleToString(Math.Ceiling(currentHp)) + " HP ";
            GameManager.Instance.sliderObject.GetComponent<Slider>().value = (float) (currentHp / maxHp);
        }

        public void SpawnAnimation()
        {
            monsterAnim.Play("Spawn", 0, 0);
        }

        public void TouchAnimation()
        {
            if (Time.time - lastPlayAudioTime >= 1.0)
            {
                if (getHitTime_SFXCounter == 0)
                {
                    getHitTime_SFXCounter = Random.Range(3, 10);
                }
                else
                {
                    getHitTime_SFXCounter--;

                    if (getHitTime_SFXCounter == 0)
                    {
                        lastPlayAudioTime = Time.time;
                        SoundManager.Instance.PlaySoundWithType(BossOnHitSound[Random.Range(0, BossOnHitSound.Count)]);
                    }
                }
            }

            monsterAnim.Play("Touch", 0, 0);
        }

        public class Factory : Factory<GameObject, Enemy>
        {
        }
    }

    public class EnemyFactory : IFactory<GameObject, Enemy>
    {
        private readonly DiContainer _container;

        public EnemyFactory(DiContainer container)
        {
            _container = container;
        }

        public Enemy Create(GameObject prefab)
        {
            Enemy enemy = null;

            if (prefab != null)
            {
                _container.InjectGameObject(prefab);

                enemy = prefab.GetComponent<Enemy>();
            }
            else
            {
                GameObject girlObj = _container.InstantiatePrefabResource("Moetan/Prefab/Boss_001");
                _container.InjectGameObject(girlObj);
                enemy = girlObj.GetComponent<Enemy>();
            }

            return enemy;
        }
    }
}