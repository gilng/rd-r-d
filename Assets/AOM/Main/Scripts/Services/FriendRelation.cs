﻿using Parse;
using System.Collections.Generic;

namespace Ignite.AOM
{
    [ParseClassName("FriendRelation")]
    public class FriendRelation : Entity
    {
        [ParseFieldName("users")]
        public IList<ParseUser> Users
        {
            get { return GetProperty<IList<ParseUser>>("Users"); }
        }
    }
}