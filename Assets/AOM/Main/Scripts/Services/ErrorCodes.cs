﻿namespace Ignite.AOM
{
    /// <summary>
    /// Error codes that may be delivered in response to requests to server.
    /// </summary>
    public class ErrorCodes
    {
        public const string TimeOut = "TimeOut";
        public const string Cancelled = "Cancelled";
        public const string Unknown = "Unknown";
        public const string Unauthorized = "Unauthorized";
        public const string BadRequest = "BadRequest";
        public const string FriendRelationAlreadyExists = "FriendRelationAlreadyExists";
        public const string UserFriendListReachedMaximum = "UserFriendListReachedMaximum";
        public const string TargetUserFriendListReachedMaximum = "TargetUserFriendListReachedMaximum";
        public const string InvaildTargetUserId = "InvaildTargetUserId";
        public const string DataNotFound = "DataNotFound";
    }
}