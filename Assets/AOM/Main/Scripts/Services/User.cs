﻿using Parse;

namespace Ignite.AOM
{
    public interface IUser
    {
        string Username { get; }
        string UserId { get; }
        string UserEmail { get; }
    }

    public class ParseUserWrapper : IUser
    {
        private readonly ParseUser _parseUser;

        public ParseUserWrapper(ParseUser parseUser)
        {
            _parseUser = parseUser;
        }

        public string Username
        {
            get { return _parseUser.Username; }
        }

        public string UserId
        {
            get { return _parseUser.ObjectId; }
        }

        public string UserEmail
        {
            get { return _parseUser.Email; }
        }
    }
}