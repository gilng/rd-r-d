﻿using System;
using UnityEngine;
using Parse;

namespace Ignite.AOM
{
    public enum DevelopMode
    {
        Local,
        Dev,
        Prod
    }

    public class ExtraParseInitialization : MonoBehaviour
    {
        public static DevelopMode CurrentDevelopMode { get; set; }

        public DevelopMode DevelopMode;

        public void Awake()
        {
            var pib = GetComponent<ParseInitializeBehaviour>();

            CurrentDevelopMode = DevelopMode;

            switch (CurrentDevelopMode)
            {
                case DevelopMode.Dev:
                    pib.applicationID = "game.ignite.aom.dev";
                    pib.serverURL = "http://aom-parse-dev.herokuapp.com/parse/";
                    break;
                case DevelopMode.Local:
                    // For Local Development
                    pib.applicationID = "aom-parse-local";
                    pib.serverURL = "http://192.168.1.134:1337/parse/";
                    break;
                case DevelopMode.Prod:
                    pib.applicationID = "game.ignite.aom.prd";
                    pib.serverURL = "http://server.aom.ignite-ga.me/parse/";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            ParseObject.RegisterSubclass<Notice>();
            ParseObject.RegisterSubclass<DropBoxItem>();

            ParseObject.RegisterSubclass<UserSummary>();
            ParseObject.RegisterSubclass<FriendRelation>();
            ParseObject.RegisterSubclass<BattleLog>();
        }
    }
}