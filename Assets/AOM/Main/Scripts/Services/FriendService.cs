﻿using Parse;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ignite.AOM
{
    public interface IFriendService: ICrudService<FriendRelation>
    {
        List<FriendRelation> FriendRelationList { get; set; }
        void FindAllFriendRelation(Action<IList<FriendRelation>> success, Action<string> error);

        List<UserSummary> FriendUserSummaryList { get; set; }
        void FindAllFriendUserSummary(IList<ParseUser> friendUsers, Action<IList<UserSummary>> success,
            Action<string> error);

        void FindFriendUserSummary(ParseUser friendUsers, Action<UserSummary> success, Action<string> error);

        void AddFriend(string targetUserID, Action<FriendRelation> success, Action<string> error);
    }

    public class FriendService : CrudService<FriendRelation>, IFriendService
    {
        public List<FriendRelation> FriendRelationList { get; set; }
        public void FindAllFriendRelation(Action<IList<FriendRelation>> success, Action<string> error)
        {
            var friendListDataQuery =
                new ParseQuery<FriendRelation>().WhereEqualTo("users", ParseUser.CurrentUser).OrderBy("createdAt");
            var loadFriendListTask = friendListDataQuery.FindAsync();

            AwaitAndCallback(loadFriendListTask, result => {
                if(FriendRelationList != null)
                    FriendRelationList.Clear();

                FriendRelationList = new List<FriendRelation>();
                FriendRelationList.AddRange(result.ToList());
                success(FriendRelationList);
            }, error);
        }

        public List<UserSummary> FriendUserSummaryList { get; set; }
        public void FindAllFriendUserSummary(IList<ParseUser> friendUsers, Action<IList<UserSummary>> success, Action<string> error)
        {
            var friendListDataQuery =
                new ParseQuery<UserSummary>().WhereContainedIn("user", friendUsers).OrderBy("createdAt");
            var loadFriendListTask = friendListDataQuery.FindAsync();

            AwaitAndCallback(loadFriendListTask, result =>
            {
                if(FriendUserSummaryList != null)
                    FriendUserSummaryList.Clear();

                FriendUserSummaryList = new List<UserSummary>();
                FriendUserSummaryList.AddRange(result.ToList());
                success(FriendUserSummaryList);
            }, error);
        }

        public void FindFriendUserSummary(ParseUser friendUsers, Action<UserSummary> success, Action<string> error)
        {
            var friendUserSummmaryDataQuery =
                new ParseQuery<UserSummary>().WhereEqualTo("user", friendUsers).OrderBy("createdAt");
            var loadFriendUserSummaryTask = friendUserSummmaryDataQuery.FirstOrDefaultAsync();

            AwaitAndCallback(loadFriendUserSummaryTask, success, error);
        }

        public void AddFriend(string targetUserID, Action<FriendRelation> success, Action<string> error)
        {
            IDictionary<string, object> parms = new Dictionary<string, object>
            {
                {"targetUserID", targetUserID}
            };

            var addFriendTask = ParseCloud.CallFunctionAsync<FriendRelation>("addFriend", parms);

            AwaitAndCallback(addFriendTask, success, error);
        }
    }
}