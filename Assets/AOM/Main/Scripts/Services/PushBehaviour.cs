﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Parse;
using System;
using UnityEngine.iOS;

namespace Ignite.AOM
{
    public class PushBehaviour : MonoBehaviour
    {
        private static PushBehaviour _instance;
        public static PushBehaviour Instance
        {
            get { return _instance; }
        }

        private readonly string ProjectID = "779470401083"; //replace with yours

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);

            _instance = this;

#if UNITY_ANDROID
            GCM.Initialize(); //create GCMReceiver

            // Set callbacks
            GCM.SetErrorCallback(errorId => Debug.Log("Parse notification error: " + errorId));
            GCM.SetUnregisteredCallback(registrationId => Debug.Log("Parse notification Unregistered: " + registrationId));
            GCM.SetDeleteMessagesCallback(total => Debug.Log("Parse notification DeleteMessages " + total));
            GCM.SetMessageCallback(HandleMessage);
#endif
        }

#if UNITY_IOS
        void Update()
        {
            foreach (var notification in UnityEngine.iOS.NotificationServices.remoteNotifications)
            {
                HandleMessage(notification.userInfo);
            }
            UnityEngine.iOS.NotificationServices.ClearRemoteNotifications();
        }
#endif

        //You can get data that tou send in notification's JSON from dict param.
        //Warrning!
        //Not all messages come here.
        //On iOS messages will be handled if you tap on notification, also it will be handled if you be in game when notification will arrive.
        //But message won't be handled if notification arrive when you not in game and you not tap the notification.
        //On Android it will be handled only if notification arrives when game running on foreground.
        void HandleMessage(IDictionary dict)
        {
            foreach (var key in dict.Keys)
            {
                Debug.Log(key + " : " + dict[key]);
            }
        }


        public void Init()
        {
            StartCoroutine(PrepareNotifications());
        }

        private IEnumerator PrepareNotifications()
        {
            //wait for internet connection
            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                yield return null;
            }

            if (PlayerPrefs.HasKey("currentInstallation"))
            {
                RefreshInstalliation();
            }
            else
            {
                Debug.Log("creating parse notification installiation");
#if UNITY_IOS
                yield return StartCoroutine(CreateIosInstalliation());
#elif UNITY_ANDROID
                CreateAndroidInstalliation();
#endif
            }
        }

        //update installiation object in case you change version or something else
        private void RefreshInstalliation()
        {
            var obj = ParseInstallation.CurrentInstallation;
            obj.SaveAsync();
        }


#if UNITY_IOS
        IEnumerator CreateIosInstalliation()
        {
            UnityEngine.iOS.NotificationServices.RegisterForNotifications(UnityEngine.iOS.NotificationType.Alert
                                                          | UnityEngine.iOS.NotificationType.Badge
                                                          | UnityEngine.iOS.NotificationType.Sound);

            //wait for token
            while (UnityEngine.iOS.NotificationServices.deviceToken == null)
            {
                yield return null;
            }

            Debug.Log("parse notification Token gotten");
            var token = UnityEngine.iOS.NotificationServices.deviceToken;
            var tokenString = System.BitConverter.ToString(token).Replace("-", "").ToLower();
            var obj = CreateInstallationObject(tokenString, "ios");

            obj.SaveAsync().ContinueWith(t =>
            {
                if (t.IsFaulted || obj.ObjectId == null)
                {
                    Debug.LogError("save parse installiation failed");
                }
                else
                {
                    Debug.LogError("save parse installiation ok");
                    PlayerPrefs.SetString("currentInstallation", obj.ObjectId);
                }
            });
        }
#elif UNITY_ANDROID
        private void CreateAndroidInstalliation()
        {
//            const string ProjectID = "779470401083"; //replace with yours
            GCM.SetRegisteredCallback(registrationId =>
            {
                Debug.Log("Parse notification Registered: " + registrationId);
                var obj = CreateInstallationObject(registrationId, "android");
//                obj["pushType"] = "gcm";
//                obj["GCMSenderId"] = ProjectID;
                obj.SaveAsync().ContinueWith(t =>
                {
                    if (t.IsFaulted)
                    {
                        Debug.LogError("error on parse push installiation ");
                    }
                    else if (obj.ObjectId != null)
                    {
                        PlayerPrefs.SetString("currentInstallation", obj.ObjectId);
                    }
                });
            });

            string[] senderIds = { ProjectID };
            GCM.Register(senderIds);
        }
#endif

        private ParseObject CreateInstallationObject(string token, string deviceType)
        {
            var obj = ParseInstallation.CurrentInstallation;
            obj["GCMSenderId"] = ProjectID;
            obj["deviceToken"] = token;
            obj["user"] = ParseUser.CurrentUser;
            obj["pushedAt"] = DateTime.MinValue;
//            obj["appIdentifier"] = "game.ignite.aom"; //replace with yours
//            obj["deviceType"] = deviceType;
//            obj["timeZone"] = "UTC";
//            obj["appName"] = "Attack on Moe"; //replace with yours
//            obj.AddToList("channels", "channel1");
//            obj.AddToList("channels", "channel2");
//            UpdateObject(obj);
            return obj;
        }


//        private void UpdateObject(ParseObject obj)
//        {
//            obj["appVersion"] = "1.0";
//            obj["parseVersion"] = "1.3.1";
//            obj["language"] = Application.systemLanguage.ToString();
//            //change other data that you want
//        }

    }
}
