﻿using System;

namespace Ignite.AOM
{
    public interface ICrudService<E> where E : Entity
    {
        void Remove(E entity, Action success, Action<string> error);
    }

    public class CrudService<E> : AbstractService, ICrudService<E> where E : Entity
    {
        public void Remove(E entity, Action success, Action<string> error)
        {
            AwaitAndCallback(entity.DeleteAsync(), success, error);
        }
    }
}