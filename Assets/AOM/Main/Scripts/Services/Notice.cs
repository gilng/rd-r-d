﻿using Parse;
using System.Collections.Generic;
using System.Text;

namespace Ignite.AOM
{
    [ParseClassName("Notice")]
    public class Notice : ParseObject
    {
        [ParseFieldName("imageURL")]
        public string ImageURL
        {
            get { return GetProperty<string>("ImageURL"); }
        }

        [ParseFieldName("order")]
        public int Order
        {
            get { return GetProperty<int>("Order"); }
        }

        [ParseFieldName("text")]
        public IDictionary<string, string> Text
        {
            get { return GetProperty<IDictionary<string, string>>("Text"); }
        }

        [ParseFieldName("url")]
        public string URL
        {
            get { return GetProperty<string>("URL"); }
        }



        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(50);
            sb.AppendFormat("imageURL: {0}, ", ImageURL);
            sb.Append("text: {");
            foreach (KeyValuePair<string, string> entry in Text)
            {
                sb.AppendFormat("{0}: {1}, ", entry.Key, entry.Value);
            }
            sb.Append("}");
            return sb.ToString();
        }
    }
}
