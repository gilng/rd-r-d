﻿using Parse;
using System.Collections.Generic;
using System.Text;
using System;

namespace Ignite.AOM
{
    [ParseClassName("UserSummary")]
    public class UserSummary : ParseObject
    {
        [ParseFieldName("user")]
        public ParseUser User
        {
            get { return GetProperty<ParseUser>("User"); }
            set { SetProperty(value, "User"); }
        }

        [ParseFieldName("displayName")]
        public string DisplayName
        {
            get { return GetProperty<string>("DisplayName"); }
            set { SetProperty(value, "DisplayName"); }
        }

        [ParseFieldName("friendPoint")]
        public int FriendPoint
        {
            get { return GetProperty<int>("FriendPoint"); }
            set { SetProperty(value, "FriendPoint"); }
        }

        [ParseFieldName("friendLimit")]
        public int FriendLimit
        {
            get { return GetProperty<int>("FriendLimit"); }
            set { SetProperty(value, "FriendLimit"); }
        }

        [ParseFieldName("ruby")]
        public int Ruby
        {
            get { return GetProperty<int>("Ruby"); }
            set { SetProperty(value, "Ruby"); }
        }

        [ParseFieldName("gem")]
        public int Gem
        {
            get { return GetProperty<int>("Gem"); }
            set { SetProperty(value, "Gem"); }
        }

        [ParseFieldName("moecrystal")]
        public int Moecrystal
        {
            get { return GetProperty<int>("Moecrystal"); }
            set { SetProperty(value, "Moecrystal"); }
        }
    }
}
