﻿using System;

namespace Ignite.AOM
{
    [Obsolete("Return error code directly instead of Error class")]
    public class ServiceException : Exception
    {
        /// <summary>The error code associated with the exception.</summary>
        public ErrorCode Code { get; private set; }

        internal ServiceException(ErrorCode code, string message, Exception cause = null)
            : base(message, cause)
        {
            Code = code;
        }

        /// <summary>
        /// Error codes that may be delivered in response to requests to server.
        /// </summary>
        [Obsolete("Return error code directly instead of Error class")]
        public enum ErrorCode
        {
            InvalidSessionToken = 209,
            UserNotFound = 300,
            GoogleIdAlreadyLink = 301,
            FriendRelationAlreadyExists = 302,
            UserFriendListReachedMaximum = 303,
            TargetUserFriendListReachedMaximum = 304,
            InvaildTargetUserId = 305,
        }
    }
}