﻿using Parse;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Ignite.AOM
{
    public class NoticeService
    {
        private static NoticeService _instance;

        public static NoticeService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new NoticeService();
                }
                return _instance;
            }
        }

        public IEnumerator FindAll(Action<IList<Notice>> callback)
        {
            var query = new ParseQuery<Notice>();

            Task<IEnumerable<Notice>> t = query.FindAsync();

            while (!t.IsCompleted)
            {
                yield return null;
            }

            if (t.IsFaulted || t.IsCanceled)
            {
                foreach (ParseException err in t.Exception.InnerExceptions)
                {
                    //ParseException parseException = (ParseException) e;
                    Debug.LogError("error message: " + err.Message);
                    Debug.LogError("error code: " + err.Code);
                    Debug.LogError("error stacktrace: " + err.StackTrace);
                }
                callback(null);
            }
            else if (t.IsCompleted)
            {
                IEnumerable<Notice> result = t.Result;

                callback(result.ToList());
            }

        }
    }
}