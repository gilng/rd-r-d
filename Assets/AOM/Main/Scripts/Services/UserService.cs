﻿using Parse;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Ignite.AOM
{
    public interface IUserService
    {
        IUser CurrentUser { get; }
        UserSummary CurrentUserSummary { get; set; }

        bool ParseSessionTokenInvalid { set; get; }

        string LastSaveDateTime { get; }

        void HandleParseError(ServiceException e);

        void SignUp(string googleId, string googleEmail, Action<ServiceException, IUser> callback);
        void Logout(Action<ServiceException> callback);

        void LoginWithUserEmail(string userEmail, string password, Action<ServiceException> callback);
        void LinkUserEmail(string userEmail, string password, Action<ServiceException> callback);
        bool IsLinkedEmail();

        void LoginWithGoogleAuthCode(string authCode, Action<ServiceException> callback);
        void LinkGoogleAccount(string authCode, Action<ServiceException> callback);
        void UnlinkGoogleAccount();
        bool IsLinkedGoogle();

        void FindUserSummary(Action<UserSummary> callback);
        void UpdateUserDisplayName(string displayName, Action<UserSummary> callback);
        void UpdateFriendPoint(int additionalFp, Action<UserSummary> callback);
        void UpdateFriendLimit(int additionalFriendLimit, Action<UserSummary> callback);
        void UpdateRubyBalance(int additionalRuby, Action<UserSummary> callback);
        void UpdateGemBalance(int additionalGems, Action<UserSummary> callback);
        void UpdateMoecrystalBalance(int additionalMoeCrystals, Action<UserSummary> callback);
        void UpdateUserSummary(UserSummary userSummary, Action<ServiceException> callback);

        void SaveGameData(SaveData data, Action<ServiceException> callback);
        void LoadGameData(Action<ServiceException, SaveData> callback);

        void FetchGameDataUpdatedAt();
    }

    public class UserService : AbstractService, IUserService
    {
        public void HandleParseError(ServiceException e)
        {
            switch (e.Code)
            {
                case ServiceException.ErrorCode.InvalidSessionToken:
                    HandleInvalidSessionToken();
                    break;
            }
        }

        public readonly string PP_PARSE_SESSION_TOKEN_INVALID = "PP_PARSE_SESSION_TOKEN_INVALID";

        public bool ParseSessionTokenInvalid
        {
            get { return PlayerPrefs.GetInt(PP_PARSE_SESSION_TOKEN_INVALID, 0) > 0; }
            set { PlayerPrefs.SetInt(PP_PARSE_SESSION_TOKEN_INVALID, value ? 1 : 0); }
        }

        private static void HandleInvalidSessionToken()
        {
            Debug.Log("Invalid Session Token Handler");

            ParseUser.LogOutAsync();

            EventManager.ParseUserSessionTokenInvalid();
        }

        public readonly string PP_PARSE_GAME_DATA_UPDATEDAT = "PP_PARSE_GAME_DATA_UPDATEDAT";

        public string LastSaveDateTime
        {
            get { return PlayerPrefs.GetString(PP_PARSE_GAME_DATA_UPDATEDAT, ""); }
        }

        private IUser _currentUser;

        public IUser CurrentUser
        {
            get
            {
                if (_currentUser == null && ParseUser.CurrentUser != null)
                {
                    _currentUser = new ParseUserWrapper(ParseUser.CurrentUser);
                }

                return _currentUser;
            }
        }

        public UserSummary CurrentUserSummary { get; set; }

        #region ServerResponse

        [Serializable]
        public class ServerResponse
        {
            public ServiceException.ErrorCode code;
            public string message;
            public string email;
            public string googleId;
        }

        #endregion

        public void SignUp(string googleId, string googleEmail, Action<ServiceException, IUser> callback)
        {
            // TODO Add CurrentUser checking?

            string username = Guid.NewGuid().ToString();
            string password = Guid.NewGuid().ToString();

            var user = new ParseUser
            {
                Username = username,
                Password = password
            };
            //            Debug.Log(googleId);
            if (googleId != null)
            {
                user["googleUserId"] = googleId;
            }
            //            Debug.Log(googleEmail);
            if (googleEmail != null)
            {
                user["email"] = googleEmail;
            }
            //            Debug.Log(user["googleUserId"]);
            //            Debug.Log(user["email"]);
            Task signUpTask = user.SignUpAsync();

            ContinueWhenTaskCompleted(signUpTask, t =>
            {
                if (t.IsCanceled)
                {
                }
                else if (t.IsFaulted)
                {
                    ServiceException e = null;
                    if (t.Exception != null)
                    {
                        foreach (ParseException err in t.Exception.InnerExceptions)
                        {
                            //                            ParseException err = t.Exception.InnerException as ParseException;
                            e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                            HandleParseError(e);
                            Debug.LogErrorFormat("Sign up fail - {0}. Error Code: {1}", err.Message, err.Code);
                            Debug.LogException(err);
                        }
                    }

                    callback(e, null);
                }
                else if (t.IsCompleted)
                {
                    _currentUser = new ParseUserWrapper(user);

                    FindUserSummary(userSummaryData => { callback(null, _currentUser); });
                }
            });
        }

        public void Logout(Action<ServiceException> callback)
        {
            if (ParseUser.CurrentUser != null)
            {
                Task logoutTask = ParseUser.LogOutAsync();
                ContinueWhenTaskCompleted(logoutTask, t =>
                {
                    if (t.IsCanceled)
                    {
                    }
                    else if (t.IsFaulted)
                    {
                        ServiceException e = null;
                        if (t.Exception != null)
                        {
                            foreach (ParseException err in t.Exception.InnerExceptions)
                            {
                                //                                ParseException err = t.Exception.InnerException as ParseException;
                                e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                                HandleParseError(e);
                                Debug.LogErrorFormat("Logout user fail - {0}. Error Code: {1}", err.Message, err.Code);
                                Debug.LogException(err);
                            }
                        }

                        callback(e);
                    }
                    else if (t.IsCompleted)
                    {
                        callback(null);
                    }
                });
            }
        }

        public void LoginWithUserEmail(string userEmail, string password, Action<ServiceException> callback)
        {
            if (ParseUser.CurrentUser == null)
            {
                IDictionary<string, object> parms = new Dictionary<string, object>
                {
                    {"username", userEmail},
                    {"password", password}
                };

                Task clearSessionTask = ParseCloud.CallFunctionAsync<string>("clearSessionToken", parms);
                ContinueWhenTaskCompleted(clearSessionTask, task =>
                {
                    Debug.LogError(">> clearSessionToken: IsFaulted: " + task.IsFaulted + " - IsCanceled: " +
                                   task.IsCanceled + " - IsCompleted: " + task.IsCompleted + "\n");
                    if (task.IsCanceled)
                    {
                    }
                    else if (task.IsFaulted)
                    {
                        ServiceException e = null;
                        if (task.Exception != null)
                        {
                            foreach (ParseException err in task.Exception.InnerExceptions)
                            {
                                //                                ParseException err = task.Exception.InnerException as ParseException;
                                e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                                HandleParseError(e);
                                Debug.LogErrorFormat("clear session token fail - {0}. Error Code: {1}", err.Message,
                                    err.Code);
                                Debug.LogException(err);
                            }
                        }

                        callback(e);
                    }
                    else if (task.IsCompleted)
                    {
                        Task loginTask = ParseUser.LogInAsync(userEmail, password);
                        ContinueWhenTaskCompleted(loginTask, t =>
                        {
                            if (t.IsCanceled)
                            {
                            }
                            else if (t.IsFaulted)
                            {
                                ServiceException e = null;
                                if (t.Exception != null)
                                {
                                    foreach (ParseException err in t.Exception.InnerExceptions)
                                    {
                                        //                                        ParseException err = t.Exception.InnerException as ParseException;
                                        e =
                                            new ServiceException((ServiceException.ErrorCode) err.Code, err.Message,
                                                err);
                                        HandleParseError(e);
                                        Debug.LogErrorFormat("Login by email fail - {0}. Error Code: {1}", err.Message,
                                            err.Code);
                                        Debug.LogException(err);
                                    }
                                }

                                callback(e);
                            }
                            else if (t.IsCompleted)
                            {
                                FindUserSummary(userSummaryData => { callback(null); });
                            }
                        });
                    }
                });
            }
        }

        public void LinkUserEmail(string userEmail, string password, Action<ServiceException> callback)
        {
            if (ParseUser.CurrentUser != null)
            {
                if (!IsLinkedGoogle())
                {
                    ParseUser user = ParseUser.CurrentUser;

                    string currentUsername = user.Username;
                    user.Username = userEmail;

                    user.Password = password;

                    user["email"] = userEmail;

                    Task userUpdateTask = user.SaveAsync();
                    ContinueWhenTaskCompleted(userUpdateTask, t =>
                    {
                        if (t.IsCanceled)
                        {
                        }
                        else if (t.IsFaulted)
                        {
                            ServiceException e = null;
                            if (t.Exception != null)
                            {
                                foreach (ParseException err in t.Exception.InnerExceptions)
                                {
                                    //                                    ParseException err = t.Exception.InnerException as ParseException;
                                    e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                                    HandleParseError(e);
                                    Debug.LogErrorFormat("Link email fail - {0}. Error Code: {1}", err.Message,
                                        err.Code);
                                    Debug.LogException(err);
                                }
                            }

                            user.Username = currentUsername;
                            user.Remove("email");

                            callback(e);
                        }
                        else if (t.IsCompleted)
                        {
                            callback(null);
                        }
                    });
                }
            }
        }

        public bool IsLinkedEmail()
        {
            return ParseUser.CurrentUser.ContainsKey("email") &&
                   ParseUser.CurrentUser["email"].ToString() == ParseUser.CurrentUser.Username;
        }

        public void LoginWithGoogleAuthCode(string authCode, Action<ServiceException> callback)
        {
            IDictionary<string, object> parms = new Dictionary<string, object>
            {
                {"authCode", authCode}
            };

            Task<IDictionary<string, object>> getUserSessionTask =
                ParseCloud.CallFunctionAsync<IDictionary<string, object>>("getUserSessionToken", parms);

            ContinueWhenTaskCompleted(getUserSessionTask, t =>
            {
                if (t.IsCanceled)
                {
                }
                else if (t.IsFaulted)
                {
                    ServiceException e = null;
                    if (t.Exception != null)
                    {
                        foreach (ParseException err in t.Exception.InnerExceptions)
                        {
                            //                            ParseException err = t.Exception.InnerException as ParseException;
                            e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                            HandleParseError(e);
                            Debug.LogErrorFormat("Get session token fail - {0}. Error Code: {1}", err.Message,
                                err.Code);
                            //                            Debug.LogException(err);

                            ServerResponse response = JsonUtility.FromJson<ServerResponse>(err.Message);

                            //                            Debug.Log(response.code);
                            //                            Debug.Log(response.message);
                            //                            Debug.Log(response.googleId);
                            //                            Debug.Log(response.email);

                            if (response.code == ServiceException.ErrorCode.UserNotFound)
                            {
                                Debug.Log("user not found with auth code, signup new one instead");
                                SignUp(response.googleId, response.email, (e2, user) =>
                                {
                                    //                                    Debug.Log("signup");
                                    if (e2 != null)
                                    {
                                        Debug.LogErrorFormat("sign up fail - {0}. Error Code: {1}", e2.Message,
                                            e2.Code);

                                        callback(e);
                                    }
                                    else
                                    {
                                        Debug.Log("sign up success");

                                        FindUserSummary(userSummaryData => { callback(null); });
                                    }
                                });
                            }
                        }
                    }
                }
                else if (t.IsCompleted && t.Result != null)
                {
                    IDictionary<string, object> result = t.Result;
                    string sessionToken = result["sessionToken"] as string;
                    Task becomeUserTask = ParseUser.BecomeAsync(sessionToken);
                    ContinueWhenTaskCompleted(becomeUserTask, tk =>
                    {
                        if (tk.IsCanceled)
                        {
                        }
                        else if (tk.IsFaulted)
                        {
                            ServiceException e = null;
                            if (tk.Exception != null)
                            {
                                foreach (ParseException err in tk.Exception.InnerExceptions)
                                {
                                    //                                    ParseException err = t.Exception.InnerException as ParseException;
                                    e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                                    HandleParseError(e);
                                    Debug.LogErrorFormat("Become user fail - {0}. Error Code: {1}", err.Message,
                                        err.Code);
                                    Debug.LogException(err);
                                }
                            }

                            callback(e);
                        }
                        else if (tk.IsCompleted)
                        {
                            Debug.Log("Login with google success - Parse Username: " + ParseUser.CurrentUser.Username);

                            FindUserSummary(userSummaryData => { callback(null); });
                        }
                    });
                }
            });
        }

        public void LinkGoogleAccount(string authCode, Action<ServiceException> callback)
        {
            IDictionary<string, object> parms = new Dictionary<string, object>
            {
                {"authCode", authCode}
            };
            Task<string> linkGoogleIdTask = ParseCloud.CallFunctionAsync<string>("linkGoogleID", parms);
            ContinueWhenTaskCompleted(linkGoogleIdTask, t =>
            {
                if (t.IsCanceled)
                {
                }
                else if (t.IsFaulted)
                {
                    ServiceException e = null;
                    if (t.Exception != null)
                    {
                        foreach (ParseException err in t.Exception.InnerExceptions)
                        {
                            //                            ParseException err = t.Exception.InnerException as ParseException;
                            e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                            HandleParseError(e);
                            Debug.LogErrorFormat("Link google ID fail - {0}. Error Code: {1}", err.Message, err.Code);
                            Debug.LogException(err);
                        }
                    }

                    callback(e);
                }
                else if (t.IsCompleted)
                {
                    Debug.Log(t.Result);
                    Task fetchTask = ParseUser.CurrentUser.FetchAsync();
                    ContinueWhenTaskCompleted(fetchTask, tk =>
                    {
                        if (tk.IsFaulted || tk.IsCanceled)
                        {
                            ServiceException e = null;
                            if (tk.Exception != null)
                            {
                                foreach (ParseException err in tk.Exception.InnerExceptions)
                                {
                                    //                                    ParseException err = tk.Exception.InnerException as ParseException;
                                    e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                                    HandleParseError(e);
                                    Debug.LogErrorFormat("Fetch user fail - {0}. Error Code: {1}", err.Message,
                                        err.Code);
                                    Debug.LogException(err);
                                }
                            }

                            callback(e);
                        }
                        else if (tk.IsCompleted)
                        {
                            callback(null);
                        }
                    });
                }
            });
        }

        public void UnlinkGoogleAccount()
        {
            if (ParseUser.CurrentUser != null)
            {
                if (IsLinkedGoogle())
                {
                    ParseUser user = ParseUser.CurrentUser;
                    user.Remove("googleUserId");
                    //                    user.Remove("googleAccountEmail");
                    Task userUpdateTask = user.SaveAsync();
                    ContinueWhenTaskCompleted(userUpdateTask, t =>
                    {
                        if (t.IsCanceled)
                        {
                        }
                        else if (t.IsFaulted)
                        {
                            ServiceException e = null;
                            if (t.Exception != null)
                            {
                                foreach (ParseException err in t.Exception.InnerExceptions)
                                {
                                    //                                    ParseException err = t.Exception.InnerException as ParseException;
                                    e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                                    HandleParseError(e);
                                    Debug.LogErrorFormat("Unlink google ID fail - {0}. Error Code: {1}", err.Message,
                                        err.Code);
                                    Debug.LogException(err);
                                }
                            }
                        }
                    });
                }
            }
        }

        public bool IsLinkedGoogle()
        {
            return ParseUser.CurrentUser.ContainsKey("googleUserId");
        }

        public void FindUserSummary(Action<UserSummary> callback)
        {
            ParseQuery<UserSummary> userSummaryDataQuery = new ParseQuery<UserSummary>()
                .WhereEqualTo("user", ParseUser.CurrentUser);

            Task<UserSummary> loadUserSummaryDataTask = userSummaryDataQuery.FirstOrDefaultAsync();

            ContinueWhenTaskCompleted(loadUserSummaryDataTask, t =>
            {
                if (t.IsFaulted || t.IsCanceled)
                {
                    ServiceException e = null;
                    if (t.Exception != null)
                    {
                        foreach (ParseException err in t.Exception.InnerExceptions)
                        {
                            //                            ParseException err = t.Exception.InnerException as ParseException;
                            e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                            HandleParseError(e);
                            Debug.LogErrorFormat("Load user summary fail - {0}. Error Code: {1}", err.Message,
                                err.Code);
                            Debug.LogException(err);
                        }
                    }

                    callback(null);
                }
                else if (t.IsCompleted)
                {
                    CurrentUserSummary = null;

                    if (t.Result != null)
                    {
                        CurrentUserSummary = t.Result;
                    }
                    else
                    {
                        CurrentUserSummary = new UserSummary
                        {
                            User = ParseUser.CurrentUser,
                            DisplayName = "",
                            FriendPoint = 0,
                            FriendLimit = 5,
                            Ruby = 0,
                            Gem = AOMStoreInventory.GetDiamondBalance(),
                            Moecrystal = AOMStoreInventory.GetRelicBalance()
                        };

                        Task createUserSummaryTask = CurrentUserSummary.SaveAsync();

                        ContinueWhenTaskCompleted(createUserSummaryTask, t2 =>
                        {
                            if (t2.IsFaulted || t2.IsCanceled)
                            {
                                ServiceException e = null;
                                if (t2.Exception != null)
                                {
                                    foreach (ParseException err in t2.Exception.InnerExceptions)
                                    {
                                        //                                        ParseException err = t2.Exception.InnerException as ParseException;
                                        e =
                                            new ServiceException((ServiceException.ErrorCode) err.Code, err.Message,
                                                err);
                                        HandleParseError(e);
                                        Debug.LogErrorFormat("Save game data fail - {0}. Error Code: {1}", err.Message,
                                            err.Code);
                                        Debug.LogException(err);
                                    }
                                }
                            }
                        });
                    }

                    callback(CurrentUserSummary);
                }
            });
        }

        public void UpdateUserDisplayName(string displayName, Action<UserSummary> callback)
        {
            if (CurrentUserSummary != null)
            {
                CurrentUserSummary.DisplayName = displayName;
                UpdateUserSummary(CurrentUserSummary, (e) =>
                {
                    if (e != null)
                    {
                        Debug.LogError("Update User Display Name fail");
                        callback(null);
                    }
                    else
                    {
                        callback(CurrentUserSummary);
                    }
                });
            }
            else
            {
                FindUserSummary(userSummary =>
                {
                    CurrentUserSummary = userSummary;
                    if (CurrentUserSummary != null)
                    {
                        CurrentUserSummary.DisplayName = displayName;
                        UpdateUserSummary(CurrentUserSummary, (e) =>
                        {
                            if (e != null)
                            {
                                Debug.LogError("Update User Display Name fail");
                                callback(null);
                            }
                            else
                            {
                                callback(CurrentUserSummary);
                            }
                        });
                    }
                    else
                    {
                        Debug.LogError("Update User Display Name fail");
                        callback(null);
                    }
                });
            }
        }

        public void UpdateFriendPoint(int additionalFp, Action<UserSummary> callback)
        {
            if (CurrentUserSummary != null)
            {
                CurrentUserSummary.FriendPoint += additionalFp;

                UpdateUserSummary(CurrentUserSummary, (e) =>
                {
                    if (e != null)
                    {
                        Debug.LogError("Update Friend Point fail");
                        callback(null);
                    }
                    else
                    {
                        callback(CurrentUserSummary);
                    }
                });
            }
            else
            {
                FindUserSummary(userSummary =>
                {
                    CurrentUserSummary = userSummary;
                    if (CurrentUserSummary != null)
                    {
                        CurrentUserSummary.FriendPoint += additionalFp;
                        UpdateUserSummary(CurrentUserSummary, (e) =>
                        {
                            if (e != null)
                            {
                                Debug.LogError("Update Friend Point fail");
                                callback(null);
                            }
                            else
                            {
                                callback(CurrentUserSummary);
                            }
                        });
                    }
                    else
                    {
                        Debug.LogError("Update Friend Point fail");
                        callback(null);
                    }
                });
            }
        }

        public void UpdateFriendLimit(int additionalFriendLimit, Action<UserSummary> callback)
        {
            if (CurrentUserSummary != null)
            {
                CurrentUserSummary.FriendLimit += additionalFriendLimit;
                UpdateUserSummary(CurrentUserSummary, (e) =>
                {
                    if (e != null)
                    {
                        Debug.LogError("Update Friend Limit fail");
                        callback(null);
                    }
                    else
                    {
                        callback(CurrentUserSummary);
                    }
                });
            }
            else
            {
                FindUserSummary(userSummary =>
                {
                    CurrentUserSummary = userSummary;

                    if (CurrentUserSummary != null)
                    {
                        CurrentUserSummary.FriendLimit += additionalFriendLimit;
                        UpdateUserSummary(CurrentUserSummary, (e) =>
                        {
                            if (e != null)
                            {
                                Debug.LogError("Update Friend Limit fail");
                                callback(null);
                            }
                            else
                            {
                                callback(CurrentUserSummary);
                            }
                        });
                    }
                    else
                    {
                        Debug.LogError("Update Friend Limit fail");
                        callback(null);
                    }
                });
            }
        }

        public void UpdateRubyBalance(int additionalRuby, Action<UserSummary> callback)
        {
            if (CurrentUserSummary != null)
            {
                CurrentUserSummary.Ruby += additionalRuby;
                UpdateUserSummary(CurrentUserSummary, (e) =>
                {
                    if (e != null)
                    {
                        Debug.LogError("Update Ruby Balance fail");
                        callback(null);
                    }
                    else
                    {
                        callback(CurrentUserSummary);
                    }
                });
            }
            else
            {
                FindUserSummary(userSummary =>
                {
                    CurrentUserSummary = userSummary;

                    if (CurrentUserSummary != null)
                    {
                        CurrentUserSummary.Ruby += additionalRuby;
                        UpdateUserSummary(CurrentUserSummary, (e) =>
                        {
                            if (e != null)
                            {
                                Debug.LogError("Update Ruby Balance fail");
                                callback(null);
                            }
                            else
                            {
                                callback(CurrentUserSummary);
                            }
                        });
                    }
                    else
                    {
                        Debug.LogError("Update Ruby Balance fail");
                        callback(null);
                    }
                });
            }
        }

        public void UpdateGemBalance(int additionalGems, Action<UserSummary> callback)
        {
            if (CurrentUserSummary != null)
            {
                CurrentUserSummary.Gem += additionalGems;
                UpdateUserSummary(CurrentUserSummary, (e) =>
                {
                    if (e != null)
                    {
                        Debug.LogError("Update Gems Balance fail");
                        callback(null);
                    }
                    else
                    {
                        callback(CurrentUserSummary);
                    }
                });
            }
            else
            {
                FindUserSummary(userSummary =>
                {
                    CurrentUserSummary = userSummary;

                    if (CurrentUserSummary != null)
                    {
                        CurrentUserSummary.Gem += additionalGems;
                        UpdateUserSummary(CurrentUserSummary, (e) =>
                        {
                            if (e != null)
                            {
                                Debug.LogError("Update Gems Balance fail");
                                callback(null);
                            }
                            else
                            {
                                callback(CurrentUserSummary);
                            }
                        });
                    }
                    else
                    {
                        Debug.LogError("Update Gems Balance fail");
                        callback(null);
                    }
                });
            }
        }

        public void UpdateMoecrystalBalance(int additionalMoecrystals, Action<UserSummary> callback)
        {
            if (CurrentUserSummary != null)
            {
                CurrentUserSummary.Moecrystal += additionalMoecrystals;
                UpdateUserSummary(CurrentUserSummary, (e) =>
                {
                    if (e != null)
                    {
                        Debug.LogError("Update Moecrystals Balance fail");
                        callback(null);
                    }
                    else
                    {
                        callback(CurrentUserSummary);
                    }
                });
            }
            else
            {
                FindUserSummary(userSummary =>
                {
                    CurrentUserSummary = userSummary;

                    if (CurrentUserSummary != null)
                    {
                        CurrentUserSummary.Moecrystal += additionalMoecrystals;
                        UpdateUserSummary(CurrentUserSummary, (e) =>
                        {
                            if (e != null)
                            {
                                Debug.LogError("Update Moecrystals Balance fail");
                                callback(null);
                            }
                            else
                            {
                                callback(CurrentUserSummary);
                            }
                        });
                    }
                    else
                    {
                        Debug.LogError("Update Moecrystals Balance fail");
                        callback(null);
                    }
                });
            }
        }

        public void UpdateUserSummary(UserSummary userSummary, Action<ServiceException> callback)
        {
            if (userSummary != null)
            {
                Task updateFriendPointTask = userSummary.SaveAsync();

                ContinueWhenTaskCompleted(updateFriendPointTask, t =>
                {
                    if (t.IsFaulted || t.IsCanceled)
                    {
                        ServiceException e = null;
                        if (t.Exception != null)
                        {
                            foreach (ParseException err in t.Exception.InnerExceptions)
                            {
                                //                                        ParseException err = t2.Exception.InnerException as ParseException;
                                e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                                HandleParseError(e);
                                Debug.LogErrorFormat("Update UserSummary fail - {0}. Error Code: {1}", err.Message,
                                    err.Code);
                                Debug.LogException(err);
                            }
                        }

                        callback(e);
                    }
                    else if (t.IsCompleted)
                    {
                        callback(null);
                    }
                });
            }
        }

        public void SaveGameData(SaveData data, Action<ServiceException> callback)
        {
            if (data != null && ParseUser.CurrentUser != null)
            {
                ParseQuery<ParseObject> query = ParseObject.GetQuery("GameData")
                    .WhereEqualTo("user", ParseUser.CurrentUser)
                    .OrderBy("updatedAt"); //OrderByDescending
                Task<ParseObject> loadGameDataTask = query.FirstOrDefaultAsync();
                ContinueWhenTaskCompleted(loadGameDataTask, t =>
                {
                    if (t.IsCanceled)
                    {
                    }
                    else if (t.IsFaulted)
                    {
                        ServiceException e = null;
                        if (t.Exception != null)
                        {
                            foreach (ParseException err in t.Exception.InnerExceptions)
                            {
                                e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                                HandleParseError(e);
                                Debug.LogErrorFormat("Load data fail - {0}. Error Code: {1}", err.Message, err.Code);
                            }
                        }
                    }
                    else if (t.IsCompleted)
                    {
                        ParseObject gameData = null;
                        string dataJson = JsonUtility.ToJson(data);

#if UNITY_EDITOR
                        Debug.Log("SaveData: " + dataJson);
                        Debug.Log(t.Result);
#endif


                        if (t.Result != null)
                        {
                            gameData = t.Result;
                        }
                        else
                        {
                            gameData = new ParseObject("GameData");
                            gameData["user"] = ParseUser.CurrentUser;
                        }

                        gameData["data"] = dataJson;
                        Task saveTask = gameData.SaveAsync();

                        ContinueWhenTaskCompleted(saveTask, t2 =>
                        {
                            if (t2.IsCanceled)
                            {
                            }
                            else if (t2.IsFaulted)
                            {
                                ServiceException e = null;
                                if (t2.Exception != null)
                                {
                                    foreach (ParseException err in t2.Exception.InnerExceptions)
                                    {
                                        //                                        ParseException err = t2.Exception.InnerException as ParseException;
                                        e =
                                            new ServiceException((ServiceException.ErrorCode) err.Code, err.Message,
                                                err);
                                        HandleParseError(e);
                                        Debug.LogErrorFormat("Save game data fail - {0}. Error Code: {1}", err.Message,
                                            err.Code);
                                        Debug.LogException(err);
                                    }
                                }

                                callback(e);
                            }
                            else if (t.IsCompleted)
                            {
#if UNITY_EDITOR
                                Debug.Log("save data date: " + gameData.UpdatedAt);
#endif
                                PlayerPrefs.SetString(PP_PARSE_GAME_DATA_UPDATEDAT, gameData.UpdatedAt.ToString());
                                callback(null);
                            }
                        });
                    }
                });
            }
        }

        public void LoadGameData(Action<ServiceException, SaveData> callback)
        {
            if (ParseUser.CurrentUser != null)
            {
                ParseQuery<ParseObject> query = ParseObject.GetQuery("GameData")
                    .WhereEqualTo("user", ParseUser.CurrentUser)
                    .OrderBy("updatedAt"); //OrderByDescending
                Task<ParseObject> loadGameDataTask = query.FirstOrDefaultAsync();

                ContinueWhenTaskCompleted(loadGameDataTask, t =>
                {
                    if (t.IsCanceled)
                    {
                    }
                    else if (t.IsFaulted)
                    {
                        ServiceException e = null;
                        if (t.Exception != null)
                        {
                            foreach (ParseException err in t.Exception.InnerExceptions)
                            {
                                //                                ParseException err = t.Exception.InnerException as ParseException;
                                e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                                HandleParseError(e);
                                Debug.LogErrorFormat("Load Game Data fail - {0}. Error Code: {1}", err.Message,
                                    err.Code);
                                Debug.LogException(err);
                            }
                        }

                        callback(e, null);
                    }
                    else if (t.IsCompleted)
                    {
                        Debug.Log(t.Result);
                        if (t.Result != null)
                        {
                            ParseObject gameData = t.Result;
                            string dataJson = gameData.Get<string>("data");
                            Debug.Log("LoadData: " + dataJson);
                            SaveData data = null;
                            if (dataJson.Length > 0)
                            {
                                data = JsonUtility.FromJson<SaveData>(dataJson);
                            }

                            Debug.Log("load data date: " + gameData.UpdatedAt);
                            PlayerPrefs.SetString(PP_PARSE_GAME_DATA_UPDATEDAT, gameData.UpdatedAt.ToString());

                            callback(null, data);
                        }
                        else
                        {
                            Debug.Log("null game data from parse");
                            callback(null, null);
                        }
                    }
                });
            }
        }

        public void FetchGameDataUpdatedAt()
        {
            if (ParseUser.CurrentUser != null)
            {
                ParseQuery<ParseObject> query = ParseObject.GetQuery("GameData")
                    .WhereEqualTo("user", ParseUser.CurrentUser)
                    .OrderBy("updatedAt"); //OrderByDescending
                Task<ParseObject> loadGameDataTask = query.FirstOrDefaultAsync();

                ContinueWhenTaskCompleted(loadGameDataTask, t =>
                {
                    if (t.IsCanceled)
                    {
                    }
                    else if (t.IsFaulted)
                    {
                        ServiceException e = null;
                        if (t.Exception != null)
                        {
                            foreach (ParseException err in t.Exception.InnerExceptions)
                            {
                                //                                ParseException err = t.Exception.InnerException as ParseException;
                                e = new ServiceException((ServiceException.ErrorCode) err.Code, err.Message, err);
                                HandleParseError(e);
                                Debug.LogErrorFormat("Fetch Game Data UpdatedAt fail - {0}. Error Code: {1}",
                                    err.Message, err.Code);
                                Debug.LogException(err);
                            }
                        }
                    }
                    else if (t.IsCompleted)
                    {
                        Debug.Log(t.Result);
                        if (t.Result != null)
                        {
                            ParseObject gameData = t.Result;
                            Debug.Log("load data date: " + gameData.UpdatedAt);
                            PlayerPrefs.SetString(PP_PARSE_GAME_DATA_UPDATEDAT, gameData.UpdatedAt.ToString());
                        }
                    }
                });
            }
        }
    }
}