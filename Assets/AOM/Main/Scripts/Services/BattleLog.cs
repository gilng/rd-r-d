﻿using Parse;
using System;

namespace Ignite.AOM
{
    [ParseClassName("BattleLog")]
    public class BattleLog : Entity
    {
        [ParseFieldName("sender")]
        public ParseUser Sender
        {
            get { return GetProperty<ParseUser>("Sender"); }
            set { SetProperty(value, "Sender"); }
        }

        [ParseFieldName("receiver")]
        public ParseUser Receiver
        {
            get { return GetProperty<ParseUser>("Receiver"); }
            set { SetProperty(value, "Receiver"); }
        }

        [ParseFieldName("senderScore")]
        public int SenderScore
        {
            get { return GetProperty<int>("SenderScore"); }
            set { SetProperty(value, "SenderScore"); }
        }

        [ParseFieldName("receiverScore")]
        public int ReceiverScore
        {
            get { return GetProperty<int>("ReceiverScore"); }
            set { SetProperty(value, "ReceiverScore"); }
        }

        [ParseFieldName("senderWin")]
        public bool SenderWin
        {
            get { return GetProperty<bool>("SenderWin"); }
            set { SetProperty(value, "SenderWin"); }
        }

        [ParseFieldName("senderClaim")]
        public bool SenderClaim
        {
            get { return GetProperty<bool>("SenderClaim"); }
            set { SetProperty(value, "SenderClaim"); }
        }

        [ParseFieldName("receiverClaim")]
        public bool ReceiverClaim
        {
            get { return GetProperty<bool>("ReceiverClaim"); }
            set { SetProperty(value, "ReceiverClaim"); }
        }

        [ParseFieldName("expired")]
        public bool Expired
        {
            get { return GetProperty<bool>("Expired"); }
            set { SetProperty(value, "Expired"); }
        }

        [ParseFieldName("receivedAt")]
        public DateTime ReceivedAt
        {
            get { return GetProperty<DateTime>("ReceivedAt"); }
            set { SetProperty(value, "ReceivedAt"); }
        }
    }
}
