﻿using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Ignite.AOM
{
    public abstract class AbstractService
    {
        private static readonly ILogger Logger = Debug.logger;
        private const float DefaultTimeOut = 30f; // TODO Allow subclass to override

        private AsyncProcessor _asyncProcessor;

        [Inject]
        public void Construct(AsyncProcessor asyncProcessor)
        {
            _asyncProcessor = asyncProcessor;
        }

        /// <summary>
        /// As Parse Async call run in another thread, Unity call like LoadLevel cannot be used in Async callback.
        /// Therefore, we need to use Coroutine to wait for the Parse Async Task to complete
        /// and then execute the callback in main thread.
        /// </summary>
        protected void AwaitAndCallback(Task t, Action success, Action<string> error)
        {
            _asyncProcessor.StartCoroutine(AwaitAndCallbackCoroutine(t, success, error));
        }

        protected void AwaitAndCallback<T>(Task<T> t, Action<T> success, Action<string> error)
        {
            _asyncProcessor.StartCoroutine(AwaitAndCallbackCoroutine(t, success, error));
        }

        private static IEnumerator AwaitAndCallbackCoroutine(Task t, Action success,
            Action<string> error)
        {
            var elapsedTime = 0f;

            var isTimeOut = false;

            while (!t.IsCompleted)
            {
                elapsedTime += Time.deltaTime;
                if (elapsedTime >= DefaultTimeOut)
                {
                    isTimeOut = true;
                    break;
                }

                yield return null;
            }

            if (isTimeOut)
            {
                error(ErrorCodes.TimeOut);
            }
            else if (!HasError(t, error))
            {
                success();
            }
        }

        private static IEnumerator AwaitAndCallbackCoroutine<T>(Task<T> t, Action<T> success,
            Action<string> error)
        {
            var elapsedTime = 0f;

            var isTimeOut = false;

            while (!t.IsCompleted)
            {
                elapsedTime += Time.deltaTime;
                if (elapsedTime >= DefaultTimeOut)
                {
                    isTimeOut = true;
                    break;
                }

                yield return null;
            }

            if (isTimeOut)
            {
                error(ErrorCodes.TimeOut);
            }
            else if (!HasError(t, error))
            {
                success(t.Result);
            }
        }

        private static bool HasError(Task t, Action<string> error)
        {
            var hasError = false;

            if (t.IsCanceled)
            {
                error(ErrorCodes.Cancelled);
                hasError = true;
            }
            else if (t.IsFaulted)
            {
                var errorCode = ErrorCodes.Unknown;

                if (t.Exception != null && t.Exception.InnerExceptions.First() != null)
                {
                    var ex = t.Exception.InnerExceptions.First();
                    Logger.LogError("Abstract Service", ex);
                    errorCode = ex.Message;
                }
                error(errorCode);
                hasError = true;
            }

            return hasError;
        }

        // TODO Refractor the referenced code to use AwaitAndCallBack instead
        [Obsolete("Use AwaitAndCallback<T>(Task<T> t, Action<Task<T>> success, Action<string> error) instead")]
        protected void ContinueWhenTaskCompleted(Task t, Action<Task> callback)
        {
            _asyncProcessor.StartCoroutine(ContinueWhenTaskCompletedCoroutine(t, callback));
        }

        [Obsolete("Use AwaitAndCallback<T>(Task<T> t, Action<Task<T>> success, Action<string> error) instead")]
        protected void ContinueWhenTaskCompleted<T>(Task<T> t, Action<Task<T>> callback)
        {
            _asyncProcessor.StartCoroutine(ContinueWhenTaskCompletedCoroutine(t, callback));
        }

        [Obsolete(
             "Use ContinueWhenTaskCompletedCoroutine<T>(Task<T> t, Action<Task<T>> success, Action<string> error) instead"
         )]
        private static IEnumerator ContinueWhenTaskCompletedCoroutine(Task t, Action<Task> callback)
        {
            while (!t.IsCompleted)
            {
                yield return null;
            }
            callback(t);
        }

        [Obsolete(
             "Use ContinueWhenTaskCompletedCoroutine<T>(Task<T> t, Action<Task<T>> success, Action<string> error) instead"
         )]
        private static IEnumerator ContinueWhenTaskCompletedCoroutine<T>(Task<T> t, Action<Task<T>> callback)
        {
            while (!t.IsCompleted)
            {
                yield return null;
            }
            callback(t);
        }
    }
}