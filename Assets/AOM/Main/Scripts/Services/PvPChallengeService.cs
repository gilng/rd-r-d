﻿using Parse;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace Ignite.AOM
{
    public interface IPvPChallengeService : ICrudService<BattleLog>
    {
        ParseUser SelectedFriendUser { get; set; }
        string SelectedFriendDisplayName { get; set; }
        List<ParseUser> SelectedFriendUsers { get; set; }

        BattleLog SelectedChallenge { get; set; }

        bool IsSenderInCurrentChallenge { get; set; }
        bool ChallengeCompleted { get; set; }
        void InitChallenge(ParseUser friendUser, string displayName, BattleLog challengeRecord, Action initCallback);
        void InitMultipleChallenge(IList<ParseUser> friendUsers, Action initCallback);

        List<BattleLog> LatestSentChallengeList { get; set; }
        void FindLatestBattleLogPerFriend(Action<IList<BattleLog>> success, Action<string> error);
        void FindLatestSentByFriendUser(ParseUser friendUser, Action<BattleLog> success, Action<string> error);

        int ReceivedChallengeCount { get; set; }
        List<BattleLog> ReceivedChallengeList { get; set; }
        void FindAllReceivedChallenge(Action<IList<BattleLog>> success, Action<string> error);

        void ChallengeMultipleFriends(IList<ParseUser> selectedFriends, int score, Action success, Action<string> error);
        void ChallengeFriend(int score, Action success, Action<string> error);
        void AcceptChallenge(int score, Action success, Action<string> error);

        void ResetSelectedData();

        void AcceptChallengeResult(string recordId, bool isSender, Action success, Action<string> error);
        void SetChallengeExpired(string recordId, Action success, Action<string> error);
    }

    public class PvPChallengeService : CrudService<BattleLog>, IPvPChallengeService
    {
        public ParseUser SelectedFriendUser { get; set; }
        public string SelectedFriendDisplayName { get; set; }
        public List<ParseUser> SelectedFriendUsers { get; set; }

        public BattleLog SelectedChallenge { get; set; }

        public bool IsSenderInCurrentChallenge { get; set; }
        public bool ChallengeCompleted { get; set; }

        public void InitChallenge(ParseUser friendUser, string displayName, BattleLog challengeRecord, Action initCallback)
        {
            SelectedFriendUser = friendUser;
            SelectedFriendDisplayName = displayName;
            SelectedChallenge = challengeRecord;

            IsSenderInCurrentChallenge = SelectedChallenge == null;

            initCallback();
        }

        public void InitMultipleChallenge(IList<ParseUser> friendUsers, Action initCallback)
        {
            SelectedFriendUsers = new List<ParseUser>();
            SelectedFriendUsers.AddRange(friendUsers);
            SelectedFriendDisplayName = "pvp_allFriend";
            IsSenderInCurrentChallenge = true;

            initCallback();
        }

        public List<BattleLog> LatestSentChallengeList { get; set; }
        public void FindLatestBattleLogPerFriend(Action<IList<BattleLog>> success, Action<string> error)
        {
            var findLatestBattleLogTask = ParseCloud.CallFunctionAsync<IList<BattleLog>>(
                "findLatestBattleLogPerFriend", null);

            AwaitAndCallback(findLatestBattleLogTask, result =>
            {
                if(LatestSentChallengeList != null)
                    LatestSentChallengeList.Clear();

                LatestSentChallengeList = new List<BattleLog>();
                LatestSentChallengeList.AddRange(result.ToList());
                success(LatestSentChallengeList);
            }, error);
        }

        public void FindLatestSentByFriendUser(ParseUser friendUser, Action<BattleLog> success, Action<string> error)
        {
            var latestSentDataQuery = new ParseQuery<BattleLog>()
                .WhereEqualTo("sender", ParseUser.CurrentUser)
                .WhereEqualTo("receiver", friendUser)
                .OrderByDescending("createdAt");

            var loadLatestSentDataTask = latestSentDataQuery.FirstOrDefaultAsync();

            AwaitAndCallback(loadLatestSentDataTask, success, error);
        }

        public int ReceivedChallengeCount { get; set; }
        public List<BattleLog> ReceivedChallengeList { get; set; }
        public void FindAllReceivedChallenge(Action<IList<BattleLog>> success, Action<string> error)
        {
            var receivedChallengeDataQuery = new ParseQuery<BattleLog>()
                .WhereEqualTo("receiver", ParseUser.CurrentUser)
                .WhereEqualTo("receiverClaim", false)
                .OrderBy("createdAt");

            var loadReceivedChallengeDataTask = receivedChallengeDataQuery.FindAsync();

            AwaitAndCallback(loadReceivedChallengeDataTask, result =>
            {
                if(ReceivedChallengeList != null)
                    ReceivedChallengeList.Clear();

                ReceivedChallengeList = new List<BattleLog>();

                foreach (BattleLog challenge in result.ToList())
                {
                    if((!challenge.Expired && challenge.ReceivedAt == DateTime.MinValue) || (!challenge.Expired && challenge.ReceivedAt != DateTime.MinValue) || (challenge.Expired && challenge.ReceivedAt != DateTime.MinValue))
                        ReceivedChallengeList.Add(challenge);
                }

                ReceivedChallengeCount = ReceivedChallengeList.Count;
                success(ReceivedChallengeList);
            }, error);
        }

        public void ChallengeMultipleFriends(IList<ParseUser> selectedFriends, int score, Action success,
            Action<string> error)
        {
            if (selectedFriends.Count > 0 && ParseUser.CurrentUser != null)
            {
                List<BattleLog> battleDataList = new List<BattleLog>();

                foreach (ParseUser friend in selectedFriends)
                {
                    BattleLog battleData = new BattleLog
                    {
                        Sender = ParseUser.CurrentUser,
                        Receiver = friend,
                        SenderScore = score,
                        ReceiverScore = 0,
                        SenderWin = false,
                        ReceivedAt = DateTime.MinValue,
                        SenderClaim = false,
                        ReceiverClaim = false,
                        Expired = false
                    };

                    battleDataList.Add(battleData);
                }

                Task sendChallengeListTask = battleDataList.SaveAllAsync();

                AwaitAndCallback(sendChallengeListTask, () =>
                {
                    ResetSelectedData();
                    success();
                }, error);
            }
        }

        public void ChallengeFriend(int score, Action success, Action<string> error)
        {
            if (SelectedFriendUser != null && ParseUser.CurrentUser != null)
            {
                ParseObject battleData = null;

                battleData = new BattleLog
                {
                    Sender = ParseUser.CurrentUser,
                    Receiver = SelectedFriendUser,
                    SenderScore = score,
                    ReceiverScore = 0,
                    SenderWin = false,
                    ReceivedAt = DateTime.MinValue,
                    SenderClaim = false,
                    ReceiverClaim = false,
                    Expired = false
                };

                Task sendChallengeTask = battleData.SaveAsync();

                AwaitAndCallback(sendChallengeTask, () =>
                {
                    ResetSelectedData();
                    success();
                }, error);
            }
        }

        public void AcceptChallenge(int score, Action success, Action<string> error)
        {
            if (SelectedFriendUser != null && ParseUser.CurrentUser != null)
            {
                if (SelectedChallenge != null)
                {
                    SelectedChallenge.ReceivedAt = DateTime.UtcNow;
                    SelectedChallenge.ReceiverScore = score;

                    if (int.Parse(SelectedChallenge.ReceiverScore.ToString()) >
                        int.Parse(SelectedChallenge.SenderScore.ToString()))
                        SelectedChallenge.SenderWin = false;
                    else
                        SelectedChallenge.SenderWin = true;

                    Task accpetChallengeTask = SelectedChallenge.SaveAsync();

                    AwaitAndCallback(accpetChallengeTask, () =>
                    {
                        //ResetSelectedData();
                        success();
                    }, error);
                }
                else
                {
                    error("Accept challenge fail - Invaild targetUser. Error Code: 305");
                }
            }
        }

        public void ResetSelectedData()
        {
            SelectedFriendUser = null;
            SelectedFriendDisplayName = "";

            if (SelectedFriendUsers != null)
            {
                SelectedFriendUsers.Clear();
                SelectedFriendUsers = null;
            }

            SelectedChallenge = null;
            IsSenderInCurrentChallenge = false;
        }

        public void AcceptChallengeResult(string recordId, bool isSender, Action success, Action<string> error)
        {
            if (ParseUser.CurrentUser != null)
            {
                var query = new ParseQuery<BattleLog>()
                    .WhereEqualTo("objectId", recordId);

                var loadChallengeRecordTask = query.FirstOrDefaultAsync();

                AwaitAndCallback(loadChallengeRecordTask, () =>
                {
                    BattleLog record = null;
                    if (loadChallengeRecordTask.Result != null)
                    {
                        record = loadChallengeRecordTask.Result;

                        if (isSender)
                            record.SenderClaim = true;
                        else
                            record.ReceiverClaim = true;

                        Task accpetChallengeResultTask = record.SaveAsync();

                        AwaitAndCallback(accpetChallengeResultTask, success, error);
                    }
                    else
                    {
                        error("Accept challenge result fail - Invaild targetUser. Error Code: 305");
                    }
                }, error);
            }
        }

        public void SetChallengeExpired(string recordId, Action success, Action<string> error)
        {
            if (ParseUser.CurrentUser != null)
            {
                ParseQuery<BattleLog> query = new ParseQuery<BattleLog>()
                    .WhereEqualTo("objectId", recordId);

                Task<BattleLog> loadChallengeRecordTask = query.FirstOrDefaultAsync();

                AwaitAndCallback(loadChallengeRecordTask, () =>
                {
                    BattleLog record = null;
                    if (loadChallengeRecordTask.Result != null)
                    {
                        record = loadChallengeRecordTask.Result;

                        record.Expired = true;

                        Task setChallengeExpiredTask = record.SaveAsync();

                        AwaitAndCallback(setChallengeExpiredTask, success, error);
                    }
                    else
                    {
                        error("Set challenge expired result fail - Invaild targetUser. Error Code: 305");
                    }
                }, error);
            }
        }
    }
}