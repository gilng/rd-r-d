using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Zenject;

namespace Ignite.AOM
{
    public class HeroManager : MonoBehaviour
    {
        private UIViewLoader _uiViewLoader;
        private HeroCore _heroCore;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader, HeroCore heroCore)
        {
            _uiViewLoader = uiViewLoader;
            _heroCore = heroCore;
        }

        public static HeroManager instance;
        Vector3 originalCameraPosition;
        float shakeAmt = 3.0f;
        public Camera mainCamera;
        public GameObject[] heros;
        //public GameObject heroSkillMenu;
        private float timer = 0;
        private int heroIdxForDeadMenu = -1;
        //public HeroDeadMenuManager heroDeadMenu;

        // Use this for Awake
        void Awake()
        {
            instance = this;
            originalCameraPosition = mainCamera.transform.position;
        }

        public void init()
        {
            EventManager.OnGoldPriceUpdated += RenderHero;
            EventManager.OnPrestigeComplete += RenderHero;
            EventManager.OnHeroDead += OnHeroDead;
            EventManager.OnHeroRevive += OnHeroRevive;
            EventManager.OnEnemyTypeChange += ShowHeroDeadMenu;
            EventManager.OnHeroLevelUp += OnHeroLevelUp;
            EventManager.OnHeroUnlockSkill += OnHeroUnlockSkill;

            RenderHero();
        }

        void OnDestroy()
        {
            EventManager.OnGoldPriceUpdated -= RenderHero;
            EventManager.OnPrestigeComplete -= RenderHero;
            EventManager.OnHeroDead -= OnHeroDead;
            EventManager.OnHeroRevive -= OnHeroRevive;
            EventManager.OnEnemyTypeChange -= ShowHeroDeadMenu;
            EventManager.OnHeroLevelUp -= OnHeroLevelUp;
            EventManager.OnHeroUnlockSkill -= OnHeroUnlockSkill;
        }

        //Camera shake effect for heavenly strike
        void CameraShake()
        {
            if (shakeAmt > 0)
            {
                float quakeAmt = UnityEngine.Random.value * shakeAmt * 2 - shakeAmt;
                Vector3 pp = mainCamera.transform.position;
                pp.y += quakeAmt; // can also add to x and/or z
                mainCamera.transform.position = pp;
            }
        }

        void StopShaking()
        {
            CancelInvoke("CameraShake");
            mainCamera.transform.position = originalCameraPosition;
        }

        void ShowHeroDeadMenu(EnemyType t)
        {
            if (heroIdxForDeadMenu != -1 && !_heroCore.HeroAlive[heroIdxForDeadMenu])
            {
                GameObject heroReviveMenuView = _uiViewLoader.LoadOverlayView("HeroReviveMenuView", null, false, false);
                heroReviveMenuView.GetComponent<HeroDeadViewController>().SetHeroIdx(heroIdxForDeadMenu);

                heroIdxForDeadMenu = -1;
            }
        }

        void FixedUpdate()
        {
            timer += Time.deltaTime;
            if (timer >= 1)
            {
                List<int> idxs = new List<int>(_heroCore.HeroReviveTime.Keys);
                for (int i = 0; i < idxs.Count; i++)
                {
                    DateTime defaultTime = DateTime.Now;
                    //					Debug.Log ( defaultTime );
                    defaultTime = defaultTime.AddHours(1);
                    _heroCore.HeroReviveTime.TryGetValue(idxs[i], out defaultTime);
                    //					Debug.Log ( "2 " +defaultTime );
                    if ((defaultTime - DateTime.Now).TotalSeconds <= 0)
                    {
                        //						Debug.Log ( defaultTime + " Revive Hero from Man");
                        _heroCore.ReviveHero(idxs[i]);
                    }
                }
                timer = 0;
            }
        }

        public void SetHeroesIdel()
        {
            for (int i = 0; i < heros.Length; i++)
            {
                GameObject hero = heros[i];
                if (hero != null && hero.activeInHierarchy)
                {
                    Hero he = (Hero)hero.GetComponent(typeof(Hero));
                    he.Idle();
                }
            }
        }

        void OnHeroDead(int heroIdx, DateTime reviveTime)
        {
            shakeAmt = 0.3f;
            InvokeRepeating("CameraShake", 0, .01f);
            Invoke("StopShaking", 0.3f);

            Hero he = (Hero)heros[heroIdx].GetComponent(typeof(Hero));
            he.Die();
            he.PlayDeadEffect();
            heroIdxForDeadMenu = heroIdx;
        }

        void OnHeroRevive(int heroIdx)
        {
            Hero he = (Hero)heros[heroIdx].GetComponent(typeof(Hero));
            he.Revive();
        }

        void OnHeroLevelUp(string heroId, int level)
        {
            Hero he = (Hero)heros[int.Parse(heroId.Replace("HE", "")) - 1].GetComponent(typeof(Hero));
            if (level == 1)
                he.PlaySpawnEffect();
            else
                he.PlayLevelUpEffect();
        }

        void OnHeroUnlockSkill(string heroId, string skillId)
        {
            Hero he = (Hero)heros[int.Parse(heroId.Replace("HE", "")) - 1].GetComponent(typeof(Hero));
            he.PlayUnlockSkillEffect();
        }

        void RenderHero()
        {
            for (int i = 0; i < HeroAssets.Heroes.Count; i++)
            {
                int currentLevel = HeroAssets.Heroes[i].GetBalance();
                Hero he = (Hero)heros[i].GetComponent(typeof(Hero));
                if (currentLevel > 0)
                {
                    he.SetLevel(currentLevel);
                    if (!heros[i].activeInHierarchy)
                    {
                        heros[i].SetActive(true);
                        he.StartAttack(HeroAssets.Heroes[i].ItemId, HeroAssets.Heroes[i].baseCost);
                    }
                    if (!_heroCore.HeroAlive[i])
                    {
                        he.Die();
                    }
                }
                else
                {
                    he.SetLevel(currentLevel);
                    heros[i].SetActive(false);
                }
            }
        }

        public void DisplayHeroSkillMenu(int idx)
        {
            GameObject heroSkillDetailView = _uiViewLoader.LoadOverlayView("HeroSkillMenuView", null, false, false);
            heroSkillDetailView.GetComponent<HeroSkillDetailViewController>().SetHero(idx);
        }
    }
}