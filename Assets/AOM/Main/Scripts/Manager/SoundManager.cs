﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using SmartLocalization;

namespace Ignite.AOM
{
    public enum SoundType
    {
        AvatarAttack_1 = 0,
        HeroSpawn_1,
        HeroLvUp_1,

        EnemyDead_1,
        EnemySpawn_1,
        EnemyTakeDamage_1,
        EnemyTakeDamage_2,
        EnemyExplosion,

        BossSpawn_1,
        BossTakeDamage_1,
        BossTakeDamage_2,
        BossTakeDamage_3,
        BossTakeDamage_4,
        BossTakeDamage_5,
        BossTakeDamage_6,
        BossTakeDamage_7,
        BossTakeDamage_8,
        BossDead_1,
        BossDead_2,

        boss_Clothes_Ripped_Snap,
        boss_Clothes_Ripped_Snap_Last,

        ButtonTouch_1,
        PopupShow_1,
        CoinCollect_1,
        TreasureCollect_1,

        SelectGirl_1,
        HandCaress_1,
        HeartBubble_1,
        LoveLevelUp_1,

        DiamondCollected,
        GachaResult,

        powerOfHolding,
        doom,

        BossTakeDamage_bee_1,
        BossTakeDamage_bird_1,
        BossTakeDamage_cat_1,
        BossTakeDamage_chimera_1,
        BossTakeDamage_cow_1,
        BossTakeDamage_dog_1,
        BossTakeDamage_flower_1,
        BossTakeDamage_goblin_1,
        BossTakeDamage_golem_1,
        BossTakeDamage_unicorn_1,

        MiniBossTakeDamage_bee_1,
        MiniBossTakeDamage_bird_1,
        MiniBossTakeDamage_cat_1,
        MiniBossTakeDamage_chimera_1,
        MiniBossTakeDamage_cow_1,
        MiniBossTakeDamage_dog_1,
        MiniBossTakeDamage_flower_1,
        MiniBossTakeDamage_goblin_1,
        MiniBossTakeDamage_golem_1,
        MiniBossTakeDamage_unicorn_1,

        trigger,
        hero_killed,

        bgm,
        bgm_storyIntro_1,
        bgm_storyIntro_2n3,

        BossTakeDamage_devil_1_1,
        MiniBossTakeDamage_devil_1_1,

        BossTakeDamage_devil_2_1,
        MiniBossTakeDamage_devil_2_1,

        BossTakeDamage_devil_3_1,
        MiniBossTakeDamage_devil_3_1,

        BossTakeDamage_mink,
        BossTakeDamage_rabbit_1,
        BossTakeDamage_rabbit_2,

        BossTakeDamage_jellyfish,
        BossTakeDamage_mermaid,
        BossTakeDamage_kraken,

        BossTakeDamage_twins,

        BossTakeDamage_skydog,

        BossTakeDamage_fox,

        DiamondUsed,

        Purchase_CashierBell,

        BossTakeDamage_greenlight,
        BossTakeDamage_pumpkin,

		bgm_pvp,
		BossTakeDamage_PVP_1,

        BossTakeDamage_Ittan,

        GainFP,

        BossTakeDamage_Tofu,
        BossTakeDamage_Marionette

    }

    [Serializable]
    public struct SoundDictionary
    {
        public SoundType type;
        public AudioClip audio;
        public float volume;
    }

    public class SoundManager : MonoBehaviour
    {
        public static string PP_BGM_SETTING = "PP_BGM_SETTING";
        public static string PP_SFX_SETTING = "PP_SFX_SETTING";

        private static SoundManager _instance;
        public static SoundManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public bool isSoundOn = true;
        public AudioSource bgm;

        public SoundDictionary[] soundDictionary;
        Dictionary<SoundType, AudioClip> soundList_audioClip = new Dictionary<SoundType, AudioClip>();
        Dictionary<SoundType, float> soundList_volume = new Dictionary<SoundType, float>();

        public SoundDictionary[] soundDictionary_BGM;
        Dictionary<SoundType, AudioClip> soundList_audioClip_BGM = new Dictionary<SoundType, AudioClip>();
        Dictionary<SoundType, float> soundList_volume_BGM = new Dictionary<SoundType, float>();



        // Use this for initialization
        void Start()
        {
            isSoundOn = PlayerPrefs.GetInt(SoundManager.PP_SFX_SETTING, 1) == 0 ? false : true;
            if (bgm != null)
                bgm.mute = PlayerPrefs.GetInt(SoundManager.PP_BGM_SETTING, 0) == 0 ? false : true;

            // Mapping
            foreach (SoundDictionary sd in soundDictionary)
            {
                if (soundList_audioClip.ContainsKey(sd.type) == false)
                    soundList_audioClip.Add(sd.type, sd.audio);
                else
                    Debug.Log("Sound Manager : (audioClip) Repeat key - " + sd.type.ToString());

                if (soundList_volume.ContainsKey(sd.type) == false)
                    soundList_volume.Add(sd.type, sd.volume);
                else
                    Debug.Log("Sound Manager : ( volume  ) Repeat key - " + sd.type.ToString());
            }

            // Mapping - BGM
            foreach (SoundDictionary sd in soundDictionary_BGM)
            {
                if (soundList_audioClip_BGM.ContainsKey(sd.type) == false)
                    soundList_audioClip_BGM.Add(sd.type, sd.audio);
                else
                    Debug.Log("Sound Manager (BGM) : (audioClip) Repeat key - " + sd.type.ToString());

                if (soundList_volume_BGM.ContainsKey(sd.type) == false)
                    soundList_volume_BGM.Add(sd.type, sd.volume);
                else
                    Debug.Log("Sound Manager (BGM) : ( volume  ) Repeat key - " + sd.type.ToString());
            }
        }

        void Awake()
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        private SoundManager()
        {
            _instance = this;
        }

        public void PlaySoundWithType(SoundType type)
        {
            bool isLoading = GameManager.Instance != null ? GameManager.Instance.loadingScene.activeSelf : false;

            if (!isLoading)
            {
                if (soundList_audioClip.ContainsKey(type) && soundList_volume.ContainsKey(type))
                    if (isSoundOn)
                        GetComponent<AudioSource>().PlayOneShot(soundList_audioClip[type], soundList_volume[type]);
            }
        }

        public void PlayBgmWithType(SoundType type)
        {
            bool isLoading = GameManager.Instance != null ? GameManager.Instance.loadingScene.activeSelf : false;

            if (!isLoading)
            {
                bgm.clip = soundList_audioClip_BGM[type];
                bgm.volume = soundList_volume_BGM[type];
                bgm.Play();
            }
        }

        public void StopSound()
        {
            GetComponent<AudioSource>().Stop();
        }

        public void SwitchSoundOnOff(Text t)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            isSoundOn = isSoundOn ? false : true;

            PlayerPrefs.SetInt(SoundManager.PP_SFX_SETTING, isSoundOn ? 1 : 0);

            if (isSoundOn)
            {
                t.text = LanguageManager.Instance.GetTextValue("setting_menu_sound") + " : " + LanguageManager.Instance.GetTextValue("setting_ON");//"Sound: ON ";
            }
            else
            {
                t.text = LanguageManager.Instance.GetTextValue("setting_menu_sound") + " : " + LanguageManager.Instance.GetTextValue("setting_OFF");//"Sound: OFF";
            }
        }

        public void SwitchMusicOnOff(Text t)
        {
            SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            if (bgm != null)
            {
                bgm.mute = bgm.mute ? false : true;

                PlayerPrefs.SetInt(SoundManager.PP_BGM_SETTING, bgm.mute ? 1 : 0);

                if (bgm.mute)
                {
                    t.text = LanguageManager.Instance.GetTextValue("setting_menu_music") + " : " + LanguageManager.Instance.GetTextValue("setting_OFF");//"Music: OFF";
                }
                else
                {
                    t.text = LanguageManager.Instance.GetTextValue("setting_menu_music") + " : " + LanguageManager.Instance.GetTextValue("setting_ON");//"Music: ON ";
                }
            }
        }

        public void FixBGM()
        {
            bgm.mute = PlayerPrefs.GetInt(SoundManager.PP_BGM_SETTING, 0) == 0 ? false : true;
        }



        public void FadeOutBGM(int times, float sec)
        {
            float vol = bgm.volume / times;

            for (int i = 0; i < times; i++)
            {
                bgm.volume -= vol;
                StartCoroutine(FadeOutProcess(sec));
            }
        }

        IEnumerator FadeOutProcess(float sec)
        {
            yield return new WaitForSeconds(sec);
        }


    }
}
