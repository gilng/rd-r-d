﻿using System;
using SmartLocalization;
using UnityEngine;
using Zenject;

namespace Ignite.AOM
{
    public class UserManager : MonoBehaviour
    {
        private UIViewLoader _uiViewLoader;
        private IUserService _userService;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader, IUserService userService)
        {
            _uiViewLoader = uiViewLoader;
            _userService = userService;
        }

        private static UserManager _instance;

        public static UserManager Instance
        {
            get { return _instance; }
        }

        public UserSummary CurrentUserSummary
        {
            get { return _userService.CurrentUserSummary; }
        }

        // Use this for initialization
        void Awake()
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        public void GiveGems(int gems, Action successCallback = null, bool showSyncOverlay = true)
        {
            CloudSavingOverlayController synchronizingOverlayController =
                showSyncOverlay ? ShowSynchronizingOverlay() : null;

            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                CloseSynchronizingOverlay(synchronizingOverlayController);
                ShowNetworkNotReachablePopup();
                return;
            }

            _userService.UpdateGemBalance(gems, userSummary =>
            {
                CloseSynchronizingOverlay(synchronizingOverlayController);

                if (userSummary != null)
                {
                    AOMStoreInventory.ResetDiamond(userSummary.Gem);

                    if (successCallback != null)
                    {
                        successCallback();
                    }
                }
                else
                {
                    ShowNetworkNotReachablePopup();
                }
            });
        }

        public void TakeGems(int gems, Action successCallback = null)
        {
            CloudSavingOverlayController synchronizingOverlayController = ShowSynchronizingOverlay();

            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                CloseSynchronizingOverlay(synchronizingOverlayController);
                ShowNetworkNotReachablePopup();
                return;
            }

            if (CurrentUserSummary != null)
            {
                if (CurrentUserSummary.Gem >= gems)
                {
                    _userService.UpdateGemBalance(-gems, userSummary =>
                    {
                        CloseSynchronizingOverlay(synchronizingOverlayController);

                        if (userSummary != null)
                        {
                            SoundManager.Instance.PlaySoundWithType(SoundType.DiamondUsed);

                            AOMStoreInventory.ResetDiamond(userSummary.Gem);

                            if (successCallback != null)
                            {
                                successCallback();
                            }
                        }
                        else
                        {
                            ShowNetworkNotReachablePopup();
                        }
                    });
                }
                else
                {
                    CloseSynchronizingOverlay(synchronizingOverlayController);
                    ShowShopMenu();
                }
            }
            else
            {
                _userService.FindUserSummary(userSummary =>
                {
                    CloseSynchronizingOverlay(synchronizingOverlayController);

                    if (userSummary != null)
                    {
                        TakeGems(gems, successCallback);
                    }
                    else
                    {
                        ShowNetworkNotReachablePopup();
                    }
                });
            }
        }

        public void GiveMoecrystals(int moecrystals, Action successCallback = null, bool showSyncOverlay = true)
        {
            CloudSavingOverlayController synchronizingOverlayController =
                showSyncOverlay ? ShowSynchronizingOverlay() : null;

            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                CloseSynchronizingOverlay(synchronizingOverlayController);
                ShowNetworkNotReachablePopup();
                return;
            }

            _userService.UpdateMoecrystalBalance(moecrystals, userSummary =>
            {
                CloseSynchronizingOverlay(synchronizingOverlayController);

                if (userSummary != null)
                {
                    AOMStoreInventory.ResetRelic(userSummary.Moecrystal);

                    if (successCallback != null)
                    {
                        successCallback();
                    }
                }
                else
                {
                    ShowNetworkNotReachablePopup();
                }
            });
        }

        public void TakeMoecrystals(int moecrystals, Action successCallback = null)
        {
            CloudSavingOverlayController synchronizingOverlayController = ShowSynchronizingOverlay();

            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                CloseSynchronizingOverlay(synchronizingOverlayController);
                ShowNetworkNotReachablePopup();
                return;
            }

            if (CurrentUserSummary != null)
            {
                if (CurrentUserSummary.Moecrystal >= moecrystals)
                {
                    _userService.UpdateMoecrystalBalance(-moecrystals, userSummary =>
                    {
                        CloseSynchronizingOverlay(synchronizingOverlayController);

                        if (userSummary != null)
                        {
                            AOMStoreInventory.ResetRelic(userSummary.Moecrystal);

                            if (successCallback != null)
                            {
                                successCallback();
                            }
                        }
                        else
                        {
                            ShowNetworkNotReachablePopup();
                        }
                    });
                }
                else
                {
                    CloseSynchronizingOverlay(synchronizingOverlayController);
                }
            }
            else
            {
                _userService.FindUserSummary(userSummary =>
                {
                    CloseSynchronizingOverlay(synchronizingOverlayController);

                    if (userSummary != null)
                    {
                        TakeMoecrystals(moecrystals, successCallback);
                    }
                    else
                    {
                        ShowNetworkNotReachablePopup();
                    }
                });
            }
        }

        public void GiveRuby(int rubys, Action successCallback = null, bool showSyncOverlay = true)
        {
            CloudSavingOverlayController synchronizingOverlayController =
                showSyncOverlay ? ShowSynchronizingOverlay() : null;

            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                CloseSynchronizingOverlay(synchronizingOverlayController);
                ShowNetworkNotReachablePopup();
                return;
            }

            _userService.UpdateRubyBalance(rubys, userSummary =>
            {
                CloseSynchronizingOverlay(synchronizingOverlayController);

                if (userSummary != null)
                {
                    if (successCallback != null)
                    {
                        successCallback();
                    }
                }
                else
                {
                    ShowNetworkNotReachablePopup();
                }
            });
        }

        public void TakeRuby(int rubys, Action successCallback = null)
        {
            CloudSavingOverlayController synchronizingOverlayController = ShowSynchronizingOverlay();

            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                CloseSynchronizingOverlay(synchronizingOverlayController);
                ShowNetworkNotReachablePopup();
                return;
            }

            if (CurrentUserSummary != null)
            {
                if (CurrentUserSummary.Ruby >= rubys)
                {
                    _userService.UpdateRubyBalance(-rubys, userSummary =>
                    {
                        CloseSynchronizingOverlay(synchronizingOverlayController);

                        if (userSummary != null)
                        {
                            if (successCallback != null)
                            {
                                successCallback();
                            }
                        }
                        else
                        {
                            ShowNetworkNotReachablePopup();
                        }
                    });
                }
                else
                {
                    CloseSynchronizingOverlay(synchronizingOverlayController);
                }
            }
            else
            {
                _userService.FindUserSummary(userSummary =>
                {
                    CloseSynchronizingOverlay(synchronizingOverlayController);

                    if (userSummary != null)
                    {
                        TakeRuby(rubys, successCallback);
                    }
                    else
                    {
                        ShowNetworkNotReachablePopup();
                    }
                });
            }
        }

        public void AddFriendLimit(int number, Action successCallback = null)
        {
            CloudSavingOverlayController synchronizingOverlayController = ShowSynchronizingOverlay();

            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                CloseSynchronizingOverlay(synchronizingOverlayController);

                ShowNetworkNotReachablePopup();
                return;
            }

            _userService.UpdateFriendLimit(number, userSummary =>
            {
                CloseSynchronizingOverlay(synchronizingOverlayController);

                if (userSummary != null)
                {
                    if (successCallback != null)
                    {
                        successCallback();
                    }
                }
                else
                {
                    ShowNetworkNotReachablePopup();
                }
            });
        }

        public void ShowShopMenu()
        {
            _uiViewLoader.LoadOverlayView("ShopMenu", null, false, false);
        }

        public CloudSavingOverlayController ShowSynchronizingOverlay()
        {
            GameObject overlay = _uiViewLoader.LoadOverlayView("CloudSavingOverlay", null, false, Time.timeScale == 0);
            CloudSavingOverlayController synchronizingOverlayController =
                overlay.GetComponent<CloudSavingOverlayController>();
            synchronizingOverlayController.ShowLoadingIndicator();

            return synchronizingOverlayController;
        }

        public void CloseSynchronizingOverlay(CloudSavingOverlayController synchronizingOverlayController)
        {
            if (synchronizingOverlayController != null)
                synchronizingOverlayController.CloseView();
        }

        public void ShowNetworkNotReachablePopup()
        {
            var msgPopupView = _uiViewLoader.LoadOverlayView("MsgPopupOverlay", null, false,
                Time.timeScale == 0);
            var msgPopupViewController = msgPopupView.GetComponent<MsgPopupViewController>();
            msgPopupViewController.InputMsg(LanguageManager.Instance.GetTextValue("saveLoadWarning_title"),
                LanguageManager.Instance.GetTextValue("pvp_errorMsg_NoInternet"), "btn_ok");
        }
    }
}