﻿using UnityEngine;
using UnityEngine.SocialPlatforms;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System;
using System.Net;
using UnityEngine.UI;
using Parse;
using System.Threading.Tasks;
using Zenject;

namespace Ignite.AOM
{
    public class SocialManager : IInitializable
    {
        private IUserService _userService;
        private IPvPChallengeService _pvpChallengeService;

        private UIViewLoader _uiViewLoader;

        [Inject]
        public void Construct(IUserService userService, IPvPChallengeService pvpChallengeService, UIViewLoader uiViewLoader)
        {
            _userService = userService;
            _pvpChallengeService = pvpChallengeService;
            _uiViewLoader = uiViewLoader;
        }

        public bool ParseUserTokenExpired
        {
            get
            {
                return _userService.ParseSessionTokenInvalid;
            }
            set
            {
                _userService.ParseSessionTokenInvalid = value;
            }
        }

        public IUser CurrentParseUser
        {
            get
            {
                return _userService.CurrentUser;
            }
        }
        
        public bool parseSignedIn
        {
            get
            {
                return _userService.CurrentUser != null;
            }
        }

        public bool linkedWithEmail
        {
            get
            {
                // To Do
                return _userService.IsLinkedEmail();
            }
        }

        public bool linkedWithGoogle
        {
            get
            {
                // To Do
                return _userService.IsLinkedGoogle();
            }
        }

        private static string googleUserID;
        public string GoogleUserID
        {
            set
            {
                googleUserID = value;
            }
            get
            {
                return googleUserID;
            }
        }

        private static string googleUserName;
        public string GoogleUserName
        {
            set
            {
                googleUserName = value;
            }
            get
            {
                return googleUserName;
            }
        }

        public bool googleSignedIn = false;

        // Use this for initialization
        public void Initialize()
        {

        }

        public void ShowSignInReminderOverlay(Action successCallback = null)
        {
            GameObject reminderOverlay = _uiViewLoader.LoadOverlayView("GoogleSignInReminderOverlay", null, false, Time.timeScale == 0);

            reminderOverlay.transform.FindChild("BtnSignIn").GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                GameObject overlay = _uiViewLoader.LoadOverlayView("CloudSavingOverlay", null, false, Time.timeScale == 0);
                CloudSavingOverlayController signInOverlayController = overlay.GetComponent<CloudSavingOverlayController>();
                signInOverlayController.ShowSigninMessage();

                Action signInCallback = () =>
                {
                    GameObject.Destroy(reminderOverlay);

                    if (signInOverlayController != null)
                        signInOverlayController.CloseView();

                    if (successCallback != null)
                        successCallback();
                };

                Action signInFailCallback = () =>
                {
                    if (signInOverlayController != null)
                        signInOverlayController.ShowSigninFailMessage();
                };

                GoogleSignIn(true, signInCallback, signInFailCallback);
            });

            reminderOverlay.transform.FindChild("BtnNo").GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

                GameObject.Destroy(reminderOverlay);
            });
        }

        public void ReportMaxStage(int stage)
        {
#if UNITY_IOS || UNITY_ANDROID
            if (googleSignedIn)
            {
                Social.ReportScore(stage, "CgkIu9y54NcWEAIQAg", (bool success) =>
                {
                    // handle success or failure
                    if (!success)
                    {
                        Debug.Log("Report max stage fail");
                    }
                });
            }
#endif

            # region iOS GameCenter Leaderboard
//#if UNITY_IOS
//                        Social.ReportScore(score, "grp.aom.topstage", (bool success) =>
//                        {
//                            // handle success or failure
//                            if (!success)
//                            {
//                                Debug.Log("Report score fail");
//                            }
//                        });
//#endif
            # endregion

        }

        public void ReportMaxPvPScore(int tapScore)
        {
#if UNITY_IOS || UNITY_ANDROID
            if (googleSignedIn)
            {
                Social.ReportScore(tapScore, "CgkIu9y54NcWEAIQCA", (bool success) =>
                {
                    // handle success or failure
                    if (!success)
                    {
                        Debug.Log("Report max tap score fail");
                    }
                });
            }
#endif
        }

        public void ReportMaxPvPTapSpeed(int tapSpeed)
        {
#if UNITY_IOS || UNITY_ANDROID
            if (googleSignedIn)
            {
                Social.ReportScore(tapSpeed, "CgkIu9y54NcWEAIQCQ", (bool success) =>
                {
                    // handle success or failure
                    if (!success)
                    {
                        Debug.Log("Report max tap speed fail");
                    }
                });
            }
#endif
        }

        public void ShowLeaderboard()
        {
            if (!googleSignedIn)
            {
#if UNITY_IOS || UNITY_ANDROID
                GameObject overlay = _uiViewLoader.LoadOverlayView("CloudSavingOverlay", null, false, Time.timeScale == 0);
                CloudSavingOverlayController signInOverlayController = overlay.GetComponent<CloudSavingOverlayController>();
                signInOverlayController.ShowSigninMessage();

                Action signInCallback = () =>
                {
                    if (signInOverlayController != null)
                        signInOverlayController.CloseView();

                    ShowLeaderboard();
                };

                Action signInFailCallback = () =>
                {
                    if (signInOverlayController != null)
                        signInOverlayController.ShowSigninFailMessage();
                };

                GoogleSignIn(true, signInCallback, signInFailCallback);
#endif
            }
            else
            {
#if UNITY_IOS || UNITY_ANDROID
                if (GamePlayStatManager.Instance.reachedHighestStage)
                    ReportMaxStage(Convert.ToInt32(GamePlayStatManager.Instance.Stat[StatType.ReachHighestStage]));

                ReportMaxPvPScore(Convert.ToInt32(GamePlayStatManager.Instance.Stat[StatType.MaxPvPScore]));

                ReportMaxPvPTapSpeed(Convert.ToInt32(GamePlayStatManager.Instance.Stat[StatType.MaxPvPTapSpeed]));

                Social.Active.ShowLeaderboardUI();
#endif
            }
        }

        public void GoogleSignIn(bool signIn, Action successCallback = null, Action failCallback = null)
        {
            if (CheckForInternetConnection())
            {
                if (signIn)
                {
                    PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
                        .EnableSavedGames()
                        .RequireGooglePlus()
                        .Build();
                    PlayGamesPlatform.InitializeInstance(config);

                    // recommended for debugging:
                    PlayGamesPlatform.DebugLogEnabled = true;
                    // Activate the Google Play Games platform
                    PlayGamesPlatform.Activate();

                    Social.localUser.Authenticate((bool success) =>
                    {
                        // handle success or failure
                        if (!success)
                        {
                            googleSignedIn = false;
                            Debug.Log("Login social fail");

                            if (failCallback != null)
                                failCallback();
                        }
                        else
                        {
                            googleSignedIn = true;

                            GoogleUserID = Social.localUser.id;
                            GoogleUserName = Social.localUser.userName;

                            if (successCallback != null)
                                successCallback();
                        }
                    });
                }
                else
                {
                    if (googleSignedIn)
                    {
                        googleSignedIn = false;

                        // Activate the Google Play Games platform
                        PlayGamesPlatform.Instance.SignOut();

                        if (successCallback != null)
                            successCallback();
                    }
                }
            }
            else
            {
                if (failCallback != null)
                    failCallback();
            }
        }

        public void ParseSignInWithGoogle(Action successCallback = null, Action failCallback = null)
        {
            if (CheckForInternetConnection())
            {
                if (googleSignedIn)
                {
                    PlayGamesPlatform.Instance.GetServerAuthCode((authStatus, code) =>
                    {
                        if (authStatus == CommonStatusCodes.Success)
                        {
                            Debug.Log("AuthCode: " + code);

                            if (!parseSignedIn)
                            {
                                _userService.LoginWithGoogleAuthCode(code, (e) =>
                                {
                                    if (e != null)
                                    {
                                        Debug.LogException(e);

                                        if (failCallback != null)
                                            failCallback();

                                    }
                                    else
                                    {
                                        if (successCallback != null)
                                            successCallback();
                                    }
                                });
                            }
                            else
                            {
                                Debug.Log("Already Login Parse User: " + _userService.CurrentUser.Username);

                                if (!_userService.IsLinkedGoogle())
                                {
                                    _userService.LinkGoogleAccount(code, (e2) =>
                                    {
                                        if (e2 != null)
                                        {
                                            Debug.LogException(e2);

                                            if (failCallback != null)
                                                failCallback();
                                        }
                                        else
                                        {
                                            if (successCallback != null)
                                                successCallback();
                                        }
                                    });
                                }
                                else
                                {
                                    Debug.Log("current google user Id: " + ParseUser.CurrentUser["googleUserId"]);

                                    if (successCallback != null)
                                        successCallback();
                                }
                            }
                        }
                        else
                        {
                            Debug.Log("Error authCode: " + authStatus.ToString() + "Code: " + code);

                            if (failCallback != null)
                                failCallback();
                        }
                    });
                }
                else
                {
                    if (failCallback != null)
                        failCallback();
                }
            }
            else
            {
                if (failCallback != null)
                    failCallback();
            }
        }

        public void ParseSignUp(Action successCallback = null, Action failCallback = null)
        {
            _userService.SignUp(null, null, (e, user) =>
            {
                if (e != null)
                {
                    Debug.LogException(e);

                    if (failCallback != null)
                        failCallback();
                }
                else
                {
                    if (successCallback != null)
                        successCallback();
                }
            });
        }

        public void ParseSignIn(string email, string password, Action successCallback = null, Action failCallback = null)
        {
            _userService.LoginWithUserEmail(email, password, (e) =>
                {
                    if (e != null)
                    {
                        Debug.LogException(e);

                        if (failCallback != null)
                            failCallback();
                    }
                    else
                    {
                        if (successCallback != null)
                            successCallback();
                    }
                });
        }

        public void ParseLogout(Action successCallback = null, Action failCallback = null)
        {
            _userService.Logout((e) =>
            {
                if (e != null)
                {
                    Debug.LogError(e);

                    if (failCallback != null)
                        failCallback();
                }
                else
                {
                    if (successCallback != null)
                        successCallback();
                }
            });
        }

        public void ParseLinkWithEmail(string email, string password, Action successCallback = null, Action failCallback = null)
        {
            if (email != "" && password.Length == 8)
            {
                _userService.LinkUserEmail(email, password, (e) =>
                {
                    if (e != null)
                    {
                        Debug.LogException(e);

                        if (failCallback != null)
                            failCallback();

                    }
                    else
                    {
                        if (successCallback != null)
                            successCallback();
                    }
                });
            }
            else
            {
                if (failCallback != null)
                    failCallback();
            }
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://www.google.com"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

    }
}
