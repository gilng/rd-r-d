﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Ignite.AOM
{
	public class TipsManager : MonoBehaviour
	{
		public List<GameObject> contents;

		int tipsNum = -1;

		// Use this for initialization
		void Start ()
		{
			ChangeTips( PlayerPrefs.GetInt("tips", -1) );
		}

		public void ChangeTips(int followTips = -1)
		{
			if (contents.Count > 0)
			{
//				int tipsNum = (followTips == -1)? Random.Range(0, contents.Count) : followTips;

				if (followTips == -1)
				{
					if (tipsNum + 1 >= contents.Count)
					{
						tipsNum = 0;
					}
					else
					{
						tipsNum++;
					}
				}
				else
				{
					tipsNum = followTips;
				}

				PlayerPrefs.SetInt("tips", tipsNum);

				for (int i=0; i<contents.Count; i++)
				{
					contents[i].SetActive(tipsNum == i);						
				}
			}
		}

	}
}