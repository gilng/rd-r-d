﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.MiniJSON;
using SmartLocalization;
using Parse;
using Zenject;
using System.Threading.Tasks;

namespace Ignite.AOM
{
    public class VersionCheckerManager : IInitializable
    {
        private readonly AsyncProcessor _asyncProcessor;
        private readonly IUserService _userService;

        [Inject]
        public VersionCheckerManager(AsyncProcessor asyncProcessor, IUserService userService)
        {
            _asyncProcessor = asyncProcessor;
            _userService = userService;
        }

        public GameObject updateBackgroundOverlay, updateMsgOverlay;

        private bool parseConfig_Finish = false;

        private string iosVersion;
        private string androidVersion;

        private string _currentVersion;

        public string CurrentVersion 
        { 
            get 
            {
                if (PlayerPrefs.GetString("CURRENT_VERSION", "0.0.0") != _currentVersion)
                    PlayerPrefs.SetString("CURRENT_VERSION", _currentVersion);

                return _currentVersion; 
            } 
        }

        private bool _versionPass;

        public bool VersionPass { get { return _versionPass; } }

        private bool _reparationAvailable;

        public bool ReparationAvailable { get { return _reparationAvailable; } }

        private bool _referralCodeAvailable;

        public bool ReferralCodeAvailable { get { return _referralCodeAvailable; } }

        // Use this for initialization
        public void Initialize()
        {
            iosVersion = "2.5.0";
            androidVersion = "2.5.0";

#if UNITY_IOS
			_currentVersion = iosVersion;
#else
            _currentVersion = androidVersion;
#endif

            //PlayerPrefs.SetString("CURRENT_VERSION", _currentVersion);

            _reparationAvailable = false;
        }

        public void VerifyAppVersion(Action<bool> callback)
        {
            _asyncProcessor.StartCoroutine(CoroutineVerifyAppVersion(callback));
        }

        IEnumerator CoroutineVerifyAppVersion(Action<bool> callback)
        {
            ParseConfig config = null;

            bool parseConfigChecker_Version = false;
            string parseConfig_Version;

            Task<ParseConfig> t = ParseConfig.GetAsync();

            while (!t.IsCompleted)
            {
                yield return null;
            }
            if (t.IsCanceled)
            {

            }
            else if (t.IsFaulted)
            {
                if (t.Exception != null)
                {
                    foreach (ParseException err in t.Exception.InnerExceptions)
                    {
                        ServiceException e = new ServiceException((ServiceException.ErrorCode)err.Code, err.Message, err);
                        _userService.HandleParseError(e);
                        Debug.LogError("error message: " + err.Message);
                        Debug.LogError("error code: " + err.Code);
                        Debug.LogError("error stacktrace: " + err.StackTrace);
                    }
                }
                
                config = ParseConfig.CurrentConfig;
            }
            else
            {
                config = t.Result;
            }


            // Verify App Version
#if UNITY_IOS
			parseConfigChecker_Version = config.TryGetValue ("Version_IOS", out parseConfig_Version);
#else
            parseConfigChecker_Version = config.TryGetValue("Version_Android", out parseConfig_Version);
#endif

            if (parseConfigChecker_Version)
            {

                Version _v_parseConfig = new Version(parseConfig_Version);
                Version _v_current = new Version(_currentVersion);

                if (_v_current.CompareTo(_v_parseConfig) > 0)
                {
                    // Enter Game (Limited)
                    _versionPass = true;
                    _referralCodeAvailable = false;
                }
                else if (_v_current.CompareTo(_v_parseConfig) < 0)
                {
                    // Call to update version
                    _versionPass = false;
                }
                else
                {
                    // Enter Game (Normal)
                    _versionPass = true;
                    _referralCodeAvailable = true;
                }
            }
            else
            {
                Debug.LogError("parseConfigChecker_Version -> FALSE");
                _versionPass = true;
                _referralCodeAvailable = false;
            }

            if (callback != null)
                callback(_versionPass);

        }

        //        public void CheckNetwork()
        //        {
        //            if (updateBackgroundOverlay != null)
        //            {
        //                if (!updateBackgroundOverlay.activeSelf)
        //                    updateBackgroundOverlay.SetActive(true);
        //
        //                updateBackgroundOverlay.transform.FindChild("BtnNew").gameObject.SetActive(false);
        //                updateBackgroundOverlay.transform.FindChild("BtnLoad").gameObject.SetActive(false);
        //                updateBackgroundOverlay.transform.FindChild("TextLoading").gameObject.SetActive(false);
        //                updateBackgroundOverlay.transform.FindChild("SliderLoading").gameObject.SetActive(false);
        //                updateBackgroundOverlay.transform.FindChild("BtnRetry").gameObject.SetActive(false);
        //                updateBackgroundOverlay.transform.FindChild("BtnRetry_Parse").gameObject.SetActive(false);
        //
        //                // Parse
        //                //				updateBackgroundOverlay.transform.FindChild ("LoadData").gameObject.SetActive (false);
        //                //				updateBackgroundOverlay.transform.FindChild ("ParseLogin").gameObject.SetActive (false);
        //                //				updateBackgroundOverlay.transform.FindChild ("BtnLogout").gameObject.SetActive (false);
        //
        //                if (SocialManager.CheckForInternetConnection())
        //                {
        //                    updateBackgroundOverlay.transform.FindChild("TextLoading").gameObject.SetActive(true);
        //                    updateBackgroundOverlay.transform.FindChild("TextLoading").GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("update_checking");
        //
        //                    _versionPass = false;
        //                    CompareVersion_ParseConfig();
        //
        //                }
        //                else
        //                {
        //                    parseConfig_Finish = true;
        //
        //                    _versionPass = true;
        //                    _referralCodeAvailable = true;
        //                }
        //            }
        //        }
        //
        //        void CompareVersion_ParseConfig()
        //        {
        //            ParseConfig config = null;
        //
        //            bool parseConfigChecker_Version = false;
        //            string parseConfig_Version;
        //
        //            ParseConfig.GetAsync().ContinueWith(t =>
        //            {
        //
        //                if (t.IsCompleted)
        //                {
        //                    config = t.Result;
        //                }
        //                else
        //                {
        //                    config = ParseConfig.CurrentConfig;
        //                }
        //
        //                // ------------------------------------------------------------------------------------------------------------------------ Check version
        //#if UNITY_IOS
        //                parseConfigChecker_Version = config.TryGetValue ("Version_IOS", out parseConfig_Version);
        //#else
        //                parseConfigChecker_Version = config.TryGetValue("Version_Android", out parseConfig_Version);
        //#endif
        //
        //                if (parseConfigChecker_Version)
        //                {
        //
        //                    Version _v_parseConfig = new Version(parseConfig_Version);
        //                    Version _v_current = new Version(_currentVersion);
        //
        //                    if (_v_current.CompareTo(_v_parseConfig) > 0)
        //                    {
        //                        // Enter Game (Limited)
        //                        _versionPass = true;
        //                        _referralCodeAvailable = false;
        //                    }
        //                    else if (_v_current.CompareTo(_v_parseConfig) < 0)
        //                    {
        //                        // Call to update version
        //                        _versionPass = false;
        //                    }
        //                    else
        //                    {
        //                        // Enter Game (Normal)
        //                        _versionPass = true;
        //                        _referralCodeAvailable = true;
        //                    }
        //                }
        //                else
        //                {
        //                    Debug.LogError("parseConfigChecker_Version -> FALSE");
        //                    _versionPass = true;
        //                    _referralCodeAvailable = false;
        //                }
        //
        //
        //                //				_versionPass = true;
        //
        //                // ------------------------------------------------------------------------------------------------------------------------ Final
        //
        //                parseConfig_Finish = true;
        //            });
        //        }
        //
        //        void Update() // Because "ParseConfig.GetAsync().ContinueWith" is slow. Using Update to receive "Finish"
        //        {
        //            if (parseConfig_Finish)
        //            {
        //
        //                if (_versionPass)
        //                {
        //                    AssetBundleLoader.Instance.InitAssetBundle();
        //
        //                    updateBackgroundOverlay = null;
        //                    updateMsgOverlay = null;
        //                }
        //                else
        //                {
        //                    if (updateMsgOverlay != null)
        //                    {
        //                        if (!updateMsgOverlay.activeSelf)
        //                            updateMsgOverlay.SetActive(true);
        //                    }
        //                }
        //
        //                parseConfig_Finish = false;
        //            }
        //        }
        //
        //        public void UpdateVersion()
        //        {
        //            string url = "";
        //
        //#if UNITY_IOS
        //            url = "https://itunes.apple.com/hk/app/attack-on-moe/id1059991997?mt=8";
        //#else
        //            url = "https://play.google.com/store/apps/details?id=game.ignite.aom&hl=zh-TW";
        //#endif
        //
        //            Application.OpenURL(url);
        //        }

    }
}
