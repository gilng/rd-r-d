﻿//using UnityEngine;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using Parse;

//namespace Ignite.AOM
//{
//    public enum ParseLoginType
//    {
//        None,
//        NewAcc,
//        ParseAcc,
//        GoogleAcc
//    }

//    public enum ParseLoadingStatus
//    {
//        None,
//        NoInternetConnection,
//        Downloading,
//        Downloaded
//    }

//    public class ParseAccountManager : MonoBehaviour
//    {
//        private static ParseAccountManager _instance;

//        public static ParseAccountManager Instance {
//            get {
//                return _instance;
//            }
//        }

//        private string parseAcc, parsePw, googleId;
//        private ParseLoginType type;
//        private ParseLoadingStatus status;
//        private List<ParseUser> userList;

//        private bool signInSuccess_run = false;
//        private bool loginSuccess_run = false;



//        void Awake ()
//        {
//            _instance = this;

//            DontDestroyOnLoad (this.gameObject);

//            type = ParseLoginType.None;
//            status = ParseLoadingStatus.None;

//            parseAcc = "";
//            parsePw = "";
//            googleId = "";


//            if (ParseUser.CurrentUser != null) {
//                ParseUser.LogOutAsync().ContinueWith(t => {
//                    Debug.LogError("Logged out successfully: " + !t.IsFaulted);
//                });
//            }


////			Debug.LogError( "ParseUser.CurrentUser exist? "+ (ParseUser.CurrentUser != null) +"\n");
//        }

//        // LoadParseUserData : Run when restore overlay active (CloudDataManager - ShowGameStartPage)
//        public void LoadParseUserData()
//        {
//            Debug.LogError ("LoadParseUserData\n");

//            if (SocialManager.CheckForInternetConnection())
//            {
//                userList = new List<ParseUser> ();

//                status = ParseLoadingStatus.Downloading;

//                var userQuery = ParseUser.Query;

//                userQuery.FindAsync().ContinueWith( t => {
					
//                    Debug.LogError(">> LoadParseUserData: IsFaulted: "+ t.IsFaulted +" - IsCanceled: "+ t.IsCanceled +" - IsCompleted: "+ t.IsCompleted +"\n");

//                    if (t.IsFaulted || t.IsCanceled)
//                    {
//                        status = ParseLoadingStatus.Downloaded;

//                        foreach (ParseException err in t.Exception.InnerExceptions)
//                        {
//                            //ParseException parseException = (ParseException) e;
//                            Debug.LogError("\n\t~ LoadParseUserData\n\t~ error message: " + err.Message +"\n\t~ error code: " + err.Code +"\n\t~ error stacktrace: " + err.StackTrace);
//                        }

//                        Debug.LogError(">> LoadParseUserData: Fail");

//                        WaitingDataDownload();
//                    }
//                    else
//                    {
//                        status = ParseLoadingStatus.Downloaded;

//                        foreach (ParseUser p in t.Result)
//                        {
//                            userList.Add(p);
//                        }

//                        Debug.LogError(">> LoadParseUserData: Success ( "+ userList.Count +" )\n");

//                        WaitingDataDownload();
//                    }

//                });

////				StartCoroutine (UserService.Instance.FindAll (list => {
////
////					status = ParseLoadingStatus.Downloaded;
////
////					if (list != null) {
////						userList = list;
////					}
////
////					WaitingDataDownload();
////				}));

//            }
//            else
//            {
//                status = ParseLoadingStatus.NoInternetConnection;
//            }
//        }

//        // InitAccount : User press "New Game -> Load Game"
//        public void InitAccount(ParseLoginType _type, string _parseAcc = "", string _parsePw = "")
//        {
//            type = _type;

//            if (status == ParseLoadingStatus.NoInternetConnection)
//            {
//                Debug.LogError("ParseAccountManager : No internet connection\n");
//                LoginFail();
//            }
//            else
//            {
//                // Read account & password from input box !
//                parseAcc = _parseAcc;
//                parsePw = _parsePw;

//                WaitingDataDownload();
//            }
//        }

//        void WaitingDataDownload ()
//        {
//            if (status == ParseLoadingStatus.Downloaded)
//            {
//                Debug.LogError ("~ WaitingDataDownload \n");

//                switch(type)
//                {
//                case ParseLoginType.NewAcc:					
//                    ParseSignIn_GenerateAccount();
//                    break;
//                case ParseLoginType.ParseAcc:
//                    ParseLogin();
//                    break;
//                case ParseLoginType.GoogleAcc:
//                    ParseSignIn_CheckingGoogleAccount();
//                    break;
//                }
//            }
//        }

//        void ParseSignIn_CheckingGoogleAccount()
//        {
//            if (SocialManager.Instance.socialSignedIn)
//            {
//                googleId = SocialManager.Instance.GoogleUserID;

//                bool isExist = false;

//                foreach (ParseUser p in userList)
//                {
//                    if (googleId == p.Email)
//                    {
//                        // Should be login by server callback

//                        Debug.LogError("Should be login by server callback \n");

//                        parseAcc = p.Username;
//                        parsePw = p.Get<string>("PW"); 

//                        ParseLogin();

//                        isExist = true;
//                    }
//                }

//                if (!isExist)
//                    ParseSignIn_GenerateAccount();
//            }
//            else
//            {
//                Action signInSuccessCallback = () => ParseSignIn_CheckingGoogleAccount();

//                Action signInFailCallback = () => 
//                { 
//                    Debug.LogError("Can't login google -> Retry\n");
//                    ParseSignIn_CheckingGoogleAccount();
//                };
//            }
//        }

//        void ParseSignIn_GenerateAccount()
//        {
//            bool allow = true;

//            Debug.LogError ("~ ParseSignIn_GenerateAccount: "+ userList.Count);

//            int count = ( userList.Count + 1) * 100;

//            do{
//                Generate_Account_Password();

//                foreach (ParseUser p in userList)
//                {
//                    if (string.Equals(parseAcc, p.Username))
//                        allow = false;
//                }

//                Debug.LogError("Username checking - "+ count +" : Acc: "+ parseAcc +" Allow? "+ allow +"\n");

//                count--;

//            } while (allow == false && userList.Count > 0 && count > 0);

//            ParseSignIn_Processing();
//        }

//        void ParseSignIn_Processing()
//        {
//            Debug.LogError(">> ParseSignIn \n>> [ Generate_Account_Password - parseAcc: "+ parseAcc +" - parsePw: "+ parsePw +" ]\n");

//            var user = new ParseUser (){
//                Username = parseAcc,
//                Password = parsePw
//            };

//            if (googleId != "")
//                user["email"] = googleId;
			
//            user["emailVerified"] = true;

//            Task signUpTask = user.SignUpAsync().ContinueWith(t => {
				
//                Debug.LogError(">> ParseSignIn: IsFaulted: "+ t.IsFaulted +" - IsCanceled: "+ t.IsCanceled +" - IsCompleted: "+ t.IsCompleted +"\n");

//                if (t.IsCanceled || t.IsFaulted)
//                {
//                    foreach (ParseException err in t.Exception.InnerExceptions)
//                    {
//                        //ParseException parseException = (ParseException) e;
//                        Debug.LogError("~ ParseSignIn\n~ error message: " + err.Message +"\n~ error code: " + err.Code +"\n~ error stacktrace: " + err.StackTrace);
//                    }
//                }
//                else
//                {
//                    signInSuccess_run = true;
//                }
//            });
//        }

//        void ParseLogin()
//        {
//            if (SocialManager.Instance.socialSignedIn) {
//                String token = "";
//                IDictionary<string, object> parms = new Dictionary<string, object> {
//                    {"email", SocialManager.Instance.GoogleUserID}
//                };

//                ParseCloud.CallFunctionAsync<object>("getUserSessionToken", parms).ContinueWith(t => {
//                    Debug.Log(t);
//                    Debug.Log(t.Result);
//                });

////				Debug.Log("token: " + token);
	
////				if (token != "") {
////					ParseUser.BecomeAsync(token).ContinueWith(t => {
////						Debug.LogError(">> ParseLogin: IsFaulted: "+ t.IsFaulted +" - IsCanceled: "+ t.IsCanceled +" - IsCompleted: "+ t.IsCompleted +"\n");
////
////						if (t.IsFaulted || t.IsCanceled)
////						{					
////							foreach (ParseException err in t.Exception.InnerExceptions)
////							{
////								//ParseException parseException = (ParseException) e;
////								Debug.LogError("\n~ ParseLogin\n~ error message: " + err.Message +"\n~ error code: " + err.Code +"\n~ error stacktrace: " + err.StackTrace);
////							}
////						} else {
////							loginSuccess_run = true;
////						}
////					});
////				}
//            } else {
//                ParseUser.LogInAsync(parseAcc, parsePw).ContinueWith(t => {

//                    Debug.LogError(">> ParseLogin: IsFaulted: "+ t.IsFaulted +" - IsCanceled: "+ t.IsCanceled +" - IsCompleted: "+ t.IsCompleted +"\n");

//                    if (t.IsFaulted || t.IsCanceled)
//                    {					
//                        foreach (ParseException err in t.Exception.InnerExceptions)
//                        {
//                            //ParseException parseException = (ParseException) e;
//                            Debug.LogError("\n~ ParseLogin\n~ error message: " + err.Message +"\n~ error code: " + err.Code +"\n~ error stacktrace: " + err.StackTrace);
//                        }
//                    } else {
//                        loginSuccess_run = true;
//                    }
//                });
//            }
//        }



//        void Update()
//        {
//            if (signInSuccess_run) {
//                ParseSignIn_Pass();
//                signInSuccess_run = false;
//            }

//            if (loginSuccess_run) {
//                LoginSucess();
//                loginSuccess_run = false;
//            }
//        }

//        void ParseSignIn_Pass()
//        {
//            Debug.LogError (">> ParseSignIn_Pass :\n>> [ parseAcc: "+ parseAcc +" ] >> [ parsePw: "+ parsePw +" ] ");

//            ParseLogin();
//        }

//        void LoginSucess()
//        {
////			Debug.LogError("LoginSuccess!!!");
////
////			switch(type)
////			{
////			case ParseLoginType.NewAcc:
////				CloudDataManager.singleton.ParseCallback_NewPlayer();
////				break;
////			case ParseLoginType.ParseAcc:
////			case ParseLoginType.GoogleAcc:
////				if (CloudDataManager.singleton != null)
////					CloudDataManager.singleton.ParseCallback_LoginSuccess();
////				break;
////			}
//        }

//        void LoginFail()
//        {
////			switch(type)
////			{
////			case ParseLoginType.NewAcc:
////				CloudDataManager.singleton.ParseCallback_NewPlayer();
////				break;
////			case ParseLoginType.ParseAcc:
////			case ParseLoginType.GoogleAcc:
////				CloudDataManager.singleton.ParseCallback_LoginFail();
////				break;
////			}
//        }









//        void Generate_Account_Password()
//        {
//            parseAcc = "";
//            parsePw = "";

//            for (int i=0; i<4; i++)
//            {
//                parseAcc += Convert.ToChar(UnityEngine.Random.Range(65, 91));
//                parseAcc += UnityEngine.Random.Range(0, 10).ToString();

//                parsePw += Convert.ToChar(UnityEngine.Random.Range(65, 91));
//                parsePw += UnityEngine.Random.Range(0, 10).ToString();
//            }
//        }



//        // Testing use
//        public void ParseUserLogout()
//        {
//            Debug.Log("Logged out processing...\n");

//            if (ParseUser.CurrentUser != null) {
				
////				ParseUser.LogOut ();
//                ParseUser.LogOutAsync().ContinueWith(t => {
//                    Debug.LogError("Logged out successfully: " + !t.IsFaulted);
//                });
//            }
//        }
//    }
//}