﻿using UnityEngine;
using System.Collections;
using System;
using SmartLocalization;
using UnityEngine.iOS;
using System.Collections.Generic;
using UnityEngine.UI;
using Soomla.Store;
using UnityEngine.Analytics;
using Zenject;

namespace Ignite.AOM
{
    public class OfflineGoldManager : MonoBehaviour
    {
        private SocialManager _socialManager;
        private HeroCore _heroCore;
        private EnemyCore _enemyCore;
        private GalleryCore _galleryCore;

        [Inject]
        public void ConstructorSafeAttribute(SocialManager socialManager, HeroCore heroCore, EnemyCore enemyCore, GalleryCore galleryCore)
        {
            _socialManager = socialManager;

            _heroCore = heroCore;
            _enemyCore = enemyCore;

            _galleryCore = galleryCore;
        }

        public static OfflineGoldManager instance;
        public const string LAST_ACTIVE_TIME = "last_active_time";

        private double offlineGoldDropAmount = 0;
        public double OfflineGoldDropAmount { get { return offlineGoldDropAmount; } }

        // Notification
        public int currentOfflineGoldNotificationId = 0;
        public bool offlineGoldNotificationSent = false;
        public double predictOfflineGoldAmount = 0;

        public int currentMaxRubQuotaNotificationId = 0;
        public bool maxRubQuotaNotificationSent = false;

        public int currentUsedNotificationId = 0;

        public double noticeBoardLastShowTime = 0;
        public bool noticeBoardShowing = false;

        bool storeInitialized = false;

        void Awake()
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);

            StoreEvents.OnSoomlaStoreInitialized += onStoreInitialized;
        }

        // Use this for initialization
        void Start()
        {
            CancelNotification();
        }

        void OnDestroy()
        {
            StoreEvents.OnSoomlaStoreInitialized -= onStoreInitialized;
        }

        void onStoreInitialized()
        {
            storeInitialized = true;
        }

        void CancelNotification()
        {
            currentUsedNotificationId = PlayerPrefs.GetInt("CURRENT_NOTIFICATION_ID", 0);

            currentOfflineGoldNotificationId = PlayerPrefs.GetInt("CURRENT_OFFLINE_GOLD_NOTIFICATION_ID", 0);

            currentMaxRubQuotaNotificationId = PlayerPrefs.GetInt("CURRENT_MAX_RUB_QUOTA_NOTIFICATION_ID", 0);

#if UNITY_ANDROID
            if (currentOfflineGoldNotificationId > 0)
                LocalNotification.CancelNotification(currentOfflineGoldNotificationId);

            if (currentMaxRubQuotaNotificationId > 0)
                LocalNotification.CancelNotification(currentMaxRubQuotaNotificationId);
#elif UNITY_IOS
            UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
#endif
            offlineGoldNotificationSent = false;
            currentOfflineGoldNotificationId = 0;
            PlayerPrefs.SetInt("CURRENT_OFFLINE_GOLD_NOTIFICATION_ID", currentOfflineGoldNotificationId);
            predictOfflineGoldAmount = 0;

            maxRubQuotaNotificationSent = false;
            currentMaxRubQuotaNotificationId = 0;
            PlayerPrefs.SetInt("CURRENT_MAX_RUB_QUOTA_NOTIFICATION_ID", currentMaxRubQuotaNotificationId);

            currentUsedNotificationId = 0;
            PlayerPrefs.SetInt("CURRENT_NOTIFICATION_ID", currentUsedNotificationId);
        }

        public bool CheckOfflineGold()
        {
            //Debug.Log("CheckOfflineGold");
            bool shouldDrop = false;
            double lastActiveTime = Double.Parse(PlayerPrefs.GetString(LAST_ACTIVE_TIME, UnitConverter.ConvertToTimestamp(System.DateTime.Now).ToString()));
            double current = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
            double heroGoldDropSkill = 0;
            _heroCore.EffectValue.TryGetValue(EffectType.GoldDropAmount, out heroGoldDropSkill);
            double unpickedAmount = Double.Parse(PlayerPrefs.GetString("UNPICKED_GOLD_AMOUNT", "0"));

            if (offlineGoldDropAmount == 0)
            {
                // offline + unpicked
                if ((current - lastActiveTime) > 20)
                    offlineGoldDropAmount = Formulas.OfflineGoldDrop(current - lastActiveTime, _enemyCore.Stage, _heroCore.TotalHeroDPS, heroGoldDropSkill, ArtifactAssets.Artifact020.EffectValue);

                offlineGoldDropAmount += unpickedAmount;

                // Get max offline gold
                predictOfflineGoldAmount = Formulas.OfflineGoldDrop((int)10800, _enemyCore.Stage, _heroCore.TotalHeroDPS, heroGoldDropSkill, ArtifactAssets.Artifact020.EffectValue);

                if (offlineGoldDropAmount > predictOfflineGoldAmount)
                    offlineGoldDropAmount = predictOfflineGoldAmount;
            }

            shouldDrop = offlineGoldDropAmount > 0;

            return shouldDrop;
        }

        public void SaveUnPickedOfflineGold(bool reset = false)
        {
            PlayerPrefs.SetString("UNPICKED_GOLD_AMOUNT", reset ? "0" : offlineGoldDropAmount.ToString());

            if (reset)
                offlineGoldDropAmount = 0;
        }

        public void OfflineGoldDrop(int goldRate)
        {
            double heroGoldDropSkill = 0;
            _heroCore.EffectValue.TryGetValue(EffectType.GoldDropAmount, out heroGoldDropSkill);

            GameManager.Instance.goldPool.GetComponent<GoldPool>().OfflineGoldDrop();

            // Add gold to player own
            GoldBalanceEventBuffer.instance.AddValue(offlineGoldDropAmount * goldRate);

            PlayerPrefs.SetString(LAST_ACTIVE_TIME, UnitConverter.ConvertToTimestamp(System.DateTime.Now).ToString());

            SaveUnPickedOfflineGold(true);

            if (GameManager.Instance.offlineGoldDropBtn != null)
                GameManager.Instance.offlineGoldDropBtn.SetActive(false);
        }

        public void SendOfflineGoldNotification()
        {
            double heroGoldDropSkill = 0;
            _heroCore.EffectValue.TryGetValue(EffectType.GoldDropAmount, out heroGoldDropSkill);

            int notificationDelay = 10800;

            predictOfflineGoldAmount = Formulas.OfflineGoldDrop(notificationDelay, _enemyCore.Stage, _heroCore.TotalHeroDPS, heroGoldDropSkill, ArtifactAssets.Artifact020.EffectValue);

            PlayerPrefs.SetString(LAST_ACTIVE_TIME, UnitConverter.ConvertToTimestamp(System.DateTime.Now).ToString());

            if (predictOfflineGoldAmount > 0)
            {
                currentUsedNotificationId += 1;
                PlayerPrefs.SetInt("CURRENT_NOTIFICATION_ID", currentUsedNotificationId);

                currentOfflineGoldNotificationId = currentUsedNotificationId;
                PlayerPrefs.SetInt("CURRENT_OFFLINE_GOLD_NOTIFICATION_ID", currentOfflineGoldNotificationId);

#if UNITY_ANDROID
                LocalNotification.SendNotification(currentOfflineGoldNotificationId,
                                                    notificationDelay, LanguageManager.Instance.GetTextValue("game_name"),
                                                    string.Format(LanguageManager.Instance.GetTextValue("offline_gold_drop_msg"),
                                                    UnitConverter.ConverterDoubleToString(predictOfflineGoldAmount)),
                                                    new Color32(0xff, 0x44, 0x44, 255),
                                                    bigIcon: "notify_icon_big",
                                                    executeMode: LocalNotification.NotificationExecuteMode.ExactAndAllowWhileIdle);
#elif UNITY_IOS
                //UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
                UnityEngine.iOS.LocalNotification n = new UnityEngine.iOS.LocalNotification();
                n.fireDate = DateTime.Now.AddSeconds(notificationDelay);
                n.alertBody = string.Format(LanguageManager.Instance.GetTextValue("offline_gold_drop_msg"), UnitConverter.ConverterDoubleToString(predictOfflineGoldAmount));
                n.hasAction = false;
                UnityEngine.iOS.NotificationServices.RegisterForNotifications(NotificationType.Alert);
                UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(n);
#endif

                offlineGoldNotificationSent = true;
            }
        }

        public void SendRubQuotaMaxNotification()
        {
            if (_galleryCore.TouchCredit < _galleryCore.MaxTouchCredit)
            {
                currentUsedNotificationId += 1;
                PlayerPrefs.SetInt("CURRENT_NOTIFICATION_ID", currentUsedNotificationId);

                currentMaxRubQuotaNotificationId = currentUsedNotificationId;
                PlayerPrefs.SetInt("CURRENT_MAX_RUB_QUOTA_NOTIFICATION_ID", currentMaxRubQuotaNotificationId);

                int notificationDelay = (int)(((_galleryCore.MaxTouchCredit - _galleryCore.TouchCredit) * _galleryCore.CreditRechargeTime) - ((UnitConverter.ConvertToTimestamp(System.DateTime.Now) - _galleryCore.LastTouch) % _galleryCore.CreditRechargeTime));

#if UNITY_ANDROID
                LocalNotification.SendNotification(currentMaxRubQuotaNotificationId,
                                                    notificationDelay, LanguageManager.Instance.GetTextValue("game_name"),
                                                    string.Format(LanguageManager.Instance.GetTextValue("rub_quota_max_msg")),
                                                    new Color32(0xff, 0x44, 0x44, 255),
                                                    bigIcon: "notify_icon_big",
                                                    executeMode: LocalNotification.NotificationExecuteMode.ExactAndAllowWhileIdle);
#elif UNITY_IOS
                //UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
                UnityEngine.iOS.LocalNotification n = new UnityEngine.iOS.LocalNotification();
                n.fireDate = DateTime.Now.AddSeconds(notificationDelay);
                n.alertBody = LanguageManager.Instance.GetTextValue("rub_quota_max_msg");
                n.hasAction = false;
                UnityEngine.iOS.NotificationServices.RegisterForNotifications(NotificationType.Alert);
                UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(n);
#endif

                maxRubQuotaNotificationSent = true;
            }
        }

        bool goldSaved = false;

        void OnApplicationFocus(bool focusStatus)
        {
            if (storeInitialized)
            {
                if (!focusStatus)
                {
                    if (GameManager.Instance != null)
                        GameManager.Instance.SavePlayingDuration();

                    SendOfflineGoldNotification();
                    SendRubQuotaMaxNotification();

                    SaveUnPickedOfflineGold();

                    if (!goldSaved)
                    {
                        AOMStoreInventory.SaveGoldValue();
                        goldSaved = true;
                    }

                    // Analytics
                    if (GamePlayStatManager.Instance.reachedHighestStage)
                    {
                        string analyticsEventName = "";

                        if (_enemyCore.Stage == 0)
                            analyticsEventName = "reachStage0";
                        else if (_enemyCore.Stage > 0 && _enemyCore.Stage <= 20)
                            analyticsEventName = "reachStage20";
                        else if (_enemyCore.Stage > 20 && _enemyCore.Stage <= 40)
                            analyticsEventName = "reachStage40";
                        else if (_enemyCore.Stage > 40 && _enemyCore.Stage <= 60)
                            analyticsEventName = "reachStage60";
                        else if (_enemyCore.Stage > 60 && _enemyCore.Stage <= 80)
                            analyticsEventName = "reachStage80";
                        else if (_enemyCore.Stage > 80 && _enemyCore.Stage <= 100)
                            analyticsEventName = "reachStage100";
                        else if (_enemyCore.Stage > 100 && _enemyCore.Stage <= 200)
                            analyticsEventName = "reachStage200";
                        else if (_enemyCore.Stage > 200 && _enemyCore.Stage <= 300)
                            analyticsEventName = "reachStage300";
                        else if (_enemyCore.Stage > 300 && _enemyCore.Stage <= 400)
                            analyticsEventName = "reachStage400";
                        else if (_enemyCore.Stage > 400 && _enemyCore.Stage <= 500)
                            analyticsEventName = "reachStage500";
                        else if (_enemyCore.Stage > 500 && _enemyCore.Stage <= 600)
                            analyticsEventName = "reachStage600";
                        else if (_enemyCore.Stage > 600 && _enemyCore.Stage <= 700)
                            analyticsEventName = "reachStage700";
                        else if (_enemyCore.Stage > 700 && _enemyCore.Stage <= 800)
                            analyticsEventName = "reachStage800";
                        else if (_enemyCore.Stage > 800 && _enemyCore.Stage <= 900)
                            analyticsEventName = "reachStage900";
                        else if (_enemyCore.Stage > 900 && _enemyCore.Stage <= 1000)
                            analyticsEventName = "reachStage1000";
                        else if (_enemyCore.Stage > 1000 && _enemyCore.Stage <= 1100)
                            analyticsEventName = "reachStage1100";
                        else if (_enemyCore.Stage > 1100 && _enemyCore.Stage <= 1200)
                            analyticsEventName = "reachStage1200";
                        else if (_enemyCore.Stage > 1200 && _enemyCore.Stage <= 1300)
                            analyticsEventName = "reachStage1300";
                        else if (_enemyCore.Stage > 1300 && _enemyCore.Stage <= 1400)
                            analyticsEventName = "reachStage1400";
                        else if (_enemyCore.Stage > 1400 && _enemyCore.Stage <= 1500)
                            analyticsEventName = "reachStage1500";
                        else if (_enemyCore.Stage > 1500 && _enemyCore.Stage <= 1600)
                            analyticsEventName = "reachStage1600";
                        else if (_enemyCore.Stage > 1600 && _enemyCore.Stage <= 1700)
                            analyticsEventName = "reachStage1700";
                        else if (_enemyCore.Stage > 1700 && _enemyCore.Stage <= 1800)
                            analyticsEventName = "reachStage1800";
                        else if (_enemyCore.Stage > 1800 && _enemyCore.Stage <= 1900)
                            analyticsEventName = "reachStage1900";
                        else if (_enemyCore.Stage > 1900 && _enemyCore.Stage <= 2000)
                            analyticsEventName = "reachStage2000";
                        else if (_enemyCore.Stage > 2000 && _enemyCore.Stage <= 2100)
                            analyticsEventName = "reachStage2100";
                        else if (_enemyCore.Stage > 2100 && _enemyCore.Stage <= 2200)
                            analyticsEventName = "reachStage2200";
                        else if (_enemyCore.Stage > 2200 && _enemyCore.Stage <= 2300)
                            analyticsEventName = "reachStage2300";
                        else if (_enemyCore.Stage > 2300 && _enemyCore.Stage <= 2400)
                            analyticsEventName = "reachStage2400";
                        else if (_enemyCore.Stage > 2400 && _enemyCore.Stage <= 2500)
                            analyticsEventName = "reachStage2500";

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                        Analytics.CustomEvent(analyticsEventName, new Dictionary<string, object>());
#endif
                        GamePlayStatManager.Instance.reachedHighestStage = false;
                    }
                }
                else
                {
                    if (goldSaved)
                        goldSaved = false;

#if UNITY_ANDROID
                    if (offlineGoldNotificationSent)
                    {
                        LocalNotification.CancelNotification(currentOfflineGoldNotificationId);

                        currentOfflineGoldNotificationId = 0;
                        PlayerPrefs.SetInt("CURRENT_OFFLINE_GOLD_NOTIFICATION_ID", currentOfflineGoldNotificationId);
                        predictOfflineGoldAmount = 0;
                        offlineGoldNotificationSent = false;
                    }

                    if (maxRubQuotaNotificationSent)
                    {
                        LocalNotification.CancelNotification(currentMaxRubQuotaNotificationId);

                        currentMaxRubQuotaNotificationId = 0;
                        PlayerPrefs.SetInt("CURRENT_MAX_RUB_QUOTA_NOTIFICATION_ID", currentMaxRubQuotaNotificationId);
                        maxRubQuotaNotificationSent = false;
                    }
#elif UNITY_IOS
                    if (offlineGoldNotificationSent || maxRubQuotaNotificationSent)
                    {
                        UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();

                        currentOfflineGoldNotificationId = 0;
                        PlayerPrefs.SetInt("CURRENT_OFFLINE_GOLD_NOTIFICATION_ID", currentOfflineGoldNotificationId);
                        predictOfflineGoldAmount = 0;
                        offlineGoldNotificationSent = false;

                        currentMaxRubQuotaNotificationId = 0;
                        PlayerPrefs.SetInt("CURRENT_MAX_RUB_QUOTA_NOTIFICATION_ID", currentMaxRubQuotaNotificationId);
                        maxRubQuotaNotificationSent = false;
                    }
#endif

                    if (GameManager.Instance != null)
                    {
                        GameManager.Instance.gameStartTime = DateTime.Now;

                        if (OfflineGoldManager.instance.noticeBoardLastShowTime > 0)
                        {
                            GameManager.Instance.ShowNoticeBoard();
                        }

                        if (CheckOfflineGold())
                        {
                            GameManager.Instance.offlineGoldDropBtn.SetActive(true);
                        }

//                        GameManager.Instance.AddReferralFriend();

                    }
                }
            }
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (storeInitialized)
            {
                if (pauseStatus)
                {
                    if (!goldSaved)
                    {
                        AOMStoreInventory.SaveGoldValue();
                        goldSaved = true;
                    }
                }
                else
                {
                    if (goldSaved)
                        goldSaved = false;
                }
            }
        }

        void OnApplicationQuit()
        {
            if (storeInitialized)
            {
                if (!goldSaved)
                {
                    AOMStoreInventory.SaveGoldValue();
                    goldSaved = true;
                }
            }

            if (_socialManager != null)
                _socialManager.GoogleSignIn(false);
        }
    }
}
