﻿using UnityEngine;
using System.Collections.Generic;

namespace Ignite.AOM
{
    public class TutorialManager : MonoBehaviour
    {
        private static TutorialManager _instance;

        public static TutorialManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public List<int> triggeredTutorialList;

        public bool tutorialCompleted = false;

        void Awake()
        {
            _instance = this;

            triggeredTutorialList = new List<int>();
            
            DontDestroyOnLoad(this.gameObject);
        }
       
        public void TriggerTutorial(int tutorialId)
        {
            if (!CheckTutorialCompletedById(tutorialId))
            {
                if (!triggeredTutorialList.Contains(tutorialId))
                {
                    triggeredTutorialList.Add(tutorialId);

                    triggeredTutorialList.Sort();
                }

                if (triggeredTutorialList[0] == tutorialId)
                    EventManager.TriggerTutorial(triggeredTutorialList[0]);
            }
        }

        public void UpdateTutorialProgress(int tutorialId)
        {
                triggeredTutorialList.Remove(tutorialId);

                EventManager.CompleteTutorial(tutorialId);

                PlayerPrefs.SetInt("UserTutorialProgress_" + tutorialId, 1);

                if(triggeredTutorialList.Count > 0)
                    EventManager.TriggerTutorial(triggeredTutorialList[0]);
        }

        public bool CheckCurrentTutorialCompleted()
        {
            bool completed = false;

            if (!tutorialCompleted)
                tutorialCompleted = PlayerPrefs.GetInt("UserTutorialCompleted", 0) > 0;

            if (!tutorialCompleted)
            {
                if(triggeredTutorialList.Count > 0)
                    completed = PlayerPrefs.GetInt("UserTutorialProgress_" + triggeredTutorialList[0], 0) > 0;
                else
                    completed = true;
            }
            else
            {
                completed = true;
            }

            return completed;
        }

        public bool CheckTutorialCompletedById(int tutorialId)
        {
            bool completed = false;

            if (!tutorialCompleted)
                tutorialCompleted = PlayerPrefs.GetInt("UserTutorialCompleted", 0) > 0;

            if (!tutorialCompleted)
                completed = PlayerPrefs.GetInt("UserTutorialProgress_" + tutorialId, 0) > 0;
            else
                completed = true;

            return completed;
        }
    }
}
