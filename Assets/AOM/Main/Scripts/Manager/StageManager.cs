﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    public class StageManager : MonoBehaviour
    {
        private UIViewLoader _uiViewLoader;
        private GalleryCore _galleryCore;
        private EnemyCore _enemyCore;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader, GalleryCore galleryCore, EnemyCore enemyCore)
        {
            _uiViewLoader = uiViewLoader;
            _galleryCore = galleryCore;
            _enemyCore = enemyCore;
        }

        private static StageManager _instance;
        public static StageManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public int currentStage
        {
            get
            {
                return _enemyCore.Stage;
            }
        }

        public int currentWave
        {
            get
            {
                return _enemyCore.Wave;
            }
        }

        public string nextMoetanId;
        public int bgId;
        public int fgId;

        public BGEffectOverlayController bgEffectOverlay;

        // Use this for initialization
        void Awake()
        {
            _instance = this;

            EventManager.OnBGSwitch += OnBGSwitch;
            EventManager.OnLanguageChange += UpdateWorldName;
        }

        void OnDestory()
        {
            EventManager.OnBGSwitch -= OnBGSwitch;
            EventManager.OnLanguageChange -= UpdateWorldName;
        }

        void OnBGSwitch()
        {
//            Background.instance.ChangeBackground(bgId, fgId);
//
//            UpdateWorldName();
        }

        void UpdateWorldName()
        {
            if (GameManager.Instance != null)
                GameManager.Instance.currentWorldNameText.text = LanguageManager.Instance.GetTextValue("bg" + bgId.ToString("000"));
        }

        public void ChangeStageWorld(bool moetanDefeated = true)
        {
            nextMoetanId = GameManager.Instance.enemyPool.GetNextMoetan(moetanDefeated).bossId;

            Gallery moetan = _galleryCore.GetGirlWithID(nextMoetanId.Replace("Boss_", ""));

            int nextBGId = moetan.bgId;
            int nextFGId = moetan.fgId;

            if (nextBGId == 0)
            {
                nextBGId = 1;
                nextFGId = 1;
            }

            bgId = nextBGId;
            fgId = nextFGId;

            if (moetanDefeated)
            {
                bgEffectOverlay.OverlayEnable("bg" + bgId.ToString("000"));
            }
            else
            {
                bgEffectOverlay.OverlayDisable();

                OnBGSwitch();
            }
        }
    }
}
