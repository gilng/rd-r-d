﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using SmartLocalization;
using Soomla.Store;
using System.Collections;
using UnityEngine.UI;
using Zenject;
using Parse;

namespace Ignite.AOM
{
    public class InitManager : IInitializable, IDisposable
    {
        private readonly VersionCheckerManager _versionCheckerManager;

        private readonly SocialManager _socialManager;
        private readonly CloudDataManager _cloudDataManager;

        private readonly AsyncProcessor _asyncProcessor;
        private readonly SoomlaInitializer _soomlaInitializer;
        private readonly UIViewLoader _uiViewLoader;

        [Inject]
        public InitManager(VersionCheckerManager versionCheckerManager, SocialManager socialManager, CloudDataManager cloudDataManager, SoomlaInitializer soomlaInitializer, AsyncProcessor asyncProcessor, UIViewLoader uiViewLoader)
        {
            _versionCheckerManager = versionCheckerManager;
            _socialManager = socialManager;
            _cloudDataManager = cloudDataManager;
            _soomlaInitializer = soomlaInitializer;

            _uiViewLoader = uiViewLoader;

            _asyncProcessor = asyncProcessor;
        }

        bool _appVersionPass = false;
        public bool AppVersionPass
        {
            get
            {
                return _appVersionPass;
            }
            set
            {
                _appVersionPass = value;
            }
        }

        bool _newInstall = false;
        public bool NewInstall
        {
            get
            {
                return _newInstall;
            }
            set
            {
                _newInstall = value;
            }
        }

        // Use this for initialization
        public void Initialize()
        {
            UpdateLanguageSetting();

            AddEventListener();

            _soomlaInitializer.InitSoomlaStore();
        }

        public void Dispose()
        {
            RemoveEventListener();
        }

        void AddEventListener()
        {
            StoreEvents.OnSoomlaStoreInitialized += SoomlaStoreInitialized;
        }

        void RemoveEventListener()
        {
            StoreEvents.OnSoomlaStoreInitialized -= SoomlaStoreInitialized;
        }

        void SoomlaStoreInitialized()
        {
            _asyncProcessor.StartCoroutine(ShowMainMenuPage());
        }

        IEnumerator ShowMainMenuPage()
        {
            string playingDuration = PlayerPrefs.GetString("PP_STAT_PlayingDuration", "");
            NewInstall = _socialManager.ParseUserTokenExpired ? true : (playingDuration == "" ? true : double.Parse(playingDuration) == 0);

            yield return new WaitForSeconds(3);

            GameObject mainMenuView = _uiViewLoader.LoadPageView("MainMenuView", null);
        }

        public void ResetProgress()
        {
            PlayerPrefs.DeleteAll();

            foreach (Gallery girl in GalleryCore.Girls)
            {
                girl.RestoreGirlsStatus(0, 0, 1);
            }

#if UNITY_IOS
                    Soomla.KeyValueStorageIOS.Purge();
#elif UNITY_ANDROID
            Soomla.KeyValueStorageAndroid.Purge();
#elif UNITY_EDITOR
                    Soomla.KeyValueStorage.Purge();
#endif
        }

        public void VerifyAppVersion()
        {
            if (SocialManager.CheckForInternetConnection())
            {
                Action<bool> verifyCallback = (bool pass) =>
                {
                    EventManager.AppVersionVerifed(pass);
                };

                _versionCheckerManager.VerifyAppVersion(verifyCallback);
            }
            else
            {
                EventManager.AppVersionVerifed(true);
            }
        }

        public void GoogleSignIn()
        {
            GameObject overlay = _uiViewLoader.LoadOverlayView("CloudSavingOverlay", null, false, false);
            CloudSavingOverlayController signInOverlayController = overlay.GetComponent<CloudSavingOverlayController>();
            signInOverlayController.ShowSigninMessage();

            if (SocialManager.CheckForInternetConnection())
            {
                Action googleSignInSuccessCallback = () =>
                {
                    EventManager.GoogleSignInCallback(true);

                    Action parseSignInSuccessCallback = () =>
                    {
                        EventManager.ParseSignInCallback(true);

                        PushBehaviour.Instance.Init();

                        if (signInOverlayController != null)
                            signInOverlayController.CloseView();

                        Debug.LogError("ID : " + ParseUser.CurrentUser.ObjectId);
                    };

                    Action parseSignInFailCallback = () =>
                    {
                        EventManager.ParseSignInCallback(false);

                        if (signInOverlayController != null)
                            signInOverlayController.CloseView();
                    };

                    _socialManager.ParseSignInWithGoogle(parseSignInSuccessCallback, parseSignInFailCallback);
                };

                Action googleSignInFailCallback = () =>
                {
                    if (signInOverlayController != null)
                        signInOverlayController.ShowSigninFailMessage();

                    EventManager.GoogleSignInCallback(false);
                };

                _socialManager.GoogleSignIn(true, googleSignInSuccessCallback, googleSignInFailCallback);
            }
            else
            {
                if (signInOverlayController != null)
                    signInOverlayController.ShowSigninFailMessage();

                EventManager.GoogleSignInCallback(false);
            }
        }

        public void ParseSignUp()
        {
            Action parseSignUpSuccessCallback = () =>
            {
                PushBehaviour.Instance.Init();

                SaveLocalProgressToParse(true);
            };

            Action parseSignUpFailCallback = () =>
            {
                VerfiyAssetBundleVersion();
            };

            _socialManager.ParseSignUp(parseSignUpSuccessCallback, parseSignUpFailCallback);
        }

        public void ParseSignIn(string id, string password)
        {
            GameObject overlay = _uiViewLoader.LoadOverlayView("CloudSavingOverlay", null, false, false);
            CloudSavingOverlayController signInOverlayController = overlay.GetComponent<CloudSavingOverlayController>();
            signInOverlayController.ShowSigninMessage();

            if (SocialManager.CheckForInternetConnection())
            {
                Action parseSignInSuccessCallback = () =>
                {
                    EventManager.ParseSignInCallback(true);

                    PushBehaviour.Instance.Init();

                    if (signInOverlayController != null)
                        signInOverlayController.CloseView();
                };

                Action parseSignInFailCallback = () =>
                {
                    EventManager.ParseSignInCallback(false);

                    if (signInOverlayController != null)
                        signInOverlayController.ShowSigninFailMessage();
                };

                _socialManager.ParseSignIn(id, password, parseSignInSuccessCallback, parseSignInFailCallback);
            }
            else
            {
                EventManager.ParseSignInCallback(false);

                if (signInOverlayController != null)
                    signInOverlayController.ShowSigninFailMessage();
            }
        }

        public void LoadProgressFromParse()
        {
            Action successCallback = () =>
            {
                EventManager.SyncGameProgressCallback(true, false);
            };

            Action failCallback = () =>
            {
                EventManager.SyncGameProgressCallback(false, false);
            };

            _cloudDataManager.LoadProgressFromParse(successCallback, failCallback);

        }

        public void LoadProgressFromGoogleCloud()
        {
            Action successCallback = () =>
            {
                EventManager.SyncGameProgressCallback(true, false);
            };

            Action failCallback = () =>
            {
                EventManager.SyncGameProgressCallback(false, false);
            };

            _cloudDataManager.LoadProgressFromGoogleCloud(true, successCallback, failCallback);
        }

        public void SaveLocalProgressToParse(bool needCallback)
        {
            Action saveProgressToParseSuccessCallback = () =>
            {
                if(needCallback)
                    EventManager.SyncGameProgressCallback(true, true);
            };

            Action saveProgressToParseFailCallback = () =>
            {
                if(needCallback)
                    EventManager.SyncGameProgressCallback(false, true);
            };

            _cloudDataManager.SaveProgressToParseUser(false, saveProgressToParseSuccessCallback, saveProgressToParseFailCallback);
        }

        public void SaveCurrentSaveDataToParse(bool needCallback)
        {
            Action saveProgressToParseSuccessCallback = () =>
            {
                if(needCallback)
                    EventManager.SyncGameProgressCallback(true, true);
            };

            Action saveProgressToParseFailCallback = () =>
            {
                if(needCallback)
                    EventManager.SyncGameProgressCallback(false, true);
            };

            _cloudDataManager.SaveProgressToParseUser(true, saveProgressToParseSuccessCallback, saveProgressToParseFailCallback);
        }

        public void VerfiyAssetBundleVersion()
        {
            AssetBundleLoader.Instance.InitAssetBundle();
        }

        public void LoadMainScene()
        {
            Application.LoadLevel("MainScene");
        }

        void UpdateLanguageSetting()
        {
            string lang = PlayerPrefs.GetString(LanguageMenuViewController.Language_PP_KEY, "");

            if (lang != "")
            {
                LanguageManager.Instance.ChangeLanguage(lang);
                EventManager.ChangeLanguage();
            }
            else
            {
                if (Application.systemLanguage == SystemLanguage.ChineseTraditional || Application.systemLanguage == SystemLanguage.ChineseSimplified)
                    LanguageManager.Instance.ChangeLanguage("zh-cht");
                else if (Application.systemLanguage == SystemLanguage.Japanese)
                    LanguageManager.Instance.ChangeLanguage("ja");
                else if (Application.systemLanguage == SystemLanguage.Korean)
                    LanguageManager.Instance.ChangeLanguage("ko");
                else
                    LanguageManager.Instance.ChangeLanguage("en");

                EventManager.ChangeLanguage();
            }
        }
    }
}
