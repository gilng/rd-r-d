﻿using Ignite.AOM.Event;
using SmartLocalization;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.Analytics;
using Zenject;

namespace Ignite.AOM
{
    public class FriendManager : IInitializable, IDisposable
    {
        private static readonly ILogger Logger = Debug.logger;

        private IFriendService _friendService;
        private UIViewLoader _uiViewLoader;

        [Inject]
        public FriendManager(IFriendService friendService, UIViewLoader uiViewLoader)
        {
            _friendService = friendService;
            _uiViewLoader = uiViewLoader;
        }

        // Use this for initialization
        public void Initialize()
        {
            AddEventListener();
        }

        public void Dispose()
        {
            RemoveEventListener();
        }

        void AddEventListener()
        {
            GameStateEvents.GameReady += AddFriend;
            BranchManager.OnBranchDataReceive += AddFriend;
        }

        void RemoveEventListener()
        {
            GameStateEvents.GameReady -= AddFriend;
            BranchManager.OnBranchDataReceive -= AddFriend;
        }

        private void AddFriend()
        {
            // Following condiction need to be fullfilled before we can add new friends
            // 1) Game need to completely loaded

            if (GameManager.Instance != null)
            {
                if (!GameManager.Instance.GameReady) return;

                var friendUserId = BranchManager.instance.GetLoadedReferralFriendUserId();

                if (friendUserId != "")
                {
                    _friendService.AddFriend(
                        friendUserId,
                        friendRelation =>
                        {
                            EventManager.FriendTableRefresh();

                            var msgPopupView = _uiViewLoader.LoadOverlayView("MsgPopupOverlay", null, false,
                                Time.timeScale == 0);
                            var msgPopupViewController = msgPopupView.GetComponent<MsgPopupViewController>();
                            msgPopupViewController.InputMsg("fds_title_add",
                                string.Format(LanguageManager.Instance.GetTextValue("fds_msg_add_success"),
                                    BranchManager.instance.LoadedReferralFriendDisplayName != ""
                                        ? BranchManager.instance.LoadedReferralFriendDisplayName
                                        : friendUserId));

                        #if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                        Analytics.CustomEvent("inviteFriend", new Dictionary<string, object>
                        {
                            {"type", "AcceptByReferralLink"},
                        });
                        #endif
                        },
                        errorCode =>
                        {
                            string errorMsgKey = "";

                            if (errorCode == "FriendRelationAlreadyExists")
                                errorMsgKey = "fds_msg_add_fail_alreadyExist";
                            else if (errorCode == "UserFriendListReachedMaximum")
                                errorMsgKey = "fds_msg_add_fail_userListMax";
                            else if (errorCode == "TargetUserFriendListReachedMaximum")
                                errorMsgKey = "fds_msg_add_fail_friendListMax";
                            else if (errorCode == "TimeOut")
                                errorMsgKey = "errorMsg_TimeOut";
                            else
                                errorMsgKey = "fds_msg_add_fail_invalidTarget";

                            var msgPopupView = _uiViewLoader.LoadOverlayView("MsgPopupOverlay", null, false,
                                Time.timeScale == 0);
                            var msgPopupViewController = msgPopupView.GetComponent<MsgPopupViewController>();
                            msgPopupViewController.InputMsg("fds_title_add",
                                string.Format(LanguageManager.Instance.GetTextValue(errorMsgKey),
                                    BranchManager.instance.LoadedReferralFriendDisplayName != ""
                                        ? BranchManager.instance.LoadedReferralFriendDisplayName
                                        : friendUserId));
                        }
                    );
                }
            }
        }
    }
}