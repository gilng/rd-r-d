﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
	public class MenuManager : MonoBehaviour {

		public GameObject menuPanel;

		public virtual void ShowMenu(){
			menuPanel.SetActive(true);
			SoundManager.Instance.PlaySoundWithType(SoundType.PopupShow_1);
		}
		
		public virtual void HideMenu(bool haveSFX){	
			menuPanel.SetActive(false);
		}
	}
}