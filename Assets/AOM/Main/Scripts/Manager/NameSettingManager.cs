﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using SmartLocalization;

namespace Ignite.AOM
{
    public class NameSettingManager : MonoBehaviour
	{
        public string currentPlayerName;
        public InputField nameField;
        public Text placeHolder_Text;

        void Start()
        {
            currentPlayerName = PlayerPrefs.GetString("PLAYER_NAME", "Hero");
            placeHolder_Text.text = currentPlayerName + " (Name Here)";
        }

        public void ComfirmSetting()
        {			
			SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);
            try
            {
                if (nameField.text.Length > 0)
                    PlayerPrefs.SetString("PLAYER_NAME", nameField.text);
                else
                    PlayerPrefs.SetString("PLAYER_NAME", currentPlayerName);

                PlayerPrefs.Save();

                currentPlayerName = PlayerPrefs.GetString("PLAYER_NAME", currentPlayerName);
            }
            catch
            {
            }

            this.gameObject.SetActive(false);

            //GameManager.instance.ShowNoticeBoard();
        }

        public void Cancel()
        {
            this.gameObject.SetActive(false);

            //GameManager.instance.ShowNoticeBoard();
        }
	}
}
