﻿using UnityEngine;
using System.Collections;
using System;
using GoogleMobileAds.Api;
using Zenject;

namespace Ignite.AOM
{
    public class AdMobManager : MonoBehaviour
    {
        private EnemyCore _enemyCore;

        [Inject]
        public void ConstructorSafeAttribute(EnemyCore enemyCore)
        {
            _enemyCore = enemyCore;
        }

        private static AdMobManager _instance;
        public static AdMobManager Instance
        {
            get
            {
                return _instance;
            }
        }

        private InterstitialAd _interstitial;
        private double lastShowTime = 0;
        private const string PP_NO_ADMOB = "PP_NO_ADMOB";
        private bool shouldNoADs;

        bool firstAdInited = false;

#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-1105895181794672/1847973455";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-1105895181794672/4801439851";
#else
        string adUnitId = "unexpected_platform";
#endif

        void Awake()
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        // Use this for initialization
        void Start()
        {
            this.shouldNoADs = PlayerPrefs.GetString(PP_NO_ADMOB, "false").Contains("true");

            if (!this.shouldNoADs)
            {
                _interstitial = new InterstitialAd(adUnitId);

                _interstitial.OnAdClosed += OnAdClosed;

                _interstitial.OnAdLoaded += OnAdLoaded;

                _interstitial.OnAdFailedToLoad += OnAdFailedLoad;

                RequestInterstitial();

                //lastShowTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);

                
            }
        }

        public void InitFirstAd()
        {
            if (!this.shouldNoADs && !firstAdInited)
            {
                firstAdInited = true;
                lastShowTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now) - 590;
            }
        }

        private void RequestInterstitial()
        {
            // Initialize an InterstitialAd.
            //interstitial = new InterstitialAd(adUnitId);
            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            //AdRequest request = new AdRequest.Builder()
            //.AddTestDevice(AdRequest.TestDeviceSimulator)       // Simulator.
            //.AddTestDevice("2044F49D075D53177E15E234B910E351")  // My test device.
            //.Build();
            // Load the interstitial with the request.
            _interstitial.LoadAd(request);

//#if UNITY_IOS
//                        AdRequest request = new AdRequest.Builder ().AddTestDevice (GameTools.GetIOSAdMobID ()).Build ();
//                        // Load the interstitial with the request.
//                        _interstitial.LoadAd(request);
//#endif
//#if UNITY_ANDROID
//            _interstitial = new InterstitialAd(adUnitId);
//            _interstitial.AdClosed += OnAdClosed;
//            _interstitial.AdClosing += OnAdClosing;
//            _interstitial.AdLoaded += OnAdLoaded;
//            _interstitial.AdOpened += OnAdOpened;
//            AdRequest request = new AdRequest.Builder()
//                .AddTestDevice(AdRequest.TestDeviceSimulator)
//                    .AddTestDevice("E6CDB888557BA9A0") // My device ID
//                    .AddKeyword("game")
//                    .SetGender(Gender.Male)
//                    .SetBirthday(new DateTime(1985, 1, 1))
//                    .TagForChildDirectedTreatment(false)
//                    .AddExtra("color_bg", "9B30FF")
//                    .Build();
//            _interstitial.LoadAd(request);

//#endif
        }

        public bool ShouldNoADs
        {
            set
            {
                this.shouldNoADs = value;
                PlayerPrefs.SetString(PP_NO_ADMOB, value == true ? "true" : "false");
            }
            get 
            { 
                return this.shouldNoADs; 
            }
        }

        public void ShowInterstitial()
        {
            if (!this.shouldNoADs && firstAdInited)
            {
                if (_enemyCore.CurrentEnemyType != EnemyType.BigBoss && _enemyCore.CurrentEnemyType != EnemyType.MiniBoss)
                {
                    //Debug.Log("ShowInterstitial: " + (UnitConverter.ConvertToTimestamp(System.DateTime.Now) - lastShowTime) + " >= 600 ?");
                    if (lastShowTime != 0 && UnitConverter.ConvertToTimestamp(System.DateTime.Now) - lastShowTime >= 600)
                    {
                        Debug.Log("show ad");
                        if (_interstitial != null)
                        {
                            if (_interstitial.IsLoaded())
                            {
                                _interstitial.Show();
                                lastShowTime = 0;
                            }
//                            else
//                            {
//#if UNITY_EDITOR
//                                Debug.Log("AdMob not loaded yet!");
//#endif
//                            }
                        }
//                        else
//                        {
//#if UNITY_EDITOR
//                            Debug.Log("_interstitial = null");
//#endif
//                        }
                    }
                }
            }
        }

        //Events handlers
        void OnAdClosed(object sender, System.EventArgs e)
        {
            //Debug.Log(string.Format("AdMob.AdClosed - OK. sender: {0}, eventArgs: {1}", sender, e));
            //if (_callback != null) _callback();
            //else Debug.Log("_callback is null");
            RequestInterstitial();
            lastShowTime = UnitConverter.ConvertToTimestamp(System.DateTime.Now);
        }

        void OnAdClosing(object sender, System.EventArgs e)
        {
            //Debug.Log(string.Format("AdMob.AdClosing - OK. sender: {0}, eventArgs: {1}", sender, e));
        }

        void OnAdFailedLoad(object sender, AdFailedToLoadEventArgs e)
        {
            //Debug.Log(string.Format("AdMob.AdFailedLoad - OK. sender: {0}, eventArgs: {1}", sender, e));
        }

        void OnAdLeftApplication(object sender, System.EventArgs e)
        {
            //Debug.Log(string.Format("AdMob.AdLeftApplication - OK. sender: {0}, eventArgs: {1}", sender, e));
        }

        private void OnAdLoaded(object sender, System.EventArgs e)
        {
            //Debug.Log(string.Format("AdMob.AdLoaded - OK. sender: {0}, eventArgs: {1}", sender, e));
        }

        private void OnAdOpened(object sender, System.EventArgs e)
        {
            //Debug.Log(string.Format("AdMob.AdOpened - OK. sender: {0}, eventArgs: {1}", sender, e));
        }
    }
}
