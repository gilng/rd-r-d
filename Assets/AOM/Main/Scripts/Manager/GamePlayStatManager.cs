﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;
using System.Linq;
using Zenject;

namespace Ignite.AOM
{
    public enum StatType
    {
        //Achievements
        InviteFriends,
        ReachHighestStage,
        CollectRelic,
        //CollectRuby,
        Tap,
        CollectGold,
        KillMonster,
        OwnArtifact,
        ReachHeroDPS,
        KillBoss,
        Prestige,
        LostGoldFromPrestoge,
        LevelUpHeroTimes,
        OpenChest,
        GetFairyPresent,
        UseJumpAttack,
        GetCriticalHit,
        //Total Stat
        FirstPlayTime,//timestamp
        PlayingDuration, // In Second 
        //Current Stat (real time cal???)
        CurrentTotalHeroLevel,
        CurrentTapChance, //TODO
        CurrentDPSMultiplier,//TODO
        CurrentCriticalHitMultiplier,//TODO
        CurrentTotalGoldMultiplier,//TODO
        LastPrestigeTime,//TODO
        MaxPvPAddedFriend,
        MaxPvPScore,
        MaxPvPTapSpeed
    }

    public class GamePlayStatManager
    {
        private IDictionary<StatType, double> _record;// total record
        private const string STAT_PP_PREFIX = "PP_STAT_";
        private static GamePlayStatManager instance;
        private StringBuilder sb;

        public GamePlayStatManager()
        {
            sb = new StringBuilder();
            _record = new Dictionary<StatType, double>();
            foreach (StatType type in Enum.GetValues(typeof(StatType)).Cast<StatType>())
            {
                sb.Clear();
                sb.Append(STAT_PP_PREFIX);
                sb.Append(Enum.GetName(typeof(StatType), type));
                _record[type] = double.Parse(PlayerPrefs.GetString(sb.ToString(), "0"));
            }
            if (_record[StatType.FirstPlayTime] == 0)
            {
                SetVal(StatType.FirstPlayTime, UnitConverter.ConvertToTimestamp(DateTime.Now));
            }
            if (_record[StatType.LastPrestigeTime] == 0)
            {
                SetVal(StatType.LastPrestigeTime, UnitConverter.ConvertToTimestamp(DateTime.Now));
            }
            if (_record[StatType.ReachHighestStage] == 0)
            {
                SetVal(StatType.ReachHighestStage, 1);
            }
        }


        public static GamePlayStatManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GamePlayStatManager();
                }
                return instance;
            }
        }

        public IDictionary<StatType, double> Stat
        {
            get { return _record; }
        }

        private void AddVal(StatType type, double val)
        {
            SetVal(type, _record[type] + val);
        }

        private void SetVal(StatType type, double val)
        {
            _record[type] = val;
            sb.Clear();
            sb.Append(STAT_PP_PREFIX);
            sb.Append(Enum.GetName(typeof(StatType), type));

            PlayerPrefs.SetString(sb.ToString(), _record[type].ToString());

            CheckAchievementRewards(type);
        }

        public void CheckPendingRewards()
        {
            foreach (StatType type in Enum.GetValues(typeof(StatType)).Cast<StatType>())
            {
                CheckAchievementRewards(type);
            }
        }

        private void CheckAchievementRewards(StatType type)
        {
            if (AchievementAssets.AchievementByStatType.ContainsKey(type))
            {
                AchievementVG acheivement = AchievementAssets.AchievementByStatType[type];
                //				Debug.Log ("Acheivement Rewards: "+ Enum.GetName(typeof(StatType), type)+" val: "+_record[type] +" target: "+acheivement.PriceForOne);
                if (acheivement.CanAffordOne())
                {
                    EventManager.NewAchievementRecord(type);
                    //					Debug.Log ("New Acheivement Rewards: "+ Enum.GetName(typeof(StatType), type));
                }
            }
        }

        public void RestoreCloudSavedGameplayState(SaveData loadedSaveData)
        {
            SetVal(StatType.InviteFriends, loadedSaveData.inviteFriends);
            SetVal(StatType.ReachHighestStage, loadedSaveData.reachHighestStage);
            SetVal(StatType.CollectRelic, loadedSaveData.collectRelic);
            //SetVal(StatType.CollectRuby, loadedSaveData.collectRuby);
            SetVal(StatType.Tap, loadedSaveData.tap);
            SetVal(StatType.CollectGold, loadedSaveData.collectGold);
            SetVal(StatType.KillMonster, loadedSaveData.killMonster);
            SetVal(StatType.OwnArtifact, loadedSaveData.ownArtifact);
            SetVal(StatType.ReachHeroDPS, loadedSaveData.reachHeroDPS);
            SetVal(StatType.KillBoss, loadedSaveData.killBoss);
            SetVal(StatType.Prestige, loadedSaveData.prestige);
            SetVal(StatType.LostGoldFromPrestoge, loadedSaveData.lostGoldFromPrestige);
            SetVal(StatType.LevelUpHeroTimes, loadedSaveData.levelUpHeroTimes);
            SetVal(StatType.OpenChest, loadedSaveData.openChest);
            SetVal(StatType.GetFairyPresent, loadedSaveData.getFairyPresent);
            SetVal(StatType.UseJumpAttack, loadedSaveData.useJumpAttack);
            SetVal(StatType.GetCriticalHit, loadedSaveData.getCriticalHit);
            SetVal(StatType.FirstPlayTime, loadedSaveData.firstPlayTime);
            SetVal(StatType.PlayingDuration, loadedSaveData.playingDuration);
            SetVal(StatType.CurrentTotalHeroLevel, loadedSaveData.currentTotalHeroLevel);
            SetVal(StatType.CurrentTapChance, loadedSaveData.currentTapChance);
            SetVal(StatType.CurrentDPSMultiplier, loadedSaveData.currentDPSMultiplier);
            SetVal(StatType.CurrentCriticalHitMultiplier, loadedSaveData.currentCriticalHitMultiplier);
            SetVal(StatType.CurrentTotalGoldMultiplier, loadedSaveData.currentTotalGoldMultiplier);
            SetVal(StatType.LastPrestigeTime, loadedSaveData.lastPrestigeTime);
        }

        public void InviteFriend(int numberOfFriend)
        {
            AddVal(StatType.InviteFriends, numberOfFriend);
        }

        public void CriticalHit()
        {
            AddVal(StatType.GetCriticalHit, 1);
        }

        public void JumpAttack()
        {
            AddVal(StatType.UseJumpAttack, 1);
        }

        public void FairyPresent()
        {
            AddVal(StatType.GetFairyPresent, 1);
        }

        public void Tap()
        {
            AddVal(StatType.Tap, 1);
        }

        public void EnemyKillWithType(EnemyType type)
        {
            AddVal(StatType.KillMonster, 1);

            if (EnemyType.BigBoss == type || EnemyType.MiniBoss == type)
                AddVal(StatType.KillBoss, 1);
            else if (EnemyType.TreasureChest == type)
                AddVal(StatType.OpenChest, 1);
        }

        public bool reachedHighestStage = false;
        public void ReachHighestStage(int val)
        {
            if (!reachedHighestStage)
                reachedHighestStage = val > _record[StatType.ReachHighestStage];

            if (val > _record[StatType.ReachHighestStage])
            {
                SetVal(StatType.ReachHighestStage, val);

                //if (_socialManager != null)
                //    _socialManager.ReportScore(val);
            }
        }

        public void ReachHeroDPS(double val)
        {
            if (val > _record[StatType.ReachHeroDPS])
                SetVal(StatType.ReachHeroDPS, val);
        }

        public void CollectGold(double val)
        {
            if (val > _record[StatType.CollectGold])
                SetVal(StatType.CollectGold, val);
        }

        public void CollectRelic(double val)
        {
            if (val > _record[StatType.CollectRelic])
                SetVal(StatType.CollectRelic, val);
        }

        //public void CollectRuby(double val)
        //{
        //    if (val > _record[StatType.CollectRuby])
        //        SetVal(StatType.CollectRuby, val);
        //}

        public void OwnArtifact(double val)
        {
            //			Debug.Log("SetVal "+val+" "+_record[StatType.OwnArtifact] );
            if (val > _record[StatType.OwnArtifact])
                SetVal(StatType.OwnArtifact, val);
        }

        public void Prestige()
        {
            AddVal(StatType.Prestige, 1);
            SetVal(StatType.CurrentTotalHeroLevel, 0);
            SetVal(StatType.LastPrestigeTime, UnitConverter.ConvertToTimestamp(DateTime.Now));
        }

        public void LevelUpHero(double val)
        {
            AddVal(StatType.LevelUpHeroTimes, val);
            AddVal(StatType.CurrentTotalHeroLevel, val);
        }

        public void AddPlayingDuration(double second)
        {
            //			Debug.Log ("AddPlayingDuration seconds: "+ second);
            AddVal(StatType.PlayingDuration, second);
        }

        public void LostGoldFromPrestige(double val)
        {
            AddVal(StatType.LostGoldFromPrestoge, val);
        }

        public bool reachedMaxPvPAddedFriend = false;
        public void ReachedMaxPvPAddedFriend(double val)
        {
            if (val > _record[StatType.MaxPvPAddedFriend])
            {
                reachedMaxPvPAddedFriend = true;
                SetVal(StatType.MaxPvPAddedFriend, val);
            }
        }

        public bool reachedMaxPvPScore = false;
        public void ReachedMaxPvPScore(double val)
        {
            if (val > _record[StatType.MaxPvPScore])
            {
                reachedMaxPvPScore = true;
                SetVal(StatType.MaxPvPScore, val);
            }
        }

        public bool reachedMaxPvPTapSpeed = false;
        public void ReachedMaxPvPTapSpeed(double val)
        {
            if (val > _record[StatType.MaxPvPTapSpeed])
            {
                reachedMaxPvPTapSpeed = true;
                SetVal(StatType.MaxPvPTapSpeed, val);
            }
        }
    }
}