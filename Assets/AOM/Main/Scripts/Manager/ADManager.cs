﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

namespace Ignite.AOM
{
    public enum VideoAdRewardType
    {
        UltimateGold,
        UltimateDiamond,
        UltimateRubQuota,
        UltimateRuby
    }

    public class ADManager : MonoBehaviour
    {
        private static ADManager _instance;

        public static ADManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public bool videoAdsPlaying = false;

        void Awake()
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        public bool CheckAdReady()
        {
            bool isReady = false;

            isReady = Advertisement.IsReady();

            return isReady;
        }

        public void ShowAd()
        {
            if (Advertisement.IsReady())
            {
                Advertisement.Show();
            }
        }

        public bool CheckRewardedAdReady()
        {
            bool isReady = false;

            isReady = Advertisement.IsReady("rewardedVideo");

            return isReady;
        }

        public void ShowRewardedAd()
        {
            if (Advertisement.IsReady("rewardedVideo"))
            {
                var options = new ShowOptions { resultCallback = HandleShowResult };
                Advertisement.Show("rewardedVideo", options);

                videoAdsPlaying = true;
            }
        }

        private void HandleShowResult(ShowResult result)
        {
            switch (result)
            {
                case ShowResult.Finished:
                    Debug.Log("The ad was successfully shown.");
                    // YOUR CODE TO REWARD THE GAMER
                    // Give coins etc.
                    EventManager.VideoAdFinished();
                    break;

                case ShowResult.Skipped:
                    Debug.Log("The ad was skipped before reaching the end.");
                    EventManager.VideoAdSkipped();
                    break;

                case ShowResult.Failed:
                    Debug.LogError("The ad failed to be shown.");
                    EventManager.VideoAdSkipped();
                    break;
            }

            videoAdsPlaying = false;
        }

        void OnApplicationFocus(bool focusStatus)
        {
            if (focusStatus)
            {
                if (videoAdsPlaying && !Advertisement.isShowing)
                {
                    Debug.Log("The ad was skipped before reaching the end.");
                    EventManager.VideoAdSkipped();

                    videoAdsPlaying = false;
                }
            }
        }
    }
}
