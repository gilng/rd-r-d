﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using SmartLocalization;

namespace Ignite.AOM
{
    public class FriendChallengeManager : MonoBehaviour
    {
        private static FriendChallengeManager _instance;
        public static FriendChallengeManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public GameObject bgm;

        private System.Text.StringBuilder sb = new System.Text.StringBuilder();

        // Use this for initialization
        void Awake()
        {
            _instance = this;

            EventManager.OnChallengeScoreBalanceUpdated += RefreshChallengeScoreText;

            SoundManager.Instance.bgm = this.bgm.GetComponent<AudioSource>();
            SoundManager.Instance.FixBGM();
        }

        void Start()
        {
            CheckLanguageVersion();
        }

        public void CheckLanguageVersion()
        {
            string lang = PlayerPrefs.GetString(LanguageMenuViewController.Language_PP_KEY, "");
            if (lang != "")
            {
                LanguageManager.Instance.ChangeLanguage(lang);
                EventManager.ChangeLanguage();
            }
            else
            {
                if (Application.systemLanguage == SystemLanguage.ChineseTraditional || Application.systemLanguage == SystemLanguage.ChineseSimplified)
                    LanguageManager.Instance.ChangeLanguage("zh-cht");
                else if (Application.systemLanguage == SystemLanguage.Japanese)
                    LanguageManager.Instance.ChangeLanguage("ja");
                else if (Application.systemLanguage == SystemLanguage.Korean)
                    LanguageManager.Instance.ChangeLanguage("ko");
                else
                    LanguageManager.Instance.ChangeLanguage("en");

                EventManager.ChangeLanguage();
            }
        }

        void RefreshChallengeScoreText()
        {
        }

        void OnDestroy()
        {
            EventManager.OnChallengeScoreBalanceUpdated -= RefreshChallengeScoreText;
        }
    }
}
