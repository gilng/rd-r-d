﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
	public class BossTextEffectManager : MonoBehaviour
	{
		public GameObject effectTarget;
		public Animator effectAnimator;

		public void EffectPlay (int animeSelect)
		{
            //if (effectTarget.activeSelf == false)
            //    effectTarget.SetActive(true);

			effectAnimator.SetInteger("AnimeSelect", animeSelect);
			effectAnimator.Play("BossTextEffect_Stay");
		}
	
		public void EffectOff ()
		{
            //if (effectTarget.activeSelf)
            //    effectTarget.SetActive(false);

			effectAnimator.SetInteger("AnimeSelect", 0);
		}
	}
}