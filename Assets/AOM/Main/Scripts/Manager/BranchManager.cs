﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using Facebook.Unity;
// IOS use
using SmartLocalization;
using UnityEngine;
using Zenject;

namespace Ignite.AOM
{
    // TODO Refractor this to BranchService and use Zenject
    public class BranchManager : MonoBehaviour
    {
        private UIViewLoader _uiViewLoader;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader)
        {
            _uiViewLoader = uiViewLoader;
        }

        public static BranchManager instance;

        BranchUniversalObject loadedUniversalObject;

        public static event Action OnBranchDataReceive;

        public static void BranchDataReceive()
        {
            if (OnBranchDataReceive != null)
            {
                OnBranchDataReceive();
            }
        }

#if UNITY_IPHONE
        [DllImport("__Internal")]
        public static extern void _copyTextToClipboard(string text);
#endif


        void Awake()
        {
            instance = this;

            DontDestroyOnLoad(this.gameObject);

            Branch.initSession(CallbackWithBranchUniversalObject);
        }

        public void CallbackWithBranchUniversalObject(BranchUniversalObject universalObject,
            BranchLinkProperties linkProperties, string error)
        {
            if (error == null)
            {
                if (universalObject != null && linkProperties != null)
                {
                    loadedUniversalObject = universalObject;

                    BranchDataReceive();
                }
                else
                {
                    ShowInvalidTargetPopup();
                }
            }
//            else
//            {
//                ShowInvalidTargetPopup();
//            }
        }

        void ShowInvalidTargetPopup()
        {
            var msgPopupView = _uiViewLoader.LoadOverlayView("MsgPopupOverlay", null, false,
                Time.timeScale == 0);
            var msgPopupViewController = msgPopupView.GetComponent<MsgPopupViewController>();
            msgPopupViewController.InputMsg(LanguageManager.Instance.GetTextValue("fds_title_add"),
                LanguageManager.Instance.GetTextValue("fds_msg_add_fail_invalidTarget"));
        }

        public string LoadedReferralFriendDisplayName { get; set; }

        public string GetLoadedReferralFriendUserId()
        {
            string loadReferralFriendUserId = "";

            if (loadedUniversalObject != null)
            {
                if (loadedUniversalObject.metadata.ContainsKey("IGNITE_CHECK"))
                {
                    if (loadedUniversalObject.metadata["IGNITE_CHECK"] == "TRUE")
                    {
                        if (loadedUniversalObject.metadata.ContainsKey("REFERRAL_USER_ID"))
                        {
                            loadReferralFriendUserId = loadedUniversalObject.metadata["REFERRAL_USER_ID"];

                            Debug.Log("LoadReferralFriendUserId - " + loadReferralFriendUserId);

                            if (loadedUniversalObject.metadata.ContainsKey("REFERRAL_DISPLAY_NAME"))
                            {
                                LoadedReferralFriendDisplayName =
                                    loadedUniversalObject.metadata["REFERRAL_DISPLAY_NAME"];

                                Debug.Log("REFERRAL_DISPLAY_NAME - " + loadReferralFriendUserId);
                            }
                            else
                            {
                                LoadedReferralFriendDisplayName = "";
                            }
                        }
                        else
                        {
                            LoadedReferralFriendDisplayName = "";
                        }
                    }
                }

                loadedUniversalObject = null;
            }
            else
            {
                LoadedReferralFriendDisplayName = "";
            }

            return loadReferralFriendUserId;
        }

        public static void ReferralLinkShareVia(string referralID, string displayName, int index)
        {
            BranchUniversalObject universalObject = new BranchUniversalObject();
            universalObject.canonicalIdentifier = "ignite/attackonmoe";
            universalObject.title = "Attack On Moe";
            universalObject.contentDescription = LanguageManager.Instance.GetTextValue("pvp_referral_msg");
            universalObject.metadata.Add("IGNITE_CHECK", "TRUE");
            universalObject.metadata.Add("REFERRAL_USER_ID", referralID);
            universalObject.metadata.Add("REFERRAL_DISPLAY_NAME", displayName);

            string[] shareVia = new string[] {"LINE", "Twitter", "Whatsapp", "Copy", "Email", "Sms", "Facebook"};

            BranchLinkProperties linkProperties = new BranchLinkProperties();
            linkProperties.feature = "sharing";
            linkProperties.channel = shareVia[index];

            Branch.getShortURL(universalObject, linkProperties, (url, error) =>
            {
                if (error == null)
                {
                    string path = "";
                    string nextMsg = LanguageManager.Instance.GetTextValue("pvp_referral_msg") + "\n" +
                                     LanguageManager.Instance.GetTextValue("pvp_referral_subtitle_1") + " : " +
                                     referralID + "\n";

                    switch (index)
                    {
                        case 0: // LINE
                            path = "http://line.me/R/msg/text/?" + url;
                            Application.OpenURL(path);
                            break;
                        case 1: // Twitter
                            path = "http://twitter.com/intent/tweet?text=" + WWW.EscapeURL(url);
                            Application.OpenURL(path);
                            break;
                        case 2: // Whatsapp
                            path = "whatsapp://send?text=" + url;
                            Application.OpenURL(path);
                            break;
                        case 3: // Copy
                            path = url;
                            CopyTextToClipboard(nextMsg + path);
                            break;
                        case 4: // Email
                            path = "mailto:?subject=" + universalObject.title + "&body=" + url;
                            Application.OpenURL(path);
                            break;
                        case 5: // Sms
#if UNITY_ANDROID
                            path = "sms:?body="+ nextMsg + url;
#elif UNITY_IOS
                            path = "sms:&body=" + System.Uri.EscapeDataString(nextMsg + url);
#endif
                            Application.OpenURL(path);
                            break;
                        case 6: // Facebook
                            FB.ShareLink(new Uri(url), LanguageManager.Instance.GetTextValue("game_name"), nextMsg);
                            break;
                    }

                    Branch.userCompletedAction("Share by " + shareVia[index]);
                }
            });
        }

        public static void CopyTextToClipboard(string str)
        {
#if UNITY_ANDROID
            AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = jc.GetStatic<AndroidJavaObject>("currentActivity");

            activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject clipboardManager = activity.Call<AndroidJavaObject>("getSystemService", "clipboard");
                AndroidJavaClass clipDataClass = new AndroidJavaClass("android.content.ClipData");
                AndroidJavaObject clipData = clipDataClass.CallStatic<AndroidJavaObject>("newPlainText", "simple text", str);
                clipboardManager.Call("setPrimaryClip", clipData);
            }));
#elif UNITY_IOS
            _copyTextToClipboard(str);
#endif
        }


        //private List<BranchUniversalObject> buoQueue = new List<BranchUniversalObject>();
        //public List<BranchUniversalObject> BuoQueue { get { return buoQueue; } }
        //
        //private List<BranchLinkProperties> blpQueue = new List<BranchLinkProperties>();
        //public List<BranchLinkProperties> BlpQueue { get { return blpQueue; } }
        //
        //public void LoadLinkData()
        //{
        //    if (buoQueue.Count > 0 && blpQueue.Count > 0)
        //    {
        //        if (buoQueue[0].metadata.ContainsKey("IGNITE_CHECK"))
        //        {
        //            if (buoQueue[0].metadata["IGNITE_CHECK"] == "TRUE")
        //            {
        //                GameObject referralMenuView = _uiViewLoader.LoadOverlayView("ReferralMenuView", null, false, false);
        //                ReferralViewController referralViewController = referralMenuView.GetComponent<ReferralViewController>();
        //
        //                if (buoQueue[0].metadata.ContainsKey("REFERRAL_USER_ID"))
        //                    referralViewController.referralMsg.text += "\n\nReceive: \n" + buoQueue[0].metadata["REFERRAL_USER_ID"] + "\n";
        //
        //                //// Testing
        //                //if (buoQueue.Count > 0)
        //                //{
        //                //    rvController.referralMsg.text += "\n--- buoQueue Count: " + buoQueue.Count + " , metadata: " + buoQueue[0].metadata.Count + " ---\n";
        //                //
        //                //    foreach (string d in buoQueue[0].metadata.Keys)
        //                //    {
        //                //        rvController.referralMsg.text += "\n>>> " + d;
        //                //    }
        //                //}
        //                //
        //                //if (blpQueue.Count > 0)
        //                //{
        //                //    rvController.referralMsg.text += "\n\n--- blpQueue Count: " + blpQueue.Count + " , controlParams: " + blpQueue[0].controlParams.Count + " ---\n";
        //                //
        //                //    foreach (string d in blpQueue[0].controlParams.Keys)
        //                //    {
        //                //        rvController.referralMsg.text += "\n>>> " + d;
        //                //    }
        //                //}
        //            }
        //        }
        //
        //        buoQueue.Clear();
        //        blpQueue.Clear();
        //    }
        //}
    }
}