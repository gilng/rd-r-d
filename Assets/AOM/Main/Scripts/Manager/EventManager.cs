﻿using UnityEngine;
using System.Collections;
using System;

namespace Ignite.AOM
{
    public delegate void AppVersionVerifyHandler(bool pass);

    public delegate void ParseSignInHandler(bool success);

    public delegate void GoogleSignInHandler(bool success);

    public delegate void SyncGameProgressHandler(bool success, bool allowSyncFail);

    public delegate void VerfiyAssetBundleVersionHandler();

    public delegate void UpdateAssetDownloadProgressHandler(int loadedBundleCount, float progress);

    public delegate void AssetBundleDownloadHandler(bool success);


    public delegate void LanguageChangeHandler();

    public delegate void TriggerTutorial(int tutorialId);

    public delegate void TutorialCompleted(int tutorialId);

    public delegate void LevelUpVGBalanceChangeHandler(LevelUpVG vg, int balance);

    public delegate void GoldPriceUpdatedHandler();

    public delegate void UpgradeCostReducedHandler();

    public delegate void RelicPriceUpdatedHandler();

    public delegate void DiamondPriceUpdatedHandler();

    public delegate void ArtifactVGBalanceChangeHandler(ArtifactVG vg, int balance);

    public delegate void GameItemVGBalanceChangeHandler();

    public delegate void AchievementVGBalanceChangeHandler();

    public delegate void ArtifactAllDamageBuffTotalChangeHandler(double newVal);

    public delegate void HeroSkillBuffTotalChangeHandler(EffectType t, double newVal);

    public delegate void HeroDeadHandler(int heroIdx, DateTime reviveTime);

    public delegate void HeroReviveHandler(int heroIdx);

    public delegate void PrestigeCompleteHandler();

    public delegate void PrestigeHandler();

    public delegate void SuperPrestigeHandler();

    public delegate void GoldBalanceUpdatedHandler();

    public delegate void RelicBalanceUpdatedHandler();

    public delegate void DiamondBalanceUpdatedHandler();

    public delegate void RubyBalanceUpdatedHandler();

    public delegate void ChallengeScoreBalanceUpdatedHandler();

    public delegate void RelicValueHandler(int value);

    public delegate void DiamondValueHandler(int value);

    public delegate void RubyValueHandler(int value);

    public delegate void ChallengeScoreValueHandler(int value);

    //	public delegate void MakeItRainHandler ();

    public delegate void RefreshSkillItemActivedHandler();

    public delegate void PowerOfHoldingItemActivedHandler();

    public delegate void DoomItemActivedHandler();

    public delegate void PowerOfHoldingCounterHandler(string displayTime);

    public delegate void EnemyShouldDropRelicHandler();

    public delegate void EnemyTypeChangeHandler(EnemyType t);

    //public delegate void ClearStageHandler();

    public delegate void BGSwitchHandler();

    public delegate void FairyLeaveHandler(bool val);

    public delegate void FairyADCollectRewardHandler(VideoAdRewardType type, bool doubleReward);

    public delegate void FairtADIgnoredHandler();

    public delegate void HiddenMenuShowHandler(bool show);

    public delegate void HeroLevelUpHandler(string heroId, int level);

    public delegate void HeroUnlockSkillHandler(string heroId, string skillId);

    public delegate void NewAchievementRecordHandler(StatType type);

    public delegate void NewGalleryReachHandler();

    public delegate void FriendInvitedHandler();

    public delegate void SalvageHandler();

    public delegate void ReloadTebleRow();

    public delegate void GalleryUpdate(string id);

    public delegate void GalleryCharacterButtonUpdate(int id);

    public delegate void GalleryCloseOverlay();

    public delegate void GalleryMoeSpiritUpdate();

    public delegate void VideoAdFinished();

    public delegate void VideoAdSkipped();

    public delegate void PromotionalPackOfferTime(bool isShow, int currentStage);

    public delegate void PromotionalPackPurchased(string itemPackId);

    public delegate void ParseDataLoaded();

    public delegate void ParseUserInvalidToken();

    public delegate void ParseFriendRelationError(string errorCode);

    public delegate void TableRefresh();

    public delegate void FriendTableRefresh();

    public class EventManager
    {
        public static event AppVersionVerifyHandler OnAppVersionVerifed;

        public static event ParseSignInHandler OnParseSignIn;
        public static event GoogleSignInHandler OnGoogleSignIn;

        public static event SyncGameProgressHandler OnSyncGameProgress;

        public static event VerfiyAssetBundleVersionHandler OnVerfiyAssetBundleVersion;
        public static event UpdateAssetDownloadProgressHandler OnUpdateAssetDownloadProgress;
        public static event AssetBundleDownloadHandler OnAssetBundleDownload;

        public static event LanguageChangeHandler OnLanguageChange;

        // Tutorial Event
        public static event TriggerTutorial OnTriggerTutorial;

        public static event TutorialCompleted OnTutorialCompleted;

        // Buff total value change event
        public static event ArtifactAllDamageBuffTotalChangeHandler OnArtifactAllDamageBuffTotalChange;

        public static event HeroSkillBuffTotalChangeHandler OnHeroSkillBuffTotalChange;

        // Vg balance updated Event
        public static event LevelUpVGBalanceChangeHandler OnLevelUpVGBalanceChange;

        public static event ArtifactVGBalanceChangeHandler OnArtifactVGBalanceChange;
        public static event GameItemVGBalanceChangeHandler OnGameItemVGBalanceChange;
        public static event AchievementVGBalanceChangeHandler OnAchievementVGBalanceChange;

        // Hero dead/revive, level up, unlock skill Event
        public static event HeroDeadHandler OnHeroDead;

        public static event HeroReviveHandler OnHeroRevive;
        public static event HeroLevelUpHandler OnHeroLevelUp;
        public static event HeroUnlockSkillHandler OnHeroUnlockSkill;

        // Prestige Event
        public static event PrestigeCompleteHandler OnPrestigeComplete;

        public static event PrestigeHandler OnPrestige;
        public static event SuperPrestigeHandler OnSuperPrestige;

        // Price updated Event
        public static event GoldPriceUpdatedHandler OnGoldPriceUpdated;

        public static event UpgradeCostReducedHandler OnUpgradeCostReduced;
        public static event RelicPriceUpdatedHandler OnRelicPriceUpdated;
        public static event DiamondPriceUpdatedHandler OnDiamondPriceUpdated;

        // Currency Balances updated Event
        public static event GoldBalanceUpdatedHandler OnGoldBalanceUpdated;

        public static event RelicBalanceUpdatedHandler OnRelicBalanceUpdated;
        public static event DiamondBalanceUpdatedHandler OnDiamondBalanceUpdated;
        public static event RubyBalanceUpdatedHandler OnRubyBalanceUpdated;
        public static event ChallengeScoreBalanceUpdatedHandler OnChallengeScoreBalanceUpdated;

        public static event RelicValueHandler OnRelicValueHandler;
        public static event DiamondValueHandler OnDiamondValueHandler;
        public static event RubyValueHandler OnRubyValueHandler;
        public static event ChallengeScoreValueHandler OnChallengeScoreValueHandler;

        // Use GameItems Event
        //		public static event MakeItRainHandler OnMakeItRainHandler;
        public static event RefreshSkillItemActivedHandler OnRefreshSkillItemActive;

        public static event PowerOfHoldingItemActivedHandler OnPowerOfHoldingItemActive;
        public static event DoomItemActivedHandler OnDoomItemActive;

        public static event PowerOfHoldingCounterHandler OnPowerOfHoldingCounting;

        // Enemy should drop relic event
        public static event EnemyShouldDropRelicHandler OnEnemyShouldDropRelic;

        // On Game Event
        public static event EnemyTypeChangeHandler OnEnemyTypeChange;

        //public static event ClearStageHandler OnClearStage;
        public static event BGSwitchHandler OnBGSwitch;

        // Fairy Event
        public static event FairyLeaveHandler OnFairyLeave;

        public static event FairyADCollectRewardHandler OnCollectReward;
        public static event FairtADIgnoredHandler OnIgnoredReward;

        // Show or Hide hidden menu event
        public static event HiddenMenuShowHandler OnHiddenMenuShow;

        // New Achievement Record event
        public static event NewAchievementRecordHandler OnNewAchievementRecord;

        // New Gallery Reach
        public static event NewGalleryReachHandler OnNewGalleryReach;

        // New Friend Invite
        public static event FriendInvitedHandler OnFriendInvited;

        // Salvage artifacts
        public static event SalvageHandler OnSalvageHandler;

        public static event ReloadTebleRow OnReloadTebleRow;

        // Gallery
        public static event GalleryUpdate OnGalleryUpdate;

        public static event GalleryCharacterButtonUpdate OnGalleryCharacterButtonUpdate;
        public static event GalleryCloseOverlay OnGalleryCloseOverlay;
        public static event GalleryMoeSpiritUpdate OnGalleryMoeSpiritUpdate;

        // Video Ad
        public static event VideoAdFinished OnVideoAdFinished;

        public static event VideoAdSkipped OnVideoAdSkipped;

        // Promotional Pack Purchase
        public static event PromotionalPackOfferTime OnPromotionalPackOfferTime;

        public static event PromotionalPackPurchased OnPromotionalPackPurchased;

        // Parse Data loaded
        public static event ParseDataLoaded OnParseDataLoaded;

        // Parse User Session Token Invalid
        public static event ParseUserInvalidToken OnParseUserInvalidToken;

        public static event ParseFriendRelationError OnParseFriendRelationError;

        public static event TableRefresh OnTableRefresh;

        public static event FriendTableRefresh OnFriendTableRefresh;

        public static void AppVersionVerifed(bool pass)
        {
            if (OnAppVersionVerifed != null)
                OnAppVersionVerifed(pass);
        }

        public static void ParseSignInCallback(bool success)
        {
            if (OnParseSignIn != null)
                OnParseSignIn(success);
        }

        public static void GoogleSignInCallback(bool success)
        {
            if (OnGoogleSignIn != null)
                OnGoogleSignIn(success);
        }

        public static void SyncGameProgressCallback(bool success, bool allowSyncFail)
        {
            if (OnSyncGameProgress != null)
                OnSyncGameProgress(success, allowSyncFail);
        }

        public static void VerfiyAssetBundleVersion()
        {
            if (OnVerfiyAssetBundleVersion != null)
                OnVerfiyAssetBundleVersion();
        }

        public static void UpdateAssetDownloadProgress(int loadedBundleCount, float progress)
        {
            if (OnUpdateAssetDownloadProgress != null)
                OnUpdateAssetDownloadProgress(loadedBundleCount, progress);
        }

        public static void AssetBundleDownloadCallback(bool success)
        {
            if (OnAssetBundleDownload != null)
                OnAssetBundleDownload(success);
        }

        public static void DiamondBalanceUpdated()
        {
            if (OnDiamondBalanceUpdated != null)
                OnDiamondBalanceUpdated();
        }

        public static void GoldBalanceUpdated()
        {
            if (OnGoldBalanceUpdated != null)
                OnGoldBalanceUpdated();
        }

        public static void RelicBalanceUpdated()
        {
            if (OnRelicBalanceUpdated != null)
                OnRelicBalanceUpdated();
        }

        public static void RubyBalanceUpdated()
        {
            if (OnRubyBalanceUpdated != null)
                OnRubyBalanceUpdated();
        }

        public static void ChallengeScoreBalanceUpdated()
        {
            if (OnChallengeScoreBalanceUpdated != null)
                OnChallengeScoreBalanceUpdated();
        }

        public static void DiamondValueAdd(int value)
        {
            if (OnDiamondValueHandler != null)
                OnDiamondValueHandler(value);
        }

        public static void RelicValueAdd(int value)
        {
            if (OnRelicValueHandler != null)
                OnRelicValueHandler(value);
        }

        public static void ChallengeScoreValueHandler(int value)
        {
            if (OnChallengeScoreValueHandler != null)
                OnChallengeScoreValueHandler(value);
        }

        public static void ChangeLanguage()
        {
            if (OnLanguageChange != null)
                OnLanguageChange();
        }

        public static void TriggerTutorial(int tutorialId)
        {
            if (OnTriggerTutorial != null)
                OnTriggerTutorial(tutorialId);
        }

        public static void CompleteTutorial(int tutorialId)
        {
            if (OnTutorialCompleted != null)
                OnTutorialCompleted(tutorialId);
        }

        public static void LevelUpVGBalanceChange(LevelUpVG vg, int balance)
        {
            if (OnLevelUpVGBalanceChange != null)
                OnLevelUpVGBalanceChange(vg, balance);
        }

        public static void ArtifactVGBalanceChange(ArtifactVG vg, int balance)
        {
            if (OnArtifactVGBalanceChange != null)
                OnArtifactVGBalanceChange(vg, balance);
        }

        public static void GameItemVGBalabceChange()
        {
            if (OnGameItemVGBalanceChange != null)
                OnGameItemVGBalanceChange();
        }

        public static void AchievementVGBalanceChange()
        {
            if (OnAchievementVGBalanceChange != null)
                OnAchievementVGBalanceChange();
        }

        public static void ArtifactAllDamageBuffTotalChange(double allDamageBuffTotal)
        {
            if (OnArtifactAllDamageBuffTotalChange != null)
                OnArtifactAllDamageBuffTotalChange(allDamageBuffTotal);
        }

        public static void HeroSkillBuffTotalChange(EffectType t, double newVal)
        {
            if (OnHeroSkillBuffTotalChange != null)
                OnHeroSkillBuffTotalChange(t, newVal);
        }

        public static void HeroDead(int heroIdx, DateTime reviveTime)
        {
            if (OnHeroDead != null)
                OnHeroDead(heroIdx, reviveTime);
        }

        public static void HeroRevive(int heroIdx)
        {
            if (OnHeroRevive != null)
                OnHeroRevive(heroIdx);
        }

        public static void HeroLevelUp(string heroId, int level)
        {
            if (OnHeroLevelUp != null)
                OnHeroLevelUp(heroId, level);
        }

        public static void HeroUnlockSkill(string heroId, string skillId)
        {
            if (OnHeroUnlockSkill != null)
                OnHeroUnlockSkill(heroId, skillId);
        }

        public static void SuperPrestige()
        {
            if (OnSuperPrestige != null)
                OnSuperPrestige();
        }

        public static void Prestige()
        {
            if (OnPrestige != null)
                OnPrestige();
        }

        public static void PrestigeComplete()
        {
            if (OnPrestigeComplete != null)
                OnPrestigeComplete();
        }

        public static void GoldPriceUpdated()
        {
            if (OnGoldPriceUpdated != null)
            {
                OnGoldPriceUpdated();
            }
        }

        public static void UpgradeCostReduced()
        {
            if (OnUpgradeCostReduced != null)
            {
                OnUpgradeCostReduced();
            }
        }

        public static void RelicPriceUpdated()
        {
            if (OnRelicPriceUpdated != null)
            {
                OnRelicPriceUpdated();
            }
        }

        public static void DiamondPriceUpdated()
        {
            if (OnDiamondPriceUpdated != null)
            {
                OnDiamondPriceUpdated();
            }
        }

        //		public static void MakeItRain ()
        //		{
        //			if (OnMakeItRainHandler != null) {
        //				OnMakeItRainHandler ();
        //			}
        //		}

        public static void RefreshSkillItemActive()
        {
            if (OnRefreshSkillItemActive != null)
            {
                OnRefreshSkillItemActive();
            }
        }

        public static void PowerOfHoldingItemActive()
        {
            if (OnPowerOfHoldingItemActive != null)
            {
                OnPowerOfHoldingItemActive();
            }
        }

        public static void DoomItemActive()
        {
            if (OnDoomItemActive != null)
            {
                OnDoomItemActive();
            }
        }

        public static void PowerOfHoldingCounting(string displayText)
        {
            if (OnPowerOfHoldingCounting != null)
            {
                OnPowerOfHoldingCounting(displayText);
            }
        }

        public static void EnemyShouldDropRelic()
        {
            if (OnEnemyShouldDropRelic != null)
            {
                OnEnemyShouldDropRelic();
            }
        }

        public static void EnemyTypeChange(EnemyType t)
        {
            if (OnEnemyTypeChange != null)
            {
                OnEnemyTypeChange(t);
            }
        }
        //		public static void BossFight(){
        //			if(OnBossFight!=null)
        //				OnBossFight();
        //		}

        //		public static void ClearStage(){
        //			if(OnClearStage!=null)
        //				OnClearStage();
        //		}

        public static void BGSwitch()
        {
            if (OnBGSwitch != null)
                OnBGSwitch();
        }

        public static void FairyLeave(bool val)
        {
            if (OnFairyLeave != null)
                OnFairyLeave(val);
        }

        public static void CollectReward(VideoAdRewardType type, bool doubleReward)
        {
            if (OnCollectReward != null)
                OnCollectReward(type, doubleReward);
        }

        public static void IgnoredReward()
        {
            if (OnIgnoredReward != null)
                OnIgnoredReward();
        }

        public static void HiddenMenuShow(bool show)
        {
            if (OnHiddenMenuShow != null)
                OnHiddenMenuShow(show);
        }

        public static void NewAchievementRecord(StatType type)
        {
            if (OnNewAchievementRecord != null)
                OnNewAchievementRecord(type);
        }

        public static void NewGalleryReach()
        {
            if (OnNewGalleryReach != null)
                OnNewGalleryReach();
        }

        public static void FriendInvited()
        {
            if (OnFriendInvited != null)
                OnFriendInvited();
        }

        public static void SalvageUpdated()
        {
            if (OnSalvageHandler != null)
                OnSalvageHandler();
        }

        public static void QuickReloadTebleRow()
        {
            if (OnReloadTebleRow != null)
                OnReloadTebleRow();
        }

        public static void CallGalleryUpdate(string id)
        {
            if (OnGalleryUpdate != null)
                OnGalleryUpdate(id);
        }

        public static void CallGalleryCharacterButtonUpdate(int id)
        {
            if (OnGalleryCharacterButtonUpdate != null)
                OnGalleryCharacterButtonUpdate(id);
        }

        public static void CallGalleryMoeSpiritUpdate()
        {
            if (OnGalleryMoeSpiritUpdate != null)
                OnGalleryMoeSpiritUpdate();
        }

        public static void BuyKeyInGallery()
        {
            if (OnGalleryCloseOverlay != null)
                OnGalleryCloseOverlay();
        }

        public static void VideoAdFinished()
        {
            if (OnVideoAdFinished != null)
                OnVideoAdFinished();
        }

        public static void VideoAdSkipped()
        {
            if (OnVideoAdSkipped != null)
                OnVideoAdSkipped();
        }

        public static void PromotionalPackOfferTime(bool isShow, int currentStage = 0)
        {
            if (OnPromotionalPackOfferTime != null)
                OnPromotionalPackOfferTime(isShow, currentStage);
        }

        public static void PromotionalPackPurchased(string itemPackId)
        {
            if (OnPromotionalPackPurchased != null)
                OnPromotionalPackPurchased(itemPackId);
        }

        public static void ParseDynamicDataLoaded()
        {
            if (OnParseDataLoaded != null)
                OnParseDataLoaded();
        }

        public static void ParseUserSessionTokenInvalid()
        {
            if (OnParseUserInvalidToken != null)
                OnParseUserInvalidToken();
        }

        public static void ParseFriendRelationError(string errorCode)
        {
            if (OnParseFriendRelationError != null)
                OnParseFriendRelationError(errorCode);
        }

//        public static void ParseFriendRelationError(ServiceException.ErrorCode errorCode)
//        {
//            if (OnParseFriendRelationError != null)
//                OnParseFriendRelationError(errorCode);
//        }

        public static void TableRefresh()
        {
            if (OnTableRefresh != null)
                OnTableRefresh();
        }

        public static void FriendTableRefresh()
        {
            if (OnFriendTableRefresh != null)
                OnFriendTableRefresh();
        }
    }
}