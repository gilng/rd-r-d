﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using System.Linq;
using UnityEngine.Analytics;
using Facebook.MiniJSON;

namespace Ignite.AOM
{
    public class FBManager : MonoBehaviour
    {
        private static FBManager _instance;

        public static FBManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public readonly string PP_FB_INVITED_LIST = "PP_FB_INVITED_LIST";

        void Awake()
        {
            _instance = this;

            if (!FB.IsInitialized)
            {
                // Initialize the Facebook SDK
                FB.Init(InitCallback, OnHideUnity);
            }
            else
            {
                // Already initialized, signal an app activation App Event
                FB.ActivateApp();
            }
        }

        private void InitCallback()
        {
            if (FB.IsInitialized)
            {
                // Signal an app activation App Event
                FB.ActivateApp();
                // Continue with Facebook SDK
                // ...
            }
            else
            {
                Debug.Log("Failed to Initialize the Facebook SDK");
            }
        }

        private void OnHideUnity(bool isGameShown)
        {
            if (!isGameShown)
            {
                // Pause the game - we will need to hide
                Time.timeScale = 0;
            }
            else
            {
                // Resume the game - we're getting focus again
                Time.timeScale = 1;
            }
        }

        // Unity will call OnApplicationPause(false) when an app is resumed
        // from the background
        void OnApplicationPause(bool pauseStatus)
        {
            // Check the pauseStatus to see if we are in the foreground
            // or background
            if (!pauseStatus)
            {
                //app resume
                if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                }
                else
                {
                    //Handle FB.Init
                    FB.Init(() =>
                    {
                        FB.ActivateApp();
                    });
                }
            }
        }

        //public void Login()
        //{
        //    FB.LogInWithReadPermissions(
        //        new List<string>() { "public_profile" },// "email", "user_friends"},
        //        AuthCallback
        //    );
        //}

        public void InviteFriend()
        {
            //FB.Mobile.AppInvite(
            //    new Uri("https://fb.me/217894885229436"),
            //    new Uri("http://i.imgur.com/zkYlB.jpg"),
            //    InvitesCallBack
            //    );

            //string excludeIdString = PlayerPrefs.GetString(PP_FB_INVITED_LIST, "");
            //String[] excludeIds = null;

            //if (excludeIdString.Length > 0)
            //{
            //    excludeIds = excludeIdString.Split(',');

            //    foreach (string idString in excludeIds)
            //    {
            //        Debug.Log("idString: " + idString);
            //    }
            //}


            FB.AppRequest(message: "Play AOM", callback: InvitesCallBack);

            //FB.AppRequest(
            //    "Play AOM",
            //    null,
            //    new List<object>() { "app_non_users" },
            //    excludeIds != null ? excludeIds : null,
            //    10,
            //    "{}",
            //    "",
            //    InvitesCallBack
            //    );
        }

        //public void AuthCallback(ILoginResult result)
        //{
        //    Debug.Log("Login result: " + result);
        //    if (FB.IsLoggedIn)
        //    {
        //        // AccessToken class will have session details
        //        var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
        //        // Print current access token's User ID
        //        Debug.Log(aToken.UserId);
        //        // Print current access token's granted permissions
        //        foreach (string perm in aToken.Permissions)
        //        {
        //            Debug.Log(perm);
        //        }
        //    }
        //    else
        //    {
        //        Debug.Log("User cancelled login: " + result.Error);
        //    }

        //    FetchFBProfile();
        //}

        //private void FetchFBProfile()
        //{
        //    Debug.Log("FetchFBProfile");
        //    FB.API("/me?fields=gender,age_range", HttpMethod.GET, FetchProfileCallback, new Dictionary<string, string>() { });
        //}

        //private void FetchProfileCallback(IGraphResult result)
        //{

        //    //			Debug.Log (result.RawResult);
        //    Dictionary<string, object> FBUserDetails = (Dictionary<string, object>)result.ResultDictionary;
        //    Debug.Log("Profile: gender: " + FBUserDetails["gender"]);
        //    //			Debug.Log ("Profile: age_range: " + FBUserDetails["age_range"]);
        //    //			Debug.Log ("Profile: first name: " + FBUserDetails["first_name"]);
        //    //			Debug.Log ("Profile: last name: " + FBUserDetails["last_name"]);
        //    //			Debug.Log ("Profile: id: " + FBUserDetails["id"]);

        //    Debug.Log("gender: " + FBUserDetails["gender"].ToString());
        //    if (FBUserDetails["gender"] != null)
        //    {
        //        if (FBUserDetails["gender"].ToString().Contains("male"))
        //        {
        //            Debug.Log("male found");
        //            Analytics.SetUserGender(Gender.Male);
        //        }
        //        else if (FBUserDetails["gender"].ToString().Contains("female"))
        //        {
        //            Debug.Log("female found");
        //            Analytics.SetUserGender(Gender.Female);
        //        }
        //        else
        //        {
        //            Debug.Log("Unknown Gender");
        //            Analytics.SetUserGender(Gender.Unknown);
        //        }
        //    }
        //    else
        //    {
        //        Debug.Log("gender = null");
        //        Analytics.SetUserGender(Gender.Unknown);
        //    }

        //    Dictionary<string, object> ageRange = (Dictionary<string, object>)FBUserDetails["age_range"];
        //    foreach (string key in ageRange.Keys)
        //    {
        //        Debug.Log(key + ": " + ageRange[key].ToString());
        //    }

        //    AOMStoreInventory.GiveDiamond(10);
        //}

        public void InvitesCallBack(IAppRequestResult result)//(IAppInviteResult result)
        {
            if (result != null)
            {
                //Debug.Log("result: " + result.ToString());

                //foreach (string key in result.ResultDictionary.Keys)
                //{
                //    Debug.Log(key + " : " + result.ResultDictionary[key].ToString());
                //}

                // get the list of users invited
                var responseObject = Json.Deserialize(result.RawResult) as Dictionary<string, object>;
                object obj = 0;

                if (responseObject.TryGetValue("cancelled", out obj))
                {
                    //Debug.Log("Request cancelled");
                    return;
                }
                else if (responseObject.TryGetValue("request", out obj))
                {
                    int numberOfInvites = 0;
                    object[] userIDs;

#if UNITY_ANDROID
                    //Debug.Log("request: " + obj.ToString());
                    responseObject.TryGetValue("to", out obj);
                    //Debug.Log("to: " + obj.ToString());
                    userIDs = obj.ToString().Split(',');
                    numberOfInvites = userIDs.Length;
#elif UNITY_IOS
                    IEnumerable<object> invitesSent = (IEnumerable<object>)responseObject["to"];
                    userIDs = invitesSent.ToArray();
                    numberOfInvites = invitesSent.Count();
#endif

                    //					Debug.Log("numberOfInvites: " + numberOfInvites);
                    if (numberOfInvites > 0)
                    {
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        string invitedList = PlayerPrefs.GetString(PP_FB_INVITED_LIST, "");
                        string[] invitedIds = invitedList.Split(',');
                        sb.Append(invitedList);

                        //foreach (object userID in userIDs)
                        //{
                        //    Debug.Log("ID: " + userID.ToString());
                        //}

                        //foreach (string userID in invitedIds)
                        //{
                        //    Debug.Log("userID: " + userID);
                        //}

                        foreach (object user in userIDs)
                        {
                            if (invitedIds.Contains(user.ToString()))
                            {
                                //Debug.Log(user.ToString() + " - invited before");
                                if (numberOfInvites > 0)
                                    numberOfInvites--;
                                continue;
                            }

                            //if (invitedList.Length != 0)
                            sb.Append(",");
                            sb.Append(user.ToString());
                        }
                        PlayerPrefs.SetString(PP_FB_INVITED_LIST, sb.ToString());

                        sb.Clear();

                        //Debug.Log("DONE: " + numberOfInvites);
                        GamePlayStatManager.Instance.InviteFriend(numberOfInvites);
                    }

                    EventManager.FriendInvited();

                    // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                    Analytics.CustomEvent("facebookFriendReferral", new Dictionary<string, object>
                                          {
                        { "numberOfInvites", numberOfInvites }
                    });
#endif
                }
            }
        }
    }
}