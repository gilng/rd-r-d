﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Parse;

namespace Ignite.AOM
{
    public class NoticeDynamicDataManager : MonoBehaviour
    {
        private static NoticeDynamicDataManager _instance;

        public static NoticeDynamicDataManager Instance
        {
            get
            {
                return _instance;
            }
        }

        private NoticeDynamicData[] dynamicData;
        public NoticeDynamicData[] DynamicData { get { return dynamicData; } }

        private bool loadedSuccess = false;
        public bool LoadedSuccess { get { return loadedSuccess; } }

        int pageNum, pageFinish;



        void Awake()
        {
            _instance = this;

            DontDestroyOnLoad(this.gameObject);
        }

        public void LoadDynamicContent()
        {
            if (!loadedSuccess)
            {
                if (SocialManager.CheckForInternetConnection())
                {
                    StartCoroutine(NoticeService.Instance.FindAll(list =>
                    {

                        if (list == null)
                        {
                            EndProcess();
                        }
                        else
                        {
                            if (list.Count == 0)
                            {
                                EndProcess();
                            }
                            else
                            {
                                LoadContent(list);
                            }
                        }
                    }));
                }
                else
                {
                    EndProcess();
                }
            }
        }

        void LoadContent(IList<Notice> notices)
        {
            pageNum = notices.Count;
            pageFinish = 0;

            dynamicData = new NoticeDynamicData[pageNum];

            int order = 0;
            IList<Notice> notice_order = new List<Notice>();

            for (int i = 0; i < pageNum; i++)
            {
                foreach (Notice n in notices)
                {
                    if (n.Order == order)
                    {
                        notice_order.Add(n);
                        order++;
                    }
                }
            }

            for (int i = 0; i < pageNum; i++)
            {
                dynamicData[i] = new NoticeDynamicData();

                StartCoroutine(DownloadingContent(i, notice_order[i]));
            }
        }

        IEnumerator DownloadingContent(int index, Notice notice)
        {
            string url = notice.ImageURL;
            string filename = System.IO.Path.GetFileName(url);
            string localName = Application.persistentDataPath + "/" + filename;

            if (System.IO.File.Exists(localName))
            {
                var www = new WWW("file://" + localName);
                yield return www;

                if (www.error == null)
                {
                    // Image
                    dynamicData[index].sprite = Sprite.Create(www.texture, new Rect(0, 0, 900, 921), new Vector2(0.5f, 0.5f), 100);
                }

                //				Debug.Log("loaded from speedy local cache.");
            }
            else
            {
                var www = new WWW(url);
                yield return www;

                if (www.error == null)
                {
                    // Image
                    dynamicData[index].sprite = Sprite.Create(www.texture, new Rect(0, 0, 900, 921), new Vector2(0.5f, 0.5f), 100);
                }

                System.IO.File.WriteAllBytes(localName, www.bytes);

                //				Debug.Log("loaded from planetary cloud data system - and saved to device SSD");
            }

            // Text
            dynamicData[index].subtitle = notice.Text;

            // URL
            if (notice.URL != null)
                dynamicData[index].url = notice.URL;

            pageFinish += 1;
            LoadingChecker();
        }

        //		IEnumerator DownloadingContent (int index, Notice notice)
        //		{
        //			WWW www = new WWW (notice.ImageURL);
        //
        //			yield return www;
        //
        //			if (www.error == null) {
        //				// Image
        //				dynamicData [index].sprite = Sprite.Create (www.texture, new Rect (0, 0, 900, 921), new Vector2 (0.5f, 0.5f), 100);
        //
        //				// Text
        //				dynamicData [index].subtitle = notice.Text;
        //
        //				// URL
        //				if (notice.URL != null)
        //					dynamicData [index].url = notice.URL;
        //
        //				pageFinish += 1;
        //				LoadingChecker ();
        //			}
        //		}

        void LoadingChecker()
        {
            if (pageFinish == pageNum)
            {
                EndProcess();
            }
        }

        void EndProcess()
        {
            loadedSuccess = true;
            EventManager.ParseDynamicDataLoaded();
        }
    }
}