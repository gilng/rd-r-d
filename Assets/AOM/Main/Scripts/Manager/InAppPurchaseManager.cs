﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using Soomla.Store;
using System.Collections.Generic;
using SmartLocalization;
using UnityEngine.Analytics;
using Zenject;

namespace Ignite.AOM
{
    public class InAppPurchaseManager : IInitializable
    {
        private UIViewLoader _uiViewLoader;
        private ArtifactCore _artifactCore;
        private IUserService _userService;

        [Inject]
        public void ConstructorSafeAttribute(UIViewLoader uiViewLoader, ArtifactCore artifactCore,
            IUserService userService)
        {
            _uiViewLoader = uiViewLoader;
            _artifactCore = artifactCore;
            _userService = userService;
        }

        bool promotionalOfferShowing = false;

        bool storeInitialized = false;

        // Use this for initialization
        public void Initialize()
        {
            AddEventListener();
        }

        public void Dispose()
        {
            RemoveEventListener();
        }

        void AddEventListener()
        {
            StoreEvents.OnSoomlaStoreInitialized += onStoreInitialized;
            StoreEvents.OnMarketPurchase += onMarketPurchase;

            EventManager.OnPromotionalPackOfferTime += ShowPromotionalOffer;
        }

        void RemoveEventListener()
        {
            StoreEvents.OnSoomlaStoreInitialized -= onStoreInitialized;
            StoreEvents.OnMarketPurchase -= onMarketPurchase;

            EventManager.OnPromotionalPackOfferTime -= ShowPromotionalOffer;
        }

        void onStoreInitialized()
        {
            storeInitialized = true;
        }

        void onMarketPurchase(PurchasableVirtualItem pvi, string payload, Dictionary<string, string> extra)
        {
            // pvi - the PurchasableVirtualItem that was just purchased
            // payload - a text that you can give when you initiate the purchase operation and
            //    you want to receive back upon completion
            // extra - contains platform specific information about the market purchase
            //    Android: The "extra" dictionary will contain: 'token', 'orderId', 'originalJson', 'signature', 'userId'
            //    iOS: The "extra" dictionary will contain: 'receiptUrl', 'transactionIdentifier', 'receiptBase64', 'transactionDate', 'originalTransactionDate', 'originalTransactionIdentifier'

            // ... your game specific implementation here ...

            //apple
            //https://developer.apple.com/library/mac/releasenotes/General/ValidateAppStoreReceipt/Chapters/ValidateRemotely.html#//apple_ref/doc/uid/TP40010573-CH104-SW1

            bool purchaseSuccess = false;

            if (storeInitialized)
            {
                //#if UNITY_IOS && !UNITY_EDITOR
                //				if (pvi != null && extra["transactionIdentifier"] != null)
                //				{
                //					purchaseSuccess = true;
                //				}
#if UNITY_ANDROID && !UNITY_EDITOR
                if (pvi != null && extra["orderId"] != null)
                {
                    purchaseSuccess = true;
                }
#elif UNITY_IOS || UNITY_EDITOR
                if (pvi != null)
                {
                    purchaseSuccess = true;
                }
#endif

                if (purchaseSuccess)
                {
                    bool isConsumableItem = false;

                    // Gems
                    if (pvi.ItemId.Contains("diamonds_") || pvi.ItemId.Contains("promotional_"))
                    {
                        int[] diamondNum = {1200, 3100, 6500, 14000};

                        for (int i = 0; i < StoreInfo.CurrencyPacks.Count; i++)
                        {
                            if (StoreInfo.CurrencyPacks[i].ItemId == pvi.ItemId)
                            {
                                if (StoreInfo.CurrencyPacks[i].CurrencyItemId == AOMStoreConstant.DIAMOND_CURRENCY_ID)
                                {
                                    _userService.UpdateGemBalance(StoreInfo.CurrencyPacks[i].CurrencyAmount,
                                        userSummary =>
                                        {
                                            if (userSummary != null)
                                            {
                                                AOMStoreInventory.ResetDiamond(userSummary.Gem);
                                            }
                                            else
                                            {
                                                if(_userService.CurrentUserSummary != null)
                                                    AOMStoreInventory.ResetDiamond(_userService.CurrentUserSummary.Gem);

                                                ShowSyncConsumableItemFailPopup();
                                            }
                                        });

                                    if (pvi.ItemId.Contains("diamonds_"))
                                    {
                                        // Give Artifact
                                        for (int j = 0; j < diamondNum.Length; j++)
                                        {
                                            if (pvi.ItemId == "diamonds_" + diamondNum[j])
                                            {
                                                _artifactCore.BuySpecifyOne("AR030");
                                                break;
                                            }
                                        }

                                        EventManager.TableRefresh();
                                    }
                                    else if (pvi.ItemId.Contains("promotional_"))
                                    {
                                        // To Do: Load Promotional ItemId from parse config
                                        EventManager.PromotionalPackPurchased(pvi.ItemId);
                                    }

                                    // Cloas Ad
                                    AdMobManager.Instance.ShouldNoADs = true;
                                }

                                isConsumableItem = true;

                                break;
                            }
                        }
                    }
                    else if (pvi.ItemId.Contains("key_"))
                    {
                        // Moetan Package Key
                        GameItemVG packageKeyVG = AOMAssets.GetGameItemVGFromItemId(pvi.ItemId);

                        if (packageKeyVG != null)
                        {
                            packageKeyVG.Give(1);

                            if (GameManager.Instance != null || FieldGuideManager.Instance != null)
                            {
                                if (GameManager.Instance != null)
                                {
                                    GameManager.Instance.enemyPool.KeyUnlock(packageKeyVG.ItemId);
                                }
                                else if (FieldGuideManager.Instance != null)
                                {
                                    EventManager.CallGalleryUpdate(packageKeyVG.ItemId);
                                    EventManager.BuyKeyInGallery();
                                }

                                GameObject purchasedView =
                                    _uiViewLoader.LoadOverlayView("GachaResultMenuView", null, false,
                                        Time.timeScale == 0);
                                purchasedView.GetComponent<GachaResultViewController>()
                                    .SetItemId(packageKeyVG, GachaType.Purchase);
                            }

                            isConsumableItem = false;
                        }
                    }

                    // Analytics
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                    decimal itemPrice = 0;

                    string receiptData = null;
                    string signature = null;

#if UNITY_ANDROID
                    itemPrice = isConsumableItem
                        ? Convert.ToDecimal(AOMAssets.CurrencyPackMarketPrice(pvi.ItemId))
                        : Convert.ToDecimal(AOMAssets.ProductMarketPrice(pvi.ItemId));

                    if (extra != null)
                    {
                        extra.TryGetValue("originalJson", out receiptData);
                        extra.TryGetValue("signature", out signature);
                        //receiptData = extra["originalJson"];
                        //signature = extra["signature"];
                    }

#elif UNITY_IOS
                    itemPrice = isConsumableItem
                        ? Convert.ToDecimal(AOMAssets.CurrencyPackMarketPrice(pvi.ItemId))
                        : Convert.ToDecimal(AOMAssets.ProductMarketPrice(pvi.ItemId.Replace("key", "keyofworld")));

                    if (extra != null)
                    {
                        extra.TryGetValue("receiptBase64", out receiptData);
                        //receiptData = extra["receiptBase64"];
                    }
#endif

                    string currencyCode = ((PurchaseWithMarket) pvi.PurchaseType).MarketItem.MarketCurrencyCode;
                    Analytics.Transaction(pvi.ItemId, itemPrice, currencyCode, receiptData, signature);

                    Debug.Log("pvi.ItemId - " + pvi.ItemId);
                    Debug.Log("itemPrice - " + itemPrice);
                    Debug.Log("currencyCode - " + currencyCode);
                    Debug.Log("receiptData - " + receiptData);
                    Debug.Log("signature - " + signature);

#endif
                }
            }
        }

        public void ShowPromotionalOffer(bool isShow, int currentStage)
        {
            if (isShow)
            {
                if (!promotionalOfferShowing)
                {
                    string packId = "";

                    if (currentStage % 100 == 0)
                    {
                        if ((currentStage / 100) % 2 == 0)
                        {
                            if (PlayerPrefs.GetInt("promotional_goldpack_PURCHASED", 0) == 0)
                                packId = "promotional_goldpack";
                        }
                        else
                        {
                            if (PlayerPrefs.GetInt("promotional_silverpack_PURCHASED", 0) == 0)
                                packId = "promotional_silverpack";
                        }
                    }
                    else
                    {
                        if ((currentStage == 35 || currentStage == 50 || currentStage == 80) &&
                            GamePlayStatManager.Instance.Stat[StatType.Prestige] == 0)
                        {
                            if (PlayerPrefs.GetInt("promotional_bronzepack_PURCHASED", 0) == 0)
                                packId = "promotional_bronzepack";
                        }
                    }

                    if (packId != "")
                    {
                        GameObject promotionalOfferView =
                            _uiViewLoader.LoadOverlayView("PromotionalOfferView", null, false, false);
                        PromotionalOfferViewController promotionalOfferViewController = promotionalOfferView
                            .GetComponent<PromotionalOfferViewController>();
                        promotionalOfferViewController.InitPromotionalItemPack(packId);

                        promotionalOfferShowing = true;
                    }
                }
            }
            else
            {
                promotionalOfferShowing = false;
            }
        }

        public void ShowSyncConsumableItemFailPopup()
        {
            var msgPopupView = _uiViewLoader.LoadOverlayView("MsgPopupOverlay", null, false,
                Time.timeScale == 0);
            var msgPopupViewController = msgPopupView.GetComponent<MsgPopupViewController>();
            msgPopupViewController.InputMsg(LanguageManager.Instance.GetTextValue("iap_fail_title"),
                LanguageManager.Instance.GetTextValue("iap_fail_msg"), "btn_ok");
        }
    }
}