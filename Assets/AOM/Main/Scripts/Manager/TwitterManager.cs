﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Soomla.Profile;

namespace Ignite.AOM
{
    public class TwitterManager : MonoBehaviour
	{
        private static TwitterManager _instance;
        public static TwitterManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public Provider targetProvider = Provider.TWITTER;
        public string message = "Hi Every One How Are You !";

        public bool inited;

        // Use this for initialization
        void Start()
        {
            DontDestroyOnLoad(this.gameObject);

            _instance = this;

            ProfileEvents.OnSoomlaProfileInitialized += () =>
            {
                Soomla.SoomlaUtils.LogDebug("TwitterManager", "SoomlaProfile Initialized !");
                inited = true;
            };

            ProfileEvents.OnLoginFinished += (UserProfile UserProfile, bool autoLogin, string payload) =>
            {
                Debug.Log("logged in");
            };

            ProfileEvents.OnGetContactsFinished += (Provider provider, SocialPageData<UserProfile> contactsData, string payload) =>
            {
                Soomla.SoomlaUtils.LogDebug("TwitterManager", "get contacts for: " + contactsData.PageData.Count + " page: " + contactsData.PageNumber + " More? " + contactsData.HasMore);
                foreach (var profile in contactsData.PageData)
                {
                    Soomla.SoomlaUtils.LogDebug("TwitterManager", "Contact: " + profile.toJSONObject().print());
                }

                if (contactsData.HasMore)
                {
                    SoomlaProfile.GetContacts(targetProvider);
                }
            };

            SoomlaProfile.Initialize();

#if UNITY_IPHONE
            Handheld.SetActivityIndicatorStyle(UnityEngine.iOS.ActivityIndicatorStyle.Gray);
#elif UNITY_ANDROID
            Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
#endif
        }

        public void TwitterSignout()
        {
            if (SoomlaProfile.IsLoggedIn(targetProvider))
            {
                SoomlaProfile.Logout(targetProvider);

            }
        }
        public void TwitterLogin()
        {
            if (!SoomlaProfile.IsLoggedIn(targetProvider))
            {
                SoomlaProfile.Login(targetProvider);
            }
        }

        public void TweetMessage()
        {
            if (SoomlaProfile.IsLoggedIn(targetProvider))
            {
                SoomlaProfile.UpdateStory(
                    targetProvider,
                    "Test - I am playing Attack on Moe",
                    "",
                    "",
                    "",
                    "",
                    "http://ignite-ga.me/images/web_aom_v6.jpg"
                    );
            }
            else
            {
                Debug.Log("Please login Twitter");
                TwitterLogin();
            }
        }
	}
}
