﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using SmartLocalization;

namespace Ignite.AOM
{
	public class EndOfWorldManager : MonoBehaviour {

		public Animator eowAnimator;
		public GameObject stopAnimeBtn, parentObj;

		public bool timerOn = false;

		float timer, time = 5;

		// Animation use
		public void WaitingPress()
		{
            timerOn = true;

			stopAnimeBtn.SetActive(true);
		}

		public void FadeOutAnimation()
		{
//			SoundManager.Instance.StopSound();

			timerOn = false;
			stopAnimeBtn.SetActive(false);

			eowAnimator.Play("EndOfWorldAnimator_FadeOut");
		}

		// Animation use
		void EndOffAnime()
		{
            Destroy(parentObj);
		}
	}
}