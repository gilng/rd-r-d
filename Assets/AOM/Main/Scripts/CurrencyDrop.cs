﻿using UnityEngine;
using System.Collections;

namespace Ignite.AOM
{
    public abstract class CurrencyDrop : MonoBehaviour
    {
        private float startPointX = 0;
        private float startPointY = 3f;
        private float controlPointX;
        private float controlPointY = 4f;
        private float endPointX;
        private float endPointY;
        private float curveX;
        private float curveY;
        private float targetX;
        private float targetY;
        private float bezierTime = 0;
        protected float speed = 2;
        private float bouncingSpeed = 8f;
        private bool dropping = false;
        private bool bouncing = false;
        protected bool returning = false;

        protected bool directReturning = false;

        private float waitForReturn = 4f;
        private float waitingTimer = 0f;
        protected double dropAmount;
        protected ObjectPool textObjectPool;
        private bool collected = false;

        private float maxLimit = 2.65f;
        private float minLimit = -2.65f;

        private bool dropRotation = false;

        private bool returnToEndPoint = false;

        protected bool customFeatureRun = false;

        public ObjectPool TextObjectPool
        {
            get { return this.textObjectPool; }
            set { this.textObjectPool = value; }
        }

        public Vector2 StartPoint
        {
            get { return new Vector2(startPointX, startPointY + Random.Range(0.15f, 0.45f)); }
        }

        public void StartDrop(float dropStartPointX, float dropStartPointY, float dropControlPointXLeft,
                               float dropControlPointXRight, float dropControlPointYUp, float dropControlPointYDown,
                                float dropEndPointX, float dropEndPointY, double dropAmount, bool isRotate = false, bool isReturnToEndPoint = true)
        {
            startPointX = dropStartPointX;
            startPointY = dropStartPointY;
            targetX = dropEndPointX;
            targetY = dropEndPointY;
            transform.position = new Vector2(startPointX, startPointY);
            controlPointX = Random.Range(dropControlPointXLeft, dropControlPointXRight);
            controlPointY = Random.Range(dropControlPointYUp, dropControlPointYDown);

            dropRotation = isRotate;

            returnToEndPoint = isReturnToEndPoint;

            // Limit X : -2.0f to 2.0f
            if (controlPointX > 0)
            {
                endPointX = (controlPointX + 0.5f > maxLimit) ? maxLimit : (controlPointX + 0.5f);
            }
            else
            {
                endPointX = (controlPointX - 0.5f < minLimit) ? minLimit : (controlPointX - 0.5f);
            }

            if (endPointX > -1.75f && endPointX < 1.75f)
            {
                endPointY = Random.Range(0.4f, 0.6f);
            }
            else
            {
                endPointY = Random.Range(0.4f, 0.75f);
            }

            /*
            if(controlPointX>0){
                endPointX=controlPointX+0.5f;
                endPointY=Random.Range(Background.instance.mountainA.transform.position.y - 0.1f, Background.instance.mountainA.transform.position.y + 0.5f);
            }else{
                endPointX=controlPointX-0.5f;
                if(endPointX<-0.91f){
                    endPointY=Random.Range(Background.instance.mountainA.transform.position.y - 0.25f, Background.instance.mountainA.transform.position.y + 0.5f);
                }else{
                    endPointY=Random.Range(Background.instance.mountainA.transform.position.y - 0.25f, Background.instance.mountainA.transform.position.y + 0.5f);
                }
            }
            */

            dropping = true;
            bouncing = false;
            returning = false;
            collected = false;

            waitingTimer = 0f;
            bezierTime = 0;
            this.dropAmount = System.Math.Ceiling(dropAmount);
        }

        private void StartBouncing()
        {
            //			transform.position =new Vector3(endPointX, endPointY, 0);
            //			startPointX=endPointX;
            //			startPointY=endPointY;
            //			float direction =0f;
            //			
            //			if(Random.Range(-1.0f,1.0f)>0){
            //				direction=0.15f;
            //			}else{
            //				direction=-0.15f;
            //			}
            //			controlPointX=startPointX+direction;
            //			controlPointY=endPointY+0.8f;
            //			endPointX=controlPointX+direction*2;
            //			bouncing=true;
            //			bezierTime = 0;

            transform.position = new Vector3(endPointX, Background.instance.mountainA.transform.position.y + endPointY, 0);
            startPointX = endPointX;
            startPointY = Background.instance.mountainA.transform.position.y + endPointY;

            float direction = (startPointX > controlPointX) ? 0.15f : -0.15f;

            if (startPointX + (direction * 2) > maxLimit || startPointX - (direction * 2) < minLimit)
                direction *= -1;

            controlPointX = startPointX + direction;
            controlPointY = Background.instance.mountainA.transform.position.y + endPointY + 0.8f;
            endPointX = controlPointX + direction;

            bouncing = true;
            dropping = false;
            returning = false;
            collected = false;
            bezierTime = 0;
        }

        private void StartReturning()
        {
            if (returnToEndPoint)
            {
                Vector3 resetEndPosition = new Vector3(transform.position.x, Background.instance.mountainA.transform.position.y + endPointY, transform.position.z);

                startPointX = transform.position.x;
                startPointY = transform.position.y;

                if (startPointX > 0)
                    controlPointX = Random.Range(0f, 2.5f);
                else
                    controlPointX = Random.Range(-2.5f, 0f);

                controlPointY = 2.5f;
                endPointX = targetX;
                endPointY = targetY;
                bezierTime = 0;

                returning = true;
                bouncing = false;
                dropping = false;
                directReturning = false;

                transform.position = resetEndPosition;
            }
            else
            {
                bouncing = false;
                dropping = false;
                directReturning = false;
                dropRotation = false;

                PauseReturn();
            }
        }

        abstract protected void PauseReturn();

        abstract protected void AfterReturn();

        protected virtual void CustomFeature()
        {

        }

        void Update()
        {
            if (dropping || bouncing)
            {
                // Quadratic Bezier curves
                curveX = (((1 - bezierTime) * (1 - bezierTime)) * startPointX) + (2 * bezierTime * (1 - bezierTime) * controlPointX) + ((bezierTime * bezierTime) * endPointX);
                curveY = (((1 - bezierTime) * (1 - bezierTime)) * startPointY) + (2 * bezierTime * (1 - bezierTime) * controlPointY) + ((bezierTime * bezierTime) * (Background.instance.mountainA.transform.position.y + endPointY));

                if (curveX > maxLimit || curveX < minLimit)
                {
                    if (curveX > maxLimit)
                    {
                        startPointX = maxLimit;
                        controlPointX = maxLimit - (endPointX - maxLimit) / 2;
                        endPointX = maxLimit - (endPointX - maxLimit);
                    }

                    if (curveX < minLimit)
                    {
                        startPointX = minLimit;
                        controlPointX = minLimit - (endPointX - minLimit) / 2;
                        endPointX = minLimit - (endPointX - minLimit);
                    }
                }
            }
            else
            {
                if (returning || directReturning)
                {
                    curveX = (((1 - bezierTime) * (1 - bezierTime)) * startPointX) + (2 * bezierTime * (1 - bezierTime) * controlPointX) + ((bezierTime * bezierTime) * endPointX);
                    curveY = (((1 - bezierTime) * (1 - bezierTime)) * startPointY) + (2 * bezierTime * (1 - bezierTime) * controlPointY) + ((bezierTime * bezierTime) * endPointY);
                }
            }

            if (dropping)
            {
                bezierTime = bezierTime + Time.deltaTime * speed;
                if (bezierTime >= 1)
                {
                    StartBouncing();
                }
                else
                {
                    transform.position = new Vector3(curveX, curveY, 0);
                }
            }
            else if (bouncing)
            {
                waitingTimer += Time.deltaTime;
                bezierTime = bezierTime + Time.deltaTime * bouncingSpeed;
                if (bezierTime >= 1)
                {
                    StartReturning();
                }
                else
                {
                    transform.position = new Vector3(curveX, curveY, 0);
                }
            }
            else if (returning)
            {
                waitingTimer += Time.deltaTime;
                if (waitingTimer > waitForReturn)
                {
                    bezierTime = bezierTime + Time.deltaTime * 1.5f;
                    if (bezierTime >= 1)
                    {
                        AfterReturn();
                        collected = true;
                    }
                    else
                    {
                        if (!collected)
                        {
                            OnCollect();
                            collected = true;
                        }
                        transform.position = new Vector3(curveX, curveY, 0);
                    }
                }
            }
            else if (directReturning)
            {
                // OfflineGlodDrop use
                transform.position = new Vector3(curveX, curveY, 0);
                bezierTime += Time.deltaTime;
                if (bezierTime >= 1)
                {
                    AfterReturn();
                }
            }

            if (dropRotation)
            {
                if (returning == false && directReturning == false)
                {
                    transform.rotation = Quaternion.Euler(0, 0, Random.Range(360, 720));
                }
                else
                {
                    if (waitingTimer > waitForReturn)
                        transform.rotation = Quaternion.Euler(0, 0, 0);
                }
            }

            if (customFeatureRun)
            {
                CustomFeature();
            }
        }

        abstract protected void OnCollect();
        //		void OnCollect(){
        //			if(!GameManager.instance.inPrestige && ){
        //				GameObject labelObject = textObjectPool.GetPooledObject();
        //				TapDamageText dmgLabel = (TapDamageText)labelObject.GetComponent<TapDamageText>();
        //				dmgLabel.textObject.text = UnitConverter.ConverterDoubleToString(dropAmount);
        //				dmgLabel.SpawnForMoneyText(transform.position);
        //				labelObject.SetActive(true);
        //			}
        //		}

        public void OnTap()
        {
            waitingTimer = waitForReturn;

            StartReturning();
        }

        public void ForceReturnDroppedGold()
        {
            if (!collected)
            {
                AfterReturn();
                collected = true;
            }
        }

        public void CallToReturn(float dropStartPointX, float dropStartPointY, float dropControlPointX, float dropControlPointY, float dropEndPointX, float dropEndPointY)
        {
            startPointX = dropStartPointX;
            startPointY = dropStartPointY;
            transform.position = new Vector2(startPointX, startPointY);

            targetX = endPointX = dropEndPointX;
            targetY = endPointY = dropEndPointY;

            controlPointX = dropControlPointX;
            controlPointY = dropControlPointY;

            dropping = false;
            bouncing = false;
            returning = false;
            collected = false;
            waitingTimer = 0f;
            bezierTime = 0;
            this.dropAmount = System.Math.Ceiling(dropAmount);

            directReturning = true;
        }
    }
}