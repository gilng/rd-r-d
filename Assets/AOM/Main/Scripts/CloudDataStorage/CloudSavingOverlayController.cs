﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SmartLocalization;

namespace Ignite.AOM
{
    public class CloudSavingOverlayController : UIOverlayViewController
    {
        public Image overlayBG;
        public Text messageTitle;
        public Text warningMessage;

        public GameObject loadingIndicator;

        public Button continueBtn;

        // Use this for initialization
        public override void InitView(UIViewController previousUIOverlayView = null, bool pause = false)
        {
            base.InitView(previousUIOverlayView, pause);
        }

        public void ShowSigninMessage()
        {
            messageTitle.text = LanguageManager.Instance.GetTextValue("save_msg_title_signIn");
            warningMessage.text = LanguageManager.Instance.GetTextValue("save_msg_subtitle_signIn");
            loadingIndicator.SetActive(false);

            continueBtn.gameObject.SetActive(false);
        }

        public void ShowSigninFailMessage()
        {
            messageTitle.text = LanguageManager.Instance.GetTextValue("save_msg_title_signInFail");
            warningMessage.text = LanguageManager.Instance.GetTextValue("save_msg_subtitle_signInFail");
            loadingIndicator.SetActive(false);

            continueBtn.gameObject.SetActive(true);
        }

        public void ShowLinkingMessage()
        {
            messageTitle.text = LanguageManager.Instance.GetTextValue("save_msg_title_linking");
            warningMessage.text = LanguageManager.Instance.GetTextValue("save_msg_subtitle_linking");
            loadingIndicator.SetActive(false);

            continueBtn.gameObject.SetActive(true);
        }

        public void ShowLinkSuccessMessage()
        {
            messageTitle.text = LanguageManager.Instance.GetTextValue("save_msg_title_linkSuccess");
            warningMessage.text = LanguageManager.Instance.GetTextValue("save_msg_subtitle_linkSuccess");
            loadingIndicator.SetActive(false);

            continueBtn.gameObject.SetActive(true);
        }

        public void ShowLinkFailMessage()
        {
            messageTitle.text = LanguageManager.Instance.GetTextValue("save_msg_title_linkFail");
            warningMessage.text = LanguageManager.Instance.GetTextValue("save_msg_subtitle_linkFail");
            loadingIndicator.SetActive(false);

            continueBtn.gameObject.SetActive(true);
        }

        public void ShowSavingMessage()
        {
            messageTitle.text = LanguageManager.Instance.GetTextValue("save_msg_title_saving");
            warningMessage.text = LanguageManager.Instance.GetTextValue("save_msg_subtitle_saving");
            loadingIndicator.SetActive(false);

            continueBtn.gameObject.SetActive(false);
        }

        public void ShowSavedMessage()
        {
            messageTitle.text = LanguageManager.Instance.GetTextValue("save_msg_title_saved");
            warningMessage.text = LanguageManager.Instance.GetTextValue("save_msg_subtitle_saved");
            loadingIndicator.SetActive(false);

            continueBtn.gameObject.SetActive(true);
        }

        public void ShowSaveFailMessage()
        {
            messageTitle.text = LanguageManager.Instance.GetTextValue("save_msg_title_saveFail");
            warningMessage.text = LanguageManager.Instance.GetTextValue("save_msg_subtitle_saveFail");
            loadingIndicator.SetActive(false);

            continueBtn.gameObject.SetActive(true);
        }

        public void ShowLoadingMessage()
        {
            messageTitle.text = LanguageManager.Instance.GetTextValue("save_msg_title_loading");
            warningMessage.text = LanguageManager.Instance.GetTextValue("save_msg_subtitle_loading");
            loadingIndicator.SetActive(false);

            continueBtn.gameObject.SetActive(false);
        }

        public void ShowLoadedMessage()
        {
            messageTitle.text = LanguageManager.Instance.GetTextValue("save_msg_title_loaded");
            warningMessage.text = LanguageManager.Instance.GetTextValue("save_msg_subtitle_loaded");
            loadingIndicator.SetActive(false);

            continueBtn.gameObject.SetActive(false);
        }

        public void ShowLoadFailMessage()
        {
            messageTitle.text = LanguageManager.Instance.GetTextValue("save_msg_title_loadFail");
            warningMessage.text = LanguageManager.Instance.GetTextValue("save_msg_subtitle_loadFail");
            loadingIndicator.SetActive(false);

            continueBtn.gameObject.SetActive(true);
        }

        public void ShowLoadingIndicator()
        {
            overlayBG.color = new Color(0, 0, 0, 0.25f);
            loadingIndicator.SetActive(true);

            continueBtn.gameObject.SetActive(false);
        }

        public override void CloseView()
        {
            if(continueBtn.gameObject.activeSelf)
                SoundManager.Instance.PlaySoundWithType(SoundType.ButtonTouch_1);

            base.CloseView();
        }
    }
}
