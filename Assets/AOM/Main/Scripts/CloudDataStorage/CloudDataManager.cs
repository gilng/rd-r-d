﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using SmartLocalization;
using Zenject;

namespace Ignite.AOM
{
    #region SaveData
    [Serializable]
    public class SaveData
    {
        public string playerName;

        public double golds;
        public int diamonds;
        public int relics;
//        public int rubys;

        public int stage;
        public int wave;

        public bool tutorialCompleted;

        public int avatarLevel;

        public List<int> avatarSkillList = new List<int>();

        public List<int> heroList = new List<int>();
        public List<bool> aliveHeroList = new List<bool>();
        public IDictionary<int, DateTime> heroReviveTimeList = new Dictionary<int, DateTime>();
        public string pp_ReviveTime;

        public List<int> heroPassiveList = new List<int>();

        public List<int> artifactOrderList = new List<int>();
        public List<int> artifactList = new List<int>();

        public List<int> moespiritList = new List<int>();

        public int touchCredit;
        public int touchCredit_Max;
        public double lastTouch;
        public int creditRechargeTime;

        public List<int> girlStatusList = new List<int>();
        public List<int> girlExpList = new List<int>();
        public List<int> girlReachedBreakLevelList = new List<int>();

        public List<int> achievementList = new List<int>();

        // Facebook Invite State
        public string pp_FBInvitedList;

        // Gameplay State
        public double inviteFriends;
        public double reachHighestStage;
        public double collectRelic;
        //public double collectRuby;
        public double tap;
        public double collectGold;
        public double killMonster;
        public double ownArtifact;
        public double reachHeroDPS;
        public double killBoss;
        public double prestige;
        public double lostGoldFromPrestige;
        public double levelUpHeroTimes;
        public double openChest;
        public double getFairyPresent;
        public double useJumpAttack;
        public double getCriticalHit;
        public double firstPlayTime;
        public double playingDuration;
        public double currentTotalHeroLevel;
        public double currentTapChance;
        public double currentDPSMultiplier;
        public double currentCriticalHitMultiplier;
        public double currentTotalGoldMultiplier;
        public double lastPrestigeTime;

        public bool appRated;
        public bool facebookLiked;
        public bool twitterFollowed;
        public bool noAdmob;

        // Purchased Item
        public bool key001Purchased;
        public bool key002Purchased;
        public bool key003Purchased;
        public bool key004Purchased;
        public bool key005Purchased;
        public bool key006Purchased;

        public bool pvpGiftKey001Purchased;
        public bool pvpGiftKey002Purchased;

        public bool goldPackPurchased;
        public bool silverPackPurchased;
        public bool bronzePackPurchased;

        // Convert class instance to byte array
        public static byte[] ToBytes(SaveData data)
        {
            var formatter = new BinaryFormatter();

            using (var stream = new MemoryStream())
            {
                formatter.Serialize(stream, data);
                return stream.ToArray();
            }
        }

        // Convert byte array to class instance
        public static SaveData FromBytes(byte[] data)
        {
            if (data == null || data.Length == 0)
            {
                Debug.LogWarning("Serialization of zero sized data array!");
                return null;
            }

            using (var stream = new MemoryStream())
            {
                try
                {
                    var formatter = new BinaryFormatter();
                    stream.Write(data, 0, data.Length);
                    stream.Seek(0, SeekOrigin.Begin);

                    SaveData block = (SaveData)formatter.Deserialize(stream);
                    return block;
                }
                catch (Exception e)
                {
                    Debug.LogError("Error when reading stream: " + e.Message);
                }
            }
            return null;
        }
    }
    #endregion

    #region SaveDataService
    public class SaveDataService
    {
        private HeroCore _heroCore;
        private GalleryCore _galleryCore;
        private ArtifactCore _artifactCore;

        [Inject]
        public SaveDataService(HeroCore heroCore, GalleryCore galleryCore, ArtifactCore artifactCore)
        {
            _heroCore = heroCore;
            _galleryCore = galleryCore;
            _artifactCore = artifactCore;
        }

        public SaveData CreateSaveData()
        {
            // Create new save data
            SaveData saveData = new SaveData
            {
                // These values are hard coded for the purpose of this tutorial.
                // Normally, you would replace these values with whatever you want to save.
                playerName = PlayerPrefs.GetString("PLAYER_NAME"),

                golds = AOMStoreInventory.GetGoldBalance(),
//                diamonds = AOMStoreInventory.GetDiamondBalance(),
//                relics = AOMStoreInventory.GetRelicBalance(),
//                rubys = AOMStoreInventory.GetRubyBalance(),

                stage = PlayerPrefs.GetInt("PP_STAGE"),
                wave = PlayerPrefs.GetInt("PP_WAVE"),

                avatarLevel = AOMAssets.AvatarLevelVG.GetBalance()
            };

            UpdateAvatarSkillLevel(saveData);
            UpdateHerosLevel(saveData);
            UpdateArtifactLevel(saveData);
            UpdateMoespiritLevel(saveData);

            UpdateGirlStatus(saveData);

            UpdateAchievementBalance(saveData);
            UpdateGamePlayState(saveData);

            UpdatePurchasedItemList(saveData);

            UpdateTutorialProgress(saveData);

            return saveData;
        }

        void UpdateAvatarSkillLevel(SaveData saveData)
        {
            saveData.avatarSkillList.Clear();

            foreach (AvatarActiveSkillVG avatarSkill in AOMAssets.AvatarActiveSkillVGs)
            {
                saveData.avatarSkillList.Add(avatarSkill.GetBalance());
            }
        }

        void UpdateHerosLevel(SaveData saveData)
        {
            saveData.heroList.Clear();
            saveData.heroPassiveList.Clear();

            foreach (CharacterLevelVG hero in HeroAssets.Heroes)
            {
                saveData.heroList.Add(hero.GetBalance());

                int unlockedSkillBalance = 0;

                for (int i = 0; i < HeroAssets.HeroSkillsByHeroId[hero.ItemId].Count; i++)
                {
                    HeroPassiveSkillVG skill = HeroAssets.HeroSkillsByHeroId[hero.ItemId][i];
                    if (skill.GetBalance() > 0)
                        unlockedSkillBalance++;
                }

                saveData.heroPassiveList.Add(unlockedSkillBalance);
            }

            saveData.aliveHeroList.Clear();
            saveData.heroReviveTimeList.Clear();

            foreach (bool alive in _heroCore.HeroAlive)
            {
                saveData.aliveHeroList.Add(alive);
            }

            saveData.heroReviveTimeList = _heroCore.HeroReviveTime;

            saveData.pp_ReviveTime = _heroCore.PP_HeroReviveTime;
        }

        void UpdateArtifactLevel(SaveData saveData)
        {
            saveData.artifactOrderList.Clear();
            saveData.artifactList.Clear();

            foreach (string artifactId in _artifactCore.PurchasedAFIds())
            {
                saveData.artifactOrderList.Add(Int32.Parse(artifactId.Remove(0, 2)));

                foreach (ArtifactVG artifact in _artifactCore.FindOwnedArtifact())
                {
                    if (artifact.ItemId == artifactId)
                    {
                        saveData.artifactList.Add(artifact.GetBalance());
                        break;
                    }
                }
            }
        }

        void UpdateMoespiritLevel(SaveData saveData)
        {
            saveData.moespiritList.Clear();

            foreach (ArtifactVG moespirit in ArtifactAssets.MoeSpirits)
            {
                saveData.moespiritList.Add(moespirit.GetBalance());
            }
        }

        void UpdateGirlStatus(SaveData saveData)
        {
            saveData.girlStatusList.Clear();
            saveData.girlExpList.Clear();
            saveData.girlReachedBreakLevelList.Clear();

            if (_galleryCore != null)
            {
                saveData.touchCredit = _galleryCore.TouchCredit;
                saveData.touchCredit_Max = _galleryCore.MaxTouchCredit;
                saveData.lastTouch = _galleryCore.LastTouch;
                saveData.creditRechargeTime = _galleryCore.CreditRechargeTime;

                foreach (Gallery girl in GalleryCore.Girls)
                {
                    saveData.girlStatusList.Add((int)girl.Status);
                    saveData.girlExpList.Add(girl.Exp);
                    saveData.girlReachedBreakLevelList.Add(girl.ReachedBreakLevel);
                }
            }
        }

        void UpdateAchievementBalance(SaveData saveData)
        {
            saveData.achievementList.Clear();

            foreach (AchievementVG achievement in AchievementAssets.Achievements)
            {
                saveData.achievementList.Add(achievement.GetBalance());
            }
        }

        void UpdateGamePlayState(SaveData saveData)
        {
            if (GamePlayStatManager.Instance != null)
            {
                saveData.inviteFriends = GamePlayStatManager.Instance.Stat[StatType.InviteFriends];
                saveData.reachHighestStage = GamePlayStatManager.Instance.Stat[StatType.ReachHighestStage];
                saveData.collectRelic = GamePlayStatManager.Instance.Stat[StatType.CollectRelic];
                //saveData.collectRuby = GamePlayStatManager.Instance.Stat[StatType.CollectRuby];
                saveData.tap = GamePlayStatManager.Instance.Stat[StatType.Tap];
                saveData.collectGold = GamePlayStatManager.Instance.Stat[StatType.CollectGold];
                saveData.killMonster = GamePlayStatManager.Instance.Stat[StatType.KillMonster];
                saveData.ownArtifact = GamePlayStatManager.Instance.Stat[StatType.OwnArtifact];
                saveData.reachHeroDPS = GamePlayStatManager.Instance.Stat[StatType.ReachHeroDPS];
                saveData.killBoss = GamePlayStatManager.Instance.Stat[StatType.KillBoss];
                saveData.prestige = GamePlayStatManager.Instance.Stat[StatType.Prestige];
                saveData.lostGoldFromPrestige = GamePlayStatManager.Instance.Stat[StatType.LostGoldFromPrestoge];
                saveData.levelUpHeroTimes = GamePlayStatManager.Instance.Stat[StatType.LevelUpHeroTimes];
                saveData.openChest = GamePlayStatManager.Instance.Stat[StatType.OpenChest];
                saveData.getFairyPresent = GamePlayStatManager.Instance.Stat[StatType.GetFairyPresent];
                saveData.useJumpAttack = GamePlayStatManager.Instance.Stat[StatType.UseJumpAttack];
                saveData.getCriticalHit = GamePlayStatManager.Instance.Stat[StatType.GetCriticalHit];
                saveData.firstPlayTime = GamePlayStatManager.Instance.Stat[StatType.FirstPlayTime];
                saveData.playingDuration = GamePlayStatManager.Instance.Stat[StatType.PlayingDuration];
                saveData.currentTotalHeroLevel = GamePlayStatManager.Instance.Stat[StatType.CurrentTotalHeroLevel];
                saveData.currentTapChance = GamePlayStatManager.Instance.Stat[StatType.CurrentTapChance];
                saveData.currentDPSMultiplier = GamePlayStatManager.Instance.Stat[StatType.CurrentDPSMultiplier];
                saveData.currentCriticalHitMultiplier = GamePlayStatManager.Instance.Stat[StatType.CurrentCriticalHitMultiplier];
                saveData.currentTotalGoldMultiplier = GamePlayStatManager.Instance.Stat[StatType.CurrentTotalGoldMultiplier];
                saveData.lastPrestigeTime = GamePlayStatManager.Instance.Stat[StatType.LastPrestigeTime];
            }

            if (FBManager.Instance != null)
                saveData.pp_FBInvitedList = PlayerPrefs.GetString(FBManager.Instance.PP_FB_INVITED_LIST, "");

            if (AdMobManager.Instance != null)
                saveData.noAdmob = AdMobManager.Instance.ShouldNoADs;

            saveData.appRated = PlayerPrefs.GetInt("APP_RATED", 0) > 0;
            saveData.facebookLiked = PlayerPrefs.GetInt("FACEBOOK_LIKED", 0) > 0;
            saveData.twitterFollowed = PlayerPrefs.GetInt("TWITTER_FOLLOWED", 0) > 0;

        }

        void UpdatePurchasedItemList(SaveData saveData)
        {
            saveData.key001Purchased = AOMAssets.AnotherWorldKeyGIVG.GetBalance() > 0;
            saveData.key002Purchased = AOMAssets.SchoolKeyGIVG.GetBalance() > 0;
            saveData.key003Purchased = AOMAssets.ForestKeyGIVG.GetBalance() > 0;
            saveData.key004Purchased = AOMAssets.SummerKeyGIVG.GetBalance() > 0;
            saveData.key005Purchased = AOMAssets.HalloweenKeyGIVG.GetBalance() > 0;
            saveData.key006Purchased = AOMAssets.FukubukuroKeyGIVG.GetBalance() > 0;

            saveData.pvpGiftKey001Purchased = AOMAssets.PvPKey001GIVG.GetBalance() > 0;
            saveData.pvpGiftKey002Purchased = AOMAssets.PvPKey002GIVG.GetBalance() > 0;

            saveData.goldPackPurchased = PlayerPrefs.GetInt("promotional_goldpack_PURCHASED", 0) > 0;
            saveData.silverPackPurchased = PlayerPrefs.GetInt("promotional_silverpack_PURCHASED", 0) > 0;
            saveData.bronzePackPurchased = PlayerPrefs.GetInt("promotional_bronzepack_PURCHASED", 0) > 0;
        }

        void UpdateTutorialProgress(SaveData saveData)
        {
            if (TutorialManager.Instance != null)
            {
                saveData.tutorialCompleted = TutorialManager.Instance.CheckTutorialCompletedById(2);
                //PlayerPrefs.SetInt("UserTutorialCompleted", saveData.tutorialCompleted ? 1 : 0);
            }
        }

    }
    # endregion

    public class CloudDataManager
    {
        private readonly SocialManager _socialManager;
        private readonly IUserService _userService;
        private readonly SaveDataService _saveDataService;

        private readonly UIViewLoader _uiViewLoader;

        [Inject]
        public CloudDataManager(SocialManager socialManager, IUserService userService, SaveDataService saveDataService, UIViewLoader uiViewLoader)
        {
            _socialManager = socialManager;
            _userService = userService;
            _saveDataService = saveDataService;

            _uiViewLoader = uiViewLoader;
        }

        // Variables
        SaveData currentSaveData = null;
        ISavedGameMetadata currentGame = null;

        public bool progressLoaded = false;

        public bool allCloudDataRestored = false;

        CloudSavingOverlayController cloudSavingOverlayController;

        public SaveData CurrentSaveData
        {
            set
            {
                currentSaveData = value;
            }
            get
            {
                return currentSaveData;
            }
        }

        public DateTime lastSaveDateTime
        {
            get
            {
                if (_userService.LastSaveDateTime != "")
                    return DateTime.Parse(_userService.LastSaveDateTime);

                return DateTime.UtcNow;
            }
        }

        // Read the save game
        void ReadSaveGame(string filename, Action<SavedGameRequestStatus, ISavedGameMetadata> callback)
        {
            // Check if signed in
            if (_socialManager.googleSignedIn)
            {
                ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

                savedGameClient.OpenWithAutomaticConflictResolution(filename,
                    DataSource.ReadCacheOrNetwork,
                    ConflictResolutionStrategy.UseLongestPlaytime,
                    callback);
            }
        }

        // Write the save game
        void WriteSaveGame(ISavedGameMetadata game, byte[] savedData, Action<SavedGameRequestStatus, ISavedGameMetadata> callback)
        {
            // Check if signed in
            if (_socialManager.googleSignedIn)
            {
                SavedGameMetadataUpdate updatedMetadata = new SavedGameMetadataUpdate.Builder()
                    .WithUpdatedPlayedTime(TimeSpan.FromMinutes(game.TotalTimePlayed.Minutes + 1))
                    .WithUpdatedDescription("Saved at: " + DateTime.Now)
                    .Build();

                ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
                savedGameClient.CommitUpdate(game, updatedMetadata, savedData, callback);
            }
        }

        public void SaveProgressToParseUser(bool syncWithGoogleCloud, Action successCallback = null, Action failCallback = null)
        {
            if (SocialManager.CheckForInternetConnection())
            {
                if (_socialManager.parseSignedIn)
                {
                    SaveData saveData = syncWithGoogleCloud ? (CurrentSaveData != null ? CurrentSaveData : _saveDataService.CreateSaveData()) : _saveDataService.CreateSaveData();

                    _userService.SaveGameData(saveData, (e) =>
                    {
                        if (e != null)
                        {
                            Debug.LogException(e);

                            if (failCallback != null)
                                failCallback();
                        }
                        else
                        {
                            if (successCallback != null)
                                successCallback();
                        }
                    });
                }
                else
                {
                    if (failCallback != null)
                        failCallback();
                }
            }
            else
            {
                if (failCallback != null)
                    failCallback();
            }
        }

        public void SaveProgressToGoogleCloud()
        {
            // Check if signed in
            if (SocialManager.CheckForInternetConnection())
            {
                if (_socialManager.googleSignedIn)
                {
                    ShowMessageOverlay();

                    if (cloudSavingOverlayController != null)
                        cloudSavingOverlayController.ShowSavingMessage();

                    SaveData saveData = _saveDataService.CreateSaveData();

                    // Save game callback
                    Action<SavedGameRequestStatus, ISavedGameMetadata> writeCallback =
                        (SavedGameRequestStatus status, ISavedGameMetadata game) =>
                        {
                            Debug.Log("writeCallback - " + status.ToString());
                            if (status == SavedGameRequestStatus.Success)
                            {
                                if (cloudSavingOverlayController != null)
                                    cloudSavingOverlayController.ShowSavedMessage();

                                cloudSavingOverlayController = null;
                            }
                            else
                            {
                                if (cloudSavingOverlayController != null)
                                    cloudSavingOverlayController.ShowSaveFailMessage();

                                cloudSavingOverlayController = null;
                            }
                        };

                    // Read binary callback
                    Action<SavedGameRequestStatus, byte[]> readBinaryCallback =
                        (SavedGameRequestStatus status, byte[] data) =>
                        {
                            Debug.Log("readBinaryCallback - " + status.ToString());

                            WriteSaveGame(currentGame, SaveData.ToBytes(saveData), writeCallback);
                        };

                    // Read game callback
                    Action<SavedGameRequestStatus, ISavedGameMetadata> readCallback =
                        (SavedGameRequestStatus status, ISavedGameMetadata game) =>
                        {
                            // Check if read was successful
                            if (status == SavedGameRequestStatus.Success)
                            {
                                currentGame = game;
                                PlayGamesPlatform.Instance.SavedGame.ReadBinaryData(game, readBinaryCallback);
                            }
                        };

                    // Replace "MySaveGame" with whatever you would like to save file to be called
                    ReadSaveGame("MySaveGame", readCallback);
                }
                else
                {
                    ShowMessageOverlay();

                    if (cloudSavingOverlayController != null)
                        cloudSavingOverlayController.ShowSigninMessage();

                    Action signInSuccessCallback = () => SaveProgressToGoogleCloud();

                    Action signInFailCallback = () =>
                    {
                        if (cloudSavingOverlayController != null)
                            cloudSavingOverlayController.ShowSigninFailMessage();

                        cloudSavingOverlayController = null;
                    };

                    _socialManager.GoogleSignIn(true, signInSuccessCallback, signInFailCallback);
                }
            }
            else
            {
                ShowMessageOverlay();

                if (cloudSavingOverlayController != null)
                    cloudSavingOverlayController.ShowSaveFailMessage();

                cloudSavingOverlayController = null;
            }
        }

        // Load the game
        public void LoadProgressFromGoogleCloud(bool init = false, Action successCallback = null, Action failCallback = null)
        {
            if (SocialManager.CheckForInternetConnection())
            {
                // Check if signed in
                if (_socialManager.googleSignedIn)
                {
                    ShowMessageOverlay();

                    if (cloudSavingOverlayController != null)
                        cloudSavingOverlayController.ShowLoadingMessage();

                    // Read binary callback
                    Action<SavedGameRequestStatus, byte[]> readBinaryCallback =
                        (SavedGameRequestStatus status, byte[] data) =>
                        {
                            // Check if read was successful
                            if (status == SavedGameRequestStatus.Success)
                            {
                                // Load game data
                                try
                                {
                                    currentSaveData = SaveData.FromBytes(data);

                                    if (currentSaveData != null)
                                    {
                                        // We are displaying these values for the purpose of the tutorial.
                                        // Normally you would set the values here.

                                        Debug.Log("Player Name = " + currentSaveData.playerName);
                                        Debug.Log("Golds = " + currentSaveData.golds);
                                        Debug.Log("Diamonds = " + currentSaveData.diamonds);
                                        Debug.Log("Relics = " + currentSaveData.relics);
                                        Debug.Log("Stage = " + currentSaveData.stage);

                                        Debug.Log("Wave = " + currentSaveData.wave);
                                        Debug.Log("TutorialCompleted = " + currentSaveData.tutorialCompleted);
                                        Debug.Log("Avatar Level = " + currentSaveData.avatarLevel);

                                        for (int i = 0; i < currentSaveData.avatarSkillList.Count; i++)
                                        {
                                            Debug.Log("Avatar Skill" + (i + 1) + " Level = " + currentSaveData.avatarSkillList[i]);
                                        }

                                        Debug.Log("Unlocked Hero Count = " + currentSaveData.heroList.Count);
                                        for (int i = 0; i < currentSaveData.heroList.Count; i++)
                                        {
                                            Debug.Log("Hero" + (i + 1) + " Level = " + currentSaveData.heroList[i]);
                                            Debug.Log("Hero" + (i + 1) + " Alive = " + currentSaveData.aliveHeroList[i]);
                                            Debug.Log("Unlocked Hero" + (i + 1) + " Passive Skill Balance = " + currentSaveData.heroPassiveList[i]);
                                        }

                                        Debug.Log("Owned Artifact Count = " + currentSaveData.artifactList.Count);
                                        for (int i = 0; i < currentSaveData.artifactList.Count; i++)
                                        {
                                            Debug.Log("Owned Artifact" + (i + 1) + " Level = " + currentSaveData.artifactList[i]);
                                        }

                                        Debug.Log("Girl Count = " + currentSaveData.girlStatusList.Count);
                                        for (int i = 0; i < currentSaveData.girlStatusList.Count; i++)
                                        {
                                            Debug.Log("Girl" + (i + 1) + " Status = " + currentSaveData.girlStatusList[i]);
                                            Debug.Log("Girl" + (i + 1) + " EXP = " + currentSaveData.girlExpList[i]);
                                            Debug.Log("Girl" + (i + 1) + " Reached Break Level = " + currentSaveData.girlReachedBreakLevelList[i]);
                                        }

                                        Debug.Log("TouchCredit - " + currentSaveData.touchCredit);
                                        Debug.Log("MaxTouchCredit - " + currentSaveData.touchCredit_Max);
                                        Debug.Log("LastTouch - " + currentSaveData.lastTouch);
                                        Debug.Log("CreditRechargeTime - " + currentSaveData.creditRechargeTime);

                                        Debug.Log("Achievement Count = " + currentSaveData.achievementList.Count);
                                        for (int i = 0; i < currentSaveData.achievementList.Count; i++)
                                        {
                                            Debug.Log("Achievement" + (i + 1) + " Balance = " + currentSaveData.achievementList[i]);
                                        }
                                    }

                                    progressLoaded = true;

                                    if (init)
                                    {
                                        if (cloudSavingOverlayController != null)
                                            cloudSavingOverlayController.CloseView();

                                        cloudSavingOverlayController = null;

                                        if (successCallback != null)
                                        {
                                            successCallback();
                                        }
                                    }
                                    else
                                    {
                                        if (cloudSavingOverlayController != null)
                                            cloudSavingOverlayController.ShowLoadedMessage();

                                        cloudSavingOverlayController = null;

                                        if (successCallback != null)
                                        {
                                            successCallback();
                                        }

                                        if (GameManager.Instance != null)
                                        {
                                            GameManager.Instance.SyncAllCloudData();

#if UNITY_EDITOR
                                            UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_IOS || UNITY_ANDROID
                                            Application.Quit();
#endif
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    Debug.LogError("Failed to read binary data: " + e.ToString());

                                    if (cloudSavingOverlayController != null)
                                        cloudSavingOverlayController.ShowLoadFailMessage();

                                    progressLoaded = false;

                                    if (failCallback != null)
                                    {
                                        failCallback();
                                    }
                                }
                            }
                            else
                            {
                                if (cloudSavingOverlayController != null)
                                    cloudSavingOverlayController.ShowLoadFailMessage();

                                progressLoaded = false;

                                if (failCallback != null)
                                {
                                    failCallback();
                                }
                            }
                        };

                    // Read game callback
                    Action<SavedGameRequestStatus, ISavedGameMetadata> readCallback =
                        (SavedGameRequestStatus status, ISavedGameMetadata game) =>
                        {
                            // Check if read was successful
                            if (status == SavedGameRequestStatus.Success)
                            {
                                currentGame = game;
                                PlayGamesPlatform.Instance.SavedGame.ReadBinaryData(game, readBinaryCallback);
                            }
                            else
                            {
                                if (cloudSavingOverlayController != null)
                                    cloudSavingOverlayController.ShowLoadFailMessage();

                                progressLoaded = false;

                                if (failCallback != null)
                                {
                                    failCallback();
                                }
                            }
                        };

                    // Replace "MySaveGame" with whatever you would like to save file to be called
                    ReadSaveGame("MySaveGame", readCallback);
                }
                else
                {
                    ShowMessageOverlay();

                    if (cloudSavingOverlayController != null)
                        cloudSavingOverlayController.ShowSigninMessage();

                    Action signInSuccessCallback = () => LoadProgressFromGoogleCloud(init);

                    Action signInFailCallback = () =>
                    {
                        if (cloudSavingOverlayController != null)
                            cloudSavingOverlayController.ShowSigninFailMessage();

                        progressLoaded = false;

                        cloudSavingOverlayController = null;

                        if (failCallback != null)
                        {
                            failCallback();
                        }
                    };

                    _socialManager.GoogleSignIn(true, signInSuccessCallback, signInFailCallback);
                }
            }
            else
            {
                ShowMessageOverlay();

                if (cloudSavingOverlayController != null)
                    cloudSavingOverlayController.ShowLoadFailMessage();

                progressLoaded = false;

                cloudSavingOverlayController = null;

                if (failCallback != null)
                {
                    failCallback();
                }
            }
        }

        public void LoadProgressFromParse(Action successCallback = null, Action failCallback = null)
        {
            if (SocialManager.CheckForInternetConnection())
            {
                // Check if signed in
                if (_socialManager.parseSignedIn)
                {
                    ShowMessageOverlay();

                    if (cloudSavingOverlayController != null)
                        cloudSavingOverlayController.ShowLoadingMessage();

                    _userService.LoadGameData((e, savaData) =>
                    {
                        if (e != null)
                        {
                            Debug.LogException(e);

                            if (cloudSavingOverlayController != null)
                                cloudSavingOverlayController.ShowLoadFailMessage();

                            cloudSavingOverlayController = null;

                            progressLoaded = false;

                            if (failCallback != null)
                            {
                                failCallback();
                            }
                        }
                        else
                        {
                            if (savaData != null)
                            {
                                if (cloudSavingOverlayController != null)
                                    cloudSavingOverlayController.CloseView();

                                cloudSavingOverlayController = null;

                                CurrentSaveData = savaData;
                                progressLoaded = true;

                                if (successCallback != null)
                                {
                                    successCallback();
                                }
                            }
                            else
                            {
                                if (_socialManager.googleSignedIn)
                                {
                                    Action loadFromGoogleCloudSuccessCallback = () =>
                                    {
                                        if (cloudSavingOverlayController != null)
                                            cloudSavingOverlayController.ShowSavingMessage();

                                        Action saveProgressToParseSuccessCallback = () =>
                                        {
                                            if (cloudSavingOverlayController != null)
                                                cloudSavingOverlayController.CloseView();

                                            cloudSavingOverlayController = null;

                                            progressLoaded = true;

                                            if (successCallback != null)
                                            {
                                                successCallback();
                                            }
                                        };

                                        Action saveProgressToParseFailCallback = () =>
                                        {
                                            if (cloudSavingOverlayController != null)
                                                cloudSavingOverlayController.ShowSaveFailMessage();

                                            cloudSavingOverlayController = null;

                                            progressLoaded = false;

                                            if (failCallback != null)
                                            {
                                                failCallback();
                                            }
                                        };

                                        SaveProgressToParseUser(true, saveProgressToParseSuccessCallback, saveProgressToParseFailCallback);

                                    };

                                    Action loadFromGoogleCloudFailCallback = () =>
                                    {
                                        if (cloudSavingOverlayController != null)
                                            cloudSavingOverlayController.ShowLoadFailMessage();

                                        cloudSavingOverlayController = null;

                                        progressLoaded = false;

                                        if (failCallback != null)
                                        {
                                            failCallback();
                                        }
                                    };

                                    LoadProgressFromGoogleCloud(true, loadFromGoogleCloudSuccessCallback, loadFromGoogleCloudFailCallback);
                                }
                                else
                                {
                                    if (cloudSavingOverlayController != null)
                                        cloudSavingOverlayController.ShowLoadFailMessage();

                                    cloudSavingOverlayController = null;

                                    progressLoaded = false;

                                    if (failCallback != null)
                                    {
                                        failCallback();
                                    }
                                }
                            }
                        }
                    });
                }
                else
                {
                    ShowMessageOverlay();

                    if (cloudSavingOverlayController != null)
                        cloudSavingOverlayController.ShowLoadFailMessage();

                    cloudSavingOverlayController = null;

                    progressLoaded = false;

                    if (failCallback != null)
                    {
                        failCallback();
                    }
                }
            }
            else
            {
                ShowMessageOverlay();

                if (cloudSavingOverlayController != null)
                    cloudSavingOverlayController.ShowLoadFailMessage();

                cloudSavingOverlayController = null;

                progressLoaded = false;

                if (failCallback != null)
                {
                    failCallback();
                }
            }
        }

        public void ShowWarningOverlay(string warningType)
        {
            GameObject overlay = _uiViewLoader.LoadOverlayView("SaveLoadWarningOverlay", null, false, Time.timeScale == 0);

            if (warningType == "SAVE")
            {
                overlay.transform.FindChild("SaveSubtitle").gameObject.SetActive(true);
                overlay.transform.FindChild("LoadSubtitle").gameObject.SetActive(false);

                if (lastSaveDateTime != DateTime.UtcNow)
                {
                    overlay.transform.FindChild("LatestProgressDateTime").GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("saveLoadWarning_lastUpdate") + "\n" + lastSaveDateTime.ToLocalTime().ToString();
                }
                else
                {
                    overlay.transform.FindChild("LatestProgressDateTime").GetComponent<Text>().text = "";
                }

                overlay.transform.FindChild("BtnYes").GetComponent<Button>().onClick.AddListener(() =>
                {
                    //SaveProgressToGoogleCloud();

                    GameObject saveProgressOverlay = _uiViewLoader.LoadOverlayView("CloudSavingOverlay", null, false, Time.timeScale == 0);
                    CloudSavingOverlayController saveProgressOverlayController = saveProgressOverlay.GetComponent<CloudSavingOverlayController>(); 
                    saveProgressOverlayController.ShowSavingMessage();

                    Action SaveProgressToParseSuccessCallback = () =>
                    {
                        if (saveProgressOverlayController != null)
                            saveProgressOverlayController.ShowSavedMessage();
                    };

                    Action SaveProgressToParseFailCallback = () =>
                    {
                        if (saveProgressOverlayController != null)
                            saveProgressOverlayController.ShowSaveFailMessage();
                    };

                    SaveProgressToParseUser(false, SaveProgressToParseSuccessCallback, SaveProgressToParseFailCallback);

                    GameObject.Destroy(overlay);
                });
            }
            else if (warningType == "LOAD")
            {
                overlay.transform.FindChild("SaveSubtitle").gameObject.SetActive(false);
                overlay.transform.FindChild("LoadSubtitle").gameObject.SetActive(true);

                if (lastSaveDateTime != DateTime.UtcNow)
                {
                    overlay.transform.FindChild("LatestProgressDateTime").GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("saveLoadWarning_lastUpdate") + "\n" + lastSaveDateTime.ToLocalTime().ToString();
                }
                else
                {
                    overlay.transform.FindChild("LatestProgressDateTime").GetComponent<Text>().text = "";
                }

                overlay.transform.FindChild("BtnYes").GetComponent<Button>().onClick.AddListener(() =>
                {
                    LoadProgressFromGoogleCloud();
                    GameObject.Destroy(overlay);
                });
            }

            overlay.transform.FindChild("BtnNo").GetComponent<Button>().onClick.AddListener(() =>
            {
                GameObject.Destroy(overlay);
            });
        }

        public void ShowMessageOverlay()
        {
            if (cloudSavingOverlayController == null)
            {
                GameObject overlay = _uiViewLoader.LoadOverlayView("CloudSavingOverlay", null, false, Time.timeScale == 0);
                cloudSavingOverlayController = overlay.GetComponent<CloudSavingOverlayController>();
            }
        }
    }
}