﻿using Ignite.AOM;
using UnityEngine;
using Zenject;
using System;

public class ParseDevUIController : MonoBehaviour
{
    private SocialManager _socialManager;

    [Inject]
    public void Construct(SocialManager socialManager)
    {
        _socialManager = socialManager;
    }

    public void TestButtonClicked()
    {
        Debug.Log("Sign up...");

        // Create Account
        Action successCallback = () =>
        {
            Debug.Log("Sign up success - " + _socialManager.CurrentParseUser.UserId);
        };

        Action failCallback = () =>
        {
            Debug.Log("Sign up fail");
        };

        _socialManager.ParseSignUp(successCallback, failCallback);
    }
}
