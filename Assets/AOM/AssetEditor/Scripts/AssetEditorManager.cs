﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using Ignite.AOM;
using SmartLocalization;

public class AssetEditorManager : MonoBehaviour
{
    public static AssetEditorManager instance;

    public InputField idField;

    public string loadedAssetType;
    public Text loadedObjName;
    public Enemy enemy;
    public GameObject loadedPrefabObj;
    public Transform assetParent;

    public Vector3 moetanScale, miniMoetanScale, treasureChestScale, enemyScale;

    public GameObject breakScene;
    public GameObject breakCharPosition;
    public SpriteRenderer breakSceneChar;

    // Use this for initialization
    void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Break()
    {
        if (breakScene.activeSelf)
            breakScene.SetActive(false);

        if (idField.text != "")
        {
            int id = int.Parse(idField.text);

            if (enemy != null && loadedAssetType == "MOETAN")
            {
                breakScene.SetActive(true);
                breakSceneChar.sprite = Resources.Load<Sprite>("AssetBundles_Resource/Moetan/Texture/mo" + id.ToString("000") + "/mo_" + id.ToString("000") + "_base" + enemy.breakLevel.ToString("00"));
            }
            else
            {
                Debug.LogError("Please Load Moetan's Prefab First");
            }
        }
        else
        {
            Debug.LogError("Please Input Moetan ID");
        }
    }

    public void LoadEnemy(string assetType)
    {
        UnloadAsset();

        if (idField.text != "")
        {
            int id = int.Parse(idField.text);
            string path = "";

            Vector3 assetScale = Vector3.one;

            try
            {
                loadedAssetType = assetType;

                switch (assetType)
                {
                    case "MOETAN":
                        path = "AssetBundles_Resource/Moetan/Prefab/Boss_" + id.ToString("000");
                        assetScale = moetanScale;
                        break;

                    case "MINI_MOETAN":
                        path = "AssetBundles_Resource/Mini_Moetan/Prefab/Mt_" + id.ToString("000");
                        assetScale = miniMoetanScale;
                        break;

                    case "ENEMY":
                        path = "AssetBundles_Resource/Enemy/Prefab/Enemy_" + id.ToString("000");
                        assetScale = id > 0 ? enemyScale : treasureChestScale;
                        break;
                }

                loadedPrefabObj = (GameObject)Instantiate(Resources.Load(path));
                loadedPrefabObj.name = loadedPrefabObj.name.Replace("(Clone)", "");
                loadedPrefabObj.transform.SetParent(assetParent, false);
                assetParent.localScale = assetScale;

                loadedObjName.text = LanguageManager.Instance.GetTextValue(loadedPrefabObj.name + "_name");

                enemy = (Enemy)loadedPrefabObj.GetComponent(typeof(Enemy));
                enemy.SpawnAnimation();
                enemy.breakLevel = 1;
            }
            catch (Exception e)
            {
                switch (assetType)
                {
                    case "MOETAN":
                        Debug.LogError("Boss_" + id.ToString("000") + ": Unavailable\n" + e);
                        break;

                    case "MINI_MOETAN":
                        Debug.LogError("Mt_" + id.ToString("000") + ": Unavailable\n" + e);
                        break;

                    case "ENEMY":
                        Debug.LogError("Enemy_" + id.ToString("000") + ": Unavailable\n" + e);
                        break;
                }
            }
        }
        else
        {
            Debug.LogError("Please Input Asset ID");
        }
    }

    public void UnloadAsset()
    {
        Destroy(loadedPrefabObj);
        loadedPrefabObj = null;

        enemy = null;

        loadedAssetType = "";
    }

    public void Hit()
    {
        if (enemy != null)
        {
            enemy.monsterAnim.Play("Hit", 0, 0);
        }
        else
        {
            Debug.LogError("Please Load Prefab First");
        }
    }

    public void Rub()
    {
        if (enemy != null && loadedAssetType == "MOETAN")
        {
            enemy.monsterAnim.Play("Touch", 0, 0);
        }
        else
        {
            Debug.LogError("Please Load Moetan's Prefab First");
        }
    }

    public void ChangeBreakLevel(int level)
    {
        if (idField.text != "")
        {
            int id = int.Parse(idField.text);
            enemy.breakLevel = level;

            if (enemy != null && loadedAssetType == "MOETAN")
            {
                enemy.spriteRenderer.sprite = Resources.Load<Sprite>("AssetBundles_Resource/Moetan/Texture/mo" + id.ToString("000") + "/mo_" + id.ToString("000") + "_base" + enemy.breakLevel.ToString("00"));
            }
            else
            {
                Debug.LogError("Please Load Moetan's Prefab First");
            }
        }
        else
        {
            Debug.LogError("Please Input Moetan ID");
        }
    }
}
