﻿//using UnityEngine;
//using System;
//using NUnit.Framework;
//
//namespace Ignite.AOM {
//	
//	[TestFixture]
//	public class AvatarSkillCoreTest : AOMCoreTest
//	{
//        [Test]
//        [Ignore("TBD")]
//        public void TestAvatarASWarCry() {
//            // TODO Implement this
//        }
//        
//        [Test]
//        public void TestHeavenlyStrikeCooldown() {
//            AvatarActiveSkillVG hsvg = AOMAssets.HeavenlyStrikeASVG;
//
//            // Heavenly Strike not yet unlocked
//            Assert.AreEqual (0, hsvg.GetBalance());
//			Assert.IsFalse(avatarSkillCore.ActivateHeavenlyStrike());
//			Debug.Log("avatarSkillCore.HeavenlyStrikeCooldown(): " + avatarSkillCore.HeavenlyStrikeCooldown());
//			Assert.AreEqual(avatarSkillCore.HeavenlyStrikeCooldown(), 600);
//			Assert.AreEqual(0, avatarSkillCore.HeavenlyStrikeRemainCD());
//
//            // Unlock skill
//            hsvg.ResetBalance(1);
//            Assert.IsTrue(avatarSkillCore.ActivateHeavenlyStrike());
//            Assert.Greater(avatarSkillCore.HeavenlyStrikeRemainCD(), 0);
//        }
//        
//        
//        [Test]
//        public void TestHeavenlyStrikeCooldownWithAR011Buff() {
//
//            AvatarActiveSkillVG hsvg = AOMAssets.HeavenlyStrikeASVG;
//            // Unlock skill
//            hsvg.ResetBalance(1);
//            Assert.IsTrue(avatarSkillCore.ActivateHeavenlyStrike());
//            Assert.AreEqual(600, avatarSkillCore.HeavenlyStrikeCooldown());
//            Assert.LessOrEqual(avatarSkillCore.HeavenlyStrikeRemainCD(), 600);
//
//            // Unlock Artifact 011
//            avatarSkillCore.RefreshSkill(true);
//            Assert.AreEqual(0, avatarSkillCore.HeavenlyStrikeRemainCD());
//            ArtifactAssets.Artifact011.ResetBalance(1);
//            Assert.AreEqual(570, avatarSkillCore.HeavenlyStrikeCooldown());
//            Assert.IsTrue(avatarSkillCore.ActivateHeavenlyStrike());
//            Assert.LessOrEqual(avatarSkillCore.HeavenlyStrikeRemainCD(), 570);
//
//            // Lv 2
//            avatarSkillCore.RefreshSkill(true);
//            Assert.AreEqual(0, avatarSkillCore.HeavenlyStrikeRemainCD());
//            ArtifactAssets.Artifact011.ResetBalance(2);
//            Assert.AreEqual(540, avatarSkillCore.HeavenlyStrikeCooldown());
//            Assert.IsTrue(avatarSkillCore.ActivateHeavenlyStrike());
//            Assert.LessOrEqual(avatarSkillCore.HeavenlyStrikeRemainCD(), 540);
//
//        }
//
//        [Test]
//        public void TestShadownCloneCooldown() {
//            AvatarActiveSkillVG asvg = AOMAssets.ShadowCloneASVG;
//            
//            // Shadown Clone not yet unlocked
//            Assert.AreEqual (0, asvg.GetBalance());
//            Assert.IsFalse(avatarSkillCore.ActivateShadowClone(false));
////            Debug.Log("avatarSkillCore.HeavenlyStrikeCooldown(): " + avatarSkillCore.HeavenlyStrikeCooldown());
//            Assert.AreEqual(avatarSkillCore.ShadowCloneCooldown(), 600);
//            Assert.AreEqual(0, avatarSkillCore.ShadowCloneRemainCD());
//            
//            // Unlock skill
//            asvg.ResetBalance(1);
//            Assert.IsTrue(avatarSkillCore.ActivateShadowClone(false));
//            Assert.Greater(avatarSkillCore.ShadowCloneRemainCD(), 0);
//            Assert.LessOrEqual(avatarSkillCore.ShadowCloneRemainCD(), 600);
//
//            // Unlock Artifact 006
//            avatarSkillCore.RefreshSkill(true);
//            Assert.AreEqual(0, avatarSkillCore.ShadowCloneRemainCD());
//            ArtifactAssets.Artifact006.ResetBalance(1);
//            Assert.AreEqual(570, avatarSkillCore.ShadowCloneCooldown());
//            Assert.IsTrue(avatarSkillCore.ActivateShadowClone(false));
//            Assert.LessOrEqual(avatarSkillCore.ShadowCloneRemainCD(), 570);
//            
//            // Lv 2
//            avatarSkillCore.RefreshSkill(true);
//            Assert.AreEqual(0, avatarSkillCore.ShadowCloneRemainCD());
//            ArtifactAssets.Artifact006.ResetBalance(2);
//            Assert.AreEqual(540, avatarSkillCore.ShadowCloneCooldown());
//            Assert.IsTrue(avatarSkillCore.ActivateShadowClone(false));
//            Assert.LessOrEqual(avatarSkillCore.ShadowCloneRemainCD(), 540);
//        }
//        
//        [Test]
//        public void TestWarCryCooldown() {
//            AvatarActiveSkillVG asvg = AOMAssets.WarCryASVG;
//            
//            // War Cry not yet unlocked
//            Assert.AreEqual (0, asvg.GetBalance());
//            Assert.IsFalse(avatarSkillCore.ActivateWarCry());
//            Assert.AreEqual(avatarSkillCore.WarCryCooldown(), 1800);
//            Assert.AreEqual(0, avatarSkillCore.WarCryRemainCD());
//            
//            // Unlock skill
//            asvg.ResetBalance(1);
//            Assert.IsTrue(avatarSkillCore.ActivateWarCry());
//            Assert.Greater(avatarSkillCore.WarCryRemainCD(), 0);
//            Assert.LessOrEqual(avatarSkillCore.WarCryRemainCD(), 1800);
//            
//            // Unlock Artifact 008
//            avatarSkillCore.RefreshSkill(true);
//            Assert.AreEqual(0, avatarSkillCore.WarCryRemainCD());
//            ArtifactAssets.Artifact008.ResetBalance(1);
//            Assert.AreEqual(1710, avatarSkillCore.WarCryCooldown());
//            Assert.IsTrue(avatarSkillCore.ActivateWarCry());
//            Assert.LessOrEqual(avatarSkillCore.WarCryRemainCD(), 1710);
//            
//            // Lv 2
//            avatarSkillCore.RefreshSkill(true);
//            Assert.AreEqual(0, avatarSkillCore.WarCryRemainCD());
//            ArtifactAssets.Artifact008.ResetBalance(2);
//            Assert.AreEqual(1620, avatarSkillCore.WarCryCooldown());
//            Assert.IsTrue(avatarSkillCore.ActivateWarCry());
//            Assert.LessOrEqual(avatarSkillCore.WarCryRemainCD(), 1620);
//        }
//        
//        [Test]
//        public void TestCriticalStrikeCooldown() {
//            AvatarActiveSkillVG asvg = AOMAssets.CriticalStrikeASVG;
//            
//            // Critical Strike not yet unlocked
//            Assert.AreEqual (0, asvg.GetBalance());
//            Assert.IsFalse(avatarSkillCore.ActivateCriticalStrike());
//            Assert.AreEqual(avatarSkillCore.CriticalStrikeCooldown(), 1800);
//            Assert.AreEqual(0, avatarSkillCore.CriticalStrikeRemainCD());
//            
//            // Unlock skill
//            asvg.ResetBalance(1);
//            Assert.IsTrue(avatarSkillCore.ActivateCriticalStrike());
//            Assert.Greater(avatarSkillCore.CriticalStrikeRemainCD(), 0);
//            Assert.LessOrEqual(avatarSkillCore.CriticalStrikeRemainCD(), 1800);
//            
//            // Unlock Artifact 008
//            avatarSkillCore.RefreshSkill(true);
//            Assert.AreEqual(0, avatarSkillCore.CriticalStrikeRemainCD());
//            ArtifactAssets.Artifact007.ResetBalance(1);
//            Assert.AreEqual(1710, avatarSkillCore.CriticalStrikeCooldown());
//            Assert.IsTrue(avatarSkillCore.ActivateCriticalStrike());
//            Assert.LessOrEqual(avatarSkillCore.CriticalStrikeRemainCD(), 1710);
//            
//            // Lv 2
//            avatarSkillCore.RefreshSkill(true);
//            Assert.AreEqual(0, avatarSkillCore.CriticalStrikeRemainCD());
//            ArtifactAssets.Artifact007.ResetBalance(2);
//            Assert.AreEqual(1620, avatarSkillCore.CriticalStrikeCooldown());
//            Assert.IsTrue(avatarSkillCore.ActivateCriticalStrike());
//            Assert.LessOrEqual(avatarSkillCore.CriticalStrikeRemainCD(), 1620);
//        }
//        
//        [Test]
//        public void TestBeserkerRageCooldown() {
//            AvatarActiveSkillVG asvg = AOMAssets.BerserkerRageASVG;
//            
//            // BerserkerRage not yet unlocked
//            Assert.AreEqual (0, asvg.GetBalance());
//            Assert.IsFalse(avatarSkillCore.ActivateBerserkerRage());
//            Assert.AreEqual(avatarSkillCore.BerserkerRageCooldown(), 3600);
//            Assert.AreEqual(0, avatarSkillCore.BerserkerRageRemainCD());
//            
//            // Unlock skill
//            asvg.ResetBalance(1);
//            Assert.IsTrue(avatarSkillCore.ActivateBerserkerRage());
//            Assert.Greater(avatarSkillCore.BerserkerRageRemainCD(), 0);
//            Assert.LessOrEqual(avatarSkillCore.BerserkerRageRemainCD(), 3600);
//            
//            // Unlock Artifact 008
//            avatarSkillCore.RefreshSkill(true);
//            Assert.AreEqual(0, avatarSkillCore.BerserkerRageRemainCD());
//            ArtifactAssets.Artifact010.ResetBalance(1);
//            Assert.AreEqual(3420, avatarSkillCore.BerserkerRageCooldown());
//            Assert.IsTrue(avatarSkillCore.ActivateBerserkerRage());
//            Assert.LessOrEqual(avatarSkillCore.BerserkerRageRemainCD(), 3420);
//            
//            // Lv 2
//            avatarSkillCore.RefreshSkill(true);
//            Assert.AreEqual(0, avatarSkillCore.BerserkerRageRemainCD());
//            ArtifactAssets.Artifact010.ResetBalance(2);
//            Assert.AreEqual(3240, avatarSkillCore.BerserkerRageCooldown());
//            Assert.IsTrue(avatarSkillCore.ActivateBerserkerRage());
//            Assert.LessOrEqual(avatarSkillCore.BerserkerRageRemainCD(), 3240);
//        }
//        
//        [Test]
//        public void TestHandOfMidasCooldown() {
//            AvatarActiveSkillVG asvg = AOMAssets.HandOfMidasASVG;
//            
//            // Hands of Midas not yet unlocked
//            Assert.AreEqual (0, asvg.GetBalance());
//            Assert.IsFalse(avatarSkillCore.ActivateHandOfMidas());
//            Assert.AreEqual(avatarSkillCore.HandOfMidasCooldown(), 3600);
//            Assert.AreEqual(0, avatarSkillCore.HandOfMidasRemainCD());
//            
//            // Unlock skill
//            asvg.ResetBalance(1);
//            Assert.IsTrue(avatarSkillCore.ActivateHandOfMidas());
//            Assert.Greater(avatarSkillCore.HandOfMidasRemainCD(), 0);
//            Assert.LessOrEqual(avatarSkillCore.HandOfMidasRemainCD(), 3600);
//            
//            // Unlock Artifact 008
//            avatarSkillCore.RefreshSkill(true);
//            Assert.AreEqual(0, avatarSkillCore.HandOfMidasRemainCD());
//            ArtifactAssets.Artifact009.ResetBalance(1);
//            Assert.AreEqual(3420, avatarSkillCore.HandOfMidasCooldown());
//            Assert.IsTrue(avatarSkillCore.ActivateHandOfMidas());
//            Assert.LessOrEqual(avatarSkillCore.HandOfMidasRemainCD(), 3420);
//            
//            // Lv 2
//            avatarSkillCore.RefreshSkill(true);
//            Assert.AreEqual(0, avatarSkillCore.HandOfMidasRemainCD());
//            ArtifactAssets.Artifact009.ResetBalance(2);
//            Assert.AreEqual(3240, avatarSkillCore.HandOfMidasCooldown());
//            Assert.IsTrue(avatarSkillCore.ActivateHandOfMidas());
//            Assert.LessOrEqual(avatarSkillCore.HandOfMidasRemainCD(), 3240);
//        }
//	}
//}