//using UnityEngine;
//using System;
//using NUnit.Framework;
//
//namespace Ignite.AOM {
//    
//    [TestFixture]
//    public class HeroCoreTest : AOMCoreTest {
//
//        private static readonly double HERO1_LV1_BASE_DMG = 4;
//        private static readonly double HERO1_LV10_BASE_DMG = 53;
//        private static readonly double HERO2_LV1_BASE_DMG = 16;
//        private static readonly double HERO2_LV10_BASE_DMG = 177;
//
//        private static readonly double HERO2_LV10_SKILL2_BUFF = 354;
//
//    	[Test]
//        public void TestBaseDPS() {
//            CharacterLevelVG hero1 = HeroAssets.Hero001;
//            Assert.AreEqual(0, heroCore.HeroDPS[0]);
//            hero1.ResetBalance(1);
//            Assert.AreEqual(HERO1_LV1_BASE_DMG, heroCore.HeroDPS[0]);
//            hero1.ResetBalance(2);
//            Assert.AreEqual(9, heroCore.HeroDPS[0]);
//            hero1.ResetBalance(3);
//            Assert.AreEqual(14, heroCore.HeroDPS[0]);
//            
//            CharacterLevelVG hero2 = HeroAssets.Hero002;
//            Assert.AreEqual(0, heroCore.HeroDPS[1]);
//            hero2.ResetBalance(1);
//            Assert.AreEqual(HERO2_LV1_BASE_DMG, heroCore.HeroDPS[1]);
//            hero2.ResetBalance(2);
//            Assert.AreEqual(32, heroCore.HeroDPS[1]);
//            hero2.ResetBalance(3);
//            Assert.AreEqual(49, heroCore.HeroDPS[1]);
//        }
//
//        [Test]
//        public void TestDPSWithHeroDamageBuff() {
//            CharacterLevelVG hero1 = HeroAssets.Hero001;
//            hero1.ResetBalance(10);
//            Assert.AreEqual(HERO1_LV10_BASE_DMG, heroCore.HeroDPS[0]);
//
//            HeroPassiveSkillVG h1s1 = HeroAssets.Hero001Skill001;
//            Assert.AreEqual(EffectType.HeroDamage, h1s1.EffectType);
//            h1s1.ResetBalance(1);
//            Assert.AreEqual(80, heroCore.HeroDPS[0]);
//            HeroPassiveSkillVG h1s2 = HeroAssets.Hero001Skill002;
//            Assert.AreEqual(EffectType.HeroDamage, h1s2.EffectType);
//            h1s2.ResetBalance(1);
//            Assert.AreEqual(134, heroCore.HeroDPS[0]);
//            
//            CharacterLevelVG hero2 = HeroAssets.Hero002;
//            hero2.ResetBalance(10);
//            Assert.AreEqual(HERO2_LV10_BASE_DMG, heroCore.HeroDPS[1]);
//            HeroPassiveSkillVG h2s2 = HeroAssets.Hero002Skill002;
//            Assert.AreEqual(EffectType.HeroDamage, h2s2.EffectType);
//            h2s2.ResetBalance(1);
//            Assert.AreEqual(HERO2_LV10_SKILL2_BUFF, heroCore.HeroDPS[1]);
//        }
//        
//        [Test]
//        public void TestDPSWithBossDamageBuff() {
//            
//            // Make sure current is stage 1 wave 1 
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//
//            CharacterLevelVG hero1 = HeroAssets.Hero001;
//            hero1.ResetBalance(10);
//            Assert.AreEqual(HERO1_LV10_BASE_DMG, heroCore.HeroDPS[0]);
//            
//            CharacterLevelVG hero2 = HeroAssets.Hero002;
//            hero2.ResetBalance(10);
//            Assert.AreEqual(HERO2_LV10_BASE_DMG, heroCore.HeroDPS[1]);
//            
//            for(int i=0; i<10; i++) {
//                enemyCore.NextEnemy(false);
//            }
//            
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(10, enemyCore.Wave);
//            Assert.AreEqual(EnemyType.MiniBoss, enemyCore.CurrentEnemyType);
//            // Hero DPS remain unchanged as not yet unlock hero boss damage buff
//            Assert.AreEqual(HERO1_LV10_BASE_DMG, heroCore.HeroDPS[0]); 
//            Assert.AreEqual(HERO2_LV10_BASE_DMG, heroCore.HeroDPS[1]);
//
//            HeroPassiveSkillVG hpsvg1 = HeroAssets.Hero007Skill002;
//            hpsvg1.ResetBalance(1);
//            Assert.AreEqual(EffectType.BossDamage, hpsvg1.EffectType);
//            Assert.Less(HERO1_LV10_BASE_DMG, heroCore.HeroDPS[0]); 
//            Assert.Less(HERO2_LV10_BASE_DMG, heroCore.HeroDPS[1]);
//        }
//        
//        [Test]
//        public void TestDPSWithHeroAllDamageBuff() {
//            CharacterLevelVG hero2 = HeroAssets.Hero002;
//            hero2.ResetBalance(10);
//            Assert.AreEqual(HERO2_LV10_BASE_DMG, heroCore.HeroDPS[1]);
//
//            HeroPassiveSkillVG s1 = HeroAssets.Hero016Skill006;
//            Assert.AreEqual(EffectType.AllDamage, s1.EffectType);
//            s1.ResetBalance(1);
//            Assert.AreEqual(203, heroCore.HeroDPS[1]);
//            
//            HeroPassiveSkillVG s2 = HeroAssets.Hero016Skill007;
//            Assert.AreEqual(EffectType.AllDamage, s2.EffectType);
//            s2.ResetBalance(1);
//            Assert.AreEqual(239, heroCore.HeroDPS[1]);
//            
//            HeroPassiveSkillVG s3 = HeroAssets.Hero002Skill005;
//            Assert.AreEqual(EffectType.AllDamage, s3.EffectType);
//            s3.ResetBalance(1);
//            Assert.AreEqual(256, heroCore.HeroDPS[1]);
//        }
//        
//        [Test]
//        public void TestDPSWithArtifactAllDamageBuff() {
//            CharacterLevelVG hero1 = HeroAssets.Hero001;
//            hero1.ResetBalance(10);
//            Assert.AreEqual(HERO1_LV10_BASE_DMG, heroCore.HeroDPS[0]);
//
//            CharacterLevelVG hero2 = HeroAssets.Hero002;
//            hero2.ResetBalance(10);
//            Assert.AreEqual(HERO2_LV10_BASE_DMG, heroCore.HeroDPS[1]);
//            
//            ArtifactVG vg = ArtifactAssets.Artifact001;
//            vg.ResetBalance(1);
//            Assert.AreEqual(85, heroCore.HeroDPS[0]);
//            Assert.AreEqual(283, heroCore.HeroDPS[1]);
//            Assert.AreEqual(heroCore.HeroDPS[0] + heroCore.HeroDPS[1], heroCore.TotalHeroDPS);
//            
//            vg.ResetBalance(2);
//            Assert.AreEqual(101, heroCore.HeroDPS[0]);
//            Assert.AreEqual(336, heroCore.HeroDPS[1]);
//            Assert.AreEqual(heroCore.HeroDPS[0] + heroCore.HeroDPS[1], heroCore.TotalHeroDPS);
//
//            vg = ArtifactAssets.Artifact002;
//            vg.ResetBalance(1);
//            Assert.AreEqual(128, heroCore.HeroDPS[0]);
//            Assert.AreEqual(425, heroCore.HeroDPS[1]);
//            Assert.AreEqual(heroCore.HeroDPS[0] + heroCore.HeroDPS[1], heroCore.TotalHeroDPS);
//        }
//        
//        [Test]
//        public void TestDPSWithAR026Buff() {
//            CharacterLevelVG hero1 = HeroAssets.Hero001;
//            hero1.ResetBalance(10);
//            Assert.AreEqual(HERO1_LV10_BASE_DMG, heroCore.HeroDPS[0]);
//            
//            CharacterLevelVG hero2 = HeroAssets.Hero002;
//            hero2.ResetBalance(10);
//            Assert.AreEqual(HERO2_LV10_BASE_DMG, heroCore.HeroDPS[1]);
//            
//            ArtifactVG vg = ArtifactAssets.Artifact026;
//            vg.ResetBalance(1);
//            Assert.AreEqual(61, heroCore.HeroDPS[0]);
//            Assert.AreEqual(203, heroCore.HeroDPS[1]);
//            Assert.AreEqual(heroCore.HeroDPS[0] + heroCore.HeroDPS[1], heroCore.TotalHeroDPS);
//            
//            vg.ResetBalance(2);
//            Assert.AreEqual(67, heroCore.HeroDPS[0]);
//            Assert.AreEqual(221, heroCore.HeroDPS[1]);
//            Assert.AreEqual(heroCore.HeroDPS[0] + heroCore.HeroDPS[1], heroCore.TotalHeroDPS);
//        }
//
//        [Test]
//        public void TestTotalDPS() {
//            Assert.AreEqual(0, heroCore.TotalHeroDPS);
//            
//            CharacterLevelVG hero1 = HeroAssets.Hero001;
//            hero1.ResetBalance(1);
//            Assert.AreEqual(HERO1_LV1_BASE_DMG, heroCore.TotalHeroDPS);
//            
//            CharacterLevelVG hero2 = HeroAssets.Hero002;
//            hero2.ResetBalance(1);
//            Assert.AreEqual(heroCore.HeroDPS[0] + heroCore.HeroDPS[1], heroCore.TotalHeroDPS);
//            Assert.AreEqual(HERO1_LV1_BASE_DMG + HERO2_LV1_BASE_DMG, heroCore.TotalHeroDPS);
//
//            hero2.ResetBalance(10);
//            Assert.AreEqual(heroCore.HeroDPS[0] + heroCore.HeroDPS[1], heroCore.TotalHeroDPS);
//            Assert.AreEqual(HERO1_LV1_BASE_DMG + HERO2_LV10_BASE_DMG, heroCore.TotalHeroDPS);
//            
//            HeroPassiveSkillVG h2s2 = HeroAssets.Hero002Skill002;
//            Assert.AreEqual(EffectType.HeroDamage, h2s2.EffectType);
//            h2s2.ResetBalance(1);
//            Assert.AreEqual(HERO2_LV10_SKILL2_BUFF, heroCore.HeroDPS[1]);
//            Assert.AreEqual(heroCore.HeroDPS[0] + heroCore.HeroDPS[1], heroCore.TotalHeroDPS);
//            Assert.AreEqual(HERO1_LV1_BASE_DMG + HERO2_LV10_SKILL2_BUFF, heroCore.TotalHeroDPS);
//        }
//
//    }
//}