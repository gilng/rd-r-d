using UnityEngine;
using System;
using Soomla.Store;
using NUnit.Framework;
using Zenject;

namespace Ignite.AOM {

    public abstract class AOMCoreTest
    {
        private AvatarCore _avatarCore;
        private AvatarSkillCore _avatarSkillCore;
        private HeroCore _heroCore;
        private EnemyCore _enemyCore;
        private GoldDropCore _goldDropCore;
        private ArtifactCore _artifactCore;

        [Inject]
        public AOMCoreTest(AvatarCore avatarCore, AvatarSkillCore avatarSkillCore, HeroCore heroCore, EnemyCore enemyCore, GoldDropCore goldDropCore, ArtifactCore artifactCore)
        {
            _avatarCore = avatarCore;
            _avatarSkillCore = avatarSkillCore;
            _heroCore = heroCore;
            _enemyCore = enemyCore;
            _goldDropCore = goldDropCore;
            _artifactCore = artifactCore;
        }

        //protected static AvatarCore avatarCore = _avatarCore;
        //protected static AvatarSkillCore avatarSkillCore = _avatarSkillCore;
        //protected static HeroCore heroCore = _heroCore;
        //protected static EnemyCore enemyCore = _enemyCore;
        //protected static ArtifactCore artifactCore = _artifactCore;
        //protected static GoldDropCore goldDropCore = _goldDropCore;
        
        [TestFixtureSetUp]
        public void InitAndResetSoomlaStore() {
            //            Soomla.KeyValueStorage.Purge();
            Soomla.CoreSettings.DebugUnityMessages = false;
            try {
                SoomlaStore.Initialize(new AOMAssets());
            } catch(Exception e) {
                Debug.LogError("Fail to init Soomla Store: " + e.Message);
            }
        }
        
        [SetUp]
        public void ResetAOMStore() {
            ResetSoomla();
        }
        
        [TestFixtureTearDown]
        public void ResetSoomlaStore() {
            ResetSoomla();
        }

        private void ResetSoomla() {
            // Reset Gold, Relics and Diamond
            AOMStoreInventory.ResetGold(0);
            AOMStoreInventory.ResetDiamond(0);
            AOMStoreInventory.ResetRelic(0);
            
            // Reset Avatar to Level 1
            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
            alvg.ResetBalance(1);
            
            // Reset All Avatar Skill
            _avatarCore.BerserkerRageEnabled = false;
            _avatarCore.CriticalStrikeASEnabled = false;
            
            foreach(AvatarActiveSkillVG asvg in AOMAssets.AvatarActiveSkillVGs) {
                asvg.ResetBalance(0, false);
            }
			_avatarSkillCore.RefreshSkill(true);
            
            // Reset All Hero Level and Skills
            foreach(CharacterLevelVG hero in HeroAssets.Heroes) {
                hero.ResetBalance(0, false);
            }
            foreach(HeroPassiveSkillVG heroSkill in HeroAssets.HeroSkills) {
                heroSkill.ResetBalance(0, false);
            }
            
            // Reset All Artifact
            foreach(ArtifactVG vg in ArtifactAssets.Artifacts) {
                //                Debug.Log("Check: " + (vg == ArtifactAssets.Artifacts[ArtifactAssets.Artifacts.Count - 1]));
                vg.ResetBalance(0, false);
            }
            _artifactCore.RecalAll();
            _heroCore.RecalAll();
            _avatarCore.RecalAll();

            // Reset Stage
            _enemyCore.ResetStage();
            _enemyCore.RecalAll();
        }
    }

}

