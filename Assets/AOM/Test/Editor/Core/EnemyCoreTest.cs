﻿//using UnityEngine;
//using System;
//using NUnit.Framework;
//
//namespace Ignite.AOM {
//    
//    [TestFixture]
//    public class EnemyCoreTest : AOMCoreTest {
//
//        [Test]
//        public void TestTreasureChestChanceWithAR018Buff() {
//
//            ArtifactVG ar018 = ArtifactAssets.Artifact018;
//            ar018.ResetBalance(240); // Suppose to be 100% Treasue Chest Chance when reach lv 240
//            Assert.AreEqual(1, 0.02 + (0.02 * (1 +ar018.EffectValue)));
//
//            enemyCore.RecalAll();
//
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//            
//            // Stage 1 to 10 wave 0 ~ 10
//            for(int i=0; i<(enemyCore.WavePerStage + 1) * 10; i++) {
//                Assert.AreNotEqual(EnemyType.TreasureChest, enemyCore.CurrentEnemyType, "Enemy Type should NOT be Treasue Chest before Stage 10. Current Stage: " + enemyCore.Stage + ", Wave: " + enemyCore.Wave);
//                enemyCore.NextEnemy(false);
//            }
//            
//            Assert.AreEqual(11, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//
//            // Stage 11 wave 0 ~ 10
//            for(int i=0; i<enemyCore.WavePerStage; i++) {
//                Assert.AreEqual(EnemyType.TreasureChest, enemyCore.CurrentEnemyType, "Enemy Type should be Treasue Ches after Stage 10. Current Stage: " + enemyCore.Stage + ", Wave: " + enemyCore.Wave);
//                enemyCore.NextEnemy(false);
//            }
//        }
//
//        [Test]
//        public void TestWavePerStageWithAR025Buff() {
//            
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//            
//            // Stage 1 wave 0 ~ 10
//            for(int i=0; i<enemyCore.WavePerStage; i++) {
//                enemyCore.NextEnemy(false);
//            }
//
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(10, enemyCore.Wave);
//            Assert.AreEqual(EnemyType.MiniBoss, enemyCore.CurrentEnemyType);
//
//            Debug.Log ("Unlock AR025");
//            ArtifactVG ar025 = ArtifactAssets.Artifact025;
//            ar025.ResetBalance(1);
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(10, enemyCore.Wave);
//            Assert.AreEqual(9 , enemyCore.WavePerStage);
//
//            enemyCore.NextEnemy(false);
//            
//            Assert.AreEqual(2, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            
//            // Stage 2 wave 0 ~ 8
//            for(int i=0; i<enemyCore.WavePerStage-1; i++) {
//                enemyCore.NextEnemy(false);
//            }
//            Assert.AreEqual(2, enemyCore.Stage);
//            Assert.AreEqual(8, enemyCore.Wave);
//            
//            ar025.ResetBalance(2);
//            Assert.AreEqual(8, enemyCore.WavePerStage);
//            Assert.AreEqual(2, enemyCore.Stage);
//            Assert.AreEqual(7, enemyCore.Wave);
//            Assert.AreNotEqual(EnemyType.MiniBoss, enemyCore.CurrentEnemyType);
//            
//            enemyCore.NextEnemy(false);
//            Assert.AreEqual(2, enemyCore.Stage);
//            Assert.AreEqual(EnemyType.MiniBoss, enemyCore.CurrentEnemyType);
//            
//            enemyCore.NextEnemy(false);
//            
//            Assert.AreEqual(3, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//
//            // Stage 3 wave 0 ~ 6
//            for(int i=0; i<6; i++) {
//                enemyCore.NextEnemy(false);
//            }
//
//            Assert.AreEqual(3, enemyCore.Stage);
//            Assert.AreEqual(6, enemyCore.Wave);
//            
//            ar025.ResetBalance(5);
//            Assert.AreEqual(5, enemyCore.WavePerStage);
//            Assert.AreEqual(3, enemyCore.Stage);
//            Assert.AreEqual(4, enemyCore.Wave);
//            Assert.AreNotEqual(EnemyType.MiniBoss, enemyCore.CurrentEnemyType);
//            
//            enemyCore.NextEnemy(false);
//            Assert.AreEqual(3, enemyCore.Stage);
//            Assert.AreEqual(EnemyType.MiniBoss, enemyCore.CurrentEnemyType);
//            
//            enemyCore.NextEnemy(false);
//            
//            Assert.AreEqual(4, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//        }
//    }
//}