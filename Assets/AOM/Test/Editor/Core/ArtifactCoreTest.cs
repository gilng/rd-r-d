﻿//using UnityEngine;
//using System.Collections.Generic;
//using Soomla.Store;
//using NUnit.Framework;
//
//namespace Ignite.AOM {
//    
//    [TestFixture]
//    public class ArtifactCoreTest : AOMCoreTest {
//
//    	[Test]
//        public void TestGacha() {
//
//            Assert.GreaterOrEqual(ArtifactAssets.Artifacts.Count, 4);
//
//            Assert.AreEqual(0, AOMStoreInventory.GetRelicBalance());
//            Assert.AreEqual(1, artifactCore.GachaCost());
//            Assert.IsFalse(artifactCore.CanAffordGacha());
//            Assert.IsNull(artifactCore.Gacha());
//            Assert.AreEqual(1, artifactCore.GachaCost());
//
//            AOMStoreInventory.ResetRelic(1);
//            Assert.IsTrue(artifactCore.CanAffordGacha());
//            ArtifactVG vg = artifactCore.Gacha();
//            Assert.IsNotNull(vg);
//            Assert.AreEqual(1, vg.GetBalance());
//            Assert.AreEqual(0, AOMStoreInventory.GetRelicBalance());
//            
//            AOMStoreInventory.ResetRelic(100000);
//            Assert.AreEqual(3, artifactCore.GachaCost());
//            vg = artifactCore.Gacha();
//            Assert.IsNotNull(vg);
//            Assert.AreEqual(1, vg.GetBalance());
//
//            Assert.AreEqual(7, artifactCore.GachaCost());
//            vg = artifactCore.Gacha();
//            Assert.IsNotNull(vg);
//            Assert.AreEqual(1, vg.GetBalance());
//            
//            Assert.AreEqual(13, artifactCore.GachaCost());
//        }
//        
//        [Test]
//        public void TestGachaAll() {
//
//            foreach(ArtifactVG vg in ArtifactAssets.Artifacts) {
//                // Assume player has not owned any artifacts
//                Assert.AreEqual(0, vg.GetBalance());
//            }
//
//            IList<ArtifactVG> ownedArtifacts = new List<ArtifactVG>();
//            for(int i=0; i < ArtifactAssets.Artifacts.Count; i++) {
//                AOMStoreInventory.ResetRelic(artifactCore.GachaCost());
//                ArtifactVG vg = artifactCore.Gacha();
//                Assert.IsNotNull(vg);
//                ownedArtifacts.Add(vg);
//            }
//            
//            foreach(ArtifactVG vg in ArtifactAssets.Artifacts) {
//                // Assume player has not owned any artifacts
//                Assert.AreEqual(1, vg.GetBalance());
//            }
//        }
//    }
//}
