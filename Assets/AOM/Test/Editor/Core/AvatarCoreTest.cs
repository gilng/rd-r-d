//using UnityEngine;
//using System;
//using NUnit.Framework;
//
//namespace Ignite.AOM {
//    
//    [TestFixture]
//    public class AvatarCoreTest : AOMCoreTest
//    {
//        private static readonly double LV1_BASE_DMG =1;
//        private static readonly double LV12_BASE_DMG = 22;
//        private static readonly double LV112_BASE_DMG = 26450;
//        private static readonly double LV112_12PERC_BUFF_BOSS_DMG = 29624;
//        private static readonly double LV400_BASE_DMG = 119613340500;
//
//        [Test]
//        public void TestSimpleTapDamage() {
//
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            alvg.ResetBalance(1);
//            AOMStoreInventory.ResetGold(10000000);
//            
//            Assert.AreEqual(1, alvg.GetBalance());
//            Assert.AreEqual(LV1_BASE_DMG, avatarCore.TapDamage);
//
//            alvg.BuyOne();
//            Assert.AreEqual(2, alvg.GetBalance());
//            Assert.AreEqual(2, avatarCore.TapDamage);
//            
//            alvg.BuyTen();
//            Assert.AreEqual(12, alvg.GetBalance());
//            Assert.AreEqual(LV12_BASE_DMG, avatarCore.TapDamage);
//            
//            alvg.BuyHundred();
//            Assert.AreEqual(112, alvg.GetBalance());
//            Assert.AreEqual(LV112_BASE_DMG, avatarCore.TapDamage);
//        }
//        
//        [Test]
//        public void TestTapDamageToBoss() {
//
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//
//            alvg.ResetBalance(112);
//            Assert.AreEqual(LV112_BASE_DMG, avatarCore.TapDamage);
//
//            for(int i=0; i<10; i++) {
//                enemyCore.NextEnemy(false);
//            }
//
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(10, enemyCore.Wave);
//            Assert.AreEqual(EnemyType.MiniBoss, enemyCore.CurrentEnemyType);
//            Assert.AreEqual(LV112_BASE_DMG, avatarCore.TapDamage); // As not yet unlock hero boss damage buff
//
//            HeroPassiveSkillVG hpsvg1 = HeroAssets.Hero007Skill002;
//            hpsvg1.ResetBalance(1);
//            Assert.AreEqual(EffectType.BossDamage, hpsvg1.EffectType);
//            Assert.AreEqual(27772, avatarCore.TapDamage);
//            
//            HeroPassiveSkillVG hpsvg2 = HeroAssets.Hero007Skill003;
//            Assert.AreEqual(EffectType.BossDamage, hpsvg2.EffectType);
//            hpsvg2.ResetBalance(1);
//            Assert.AreEqual(LV112_12PERC_BUFF_BOSS_DMG, avatarCore.TapDamage);
//
//            enemyCore.NextEnemy(false);
//            Assert.AreEqual(2, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(EnemyType.Normal, enemyCore.CurrentEnemyType);
//            Assert.AreEqual(LV112_BASE_DMG, avatarCore.TapDamage);
//            
//            for(int i=0; i<43; i++) {
//                enemyCore.NextEnemy(false);
//            }
//
//            Assert.AreEqual(5, enemyCore.Stage);
//            Assert.AreEqual(10, enemyCore.Wave);
//            Assert.AreEqual(EnemyType.BigBoss, enemyCore.CurrentEnemyType);
//            Assert.AreEqual(LV112_12PERC_BUFF_BOSS_DMG, avatarCore.TapDamage);
//
//            // Infinity Loop Case
//            enemyCore.NextEnemy(true);
//            Assert.AreEqual(5, enemyCore.Stage);
//            Assert.AreEqual(10, enemyCore.Wave);
//            Assert.AreNotEqual(EnemyType.BigBoss, enemyCore.CurrentEnemyType);
//            Assert.AreNotEqual(EnemyType.MiniBoss, enemyCore.CurrentEnemyType);
//            Assert.AreEqual(LV112_BASE_DMG, avatarCore.TapDamage);
//            
//            // Infinity Loop Case
//            enemyCore.NextEnemy(false);
//            Assert.AreEqual(5, enemyCore.Stage);
//            Assert.AreEqual(10, enemyCore.Wave);
//            Assert.AreEqual(EnemyType.BigBoss, enemyCore.CurrentEnemyType);
//            Assert.AreEqual(LV112_12PERC_BUFF_BOSS_DMG, avatarCore.TapDamage);
//        }
//
//        [Test]
//        public void TestCriticalDamage() {
//            
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            
//            alvg.ResetBalance(112);
//            Assert.AreEqual(LV112_BASE_DMG, avatarCore.TapDamage);
//
//            Assert.GreaterOrEqual(avatarCore.CriticalDamage, Math.Round (avatarCore.TapDamage * 3, MidpointRounding.AwayFromZero));
//            Assert.LessOrEqual(avatarCore.CriticalDamage, Math.Round (avatarCore.TapDamage * 10, MidpointRounding.AwayFromZero));
//        }
//        
//        [Test]
//        public void TestCriticalDamageWithHeroCriticalDamageBuff() {
//            
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            
//            alvg.ResetBalance(112);
//            Assert.AreEqual(LV112_BASE_DMG, avatarCore.TapDamage);
//
//            HeroPassiveSkillVG h4s5 = HeroAssets.Hero004Skill005;
//            Assert.AreEqual(EffectType.CriticalDamage, h4s5.EffectType);
//            h4s5.ResetBalance(1);
//            Assert.GreaterOrEqual(avatarCore.CriticalDamage, Math.Round (avatarCore.TapDamage * 3 * (1+h4s5.EffectValue), MidpointRounding.AwayFromZero));
//            Assert.LessOrEqual(avatarCore.CriticalDamage, Math.Round (avatarCore.TapDamage * 10 * (1+h4s5.EffectValue), MidpointRounding.AwayFromZero));
//
//            HeroPassiveSkillVG h6s5 = HeroAssets.Hero006Skill005;
//            Assert.AreEqual(EffectType.CriticalDamage, h6s5.EffectType);
//            h6s5.ResetBalance(1);
//            Assert.GreaterOrEqual(avatarCore.CriticalDamage, Math.Round (avatarCore.TapDamage * 3 * (1 + h4s5.EffectValue + h6s5.EffectValue), MidpointRounding.AwayFromZero));
//            Assert.LessOrEqual(avatarCore.CriticalDamage, Math.Round (avatarCore.TapDamage * 10 * (1 + h4s5.EffectValue + h6s5.EffectValue), MidpointRounding.AwayFromZero));
//        }
//        
//        [Test]
//        public void TestCriticalDamageWithAR017Buff() {
//            
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            
//            alvg.ResetBalance(112);
//            Assert.AreEqual(LV112_BASE_DMG, avatarCore.TapDamage);
//
//            ArtifactVG ar017 = ArtifactAssets.Artifact017;
//            ar017.ResetBalance(1);
//            Assert.Greater(avatarCore.CriticalDamage, avatarCore.TapDamage);
//            Assert.GreaterOrEqual(avatarCore.CriticalDamage, Math.Round (avatarCore.TapDamage * 3 * (1+ar017.EffectValue), MidpointRounding.AwayFromZero));
//            Assert.LessOrEqual(avatarCore.CriticalDamage, Math.Round (avatarCore.TapDamage * 10 * (1+ar017.EffectValue), MidpointRounding.AwayFromZero));
//
//            ar017.ResetBalance(2);
//            Assert.Greater(avatarCore.CriticalDamage, avatarCore.TapDamage);
//            Assert.GreaterOrEqual(avatarCore.CriticalDamage, Math.Round (avatarCore.TapDamage * 3 * (1+ar017.EffectValue), MidpointRounding.AwayFromZero));
//            Assert.LessOrEqual(avatarCore.CriticalDamage, Math.Round (avatarCore.TapDamage * 10 * (1+ar017.EffectValue), MidpointRounding.AwayFromZero));
//        }
//
//        [Test]
//        public void TestTapDamageWithBerserkerRage() {
//            
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            AvatarActiveSkillVG brasvg = AOMAssets.BerserkerRageASVG;
//
//            alvg.ResetBalance(400);
//            brasvg.ResetBalance(1);
//
//            Assert.AreEqual(LV400_BASE_DMG, avatarCore.TapDamage);
//
//            /// Enable Berserker Rage Skill
//            avatarCore.BerserkerRageEnabled = true;
//            Assert.AreEqual(203342678849, avatarCore.TapDamage);
//
//            // Raise Berserker Rage Level
//            AOMStoreInventory.ResetGold(brasvg.PriceForOne);
//            brasvg.BuyOne();
//            Assert.AreEqual(239226680999, avatarCore.TapDamage);
//
//            /// Disable Berserker Rage Skill
//            avatarCore.BerserkerRageEnabled = false;
//            Assert.AreEqual(LV400_BASE_DMG, avatarCore.TapDamage);
//        }
//
//        [Test]
//        public void TestTapDamageWithHeroTapDamageBuff() {
//
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//
//            CharacterLevelVG hero002 = HeroAssets.Hero002;
//            HeroPassiveSkillVG h2s1 = HeroAssets.Hero002Skill001;
//            Assert.AreEqual(EffectType.TapDamage, h2s1.EffectType);
//            HeroPassiveSkillVG h5s6 = HeroAssets.Hero005Skill006;
//            Assert.AreEqual(EffectType.TapDamage, h5s6.EffectType);
//            
//            alvg.ResetBalance(12);
//            Assert.AreEqual(LV12_BASE_DMG, avatarCore.TapDamage);
//            
//            // Unlock Skill
//            Assert.AreEqual(LV12_BASE_DMG, avatarCore.TapDamage);
//
//            hero002.ResetBalance(10);
//            h2s1.ResetBalance(1);
//            Assert.AreEqual(23, avatarCore.TapDamage);
//
//        }
//        
//        [Test]
//        public void TestTapDamageWithAR029Buff() {
//            
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            alvg.ResetBalance(12);
//            Assert.AreEqual(LV12_BASE_DMG, avatarCore.TapDamage);
//            
//            ArtifactVG ar029 = ArtifactAssets.Artifact029;
//            ar029.ResetBalance(1);
//            Assert.AreEqual(35, avatarCore.TapDamage);
//            ar029.ResetBalance(2);
//            Assert.AreEqual(43, avatarCore.TapDamage);
//            ar029.ResetBalance(5);
//            Assert.AreEqual(66, avatarCore.TapDamage);
//            ar029.ResetBalance(100);
//            Assert.AreEqual(2024, avatarCore.TapDamage);
//        }
//
//        [Test]
//        public void TestTapDamageWithHeroAllDamageBuff() {
//            
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//
//            
//            alvg.ResetBalance(12);
//            Assert.AreEqual(LV12_BASE_DMG, avatarCore.TapDamage);
//            
//            // Unlock Skill
//            CharacterLevelVG hero001 = HeroAssets.Hero001;
//            HeroPassiveSkillVG h1s3 = HeroAssets.Hero001Skill003;
//            Assert.AreEqual(EffectType.AllDamage, h1s3.EffectType);
//            
//            hero001.ResetBalance(10);
//            h1s3.ResetBalance(1);
//            Assert.AreEqual(24, avatarCore.TapDamage);
//
//            // Unlock more all damage passive skill
//            HeroPassiveSkillVG h1s6 = HeroAssets.Hero001Skill006;
//            Assert.AreEqual(EffectType.AllDamage, h1s6.EffectType);
//            h1s6.ResetBalance(1);
//            Assert.AreEqual(29, avatarCore.TapDamage);
//
//        }
//        
//        [Test]
//        public void TestTapDamageWithArtifactAllDamageBuff() {
//            
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//
//            alvg.ResetBalance(12);
//            Assert.AreEqual(LV12_BASE_DMG, avatarCore.TapDamage);
//            
//            // Unlock Artifacts
//            ArtifactVG ar001 = ArtifactAssets.Artifact001;
//            ar001.ResetBalance(1);
//            Assert.AreEqual(34, avatarCore.TapDamage);
//            ar001.ResetBalance(2);
//            Assert.AreEqual(41, avatarCore.TapDamage);
//
//            ArtifactVG ar003 = ArtifactAssets.Artifact003;
//            ar003.ResetBalance(1);
//            Assert.AreEqual(47, avatarCore.TapDamage);
//            ar003.ResetBalance(2);
//            Assert.AreEqual(51, avatarCore.TapDamage);
//
//        }
//        
//        [Test]
//        public void TestHeavenlyStrikeDamage() {
//            
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            alvg.ResetBalance(12);
//            Assert.AreEqual(LV12_BASE_DMG, avatarCore.TapDamage);
//
//            AvatarActiveSkillVG hsvg = AOMAssets.HeavenlyStrikeASVG;
//            hsvg.ResetBalance(1);
//            Assert.AreEqual(140, hsvg.EffectValue);
//            Assert.AreEqual(3080, avatarCore.HeavenlyStrikeDamage);
//            
//            hsvg.ResetBalance(2);
//            Assert.AreEqual(210, hsvg.EffectValue);
//            Assert.AreEqual(4620, avatarCore.HeavenlyStrikeDamage);
//        }
//        
//        [Test]
//        public void TestCriticalChance() {
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            alvg.ResetBalance(1);
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE, avatarCore.CriticalChance); 
//            alvg.ResetBalance(10);
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE, avatarCore.CriticalChance); 
//            
//            AvatarActiveSkillVG csvg = AOMAssets.CriticalStrikeASVG;
//            csvg.ResetBalance(1);
//
//            // Avatar Skill unlocked but not trigger yet
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE, avatarCore.CriticalChance); 
//
//            // Trigger the CS Active Skill now
//            avatarCore.CriticalStrikeASEnabled = true;
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE+0.17, avatarCore.CriticalChance); 
//            csvg.ResetBalance(2);
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE+0.2, avatarCore.CriticalChance); 
//
//            // With Hero CS Chance Buff
//            HeroPassiveSkillVG s1 = HeroAssets.Hero003Skill006;
//            Assert.AreEqual(EffectType.CriticalChance, s1.EffectType);
//            s1.ResetBalance(1);
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE + 0.21, avatarCore.CriticalChance); 
//
//            // Disable CS Active Skill
//            avatarCore.CriticalStrikeASEnabled = false;
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE + 0.01, avatarCore.CriticalChance); 
//
//            // Add another Hero CS Chance Buff
//            HeroPassiveSkillVG s2 = HeroAssets.Hero015Skill004;
//            Assert.AreEqual(EffectType.CriticalChance, s2.EffectType);
//            s2.ResetBalance(1);
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE + 0.03, avatarCore.CriticalChance); 
//            
//            ArtifactVG ar004 = ArtifactAssets.Artifact004;
//            ar004.ResetBalance(1);
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE + 0.05, avatarCore.CriticalChance, 0.01); 
//        }
//        
//        [Test]
//        public void TestCriticalChanceWithAR004Buff() {
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            alvg.ResetBalance(1);
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE, avatarCore.CriticalChance); 
//            alvg.ResetBalance(10);
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE, avatarCore.CriticalChance); 
//
//            ArtifactVG ar004 = ArtifactAssets.Artifact004;
//            ar004.ResetBalance(1);
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE + 0.02, avatarCore.CriticalChance); 
//
//            ar004.ResetBalance(3);
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE + 0.06, avatarCore.CriticalChance); 
//            
//            ar004.ResetBalance(25);
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE + 0.5, avatarCore.CriticalChance); 
//
//            ar004.ResetBalance(26); // AR004 max'ed at lv 25
//            Assert.AreEqual(AvatarCore.BASE_CRITICAL_CHANCE + 0.5, avatarCore.CriticalChance); 
//        }
//
//        [Test]
//        public void TestTapDamageWithHeroTotalDPSDamageBuff() {
//            
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//
//            alvg.ResetBalance(12);
//            Assert.AreEqual(LV12_BASE_DMG, avatarCore.TapDamage);
//            
//            // Unlock Hero 1 and Hero 2
//            CharacterLevelVG hero1 = HeroAssets.Hero001;
//            hero1.ResetBalance(100);
//
//            CharacterLevelVG hero002 = HeroAssets.Hero002;
//            hero002.ResetBalance(100);
//
//            Assert.AreEqual(LV12_BASE_DMG, avatarCore.TapDamage);
//            
//            // Unlock Hero002Skill004
//            HeroPassiveSkillVG h2s4 = HeroAssets.Hero002Skill004;
//            Assert.AreEqual(EffectType.TotalDPSTapDamage, h2s4.EffectType);
//            h2s4.ResetBalance(1);
//            double prevTotalDPS = heroCore.TotalHeroDPS;
//            Assert.AreEqual(Math.Round (avatarCore.BaseDamage + heroCore.TotalHeroDPS * h2s4.EffectValue), avatarCore.TapDamage);
//
//            hero1.ResetBalance(500);
//            Assert.AreNotEqual(prevTotalDPS, heroCore.TotalHeroDPS);
//            Assert.AreEqual(Math.Round(avatarCore.BaseDamage + heroCore.TotalHeroDPS * h2s4.EffectValue), avatarCore.TapDamage);
//
//            // TODO Add more and better test cases
//            
//        }
//    }
//}