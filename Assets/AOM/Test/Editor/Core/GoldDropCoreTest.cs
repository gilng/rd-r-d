﻿//using UnityEngine;
//using System;
//using NUnit.Framework;
//
//namespace Ignite.AOM {
//
//    [TestFixture]
//    public class GoldDropCoreTest : AOMCoreTest {
//
//        private static readonly double STAGE22_NORMAL_GOLD = 11289;
//        private static readonly double STAGE22_TREASURE_CHEST_GOLD = STAGE22_NORMAL_GOLD * 10;
//        private static readonly double STAGE22_MINI_BOSS_GOLD = 45156;
//
//        private static readonly double STAGE23_NORMAL_GOLD = 17990;
//        private static readonly double STAGE23_TREASURE_CHEST_GOLD = STAGE23_NORMAL_GOLD * 10;
//        private static readonly double STAGE23_MINI_BOSS_GOLD = 107940;
//        
//        private static readonly double STAGE24_NORMAL_GOLD = 28663;
//        private static readonly double STAGE24_TREASURE_CHEST_GOLD = STAGE24_NORMAL_GOLD * 10;
//        private static readonly double STAGE24_MINI_BOSS_GOLD = 229304;
//        
//        private static readonly double STAGE25_NORMAL_GOLD = 45658;
//        private static readonly double STAGE25_TREASURE_CHEST_GOLD = STAGE25_NORMAL_GOLD * 10;
//        private static readonly double STAGE25_BIG_BOSS_GOLD = 456580;
//        
//        private static readonly double STAGE26_NORMAL_GOLD = 72715;
//        private static readonly double STAGE26_TREASURE_CHEST_GOLD = STAGE26_NORMAL_GOLD * 10;
//        private static readonly double STAGE26_MINI_BOSS_GOLD = 145430;
//          
//        [Test]
//        public void TestOrdinaryGoldDrop() {
//            
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//
//            // Stage 1 wave 0 ~ 10
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                    case EnemyType.Normal:
//                        expectedGold = 1;
//                        break;
//                    case EnemyType.TreasureChest:
//                        expectedGold = 10;
//                        break;
//                    case EnemyType.MiniBoss:
//                        expectedGold = 2;
//                        break;
//                }
//                
//                Assert.AreEqual(1, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop());
//
//                enemyCore.NextEnemy(false);
//            }
//
//            // Stage 2 wave 0 ~ 10
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = 1;
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = 10;
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = 4;
//                    break;
//                }
//                
//                Assert.AreEqual(2, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop());
//                
//                enemyCore.NextEnemy(false);
//            }
//            
//            // Stage 3 wave 0 ~ 10
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = 2;
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = 20;
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = 12;
//                    break;
//                }
//                
//                Assert.AreEqual(3, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop());
//                
//                enemyCore.NextEnemy(false);
//            }
//
//            for(int i=0; i< 18 * (enemyCore.WavePerStage+1); i++) {
//                enemyCore.NextEnemy(false);
//            }
//            
//            // Stage 22
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE22_NORMAL_GOLD;
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE22_TREASURE_CHEST_GOLD;
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE22_MINI_BOSS_GOLD;
//                    break;
//                }
//                
//                Assert.AreEqual(22, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop());
//                
//                enemyCore.NextEnemy(false);
//            }
//        }
//        
//        [Test]
//        public void TestGoldDropWithHeroSkillGoldDropBuff() {
//
//            HeroAssets.Hero002Skill006.ResetBalance(1);
//            HeroAssets.Hero003Skill002.ResetBalance(1);
//
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//
//            for(int i=0; i< 21 * (enemyCore.WavePerStage+1); i++) {
//                enemyCore.NextEnemy(false);
//            }
//
//            // Stage 22
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE22_NORMAL_GOLD * 2; // As round up
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE22_TREASURE_CHEST_GOLD * 2; // As round up
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE22_MINI_BOSS_GOLD * 2; // As round up
//                    break;
//                }
//                
//                Assert.AreEqual(22, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop());
//                
//                enemyCore.NextEnemy(false);
//            }
//
//            // Unlock more hero gold drop skill
//            HeroAssets.Hero004Skill003.ResetBalance(1);
//            HeroAssets.Hero004Skill005.ResetBalance(1);
//            HeroAssets.Hero005Skill002.ResetBalance(1);
//            HeroAssets.Hero005Skill004.ResetBalance(1);
//            HeroAssets.Hero010Skill004.ResetBalance(1);
//            HeroAssets.Hero011Skill005.ResetBalance(1);
//            HeroAssets.Hero012Skill007.ResetBalance(1);
//
//            double buffVal = heroCore.EffectValue[EffectType.GoldDropAmount];
//            Assert.Greater(buffVal, 1);
//            Assert.Less(buffVal, 2);
//
//            // Stage 23
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE23_NORMAL_GOLD * 3; // As round up
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE23_TREASURE_CHEST_GOLD * 3; // As round up
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE23_MINI_BOSS_GOLD * 3; // As round up
//                    break;
//                }
//                
//                Assert.AreEqual(23, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop());
//                
//                enemyCore.NextEnemy(false);
//            }
//        }
//        
//        [Test]
//        public void TestGoldDropWithAR001Buff() {
//
//            
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//            
//            for(int i=0; i< 21 * (enemyCore.WavePerStage+1); i++) {
//                enemyCore.NextEnemy(false);
//            }
//
//            // Stage 22
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE22_NORMAL_GOLD; // As round up
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE22_TREASURE_CHEST_GOLD; // As round up
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE22_MINI_BOSS_GOLD; // As round up
//                    break;
//                }
//                
//                Assert.AreEqual(22, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop());
//                
//                enemyCore.NextEnemy(false);
//            }
//
//            // Unlock AR001
//            ArtifactAssets.Artifact001.ResetBalance(1);
//            
//            // Stage 23
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE23_NORMAL_GOLD; // As AR001 affect boss gold only
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE23_TREASURE_CHEST_GOLD; // As AR001 affect boss gold only
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE23_MINI_BOSS_GOLD * 2; // As round up
//                    break;
//                }
//                
//                Assert.AreEqual(23, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 23, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//            
//            ArtifactAssets.Artifact001.ResetBalance(2);
//            
//            // Stage 24
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE24_NORMAL_GOLD; // As AR001 affect boss gold only
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE24_TREASURE_CHEST_GOLD; // As AR001 affect boss gold only
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE24_MINI_BOSS_GOLD * 3; 
//                    break;
//                }
//                
//                Assert.AreEqual(24, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 24, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//            
//            ArtifactAssets.Artifact001.ResetBalance(3);
//            
//            // Stage 25
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE25_NORMAL_GOLD; // As AR001 affect boss gold only
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE25_TREASURE_CHEST_GOLD; // As AR001 affect boss gold only
//                    break;
//                case EnemyType.BigBoss:
//                    expectedGold = STAGE25_BIG_BOSS_GOLD * 4; 
//                    break;
//                }
//                
//                Assert.AreEqual(25, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 25, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//        }
//        
//        [Test]
//        public void TestGoldDropWithAR002Buff() {
//
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//            
//            for(int i=0; i< 21 * (enemyCore.WavePerStage+1); i++) {
//                enemyCore.NextEnemy(false);
//            }
//            
//            // Stage 22
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE22_NORMAL_GOLD;
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE22_TREASURE_CHEST_GOLD; // As AR002 affect normal enemy gold only
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE22_MINI_BOSS_GOLD; // As AR002 affect normal enemy gold only
//                    break;
//                }
//                
//                Assert.AreEqual(22, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop());
//                
//                enemyCore.NextEnemy(false);
//            }
//            
//            // Unlock AR002
//            ArtifactAssets.Artifact002.ResetBalance(1);
//            
//            // Stage 23
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE23_NORMAL_GOLD * 1.1;
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE23_TREASURE_CHEST_GOLD; // As AR002 affect normal enemy gold only
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE23_MINI_BOSS_GOLD; // As AR002 affect normal enemy gold only
//                    break;
//                }
//                
//                Assert.AreEqual(23, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 23, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//            
//            ArtifactAssets.Artifact002.ResetBalance(2);
//            
//            // Stage 24
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE24_NORMAL_GOLD * 1.2; 
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE24_TREASURE_CHEST_GOLD; // As AR002 affect normal enemy gold only
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE24_MINI_BOSS_GOLD; // As AR002 affect normal enemy gold only 
//                    break;
//                }
//                
//                Assert.AreEqual(24, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 24, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//            
//            ArtifactAssets.Artifact002.ResetBalance(3);
//            
//            // Stage 25
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE25_NORMAL_GOLD * 1.3; 
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE25_TREASURE_CHEST_GOLD; // As AR002 affect normal enemy gold only
//                    break;
//                case EnemyType.BigBoss:
//                    expectedGold = STAGE25_BIG_BOSS_GOLD;  // As AR002 affect normal enemy gold only
//                    break;
//                }
//                
//                Assert.AreEqual(25, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 25, Wave " + i);
//
//                if(i == enemyCore.WavePerStage) {
//                    // Make use of AR018 to get 100% chance Treasue Box Enemy
//                    ArtifactAssets.Artifact018.ResetBalance(240); // Suppose to be 100% Treasue Chest Chance when reach lv 240
//                }
//                enemyCore.NextEnemy(false);
//            }
//
//            // Stage 26
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                    case EnemyType.TreasureChest:
//                        expectedGold = STAGE26_TREASURE_CHEST_GOLD; // As AR002 affect normal enemy gold only
//                        break;
//                    case EnemyType.MiniBoss:
//                    expectedGold = STAGE26_MINI_BOSS_GOLD;  // As AR002 affect normal enemy gold only
//                        break;
//                }
//                
//                Assert.AreEqual(26, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreNotEqual(EnemyType.Normal, enemyCore.CurrentEnemyType, "Enemy Type should not be normal when ar018 is of lv 240. Wave: " + i);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 26, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//        }
//
//        [Test]
//        public void TestGoldDropWithAR019Buff() {
//            
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//            
//            for(int i=0; i< 22 * (enemyCore.WavePerStage+1); i++) {
//                enemyCore.NextEnemy(false);
//            }
//            
//            // Unlock AR002
//            ArtifactAssets.Artifact019.ResetBalance(1);
//            
//            // Stage 23
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE23_NORMAL_GOLD;
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE23_TREASURE_CHEST_GOLD * 1.2; 
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE23_MINI_BOSS_GOLD; 
//                    break;
//                }
//                
//                Assert.AreEqual(23, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 23, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//            
//            ArtifactAssets.Artifact019.ResetBalance(2);
//            
//            // Stage 24
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE24_NORMAL_GOLD; 
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE24_TREASURE_CHEST_GOLD * 1.4; 
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE24_MINI_BOSS_GOLD;
//                    break;
//                }
//                
//                Assert.AreEqual(24, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 24, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//            
//            ArtifactAssets.Artifact019.ResetBalance(3);
//            
//            // Stage 25
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE25_NORMAL_GOLD; 
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE25_TREASURE_CHEST_GOLD * 1.6; 
//                    break;
//                case EnemyType.BigBoss:
//                    expectedGold = STAGE25_BIG_BOSS_GOLD;
//                    break;
//                }
//                
//                Assert.AreEqual(25, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 25, Wave " + i);
//                
//                if(i == enemyCore.WavePerStage) {
//                    // Make use of AR018 to get 100% chance Treasue Box Enemy
//                    ArtifactAssets.Artifact018.ResetBalance(240); // Suppose to be 100% Treasue Chest Chance when reach lv 240
//                }
//                enemyCore.NextEnemy(false);
//            }
//
//            ArtifactAssets.Artifact019.ResetBalance(4);
//
//            // Stage 26
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE26_TREASURE_CHEST_GOLD * 1.8; // As AR002 affect normal enemy gold only
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE26_MINI_BOSS_GOLD;  // As AR002 affect normal enemy gold only
//                    break;
//                }
//                
//                Assert.AreEqual(26, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreNotEqual(EnemyType.Normal, enemyCore.CurrentEnemyType, "Enemy Type should not be normal when ar018 is of lv 240. Wave: " + i);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 26, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//        }
//        
//        [Test]
//        public void TestGoldDropWithAR020Buff() {
//            
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//            
//            for(int i=0; i< 22 * (enemyCore.WavePerStage+1); i++) {
//                enemyCore.NextEnemy(false);
//            }
//            
//            // Unlock AR002
//            ArtifactAssets.Artifact020.ResetBalance(1);
//            
//            // Stage 23
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE23_NORMAL_GOLD * 2;
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE23_TREASURE_CHEST_GOLD * 2; 
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE23_MINI_BOSS_GOLD * 2; 
//                    break;
//                }
//                
//                Assert.AreEqual(23, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 23, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//            
//            ArtifactAssets.Artifact020.ResetBalance(2);
//            
//            // Stage 24
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE24_NORMAL_GOLD * 2; 
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE24_TREASURE_CHEST_GOLD * 2; 
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE24_MINI_BOSS_GOLD * 2;
//                    break;
//                }
//                
//                Assert.AreEqual(24, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 24, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//            
//            ArtifactAssets.Artifact020.ResetBalance(21);
//            
//            // Stage 25
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE25_NORMAL_GOLD * 3; 
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE25_TREASURE_CHEST_GOLD * 3; 
//                    break;
//                case EnemyType.BigBoss:
//                    expectedGold = STAGE25_BIG_BOSS_GOLD * 3;
//                    break;
//                }
//                
//                Assert.AreEqual(25, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 25, Wave " + i);
//                
//                if(i == enemyCore.WavePerStage) {
//                    // Make use of AR018 to get 100% chance Treasue Box Enemy
//                    ArtifactAssets.Artifact018.ResetBalance(240); // Suppose to be 100% Treasue Chest Chance when reach lv 240
//                }
//                enemyCore.NextEnemy(false);
//            }
//
//            // Stage 26
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE26_TREASURE_CHEST_GOLD * 3; // As AR002 affect normal enemy gold only
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE26_MINI_BOSS_GOLD * 3;  // As AR002 affect normal enemy gold only
//                    break;
//                }
//                
//                Assert.AreEqual(26, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreNotEqual(EnemyType.Normal, enemyCore.CurrentEnemyType, "Enemy Type should not be normal when ar018 is of lv 240. Wave: " + i);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 26, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//        }
//        
//        
//        [Test]
//        public void TestGoldDropWithAR021Buff() {
//            
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//            
//            for(int i=0; i< 22 * (enemyCore.WavePerStage+1); i++) {
//                enemyCore.NextEnemy(false);
//            }
//            
//            // Unlock AR002
//            ArtifactAssets.Artifact021.ResetBalance(200); // i.e. 100% chance 10x gold
//            
//            // Stage 23
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE23_NORMAL_GOLD * 10;
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE23_TREASURE_CHEST_GOLD * 10; 
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE23_MINI_BOSS_GOLD; 
//                    break;
//                }
//                
//                Assert.AreEqual(23, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 23, Wave " + i);
//                
//                if(i == enemyCore.WavePerStage) {
//                    // Make use of AR018 to get 100% chance Treasue Box Enemy
//                    ArtifactAssets.Artifact018.ResetBalance(240); // Suppose to be 100% Treasue Chest Chance when reach lv 240
//                }
//                
//                enemyCore.NextEnemy(false);
//            }
//
//            // Stage 24
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE24_TREASURE_CHEST_GOLD * 10; // As AR002 affect normal enemy gold only
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE24_MINI_BOSS_GOLD;  // As AR002 affect normal enemy gold only
//                    break;
//                }
//                
//                Assert.AreEqual(24, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreNotEqual(EnemyType.Normal, enemyCore.CurrentEnemyType, "Enemy Type should not be normal when ar018 is of lv 240. Wave: " + i);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 24, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//        }
//        
//        [Test]
//        public void TestGoldDropWithAR027Buff() {
//            
//            Assert.AreEqual(1, enemyCore.Stage);
//            Assert.AreEqual(0, enemyCore.Wave);
//            Assert.AreEqual(10, enemyCore.WavePerStage);
//            
//            for(int i=0; i< 22 * (enemyCore.WavePerStage+1); i++) {
//                enemyCore.NextEnemy(false);
//            }
//            
//            // Unlock AR002
//            ArtifactAssets.Artifact027.ResetBalance(1);
//            
//            // Stage 23
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE23_NORMAL_GOLD * 1.15;
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE23_TREASURE_CHEST_GOLD * 1.15; 
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE23_MINI_BOSS_GOLD * 1.15; 
//                    break;
//                }
//                
//                Assert.AreEqual(23, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 23, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//            
//            ArtifactAssets.Artifact027.ResetBalance(2);
//            
//            // Stage 24
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE24_NORMAL_GOLD * 1.3; 
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE24_TREASURE_CHEST_GOLD * 1.3; 
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE24_MINI_BOSS_GOLD * 1.3;
//                    break;
//                }
//                
//                Assert.AreEqual(24, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 24, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//            
//            ArtifactAssets.Artifact027.ResetBalance(3);
//            
//            // Stage 25
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.Normal:
//                    expectedGold = STAGE25_NORMAL_GOLD * 1.45; 
//                    break;
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE25_TREASURE_CHEST_GOLD * 1.45; 
//                    break;
//                case EnemyType.BigBoss:
//                    expectedGold = STAGE25_BIG_BOSS_GOLD * 1.45;
//                    break;
//                }
//                
//                Assert.AreEqual(25, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 25, Wave " + i);
//                
//                if(i == enemyCore.WavePerStage) {
//                    // Make use of AR018 to get 100% chance Treasue Box Enemy
//                    ArtifactAssets.Artifact018.ResetBalance(240); // Suppose to be 100% Treasue Chest Chance when reach lv 240
//                }
//                enemyCore.NextEnemy(false);
//            }
//            
//            ArtifactAssets.Artifact027.ResetBalance(4);
//            
//            // Stage 26
//            for(int i=0; i<enemyCore.WavePerStage+1; i++) {
//                double expectedGold = 0;
//                switch(enemyCore.CurrentEnemyType) {
//                case EnemyType.TreasureChest:
//                    expectedGold = STAGE26_TREASURE_CHEST_GOLD * 1.6; // As AR002 affect normal enemy gold only
//                    break;
//                case EnemyType.MiniBoss:
//                    expectedGold = STAGE26_MINI_BOSS_GOLD * 1.6;  // As AR002 affect normal enemy gold only
//                    break;
//                }
//                
//                Assert.AreEqual(26, enemyCore.Stage);
//                Assert.AreEqual(i, enemyCore.Wave);
//                Assert.AreNotEqual(EnemyType.Normal, enemyCore.CurrentEnemyType, "Enemy Type should not be normal when ar018 is of lv 240. Wave: " + i);
//                Assert.AreEqual(expectedGold, goldDropCore.CurrentEnemyGoldDrop(), "Invalid Gold for Stage 26, Wave " + i);
//                
//                enemyCore.NextEnemy(false);
//            }
//        }
//    }
//}