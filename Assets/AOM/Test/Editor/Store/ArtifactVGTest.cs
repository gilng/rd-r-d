﻿//using UnityEngine;
//using System.Collections;
//
//using NUnit.Framework;
//
//namespace Ignite.AOM {
//
//    [TestFixture]
//    public class ArtifactVGTest : AOMCoreTest {
//
//        [Test]
//        public void TestMaxLevel() {
//            ArtifactVG ar1 = ArtifactAssets.Artifact001;
//            ar1.ResetBalance(1);
//            Assert.AreNotEqual(ar1.MaxLevel, ar1.GetBalance());
//            
//            ar1.ResetBalance(100);
//            Assert.AreNotEqual(ar1.MaxLevel, ar1.GetBalance());
//            
//            ArtifactVG ar10 = ArtifactAssets.Artifact010;
//            Assert.AreNotEqual(int.MaxValue, ar10.MaxLevel);
//
//            ar10.ResetBalance(ar10.MaxLevel);
//            Assert.AreEqual(ar10.MaxLevel, ar10.GetBalance());
//            
//            ar10.ResetBalance(ar10.MaxLevel + 1);
//            Assert.AreEqual(ar10.MaxLevel, ar10.GetBalance());
//            
//            ar10.Give(1);
//            Assert.AreEqual(ar10.MaxLevel, ar10.GetBalance());
//            
//            ar10.Give(100);
//            Assert.AreEqual(ar10.MaxLevel, ar10.GetBalance());
//        }
//
//        [Test]
//        public void TestAllDamageValue() {
//            ArtifactVG vg = ArtifactAssets.Artifact001;
//
//            vg.ResetBalance(1);
//            Assert.AreEqual(1, vg.GetBalance());
//            Assert.AreEqual(vg.BaseAllDamageValue * 2, vg.AllDamageValue);
//            
//            vg.ResetBalance(2);
//            Assert.AreEqual(vg.BaseAllDamageValue * 3, vg.AllDamageValue);
//            
//            ArtifactVG ar10 = ArtifactAssets.Artifact010;
//            Assert.AreNotEqual(int.MaxValue, ar10.MaxLevel);
//            ar10.ResetBalance(ar10.MaxLevel);
//            Assert.AreEqual(ar10.BaseAllDamageValue  * (ar10.MaxLevel+1), ar10.AllDamageValue);
//            
//            ar10.ResetBalance(ar10.MaxLevel + 100);
//            Assert.AreEqual(ar10.BaseAllDamageValue * (ar10.MaxLevel+1), ar10.AllDamageValue);
//        }
//        
//        [Test]
//        public void TestEffectValue() {
//            ArtifactVG ar10 = ArtifactAssets.Artifact010;
//            Assert.AreNotEqual(int.MaxValue, ar10.MaxLevel);
//            
//            ar10.ResetBalance(1);
//            Assert.AreEqual(ar10.BaseEffectValue, ar10.EffectValue);
//            
//            ar10.ResetBalance(2);
//            Assert.AreEqual(2 * ar10.BaseEffectValue, ar10.EffectValue);
//            
//            ar10.ResetBalance(ar10.MaxLevel);
//            Assert.AreEqual(ar10.MaxLevel * ar10.BaseEffectValue, ar10.EffectValue);
//
//            ar10.ResetBalance(ar10.MaxLevel + 1);
//            Assert.AreEqual(ar10.MaxLevel * ar10.BaseEffectValue, ar10.EffectValue);
//            
//            ar10.Give(1);
//            Assert.AreEqual(ar10.MaxLevel * ar10.BaseEffectValue, ar10.EffectValue);
//            
//            ar10.Give(100);
//            Assert.AreEqual(ar10.MaxLevel * ar10.BaseEffectValue, ar10.EffectValue);
//        }
//    }
//
//}