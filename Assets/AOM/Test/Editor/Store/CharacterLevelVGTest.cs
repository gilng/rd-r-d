//using System.Collections;
//using Soomla.Store;
//using NUnit.Framework;
//using UnityEngine;
//
//namespace Ignite.AOM {
//
//    [TestFixture]
//    public class CharacterLevelVGTest : AOMCoreTest
//    {   
//        private const double AVATAR_LV1_PRICE_ONE = 5;
//        private const double AVATAR_LV1_PRICE_TEN = 76;
//        private const double AVATAR_LV1_PRICE_HUNDRED = 99046;
//        
////        [TestFixtureSetUp]
////        public void InitAndResetSoomlaStore() {
////            //            Soomla.KeyValueStorage.Purge();
////            Soomla.CoreSettings.DebugUnityMessages = false;
////            SoomlaStore.Initialize(new AOMAssets());
////            //            AOMStoreInventory.ResetGold(0);
////        }
//
//        [Test]
//        public void TestAvatarMinLevelShouldBeOne() {
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            alvg.ResetBalance(0);
//            Assert.AreEqual(1, alvg.GetBalance());
//        }
//        
//        [Test]
//        public void TestHeroMinLevelShouldBeZero() {
//            CharacterLevelVG alvg = HeroAssets.Hero001;
//            alvg.ResetBalance(0);
//            Assert.AreEqual(0, alvg.GetBalance());
//        }
//
//        [Test]
//        public void TestAvatarLevelPriceLv1() {
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            alvg.ResetBalance(1);
//
//            Assert.AreEqual(AVATAR_LV1_PRICE_ONE, alvg.PriceForOne);
//            Assert.AreEqual(AVATAR_LV1_PRICE_TEN, alvg.PriceForTen);
//            Assert.AreEqual(AVATAR_LV1_PRICE_HUNDRED, alvg.PriceForHundred);
//        }
//        
//        [Test]
//        public void TestHero001PriceLv0() {
//			CharacterLevelVG alvg = HeroAssets.Hero001;
////            Debug.LogFormat("Before Balance: {0}", alvg.GetBalance());
//            alvg.ResetBalance(0);
////            Debug.LogFormat("Refreshed Balance: {0}", alvg.GetBalance());
//            Assert.AreEqual(alvg.baseCost, alvg.PriceForOne);
//        }
//        
//        [Test]
//        public void TestHero001PriceLv1() {
//			CharacterLevelVG alvg = HeroAssets.Hero001;
//            alvg.ResetBalance(1);
//            
//            Assert.AreEqual(54, alvg.PriceForOne);
//            Assert.AreEqual(761, alvg.PriceForTen);
//            Assert.AreEqual(990493, alvg.PriceForHundred);
//        }
//        
//        [Test]
//        public void TestAvatarLevelCanAffordLv1() {
//            
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            alvg.ResetBalance(1);
//
//            AOMStoreInventory.ResetGold(0);
//            Assert.IsFalse(alvg.CanAffordOne());
//            Assert.IsFalse(alvg.CanAffordTen());
//            Assert.IsFalse(alvg.CanAffordHundred());
//            
//            AOMStoreInventory.ResetGold(AVATAR_LV1_PRICE_ONE-1);
//            Assert.IsFalse(alvg.CanAffordOne());
//            Assert.IsFalse(alvg.CanAffordTen());
//            Assert.IsFalse(alvg.CanAffordHundred());
//            
//            AOMStoreInventory.ResetGold(AVATAR_LV1_PRICE_ONE);
//            Assert.IsTrue(alvg.CanAffordOne());
//            Assert.IsFalse(alvg.CanAffordTen());
//            Assert.IsFalse(alvg.CanAffordHundred());
//
//            AOMStoreInventory.ResetGold(AVATAR_LV1_PRICE_TEN - 1);
//            Assert.IsTrue(alvg.CanAffordOne());
//            Assert.IsFalse(alvg.CanAffordTen());
//            Assert.IsFalse(alvg.CanAffordHundred());
//            
//            AOMStoreInventory.ResetGold(AVATAR_LV1_PRICE_TEN);
//            Assert.IsTrue(alvg.CanAffordOne());
//            Assert.IsTrue(alvg.CanAffordTen());
//            Assert.IsFalse(alvg.CanAffordHundred());
//            
//            AOMStoreInventory.ResetGold(AVATAR_LV1_PRICE_HUNDRED - 1);
//            Assert.IsTrue(alvg.CanAffordOne());
//            Assert.IsTrue(alvg.CanAffordTen());
//            Assert.IsFalse(alvg.CanAffordHundred());
//
//            AOMStoreInventory.ResetGold(AVATAR_LV1_PRICE_HUNDRED);
//            Assert.IsTrue(alvg.CanAffordOne());
//            Assert.IsTrue(alvg.CanAffordTen());
//            Assert.IsTrue(alvg.CanAffordHundred());
//        }
//        
//        [Test]
//        public void TestAvatarLevelBuyLv1() {
//            
//            CharacterLevelVG alvg = AOMAssets.AvatarLevelVG;
//            alvg.ResetBalance(1);
//
//            // Nothing change when not enough gold
//            AOMStoreInventory.ResetGold(1);
//            Assert.IsFalse(alvg.CanAffordOne());
//            alvg.BuyOne();
//            Assert.AreEqual(1, alvg.GetBalance());
//            Assert.AreEqual(1, AOMStoreInventory.GetGoldBalance());
//
//            AOMStoreInventory.ResetGold(alvg.PriceForOne);
//            alvg.BuyOne();
//            Assert.AreEqual(2, alvg.GetBalance());
//            Assert.AreEqual(0, AOMStoreInventory.GetGoldBalance());
//
//            double gold = alvg.PriceForTen + 1d;
//            AOMStoreInventory.ResetGold(gold);
//            Assert.IsTrue(alvg.CanAffordTen());
//            alvg.BuyTen();
//            Assert.AreEqual(12, alvg.GetBalance());
//            Assert.AreEqual(1, AOMStoreInventory.GetGoldBalance());
//
//            gold = alvg.PriceForHundred + 2;
//            AOMStoreInventory.ResetGold(gold);
//            Assert.IsTrue(alvg.CanAffordHundred());
//            alvg.BuyHundred();
//            Assert.AreEqual(112, alvg.GetBalance());
//            Assert.AreEqual(2, AOMStoreInventory.GetGoldBalance());
//        }
//    }
//}
//
