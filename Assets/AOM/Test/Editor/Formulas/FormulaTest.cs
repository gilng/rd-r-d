//using System;
//using NUnit.Framework;
//
//namespace Ignite.AOM {
//    
//    [TestFixture]
//    public class FormulaTest
//    {
//        [Test]
//        public void TestEnemyTypeForStage() {
//
//            // No artifact effect
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(1,1,10, false));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(1,2,10, false));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(1,9,10, false));
//            Assert.AreEqual(EnemyType.MiniBoss, Formulas.EnemyTypeForStage(1,10,10, false));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(2,1,10, false));
//            Assert.AreEqual(EnemyType.MiniBoss, Formulas.EnemyTypeForStage(2,10,10, false));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(5,9,10, false));
//            Assert.AreEqual(EnemyType.BigBoss, Formulas.EnemyTypeForStage(5,10,10, false));
//            Assert.AreEqual(EnemyType.MiniBoss, Formulas.EnemyTypeForStage(9,10,10, false));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(10,9,10, false));
//            Assert.AreEqual(EnemyType.BigBoss, Formulas.EnemyTypeForStage(10,10,10, false));
//            
//            // Add artifact effect
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(1,1,9, false));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(1,2,9, false));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(1,8,9, false));
//            Assert.AreEqual(EnemyType.MiniBoss, Formulas.EnemyTypeForStage(1,9,9, false));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(2,1,9, false));
//            Assert.AreEqual(EnemyType.MiniBoss, Formulas.EnemyTypeForStage(2,9,9, false)); 
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(5,1,9, false));
//            Assert.AreEqual(EnemyType.BigBoss, Formulas.EnemyTypeForStage(5,9,9, false));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(5,1,5, false));
//            Assert.AreEqual(EnemyType.BigBoss, Formulas.EnemyTypeForStage(5,5,5, false));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(6,1,5, false));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(6,4,5, false));
//            Assert.AreEqual(EnemyType.MiniBoss, Formulas.EnemyTypeForStage(6,5,5, false)); 
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(10,1,5, false));  
//            Assert.AreEqual(EnemyType.BigBoss, Formulas.EnemyTypeForStage(10,5,5, false));  
//
//            // Infinity Loop Case (i.e. fail to KO boss)
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(1,1,10, true));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(1,2,10, true));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(1,9,10, true));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(1,10,10, true));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(2,1,10, true));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(2,10,10, true));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(5,9,10, true));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(5,10,10, true));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(9,10,10, true));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(10,9,10, true));
//            Assert.AreEqual(EnemyType.Normal, Formulas.EnemyTypeForStage(10,10,10, true));
//        }
//        
//        [Test]
//        public void TestEnemyHPForWave() {
//            Assert.AreEqual(29, Math.Floor(Formulas.EnemyHPForWave(1,1,10, false)));
//            Assert.AreEqual(29, Math.Floor(Formulas.EnemyHPForWave(1,2,10, false)));
//            Assert.AreEqual(29, Math.Floor(Formulas.EnemyHPForWave(1,9,10, false)));
//            Assert.AreEqual(58, Math.Floor(Formulas.EnemyHPForWave(1,10,10, false)));
//            Assert.AreEqual(45, Math.Floor(Formulas.EnemyHPForWave(2,1,10, false)));
//            Assert.AreEqual(45, Math.Floor(Formulas.EnemyHPForWave(2,2,10, false)));
//            Assert.AreEqual(45, Math.Floor(Formulas.EnemyHPForWave(2,9,10, false)));
//            Assert.AreEqual(182, Math.Floor(Formulas.EnemyHPForWave(2,10,10, false)));
//            Assert.AreEqual(71, Math.Floor(Formulas.EnemyHPForWave(3,1,10, false)));
//            Assert.AreEqual(71, Math.Floor(Formulas.EnemyHPForWave(3,9,10, false)));
//            Assert.AreEqual(429, Math.Floor(Formulas.EnemyHPForWave(3,10,10, false)));
//            Assert.AreEqual(112, Math.Floor(Formulas.EnemyHPForWave(4,1,10, false)));
//            Assert.AreEqual(112, Math.Floor(Formulas.EnemyHPForWave(4,9,10, false)));
//            Assert.AreEqual(899, Math.Floor(Formulas.EnemyHPForWave(4,10,10, false)));
//            Assert.AreEqual(176, Math.Floor(Formulas.EnemyHPForWave(5,1,10, false)));
//            Assert.AreEqual(176, Math.Floor(Formulas.EnemyHPForWave(5,9,10, false)));
//            Assert.AreEqual(1764, Math.Floor(Formulas.EnemyHPForWave(5,10,10, false)));
//            Assert.AreEqual(277, Math.Floor(Formulas.EnemyHPForWave(6,1,10, false)));
//            Assert.AreEqual(8577, Math.Floor(Formulas.EnemyHPForWave(9,10,10, false)));
//            Assert.AreEqual(1683, Math.Floor(Formulas.EnemyHPForWave(10,1,10, false)));
//            Assert.AreEqual(16833, Math.Floor(Formulas.EnemyHPForWave(10,10,10, false)));
//            Assert.AreEqual(677407338, Math.Floor(Formulas.EnemyHPForWave(34,10,10, false)));
//            Assert.AreEqual(1329411902, Math.Floor(Formulas.EnemyHPForWave(35,10,10, false)));
//        }
//        
//        [Test]
//        public void TestEnemyBaseGoldDropFormula() {
//            Assert.AreEqual(1, Formulas.EnemyBasicGold(1));
//            Assert.AreEqual(1, Formulas.EnemyBasicGold(2));
//            Assert.AreEqual(2, Formulas.EnemyBasicGold(3));
//            Assert.AreEqual(3, Formulas.EnemyBasicGold(4));
//            Assert.AreEqual(4, Formulas.EnemyBasicGold(5));
//            Assert.AreEqual(7, Formulas.EnemyBasicGold(6));
//            Assert.AreEqual(11, Formulas.EnemyBasicGold(7));
//            Assert.AreEqual(17, Formulas.EnemyBasicGold(8));
//            Assert.AreEqual(26, Formulas.EnemyBasicGold(9));
//            Assert.AreEqual(42, Formulas.EnemyBasicGold(10));
//            Assert.AreEqual(3090442707, Formulas.EnemyBasicGold(49));
//            Assert.AreEqual(4903918896, Formulas.EnemyBasicGold(50));
//        }
//
//        [Test]
//        public void TestEnemyGoldDropFormula() {
//            Assert.AreEqual(1, Math.Floor(Formulas.EnemyGoldDropForLevel(1,EnemyType.Normal)));
//			Assert.AreEqual(10, Math.Floor(Formulas.EnemyGoldDropForLevel(1,EnemyType.TreasureChest)));
//			Assert.AreEqual(2, Math.Floor(Formulas.EnemyGoldDropForLevel(1,EnemyType.MiniBoss)));
//			Assert.AreEqual(1, Math.Floor(Formulas.EnemyGoldDropForLevel(2,EnemyType.Normal)));
//            Assert.AreEqual(10, Math.Floor(Formulas.EnemyGoldDropForLevel(2,EnemyType.TreasureChest)));
//			Assert.AreEqual(4, Math.Floor(Formulas.EnemyGoldDropForLevel(2,EnemyType.MiniBoss)));
//			Assert.AreEqual(2, Math.Floor(Formulas.EnemyGoldDropForLevel(3,EnemyType.Normal)));
//            Assert.AreEqual(20, Math.Floor(Formulas.EnemyGoldDropForLevel(3,EnemyType.TreasureChest)));
//			Assert.AreEqual(12, Math.Floor(Formulas.EnemyGoldDropForLevel(3,EnemyType.MiniBoss)));
//            Assert.AreEqual(3, Math.Floor(Formulas.EnemyGoldDropForLevel(4,EnemyType.Normal)));
//            Assert.AreEqual(30, Math.Floor(Formulas.EnemyGoldDropForLevel(4,EnemyType.TreasureChest)));
//			Assert.AreEqual(24, Math.Floor(Formulas.EnemyGoldDropForLevel(4,EnemyType.MiniBoss)));
//            Assert.AreEqual(4, Math.Floor(Formulas.EnemyGoldDropForLevel(5,EnemyType.Normal)));
//            Assert.AreEqual(40, Math.Floor(Formulas.EnemyGoldDropForLevel(5,EnemyType.TreasureChest)));
//			Assert.AreEqual(40, Math.Floor(Formulas.EnemyGoldDropForLevel(5,EnemyType.BigBoss)));
//            Assert.AreEqual(7, Math.Floor(Formulas.EnemyGoldDropForLevel(6,EnemyType.Normal)));
//            Assert.AreEqual(70, Math.Floor(Formulas.EnemyGoldDropForLevel(6,EnemyType.TreasureChest)));
//			Assert.AreEqual(14, Math.Floor(Formulas.EnemyGoldDropForLevel(6,EnemyType.MiniBoss)));
//            Assert.AreEqual(11, Math.Floor(Formulas.EnemyGoldDropForLevel(7,EnemyType.Normal)));
//            Assert.AreEqual(110, Math.Floor(Formulas.EnemyGoldDropForLevel(7,EnemyType.TreasureChest)));
//			Assert.AreEqual(44, Math.Floor(Formulas.EnemyGoldDropForLevel(7,EnemyType.MiniBoss)));
//			Assert.AreEqual(420, Math.Floor(Formulas.EnemyGoldDropForLevel(10,EnemyType.BigBoss)));
//        }
//
//        [Test]
//        public void TestCharacterUpgradeGoldFormula() {
//            double baseGold = 5;
//            Assert.AreEqual(baseGold, Formulas.GoldToUpgradeCharacterToLevel(1, baseGold));
//            Assert.AreEqual(5, Formulas.GoldToUpgradeCharacterToLevel(2, baseGold));
//            Assert.AreEqual(6, Formulas.GoldToUpgradeCharacterToLevel(3, baseGold));
//            Assert.AreEqual(6, Formulas.GoldToUpgradeCharacterToLevel(4, baseGold));
//            Assert.AreEqual(7, Formulas.GoldToUpgradeCharacterToLevel(5, baseGold));
//            Assert.AreEqual(7, Formulas.GoldToUpgradeCharacterToLevel(6, baseGold));
//            Assert.AreEqual(8, Formulas.GoldToUpgradeCharacterToLevel(7, baseGold));
//            Assert.AreEqual(8, Formulas.GoldToUpgradeCharacterToLevel(8, baseGold));
//            Assert.AreEqual(9, Formulas.GoldToUpgradeCharacterToLevel(9, baseGold));
//            Assert.AreEqual(10, Formulas.GoldToUpgradeCharacterToLevel(10, baseGold));
//            Assert.AreEqual(10, Formulas.GoldToUpgradeCharacterToLevel(11, baseGold));
//        }
//    }
//}
//
