﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

namespace Ignite.AOM
{
    public class NativeShare : MonoBehaviour
    {
		public static void Share(string title, string text, string imagePath = "", string subject = "")
        {
#if UNITY_ANDROID
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            intentObject.Call<AndroidJavaObject>("setType", "text/plain");
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), title);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), text);
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

            currentActivity.Call("startActivity", intentObject);

#elif UNITY_IOS
			CallSocialShareAdvanced(title, subject, text, imagePath);
#else
			Debug.Log("No sharing set up for this platform.");
#endif
        }

#if UNITY_IOS
		public struct ConfigStruct
		{
			public string title;
			public string message;
		}

		[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

		public struct SocialSharingStruct
		{
			public string text;
			public string url;
			public string image;
			public string subject;
		}

		[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

		public static void CallSocialShare(string title, string message)
		{
			ConfigStruct conf = new ConfigStruct();
			conf.title  = title;
			conf.message = message;
			showAlertMessage(ref conf);
		}

		public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
		{
			SocialSharingStruct conf = new SocialSharingStruct();
			conf.text = defaultTxt; 
			conf.url = url;
			conf.image = img;
			conf.subject = subject;

			showSocialSharing(ref conf);
		}
#endif
    }
}