﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace AssetBundles
{
    public class AssetBundlesMenuItems
    {
        const string kSimulationMode = "Assets/AssetBundles/Simulation Mode";

        [MenuItem(kSimulationMode)]
        public static void ToggleSimulationMode()
        {
            AssetBundleManager.SimulateAssetBundleInEditor = !AssetBundleManager.SimulateAssetBundleInEditor;
        }

        [MenuItem(kSimulationMode, true)]
        public static bool ToggleSimulationModeValidate()
        {
            Menu.SetChecked(kSimulationMode, AssetBundleManager.SimulateAssetBundleInEditor);
            return true;
        }

        [MenuItem("Assets/AssetBundles/Build All Platform AssetBundles")]
        static public void BuildAllPlatformAssetBundles()
        {
            BuildScript.BuildAllPlatformAssetBundles();
        }

        [MenuItem("Assets/AssetBundles/Build AssetBundles")]
        static public void BuildAssetBundles()
        {
            BuildScript.BuildAssetBundles();
        }

        [MenuItem("Assets/AssetBundles/Clean Cache")]
        public static void CleanCache()
        {
            if (Caching.CleanCache())
            {
                Debug.LogWarning("Successfully cleaned all caches.");
            }
            else
            {
                Debug.LogWarning("Cache was in use.");
            }
        }
    }
}